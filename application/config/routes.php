<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "admin_route";
$route['404_override'] = '';
$route['src/(:any)'] = "sourcefile/index/$1";
$route['lgphoto/(:any)'] = "lgphoto/create/$1";
$route['admin'] = "administrator/admin_route";
$route['administrator'] = "administrator/index";
$route['qr/(:any)'] = "qrscan/index/$1";
$route['qr'] = "qrscan/error/$1";
global $cms_dyn_route;
if (!is_null($cms_dyn_route) && is_array($cms_dyn_route)) {
    foreach($cms_dyn_route as $route_key=>$dynamic_route) {
        $route[$route_key] = $dynamic_route;
    }
}
/* End of file routes.php */
/* Location: ./application/config/routes.php */