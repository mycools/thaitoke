<?php
class Sourcefile extends CI_Controller{
	public function __construct()
	{
			parent::__construct();
			//$this->db->close();
			
			
	}
	public function index()
	{
		$numargs = func_num_args();
		$arg_list = func_get_args();
		$file_path = "public/uploads";
		
		for ($i = 1; $i < $numargs-1; $i++) {
			$file_path .= "/";
			$file_path .= $arg_list[$i];
		}
		$file_path .= "/".$arg_list[$numargs-1];
		$info = pathinfo($file_path);
		if(file_exists($file_path)){
			header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + 86400));
			header('Cache-Control: max-age=86400');
			header('Pragma: public');
			if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])){
		      header('Last-Modified: '.$_SERVER['HTTP_IF_MODIFIED_SINCE'],true,304);
		      exit;
		    }
		}
		if(!@$info['extension']){
			redirect(current_url()."/default.png");
		}
		if(in_array(strtolower($info['extension']),array("jpeg","jpg","png","jpeg"))){
			@list($width,$height) = explode("x",$arg_list[0]);
			if(!$height){
				$height=$width;	
			}
			$this->makeImageCache($width,$height,$file_path);
		}else{
			$this->download($file_path);
		}
	}
	private function makeImageCache($width,$height,$file_path)
	{
		$info = pathinfo($file_path);
		$file_paths = realpath('') . "/" . $file_path;
		$cache_folder = $info['dirname'] . "/{$width}x{$height}/";
		if(!file_exists($file_paths)){
			$file_path = $info['dirname'] . "/default.jpg";
		}
		if(!file_exists($file_paths)){
			$file_path = $info['dirname'] . "/default.png";
		}
		if(!file_exists($file_path)){
			$file_path =  "public/uploads/default.png";
		}
		if(!file_exists($file_path)){
			$file_path =  "public/uploads/default.jpg";
		}
		if(!file_exists($file_path)){
			header('HTTP/1.1 404 Not Found');
			show_404();
		}
		$info = pathinfo($file_path);
		$config['source_image']	= $file_path;
		$config['create_thumb'] = false;
		$config['maintain_ratio'] = true;
		$config['quality']	 = '100';
		$config['width']	 = $width;
		$config['height']	= $height;
		$config['dynamic_output'] = true;
		$this->load->library('image_lib', $config); 
		if ( ! $this->image_lib->resize())
		{
			show_error($this->image_lib->display_errors() . "<br />" . $file_path);
		}
	}
	private function download($file_path)
	{
		$file_path = realpath('') . "/" . $file_path;
		$info = pathinfo($file_path);
		if(!file_exists($file_path)){
			header('HTTP/1.1 404 Not Found');
			show_404();	
		}
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.$info['base_name'].'"'); 
		header('Content-Transfer-Encoding: binary');	
		readfile($file_path);
	}
}
?>