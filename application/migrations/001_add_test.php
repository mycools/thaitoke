<?php
	class Migration_Add_test extends CI_Migration{
		public function up()
		{
			$mssql = $this->load->database("mssql", true);
			$tables = $mssql->list_tables();

			foreach ($tables as $table)
			{
			   echo $table;
			}
			
			$this->load->dbforge("mssql", true);
			$fields = array(
                        'blog_id' => array(
                                                 'type' => 'INT',
                                                 'constraint' => 5,
                                                 'unsigned' => TRUE,
                                                 'IDENTITY' => TRUE
                                          ),
                        'blog_title' => array(
                                                 'type' => 'VARCHAR',
                                                 'constraint' => '100',
                                          ),
                        'blog_author' => array(
                                                 'type' =>'VARCHAR',
                                                 'constraint' => '100',
                                                 'default' => 'King of Town',
                                          ),
                        'blog_description' => array(
                                                 'type' => 'TEXT',
                                                 'null' => TRUE,
                                          ),
                );

			$this->dbforge->add_field($fields);
			
			$this->dbforge->add_key('blog_id', TRUE);
			// gives PRIMARY KEY (blog_id)
			
			$this->dbforge->add_key('blog_title');
			// gives KEY (blog_title)
			
			$this->dbforge->create_table('blog');
		}
		public function down()
		{
			
		}
	}