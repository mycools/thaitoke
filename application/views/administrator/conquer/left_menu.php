
			<ul class="main-navigation-menu">
            	<?php foreach($menu_entry as $rs){ ?>
                <?php if( isset( $rs['submenu_entry'] ) && count( $rs['submenu_entry'] ) > 0 ){ ?>
                <li class="has-sub <?php echo $rs['active']; ?>">
                	<a href="<?php echo $rs['link']; ?>">
					<div class="item-content">
							<div class="item-media">
								<i class="<?php echo $rs['icon']; ?>"></i>
							</div>
							<div class="item-inner">
								<span class="title"> <?php echo $rs['label']; ?> </span>
							</div>
						</div>	
					</a>
					<ul class="sub-menu">
                    	<?php foreach($rs['submenu_entry'] as $submenu){ ?>
						<li class="<?php echo $submenu['active']; ?>"><a href="<?php echo $submenu['link']; ?>"><span class="title"><i class="<?php echo $submenu['icon']; ?>"></i> <?php echo $submenu['label']; ?></span></a></li>
                        <?php } ?>
					</ul>
				</li>
                <?php }else{ ?>
				<li class="<?php echo $rs['active']; ?>">
					<a href="<?php echo $rs['link']; ?>">
						<div class="item-content">
							<div class="item-media">
								<i class="<?php echo $rs['icon']; ?>"></i>
							</div>
							<div class="item-inner">
								<span class="title"> <?php echo $rs['label']; ?> </span>
							</div>
						</div>	
					</a>				
				</li>
             	<?php } ?>
				<?php } ?>
                
			</ul>
			
			
		