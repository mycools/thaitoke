<header class="navbar navbar-default navbar-static-top">
	<!-- start: NAVBAR HEADER -->
	<div class="navbar-header">
		<a href="#" class="sidebar-mobile-toggler pull-left hidden-md hidden-lg" class="btn btn-navbar sidebar-toggle" data-toggle-class="app-slide-off" data-toggle-target="#app" data-toggle-click-outside="#sidebar">
			<i class="ti-align-justify"></i>
		</a>
		<a class="navbar-brand" href="<?php echo admin_url(); ?>">
			<img src="assets/images/logo.png" alt="Thai Toke"/>
		</a>
		<a href="#" class="sidebar-toggler pull-right visible-md visible-lg" data-toggle-class="app-sidebar-closed" data-toggle-target="#app">
			<i class="ti-align-justify"></i>
		</a>
		<a class="pull-right menu-toggler visible-xs-block" id="menu-toggler" data-toggle="collapse" href=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<i class="ti-view-grid"></i>
		</a>
	</div>
	<!-- end: NAVBAR HEADER -->
	<!-- start: NAVBAR COLLAPSE -->
	<div class="navbar-collapse collapse">
		<ul class="nav navbar-right">
			
			<!-- start: USER OPTIONS DROPDOWN -->
			<li class="dropdown current-user">
				<a href class="dropdown-toggle" data-toggle="dropdown">
					<?php if($user_info['user_avatar'] != ""){ ?>
					<img src="<?php echo site_url("src/50x50/user_admin/".$user_info['user_avatar']); ?>" />
					<?php }else{ ?>
					<i class="icon-user"></i>
					<?php } ?><span class="username"><?php echo $user_info['user_fullname']; ?> <i class="ti-angle-down"></i></i></span>
				</a>
				<ul class="dropdown-menu dropdown-dark">
					<li><a href="<?php echo admin_url("user_profile"); ?>"><i class="icon-github-square"></i> ข้อมูลส่วนตัว</a></li>
					<li><a href="<?php echo site_url(); ?>" target="_blank"><i class=" icon-home"></i> ไปยังหน้าแรกเว็บไซต์</a></li>
					<li><a href="<?php echo admin_url("logout"); ?>" class="force_redirect"><i class="icon-sign-out"></i> ออกจากระบบ</a></li>
					
				</ul>
			</li>
			<!-- end: USER OPTIONS DROPDOWN -->
		</ul>
		<!-- start: MENU TOGGLER FOR MOBILE DEVICES -->
		
		<!-- end: MENU TOGGLER FOR MOBILE DEVICES -->
	</div>
	
	<!-- end: NAVBAR COLLAPSE -->
</header>