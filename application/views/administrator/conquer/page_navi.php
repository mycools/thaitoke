<section id="page-title">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle"><?php echo $navi_title; ?></h1>
			
			<?php if( isset( $page_detail ) ): ?>
			    <span class="mainDescription"><?php echo $page_detail; ?></span>
            <?php endif; ?>
			
		</div>
		<ol class="breadcrumb">
		<li>
				<span>
					<i class="icon-home"></i>
					<a href="<?php echo admin_url(); ?>"><?php echo $this->admin_library->getCompanyName(); ?></a> 
				</span>
			</li>
		<?php foreach($this->admin_library->breadcrumb() as $breadcrumb){ ?>
			<li>
				<span>
					<i class="<?php echo $breadcrumb['menu_icon']; ?>"></i>
                    <a href="<?php echo $breadcrumb['menu_link']; ?>"><?php echo $breadcrumb['menu_label']; ?></a>
				</span>
			</li>
		<?php } ?>
			
		</ol>
	</div>
</section>				
