<div class="col-sm-12">
	<!-- BEGIN SAMPLE TABLE PORTLET-->
	<div class="panel panel-white" id="panel4">
		<div class="panel-heading">
			<h4 class="panel-title text-primary"><i class="icon-list-alt"></i> Washer product income testing [<?php echo $income_info['income_job_id']; ?>]</h4>
			<div class="panel-tools">
				
				<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>	
			</div>
		</div>
		<div class="panel-body">
    <?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
    	<form method="post" name="menu_form" id="menu_form" enctype="multipart/form-data" class="form-vertical">
    	<input type="hidden" name="income_id" value="<?php echo $income_id;?>" />
    	
          <?php if(@$success_message!=NULL){ ?>
      <div class="alert alert-success"> 
        <button class="close" data-dismiss="alert">×</button>
        <strong>Success !</strong> <?php echo $success_message; ?>
      </div>
      <?php } ?>

        <?php echo @$validation_errors; ?>
            <?php if(@$error_message!=NULL){ ?>
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">×</button>
                    <strong>Error !</strong> <?php echo $error_message; ?>
                </div>
            <?php }?>
			

			           
            <div class="form-group">
                <label ></label>
                <div class="controls" style="margin-left:0 !important;">
                <div class="table-responsive">        
                <table width="100%" border="1" class="table table-striped table-bordered">
                	<thead>
                	<tr><th colspan="14">Standard Test</th></tr>
	                <tr>
	                	<th width="10" rowspan="2">No.</th>
	                	<th rowspan="2">Description</th>
	                	<th rowspan="2">Standard</th>
	                	<?php /*?><th rowspan="2">Inspection tool</th>
	                	<th rowspan="2">Frequency of test</th>
	                	<th rowspan="2">Inspection quantity</th><?php */?>
	                	<th width="60" rowspan="2">1</th>
	                	<th width="60" rowspan="2">2</th>
	                	<th width="60" rowspan="2">3</th>
	                	<th width="60" rowspan="2">4</th>
	                	<th width="60" rowspan="2">5</th>
	                	<th width="60" rowspan="2">AVG.</th>
	                	<th colspan="2">Overall result</th>
	                	<th width="150" rowspan="2">Remark</th>
	                	
	                </tr>
                    <tr>
	                	
	                	<th width="40">Pass</th>
	                	<th width="40">No</th>
	            
	                	
	                </tr>
                	</thead>
                	<tbody>
	                <?PHP	$i = 0; 
                            foreach( $std_list->result_array() AS $row ){      
                            ?>
	                <tr>
		                <td align="center"><?php echo $i+1;?><input type="hidden" name="washer_standard_id[]" value="<?php echo $row['ps_id'];?>" /></td>
		                <td align="center"><?php echo $row['standard_description'];?></td>
		                <td align="center">
			                <?php echo $row['ps_min']. $row['ps_min_unit']."<br/> ".$row['ps_max']. $row['ps_max_unit'];?>
			                <input type="hidden" id="ps_min_<?php echo $i; ?>" value="<?php echo $row['ps_min']; ?>" />
			                <input type="hidden" id="ps_max_<?php echo $i; ?>" value="<?php echo $row['ps_max']; ?>" />
			            </td>
		               <?php /*?> <td align="center"><?php $ins = $this->income_test_model->get_inspection($row['inspection_id']);  echo $ins['inspection_name']; ?></td>
		                <td align="center">Before Delivery</td>
		                <td align="center">5pcs/lot</td><?php */?>
		                <td class="center"><input class="span12" name="test1[]" id="test1[]" type="text" value="<?php echo set_value('test1[]'); ?>" onblur="checkvalue(<?php echo $i; ?>, $(this));" /></td>
		                <td class="center"><input class="span12" name="test2[]" id="test2[]" type="text" value="<?php echo set_value('test2[]'); ?>" onblur="checkvalue(<?php echo $i; ?>, $(this));" /></td>
		                <td class="center"><input class="span12" name="test3[]" id="test3[]" type="text" value="<?php echo set_value('test3[]'); ?>" onblur="checkvalue(<?php echo $i; ?>, $(this));" /></td>
		                <td class="center"><input class="span12" name="test4[]" id="test4[]" type="text" value="<?php echo set_value('test4[]'); ?>" onblur="checkvalue(<?php echo $i; ?>, $(this));" /></td>
		                <td class="center"><input class="span12" name="test5[]" id="test5[]" type="text" value="<?php echo set_value('test5[]'); ?>" onblur="checkvalue(<?php echo $i; ?>, $(this));" /></td>
		                <td class="center"><input class="span12" name="test_avg[]" id="test_avg[]" type="text" value="<?php echo set_value('test_avg[]'); ?>" onblur="checkvalue(<?php echo $i; ?>, $(this));" /></td>
		                <td class="center">
		                	<input name="testing_status[<?php echo $i; ?>]" value="pass" type="radio" style="margin-left:0 !important;" />
		                </td>
		                <td class="center">
		                	<input name="testing_status[<?php echo $i; ?>]" value="not_pass" type="radio" style="margin-left:0 !important;" />
		                </td>
		                <td><input class="input span12" name="remark[]" id="remark[]" type="text" value="<?php echo set_value('remark[]'); ?>" /></td>
	                </tr>
	                 <?php
                         $i = $i+1;   }
                        ?>
                        
                    <tr><th colspan="14">Size Test</th></tr>
	                <tr>
	                	<th width="10" rowspan="2">No.</th>
	                	<th rowspan="2">Description</th>
	                	<th rowspan="2">Standard</th>
	                	<?php /*?><th rowspan="2">Inspection tool</th>
	                	<th rowspan="2">Frequency of test</th>
	                	<th rowspan="2">Inspection quantity</th><?php */?>
	                	<th width="60" rowspan="2">1</th>
	                	<th width="60" rowspan="2">2</th>
	                	<th width="60" rowspan="2">3</th>
	                	<th width="60" rowspan="2">4</th>
	                	<th width="60" rowspan="2">5</th>
	                	<th width="60" rowspan="2">AVG.</th>
	                	<th colspan="2">Overall result</th>
	                	<th width="150" rowspan="2">Remark</th>
	                	
	                </tr>
                    <tr>
	                	
	                	<th width="40">Pass</th>
	                	<th width="40">No</th>
	            
	                	
	                </tr>
                	</thead>
                	<tbody>
	                <?PHP	$i = 0; 
                            foreach( $cer_size_list->result_array() AS $row1 ){      
                            ?>
	                <tr>
		                <td align="center"><?php echo $i+1;?><input type="hidden" name="washer_cer_size_id[]" value="<?php echo $row1['ps_id'];?>" /></td>
		                <td align="center"><?php echo $row1['ps_description'];?></td>
		                <td align="center">
			                <?php echo $row1['ps_min']. $row1['ps_min_unit']."<br/> ".$row1['ps_max']. $row1['ps_max_unit'];?>
			                <input type="hidden" id="cps_min_<?php echo $i; ?>" value="<?php echo $row1['ps_min']; ?>" />
			                <input type="hidden" id="cps_max_<?php echo $i; ?>" value="<?php echo $row1['ps_max']; ?>" />
			            </td>
		               <?php /*?> <td align="center"><?php $ins1 = $this->income_test_model->get_inspection($row1['inspection_id']);  echo $ins1['inspection_name']; ?></td>
		                <td align="center">Before Delivery</td>
		                <td align="center">5pcs/lot</td><?php */?>
		                <td class="center"><input class="span12" name="ctest1[]" id="ctest1[]" type="text" value="<?php echo set_value('ctest1[]'); ?>" onblur="c_checkvalue(<?php echo $i; ?>, $(this));" /></td>
		                <td class="center"><input class="span12" name="ctest2[]" id="ctest2[]" type="text" value="<?php echo set_value('ctest2[]'); ?>" onblur="c_checkvalue(<?php echo $i; ?>, $(this));" /></td>
		                <td class="center"><input class="span12" name="ctest3[]" id="ctest3[]" type="text" value="<?php echo set_value('ctest3[]'); ?>" onblur="c_checkvalue(<?php echo $i; ?>, $(this));" /></td>
		                <td class="center"><input class="span12" name="ctest4[]" id="ctest4[]" type="text" value="<?php echo set_value('ctest4[]'); ?>" onblur="c_checkvalue(<?php echo $i; ?>, $(this));" /></td>
		                <td class="center"><input class="span12" name="ctest5[]" id="ctest5[]" type="text" value="<?php echo set_value('ctest5[]'); ?>" onblur="c_checkvalue(<?php echo $i; ?>, $(this));" /></td>
		                <td class="center"><input class="span12" name="ctest_avg[]" id="ctest_avg[]" type="text" value="<?php echo set_value('ctest_avg[]'); ?>" onblur="c_checkvalue(<?php echo $i; ?>, $(this));" /></td>
		                <td class="center">
		                	<input name="ctesting_status[<?php echo $i; ?>]" value="pass" type="radio" style="margin-left:0 !important;" />
		                </td>
		                <td class="center">
		                	<input name="ctesting_status[<?php echo $i; ?>]" value="not_pass" type="radio" style="margin-left:0 !important;" />
		                </td>
		                <td><input class="input span12" name="cremark[]" id="cremark[]" type="text" value="<?php echo set_value('cremark[]'); ?>" /></td>
	                </tr>
	                 <?php
                         $i = $i+1;   }
                        ?>

                	</tbody>
                </table>                
                </div>          
                 
                 </div>
            </div>
            

            <div class="form-actions">
                <button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> Save &amp Chemical  Testing</button>
               <a class="btn btn-warning" href="<?php echo admin_url("income_manager/washer_list"); ?>"><i class="icon-reply"></i> ยกเลิก</a>
            </div>
    	</form>
    	
    </div>
  </div>
  <!-- END RECENT ORDERS PORTLET--> 
</div>
<style>
.table thead th{
	text-align: center;
}
.center{
	text-align: center;
	vertical-align: middle;
}
</style>
<script type="text/javascript">
function checkvalue(i, input){
	var min_val = $('#ps_min_'+i).val();
	var max_val = $('#ps_max_'+i).val();
	var input_val = input.val();
	if(input.val()!=0 || input.val()!=''){
		if(parseFloat(input_val)<parseFloat(min_val)){
			alert('ค่าที่กรอกต่ำกว่าค่า min ค่ะ');
			input.val('');
			input.focus();
		}else if(parseFloat(input_val)>parseFloat(max_val)){
			alert('ค่าที่กรอกสูงกว่าค่า max ค่ะ');
			input.val('');
			input.focus();
		}else{
			return false;
		}
	}
}

function c_checkvalue(i, input){
	var min_val = $('#cps_min_'+i).val();
	var max_val = $('#cps_max_'+i).val();
	var input_val = input.val();
	if(input.val()!=0 || input.val()!=''){
		if(parseFloat(input_val)<parseFloat(min_val)){
			alert('ค่าที่กรอกต่ำกว่าค่า min ค่ะ');
			input.val('');
			input.focus();
		}else if(parseFloat(input_val)>parseFloat(max_val)){
			alert('ค่าที่กรอกสูงกว่าค่า max ค่ะ');
			input.val('');
			input.focus();
		}else{
			return false;
		}
	}
}
</script>