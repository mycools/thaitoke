<div class="col-sm-12">
	<!-- BEGIN SAMPLE TABLE PORTLET-->
	<div class="panel panel-white" id="panel4">
		<div class="panel-heading">
			<h4 class="panel-title text-primary"><i class="icon-plus"></i> แก้ไขเมนู</h4>
			<div class="panel-tools">
				
				<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>	
			</div>
		</div>
		<div class="panel-body">
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
			<form method="post">
			<input type="hidden" name="menu_id" value="<?php echo set_value('menu_id',$this->_data['row']['menu_id']); ?>" />
				<div class="form-group">
					<label  for="username">ชื่อเมนู * :</label>
					
						
						   <input class="form-control" name="menu_label" id="username" type="text" placeholder="กรุณาระบุชื่อเมนู" value="<?php echo set_value('menu_label',$this->_data['row']['menu_label']); ?>" />	
						
					
				</div>

				<div class="form-group">
					<label >เชื่อมโยงไปยัง * :</label>
					
						<select name="menu_link" id="menu_link" class="js-example-basic-single js-states form-control" data-placeholder="เลือกลิ้งค์เชื่อมโยง">
						   <?php foreach($this->controllerlist->getControllers() as $cont=>$rs){ ?>
					       <optgroup label="<?php echo ucfirst($cont); ?>">
					       		<?php foreach($rs as $method){ ?>
					       		<?php 
					       		
					       		$url =  $cont."/".$method; 
					       		?>
					       			<option value="<?php echo $url; ?>" <?php if(set_value("menu_link",$this->_data['row']['menu_link'])==$url){ ?> selected="selected" <?php } ?>><?php echo ucfirst($url); ?></option>
					       		<?php } ?>
					       </optgroup>
					       <?php } ?>
						</select>
					
				</div>
				<!--
<div class="form-group">
					<label  >การเรียงลำดับ * :</label>
					<div class="controls">
						<select name="menu_sequent" id="menu_sequent" class="span4 chosen" data-placeholder="เลือกการเรียงลำดับ">
							<option value=""></option>
						   <?php foreach($this->menu_model->dataTable() as $rs){ ?>
					       <option value="<?php echo $rs['menu_sequent']; ?>">แสดงก่อน "<?php echo $rs['menu_label']; ?>"</option>
					       <?php } ?>
					       <option value="<?php echo $this->menu_model->get_menu_last_sequent(); ?>" selected="selected">อยู่ล่างสุดเสมอ</option>
						</select>
					</div>
				</div>
-->
				<div class="form-group">
					<label >Icon * :</label>
					
						<select name="menu_icon" id="menu_icon"   class="js-example-basic-single js-states form-control" data-placeholder="เลือก Icon">
							<option value=""></option>
						   <?php foreach($this->menu_model->get_icons() as $key=>$text){ ?>
					       <option data-class="<?php echo $key; ?>"  value="<?php echo $key; ?>" <?php if(set_value("menu_icon",$this->_data['row']['menu_icon'])==$key){ ?> selected="selected" <?php } ?>> <?php echo ucfirst(str_replace("icon-","",$key)); ?></option>
					       <?php } ?>
						</select>
					
				</div>
				<?php //var_dump($this->menu_model->get_icons()); ?>
				<div class="form-actions">
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-mini" href="<?php echo admin_url("menu_manager/index"); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			 </form>
		</div>
	</div>
</div>
<style type="text/css">
#menu_icon_chzn{
	font-family: 'FontAwesome',Tahoma;
}
</style>