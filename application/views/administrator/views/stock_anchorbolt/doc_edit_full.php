
<div class="row-fluid">
<div class="span12">
	<div class="widget">
		<div class="widget-title">
			<h4><i class="icon-plus"></i>แก้ไขใบงาน</h4>
			<span class="tools">
			<a href="javascript:;" class="icon-chevron-down"></a>
			<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>		
			</span>							
		</div>
		<div class="widget-body form">
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด ! </strong>','</div>'); ?>
			<form method="post" class="form-vertical">
				<input name="size_id" id="size_id" type="hidden" value="<?php echo set_value('size_id',$size_id); ?>" />
				<input name="pro_id" id="pro_id" type="hidden" value="<?php echo set_value('pro_id',$pro_id); ?>" />
				
				<div class="form-group">
					<label  for="doc_no">เลขที่ใบงาน * :</label>
					<div class="controls">
						<input type="text" name="doc_no" id="doc_no" value="<?php echo set_value('doc_no', $row['doc_no']); ?>" class="form-control input-md" />
					</div>
				</div>

				<div class="form-group">
					<label  for="doc_qty">จำนวนเหล็ก * :</label>
					<div class="controls">
						<input type="text" name="doc_qty" id="doc_qty" value="<?php echo set_value('doc_qty', $row['doc_qty']); ?>" class="form-control input-sm" disabled="disabled" />
					</div>
				</div>
				
				<div class="form-group">
					<label  for="doc_outcome">ผลิตเป็นสินค้า * : </label>
					<div class="controls">
						<input type="text" name="doc_outcome" id="doc_outcome" value="<?php echo set_value('doc_outcome', $row['doc_outcome']); ?>" class="form-control input-md" />
					</div>
				</div>
				
				<div class="form-group">
					<label  for="doc_compound">ชุดประกอบ * : </label>
					<div class="controls">
						<textarea class="span12" name="doc_compound" id="doc_compound" placeholder="" cols="5" rows="5"  /><?php echo set_value('doc_compound', $row['doc_compound']); ?></textarea>	
					</div>
				</div>
				
				<div class="form-group">
					<label  for="doc_amount">จำนวนผลิต * : </label>
					<div class="controls">
						<input type="text" name="doc_amount" id="doc_amount" value="<?php echo set_value('doc_amount', $row['doc_amount']); ?>" class="form-control input-sm" disabled="disabled" />
					</div>
				</div>
				
				<div class="form-group">
					<label  for="doc_customer">ชื่อบริษัทลูกค้า * : </label>
					<div class="controls">
						<input type="text" name="doc_customer" id="doc_customer" value="<?php echo set_value('doc_customer', $row['doc_customer']); ?>" class="form-control input-md" />
					</div>
				</div>
				
				<div class="form-group">
					<label  for="doc_due">กำหนดส่ง * : </label>
					<div class="controls">
						<input type="text" name="doc_due" id="doc_due" value="<?php echo set_value('doc_due', $row['doc_due']); ?>" class="form-control input-md datepicker" />
					</div>
				</div>
				
				<div class="form-group">
					<label  for="doc_remark">หมายเหตุ : </label>
					<div class="controls">
						<textarea class="span12" name="doc_remark" id="doc_remark" placeholder="" cols="5" rows="5"  /><?php echo set_value('doc_remark', $row['doc_remark']); ?></textarea>	
					</div>
				</div>
			</div>
			<div class="form-actions">
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-mini" href="<?php echo admin_url($this->router->fetch_class() . "/doc_list_full/".$type."/".$pro_id."/".$size_id); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			 </form>
		</div>
	</div>
</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('.datepicker').datepicker({
		dateFormat : 'yy-mm-dd',
		changeMonth : true,
		changeYear : true
	});
});
</script>