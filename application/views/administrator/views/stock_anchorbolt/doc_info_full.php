
<div class="row-fluid">
<div class="span12">
	<div class="widget">
		<div class="widget-title">
			<h4><i class="icon-plus"></i>รายละเอียดใบงาน</h4>
			<span class="tools">
			<a href="javascript:;" class="icon-chevron-down"></a>
			<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>		
			</span>							
		</div>
		<div class="widget-body form">
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด ! </strong>','</div>'); ?>
			<form method="post" class="form-vertical">
				<input name="size_id" id="size_id" type="hidden" value="<?php echo set_value('size_id',$size_id); ?>" />
				<input name="pro_id" id="pro_id" type="hidden" value="<?php echo set_value('pro_id',$pro_id); ?>" />
				
				<div class="form-group">
					<label  for="doc_no">เลขที่ใบงาน * :</label>
					<div class="controls">
						<?php echo $row['doc_no']; ?>
					</div>
				</div>

				<div class="form-group">
					<label  for="doc_qty">จำนวนเหล็ก * :</label>
					<div class="controls">
						<?php echo $row['doc_qty']; ?>
					</div>
				</div>
				
				<div class="form-group">
					<label  for="doc_outcome">ผลิตเป็นสินค้า * : </label>
					<div class="controls">
						<?php echo $row['doc_outcome']; ?>
					</div>
				</div>
				
				<div class="form-group">
					<label  for="doc_compound">ชุดประกอบ * : </label>
					<div class="controls">
						<?php echo $row['doc_compound']; ?>
					</div>
				</div>
				
				<div class="form-group">
					<label  for="doc_amount">จำนวนผลิต * : </label>
					<div class="controls">
						<?php echo $row['doc_amount']; ?>
					</div>
				</div>
				
				<div class="form-group">
					<label  for="doc_customer">ชื่อบริษัทลูกค้า * : </label>
					<div class="controls">
						<?php echo $row['doc_customer']; ?>
					</div>
				</div>
				
				<div class="form-group">
					<label  for="doc_due">กำหนดส่ง * : </label>
					<div class="controls">
						<?php echo $row['doc_due']; ?>
					</div>
				</div>
				
				<div class="form-group">
					<label  for="doc_remark">หมายเหตุ : </label>
					<div class="controls">
						<?php echo $row['doc_remark']; ?>
					</div>
				</div>
			</div>
			<div class="form-actions">
				 	<a class="btn btn-mini" href="<?php echo admin_url($this->router->fetch_class() . "/doc_list_full/".$type."/".$pro_id."/".$size_id); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			 </form>
		</div>
	</div>
</div>
</div>