<div class="span12">
	<div class="widget">
		<div class="widget-title">
			<h4><i class="icon-plus"></i> สร้างข้อมูลใหม่</h4>
			<span class="tools">
			<a href="javascript:;" class="icon-chevron-down"></a>
			<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>		
			</span>							
		</div>
		<div class="widget-body form">
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
			<form method="post" class="form-vertical">				
				<fieldset>
					<legend>ลูกค้า</legend>
					<div class="form-group">
						<label  >Customer * :</label>
						<div class="controls">
							<select name="customer_id" id="customer_id" class="span4 chosen" data-placeholder="เลือก SUPPLIER">
								<option value=""></option>
							   <?php foreach($this->final_manager_anchor_model->getCustomer() as $rs){ ?>
						       <option value="<?php echo $rs['customer_id']; ?>" <?php if($this->_data['row']['customer_id']==$rs['customer_id']){ ?> selected="selected" <?php } ?>><?php echo $rs['customer_name']; ?></option>
						       <?php } ?>
							</select>
						</div>
				</div>
                <div class="form-group">
						<label  for="final_sale_order_no">Job No. :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="final_job_no" id="final_job_no" type="text" placeholder="" value="<?php echo set_value('final_job_no',$this->_data['row']['final_job_no']); ?>" />	
							</div>
						
						</div>
					</div>
					
				<div class="form-group">
						<label  for="final_invoice_no">Invoice NO * :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="final_invoice_no" id="final_invoice_no" type="text" placeholder="" value="<?php echo set_value('final_invoice_no',$this->_data['row']['final_invoice_no']); ?>" />	
							</div>
						
						</div>
					</div>
					
					<div class="form-group">
						<label  for="final_purchase_no">Purchase :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="final_purchase_no" id="final_purchase_no" type="text" placeholder="" value="<?php echo set_value('final_purchase_no',$this->_data['row']['final_purchase_no']); ?>" />	
							</div>
						
						</div>
					</div>
                    <div class="form-group">
						<label  for="final_delivery_no">Delivery NO  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="final_delivery_no" id="final_delivery_no" type="text" placeholder="" value="<?php echo set_value('final_delivery_no',$this->_data['row']['final_delivery_no']); ?>" />	
							</div>
						
						</div>
					</div>
						
				</fieldset>
				<fieldset>
					<legend>สินค้า</legend>
                <div class="form-group">
						<label  for="final_date_delivery">Delivery Date:</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-sm date-picker" size="16" type="text" value="<?php echo set_value("final_date_delivery",date("d/m/Y",strtotime($this->_data['row']['final_date_delivery']))); ?>"  name="final_date_delivery" id="final_date_delivery" />
                               
							</div>
						
						</div>
					</div>
                    <div class="form-group">
						<label  for="final_material_no">Material NO  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="final_material_no" id="final_material_no" type="text" placeholder="" value="<?php echo set_value('final_material_no',$this->_data['row']['final_material_no']); ?>" />	
							</div>
						
						</div>
					</div>
                    <div class="form-group">
						<label  for="final_certificate_no">Certificate NO  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="final_certificate_no" id="final_certificate_no" type="text" placeholder="" value="<?php echo set_value('final_certificate_no',$this->_data['row']['final_certificate_no']); ?>" />	
							</div>
						
						</div>
					</div>
                    <div class="form-group">
						<label  for="final_quantity">Quantity :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="final_quantity" id="final_quantity" type="text" placeholder="" value="<?php echo set_value('final_quantity',$this->_data['row']['final_quantity']); ?>" />
							</div>
						
						</div>
					</div>			
						
				</fieldset>
				
				<div class="form-actions">
               		<input type="hidden" name="product_type" id="product_type" value="<?php echo $type; ?>" />
                    <input type="hidden" name="product_id" id="product_id" value="<?php echo $product_id; ?>" />
                    <input type="hidden" name="final_id" id="product_id" value="<?php echo $this->_data['row']['final_id']; ?>" />
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-mini" href="<?php echo admin_url($this->router->fetch_class() . "/final_list/".$type."/".$product_id); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			 </form>
		</div>
	</div>
</div>
