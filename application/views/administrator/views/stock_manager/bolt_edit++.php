<div class="row-fluid">
<div class="span12">
	<div class="widget">
		<div class="widget-title">
			<h4><i class="icon-plus"></i> เพิ่มสต๊อกใหม่</h4>
			<span class="tools">
			<a href="javascript:;" class="icon-chevron-down"></a>
			<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>		
			</span>							
		</div>
		<div class="widget-body form">
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
			<form method="post" class="form-vertical">
			<div class="row-fluid">
				<div class="span6">
					<div class="form-group">
						<label  for="username">LOT NO * :</label>
						<div class="controls">
							<div class="input-prepend">
							   <span class="add-on"><i class="icon-code"></i></span>
							   <input class="form-control input-md" name="lot_no" id="lot_no" type="text" placeholder="" value="<?php echo set_value('lot_no',$this->_data['row']['lot_no']); ?>" />	
							</div>
						
						</div>
					</div>
				</div>
				<div class="span6">
					<div class="form-group">
						<label  >BOLT * :</label>
						<div class="controls">
							<select name="product_id" id="product_id" class="span12 chosen" data-placeholder="เลือก BOLT">
							   <?php foreach($this->bolt_stock->getProduct() as $rs){ ?>
						       <option value="<?php echo $rs['product_id']; ?>" <?php if(set_value("product_id",$this->_data['row']['product_id'])==$rs['product_id']){ ?> selected="selected" <?php } ?>><?php echo $rs['product_name']; ?></option>
						       <?php } ?>
							</select>
						</div>
					</div>
				</div>
			</div>
			<p></p>
			<div class="row-fluid">
				<div class="span6">
					<div class="form-group">
						<label  for="username">CERTIFICATION NO * :</label>
						<div class="controls">
							<div class="input-prepend">
							   <span class="add-on"><i class="icon-credit-card"></i></span>
							   <input class="form-control input-md" name="lot_certificate_no" id="lot_certificate_no" type="text" placeholder="" value="<?php echo set_value('lot_certificate_no',$this->_data['row']['lot_certificate_no']); ?>" />	
							</div>
						
						</div>
					</div>
				</div>
				<div class="span6">
					<div class="form-group">
						<label  for="username">SURFACE * :</label>
						<div class="controls">
							<div class="input-prepend">
							   <span class="add-on"><i class="icon-code"></i></span>
							   <input class="form-control input-md" name="lot_surface" id="lot_surface" type="text" placeholder="" value="<?php echo set_value('lot_surface',$this->_data['row']['lot_surface']); ?>" />	
							</div>
						
						</div>
					</div>
				</div>
			</div>
			<p></p>
			<div class="row-fluid">
				<div class="span6">
					<div class="form-group">
						<label  for="username">จำนวนสินค้า. * :</label>
						<div class="controls">
							<div class="input-prepend">
							   <span class="add-on"><i class="icon-code"></i></span>
							   <input class="form-control input-md" name="lot_amount" id="lot_amount" type="text" placeholder="" value="<?php echo set_value('lot_amount',$this->_data['row']['lot_amount']); ?>" />	
							</div>
						
						</div>
					</div>
				</div>
				<div class="span6">
					<div class="form-group">
						<label  >SUPPLIER * :</label>
						<div class="controls">
							<select name="supplier_id" id="supplier_id" class="span12 chosen" data-placeholder="เลือก SUPPLIER">
								<option value=""></option>
							   <?php foreach($this->bolt_stock->getSupplier() as $rs){ ?>
						       <option value="<?php echo $rs['supplier_id']; ?>" <?php if(set_value("supplier_id",$this->_data['row']['supplier_id'])==$rs['supplier_id']){ ?> selected="selected" <?php } ?>><?php echo $rs['supplier_name']; ?></option>
						       <?php } ?>
							</select>
						</div>
					</div>
				</div>
			</div>
			<p></p>
			<div class="row-fluid">
				<div class="span6">
					<div class="form-group">
						<label  >SIZE M*P*LENG * :</label>
						<div class="controls">
							<select name="size_id" id="size_id" class="span12 chosen" data-placeholder="เลือก SIZE M*P*LENG">
								<option value=""></option>
							   <?php foreach($this->bolt_stock->getSize() as $rs){ ?>
						       <option value="<?php echo $rs['size_id']; ?>" <?php if(set_value("size_id",$this->_data['row']['size_id'])==$rs['size_id']){ ?> selected="selected" <?php } ?>><?php echo $rs['size_m']."*".$rs['size_p']."*".$rs['size_length']; ?></option>
						       <?php } ?>
							</select>
						</div>
					</div>
				</div>
				<div class="span6">
					<div class="form-group">
						<label  for="username">COMMODITY * :</label>
						<div class="controls">
							<div class="input-prepend">
							   <span class="add-on"><i class="icon-code"></i></span>
							   <input class="form-control input-md" name="lot_commodity" id="lot_commodity" type="text" placeholder="" value="<?php echo set_value('lot_commodity',$this->_data['row']['lot_commodity']); ?>" />	
							</div>
						
						</div>
					</div>
				</div>
			</div>
			<p></p>
			<div class="row-fluid">
				<div class="span6">
					<div class="form-group">
						<label  for="username">MATERIAL * :</label>
						<div class="controls">
							<div class="input-prepend">
							   <span class="add-on"><i class="icon-code"></i></span>
							   <input class="form-control input-md" name="lot_material" id="lot_material" type="text" placeholder="" value="<?php echo set_value('lot_material',$this->_data['row']['lot_material']); ?>" />	
							</div>
						
						</div>
					</div>
				</div>
				<div class="span6">
					<div class="form-group">
						<label  for="username">HEAT No * :</label>
						<div class="controls">
							<div class="input-prepend">
							   <span class="add-on"><i class="icon-code"></i></span>
							   <input class="form-control input-md" name="lot_heat_no" id="lot_heat_no" type="text" placeholder="" value="<?php echo set_value('lot_heat_no',$this->_data['row']['lot_heat_no']); ?>" />	
							</div>
						
						</div>
					</div>
				</div>
			</div>
			<p></p>
			<div class="row-fluid">
				<div class="span4">
					<div class="form-group">
						<label  >COLOR * :</label>
						<div class="controls">
							<select name="lot_color" id="lot_color" class="span12" data-placeholder="เลือก COLOR">
							   <option value="BLACK">BLACK</option>
							   <option value="HDG">HDG</option>
							   <option value="ZING">ZING</option>
							</select>
						</div>
					</div>
				</div>
				<div class="span8">
					
						
						
							<div class="input-prepend">
							   <span class="add-on"><i class="icon-comment"></i></span>
							   <input class="input-xxlarge" name="lot_color_value" id="lot_color_value" type="text" placeholder=""  value="<?php echo set_value('lot_color_value',$this->_data['row']['lot_color_value']); ?>" />	
							</div>
						
						
					
				</div>
				
			</div>
			<p></p>
			<div class="row-fluid">
				<div class="form-group">
					<label  for="username">หมายเหตุ * :</label>
					<div class="controls">
						   <textarea  class="span12" name="lot_remark" id="lot_remark" placeholder="" /><?php echo set_value('lot_remark',$this->_data['row']['lot_remark']); ?></textarea>	
					</div>
				</div>
			</div>
			<div class="form-actions">
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-mini" href="<?php echo admin_url($this->router->fetch_class() . "/bolt_list"); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			 </form>
		</div>
	</div>
</div>
</div>