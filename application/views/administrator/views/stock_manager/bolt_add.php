<div class="row-fluid">
<div class="span12">
	<div class="widget">
		<div class="widget-title">
			<h4><i class="icon-plus"></i> เพิ่มสต๊อกใหม่</h4>
			<span class="tools">
			<a href="javascript:;" class="icon-chevron-down"></a>
			<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>		
			</span>							
		</div>
		<div class="widget-body form">
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด ! </strong>','</div>'); ?>
			<form method="post" class="form-vertical">

					<div class="form-group">
						<label  for="username">LOT NO * :</label>
						<div class="controls">
							<div class="input-prepend">
							   <span class="add-on"><i class="icon-code"></i></span>
							   <input class="form-control input-md" name="lot_no" id="lot_no" type="text" placeholder="" value="<?php echo set_value('lot_no'); ?>" />	
							</div>
						
						</div>
					</div>
					
					<div class="form-group">
						<label  >Incoming ID * :</label>
						<div class="controls">
							<select name="income_id" id="income_id" class="span6 chosen" data-placeholder="เลือก Incomeing ID">
								<option value=""></option>
								<?php
								if($incomelist){
									foreach($incomelist as $ilist){
										?>
										<option value="<?php echo $ilist['income_id']; ?>"><?php echo $ilist['income_job_id']; ?></option>
										<?php
									}
								}
								?>
							</select>
						</div>
					</div>
					
					<div class="form-group" id="supplier">
						<label  >Supplier :</label>
						<p style="position:relative; top:5px; left:20px;"></p>
						<input type="hidden" name="supplier_id" id="supplier_id" value="" />
					</div>


					<div class="form-group" id="bolt">
						<label  >BOLT * :</label>
						<p style="position:relative; top:5px; left:20px;"></p>
						<input type="hidden" name="product_id" id="product_id" value="" />
					</div>
					
								
					<div class="form-group" id="size">
						<label  >SIZE M*P*LENGTH * :</label>
						<p style="position:relative; top:5px; left:20px;"></p>
					</div>
	
					<div class="form-group" id="certificate">
						<label  for="username">CERTIFICATION NO * :</label>
						<p style="position:relative; top:5px; left:20px;"></p>
					</div>
	
					<div class="form-group">
						<label  for="username">จำนวนสินค้า. * :</label>
						<div class="controls">
							<div class="input-prepend">
							   <span class="add-on"><i class="icon-code"></i></span>
							   <input class="form-control input-md" name="lot_amount" id="lot_amount" type="text" placeholder="" value="<?php echo set_value('lot_amount'); ?>" maxlength="10" />	
							</div>
						
						</div>
					</div>
				
					<div class="form-group">
						<label  for="username">วันนำเข้า * :</label>
						<div class="controls">
							<div class="input-append date date-picker" data-date="<?php echo set_value('lot_import_date'); ?>" data-date-format="dd/mm/yyyy">
							   		 <input class="form-control input-sm date-picker" name="lot_import_date" id="lot_import_date" type="text" placeholder="" value="<?php echo set_value('lot_import_date'); ?>" />	
                                     <span class="add-on"><i class="icon-calendar"></i></span>
							</div>
						</div>
					</div>
							
						
			
				<div class="form-group">
					<label  for="username">หมายเหตุ * :</label>
					<div class="controls">
						   <textarea  class="span12" name="lot_remark" id="lot_remark" placeholder="" cols="5" rows="5"  /><?php echo set_value('lot_remark'); ?></textarea>	
					</div>
				</div>
			</div>
			<div class="form-actions">
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-mini" href="<?php echo admin_url($this->router->fetch_class() . "/bolt_list"); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			 </form>
		</div>
	</div>
</div>
</div>
<script type="text/javascript">
function get_size()
{
	var product_id = $("#product_id").val();
	var curr_val = <?php echo intval(set_value("size_id")); ?>;
	$("#size_id option[value !='']").remove();
	$.post(admin_url+"stock_manager/bolt_getsize/"+product_id,{token : "<?php echo md5(time()); ?>" },function(json_response){
		for(x in json_response.data.res){
			var opt = $('<option />').val(json_response.data.res[x]['value']).text(json_response.data.res[x]['label']);
			if(curr_val==json_response.data.res[x]['value']){
				opt.attr("selected","selected");
			}
			$("#size_id").append(opt);
		}
		$("#size_id").trigger("chosen:updated");
		$("#size_id").val(curr_val).trigger("liszt:updated")
	},"json");
}
$(document).ready(function(){
	 $("#product_id").val(<?php echo intval(set_value("product_id")); ?>).change(get_size);
	 get_size(<?php echo intval(set_value("product_id")); ?>);
	 
	 $('#income_id').change(function(){
		 $.post('<?php echo site_url('stock_manager/getstockinfo'); ?>', { income_id : $(this).val() }, function(data){
		 	for(var x in data){
			 	$('#supplier').find('p').html(data[x].supplier);
			 	$('#supplier_id').val(data[x].supplier_id);
			 	$('#bolt').find('p').html(data[x].bolt);
			 	$('#product_id').val(data[x].product_id);
			 	$('#size').find('p').html(data[x].size);
			 	$('#certificate').find('p').html(data[x].certificate);
		 	}
			 
		 });
	 });
});
</script>