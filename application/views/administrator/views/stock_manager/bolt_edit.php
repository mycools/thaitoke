<div class="col-sm-12">
	<!-- BEGIN SAMPLE TABLE PORTLET-->
	<div class="panel panel-white" id="panel4">
		<div class="panel-heading">
			<h4 class="panel-title text-primary"><i class="icon-plus"></i> แก้ไขสต๊อก</h4>
			<div class="panel-tools">
				
				<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>	
			</div>
		</div>
		<div class="panel-body">


			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด ! </strong>','</div>'); ?>
			<form method="post" class="form-vertical">
				<input name="size_id" id="size_id" type="hidden" value="<?php echo set_value('size_id',$size_id); ?>" />
				<input name="qty_old" id="qty_old" type="hidden" value="<?php echo set_value('qty_old',$row['size_qty']); ?>" />
				<input name="product_id" id="product_id" type="hidden" value="<?php echo set_value('product_id',$row['product_id']); ?>" />

					<div class="form-group">
						<label  for="username">จำนวน * :</label>
						<div class="controls">
							<div class="input-prepend">
							   <span class="add-on"><i class="icon-code"></i></span>
							   <input class="form-control" name="qty" id="qty" type="text" placeholder="" value="" />	
							</div>
						
						</div>
					</div>

							
						
			
				<div class="form-group">
					<label  for="username">หมายเหตุ * :</label>
					<div class="controls">
						   <textarea  class="form-control" name="remark" id="remark" placeholder="" cols="5" rows="5"  /></textarea>	
					</div>
				</div>
			
			<div class="form-actions">
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-warning" href="<?php echo admin_url($this->router->fetch_class() . "/bolt_size_list/".$row['product_id']); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			 </form>
		</div>
	</div>
</div>
