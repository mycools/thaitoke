<div class="col-sm-12">
	<!-- BEGIN SAMPLE TABLE PORTLET-->
	<div class="panel panel-white" id="panel4">
		<div class="panel-heading">
			<h4 class="panel-title text-primary"><i class="icon-list-alt"></i> <?php echo $_menu_name; ?></h4>
			<div class="panel-tools">
				
				<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>	
			</div>
		</div>
		<div class="panel-body">
    	<form method="post" name="menu_form" id="menu_form" enctype="multipart/form-data">
    	<input type="hidden" name="customer_id" value="<?php echo $row['customer_id']?>" />
			  <?php if(@$success_message!=NULL){ ?>
		      <div class="alert alert-success"> 
		        <button class="close" data-dismiss="alert">×</button>
		        <strong>Success !</strong> <?php echo $success_message; ?>
		      </div>
		      <?php } ?>
		     
		      	<?php echo @$validation_errors; ?>
			    <?php if(@$error_message!=NULL){ ?>
			    	<div class="alert alert-error">
			        	<button class="close" data-dismiss="alert">×</button>
			            <strong>Error !</strong> <?php echo $error_message; ?>
			        </div>
			    <?php }?>
			 
			 <div class="form-group">
			 <label  for="product_name">ชื่อ * :</label>
			 <div class="controls">
			    <div class="input-group">
			       <span class="input-group-addon"><i class="icon-font"></i></span>
			 <input class="form-control input-md" name="customer_name" id="customer_name" type="text" placeholder="กรุณาระบุชื่อ" value="<?php echo $row['customer_name']; ?>" />	
			        
			    </div>
			    
			 </div>
			</div>

			<div class="form-group">
			 <label  for="product_name">ที่อยู่ * :</label>
			 <div class="controls">
			<textarea class="form-control" name="customer_address" id="customer_address" rows="5" ><?php echo $row['customer_address']; ?></textarea>
			 </div>
			</div>
           
            <div class="form-group">
                <label >จังหวัด</label>
                <div class="controls">
                    <select  name="customer_province" class="js-example-basic-single js-states form-control" tabindex="-1" >
                        <option value=""></option>
                        <?PHP
                            foreach( $province->result_array() AS $pro ){
                            ?>
                                <option value="<?php echo $pro['province_id']?>" <?php if($row['customer_province']==$pro['province_id']){ ?>selected="selected" <?php } ?> ><?php echo $pro['province_name_th'];?></option>
                            <?php
                            }
                        ?>
                    </select>   
                 </div>
            </div>
            
            <div class="form-group">
			 <label  for="product_name">อีเมล * :</label>
			 <div class="controls">
			    <div class="input-group">
			       <span class="input-group-addon">@</span>
			 <input class="form-control input-md" name="customer_email" id="customer_email" type="text" placeholder="กรุณาระบุอีเมล" value="<?php echo $row['customer_email']; ?>" />	
			        
			    </div>
			    
			 </div>
			</div>
            
            <div class="form-group">
			 <label  for="product_name">เบอร์โทร * :</label>
			 <div class="controls">
			    <div class="input-group">
			       <span class="input-group-addon"><i class="icon-phone"></i></span>
			 <input class="form-control input-md" name="customer_tel" id="customer_tel" type="text" placeholder="กรุณาระบุเบอร์โทร" value="<?php echo $row['customer_tel']; ?>" />	
			        
			    </div>
			    
			 </div>
			</div>
			
			<div class="form-group">
			 <label  for="product_name">เบอร์แฟกซ์ * :</label>
			 <div class="controls">
			    <div class="input-group">
			       <span class="input-group-addon"><i class="icon-fax"></i></span>
			 <input class="form-control input-md" name="customer_fax" id="customer_fax" type="text" placeholder="กรุณาระบุเบอร์แฟกซ์" value="<?php echo $row['customer_fax']; ?>" />	
			        
			    </div>
			    
			 </div>
			</div>
			
            <div class="form-actions">
             <button type="submit" class="btn btn-primary">Save</button>
             <button type="button" onclick="$.getLocation('<?php echo admin_url('company/customer_list');?>')" class="btn">Cancel</button>
          </div>
    	</form>
    	
    </div>
  </div>
  <!-- END RECENT ORDERS PORTLET--> 
</div> 
<script  type="text/javascript">
function change_url()
{
	
	if($('#menu_url_system').val()==""){
		$('#menu_url').show();
	}else{
		$('#menu_url').hide();
		//if($('#menu_url_system').val()!="-"){
			$('#menu_url').val($('#menu_url_system').val())
		//}
	}
}
$(document).ready(function(){ change_url(); });
function delete_data(cms_id)
{
	if(confirm("Delete Data !. Are you sure ?")){
	$("#menu_form").attr("action",admin_url+"site_menu/delete_menu/"+cms_id+"/");
	$("#menu_form").submit();
	}
}
</script>