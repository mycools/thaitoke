<div class="col-sm-12">
	<!-- BEGIN SAMPLE TABLE PORTLET-->
	<div class="panel panel-white" id="panel4">
		<div class="panel-heading">
			<h4 class="panel-title text-primary"><i class="icon-plus"></i> เพิ่มข้อมูล Washer</h4>
			<div class="panel-tools">
				
				<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>	
			</div>
		</div>
		<div class="panel-body">

			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
				
			<form method="post"   enctype="multipart/form-data">
				<div class="row">
					<div class="col-md-6">

				
				<div class="form-group">
					<label  for="product_name">ชื่อเกรด : *</label>
					<div class="controls">
						<input type="text" name="product_name" id="product_name" value="<?php echo set_value('product_name'); ?>" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label  for="product_grade">ชื่อ Standard : </label>
					<div class="controls">
						<input type="text" name="product_standard" id="product_standard" value="<?php echo set_value('product_standard'); ?>" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label  for="product_initial">ชื่อย่อ : </label>
					<div class="controls">
						<input type="text" name="product_initial" id="product_initial" value="<?php echo set_value('product_initial'); ?>" class="form-control" />
					</div>
				</div>
				
				<div class="form-group">
					<label >ประเภท : </label>
					<div class="controls">
						<select name="product_type" id="product_type" class="js-example-basic-single js-states form-control">
							<option value="BLACK" <?php echo set_select('product_type','BLACK', true); ?>>BLACK </option>
							<option value="Hot Dip Galvanized Coating" <?php echo set_select('product_type','Hot Dip Galvanized Coating'); ?>>Hot Dip Galvanized Coating</option>
							<option value="ZINC" <?php echo set_select('product_type','ZINC'); ?>>ZINC</option>
						</select>
					</div>
				</div>
                 <div class="control-group">
					<input type="hidden" name="certificate_id" id="certificate_id" value="4" />
				</div>
				<div class="form-group">
					<label  for="product_initial">Part Name : </label>
					<div class="controls">
						<input type="text" name="product_part_name" id="product_part_name" value="<?php echo set_value('product_part_name'); ?>" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label  for="product_initial">ISO : </label>
					<div class="controls">
						<input type="text" name="product_iso" id="product_iso" value="<?php echo set_value('product_iso'); ?>" class="form-control" />
					</div>
				</div>
				<div class="form-actions">
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-mini btn-warning" href="<?php echo admin_url("manageproduct/washer_list"); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
					</div>
					<div class="col-md-6">
					<div class="form-group">
					<label>
						รูปภาพ
					</label>
					<div class="fileinput fileinput-new" data-provides="fileinput">
						<div class="fileinput-new thumbnail">
							<img src="<?php echo site_url("src/200x200/default.png"); ?>" alt="" />
						</div>
						<div class="fileinput-preview fileinput-exists thumbnail"></div>
						<div class="user-edit-image-buttons">
							<span class="btn btn-azure btn-file"><span class="fileinput-new"><i class="fa fa-picture"></i> เลือกรูปภาพ </span><span class="fileinput-exists"><i class="fa fa-picture"></i> เปลี่ยน</span>
								<input type="file" name="product_image" id="product_image">
							</span>
							<a href="#" class="btn fileinput-exists btn-red" data-dismiss="fileinput">
								<i class="fa fa-times"></i> ยกเลิก
							</a>
						</div>
					</div>
				</div>
					</div>
				</div>
			 </form>
		</div>
	</div>
</div>
<style type="text/css">
#menu_icon_chzn{
	font-family: 'FontAwesome',Tahoma;
}
</style>