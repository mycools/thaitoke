<div class="col-sm-12">
	<!-- BEGIN SAMPLE TABLE PORTLET-->
	<div class="panel panel-white" id="panel4">
		<div class="panel-heading">
			<h4 class="panel-title text-primary"><i class="icon-plus"></i> เพิ่ม Washer size</h4>
			<div class="panel-tools">
				
				<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>	
			</div>
		</div>
		<div class="panel-body">

			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
				
			<form method="post" role="form" class="form-vertical">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-sm-3 control-label" for="inputEmail3">
								M * :
							</label>
							<div class="col-sm-9">
								<input type="text" name="size_m" id="size_m" value="<?php echo set_value('size_m'); ?>" class="form-control input-sm" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="inputEmail3">
								P :
							</label>
							<div class="col-sm-9">
								<input type="text" name="size_p" id="size_p" value="<?php echo set_value('size_p'); ?>" class="form-control input-sm" />
							</div>
						</div>
					</div>
					<div class="col-md-6"></div>
				</div>
				<div class="form-group">
					<input type="hidden" name="size_length" id="size_length" value="0" />
				</div>
				<div class="form-group">
					
					
						<table class="table table-striped table-bordered table-advance table-hover"> 
							<tr>
								<th style="width:5%;">No</th>
								<th style="width:30%;">Description</th>
								<th style="width:35%;">Standard value</th>
								<th style="width:30%;">Inspection tool</th>
							</tr>
					<?php
					if($row){
						foreach($cer_list as $index=>$r){
							?>	
							<tr>
								<th style="width:5%;"><?php echo $index+1;?></th>
								<th>
									<input readonly type="text" name="size_description[<?php echo $index; ?>]" value="<?php echo set_value('size_description['.$index.']', $r['size_description']); ?>" class="form-control" />
								</th>
								<th>
									<div class="row margin-top-15">
										<div class="col-sm-2">
											MIN
										</div>
										<div class="col-sm-5">
											<input type="text" name="ps_min[<?php echo $index; ?>]" value="<?php echo trim(set_value('ps_min['.$index.']', @$r['ps_min'])); ?>" class="numericonly form-control input-sm"  placeholder="ค่า" />
										</div>
										<div class="col-sm-5">
											<input type="text" name="ps_min_unit[]" value="<?php echo trim(set_value('ps_min_unit[]', @$r['ps_min_unit'])); ?>" class="form-control input-sm" placeholder="หน่วย" />
										</div>
									</div>
									<hr />
									<div class="row margin-bottom-15">
										<div class="col-sm-2">
											MAX
										</div>
										<div class="col-sm-5">
											<input type="text" name="ps_max[<?php echo $index; ?>]" value="<?php echo trim(set_value('ps_max['.$index.']', @$r['ps_max'])); ?>" class="numericonly form-control input-sm" placeholder="ค่า" />
										</div>
										<div class="col-sm-5">
											<input type="text" name="ps_max_unit[]" value="<?php echo trim(set_value('ps_max_unit[]', @$r['ps_max_unit'])); ?>" class="form-control input-sm" placeholder="หน่วย" />
										</div>
									</div>
								</th>
								<th style="width:30%;">
								<select name="inspection_id[]" class="js-example-basic-single js-states form-control">
										<option value="6" <?php echo set_select('inspection_id[]','6', @$r['inspection_id']==6); ?>>No inspection tools</option>
										<option value="1" <?php echo set_select('inspection_id[]','1', @$r['inspection_id']==1); ?>>Vernier</option>
										<option value="2" <?php echo set_select('inspection_id[]','2', @$r['inspection_id']==2); ?>>Ruler</option>
										<option value="3" <?php echo set_select('inspection_id[]','3', @$r['inspection_id']==3); ?>>Sight</option>
										<option value="4" <?php echo set_select('inspection_id[]','4', @$r['inspection_id']==4); ?>>Pitch gage</option>
										<option value="5" <?php echo set_select('inspection_id[]','5', @$r['inspection_id']==5); ?>>THK Tester</option>
                                         <option value="7" <?php echo set_select('inspection_id[]','7', @$r['inspection_id']==7); ?>>Mitutoyo , ARK 600</option>
                                        <option value="8" <?php echo set_select('inspection_id[]','8', @$r['inspection_id']==8); ?>>Plug Gauge</option>
									</select>
</th>
							</tr>
					<?php } }?>
						</table>					

					
				</div>
				
				<div class="form-actions">
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-warning" href="<?php echo admin_url("manageproduct/washer_size_list/".$row['product_id']); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			 </form>
		</div>
	</div>
</div>
<style type="text/css">
#menu_icon_chzn{
	font-family: 'FontAwesome',Tahoma;
}
</style>