<div class="span12">
	<div class="widget">
		<div class="widget-title">
			<h4><i class="icon-cogs"></i>เพิ่ม Taper Washer size</h4>
			<span class="tools">
			<a href="javascript:;" class="icon-chevron-down"></a>
			<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>		
			</span>							
		</div>
		<div class="widget-body form">
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
				
			<form method="post" class="form-vertical">
				<div class="form-group">
					<label  for="size_m">M * :</label>
					<div class="controls">
						<input type="text" name="size_m" id="size_m" value="<?php echo set_value('size_m', $sizeinfo['size_m']); ?>" class="form-control input-sm" />
					</div>
				</div>
				<div class="form-group">
					<label  for="size_m">P : </label>
					<div class="controls">
						<input type="text" name="size_p" id="size_p" value="<?php echo set_value('size_p', $sizeinfo['size_p']); ?>" class="form-control input-sm" />
					</div>
				</div>
				<div class="form-group">
					<input type="hidden" name="size_length" id="size_length" value="0" />
				</div>
				<div class="form-group">
					
					
						<table class="table table-striped table-bordered table-advance table-hover"> 
							<tr>
								<th style="width:5%;">No</th>
								<th style="width:30%;">Description</th>
								<th style="width:35%;">Standard value</th>
								<th style="width:30%;">Inspection tool</th>
							</tr>
					<?php
					if($row){
						foreach($cer_list as $index=>$r){
							?>	
							<tr>
								<th style="width:5%;"><?php echo $index+1;?></th>
								<th style="width:30%;"><input readonly="true" type="text" name="size_description[<?php echo $index; ?>]" value="<?php echo set_value('size_description['.$index.']', $r['ps_description']); ?>" class="form-control input-md" /></th>
								<th style="width:35%;">MIN <input type="text" name="ps_min[<?php echo $index; ?>]" value="<?php echo trim(set_value('ps_min['.$index.']', @$r['ps_min'])); ?>" class="span2 numericonly" placeholder="ค่า" /><input type="text" name="ps_min_unit[]" value="<?php echo trim(set_value('ps_min_unit[]', @$r['ps_min_unit'])); ?>" class="span2" placeholder="หน่วย" />
									MAX <input type="text" name="ps_max[<?php echo $index; ?>]" value="<?php echo trim(set_value('ps_max['.$index.']', @$r['ps_max'])); ?>" class="span2 numericonly" placeholder="ค่า" /><input type="text" name="ps_max_unit[]" value="<?php echo trim(set_value('ps_max_unit[]', @$r['ps_max_unit'])); ?>" class="span2" placeholder="หน่วย" /></th>
								<th style="width:30%;">
								<select name="inspection_id[]">
										<option value="6" <?php echo set_select('inspection_id[]','6', @$r['inspection_id']==6); ?>>No inspection tools</option>
										<option value="1" <?php echo set_select('inspection_id[]','1', @$r['inspection_id']==1); ?>>Vernier</option>
										<option value="2" <?php echo set_select('inspection_id[]','2', @$r['inspection_id']==2); ?>>Ruler</option>
										<option value="3" <?php echo set_select('inspection_id[]','3', @$r['inspection_id']==3); ?>>Sight</option>
										<option value="4" <?php echo set_select('inspection_id[]','4', @$r['inspection_id']==4); ?>>Pitch gage</option>
										<option value="5" <?php echo set_select('inspection_id[]','5', @$r['inspection_id']==5); ?>>THK Tester</option>
									</select>
</th>
							</tr>
							<input type="hidden" name="ps_id[<?php echo $index; ?>]" value="<?php echo @$r['ps_id']; ?>" />
					<?php } }?>
						</table>					

					
				</div>
				
				<div class="form-actions">
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-mini" href="<?php echo admin_url("manageproduct/taper_size_list/".$row['product_id']); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			 </form>
		</div>
	</div>
</div>
<style type="text/css">
#menu_icon_chzn{
	font-family: 'FontAwesome',Tahoma;
}
</style>