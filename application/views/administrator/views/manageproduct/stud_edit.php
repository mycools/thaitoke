<div class="span12">
	<div class="widget">
		<div class="widget-title">
			<h4><i class="icon-cogs"></i>แก้ไข Stud</h4>
			<span class="tools">
			<a href="javascript:;" class="icon-chevron-down"></a>
			<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>		
			</span>							
		</div>
		<div class="widget-body form">
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
				
			<form method="post" class="form-vertical" enctype="multipart/form-data">
				<?php
				if(trim($row['product_image'])!=''){
					?>
					<div class="form-group">
						<div class="controls">
							<img src="<?php echo site_url("src/100x100/product/stud/".$row['product_image']); ?>" alt="" />
						</div>
					</div>
					<?php
				}
				?>
				<div class="form-group">
					<label  for="product_image">รูปภาพ : </label>
					<div class="controls">
						<input type="file" name="product_image" id="product_image" />
						<p class="help-block">เว้นว่างไว้หากไม่ต้องการเปลี่ยน ขนาดไฟล์ไม่เกิน 1Mb</p>
					</div>
				</div>
				<div class="form-group">
					<label  for="product_name">ชื่อเกรด : *</label>
					<div class="controls">
						<input type="text" name="product_name" id="product_name" value="<?php echo set_value('product_name', $row['product_name']); ?>" class="form-control input-md" />
					</div>
				</div>
				<div class="form-group">
					<label  for="product_standard">ชื่อ Standard : </label>
					<div class="controls">
						<input type="text" name="product_standard" id="product_standard" value="<?php echo set_value('product_standard', $row['product_standard']); ?>" class="form-control input-md" />
					</div>
				</div>
				<div class="form-group">
					<label  for="product_initial">ชื่อย่อ : </label>
					<div class="controls">
						<input type="text" name="product_initial" id="product_initial" value="<?php echo set_value('product_initial', $row['product_initial']); ?>" class="form-control input-md" />
					</div>
				</div>
				<div class="form-group">
					<input type="hidden" name="certificate_id" id="certificate_id" value="9" />
				</div>
				<div class="form-group">
					<label >ประเภท : </label>
					<div class="controls">
						<select name="product_type" id="product_type">
							<option value="BLACK" <?php echo set_select('product_type','BLACK', $row['product_type']=='BLACK'); ?>>BLACK </option>
							<option value="HDG" <?php echo set_select('product_type','HDG', $row['product_type']=='HDG'); ?>>HDG</option>
							<option value="ZING" <?php echo set_select('product_type','ZINC', $row['product_type']=='ZINC'); ?>>ZINC</option>
						</select>
					</div>
				</div>
				
				<div class="form-actions">
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-mini" href="<?php echo admin_url("manageproduct/stud_list"); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			 </form>
		</div>
	</div>
</div>
<style type="text/css">
#menu_icon_chzn{
	font-family: 'FontAwesome',Tahoma;
}
</style>