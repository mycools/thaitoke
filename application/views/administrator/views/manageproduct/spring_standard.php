<div class="span12">
	<div class="widget">
		<div class="widget-title">
			<h4><i class="icon-cogs"></i>เพิ่มค่ามาตรฐานของ Spring Washer</h4>
			<span class="tools">
			<a href="javascript:;" class="icon-chevron-down"></a>
			<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>		
			</span>							
		</div>
		<div class="widget-body form">
			<?php echo $this->session->flashdata('message-success'); ?>
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
				
			<form method="post" class="form-vertical">
				<table class="table table-striped table-bordered table-advance table-hover"> 
					<tr>
						<th style="width:30%;">Description</th>
						<th style="width:35%;">Standard value</th>
						<th style="width:35%;">Inspection tool</th>
					</tr>
					<?php
					if($row){
						foreach($row as $index=>$r){
							?>
							<tr>
								<td><input readonly="true" type="text" name="standard_description[<?php echo $index; ?>]" value="<?php echo set_value('standard_description['.$index.']', $r['standard_description']); ?>" class="form-control input-md" /></td>
								<td>
									MIN <input type="text" name="ps_min[<?php echo $index; ?>]" value="<?php echo trim(set_value('ps_min[]', @$r['ps_min'])); ?>" class="span2 numericonly" placeholder="ค่า" /><input type="text" name="ps_min_unit[]" value="<?php echo trim(set_value('ps_min_unit[]', @$r['ps_min_unit'])); ?>" class="span2" placeholder="หน่วย" />
									MAX <input type="text" name="ps_max[<?php echo $index; ?>]" value="<?php echo trim(set_value('ps_max[]', @$r['ps_max'])); ?>" class="span2 numericonly" placeholder="ค่า" /><input type="text" name="ps_max_unit[]" value="<?php echo trim(set_value('ps_max_unit[]', @$r['ps_max_unit'])); ?>" class="span2" placeholder="หน่วย" />
								</td>
								<td>
									<select name="inspection_id[]">
										<option value="6" <?php echo set_select('inspection_id[]','6', @$r['inspection_id']==6); ?>>No inspection tools</option>
										<option value="1" <?php echo set_select('inspection_id[]','1', @$r['inspection_id']==1); ?>>Vernier</option>
										<option value="2" <?php echo set_select('inspection_id[]','2', @$r['inspection_id']==2); ?>>Ruler</option>
										<option value="3" <?php echo set_select('inspection_id[]','3', @$r['inspection_id']==3); ?>>Sight</option>
										<option value="4" <?php echo set_select('inspection_id[]','4', @$r['inspection_id']==4); ?>>Pitch gage</option>
										<option value="5" <?php echo set_select('inspection_id[]','5', @$r['inspection_id']==5); ?>>THK Tester</option>
									</select>
								</td>
								<input type="hidden" name="ps_id[<?php echo $index; ?>]" value="<?php echo @$r['ps_id']; ?>" />
							</tr>
							<?php
						}
					}
					?>
				</table>
				<p style="clear:both;">&nbsp;</p>
				
				
				<div class="form-actions">
					<input type="hidden" name="process_status" id="process_status" value="<?php echo $status; ?>" />
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-mini" href="<?php echo admin_url("manageproduct/spring_list"); ?>"><i class="icon-reply"></i> กลับไปยังรายการสินค้า Spring Washer</a>
				 	<a class="btn btn-mini" href="<?php echo admin_url("manageproduct/spring_chemical/".$this->uri->segment(3)); ?>"><i class="icon-edit"></i> ไปยังส่วน Chemical</a>
				</div>
			 </form>
		</div>
	</div>
</div>
<style type="text/css">
#menu_icon_chzn{
	font-family: 'FontAwesome',Tahoma;
}
.clone.master{
	display: none;
}
</style>

<script type="text/javascript">
$(document).ready(function(){
	$('.btnClone').click(function(){
		var html = $('.clone').html();
		html = '<tr class="clone">'+html+'</tr>';
		$('.table').append(html);
	});
	$('.btnDel').on('click',function(){
		var target = $(this).parent().parent();
		if(target.hasClass('master')===false){
			target.remove();
		}
	});
	
	$('.numericonly').on('keyup',function(){
		var valid = checkValidNumber($(this).val());
		/*
if(valid==0){
			alert('Numeric only.');
			$(this).val(0);
			$(this).focus();
			return false;
		}
*/
	});
});

function checkValidNumber(data){
	var filter = /^[1-9]\d*(?:\.\d{0,2})?$/;
	if(filter.test(data)){
		return 1;
	}else{
		return 0;
	}
}

function isNumberKey(evt)
{
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode != 46 && charCode > 31 
    && (charCode < 48 || charCode > 57))
     return false;

  return true;
}
</script>