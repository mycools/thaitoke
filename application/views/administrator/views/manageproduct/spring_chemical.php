<div class="span12">
	<div class="widget">
		<div class="widget-title">
			<h4><i class="icon-cogs"></i>เพิ่มค่า Chemical ของ Spring Washer</h4>
			<span class="tools">
			<a href="javascript:;" class="icon-chevron-down"></a>
			<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>		
			</span>							
		</div>
		<div class="widget-body form">
			<?php echo $this->session->flashdata('message-success'); ?>
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
				
			<form method="post" class="form-vertical">
				<table class="table table-striped table-bordered table-advance table-hover"> 
					<tr>
						<th style="width:30%;">Chemical composition</th>
						<th style="width:70%;">Chemical value (%)</th>
					</tr>
					<?php
					if($row){
						foreach($row as $index=>$r){
							?>
							<tr>
								<td>
									<input readonly="true" type="text" name="chemical_value[<?php echo $index; ?>]" value="<?php echo set_value('chemical_value['.$index.']', $r['chemical_value']); ?>" class="form-control input-md" />
								</td>
								<td>
									MIN <input type="text" name="ps_min[<?php echo $index; ?>]" value="<?php echo trim(set_value('ps_min[]', @$r['ps_min'])); ?>" class="span2 numericonly" placeholder="ค่า" />
									MAX <input type="text" name="ps_max[<?php echo $index; ?>]" value="<?php echo trim(set_value('ps_max[]', @$r['ps_max'])); ?>" class="span2 numericonly" placeholder="ค่า" />
								</td>
							</tr>
							<input type="hidden" name="chemical_id[<?php echo $index; ?>]" value="<?php echo @$r['chemical_id']; ?>" />
							<?php
						}
					}
					?>
					
				</table>
				<p style="clear:both;">&nbsp;</p>
				
				
				<div class="form-actions">
					<input type="hidden" name="process_status" id="process_status" value="<?php echo $status; ?>" />
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-mini" href="<?php echo admin_url("manageproduct/spring_list"); ?>"><i class="icon-reply"></i> กลับไปยังรายการสินค้า Spring Washer</a>
				</div>
			 </form>
		</div>
	</div>
</div>
<style type="text/css">
#menu_icon_chzn{
	font-family: 'FontAwesome',Tahoma;
}
</style>

<script type="text/javascript">
$(document).ready(function(){
	$('.btnClone').click(function(){
		var html = $('.clone').html();
		html = '<tr class="clone">'+html+'</tr>';
		$('.table').append(html);
	});
	$('.btnDel').on('click',function(){
		var target = $(this).parent().parent();
		if(target.hasClass('master')===false){
			target.remove();
		}
	});
});
</script>