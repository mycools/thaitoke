<div class="col-sm-12">
	<!-- BEGIN SAMPLE TABLE PORTLET-->
	<div class="panel panel-white" id="panel4">
		<div class="panel-heading">
			<h4 class="panel-title text-primary"><i class="icon-plus"></i> เพิ่มค่ามาตรฐานของ Bolt</h4>
			<div class="panel-tools">
				
				<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>	
			</div>
		</div>
		<div class="panel-body">

			<?php echo $this->session->flashdata('message-success'); ?>
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
				
			<form method="post">
				<table class="table table-striped table-bordered table-advance table-hover"> 
					<tr>
						<th style="width:30%;">Description</th>
						<th style="width:45%;">Standard value</th>
						<th style="width:25%;">Inspection tool</th>
					</tr>
					<?php
					if($row){
						foreach($row as $index=>$r){
							?>
							<tr>
								<td><input readonly type="text" name="standard_description[<?php echo $index; ?>]" value="<?php echo set_value('standard_description['.$index.']', $r['standard_description']); ?>" class="form-control input-sm" /></td>
								<td>
									<div class="row margin-top-15">
										<div class="col-sm-2">
											MIN
										</div>
										<div class="col-sm-5">
											<input type="text" name="ps_min[<?php echo $index; ?>]" value="<?php echo trim(set_value('ps_min['.$index.']', @$r['ps_min'])); ?>" class="numericonly form-control input-sm"  placeholder="ค่า" />
										</div>
										<div class="col-sm-5">
											<input type="text" name="ps_min_unit[]" value="<?php echo trim(set_value('ps_min_unit[]', @$r['ps_min_unit'])); ?>" class="form-control input-sm" placeholder="หน่วย" />
										</div>
									</div>
									<hr />
									<div class="row margin-bottom-15">
										<div class="col-sm-2">
											MAX
										</div>
										<div class="col-sm-5">
											<input type="text" name="ps_max[<?php echo $index; ?>]" value="<?php echo trim(set_value('ps_max['.$index.']', @$r['ps_max'])); ?>" class="numericonly form-control input-sm" placeholder="ค่า" />
										</div>
										<div class="col-sm-5">
											<input type="text" name="ps_max_unit[]" value="<?php echo trim(set_value('ps_max_unit[]', @$r['ps_max_unit'])); ?>" class="form-control input-sm" placeholder="หน่วย" />
										</div>
									</div>
								</td>
								<td>
									<select name="inspection_id[]" class="js-example-basic-single js-states form-control">
										<option value="6" <?php echo set_select('inspection_id[]','6', @$r['inspection_id']==6); ?>>No inspection tools</option>
										<option value="1" <?php echo set_select('inspection_id[]','1', @$r['inspection_id']==1); ?>>Vernier</option>
										<option value="2" <?php echo set_select('inspection_id[]','2', @$r['inspection_id']==2); ?>>Ruler</option>
										<option value="3" <?php echo set_select('inspection_id[]','3', @$r['inspection_id']==3); ?>>Sight</option>
										<option value="4" <?php echo set_select('inspection_id[]','4', @$r['inspection_id']==4); ?>>Pitch gage</option>
										<option value="5" <?php echo set_select('inspection_id[]','5', @$r['inspection_id']==5); ?>>THK Tester</option>
                                        <option value="7" <?php echo set_select('inspection_id[]','7', @$r['inspection_id']==7); ?>>Mitutoyo , ARK 600</option>
                                        <option value="8" <?php echo set_select('inspection_id[]','8', @$r['inspection_id']==8); ?>>Plug Gauge</option>
									</select>
								</td>
							</tr>
							<input type="hidden" name="ps_id[<?php echo $index; ?>]" value="<?php echo @$r['ps_id']; ?>" />
							<?php
						}
					}
					?>
				</table>
				<p style="clear:both;">&nbsp;</p>
				
				
				<div class="form-actions">
					<input type="hidden" name="process_status" id="process_status" value="<?php echo $status; ?>" />
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-warning" href="<?php echo admin_url("manageproduct/bolt_list"); ?>"><i class="icon-reply"></i> กลับไปยังรายการสินค้า Bolt</a>
				 	<a class="btn btn-primary" href="<?php echo admin_url("manageproduct/bolt_chemical/".$this->uri->segment(3)); ?>"><i class="icon-edit"></i> ไปยังส่วน Chemical</a>
				</div>
			 </form>
		</div>
	</div>
</div>
<style type="text/css">
#menu_icon_chzn{
	font-family: 'FontAwesome',Tahoma;
}
.clone.master{
	display: none;
}
</style>

<script type="text/javascript">
$(document).ready(function(){
	$('.btnClone').click(function(){
		var html = $('.clone').html();
		html = '<tr class="clone">'+html+'</tr>';
		$('.table').append(html);
	});
	$('.btnDel').on('click',function(){
		var target = $(this).parent().parent();
		if(target.hasClass('master')===false){
			target.remove();
		}
	});
	
	$('.numericonly').on('keyup',function(){
		var valid = checkValidNumber($(this).val());
		/*
if(valid==0){
			alert('Numeric only.');
			$(this).val(0);
			$(this).focus();
			return false;
		}
*/
	});
});

function checkValidNumber(data){
	if(data!=''){
		if(!$.isNumeric(data)){
			alert('Numeric Only');
		}
	}
}

function checkValidNumber(data){
	var filter = /^[1-9]\d*(?:\.\d{0,2})?$/;
	if(filter.test(data)){
		return 1;
	}else{
		return 0;
	}
}

function isNumberKey(evt)
{
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode != 46 && charCode > 31 
    && (charCode < 48 || charCode > 57))
     return false;

  return true;
}
</script>