<div class="span12"> 
  <!-- BEGIN RECENT ORDERS PORTLET-->
  <div class="widget">
    <div class="widget-title widget-user">
      <h4><i class="icon-list-alt"></i> Anchor product income testing [<?php echo $income_info['income_job_id']; ?>]</h4>  
      <span class="tools">
      	
      </span>
      </div>
    <div class="widget-body form">
    <?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
    	<form method="post" name="menu_form" id="menu_form" enctype="multipart/form-data" class="form-vertical">
    	<input type="hidden" name="income_id" value="<?php echo $income_id;?>" />
    	
          <?php if(@$success_message!=NULL){ ?>
      <div class="alert alert-success"> 
        <button class="close" data-dismiss="alert">×</button>
        <strong>Success !</strong> <?php echo $success_message; ?>
      </div>
      <?php } ?>

        <?php echo @$validation_errors; ?>
            <?php if(@$error_message!=NULL){ ?>
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">×</button>
                    <strong>Error !</strong> <?php echo $error_message; ?>
                </div>
            <?php }?>
			

			           
            <div class="form-group">
                <label ></label>
                <div class="controls" style="margin-left:0 !important;">
                        
                <table width="100%" border="1" class="table table-striped table-bordered">
                	<thead>
                	<tr><th colspan="14">Standard Test</th></tr>
	                <tr>
	                	<th width="10" rowspan="2">No.</th>
	                	<th rowspan="2">Description</th>
	                	<th rowspan="2">Standard</th>
	                	<?php /*?><th rowspan="2">Inspection tool</th>
	                	<th rowspan="2">Frequency of test</th>
	                	<th rowspan="2">Inspection quantity</th><?php */?>
	                	<th width="60" rowspan="2">1</th>
	                	<th width="60" rowspan="2">2</th>
	                	<th width="60" rowspan="2">3</th>
	                	<th width="60" rowspan="2">4</th>
	                	<th width="60" rowspan="2">5</th>
	                	<th colspan="2">Overall result</th>
	                	<th width="150" rowspan="2">Remark</th>
	                	
	                </tr>
                    <tr>
	                	
	                	<th width="40">Pass</th>
	                	<th width="40">No</th>
	            
	                	
	                </tr>
                	</thead>
                	<tbody>
	                <?PHP	$i = 0; 
                            foreach( $std_list->result_array() AS $row ){      
                            
                            ?>
	                <tr>
		                <td align="center"><?php echo $i+1;?><input type="hidden" name="anchor_standard_id[]" value="<?php echo $row['ps_id'];?>" /></td>
		                <td align="center"><?php echo $row['standard_description'];?></td>
		                <td align="center">
			                <?php echo $row['ps_min']. $row['ps_min_unit']."<br/> ".$row['ps_max']. $row['ps_max_unit'];?>
			                <input type="hidden" id="ps_min_<?php echo $i; ?>" value="<?php echo $row['ps_min']; ?>" />
			                <input type="hidden" id="ps_max_<?php echo $i; ?>" value="<?php echo $row['ps_max']; ?>" />
			            </td>
		                <?php /*?><td align="center"><?php $ins = $this->income_test_model->get_inspection($row['inspection_id']);  echo $ins['inspection_name']; ?></td>
		                <td align="center">Before Delivery</td>
		                <td align="center">5pcs/lot</td><?php */?>
		                <td class="center"><input class="span12" name="test1[]" id="test1[]" type="text" value="<?php echo set_value('test1[]'); ?>" onblur="checkvalue(<?php echo $i; ?>, $(this));" /></td>
		                <td class="center"><input class="span12" name="test2[]" id="test2[]" type="text" value="<?php echo set_value('test2[]'); ?>" onblur="checkvalue(<?php echo $i; ?>, $(this));" /></td>
		                <td class="center"><input class="span12" name="test3[]" id="test3[]" type="text" value="<?php echo set_value('test3[]'); ?>" onblur="checkvalue(<?php echo $i; ?>, $(this));" /></td>
		                <td class="center"><input class="span12" name="test4[]" id="test4[]" type="text" value="<?php echo set_value('test4[]'); ?>" onblur="checkvalue(<?php echo $i; ?>, $(this));" /></td>
		                <td class="center"><input class="span12" name="test5[]" id="test5[]" type="text" value="<?php echo set_value('test5[]'); ?>" onblur="checkvalue(<?php echo $i; ?>, $(this));" /></td>
		                <td class="center">
		                	<input name="testing_status[<?php echo $i; ?>]" value="pass" type="radio" style="margin-left:0 !important;" <?php echo set_radio('testing_status['.$i.']', 'pass'); ?> />
		                </td>
		                <td class="center">
		                	<input name="testing_status[<?php echo $i; ?>]" value="not_pass" type="radio" style="margin-left:0 !important;" <?php echo set_radio('testing_status['.$i.']', 'not_pass'); ?> />
		                </td>
		                <td><input class="input span12" name="remark[]" id="remark[]" type="text" value="<?php echo set_value('remark[]'); ?>" /></td>
	                </tr>
	                 <?php
                        $i++; }
                        ?>
                        
                   
	               
                   
                	</thead>
                	
                </table>                
                           
                 <p>
					<label><b>สถานะการทดสอบ : </b></label>
					<label for="test_status_pass"><input type="radio" name="test_status" id="test_status_pass" value="2" <?php echo set_radio('test_status', 2, @$incomeinfo['test_status']==2); ?> /> ผ่าน</label>
					<label for="test_status_failed"><input type="radio" name="test_status" id="test_status_failed" value="1" <?php echo set_radio('test_status', 1, @$incomeinfo['test_status']==1); ?> /> ไม่ผ่าน</label>
					</p>
                 </div>
            </div>
            

            <div class="form-actions">
                <button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> Save 