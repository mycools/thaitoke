<div class="col-sm-12">
	<!-- BEGIN SAMPLE TABLE PORTLET-->
	<div class="panel panel-white" id="panel4">
		<div class="panel-heading">
			<h4 class="panel-title text-primary"><i class="icon-plus"></i> สร้างข้อมูลใหม่</h4>
			<div class="panel-tools">
				
				<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>	
			</div>
		</div>
		<div class="panel-body">

			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
			<form method="post">
				<fieldset>
					<legend>บริษัท</legend>
					<div class="form-group">
						<label  for="final_invoice_no">Invoice NO * :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="final_invoice_no" id="final_invoice_no" type="text" placeholder="" value="<?php echo set_value('final_invoice_no'); ?>" />	
							</div>
						
						</div>
					</div>
					
					<div class="form-group">
						<label  for="final_po_no">P/O Number  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="final_po_no" id="final_po_no" type="text" placeholder="" value="<?php echo set_value('final_po_no'); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="final_delivery_no">Delivery NO  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="final_delivery_no" id="final_delivery_no" type="text" placeholder="" value="<?php echo set_value('final_delivery_no'); ?>" />	
							</div>
						
						</div>
					</div>
					
				</fieldset>
				
				<fieldset>
					<legend>ลูกค้า</legend>
					<div class="form-group">
						<label  >Customer * :</label>
						<div class="controls">
							<select name="customer_id" id="customer_id" class="js-example-basic-single js-states form-control" data-placeholder="เลือก SUPPLIER">
								<option value=""></option>
							   <?php foreach($this->bolt_final->getCustomer() as $rs){ ?>
						       <option value="<?php echo $rs['customer_id']; ?>" <?php if(set_value("customer_id")==$rs['customer_id']){ ?> selected="selected" <?php } ?>><?php echo $rs['customer_name']; ?></option>
						       <?php } ?>
							</select>
						</div>
				</div>

					<div class="form-group">
						<label  for="final_sale_order_no">Sale Order NO. :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="final_sale_order_no" id="final_sale_order_no" type="text" placeholder="" value="<?php echo set_value('final_sale_order_no'); ?>" />	
							</div>
						
						</div>
					</div>
					
					<div class="form-group">
						<label  for="final_purchase_no">Purchase :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="final_purchase_no" id="final_purchase_no" type="text" placeholder="" value="<?php echo set_value('final_purchase_no'); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="final_date_edit">เวลา :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-sm date-picker" size="16" type="text" value="<?php echo date("d/m/Y",strtotime(set_value("final_date_edit",date("Y-m-d")))); ?>"  readonly="readonly" name="final_date_edit" id="final_date_edit" />
							</div>
						
						</div>
					</div>					
				</fieldset>
				<fieldset>
					<legend>สินค้า</legend>
						<div class="form-group">
						<label  >BOLT :</label>
						<div class="controls">
							<select name="final_bolt_id" id="final_bolt_id" class="js-example-basic-single js-states form-control" data-placeholder="เลือก BOLT">
								<option value=""></option>
							   <?php foreach($this->bolt_stock->getProduct() as $rs){ ?>
							  
						       <option value="<?php echo $rs['product_id']; ?>" <?php if(set_value("final_bolt_id")==$rs['product_id']){ ?> selected="selected" <?php } ?>><?php echo $rs['product_name']." ".$rs['product_type']; ?></option>
						       <?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label  >ขนาด :</label>
						<div class="controls">
							<select name="final_bolt_size_id" id="final_bolt_size_id" class="js-example-basic-single js-states form-control" data-placeholder="เลือก Size" style="min-width:300px;">
								<option value=""></option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label  >Incoiming ID :</label>
						<div class="controls">
							<select name="final_bolt_income_id" id="final_bolt_income_id" class="js-example-basic-single js-states form-control" data-placeholder="เลือก Incoiming" style="min-width:300px;">
								<option value=""></option>
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<label  for="final_bolt_qty">Quantity :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="final_bolt_qty" id="final_bolt_qty" type="text" placeholder="" value="<?php echo set_value('final_bolt_qty'); ?>" />
							</div>
						
						</div>
					</div>
					
					<div class="form-group">
						<label  for="final_have_nut">Have Nut? :</label>
						<div class="controls">
							<div class="input-prepend">
							   <input type="checkbox" name="final_have_nut" id="final_have_nut" value="1" <?php echo set_checkbox('final_have_nut', 1); ?> />
							</div>
						
						</div>
					</div>
					
					<div id="nut" style="display:none;">
						<div class="form-group">
							<label  >NUT :</label>
							<div class="controls">
								<select name="final_nut_id" id="final_nut_id" class="form-control" data-placeholder="เลือก NUT" style="min-width:300px;">
									<option value=""></option>
								   <?php foreach($this->nut_stock->getProduct() as $rs){ ?>
								  
							       <option value="<?php echo $rs['product_id']; ?>" <?php if(set_value("final_nut_id")==$rs['product_id']){ ?> selected="selected" <?php } ?>><?php echo $rs['product_name']." ".$rs['product_type']; ?></option>
							       <?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label  >ขนาด :</label>
							<div class="controls">
								<select name="final_nut_size_id" id="final_nut_size_id" class="form-control" data-placeholder="เลือก Size" style="min-width:300px;">
									<option value=""></option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label  >Incoiming ID :</label>
							<div class="controls">
								<select name="final_nut_income_id" id="final_nut_income_id" class="form-control" data-placeholder="เลือก Incoiming" style="min-width:300px;">
									<option value=""></option>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label  for="final_nut_qty">Quantity :</label>
							<div class="controls">
								<div class="input-prepend">
								   
								   <input class="form-control input-md" name="final_nut_qty" id="final_nut_qty" type="text" placeholder="" value="<?php echo set_value('final_nut_qty'); ?>" />
								</div>
							
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<label  for="final_have_nut">Have Washer? :</label>
						<div class="controls">
							<div class="input-prepend">
							   <input type="checkbox" name="final_have_washer" id="final_have_washer" value="1" <?php echo set_checkbox('final_have_washer', 1); ?> />
							</div>
						
						</div>
					</div>
					
					<div id="washer" style="display:none;">
						<div class="form-group">
							<label  >WASHER :</label>
							<div class="controls">
								<select name="final_washer_id" id="final_washer_id" class="form-control" data-placeholder="เลือก WASHER">
									<option value=""></option>
								   <?php foreach($this->washer_stock->getProduct() as $rs){ ?>
								  
							       <option value="<?php echo $rs['product_id']; ?>" <?php if(set_value("final_washer_id")==$rs['product_id']){ ?> selected="selected" <?php } ?>><?php echo $rs['product_name']." ".$rs['product_type']; ?></option>
							       <?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label  >ขนาด :</label>
							<div class="controls">
								<select name="final_washer_size_id" id="final_washer_size_id" class="form-control" data-placeholder="เลือก Size" style="min-width:300px;">
									<option value=""></option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label  >Incoiming ID :</label>
							<div class="controls">
								<select name="final_washer_income_id" id="final_washer_income_id" class="form-control" data-placeholder="เลือก Incoiming" style="min-width:300px;">
									<option value=""></option>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label  for="final_washer_qty">Quantity :</label>
							<div class="controls">
								<div class="input-prepend">
								   
								   <input class="form-control input-md" name="final_washer_qty" id="final_washer_qty" type="text" placeholder="" value="<?php echo set_value('final_washer_qty'); ?>" />
								</div>
							
							</div>
						</div>
					</div>

				</fieldset>
				
				<div class="form-actions">
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-warning" href="<?php echo admin_url($this->router->fetch_class() . "/final_bolt_list"); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			 </form>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	
	$('#final_bolt_id').change(function(){
		var boltid = parseInt($('#final_bolt_id').val());
		$.post('<?php echo site_url('final_manager/get_size_bolt_byid'); ?>', { bolt_id : boltid }, function(data){
			$('#final_bolt_size_id').html(data);
		});
	});
	
	$('#final_bolt_size_id').change(function(){
		var boltsizeid = parseInt($('#final_bolt_size_id').val());
		var boltid = parseInt($('#final_bolt_id').val());
		
		$.post('<?php echo site_url('final_manager/get_income_bolt_byid'); ?>', { bolt_size_id : boltsizeid, bolt_id : boltid }, function(data){
			$('#final_bolt_income_id').html(data);
		});
	});
	
	$('#final_nut_id').change(function(){
		var nutid = parseInt($('#final_nut_id').val());
		$.post('<?php echo site_url('final_manager/get_size_nut_byid'); ?>', { nut_id : nutid }, function(data){
			$('#final_nut_size_id').html(data);
		});
	});
	
	$('#final_nut_size_id').change(function(){
		var nutsizeid = parseInt($('#final_nut_size_id').val());
		var nutid = parseInt($('#final_nut_id').val());
		
		$.post('<?php echo site_url('final_manager/get_income_nut_byid'); ?>', { nut_size_id : nutsizeid, nut_id : nutid }, function(data){
			console.log(data);
			$('#final_nut_income_id').html(data);
		});
	});
	
	$('#final_washer_id').change(function(){
		var washerid = parseInt($('#final_washer_id').val());
		$.post('<?php echo site_url('final_manager/get_size_washer_byid'); ?>', { washer_id : washerid }, function(data){
			$('#final_washer_size_id').html(data);
		});
	});
	
	$('#final_washer_size_id').change(function(){
		var washersizeid = parseInt($('#final_washer_size_id').val());
		var washerid = parseInt($('#final_washer_id').val());
		
		$.post('<?php echo site_url('final_manager/get_income_washer_byid'); ?>', { washer_size_id : washersizeid, washer_id : washerid }, function(data){
			$('#final_washer_income_id').html(data);
		});
	});
	
	$('#final_have_nut').on('click', function(){
		var status = $(this).prop('checked');
		if(status===true){
			$('#nut').slideDown('fast');
		}else{
			$('#nut').slideUp('fast');
		}
	});
	
	$('#final_have_washer').on('click', function(){
		var status = $(this).prop('checked');
		if(status===true){
			$('#washer').slideDown('fast');
		}else{
			$('#washer').slideUp('fast');
		}
	});
});	
</script>