<div class="span12">
	<div class="widget">
		<div class="widget-title">
			<h4><i class="icon-plus"></i> สร้างข้อมูลใหม่</h4>
			<span class="tools">
			<a href="javascript:;" class="icon-chevron-down"></a>
			<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>		
			</span>							
		</div>
		<div class="widget-body form">
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
			<form method="post" class="form-vertical">
				
				<div class="form-group">
						<label  >SUPPLIER * :</label>
						<div class="controls">
							<select name="supplier_id" id="supplier_id" class="span6 chosen" data-placeholder="เลือก SUPPLIER">
								<option value=""></option>
							   <?php foreach($this->bolt_stock->getSupplier() as $rs){ ?>
						       <option value="<?php echo $rs['supplier_id']; ?>" <?php if(set_value("supplier_id")==$rs['supplier_id']){ ?> selected="selected" <?php } ?>><?php echo $rs['supplier_name']; ?></option>
						       <?php } ?>
							</select>
						</div>
				</div>
				
				<div class="form-group">
						<label  >BOLT * :</label>
						<div class="controls">
							<select name="product_id" id="product_id" class="span6 chosen" data-placeholder="เลือก BOLT">
								<option value=""></option>
							   <?php foreach($this->bolt_stock->getProduct() as $rs){ ?>
						       <option value="<?php echo $rs['product_id']; ?>" <?php if(set_value("product_id")==$rs['product_id']){ ?> selected="selected" <?php } ?>><?php echo $rs['product_name']; ?></option>
						       <?php } ?>
							</select>
						</div>
					</div>

				
				<div class="form-actions">
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-mini" href="<?php echo admin_url($this->router->fetch_class() . "/bolt_list"); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			 </form>
		</div>
	</div>
</div>
<script type="text/javascript">
var income_bolt_include = <?php echo (set_value('income_bolt_include','no')=="yes")?"true":"false"; ?>;
var income_nut_include = <?php echo (set_value('income_nut_include','no')=="yes")?"true":"false"; ?>;
var income_washer_include = <?php echo (set_value('income_washer_include','no')=="yes")?"true":"false"; ?>;
var income_bolt_lot_id = <?php echo (set_value('income_bolt_lot_id','0')>0)?"true":"false"; ?>;
var income_nut_lot_id = <?php echo (set_value('income_nut_lot_id','0')>0)?"true":"false"; ?>;
var income_washer_lot_id = <?php echo (set_value('income_washer_lot_id','0')>0)?"true":"false"; ?>;
$(document).ready(function(){
	$(".group_nut").attr("checked",income_nut_include);
	$(".group_nut").attr("disabled",!income_nut_include);
	
	$(".group_washer").attr("checked",income_washer_include);
	$(".group_washer").attr("disabled",!income_washer_include);
	
	$("#income_bolt_qty").attr("disabled",!income_bolt_lot_id);
	$("#income_nut_qty").attr("disabled",!income_nut_lot_id);
	$("#income_washer_qty").attr("disabled",!income_washer_lot_id);
	
	$("#income_nut_include").val("yes").change(income_nut_include_change);
	$("#income_washer_include").val("yes").change(income_washer_include_change);
	
	$("#income_nut_include").val("yes").change(function(){
		if($(this).is(":checked")){
			$(".group_nut").attr("disabled",false);
		}else{
			$(".group_nut").attr("disabled",true);
		}
	});
	$("#income_washer_include").val("yes").change(function(){
		if($(this).is(":checked")){
			$(".group_washer").attr("disabled",false);
		}else{
			$(".group_washer").attr("disabled",true);
		}
	});
	$("#income_bolt_lot_id").change(function(){
		var remain = $("#income_bolt_lot_id option:selected").attr("data-remain");
		$("#income_bolt_qty").val("0");
		$("#income_bolt_qty").attr("data-maxqty",remain);
		if($(this).val() < 1){
			$("#income_bolt_qty").attr("disabled",true);
		}else{
			$("#income_bolt_qty").attr("disabled",false);
		}
	});
	
	$("#income_bolt_qty").change(function(){
		var remain = parseInt($(this).attr("data-maxqty"));
		var val = parseInt($(this).val());
		if(val > remain){
			$("#income_bolt_qty").val("0");
			alert("จำนวน BOLT ไม่เพียงพอ คงเหลือ "+remain+" หน่วย.");
		}
	});
	
	$("#income_nut_lot_id").change(function(){
		var remain = $("#income_nut_lot_id option:selected").attr("data-remain");
		$("#income_nut_qty").val("0");
		$("#income_nut_qty").attr("data-maxqty",remain);
		if($(this).val() < 1){
			$("#income_nut_qty").attr("disabled",true);
		}else{
			$("#income_nut_qty").attr("disabled",false);
		}
	});
	
	$("#income_nut_qty").change(function(){
		var remain = parseInt($(this).attr("data-maxqty"));
		var val = parseInt($(this).val());
		if(val > remain){
			$("#income_nut_qty").val("0");
			alert("จำนวน NUT ไม่เพียงพอ คงเหลือ "+remain+" หน่วย.");
		}
	});
	
	$("#income_washer_lot_id").change(function(){
		var remain = $("#income_washer_lot_id option:selected").attr("data-remain");
		$("#income_washer_qty").val("0");
		$("#income_washer_qty").attr("data-maxqty",remain);
		if($(this).val() < 1){
			$("#income_washer_qty").attr("disabled",true);
		}else{
			$("#income_washer_qty").attr("disabled",false);
		}
	});
	
	$("#income_washer_qty").change(function(){
		var remain = parseInt($(this).attr("data-maxqty"));
		var val = parseInt($(this).val());
		if(val > remain){
			$("#income_washer_qty").val("0");
			alert("จำนวน WASHER ไม่เพียงพอ คงเหลือ "+remain+" หน่วย.");
		}
	});
	
});
function income_nut_include_change(){
	if($("#income_nut_include").is(":checked")){
		$(".group_nut").attr("disabled",false);
	}else{
		$(".group_nut").attr("disabled",true);
	}
}
function income_washer_include_change(){
	if($("#income_washer_include").is(":checked")){
		$(".group_washer").attr("disabled",false);
	}else{
		$(".group_washer").attr("disabled",true);
	}
}
</script>