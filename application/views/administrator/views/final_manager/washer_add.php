<div class="col-sm-12">
	<!-- BEGIN SAMPLE TABLE PORTLET-->
	<div class="panel panel-white" id="panel4">
		<div class="panel-heading">
			<h4 class="panel-title text-primary"><i class="icon-plus"></i> สร้างข้อมูลใหม่</h4>
			<div class="panel-tools">
				
				<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>	
			</div>
		</div>
		<div class="panel-body">
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
			<form method="post" class="form-vertical">
				<fieldset>
					<legend>บริษัท</legend>
					<div class="form-group">
						<label  for="final_invoice_no">Invoice NO * :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="final_invoice_no" id="final_invoice_no" type="text" placeholder="" value="<?php echo set_value('final_invoice_no'); ?>" />	
							</div>
						
						</div>
					</div>
					
					<div class="form-group">
						<label  for="final_po_no">P/O Number  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="final_po_no" id="final_po_no" type="text" placeholder="" value="<?php echo set_value('final_po_no'); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="final_delivery_no">Delivery NO  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="final_delivery_no" id="final_delivery_no" type="text" placeholder="" value="<?php echo set_value('final_delivery_no'); ?>" />	
							</div>
						
						</div>
					</div>
					
				</fieldset>
				
				<fieldset>
					<legend>ลูกค้า</legend>
					<div class="form-group">
						<label  >Customer * :</label>
						<div class="controls">
							<select name="customer_id" id="customer_id" class="span4 chosen" data-placeholder="เลือก SUPPLIER">
								<option value=""></option>
							   <?php foreach($this->washer_final->getCustomer() as $rs){ ?>
						       <option value="<?php echo $rs['customer_id']; ?>" <?php if(set_value("customer_id")==$rs['customer_id']){ ?> selected="selected" <?php } ?>><?php echo $rs['customer_name']; ?></option>
						       <?php } ?>
							</select>
						</div>
				</div>

					<div class="form-group">
						<label  for="final_sale_order_no">Sale Order NO. :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="final_sale_order_no" id="final_sale_order_no" type="text" placeholder="" value="<?php echo set_value('final_sale_order_no'); ?>" />	
							</div>
						
						</div>
					</div>
					
					<div class="form-group">
						<label  for="final_purchase_no">Purchase :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="final_purchase_no" id="final_purchase_no" type="text" placeholder="" value="<?php echo set_value('final_purchase_no'); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="final_date_edit">เวลา :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-sm date-picker" size="16" type="text" value="<?php echo date("d/m/Y",strtotime(set_value("final_date_edit",date("Y-m-d")))); ?>"  readonly="readonly" name="final_date_edit" id="final_date_edit" />
							</div>
						
						</div>
					</div>					
				</fieldset>
				<fieldset>
					<legend>สินค้า</legend>
											
					<div id="washer">
						<div class="form-group">
							<label  >washer :</label>
							<div class="controls">
								<select name="final_washer_id" id="final_washer_id" class="span4" data-placeholder="เลือก washer">
									<option value=""></option>
								   <?php foreach($this->washer_stock->getProduct() as $rs){ ?>
								  
							       <option value="<?php echo $rs['product_id']; ?>" <?php if(set_value("final_washer_id")==$rs['product_id']){ ?> selected="selected" <?php } ?>><?php echo $rs['product_name']." ".$rs['product_type']; ?></option>
							       <?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label  >ขนาด :</label>
							<div class="controls">
								<select name="final_washer_size_id" id="final_washer_size_id" class="span4" data-placeholder="เลือก Size" style="min-width:300px;">
									<option value=""></option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label  >Incoiming ID :</label>
							<div class="controls">
								<select name="final_washer_income_id" id="final_washer_income_id" class="span4" data-placeholder="เลือก Incoiming" style="min-width:300px;">
									<option value=""></option>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label  for="final_washer_qty">Quantity :</label>
							<div class="controls">
								<div class="input-prepend">
								   
								   <input class="form-control input-md" name="final_washer_qty" id="final_washer_qty" type="text" placeholder="" value="<?php echo set_value('final_washer_qty'); ?>" />
								</div>
							
							</div>
						</div>
					</div>
					
											
						
					</div>

				</fieldset>
				
				<div class="form-actions">
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-warning" href="<?php echo admin_url($this->router->fetch_class() . "/final_washer_list"); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			 </form>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	
	
	
	$('#final_washer_id').change(function(){
		var washerid = parseInt($('#final_washer_id').val());
		$.post('<?php echo site_url('final_manager/get_size_washer_byid'); ?>', { washer_id : washerid }, function(data){
			$('#final_washer_size_id').html(data);
		});
	});
	
	$('#final_washer_size_id').change(function(){
		var washersizeid = parseInt($('#final_washer_size_id').val());
		var washerid = parseInt($('#final_washer_id').val());
		
		$.post('<?php echo site_url('final_manager/get_income_washer_byid'); ?>', { washer_size_id : washersizeid, washer_id : washerid }, function(data){
			console.log(data);
			$('#final_washer_income_id').html(data);
		});
	});
	

	
		
	
});	
</script>