<div class="span12">
	<div class="widget">
		<div class="widget-title">
			<h4><i class="icon-cogs"></i>Final Test (Step 1)</h4>
			<span class="tools">
			<a href="javascript:;" class="icon-chevron-down"></a>
			<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>		
			</span>							
		</div>
		<div class="widget-body form">
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
				
			<form method="post" class="form-vertical" enctype="multipart/form-data">
				
				<div class="form-group">
					<label  for="income_id">Income ID : </label>
					<div class="controls">
						<select name="income_id" id="income_id">
							<option value="" <?php echo set_select('income_id',''); ?>>เลือก Income ID</option>
							<?php
								if($incomelist){
									foreach($incomelist as $ilist){
										?>
										<option value="<?php echo $ilist->income_id; ?>" <?php echo set_select('income_id', $ilist->income_id, $testinfo->income_id==$ilist->income_id); ?>><?php echo $ilist->income_id.' - '.$ilist->income_job_id; ?></option>
										<?php
									}
								}
								?>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label  for="supplier_name">Supplier Name : </label>
					<div class="controls">
						<input type="hidden" name="supplier_id" id="supplier_id" value="<?php echo $testinfo->supplier_id; ?>" />
						<input type="text" name="supplier_name" id="supplier_name" value="<?php echo set_value('supplier_name', $supplierinfo->supplier_name); ?>" class="form-control input-md" />
					</div>
				</div>
				
				<div class="form-group">
					<label  for="income_test_date">Test Date : </label>
					<div class="controls">
						<input type="text" name="income_test_date" id="income_test_date" value="<?php echo set_value('income_test_date', $testinfo->income_test_date); ?>" class="form-control input-md" />
					</div>
				</div>
				
				<div class="form-group">
					<label  for="product_type">Product Type : </label>
					<div class="controls">
						<select name="product_type" id="product_type">
							<option value="black" <?php echo set_select('product_type','black', $testinfo->product_type=='black'); ?>>BLACK</option>
							<option value="hdg" <?php echo set_select('product_type','hdg', $testinfo->product_type=='hdg'); ?>>HDG</option>
							<option value="zing" <?php echo set_select('product_type','zing', $testinfo->product_type=='zing'); ?>>ZING</option>
						</select>
					</div>
				</div>
				
				<div class="form-actions">
					<input type="hidden" name="product_id" id="product_id" value="<?php echo $productid; ?>" />
					<input type="hidden" name="final_id" id="final_id" value="<?php echo $finalid; ?>" />
					<input type="hidden" name="size_id" id="size_id" value="<?php echo $productinfo->size_id; ?>" />
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-mini" href="<?php echo admin_url("final_manager_anchor/final_list/".$productinfo->product_type."/".$productinfo->product_id); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
				
			</form>
			
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('#income_id').change(function(){
		$.post('<?php echo admin_url('final_test_anchor/get_incomeinfo'); ?>/'+$(this).val(), function(res){
			$('#supplier_id').val(res.data.income_info.supplier_id);
			$('#supplier_name').val(res.data.income_info.supplier_name);
			$('#income_test_date').val(res.data.income_info.income_createdtime);
		});
	});
});
</script>