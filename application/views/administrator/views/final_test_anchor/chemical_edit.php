<div class="span12">
	<div class="widget">
		<div class="widget-title">
			<h4><i class="icon-cogs"></i>ทดสอบค่า Chemical ของ Anchor</h4>
			<span class="tools">
			<a href="javascript:;" class="icon-chevron-down"></a>
			<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>		
			</span>							
		</div>
		<div class="widget-body form">
			<?php echo $this->session->flashdata('message-success'); ?>
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
				
			<form method="post" class="form-vertical">
				<div class="form-group">
					<label  for="chemical_test_using">Use? : </label>
					<div class="controls">
						<select name="chemical_test_using" id="chemical_test_using">
							<option value="0" <?php echo set_select('chemical_test_using','0', $testinfo->chemical_test_use==0); ?>>No</option>
							<option value="1" <?php echo set_select('chemical_test_using','1', $testinfo->chemical_test_use==1); ?>>Yes</option>
						</select>
					</div>
				</div>
				<table class="table table-striped table-bordered table-advance table-hover" id="table-chemical" style="display:<?php echo ($testinfo->chemical_test_use==0)?'none':''; ?>;"> 
					<tr>
						<th style="width:30%;">Chemical composition</th>
						<th style="width:30%;">Chemical value (%)</th>
                        <th style="width:40%;">Chemical result</th>
					</tr>
					<?php
					if($chemicallist){
						foreach($chemicallist as $clist){
							?>
							<tr>
								<td>
									<?php echo $clist->chemical_value; ?>
								</td>
								<td>
									<?php echo ($clist->ps_min>0)?$clist->ps_min:0; ?> - <?php echo ($clist->ps_max>0)?$clist->ps_max:''; ?>
								</td>
                                <td><input type="text" name="chemical_result[]" value="<?php echo trim(set_value('chemical_result[]', $clist->chemical_result)); ?>" class="span2 numericonly" placeholder="ค่า" /></td>
							</tr>
							<input type="hidden" name="chemical_id[]" value="<?php echo $clist->chemical_id; ?>" />
							<input type="hidden" name="chemical_value[]" value="<?php echo $clist->chemical_value; ?>" />
							<input type="hidden" name="ps_min[]" value="<?php echo $clist->ps_min; ?>" />
							<input type="hidden" name="ps_max[]" value="<?php echo $clist->ps_max; ?>" />
                            <input type="hidden" name="product_type" id="product_type" value="<?php echo $productinfo->product_type; ?>" />
							<?php
						}
					}
					?>
					
				</table>
				<p style="clear:both;">&nbsp;</p>
				
				
				<div class="form-actions">
					<input type="hidden" name="process_status" id="process_status" value="<?php echo $test_status; ?>" />
					<input type="hidden" name="income_id" id="income_id" value="<?php echo $testinfo->income_id; ?>" />
					<input type="hidden" name="product_id" id="product_id" value="<?php echo $productid; ?>" />
					<input type="hidden" name="final_id" id="final_id" value="<?php echo $finalid; ?>" />
					<input type="hidden" name="test_id" id="test_id" value="<?php echo $testid; ?>" />
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-mini" href="javascript:window.history.back();"><i class="icon-undo"></i> กลับไปยังหน้าที่แล้ว</a>
				 	<a class="btn btn-mini pull-right" href="<?php echo admin_url("final_manager_anchor/final_list/".$productinfo->product_type."/".$productinfo->product_id); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			 </form>
		</div>
	</div>
</div>
<style type="text/css">
#menu_icon_chzn{
	font-family: 'FontAwesome',Tahoma;
}
</style>

<script type="text/javascript">
$(document).ready(function(){
	$('#chemical_test_using').change(function(){
		if($(this).val()=='1'){
			$('#table-chemical').show();
		}else{
			$('#table-chemical').hide();
		}
	});
});
</script>