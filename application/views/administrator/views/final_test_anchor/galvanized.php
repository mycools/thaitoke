<div class="span12">
	<div class="widget">
		<div class="widget-title">
			<h4><i class="icon-cogs"></i>ทดสอบค่า Galvanized &amp; Heat Treatment ของ Anchor</h4>
			<span class="tools">
			<a href="javascript:;" class="icon-chevron-down"></a>
			<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>		
			</span>							
		</div>
		<div class="widget-body form">
			<?php echo $this->session->flashdata('message-success'); ?>
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
				
			<form method="post" class="form-vertical">
				<div class="form-group">
					<label  for="galvanized_test_using">Galvanized Use? : </label>
					<div class="controls">
						<select name="galvanized_test_using" id="galvanized_test_using">
							<option value="0" <?php echo set_select('galvanized_test_using','0', @$testinfo->galvanized_test_using==0); ?>>No</option>
							<option value="1" <?php echo set_select('galvanized_test_using','1', @$testinfo->galvanized_test_using==1); ?>>Yes</option>
						</select>
					</div>
				</div>
				<table class="table table-striped table-bordered table-advance table-hover" id="table-galvanized" style="display:<?php echo (@$testinfo->galvanized_test_using==0)?'none':''; ?>;">
					<tr>
						<th style="text-align:center;" colspan="2">Thickness Galvanized Standard : </th>
						<th style="text-align:center;">
							<select name="standard_id" id="standard_id">
								<?php
									if($standardlist){
										foreach($standardlist as $slist){
											?>
											<option value="<?php echo $slist->id; ?>" <?php echo set_select('standard_id', $slist->id, @$testinfo->standard_id==$slist->id); ?>><?php echo $slist->name; ?> <?php echo $slist->condition.' '.$slist->condition_value; ?></option>
											<?php
										}
									}
								?>
							</select>
						</th>
					</tr>
					<tr>
						<th style="text-align:center;">POINT NO.</th>
						<th style="text-align:center;">LOCATION</th>
						<th style="text-align:center;">THICKNESS HOTDIP GALVANIZED (Um)</th>
					</tr>
					<tr>
						<td style="text-align:center;">1</td>
						<td style="text-align:center;">BASE</td>
						<td style="text-align:center;">
							<input type="text" name="thickness_1" id="thickness_1" value="<?php echo set_value('thickness_1', @$testinfo->thickness_1); ?>" class="form-control input-sm" />
						</td>
					</tr>
					<tr>
						<td style="text-align:center;">2</td>
						<td style="text-align:center;">BASE</td>
						<td style="text-align:center;">
							<input type="text" name="thickness_2" id="thickness_2" value="<?php echo set_value('thickness_2', @$testinfo->thickness_2); ?>" class="form-control input-sm" />
						</td>
					</tr>
					<tr>
						<td style="text-align:center;">3</td>
						<td style="text-align:center;">BASE</td>
						<td style="text-align:center;">
							<input type="text" name="thickness_3" id="thickness_3" value="<?php echo set_value('thickness_3', @$testinfo->thickness_3); ?>" class="form-control input-sm" />
						</td>
					</tr>
					<tr>
						<td style="text-align:center;">4</td>
						<td style="text-align:center;">BASE</td>
						<td style="text-align:center;">
							<input type="text" name="thickness_4" id="thickness_4" value="<?php echo set_value('thickness_4', @$testinfo->thickness_4); ?>" class="form-control input-sm" />
						</td>
					</tr>
					<tr>
						<td style="text-align:center;">5</td>
						<td style="text-align:center;">BASE</td>
						<td style="text-align:center;">
							<input type="text" name="thickness_5" id="thickness_5" value="<?php echo set_value('thickness_5', @$testinfo->thickness_5); ?>" class="form-control input-sm" />
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>AVERAGE</td>
						<td style="text-align:center;">
							<input type="text" name="average" id="average" value="<?php echo set_value('average', @$testinfo->average); ?>" class="form-control input-sm" />
							<input type="button" name="calculate_average" id="calculate_average" value="คำนวนค่า Average" class="btn btn-mini" />
						</td>
					</tr>
				</table>
				<p style="clear:both;">&nbsp;</p>
				<div class="form-group">
					<label  for="heattreatment_test_using">Heat Treatment Use? : </label>
					<div class="controls">
						<select name="heattreatment_test_using" id="heattreatment_test_using">
							<option value="0" <?php echo set_select('heattreatment_test_using','0', @$testinfo->heattreatment_test_using==0); ?>>No</option>
							<option value="1" <?php echo set_select('heattreatment_test_using','1', @$testinfo->heattreatment_test_using==1); ?>>Yes</option>
						</select>
					</div>
				</div>
				<table class="table table-striped table-bordered table-advance table-hover" id="table-heattreatment" style="display:<?php echo (@$testinfo->heattreatment_test_using==0)?'none':''; ?>;">
					<tr>
						<th style="text-align:center;" colspan="4">HEAT TREATMENT</th>
					</tr>
					<tr>
						<th style="text-align:center;" colspan="2">Quenching</th>
						<th style="text-align:center;" colspan="2">Tempering</th>
					</tr>
					<tr>
						<th style="text-align:center;" colspan="2">
							<select name="quenching_temperature" id="quenching_temperature">
								<option value="C" <?php echo set_select('quenching_temperature','C', @$testinfo->quenching_temperature=='C'); ?>>C</option>
								<option value="F" <?php echo set_select('quenching_temperature','F', @$testinfo->quenching_temperature=='F'); ?>>F</option>
							</select>
						<th style="text-align:center;" colspan="2">
							<select name="tempering_temperature" id="tempering_temperature">
								<option value="C" <?php echo set_select('tempering_temperature','C', @$testinfo->tempering_temperature=='C'); ?>>C</option>
								<option value="F" <?php echo set_select('tempering_temperature','F', @$testinfo->tempering_temperature=='F'); ?>>F</option>
							</select>
						</th>
					</tr>
					<tr>
						<th style="text-align:center;">MIN</th>
						<th style="text-align:center;">MAX</th>
						<th style="text-align:center;">MIN</th>
						<th style="text-align:center;">MAX</th>
					</tr>
					<tr>
						<td style="text-align:center;"><input type="text" name="quenching_min" id="quenching_min" value="<?php echo set_value('quenching_min', @$testinfo->quenching_min); ?>" class="form-control input-sm" /></td>
						<td style="text-align:center;"><input type="text" name="quenching_max" id="quenching_max" value="<?php echo set_value('quenching_max', @$testinfo->quenching_max); ?>" class="form-control input-sm" /></td>
						<td style="text-align:center;"><input type="text" name="tempering_min" id="tempering_min" value="<?php echo set_value('tempering_min', @$testinfo->tempering_min); ?>" class="form-control input-sm" /></td>
						<td style="text-align:center;"><input type="text" name="tempering_max" id="tempering_max" value="<?php echo set_value('tempering_max', @$testinfo->tempering_max); ?>" class="form-control input-sm" /></td>
					</tr>
					<tr>
						<td style="text-align:center;" colspan="2"><input type="text" name="quenching_test" id="quenching_test" value="<?php echo set_value('quenching_test', @$testinfo->quenching_test); ?>" /></td>
						<td style="text-align:center;" colspan="2"><input type="text" name="tempering_test" id="tempering_test" value="<?php echo set_value('tempering_test', @$testinfo->tempering_test); ?>" /></td>
					</tr>
				</table>
				<p>
					สถานะการทดสอบ : 
					<label for="test_stauts_passed"><input type="radio" name="test_status" id="test_stauts_passed" value="2" <?php echo set_radio('test_status',2, @$test_status->test_status==2); ?> /> ผ่าน</label>
					<label for="test_stauts_notpass"><input type="radio" name="test_status" id="test_stauts_notpass" value="1" <?php echo set_radio('test_status',1, @$test_status->test_status==1); ?> /> ไม่ผ่าน</label>
				</p>
				
				<div class="form-actions">
					<input type="hidden" name="income_id" id="income_id" value="<?php echo $testinfo->income_id; ?>" />
					<input type="hidden" name="product_id" id="product_id" value="<?php echo $productid; ?>" />
					<input type="hidden" name="final_id" id="final_id" value="<?php echo $finalid; ?>" />
					<input type="hidden" name="test_id" id="test_id" value="<?php echo $testid; ?>" />
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-mini" href="javascript:window.history.back();"><i class="icon-undo"></i> กลับไปยังหน้าที่แล้ว</a>
				 	<a class="btn btn-mini pull-right" href="<?php echo admin_url("final_manager_anchor/final_list/".$productinfo->product_type."/".$productinfo->product_id); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			 </form>
		</div>
	</div>
</div>
<style type="text/css">
#menu_icon_chzn{
	font-family: 'FontAwesome',Tahoma;
}
</style>

<script type="text/javascript">
$(document).ready(function(){
	$('#galvanized_test_using').change(function(){
		if($(this).val()=='1'){
			$('#table-galvanized').show();
		}else{
			$('#table-galvanized').hide();
		}
	});
	
	$('#heattreatment_test_using').change(function(){
		if($(this).val()=='1'){
			$('#table-heattreatment').show();
		}else{
			$('#table-heattreatment').hide();
		}
	});
	
	$('#calculate_average').bind('click', function(){
		var thickness_1 = $('#thickness_1').val();
		var thickness_2 = $('#thickness_2').val();
		var thickness_3 = $('#thickness_3').val();
		var thickness_4 = $('#thickness_4').val();
		var thickness_5 = $('#thickness_5').val();
		
		if(thickness_1==''){
			alert('กรุณากรอก Point No. 1');
			$('#thickness_1').focus();
		}else if(thickness_2==''){
			alert('กรุณากรอก Point No. 2');
			$('#thickness_2').focus();
		}else if(thickness_3==''){
			alert('กรุณากรอก Point No. 3');
			$('#thickness_3').focus();
		}else if(thickness_4==''){
			alert('กรุณากรอก Point No. 4');
			$('#thickness_4').focus();
		}else if(thickness_5==''){
			alert('กรุณากรอก Point No. 5');
			$('#thickness_5').focus();
		}else{
			var result = (parseFloat(thickness_1)+parseFloat(thickness_2)+parseFloat(thickness_3)+parseFloat(thickness_4)+parseFloat(thickness_5))/5;
			$('#average').val(result);
		}
	});
});
</script>