<div class="span12">
	<div class="widget">
		<div class="widget-title">
			<h4><i class="icon-cogs"></i>ทดสอบค่า Material ของ Anchor</h4>
			<span class="tools">
			<a href="javascript:;" class="icon-chevron-down"></a>
			<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>		
			</span>							
		</div>
		<div class="widget-body form">
			<?php echo $this->session->flashdata('message-success'); ?>
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
				
			<form method="post" class="form-vertical">
				<div class="form-group">
					<label  for="material_test_using">Use? : </label>
					<div class="controls">
						<select name="material_test_using" id="material_test_using">
							<option value="0" <?php echo set_select('material_test_using','0', TRUE); ?>>No</option>
							<option value="1" <?php echo set_select('material_test_using','1'); ?>>Yes</option>
						</select>
					</div>
				</div>
				<table class="table table-striped table-bordered table-advance table-hover" id="table-material" style="display:none;"> 
					<tr>
						<th style="text-align:center;">Material</th>
						<?php
							if($materiallist){
								foreach($materiallist as $m){
									?>
									<th style="text-align:center;" colspan="2">
										<?php echo $m->standard_description; ?>
										<input type="hidden" name="description[]" value="<?php echo $m->standard_description; ?>" />
									</th>
									<?php
								}
							}
						?>
						<th style="text-align:center;" colspan="2">
							Hardness
							<input type="hidden" name="description[]" value="Hardness" />
						</th>
					</tr>
					<tr>
						<th>&nbsp;</th>
						<?php
							if($materiallist){
								foreach($materiallist as $m){
									?>
									<th style="text-align:center;">
										Min (<?php echo $m->ps_min_unit; ?>)
										<input type="hidden" name="ps_min_unit[]" value="<?php echo $m->ps_min_unit; ?>" />
									</th>
									<th style="text-align:center;">
										Max (<?php echo $m->ps_max_unit; ?>)
										<input type="hidden" name="ps_max_unit[]" value="<?php echo $m->ps_max_unit; ?>" />
									</th>
									<?php
								}
							}
						?>
						<th style="text-align:center;">
							Min (<?php echo $anchortesting->ps_min_unit; ?>)
							<input type="hidden" name="ps_min_unit[]" value="<?php echo $anchortesting->ps_min_unit; ?>" />
						</th>
						<th style="text-align:center;">
							Max (<?php echo $anchortesting->ps_max_unit; ?>)
							<input type="hidden" name="ps_max_unit[]" value="<?php echo $anchortesting->ps_max_unit; ?>" />
						</th>
					</tr>
					<tr>
						<th>&nbsp;</th>
						<?php
							if($materiallist){
								foreach($materiallist as $m){
									?>
									<th style="text-align:center;">
										<?php echo ($m->ps_min>0)?$m->ps_min:0; ?>
										<input type="hidden" name="ps_min[]" value="<?php echo $m->ps_min; ?>" />
									</th>
									<th style="text-align:center;">
										<?php echo ($m->ps_max>0)?$m->ps_max:0; ?>
										<input type="hidden" name="ps_max[]" value="<?php echo $m->ps_max; ?>" />
									</th>
									<?php
								}
							}
						?>
						<th style="text-align:center;">
							<?php echo ($anchortesting->ps_min>0)?$anchortesting->ps_min:0; ?>
							<input type="hidden" name="ps_min[]" value="<?php echo $anchortesting->ps_min; ?>" />
						</th>
						<th style="text-align:center;">
							<?php echo ($anchortesting->ps_max>0)?$anchortesting->ps_max:0; ?>
							<input type="hidden" name="ps_max[]" value="<?php echo $anchortesting->ps_max; ?>" />
						</th>
					</tr>
					<tr>
						<th style="text-align:center;">Result</th>
						<?php
							if($materiallist){
								foreach($materiallist as $m){
									?>
									<td colspan="2" style="text-align:center;">
										<input type="text" name="test_result[]" value="<?php echo set_value('test_result[]', $m->ps_result); ?>" class="form-control input-sm" style="text-align:center;" />
									</td>
									<?php
								}
							}
						?>
						<td colspan="2" style="text-align:center;">
							<input type="text" name="test_result[]" value="<?php echo set_value('test_result[]', $anchortesting->anchor_testing1); ?>" class="form-control input-sm" style="text-align:center;" />
						</td>
					</tr>
				</table>
				<p style="clear:both;">&nbsp;</p>
				
				<div class="form-actions">
					<input type="hidden" name="process_status" id="process_status" value="<?php echo $test_status; ?>" />
					<input type="hidden" name="income_id" id="income_id" value="<?php echo $testinfo->income_id; ?>" />
					<input type="hidden" name="product_id" id="product_id" value="<?php echo $productid; ?>" />
					<input type="hidden" name="final_id" id="final_id" value="<?php echo $finalid; ?>" />
					<input type="hidden" name="test_id" id="test_id" value="<?php echo $testid; ?>" />
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-mini" href="javascript:window.history.back();"><i class="icon-undo"></i> กลับไปยังหน้าที่แล้ว</a>
				 	<a class="btn btn-mini pull-right" href="<?php echo admin_url("final_manager_anchor/final_list/".$productinfo->product_type."/".$productinfo->product_id); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			 </form>
		</div>
	</div>
</div>
<style type="text/css">
#menu_icon_chzn{
	font-family: 'FontAwesome',Tahoma;
}
</style>

<script type="text/javascript">
$(document).ready(function(){
	$('#material_test_using').change(function(){
		if($(this).val()=='1'){
			$('#table-material').show();
		}else{
			$('#table-material').hide();
		}
	});
});
</script>