<div class="row">
	<div class="col-md-12">
	<h5 class="over-title margin-bottom-15"><span class="text-bold"><i class="fa fa-dashboard"></i> Quick Launch</span></h5>
	<?php $success = $this->session->flashdata("message-success"); ?>
	<?php if($success){ ?>
	<div class="alert alert-success">
		<button class="close" data-dismiss="alert">×</button>
		<strong>การทำรายการเสร็จสมบูรณ์ </strong> <?php echo $success; ?>
	</div>
	<?php } ?>

	<?php $info = $this->session->flashdata("message-info"); ?>
	<?php if($info){ ?>
	<div class="alert alert-info">
		<button class="close" data-dismiss="alert">×</button>
		<strong>ข้อมูลเพิ่มเติม </strong> <?php echo $info; ?>
	</div>
	<?php } ?>

	<?php $error = $this->session->flashdata("message-error"); ?>
	<?php if($error){ ?>
	<div class="alert alert-danger">
		<button class="close" data-dismiss="alert">×</button>
		<strong>เกิดข้อผิดพลาด </strong> <?php echo $error; ?>
	</div>
	<?php } ?>
	<?php $warning = $this->session->flashdata("message-warning"); ?>
	<?php if($warning){ ?>
	<div class="alert alert-warning">
		<button class="close" data-dismiss="alert">×</button>
		<strong>คำเตือน </strong> <?php echo $warning; ?>
	</div>
	<?php } ?>
	</div>
	<div class="col-sm-4">
		<div class="panel panel-white no-radius text-center">
			<div class="panel-body">
				<span class="fa-stack fa-2x"> <i class="fa fa-square fa-stack-2x text-warning"></i> <i class="fa fa-address-book-o fa-stack-1x fa-inverse"></i> </span>
				<h2 class="StepTitle text-warning">CUSTOMER</h2>
				<p class="text-small">
					Manage your customers profile.
				</p>
				<p class="links">
					<a class="btn" href="<?php echo admin_url('company/add_customer'); ?>">
						+ADD
					</a> 
					<a class="btn" href="<?php echo admin_url('company/customer_list'); ?>">
						MORE
					</a>
				</p>
			</div>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="panel panel-white no-radius text-center">
			<div class="panel-body">
				<span class="fa-stack fa-2x"> <i class="fa fa-square fa-stack-2x text-info"></i> <i class="fa fa-briefcase fa-stack-1x fa-inverse"></i> </span>
				<h2 class="StepTitle">SUPPLIER</h2>
				<p class="text-small">
					Manage your suppliers profile.
				</p>
				<p class="links">
					<a class="btn" href="<?php echo admin_url('company/add_supplier'); ?>">
						+ADD
					</a> 
					<a class="btn" href="<?php echo admin_url('company/supplier_list'); ?>">
						MORE
					</a>
				</p>
			</div>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="panel panel-white no-radius text-center">
			<div class="panel-body">
				<span class="fa-stack fa-2x"> <i class="fa fa-square fa-stack-2x text-primary"></i> <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i> </span>
				<h2 class="StepTitle">PRODUCT</h2>
				<p class="text-small">
					Manage yours product
				</p>
				<p class="links">
					<div class="btn-group">
						<a class="btn dropdown-toggle" data-toggle="dropdown" href="<?php echo admin_url('manageproduct/bolt_add'); ?>">
							+ADD
						</a>
						<ul role="menu" class="dropdown-menu dropdown-light pull-right">
							<li>
								<a href="<?php echo admin_url('manageproduct/bolt_add'); ?>"><i class="tokeicon-bolt"></i> ADD BOLT</a>
							</li>
							<li>
								<a href="<?php echo admin_url('manageproduct/nut_add'); ?>"><i class="tokeicon-nut"></i> ADD NUT</a>
							</li>
							<li>
								<a href="<?php echo admin_url('manageproduct/washer_add'); ?>"><i class="tokeicon-washer"></i> ADD WASHER</a>
							</li>
							<li>
								<a href="<?php echo admin_url('manageproduct/spring_add'); ?>"><i class="tokeicon-spring-washer"></i> ADD SPRING WASHER</a>
							</li>
							<li>
								<a href="<?php echo admin_url('manageproduct/taper_add'); ?>"><i class="tokeicon-taper-washer"></i> ADD TAPER WASHER</a>
							</li>
							<li>
								<a href="<?php echo admin_url('manageproduct/stud_add'); ?>"><i class="tokeicon-stud-bolt"></i> ADD STUD BOLT</a>
							</li>
						</ul> 
					</div>
					<div class="btn-group">
						<a class="btn dropdown-toggle" data-toggle="dropdown" href="<?php echo admin_url('manageproduct/bolt_list'); ?>">
							MORE
						</a>
						<ul role="menu" class="dropdown-menu dropdown-light pull-right">
							<li>
								<a href="<?php echo admin_url('manageproduct/bolt_list'); ?>"><i class="tokeicon-bolt"></i> MANAGE BOLT</a>
							</li>
							<li>
								<a href="<?php echo admin_url('manageproduct/nut_list'); ?>"><i class="tokeicon-nut"></i> MANAGE NUT</a>
							</li>
							<li>
								<a href="<?php echo admin_url('manageproduct/washer_list'); ?>"><i class="tokeicon-washer"></i> MANAGE WASHER</a>
							</li>
							<li>
								<a href="<?php echo admin_url('manageproduct/spring_list'); ?>"><i class="tokeicon-spring-washer"></i> MANAGE SPRING WASHER</a>
							</li>
							<li>
								<a href="<?php echo admin_url('manageproduct/taper_list'); ?>"><i class="tokeicon-taper-washer"></i> MANAGE TAPER WASHER</a>
							</li>
							<li>
								<a href="<?php echo admin_url('manageproduct/stud_list'); ?>"><i class="tokeicon-stud-bolt"></i> MANAGE STUD BOLT</a>
							</li>
						</ul> 
					</div>
					
				</p>
			</div>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="panel panel-white no-radius text-center">
			<div class="panel-body">
				<span class="fa-stack fa-2x"> <i class="fa fa-square fa-stack-2x text-primary"></i> <i class="fa fa-truck fa-stack-1x fa-inverse"></i> </span>
				<h2 class="StepTitle">STOCK</h2>
				<p class="text-small">
					Manage yours product stock
				</p>
				<p class="links">
					<div class="btn-group">
						<a class="btn dropdown-toggle" data-toggle="dropdown" href="<?php echo admin_url('manageproduct/bolt_add'); ?>">
							+ADD
						</a>
						<ul role="menu" class="dropdown-menu dropdown-light">
							<li>
								<a href="<?php echo admin_url('stock_manager/bolt_add'); ?>"><i class="tokeicon-bolt"></i> ADD BOLT STOCK</a>
							</li>
							<li>
								<a href="<?php echo admin_url('stock_manager/nut_add'); ?>"><i class="tokeicon-nut"></i> ADD NUT STOCK</a>
							</li>
							<li>
								<a href="<?php echo admin_url('stock_manager/washer_add'); ?>"><i class="tokeicon-washer"></i> ADD WASHER STOCK</a>
							</li>
							<li>
								<a href="<?php echo admin_url('stock_manager/spring_add'); ?>"><i class="tokeicon-spring-washer"></i> ADD SPRING WASHER STOCK</a>
							</li>
							<li>
								<a href="<?php echo admin_url('stock_manager/taper_add'); ?>"><i class="tokeicon-taper-washer"></i> ADD TAPER WASHER STOCK</a>
							</li>
							<li>
								<a href="<?php echo admin_url('stock_manager/stud_add'); ?>"><i class="tokeicon-stud-bolt"></i> ADD STUD BOLT STOCK</a>
							</li>
						</ul> 
					</div>
					<div class="btn-group">
						<a class="btn dropdown-toggle" data-toggle="dropdown" href="<?php echo admin_url('manageproduct/bolt_list'); ?>">
							MORE
						</a>
						<ul role="menu" class="dropdown-menu dropdown-light">
							<li>
								<a href="<?php echo admin_url('stock_manager/bolt_list'); ?>"><i class="tokeicon-bolt"></i> MANAGE BOLT STOCK</a>
							</li>
							<li>
								<a href="<?php echo admin_url('stock_manager/nut_list'); ?>"><i class="tokeicon-nut"></i> MANAGE NUT STOCK</a>
							</li>
							<li>
								<a href="<?php echo admin_url('stock_manager/washer_list'); ?>"><i class="tokeicon-washer"></i> MANAGE WASHER STOCK</a>
							</li>
							<li>
								<a href="<?php echo admin_url('stock_manager/spring_list'); ?>"><i class="tokeicon-spring-washer"></i> MANAGE SPRING WASHER STOCK</a>
							</li>
							<li>
								<a href="<?php echo admin_url('stock_manager/taper_list'); ?>"><i class="tokeicon-taper-washer"></i> MANAGE TAPER WASHER STOCK</a>
							</li>
							<li>
								<a href="<?php echo admin_url('stock_manager/stud_list'); ?>"><i class="tokeicon-stud-bolt"></i> MANAGE STUD BOLT STOCK</a>
							</li>
						</ul> 
					</div>
					
				</p>
			</div>
		</div>
		
	</div>
	<div class="col-sm-4">
		<div class="panel panel-white no-radius text-center">
			<div class="panel-body">
				<span class="fa-stack fa-2x"> <i class="fa fa-square fa-stack-2x text-primary"></i> <i class="icofont icofont-law-document fa-stack-1x fa-inverse"></i> </span>
				<h2 class="StepTitle">INCOMING</h2>
				<p class="text-small">
					Manage yours product incoming
				</p>
				<p class="links">
					<div class="btn-group">
						<a class="btn dropdown-toggle" data-toggle="dropdown" href="<?php echo admin_url('income_manager/bolt_add'); ?>">
							+ADD
						</a>
						<ul role="menu" class="dropdown-menu dropdown-light">
							<li>
								<a href="<?php echo admin_url('income_manager/bolt_add'); ?>"><i class="tokeicon-bolt"></i> ADD BOLT INCOMING</a>
							</li>
							<li>
								<a href="<?php echo admin_url('income_manager/nut_add'); ?>"><i class="tokeicon-nut"></i> ADD NUT INCOMING</a>
							</li>
							<li>
								<a href="<?php echo admin_url('income_manager/washer_add'); ?>"><i class="tokeicon-washer"></i> ADD WASHER INCOMING</a>
							</li>
							<li>
								<a href="<?php echo admin_url('income_manager/spring_add'); ?>"><i class="tokeicon-spring-washer"></i> ADD SPRING WASHER INCOMING</a>
							</li>
							<li>
								<a href="<?php echo admin_url('income_manager/taper_add'); ?>"><i class="tokeicon-taper-washer"></i> ADD TAPER WASHER INCOMING</a>
							</li>
							<li>
								<a href="<?php echo admin_url('income_manager/stud_add'); ?>"><i class="tokeicon-stud-bolt"></i> ADD STUD BOLT INCOMING</a>
							</li>
						</ul> 
					</div>
					<div class="btn-group">
						<a class="btn dropdown-toggle" data-toggle="dropdown" href="<?php echo admin_url('manageproduct/bolt_list'); ?>">
							MORE
						</a>
						<ul role="menu" class="dropdown-menu dropdown-light">
							<li>
								<a href="<?php echo admin_url('income_manager/bolt_list'); ?>"><i class="tokeicon-bolt"></i> MANAGE BOLT INCOMING</a>
							</li>
							<li>
								<a href="<?php echo admin_url('income_manager/nut_list'); ?>"><i class="tokeicon-nut"></i> MANAGE NUT INCOMING</a>
							</li>
							<li>
								<a href="<?php echo admin_url('income_manager/washer_list'); ?>"><i class="tokeicon-washer"></i> MANAGE WASHER INCOMING</a>
							</li>
							<li>
								<a href="<?php echo admin_url('income_manager/spring_list'); ?>"><i class="tokeicon-spring-washer"></i> MANAGE SPRING WASHER INCOMING</a>
							</li>
							<li>
								<a href="<?php echo admin_url('income_manager/taper_list'); ?>"><i class="tokeicon-taper-washer"></i> MANAGE TAPER WASHER INCOMING</a>
							</li>
							<li>
								<a href="<?php echo admin_url('income_manager/stud_list'); ?>"><i class="tokeicon-stud-bolt"></i> MANAGE STUD BOLT INCOMING</a>
							</li>
						</ul> 
					</div>
					
				</p>
			</div>
		</div>
		
	</div>
	<div class="col-sm-4">
		<div class="panel panel-white no-radius text-center">
			<div class="panel-body">
				<span class="fa-stack fa-2x"> <i class="fa fa-square fa-stack-2x text-primary"></i> <i class="fa icofont icofont-certificate-alt-1 fa-stack-1x fa-inverse"></i> </span>
				<h2 class="StepTitle">CERTIFICATE</h2>
				<p class="text-small">
					Manage yours product cetificate
				</p>
				<p class="links">
					
					<div class="btn-group">
						<a class="btn dropdown-toggle" data-toggle="dropdown" href="<?php echo admin_url('manageproduct/bolt_list'); ?>">
							MORE
						</a>
						<ul role="menu" class="dropdown-menu dropdown-light pull-right">
							<li>
								<a href="<?php echo admin_url('certificate_manager/certificate_bolt_list'); ?>"><i class="tokeicon-bolt"></i> BOLT CERTIFICATE</a>
							</li>
							<li>
								<a href="<?php echo admin_url('certificate_manager/certificate_nut_list'); ?>"><i class="tokeicon-nut"></i> NUT CERTIFICATE</a>
							</li>
							<li>
								<a href="<?php echo admin_url('certificate_manager/certificate_washer_list'); ?>"><i class="tokeicon-washer"></i> WASHER CERTIFICATE</a>
							</li>
							<li>
								<a href="<?php echo admin_url('certificate_manager/certificate_spring_list'); ?>"><i class="tokeicon-spring-washer"></i> SPRING WASHER CERTIFICATE</a>
							</li>
							<li>
								<a href="<?php echo admin_url('certificate_manager/certificate_taper_list'); ?>"><i class="tokeicon-taper-washer"></i> TAPER WASHER CERTIFICATE</a>
							</li>
							<li>
								<a href="<?php echo admin_url('certificate_manager/certificate_stud_list'); ?>"><i class="tokeicon-stud-bolt"></i> STUD BOLT CERTIFICATE</a>
							</li>
						</ul> 
					</div>
					
				</p>
			</div>
		</div>
		
	</div>
	<div class="col-sm-12">
		<div style="height: 300px;"></div>
	</div>
</div>