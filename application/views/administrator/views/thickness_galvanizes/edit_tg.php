<div class="col-sm-12">
	<!-- BEGIN SAMPLE TABLE PORTLET-->
	<div class="panel panel-white" id="panel4">
		<div class="panel-heading">
			<h4 class="panel-title text-primary"><i class="icon-list-alt"></i> <?php echo $_menu_name; ?></h4>
			<div class="panel-tools">
				
				<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>	
			</div>
		</div>
		<div class="panel-body">
    	<form method="post" name="menu_form" id="menu_form" enctype="multipart/form-data">
    	<input type="hidden" name="id" value="<?php echo $row['id']?>" />
			  <?php if(@$success_message!=NULL){ ?>
		      <div class="alert alert-success"> 
		        <button class="close" data-dismiss="alert">×</button>
		        <strong>Success !</strong> <?php echo $success_message; ?>
		      </div>
		      <?php } ?>
		     
		      	<?php echo @$validation_errors; ?>
			    <?php if(@$error_message!=NULL){ ?>
			    	<div class="alert alert-error">
			        	<button class="close" data-dismiss="alert">×</button>
			            <strong>Error !</strong> <?php echo $error_message; ?>
			        </div>
			    <?php }?>
			 
			 <div class="form-group">
			 <label  for="product_name">ชื่อ * :</label>
			 <div class="controls">
			    <div class="input-group">
			       <span class="input-group-addon"><i class="icon-font"></i></span>
			 <input class="form-control input-md" name="name" id="name" type="text" placeholder="กรุณาระบุชื่อ" value="<?php echo $row['name']; ?>" />	
			        
			    </div>
			    
			 </div>
			</div>

			 <div class="form-group">
			 <label  for="product_name">เกรด * :</label>
			 <div class="controls">
			    <div class="input-group">
			       <span class="input-group-addon"><i class="icon-slack"></i></span>
			 <input class="form-control input-md" name="grade" id="grade" type="text" placeholder="กรุณาระบุเกรด" value="<?php echo $row['grade']; ?>" />	
			        
			    </div>
			    
			 </div>
			</div>
            
            <div class="form-group">
			 <label  for="product_name">ค่า * :</label>
			 <div class="controls">
			    		
			       <select  name="condition" class="js-example-basic-single js-states form-control" tabindex="-1" >
                        <option value=">" <?php if($row['condition'] == ">"){?> selected="selected" <?php }?>> > </option>
						<option value="<" <?php if($row['condition'] == "<"){?> selected="selected" <?php }?>> < </option>
						<option value="=" <?php if($row['condition'] == "="){?> selected="selected" <?php }?>> = </option>
                    </select>   
			 	   <input class="form-control input-md" name="condition_value" id="condition_value" type="text" placeholder="กรุณาระบุค่า" value="<?php echo $row['condition_value']; ?>" />
			        
			    </div>
			    
			 </div>
			<div class="form-group">
			 <label  for="product_name">หน่วย * :</label>
			 <div class="controls">
			    <div class="input-group">
			      
			 <input class="form-control input-md" name="unit" id="unit" type="text" placeholder="กรุณาระบุหน่วย" value="<?php echo $row['unit']; ?>" />
			        
			    </div>
			    
			 </div>
			</div>
            
            <div class="form-group">
			 <label  for="product_name"> เครื่องมือ * :</label>
			 <div class="controls">
			    <div class="input-group">
			      
			 <input class="form-control input-md" name="tools" id="tools" type="text" placeholder="กรุณาระบุเครื่องมือ" value="<?php echo $row['tools']; ?>" />
			        
			    </div>
			    
			 </div>
			</div>
			
            <div class="form-actions">
                <button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> Save </button>
               <a class="btn btn-warning" href="<?php echo admin_url("thickness_galvanizes/tg_list"); ?>"><i class="icon-reply"></i> ยกเลิก</a>
            </div>    	
            </form>
    	
    </div>
  </div>
  <!-- END RECENT ORDERS PORTLET--> 
</div> 
<script  type="text/javascript">
function change_url()
{
	
	if($('#menu_url_system').val()==""){
		$('#menu_url').show();
	}else{
		$('#menu_url').hide();
		//if($('#menu_url_system').val()!="-"){
			$('#menu_url').val($('#menu_url_system').val())
		//}
	}
}
$(document).ready(function(){ change_url(); });
function delete_data(cms_id)
{
	if(confirm("Delete Data !. Are you sure ?")){
	$("#menu_form").attr("action",admin_url+"site_menu/delete_menu/"+cms_id+"/");
	$("#menu_form").submit();
	}
}
</script>