<div class="col-sm-12">
	<!-- BEGIN SAMPLE TABLE PORTLET-->
	<div class="panel panel-white" id="panel4">
		<div class="panel-heading">
			<h4 class="panel-title text-primary"><i class="icon-list-alt"></i> <?php echo $_menu_name; ?></h4>
			<div class="panel-tools">
				
				<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>	
			</div>
		</div>
		<div class="panel-body">
    	<form method="post" name="menu_form" id="menu_form" enctype="multipart/form-data" class="form-vertical">
          <?php if(@$success_message!=NULL){ ?>
      <div class="alert alert-success"> 
        <button class="close" data-dismiss="alert">×</button>
        <strong>Success !</strong> <?php echo $success_message; ?>
      </div>
      <?php } ?>

        <?php echo @$validation_errors; ?>
            <?php if(@$error_message!=NULL){ ?>
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">×</button>
                    <strong>Error !</strong> <?php echo $error_message; ?>
                </div>
            <?php }?>
			<div class="form-group">
			 <label  for="product_name">ชื่อ * :</label>
			 <div class="controls">
			    <div class="input-group">
			       <span class="input-group-addon"><i class="icon-font"></i></span>
			 <input class="form-control input-md" name="name" id="name" type="text" placeholder="กรุณาระบุชื่อ" value="<?php echo set_value('name'); ?>" />
			        
			    </div>
			    
			 </div>
			</div>

			            
            <div class="form-group">
			 <label  for="product_name">เกรด * :</label>
			 <div class="controls">
			    <div class="input-group">
			       <span class="input-group-addon"><i class="icon-slack"></i></span>
			 <input class="form-control input-md" name="grade" id="grade" type="text" placeholder="กรุณาระบุเกรด" value="<?php echo set_value('grade'); ?>" />	
			        
			    </div>
			    
			 </div>
			</div>
            
            <div class="form-group">
			 <label  for="product_name">ค่า * :</label>
			 <div class="controls">
			    		
			       <select  name="condition" class="js-example-basic-single js-states form-control" tabindex="-1" >
                        <option value=">"> > </option>
						<option value="<"> < </option>
						<option value="="> = </option>
                    </select>   
			 	   <input class="form-control input-md" name="condition_value" id="condition_value" type="text" placeholder="กรุณาระบุค่า" value="<?php echo set_value('condition_value'); ?>" />
			        
			    </div>
			    
			 </div>
			
			<div class="form-group">
			 <label  for="product_name">หน่วย * :</label>
			 <div class="controls">
			    <div class="input-group">
			       
			 <input class="form-control input-md" name="unit" id="unit" type="text" placeholder="กรุณาระบุหน่วย" value="<?php echo set_value('unit'); ?>" />
			        
			    </div>
			    
			 </div>
			</div>
            
            <div class="form-group">
			 <label  for="product_name">เครื่องมือ * :</label>
			 <div class="controls">
			    <div class="input-group">
			       
			 <input class="form-control input-md" name="tools" id="tools" type="text" placeholder="กรุณาระบุเครื่องมือ" value="<?php echo set_value('tools'); ?>" />
			        
			    </div>
			    
			 </div>
			</div>
            
			<div class="form-actions">
                <button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> Save </button>
               <a class="btn btn-warning" href="<?php echo admin_url("thickness_galvanizes/tg_list"); ?>"><i class="icon-reply"></i> ยกเลิก</a>
            </div>
           
    	</form>
    	
    </div>
  </div>
  <!-- END RECENT ORDERS PORTLET--> 
</div>