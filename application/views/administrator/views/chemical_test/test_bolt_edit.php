<div class="col-sm-12">
	<!-- BEGIN SAMPLE TABLE PORTLET-->
	<div class="panel panel-white" id="panel4">
		<div class="panel-heading">
			<h4 class="panel-title text-primary"><i class="icon-cogs"></i>ทดสอบค่า Chemical ของ Bolt</h4>
			<div class="panel-tools">
				
				<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>	
			</div>
		</div>
		<div class="panel-body">
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
				
			<form method="post" class="form-vertical">
				<table class="table table-striped table-bordered table-advance table-hover"> 
					
					<tr>
						<?php
						if($row){
							foreach($row as $r){
								?>
								<th><?php echo $r['chemical_value']; ?></th>
								<?php
							}
						}
						?>
					</tr>
					<tr>
						<?php
						if($row){
							foreach($row as $r){
								?>
								<th><?php echo $r['ps_min']."-".$r['ps_max']; ?></th>
								<?php
							}
						}
						?>
					</tr>
					<tr>
						<?php
						$disabled = 'disabled';
						if($row){
							foreach($row as $r){
								$testresult = $this->chemical_test_model->get_bolt_test_result($income_id, $r['chemical_id']);
								if(@$testresult['test_value']==''){
									$disabled = 'enabled';
								}
								?>
								<th>
									<input type="text" name="test_value[]" value="<?php echo set_value('test_value', @$testresult['test_value']); ?>" class="form-control input-sm" />
									<input type="hidden" name="chemical_id[]" value="<?php echo $r['chemical_id']; ?>" />
									<input type="hidden" name="chemical_value[]" value="<?php echo $r['chemical_value']; ?>" />
								</th>
								<?php
							}
						}
						?>
					</tr>
				</table><br>
				<table class="table table-striped table-bordered table-advance table-hover">
					<tr>
						<td>THICKNESS GALVANIZED</td>
						<td>STANDARD GALVANIZED</td>
						<td></td>
					</tr>
					<tr>
						<td>
							<select name="thickness_id" id="thickness_id">
								<option value="">เลือก Thickness Galvarnized</option>
								<?php
								if($list){
									foreach($list as $tlist){
										?>
										<option value="<?php echo $tlist['id']; ?>"><?php echo $tlist['name']; ?></option>
										<?php
									}
								}
								?>
							</select>
						</td>
						<td><span id="thickness-condition-value">&nbsp;</span></td>
						<td><input type="text" name="thickness_value" value="<?php echo set_value('thickness_value'); ?>" /></td>
					</tr>
				</table>
				<p>
					<label><b>สถานะการทดสอบ : </b></label>
					<label for="test_status_pass"><input type="radio" name="test_status" id="test_status_pass" value="2" /> ผ่าน</label>
					<label for="test_status_failed"><input type="radio" name="test_status" id="test_status_failed" value="1" /> ไม่ผ่าน</label>
					<label for="test_status_undefied"><input type="radio" name="test_status" id="test_status_undefied" value="0" /> ยังไม่ได้ทดสอบ</label>
				</p>
				<p style="clear:both;">&nbsp;</p>
				<div class="form-actions">
				 	<button type="submit" class="btn btn-mini btn-primary" <?php echo ($disabled=='disabled')?'disabled="disabled"':''; ?>><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-warning" href="<?php echo admin_url("income_manager/bolt_list"); ?>"><i class="icon-reply"></i> กลับไปยัง Income bolt</a>
				</div>
			 </form>
		</div>
	</div>
</div>
<style type="text/css">
#menu_icon_chzn{
	font-family: 'FontAwesome',Tahoma;
}
</style>

<script type="text/javascript">
$(document).ready(function(){
	$('#thickness_id').change(function(){
		$.post('<?php echo site_url('chemical_test/get_thickness_value'); ?>', { thickness_id : $(this).val() }, function(data){
			if(data){
				$('#thickness-condition-value').html(data);
			}
		});
	});
	var disabledstatus = '<?php echo $disabled; ?>';
	if(disabledstatus=='disabled'){
		alert('มีการทดสอบ Income นี้เรียบร้อยแล้วค่ะ');
		top.location.href='<?php echo site_url('income_manager/bolt_list'); ?>';
	}
});
</script>