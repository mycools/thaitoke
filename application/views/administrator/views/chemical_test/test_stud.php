<div class="span12">
	<div class="widget">
		<div class="widget-title">
			<h4><i class="icon-cogs"></i>ทดสอบค่า Chemical ของ Stud</h4>
			<span class="tools">
			<a href="javascript:;" class="icon-chevron-down"></a>
			<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>		
			</span>							
		</div>
		<div class="widget-body form">
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
				
			<form method="post" class="form-vertical">
				<table class="table table-striped table-bordered table-advance table-hover"> 
					
					<tr>
						<?php
						if($row){
							foreach($row as $r){
								?>
								<th><?php echo $r['chemical_value']; ?></th>
								<?php
							}
						}
						?>
					</tr>
					<tr>
						<?php
						if($row){
							$i=0;
							foreach($row as $r){
								$i++;
								?>
								<th>
									<?php echo 'MIN : '.$r['ps_min'].'<br /> MAX : '.$r['ps_max']; ?>
									<input type="hidden" id="ps_min_<?php echo $i; ?>" value="<?php echo $r['ps_min']; ?>" />
									<input type="hidden" id="ps_max_<?php echo $i; ?>" value="<?php echo $r['ps_max']; ?>" />
								</th>
								<?php
							}
						}
						?>
					</tr>
					<tr>
						<?php
						$disabled = 'disabled';
						if($row){
							$i=0;
							foreach($row as $r){
								$i++;
								$testresult = $this->chemical_test_model->get_stud_test_result($income_id, $r['chemical_id']);
								if(@$testresult['test_value']==''){
									$disabled = 'enabled';
								}
								?>
								<th>
									<input type="text" name="test_value[]" value="<?php echo set_value('test_value', @$testresult['test_value']); ?>" class="form-control input-sm" onblur="checkvalue(<?php echo $i; ?>, $(this));" />
									<input type="hidden" name="chemical_id[]" value="<?php echo $r['chemical_id']; ?>" />
									<input type="hidden" name="chemical_value[]" value="<?php echo $r['chemical_value']; ?>" />
								</th>
								<?php
							}
						}
						?>
					</tr>
				</table><br>
				<table class="table table-striped table-bordered table-advance table-hover">
					<tr>
						<td>THICKNESS GALVANIZED</td>
						<td>STANDARD GALVANIZED</td>
						<td></td>
					</tr>
					<tr>
						<td>
							<select name="thickness_id" id="thickness_id">
								<option value="">เลือก Thickness Galvarnized</option>
								<?php
								if($list){
									foreach($list as $tlist){
										?>
										<option value="<?php echo $tlist['id']; ?>" <?php echo set_select('thickness_id', $tlist['id'], @$thicknessinfo['thickness_id']==$tlist['id']); ?>><?php echo $tlist['name']; ?></option>
										<?php
									}
								}
								?>
							</select>
						</td>
						<td><span id="thickness-condition-value">&nbsp;</span></td>
						<td><input type="text" name="thickness_value" value="<?php echo set_value('thickness_value', @$thicknessinfo['thickness_value']); ?>" /></td>
					</tr>
				</table>
				<p>
					<label><b>สถานะการทดสอบ : </b></label>
					<label for="test_status_pass"><input type="radio" name="test_status" id="test_status_pass" value="2" <?php echo set_radio('test_status', 2, @$incomeinfo['test_status']==2); ?> /> ผ่าน</label>
					<label for="test_status_failed"><input type="radio" name="test_status" id="test_status_failed" value="1"  <?php echo set_radio('test_status', 1, @$incomeinfo['test_status']==1); ?> /> ไม่ผ่าน</label>
					<label for="test_status_undefied"><input type="radio" name="test_status" id="test_status_undefied" value="0" <?php echo set_radio('test_status', 0, @$incomeinfo['test_status']==0); ?> /> ยังไม่ได้ทดสอบ</label>
				</p>
				<p style="clear:both;">&nbsp;</p>
				<div class="form-actions">
				 	<button type="submit" class="btn btn-mini btn-primary" ><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-mini" href="<?php echo admin_url("income_manager/stud_list"); ?>"><i class="icon-reply"></i> กลับไปยัง Income Stud</a>
				</div>
			 </form>
		</div>
	</div>
</div>
<style type="text/css">
#menu_icon_chzn{
	font-family: 'FontAwesome',Tahoma;
}
</style>

<script type="text/javascript">
$(document).ready(function(){
	$('#thickness_id').change(function(){
		$.post('<?php echo site_url('chemical_test/get_thickness_value'); ?>', { thickness_id : $(this).val() }, function(data){
			if(data){
				$('#thickness-condition-value').html(data);
			}
		});
	});
});

function checkvalue(i, input){
	var min_val = $('#ps_min_'+i).val();
	var max_val = $('#ps_max_'+i).val();
	var input_val = input.val();
	console.log(min_val);
	console.log(max_val);
	console.log(input_val);
	if(input_val || input_val!=''){
		if(parseFloat(input_val)<parseFloat(min_val)){
			alert('ค่าที่กรอกต่ำกว่าค่า min ค่ะ');
			input.val('');
			input.focus();
		}else if(parseFloat(input_val)>parseFloat(max_val)){
			alert('ค่าที่กรอกสูงกว่าค่า max ค่ะ');
			input.val('');
			input.focus();
		}else{
			return false;
		}
	}
}
</script>