<div class="col-sm-12">
	<!-- BEGIN SAMPLE TABLE PORTLET-->
	<div class="panel panel-white" id="panel4">
		<div class="panel-heading">
			<h4 class="panel-title text-primary"><i class="icon-plus"></i> สร้างข้อมูลใหม่</h4>
			<div class="panel-tools">
				
				<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>	
			</div>
		</div>
		<div class="panel-body">
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
			<form method="post" class="form-vertical">
				<input name="income_id" id="income_id" type="hidden" value="<?php echo set_value('income_id',$this->_data['row']['income_id']); ?>" />
					<fieldset>
					<legend>ผู้จัดจำหน่าย</legend>
					<div class="form-group">
						<label  >Name of Shiper * :</label>
						<div class="controls">
							<select name="supplier_id" id="supplier_id" class="js-example-basic-single js-states form-control chosen" data-placeholder="เลือก SUPPLIER">
							 <?php foreach($this->washer_stock->getSupplier() as $rs){ ?>
								<option value="<?php echo $rs['supplier_id']; ?>" <?php if($this->_data['row']['income_supplier_id']==$rs['supplier_id']){ ?> selected="selected" <?php } ?>><?php echo $rs['supplier_name']; ?></option>
						       <?php } ?>
							</select>
						</div>
				</div>

					<div class="form-group">
						<label  for="income_perchase_order_no">Invoice NO * :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_invoice_no" id="income_invoice_no" type="text" placeholder="" value="<?php echo set_value('income_invoice_no',$this->_data['row']['income_invoice_no']); ?>" />	
							</div>
						
						</div>
					</div>
					
					<div class="form-group">
						<label  for="income_purchase_no">Purchase Order NO * :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_purchase_no" id="income_purchase_no" type="text" placeholder="" value="<?php echo set_value('income_purchase_no',$this->_data['row']['income_purchase_no']); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_perchase_order_no">Material :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_material" id="income_material" type="text" placeholder="" value="<?php echo set_value('income_material',$this->_data['row']['income_material']); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_perchase_order_no">Commodity :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_commodity" id="income_commodity" type="text" placeholder="" value="<?php echo set_value('income_commodity',$this->_data['row']['income_commodity']); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_perchase_order_no">Certificate NO  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_certificate_no" id="income_certificate_no" type="text" placeholder="" value="<?php echo set_value('income_certificate_no',$this->_data['row']['income_certificate_no']); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_perchase_order_no">P/O Number  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_po_no" id="income_po_no" type="text" placeholder="" value="<?php echo set_value('income_po_no',$this->_data['row']['income_po_no']); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_perchase_order_no">Delivery NO  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_delivery_no" id="income_delivery_no" type="text" placeholder="" value="<?php echo set_value('income_delivery_no',$this->_data['row']['income_delivery_no']); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_perchase_order_no">Heat NO  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_heat_no" id="income_heat_no" type="text" placeholder="" value="<?php echo set_value('income_heat_no',$this->_data['row']['income_heat_no']); ?>" />	
							</div>
						
						</div>
					</div>
                    <div class="form-group">
						<label  for="income_perchase_order_no">Lot NO  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_lot_no" id="income_lot_no" type="text" placeholder="" value="<?php echo set_value('income_lot_no',$this->_data['row']['income_lot_no']); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_perchase_order_no">Grade Mark  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="grade_mark" id="grade_mark" type="text" placeholder="" value="<?php echo set_value('grade_mark',$this->_data['row']['grade_mark']); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_perchase_order_no">Quantity  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_quantity" id="income_quantity" type="text" placeholder="" value="<?php echo set_value('income_quantity',$this->_data['row']['income_quantity']); ?>" disabled="disabled" />	
							</div>
						
						</div>
					</div>
				</fieldset>
				
										
				<div class="form-actions">
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-warning" href="<?php echo admin_url($this->router->fetch_class() . "/washer_list"); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			 </form>
		</div>
	</div>
</div>
