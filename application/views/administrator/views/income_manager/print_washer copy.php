<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<title>Washer income report</title>
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 14">
<link rel=File-List href="incom_files/filelist.xml">
<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
x\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]-->
<style id="incom_10942_Styles">
<!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
.font510942
	{color:black;
	font-size:36.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;}
.font610942
	{color:#F79646;
	font-size:36.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;}
.xl1510942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl6310942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:18.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl6410942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:18.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl6510942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:18.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl6610942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:20.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:.5pt dashed windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl6710942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl6810942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:.5pt dashed windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl6910942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7010942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:.5pt dashed windowtext;
	border-right:none;
	border-bottom:.5pt dashed windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7110942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7210942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7310942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:none;
	border-right:none;
	border-bottom:.5pt dashed windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7410942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:36.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:1.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:1.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7510942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:36.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:1.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7610942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:30.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:1.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7710942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border-top:1.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7810942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:1.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7910942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:1.5pt solid windowtext;
	border-right:1.5pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl8010942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:36.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:none;
	border-right:none;
	border-bottom:1.5pt solid windowtext;
	border-left:1.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl8110942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:36.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:none;
	border-right:none;
	border-bottom:1.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl8210942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:30.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:none;
	border-right:none;
	border-bottom:1.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl8310942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:1.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl8410942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:18.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:1.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl8510942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:18.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:1.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl8610942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:1.5pt solid windowtext;
	border-bottom:1.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl8710942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border:1.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl8810942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:bottom;
	border-top:1.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl8910942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:1.5pt solid windowtext;
	border-right:none;
	border-bottom:.5pt dashed windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl9010942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:middle;
	border-top:1.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl9110942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:1.5pt solid windowtext;
	border-right:none;
	border-bottom:.5pt dashed windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl9210942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:1.5pt solid windowtext;
	border-right:none;
	border-bottom:.5pt dotted windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl9310942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:18.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:1.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl9410942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:18.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:1.5pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl9510942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:1.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl9610942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:1.5pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl9710942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:bottom;
	border-top:1.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:1.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl9810942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:1.5pt solid windowtext;
	border-right:1.5pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl9910942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:1.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl10010942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:1.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl10110942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:1.5pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl10210942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:1.5pt solid windowtext;
	border-left:1.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl10310942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl10410942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl10510942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl10610942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:20.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl10710942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl10810942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl10910942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:1.0pt solid windowtext;
	border-right:1.0pt solid windowtext;
	border-bottom:1.0pt solid windowtext;
	border-left:1.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11010942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:1.0pt solid windowtext;
	border-right:1.5pt solid windowtext;
	border-bottom:1.0pt solid windowtext;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11110942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:1.5pt solid windowtext;
	border-bottom:1.0pt solid windowtext;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11210942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:1.5pt solid windowtext;
	border-bottom:1.0pt solid windowtext;
	border-left:1.0pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11310942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:18.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:1.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:1.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11410942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:18.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:1.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11510942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:18.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:1.5pt solid windowtext;
	border-right:1.5pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11610942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:1.5pt solid windowtext;
	border-left:1.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11710942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:1.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11810942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:1.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11910942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:.5pt dashed windowtext;
	border-right:none;
	border-bottom:1.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl12010942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:1.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl12110942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border-top:.5pt dashed windowtext;
	border-right:none;
	border-bottom:1.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl12210942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:1.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl12310942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:1.5pt solid windowtext;
	border-bottom:1.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl12410942
	{padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:22.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:.5pt dashed windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
-->
</style>
</head>

<body>
<!--[if !excel]>&nbsp;&nbsp;<![endif]-->
<!--The following information was generated by Microsoft Excel's Publish as Web
Page wizard.-->
<!--If the same item is republished from Excel, all information between the DIV
tags will be replaced.-->
<!----------------------------->
<!--START OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD -->
<!----------------------------->

<div id="incom_10942" align=center x:publishsource="Excel">

<table border=0 cellpadding=0 cellspacing=0 width=2095 style='border-collapse:
 collapse;table-layout:fixed;width:1573pt'>
 <col width=70 style='mso-width-source:userset;mso-width-alt:2560;width:53pt'>
 <col width=129 style='mso-width-source:userset;mso-width-alt:4717;width:97pt'>
 <col width=131 style='mso-width-source:userset;mso-width-alt:4790;width:98pt'>
 <col width=140 style='mso-width-source:userset;mso-width-alt:5120;width:105pt'>
 <col width=146 style='mso-width-source:userset;mso-width-alt:5339;width:110pt'>
 <col width=144 style='mso-width-source:userset;mso-width-alt:5266;width:108pt'>
 <col width=92 style='mso-width-source:userset;mso-width-alt:3364;width:69pt'>
 <col width=93 style='mso-width-source:userset;mso-width-alt:3401;width:70pt'>
 <col width=94 span=2 style='mso-width-source:userset;mso-width-alt:3437;
 width:71pt'>
 <col width=75 span=2 style='mso-width-source:userset;mso-width-alt:2742;
 width:56pt'>
 <col width=84 span=2 style='mso-width-source:userset;mso-width-alt:3072;
 width:63pt'>
 <col width=86 style='mso-width-source:userset;mso-width-alt:3145;width:65pt'>
 <col width=87 style='mso-width-source:userset;mso-width-alt:3181;width:65pt'>
 <col width=91 style='mso-width-source:userset;mso-width-alt:3328;width:68pt'>
 <col width=96 style='mso-width-source:userset;mso-width-alt:3510;width:72pt'>
 <col width=119 style='mso-width-source:userset;mso-width-alt:4352;width:89pt'>
 <col width=37 style='mso-width-source:userset;mso-width-alt:1353;width:28pt'>
 <col width=64 span=2 style='mso-width-source:userset;mso-width-alt:2340;
 width:48pt'>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td colspan=6 rowspan=2 height=73 class=xl7410942 width=626 style='border-bottom:
  1.5pt solid black;height:54.75pt;width:470pt'><font class="font610942">THAITOKE</font><font
  class="font510942"> ENGINEERING</font></td>
  <td class=xl7610942 width=92 style='width:69pt'>&nbsp;</td>
  <td colspan=8 rowspan=2 class=xl7510942 width=685 style='border-bottom:1.5pt solid black;
  width:515pt'>INCOMING INSPECTION SHEET</td>
  <td class=xl7710942 width=87 style='width:65pt'>&nbsp;</td>
  <td class=xl7710942 width=91 style='width:68pt'>&nbsp;</td>
  <td class=xl7710942 width=96 style='width:72pt'>&nbsp;</td>
  <td class=xl7810942 width=95 style='width:71pt'>&nbsp;</td>
  <td class=xl7810942 width=37 style='width:28pt'>&nbsp;</td>
  <td class=xl7810942 width=64 style='width:48pt'>&nbsp;</td>
  <td class=xl7910942 width=64 style='width:48pt'>&nbsp;</td>
 </tr>
 <tr height=53 style='height:39.75pt'>
  <td height=53 class=xl8210942 style='height:39.75pt'>&nbsp;</td>
  <td class=xl8310942>&nbsp;</td>
  <td colspan=2 class=xl8410942>Date :</td>
  <td colspan=3 class=xl8510942><?php echo date("d/m/Y");?></td>
  <td class=xl8610942>&nbsp;</td>
 </tr>
 <tr height=32 style='height:24.0pt'>
  <td height=32 class=xl11310942 style='height:24.0pt;border-top:none'>&nbsp;</td>
  <td class=xl11410942 style='border-top:none'>&nbsp;</td>
  <td class=xl11410942 style='border-top:none'>&nbsp;</td>
  <td class=xl11410942 style='border-top:none'>&nbsp;</td>
  <td class=xl11410942 style='border-top:none'>&nbsp;</td>
  <td class=xl11410942 style='border-top:none'>&nbsp;</td>
  <td class=xl11410942 style='border-top:none'>&nbsp;</td>
  <td class=xl11410942 style='border-top:none'>&nbsp;</td>
  <td class=xl11410942 style='border-top:none'>&nbsp;</td>
  <td class=xl11410942 style='border-top:none'>&nbsp;</td>
  <td class=xl11410942 style='border-top:none'>&nbsp;</td>
  <td class=xl11410942 style='border-top:none'>&nbsp;</td>
  <td class=xl11410942 style='border-top:none'>&nbsp;</td>
  <td class=xl11410942 style='border-top:none'>&nbsp;</td>
  <td class=xl11410942 style='border-top:none'>&nbsp;</td>
  <td class=xl11410942 style='border-top:none'>&nbsp;</td>
  <td class=xl11410942 style='border-top:none'>&nbsp;</td>
  <td class=xl11410942 style='border-top:none'>&nbsp;</td>
  <td class=xl11410942 style='border-top:none'>&nbsp;</td>
  <td class=xl11410942 style='border-top:none'>&nbsp;</td>
  <td class=xl11410942 style='border-top:none'>&nbsp;</td>
  <td class=xl11510942 style='border-top:none'>&nbsp;</td>
 </tr>
 <tr height=38 style='height:28.5pt'>
  <td colspan=2 height=38 class=xl9510942 style='height:28.5pt'>Commodity :</td>
  <td colspan=2 class=xl6810942><?php echo $income_info['income_commodity']; ?></td>
  <td class=xl6910942>Income ID:</td>
  <td colspan=2 class=xl6810942><?php echo $income_info['income_job_id']; ?></td>
  <td colspan=3 class=xl6710942>P/O Number:</td>
  <td colspan=3 class=xl6810942><?php echo $income_info['income_po_no']; ?></td>
  <td colspan=3 class=xl6710942>Certificate No :</td>
  <td colspan=3 class=xl7310942><?php echo $income_info['income_certificate_no']; ?></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl9610942>&nbsp;</td>
 </tr>
 <tr height=38 style='height:28.5pt'>
  <td colspan=2 height=38 class=xl9510942 style='height:28.5pt'>Quantity :</td>
  <td colspan=2 class=xl7010942><?php echo $income_info['income_quantity']; ?></td>
  <td class=xl6910942>Invoice No :</td>
  <td colspan=2 class=xl7110942><?php echo $income_info['income_invoice_no']; ?></td>
  <td colspan=3 class=xl6710942>Purchase Order No :</td>
  <td colspan=3 class=xl7010942><?php echo $income_info['income_purchase_no']; ?></td>
  <td colspan=3 class=xl12410942>Name Of Supplier :</td>
  <td colspan=5 class=xl6610942><?php echo $shipper['supplier_name']; ?></td>
  <td class=xl9610942>&nbsp;</td>
 </tr>
 <tr height=7 style='mso-height-source:userset;height:5.25pt'>
  <td height=7 class=xl11610942 style='height:5.25pt'>&nbsp;</td>
  <td class=xl11710942>&nbsp;</td>
  <td class=xl11810942>&nbsp;</td>
  <td class=xl11710942>&nbsp;</td>
  <td class=xl11710942>&nbsp;</td>
  <td class=xl11910942>&nbsp;</td>
  <td class=xl11910942>&nbsp;</td>
  <td class=xl11810942>&nbsp;</td>
  <td class=xl11810942>&nbsp;</td>
  <td class=xl11810942>&nbsp;</td>
  <td class=xl11810942>&nbsp;</td>
  <td class=xl11810942>&nbsp;</td>
  <td class=xl11810942>&nbsp;</td>
  <td class=xl12010942>&nbsp;</td>
  <td class=xl12110942 style='border-top:none'>&nbsp;</td>
  <td class=xl12110942 style='border-top:none'>&nbsp;</td>
  <td class=xl12010942>&nbsp;</td>
  <td class=xl12010942>&nbsp;</td>
  <td class=xl12010942>&nbsp;</td>
  <td class=xl12210942>&nbsp;</td>
  <td class=xl12210942>&nbsp;</td>
  <td class=xl12310942>&nbsp;</td>
 </tr>
 <tr height=39 style='height:29.25pt'>
  <td colspan=2 height=39 class=xl9710942 style='height:29.25pt'>Washer Grade :</td>
  <td colspan=3 class=xl8910942><?php echo $product['product_name']; ?></td>
  <td class=xl7810942 style='border-top:none'>&nbsp;</td>
  <td class=xl8810942 style='border-top:none'>Size :</td>
  <td colspan=2 class=xl8910942><?php echo $size['size_m']; ?>x<?php echo $size['size_p']; ?></td>
  <td class=xl7810942 style='border-top:none'>&nbsp;</td>
  <td colspan=3 class=xl9010942>Surface Condition :</td>
  <td colspan=3 class=xl9110942><?php echo $product['product_type']; ?></td>
  <td colspan=2 class=xl8810942>Heat No:</td>
  <td colspan=3 class=xl9210942><?php echo $income_info['income_heat_no']; ?></td>
  <td class=xl9810942 style='border-top:none'>&nbsp;</td>
 </tr>
 <tr height=7 style='mso-height-source:userset;height:5.25pt'>
  <td height=7 class=xl9910942 style='height:5.25pt'>&nbsp;</td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl9610942>&nbsp;</td>
 </tr>
 <tr height=38 style='mso-height-source:userset;height:28.5pt'>
  <td rowspan=2 height=77 class=xl10910942 style='height:57.75pt'>NO.</td>
  <td colspan=3 rowspan=2 class=xl10310942>DESCRIPTION</td>
  <td colspan=2 class=xl10410942 style='border-left:none'>STANDARD</td>
  <td colspan=2 rowspan=2 class=xl10510942 width=185 style='width:139pt'>INSECTION
  <br>
    TOOL</td>
  <td colspan=2 rowspan=2 class=xl10510942 width=188 style='width:142pt'>FREQUENCY
  OF <br>
    TESTING</td>
  <td colspan=2 rowspan=2 class=xl10610942 width=150 style='width:112pt'>INSPECTION
  <br>
    QUANTITY</td>
  <td rowspan=2 class=xl10310942>1</td>
  <td rowspan=2 class=xl10310942>2</td>
  <td rowspan=2 class=xl10310942>3</td>
  <td rowspan=2 class=xl10310942>4</td>
  <td rowspan=2 class=xl10310942>5</td>
  <td colspan=2 class=xl10310942 style='border-left:none'>OVERALL RESULT</td>
  <td colspan=3 rowspan=2 class=xl10310942 style='border-right:1.5pt solid black'>REASON</td>
 </tr>
 <tr height=39 style='height:29.25pt'>
  <td height=39 class=xl10410942 style='height:29.25pt;border-top:none;
  border-left:none'>MIN</td>
  <td class=xl10410942 style='border-top:none;border-left:none'>MAX</td>
  <td class=xl10310942 style='border-top:none;border-left:none'>PASS</td>
  <td class=xl10310942 style='border-top:none;border-left:none'>NO</td>
 </tr>
 <?PHP	$i = 0; 
        foreach( $std_list->result_array() AS $row ){   
        $income_data =  $this->income_test_model->get_income_washer_tested($income_id,$row['ps_id']);   
 ?>
 <tr height=39 style='height:29.25pt'>
  <td height=39 class=xl10910942 style='height:29.25pt;border-top:none'><?php echo $i+1;?></td>
  <td colspan=3 class=xl10410942 style='border-left:none'><?php echo $row['standard_description'];?></td>
  <td class=xl10410942 style='border-top:none;border-left:none'><?php echo $row['ps_min']. $row['ps_min_unit'];?></td>
  <td class=xl10410942 style='border-top:none;border-left:none'><?php echo $row['ps_max']. $row['ps_max_unit'];?></td>
  <td colspan=2 class=xl10410942 style='border-left:none'><?php $ins = $this->income_test_model->get_inspection($row['inspection_id']); if($row['inspection_id'] != "6"){ echo $ins['inspection_name']; } ?></td>
  <td colspan=2 class=xl10410942 style='border-left:none'><?php if($row['inspection_id'] != "6"){ echo "Before Delivery"; }?></td>
  <td colspan=2 class=xl10410942 style='border-left:none'><?php if($row['inspection_id'] != "6"){ echo "5 PCS/LOT"; }?></td>
  <td class=xl10410942 style='border-top:none;border-left:none'><?php echo $income_data['washer_testing1']; ?></td>
  <td class=xl10410942 style='border-top:none;border-left:none'><?php echo $income_data['washer_testing2']; ?></td>
  <td class=xl10410942 style='border-top:none;border-left:none'><?php echo $income_data['washer_testing3']; ?></td>
  <td class=xl10410942 style='border-top:none;border-left:none'><?php echo $income_data['washer_testing4']; ?></td>
  <td class=xl10410942 style='border-top:none;border-left:none'><?php echo $income_data['washer_testing5']; ?></td>
  <td class=xl10410942 style='border-top:none;border-left:none'><?php if($income_data['washer_testing_status']=='pass'){ ?>√<?php } ?></td>
  <td class=xl10410942 style='border-top:none;border-left:none'><?php if($income_data['washer_testing_status']=='not_pass'){ ?>√<?php } ?></td>
  <td colspan=3 class=xl10710942 style='border-top:none;border-left:none'><?php echo $income_data['washer_testing_remark']; ?></td>
  
 </tr>
 <?php  $i = $i+1; } ?>
                    
 <?PHP	//$i = 0; 
        foreach( $cer_size_list->result_array() AS $row1 ){  
        $income_cer_size_data =  $this->income_test_model->get_income_washer_cer_size_tested($income_id,$row1['ps_id']);
 ?>
 <tr height=39 style='height:29.25pt'>
  <td height=39 class=xl10910942 style='height:29.25pt;border-top:none'><?php echo $i+1;?></td>
  <td colspan=3 class=xl10410942 style='border-left:none'><?php echo $row1['ps_description'];?></td>
  <td class=xl10410942 style='border-top:none;border-left:none'><?php echo $row1['ps_min']. $row1['ps_min_unit'];?></td>
  <td class=xl10410942 style='border-top:none;border-left:none'><?php echo $row1['ps_max']. $row1['ps_max_unit'];?></td>
  <td colspan=2 class=xl10410942 style='border-left:none'><?php $ins1 = $this->income_test_model->get_inspection($row1['inspection_id']); if($row1['inspection_id'] != "6"){ echo $ins1['inspection_name']; } ?></td>
  <td colspan=2 class=xl10410942 style='border-left:none'><?php if($row1['inspection_id'] != "6"){ echo "Before Delivery"; }?></td>
  <td colspan=2 class=xl10410942 style='border-left:none'><?php if($row1['inspection_id'] != "6"){ echo "5 PCS/LOT"; }?></td>
  <td class=xl10410942 style='border-top:none;border-left:none'><?php echo $income_cer_size_data['washer_testing1']; ?></td>
  <td class=xl10410942 style='border-top:none;border-left:none'><?php echo $income_cer_size_data['washer_testing2']; ?></td>
  <td class=xl10410942 style='border-top:none;border-left:none'><?php echo $income_cer_size_data['washer_testing3']; ?></td>
  <td class=xl10410942 style='border-top:none;border-left:none'><?php echo $income_cer_size_data['washer_testing4']; ?></td>
  <td class=xl10410942 style='border-top:none;border-left:none'><?php echo $income_cer_size_data['washer_testing5']; ?></td>
  <td class=xl10410942 style='border-top:none;border-left:none'><?php if($income_cer_size_data['washer_testing_status']=='pass'){ ?>√<?php } ?></td>
  <td class=xl10410942 style='border-top:none;border-left:none'><?php if($income_cer_size_data['washer_testing_status']=='not_pass'){ ?>√<?php } ?></td>
  <td colspan=3 class=xl10710942 style='border-top:none;border-left:none'><?php echo $income_cer_size_data['washer_testing_remark']; ?></td>
  
 </tr>
                   
 <?php $i = $i+1; } ?>
 <tr height=39 style='height:29.25pt'>
  <td height=39 class=xl10910942 style='height:29.25pt;border-top:none'><?php echo $i+1;?></td>
  <td colspan=3 class=xl10410942 style='border-left:none'><?php echo "Grade Mark";?></td>
  <td colspan=4 class=xl10410942 style='border-top:none;border-left:none'><?php echo $income_info['grade_mark']; ?></td>
  
  <td colspan=2 class=xl10410942 style='border-left:none'></td>
  <td colspan=2 class=xl10410942 style='border-left:none'></td>
  <td class=xl10410942 style='border-top:none;border-left:none'></td>
  <td class=xl10410942 style='border-top:none;border-left:none'></td>
  <td class=xl10410942 style='border-top:none;border-left:none'></td>
  <td class=xl10410942 style='border-top:none;border-left:none'></td>
  <td class=xl10410942 style='border-top:none;border-left:none'></td>
  <td class=xl10410942 style='border-top:none;border-left:none'></td>
  <td class=xl10410942 style='border-top:none;border-left:none'></td>
  <td colspan=3 class=xl10710942 style='border-top:none;border-left:none'></td>
  
 </tr>
<tr height=39 style='height:29.25pt'>
  <td height=39 class=xl10910942 style='height:29.25pt;border-top:none'><?php echo $i+2;?></td>
  <td colspan=3 class=xl10410942 style='border-left:none'><?php echo "Pitch";?></td>
  <td colspan=4 class=xl10410942 style='border-top:none;border-left:none'><?php echo $size['size_p']; ?></td>
  
  <td colspan=2 class=xl10410942 style='border-left:none'></td>
  <td colspan=2 class=xl10410942 style='border-left:none'></td>
  <td class=xl10410942 style='border-top:none;border-left:none'></td>
  <td class=xl10410942 style='border-top:none;border-left:none'></td>
  <td class=xl10410942 style='border-top:none;border-left:none'></td>
  <td class=xl10410942 style='border-top:none;border-left:none'></td>
  <td class=xl10410942 style='border-top:none;border-left:none'></td>
  <td class=xl10410942 style='border-top:none;border-left:none'></td>
  <td class=xl10410942 style='border-top:none;border-left:none'></td>
  <td colspan=3 class=xl10710942 style='border-top:none;border-left:none'></td>
  
 </tr>
<tr height=39 style='height:29.25pt'>
  <td height=39 class=xl10910942 style='height:29.25pt;border-top:none'><?php echo $i+3;?></td>
  <td colspan=3 class=xl10410942 style='border-left:none'><?php echo "Thickness Galvanized";?></td>
  <td colspan=4 class=xl10410942 style='border-top:none;border-left:none'><?php echo $washer_thickness_info['condition'].$washer_thickness_info['condition_value']; ?> <?php if($washer_thickness_info['condition']){ echo "Micron"; }?></td>
  
  <td colspan=2 class=xl10410942 style='border-left:none'></td>
  <td colspan=2 class=xl10410942 style='border-left:none'></td>
  <td class=xl10410942 style='border-top:none;border-left:none'></td>
  <td class=xl10410942 style='border-top:none;border-left:none'></td>
  <td class=xl10410942 style='border-top:none;border-left:none'></td>
  <td class=xl10410942 style='border-top:none;border-left:none'></td>
  <td class=xl10410942 style='border-top:none;border-left:none'></td>
  <td class=xl10410942 style='border-top:none;border-left:none'></td>
  <td class=xl10410942 style='border-top:none;border-left:none'></td>
  <td colspan=3 class=xl10710942 style='border-top:none;border-left:none'></td>
  
 </tr>

 <tr height=38 style='height:28.5pt'>
  <td height=38 class=xl9910942 style='height:28.5pt'>&nbsp;</td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl9610942>&nbsp;</td>
 </tr>
 <tr height=39 style='height:29.25pt'>
  <td height=39 class=xl9910942 style='height:29.25pt'>&nbsp;</td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl9610942>&nbsp;</td>
 </tr>
 <tr height=40 style='height:30.0pt'>
  <td height=40 class=xl9910942 style='height:30.0pt'>&nbsp;</td>
  <td colspan=4 class=xl7210942></td>
  <td colspan=3 class=xl7210942></td>
  <td colspan=4 class=xl7210942></td>
  <td class=xl6910942></td>
  <td colspan=8 class=xl8710942>To Approve the Document</td>
  <td class=xl9610942>&nbsp;</td>
 </tr>
 <tr height=40 style='height:30.0pt'>
  <td height=40 class=xl9910942 style='height:30.0pt'>&nbsp;</td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td class=xl6910942></td>
  <td colspan=3 class=xl7210942></td>
  <td colspan=4 class=xl7210942></td>
  <td class=xl6910942></td>
  <td colspan=2 class=xl8710942>&nbsp;</td>
  <td colspan=2 class=xl8710942 style='border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl8710942 style='border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl8710942 style='border-left:none'>&nbsp;</td>
  <td class=xl9610942>&nbsp;</td>
 </tr>
 <tr height=40 style='height:30.0pt'>
  <td height=40 class=xl9910942 style='height:30.0pt'>&nbsp;</td>
  <td colspan=2 class=xl7210942></td>
  <td colspan=2 class=xl7110942></td>
  <td colspan=3 class=xl7210942></td>
  <td colspan=4 class=xl7210942></td>
  <td class=xl6910942></td>
  <td colspan=2 class=xl8710942>Revised</td>
  <td colspan=2 class=xl8710942 style='border-left:none'>Approved</td>
  <td colspan=2 class=xl8710942 style='border-left:none'>Auditor</td>
  <td colspan=2 class=xl8710942 style='border-left:none'>Maker</td>
  <td class=xl9610942>&nbsp;</td>
 </tr>
 <tr height=32 style='height:24.0pt'>
  <td height=32 class=xl9310942 style='height:24.0pt'>&nbsp;</td>
  <td colspan=2 class=xl6510942></td>
  <td colspan=2 class=xl6510942></td>
  <td colspan=3 class=xl6510942></td>
  <td colspan=4 class=xl6410942></td>
  <td class=xl6310942></td>
  <td class=xl6310942></td>
  <td class=xl6310942></td>
  <td class=xl6310942></td>
  <td class=xl6310942></td>
  <td class=xl6310942></td>
  <td class=xl6310942></td>
  <td class=xl6310942></td>
  <td class=xl6310942></td>
  <td class=xl9410942>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl10010942 style='height:15.0pt'>&nbsp;</td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl10110942>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl10010942 style='height:15.0pt'>&nbsp;</td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl10110942>&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl10010942 style='height:15.0pt'>&nbsp;</td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl10110942>&nbsp;</td>
 </tr>
 <tr height=21 style='height:15.75pt'>
  <td height=21 class=xl10210942 style='height:15.75pt'>&nbsp;</td>
  <td class=xl8310942>&nbsp;</td>
  <td class=xl8310942>&nbsp;</td>
  <td class=xl8310942>&nbsp;</td>
  <td class=xl8310942>&nbsp;</td>
  <td class=xl8310942>&nbsp;</td>
  <td class=xl8310942>&nbsp;</td>
  <td class=xl8310942>&nbsp;</td>
  <td class=xl8310942>&nbsp;</td>
  <td class=xl8310942>&nbsp;</td>
  <td class=xl8310942>&nbsp;</td>
  <td class=xl8310942>&nbsp;</td>
  <td class=xl8310942>&nbsp;</td>
  <td class=xl8310942>&nbsp;</td>
  <td class=xl8310942>&nbsp;</td>
  <td class=xl8310942>&nbsp;</td>
  <td class=xl8310942>&nbsp;</td>
  <td class=xl8310942>&nbsp;</td>
  <td class=xl8310942>&nbsp;</td>
  <td class=xl8310942>&nbsp;</td>
  <td class=xl8310942>&nbsp;</td>
  <td class=xl8610942>&nbsp;</td>
 </tr>
 <tr height=21 style='height:15.75pt'>
  <td height=21 class=xl1510942 style='height:15.75pt'></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
  <td class=xl1510942></td>
 </tr>
 <![if supportMisalignedColumns]>
 <tr height=0 style='display:none'>
  <td width=70 style='width:53pt'></td>
  <td width=103 style='width:77pt'></td>
  <td width=102 style='width:77pt'></td>
  <td width=103 style='width:77pt'></td>
  <td width=124 style='width:93pt'></td>
  <td width=124 style='width:93pt'></td>
  <td width=92 style='width:69pt'></td>
  <td width=93 style='width:70pt'></td>
  <td width=94 style='width:71pt'></td>
  <td width=94 style='width:71pt'></td>
  <td width=75 style='width:56pt'></td>
  <td width=75 style='width:56pt'></td>
  <td width=84 style='width:63pt'></td>
  <td width=84 style='width:63pt'></td>
  <td width=86 style='width:65pt'></td>
  <td width=87 style='width:65pt'></td>
  <td width=91 style='width:68pt'></td>
  <td width=96 style='width:72pt'></td>
  <td width=95 style='width:71pt'></td>
  <td width=37 style='width:28pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
 </tr>
 <![endif]>
</table>

</div>


<!----------------------------->
<!--END OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD-->
<!----------------------------->
</body>

</html>
