<div class="col-sm-12">
	<!-- BEGIN SAMPLE TABLE PORTLET-->
	<div class="panel panel-white" id="panel4">
		<div class="panel-heading">
			<h4 class="panel-title text-primary"><i class="icon-plus"></i> สร้างข้อมูลใหม่</h4>
			<div class="panel-tools">
				
				<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>	
			</div>
		</div>
		<div class="panel-body">


			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
			<form method="post" class="form-vertical">
				<fieldset>
					<legend>ผู้จัดจำหน่าย</legend>
					<div class="form-group">
						<label  >Name of Shiper * :</label>
						<div class="controls">
							<select name="supplier_id" id="supplier_id" class="js-example-basic-single js-states form-control" data-placeholder="เลือก SUPPLIER">
								<option value=""></option>
							   <?php foreach($this->bolt_stock->getSupplier() as $rs){ ?>
						       <option value="<?php echo $rs['supplier_id']; ?>" <?php if(set_value("supplier_id")==$rs['supplier_id']){ ?> selected="selected" <?php } ?>><?php echo $rs['supplier_name']; ?></option>
						       <?php } ?>
							</select>
						</div>
				</div>

					<div class="form-group">
						<label  for="income_perchase_order_no">Invoice NO * :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_invoice_no" id="income_invoice_no" type="text" placeholder="" value="<?php echo set_value('income_invoice_no'); ?>" />	
							</div>
						
						</div>
					</div>
					
					<div class="form-group">
						<label  for="income_purchase_no">Purchase Order NO * :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_purchase_no" id="income_purchase_no" type="text" placeholder="" value="<?php echo set_value('income_purchase_no'); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_perchase_order_no">Material :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_material" id="income_material" type="text" placeholder="" value="<?php echo set_value('income_material'); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_perchase_order_no">Commodity :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_commodity" id="income_commodity" type="text" placeholder="" value="<?php echo set_value('income_commodity'); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_perchase_order_no">Certificate NO  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_certificate_no" id="income_certificate_no" type="text" placeholder="" value="<?php echo set_value('income_certificate_no'); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_perchase_order_no">P/O Number  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_po_no" id="income_po_no" type="text" placeholder="" value="<?php echo set_value('income_po_no'); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_perchase_order_no">Delivery NO  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_delivery_no" id="income_delivery_no" type="text" placeholder="" value="<?php echo set_value('income_delivery_no'); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_perchase_order_no">Heat NO  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_heat_no" id="income_heat_no" type="text" placeholder="" value="<?php echo set_value('income_heat_no'); ?>" />	
							</div>
						
						</div>
					</div>
                    <div class="form-group">
						<label  for="income_perchase_order_no">Lot NO  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_lot_no" id="income_lot_no" type="text" placeholder="" value="<?php echo set_value('income_lot_no'); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_perchase_order_no">Grade Mark  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="grade_mark" id="grade_mark" type="text" placeholder="" value="<?php echo set_value('grade_mark'); ?>" />	
							</div>
						
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend>สินค้า</legend>
					<div class="form-group">
						<label  >BOLT :</label>
						<div class="controls">
							<select name="product_id" id="product_id" class="js-example-basic-single js-states form-control" data-placeholder="เลือก BOLT">
								<option value=""></option>
							   <?php foreach($this->bolt_stock->getProduct() as $rs){ ?>
							  
						       <option value="<?php echo $rs['product_id']; ?>" <?php if(set_value("product_id")==$rs['product_id']){ ?> selected="selected" <?php } ?>><?php echo $rs['product_name']." ".$rs['product_type']; ?></option>
						       <?php } ?>
							</select>
						</div>
					</div>
                    
					<div class="form-group">
						<label  >ขนาด :</label>
						<div class="controls">
							<select name="size_id" id="size_id" class="js-example-basic-single js-states form-control" data-placeholder="เลือก Size">
								<option value=""></option>
							</select>
						</div>
					</div>
					
					
<div class="form-group">
						<label  for="income_perchase_order_no">Quantity :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="quantity" id="quantity" type="text" placeholder="" value="<?php echo set_value('quantity'); ?>" />
							</div>
						
						</div>
					</div>

				</fieldset>
				<div class="form-actions">
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-warning" href="<?php echo admin_url($this->router->fetch_class() . "/bolt_list"); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			 </form>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	
	$('#product_id').change(function(){
		var boltid = parseInt($('#product_id').val());
		$.post('<?php echo site_url('income_manager/get_size_bolt_byid'); ?>', { bolt_id : boltid }, function(data){
			$('#size_id').html(data);
		});
	});
});	
</script>