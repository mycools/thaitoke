<div class="span12">
	<div class="widget">
		<div class="widget-title">
			<h4><i class="icon-plus"></i> สร้างข้อมูลใหม่</h4>
			<span class="tools">
			<a href="javascript:;" class="icon-chevron-down"></a>
			<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>		
			</span>							
		</div>
		<div class="widget-body form">
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
			<form method="post" class="form-vertical">
				<fieldset>
					<legend>ผู้จัดจำหน่าย</legend>
					<div class="form-group">
						<label  >Name of Shiper * :</label>
						<div class="controls">
							<select name="supplier_id" id="supplier_id" class="span4 chosen" data-placeholder="เลือก SUPPLIER">
								<option value=""></option>
							   <?php foreach($this->spring_stock->getSupplier() as $rs){ ?>
						       <option value="<?php echo $rs['supplier_id']; ?>" <?php if(set_value("supplier_id")==$rs['supplier_id']){ ?> selected="selected" <?php } ?>><?php echo $rs['supplier_name']; ?></option>
						       <?php } ?>
							</select>
						</div>
				</div>

					<div class="form-group">
						<label  for="income_perchase_order_no">Invoice NO * :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_invoice_no" id="income_invoice_no" type="text" placeholder="" value="<?php echo set_value('income_invoice_no'); ?>" />	
							</div>
						
						</div>
					</div>
					
					<div class="form-group">
						<label  for="income_purchase_no">Purchase Order NO * :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_purchase_no" id="income_purchase_no" type="text" placeholder="" value="<?php echo set_value('income_purchase_no'); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_perchase_order_no">Material :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_material" id="income_material" type="text" placeholder="" value="<?php echo set_value('income_material'); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_perchase_order_no">Commodity :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_commodity" id="income_commodity" type="text" placeholder="" value="<?php echo set_value('income_commodity'); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_perchase_order_no">Certificate NO  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_certificate_no" id="income_certificate_no" type="text" placeholder="" value="<?php echo set_value('income_certificate_no'); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_perchase_order_no">P/O Number  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_po_no" id="income_po_no" type="text" placeholder="" value="<?php echo set_value('income_po_no'); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_perchase_order_no">Delivery NO  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_delivery_no" id="income_delivery_no" type="text" placeholder="" value="<?php echo set_value('income_delivery_no'); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_perchase_order_no">Heat NO  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_heat_no" id="income_heat_no" type="text" placeholder="" value="<?php echo set_value('income_heat_no'); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_perchase_order_no">Grade Mark  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="grade_mark" id="grade_mark" type="text" placeholder="" value="<?php echo set_value('grade_mark'); ?>" />	
							</div>
						
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend>สินค้า</legend>
					<div class="form-group">
						<label  >spring :</label>
						<div class="controls">
							<select name="product_id" id="product_id" class="span4" data-placeholder="เลือก spring">
								<option value=""></option>
							   <?php foreach($this->spring_stock->getProduct() as $rs){ ?>
							  
						       <option value="<?php echo $rs['product_id']; ?>" <?php if(set_value("product_id")==$rs['product_id']){ ?> selected="selected" <?php } ?>><?php echo $rs['product_name']." ".$rs['product_type']; ?></option>
						       <?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label  >ขนาด :</label>
						<div class="controls">
							<select name="size_id" id="size_id" class="span4" data-placeholder="เลือก Size">
								<option value=""></option>
							</select>
						</div>
					</div>
					
					
<div class="form-group">
						<label  for="income_perchase_order_no">Quantity :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="quantity" id="quantity" type="text" placeholder="" value="<?php echo set_value('quantity'); ?>" />
							</div>
						
						</div>
					</div>

				</fieldset>
				<div class="form-actions">
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-mini" href="<?php echo admin_url($this->router->fetch_class() . "/spring_list"); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			 </form>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	
	$('#product_id').change(function(){
		var springid = parseInt($('#product_id').val());
		$.post('<?php echo site_url('income_manager/get_size_spring_byid'); ?>', { spring_id : springid }, function(data){
			$('#size_id').html(data);
		});
	});
});	
</script>