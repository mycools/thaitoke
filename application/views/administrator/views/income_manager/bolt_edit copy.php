<div class="span12">
	<div class="widget">
		<div class="widget-title">
			<h4><i class="icon-plus"></i> สร้างข้อมูลใหม่</h4>
			<span class="tools">
			<a href="javascript:;" class="icon-chevron-down"></a>
			<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>		
			</span>							
		</div>
		<div class="widget-body form">
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
			<form method="post" class="form-vertical">
				<input name="income_id" id="income_id" type="hidden" value="<?php echo set_value('income_id',$this->_data['row']['income_id']); ?>" />
				<fieldset>
					<legend>บริษัท</legend>
					<div class="form-group">
						<label  for="income_job_id">JOB ID * :</label>
						<div class="controls">
							<div class="input-prepend">
							   <span class="add-on"><i class="icon-code"></i></span>
							   <input class="form-control input-md" name="income_job_id" id="income_job_id" type="text" placeholder="" value="<?php echo set_value('income_job_id',$this->_data['row']['income_job_id']); ?>" />	
							</div>
						
						</div>
					</div>
					
					<div class="form-group">
						<label  for="income_inv_no">INVOICE NO * :</label>
						<div class="controls">
							<div class="input-prepend">
							   <span class="add-on"><i class="icon-code"></i></span>
							   <input class="form-control input-md" name="income_inv_no" id="income_inv_no" type="text" placeholder="" value="<?php echo set_value('income_inv_no',$this->_data['row']['income_inv_no']); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_po_no">P/O NUMBER * :</label>
						<div class="controls">
							<div class="input-prepend">
							   <span class="add-on"><i class="icon-code"></i></span>
							   <input class="form-control input-md" name="income_po_no" id="income_po_no" type="text" placeholder="" value="<?php echo set_value('income_po_no',$this->_data['row']['income_po_no']); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_delivery_no">DELIVERY NO * :</label>
						<div class="controls">
							<div class="input-prepend">
							   <span class="add-on"><i class="icon-code"></i></span>
							   <input class="form-control input-md" name="income_delivery_no" id="income_delivery_no" type="text" placeholder="" value="<?php echo set_value('income_delivery_no',$this->_data['row']['income_delivery_no']); ?>" />	
							</div>
						
						</div>
					</div>
					
				</fieldset>
				<fieldset>
					<legend>ลูกค้า</legend>
					<div class="form-group">
						<label  for="income_sale_order_no">SALE ORDER NO * :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_sale_order_no" id="income_sale_order_no" type="text" placeholder="" value="<?php echo set_value('income_sale_order_no',$this->_data['row']['income_sale_order_no']); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_perchase_order_no">PURCHASE ORDER NO * :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_perchase_order_no" id="income_perchase_order_no" type="text" placeholder="" value="<?php echo set_value('income_perchase_order_no',$this->_data['row']['income_perchase_order_no']); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  >CUSTOMER * :</label>
						<div class="controls">
							<select name="income_customer_no" id="income_customer_no" class="span4 chosen" data-placeholder="เลือกลูกค้า">
								<option value=""></option>
							   <?php foreach($this->bolt_income->getCustomer() as $rs){ ?>
						       <option value="<?php echo $rs['customer_id']; ?>" <?php if(set_value("income_customer_no",$this->_data['row']['income_customer_no'])==$rs['customer_id']){ ?> selected="selected" <?php } ?>><?php echo $rs['customer_name']; ?></option>
						       <?php } ?>
							</select>
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend>สินค้า</legend>
					<div class="form-group">
						<label  >BOLT * :</label>
						<div class="controls">
							<input type="checkbox" checked="checked" disabled="disabled" /> 
							<select name="income_bolt_lot_id" id="income_bolt_lot_id" class="span5" data-placeholder="SELECT LOT NO.">
							   <option value="">SELECT LOT NO.</option>
							   <?php foreach($this->bolt_income->getBoltAvailableStock(true) as $rs){ ?>
						       <option value="<?php echo $rs['lot_id']; ?>" <?php if(set_value("income_bolt_lot_id",$this->_data['row']['income_bolt_lot_id'])==$rs['lot_id']){ ?> selected="selected" <?php } ?> data-remain="<?php echo $rs['lot_remain']+$this->_data['row']['income_bolt_qty']; ?>">LOT No. <?php echo $rs['lot_no']; ?> คงเหลือ <?php echo number_format($rs['lot_remain']+$this->_data['row']['income_bolt_qty']); ?> หน่วย</option>
						       <?php } ?>
							</select>
							ระบุจำนวน : <input type="text" class="span2" name="income_bolt_qty" id="income_bolt_qty" value="<?php echo set_value('income_bolt_qty',$this->_data['row']['income_bolt_qty']); ?>" placeholder="จำนวน" style="text-align:right;" />
							
						</div>
					</div>
					
					<div class="form-group">
						<label  >NUT * :</label>
						<div class="controls">
							<input type="checkbox" name="income_nut_include" id="income_nut_include" value="yes" <?php if(set_value("income_nut_include",$this->_data['row']['income_nut_include'])=="yes"){ ?> checked="checked" <?php } ?> /> 
							<select name="income_nut_lot_id" id="income_nut_lot_id" class="span5 group_nut" data-placeholder="SELECT LOT NO." disabled="disabled"> 
								<option value="">SELECT LOT NO.</option>
							   <?php foreach($this->bolt_income->getNutAvailableStock(true) as $rs){ ?>
						       <option value="<?php echo $rs['lot_id']; ?>" <?php if(set_value("income_nut_lot_id",$this->_data['row']['income_nut_lot_id'])==$rs['lot_id']){ ?> selected="selected" <?php } ?> data-remain="<?php echo $rs['lot_remain']+$this->_data['row']['income_nut_qty']; ?>">LOT No. <?php echo $rs['lot_no']; ?> คงเหลือ <?php echo number_format($rs['lot_remain']+$this->_data['row']['income_nut_qty']); ?> หน่วย</option>
						       <?php } ?>
							</select>
							ระบุจำนวน : <input type="text" class="span2 group_nut" name="income_nut_qty" id="income_nut_qty" value="<?php echo set_value('income_nut_qty',$this->_data['row']['income_nut_qty']); ?>" placeholder="จำนวน" style="text-align:right;" disabled="disabled" />
						</div>
					</div>
					<div class="form-group">
						<label  >WASHER * :</label>
						<div class="controls">
							<input type="checkbox" name="income_washer_include" id="income_washer_include" value="yes" <?php if(set_value("income_nut_include",$this->_data['row']['income_nut_include'])=="yes"){ ?> checked="checked" <?php } ?> /> 
							<select name="income_washer_lot_id" id="income_washer_lot_id" class="span5 group_washer" data-placeholder="SELECT LOT NO." disabled="disabled"> 
								<option value="">SELECT LOT NO.</option>
							   <?php foreach($this->bolt_income->getWasherAvailableStock(true) as $rs){ ?>
						       <option value="<?php echo $rs['lot_id']; ?>" <?php if(set_value("income_washer_lot_id",$this->_data['row']['income_washer_lot_id'])==$rs['lot_id']){ ?> selected="selected" <?php } ?> data-remain="<?php echo $rs['lot_remain']+$this->_data['row']['income_washer_qty']; ?>">LOT No. <?php echo $rs['lot_no']; ?> คงเหลือ <?php echo number_format($rs['lot_remain']+$this->_data['row']['income_washer_qty']); ?> หน่วย</option>
						       <?php } ?>
							</select>
							ระบุจำนวน : <input type="text" class="span2 group_washer" name="income_washer_qty" id="income_washer_qty" value="<?php echo set_value('income_washer_qty',$this->_data['row']['income_washer_qty']); ?>" placeholder="จำนวน" style="text-align:right;" disabled="disabled" />
						</div>
					</div>
				</fieldset>
				<div class="form-actions">
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-mini" href="<?php echo admin_url($this->router->fetch_class() . "/bolt_list"); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			 </form>
		</div>
	</div>
</div>
<script type="text/javascript">
var income_bolt_include = <?php echo (set_value('income_bolt_include',$this->_data['row']['income_bolt_include'])=="yes")?"true":"false"; ?>;
var income_nut_include = <?php echo (set_value('income_nut_include',$this->_data['row']['income_nut_include'])=="yes")?"true":"false"; ?>;
var income_washer_include = <?php echo (set_value('income_washer_include',$this->_data['row']['income_washer_include'])=="yes")?"true":"false"; ?>;
var income_bolt_lot_id = <?php echo (set_value('income_bolt_lot_id',$this->_data['row']['income_bolt_lot_id'])>0)?"true":"false"; ?>;
var income_nut_lot_id = <?php echo (set_value('income_nut_lot_id',$this->_data['row']['income_nut_lot_id'])>0)?"true":"false"; ?>;
var income_washer_lot_id = <?php echo (set_value('income_washer_lot_id',$this->_data['row']['income_washer_lot_id'])>0)?"true":"false"; ?>;
$(document).ready(function(){
	$(".group_nut").attr("checked",income_nut_include);
	$(".group_nut").attr("disabled",!income_nut_include);
	
	$(".group_washer").attr("checked",income_washer_include);
	$(".group_washer").attr("disabled",!income_washer_include);
	
	$("#income_bolt_qty").attr("disabled",!income_bolt_lot_id);
	$("#income_nut_qty").attr("disabled",!income_nut_lot_id);
	$("#income_washer_qty").attr("disabled",!income_washer_lot_id);
	
	$("#income_nut_include").val("yes").change(income_nut_include_change);
	$("#income_washer_include").val("yes").change(income_washer_include_change);
	
	$("#income_nut_include").val("yes").change(function(){
		if($(this).is(":checked")){
			$(".group_nut").attr("disabled",false);
		}else{
			$(".group_nut").attr("disabled",true);
		}
	});
	$("#income_washer_include").val("yes").change(function(){
		if($(this).is(":checked")){
			$(".group_washer").attr("disabled",false);
		}else{
			$(".group_washer").attr("disabled",true);
		}
	});
	$("#income_bolt_lot_id").change(function(){
		var remain = $("#income_bolt_lot_id option:selected").attr("data-remain");
		$("#income_bolt_qty").val("0");
		$("#income_bolt_qty").attr("data-maxqty",remain);
		if($(this).val() < 1){
			$("#income_bolt_qty").attr("disabled",true);
		}else{
			$("#income_bolt_qty").attr("disabled",false);
		}
	});
	
	$("#income_bolt_qty").change(function(){
		var remain = parseInt($(this).attr("data-maxqty"));
		var val = parseInt($(this).val());
		if(val > remain){
			$("#income_bolt_qty").val("0");
			alert("จำนวน BOLT ไม่เพียงพอ คงเหลือ "+remain+" หน่วย.");
		}
	});
	
	$("#income_nut_lot_id").change(function(){
		var remain = $("#income_nut_lot_id option:selected").attr("data-remain");
		$("#income_nut_qty").val("0");
		$("#income_nut_qty").attr("data-maxqty",remain);
		if($(this).val() < 1){
			$("#income_nut_qty").attr("disabled",true);
		}else{
			$("#income_nut_qty").attr("disabled",false);
		}
	});
	
	$("#income_nut_qty").change(function(){
		var remain = parseInt($(this).attr("data-maxqty"));
		var val = parseInt($(this).val());
		if(val > remain){
			$("#income_nut_qty").val("0");
			alert("จำนวน NUT ไม่เพียงพอ คงเหลือ "+remain+" หน่วย.");
		}
	});
	
	$("#income_washer_lot_id").change(function(){
		var remain = $("#income_washer_lot_id option:selected").attr("data-remain");
		$("#income_washer_qty").val("0");
		$("#income_washer_qty").attr("data-maxqty",remain);
		if($(this).val() < 1){
			$("#income_washer_qty").attr("disabled",true);
		}else{
			$("#income_washer_qty").attr("disabled",false);
		}
	});
	
	$("#income_washer_qty").change(function(){
		var remain = parseInt($(this).attr("data-maxqty"));
		var val = parseInt($(this).val());
		if(val > remain){
			$("#income_washer_qty").val("0");
			alert("จำนวน WASHER ไม่เพียงพอ คงเหลือ "+remain+" หน่วย.");
		}
	});
	
});
function income_nut_include_change(){
	if($("#income_nut_include").is(":checked")){
		$(".group_nut").attr("disabled",false);
	}else{
		$(".group_nut").attr("disabled",true);
	}
}
function income_washer_include_change(){
	if($("#income_washer_include").is(":checked")){
		$(".group_washer").attr("disabled",false);
	}else{
		$(".group_washer").attr("disabled",true);
	}
}
</script>