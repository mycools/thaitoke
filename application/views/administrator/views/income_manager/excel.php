<div class="span12">
	<div class="widget">
		<div class="widget-title">
			<h4><i class="icon-excel"></i> Excel Editor</h4>
			<span class="tools">
			<a href="javascript:;" class="icon-chevron-down"></a>
			<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>		
			</span>							
		</div>
		<div class="widget-body form">
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
			<form method="post" class="form-vertical">
			<div class="form-group">
				<div class="excel_viewer">
					<table class="excel">
						<thead>
							<tr class="excel-column">
								<th class="excel-rowid">&nbsp;</th>
								<?php foreach (range('A', 'Z') as $char) { ?>
								<th class="excel-rowcolumn"><?php echo $char; ?></th>
								<?php } ?>
							</tr>
						</thead>
						<tbody>
							<?php for($row=1;$row<20;$row++){ ?>
							<tr class="excel-row">
								<td class="excel-rowid"><?php echo $row; ?></td>
								<?php foreach (range('A', 'Z') as $char) { ?>
								<td class="col-<?php echo $char; ?> row-<?php echo $row; ?> <?php echo $char; ?><?php echo $row; ?>">
									<input type="text" class="excel-input span12" />
								</td>
								<?php } ?>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
			
			<div class="form-actions">
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-mini" href="<?php echo admin_url($this->router->fetch_class() . "/index"); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			 </form>
		</div>
	</div>
</div>
<style type="text/css">
.excel_viewer{
	overflow: auto;
	border: solid 1px #e0e0e0;
	min-height: 400px;
	max-height: 700px;
	width: 100%;
}
table.excel{
	font-size: 14px;
	font-weight: bold;
}
table.excel thead tr.excel-column{
	background-color:#e7e7e7; 
}
table.excel thead tr.excel-column th{
	min-width: 100px;
	background-color:#e7e7e7;
	font-weight: bolder;
	text-align: center;
	border: solid 1px #969696;
	
}
table.excel thead tr.excel-column th.excel-rowid{
	min-width: 50px;
	overflow: visible;
	background-color: #b3b3b3;
}
table.excel tbody tr.excel-row td{
	border: solid 1px #cecece;
	background-color: #ffffff;
	text-overflow: ellipsis;
	white-space: nowrap;
}
table.excel tbody tr.excel-row td.excel-rowid{
	min-width: 50px;
	background-color:#e7e7e7;
	font-weight: bolder;
	text-align: center;
	border: solid 1px #969696;
}
table.excel tbody tr.excel-row td .excel-input{
	border: none;
	font-size: 12px;
	min-height: 20px;
}
</style>