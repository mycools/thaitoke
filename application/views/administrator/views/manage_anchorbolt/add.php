<div class="span12">
	<div class="widget">
		<div class="widget-title">
			<h4><i class="icon-cogs"></i>เพิ่ม Anchor Bolt</h4>
			<span class="tools">
			<a href="javascript:;" class="icon-chevron-down"></a>
			<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>		
			</span>							
		</div>
		<div class="widget-body form">
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
				
			<form method="post" class="form-vertical" enctype="multipart/form-data">
				<div class="form-group">
					<label  for="product_thumbnail">รูปภาพ : </label>
					<div class="controls">
						<input type="file" name="product_thumbnail" id="product_thumbnail" />
						<p class="help-block">เว้นว่างไว้หากไม่ต้องการเปลี่ยน ขนาดไฟล์ไม่เกิน 1Mb</p>
					</div>
				</div>
				<div class="form-group">
					<label  for="product_grade">ชื่อเกรด : *</label>
					<div class="controls">
						<input type="text" name="product_grade" id="product_grade" value="<?php echo set_value('product_grade'); ?>" class="form-control input-md" />
					</div>
				</div>
				<div class="form-group">
					<label  for="product_low_carbon">Low Carbon : </label>
					<div class="controls">
						<input type="checkbox" name="product_low_carbon" id="product_low_carbon" value="1" <?php echo set_checkbox('product_low_carbon','1'); ?> />
					</div>
				</div>
				<div class="form-actions">
					<input type="hidden" name="product_type" id="product_type" value="<?php echo $type; ?>" />
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-mini" href="<?php echo admin_url("manage_anchorbolt/index/".$type); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			</form>
		</div>
	</div>
</div>
<style type="text/css">
#menu_icon_chzn{
	font-family: 'FontAwesome',Tahoma;
}
</style>