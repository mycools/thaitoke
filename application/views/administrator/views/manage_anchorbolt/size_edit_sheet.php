<div class="span12">
	<div class="widget">
		<div class="widget-title">
			<h4><i class="icon-cogs"></i>แก้ไข Anchor Bolt Size</h4>
			<span class="tools">
			<a href="javascript:;" class="icon-chevron-down"></a>
			<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>		
			</span>							
		</div>
		<div class="widget-body form">
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
				
			<form method="post" class="form-vertical"  enctype="multipart/form-data">
				<div class="form-group">
					<label  for="size_t">T : </label>
					<div class="controls">
						<input type="text" name="size_t" id="size_t" value="<?php echo set_value('size_t', $row['size_t']); ?>" class="form-control input-sm" />
					</div>
				</div>
                <div class="form-group">
					<label  for="size_w">W : </label>
					<div class="controls">
						<input type="text" name="size_w" id="size_w" value="<?php echo set_value('size_w', $row['size_w']); ?>" class="form-control input-sm" />
					</div>
				</div>
				<div class="form-group">
					<label  for="size_length">Length : </label>
					<div class="controls">
						<input type="text" name="size_length" id="size_length" value="<?php echo set_value('size_length', $row['size_length']); ?>" class="form-control input-sm" />
					</div>
				</div>
               
				<div class="form-actions">
					<input type="hidden" name="product_id" id="product_id" value="<?php echo $row['product_id']; ?>" />
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-mini" href="<?php echo admin_url("manage_anchorbolt/size_list/".$type."/".$row['product_id']); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			</form>
			
		</div>
	</div>
</div>