<div class="span12"> 
  <!-- BEGIN RECENT ORDERS PORTLET-->
  <div class="widget">
    <div class="widget-title widget-user">
      <h4><i class="icon-list-alt"></i> Taper taper product final testing [<?php echo $final_info['final_invoice_no']; ?>]</h4>  
      <span class="tools">
      	
      </span>
      </div>
    <div class="widget-body form">
    <?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
    	<form method="post" name="menu_form" id="menu_form" enctype="multipart/form-data" class="form-vertical">
    	<input type="hidden" name="income_id" value="<?php echo $income_id;?>" />
    	
          <?php if(@$success_message!=NULL){ ?>
      <div class="alert alert-success"> 
        <button class="close" data-dismiss="alert">×</button>
        <strong>Success !</strong> <?php echo $success_message; ?>
      </div>
      <?php } ?>

        <?php echo @$validation_errors; ?>
            <?php if(@$error_message!=NULL){ ?>
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">×</button>
                    <strong>Error !</strong> <?php echo $error_message; ?>
                </div>
            <?php }?>
			

			           
            <div class="form-group">
                <label ></label>
                <div class="controls" style="margin-left:0 !important;">
                        
                <table width="100%" border="1" class="table table-striped table-bordered">
                	<thead>
                	<tr><th colspan="14">Standard Test</th></tr>
	                <tr>
	                	<th width="10" rowspan="2">No.</th>
	                	<th rowspan="2">Description</th>
	                	<th rowspan="2">Standard</th>
	                	<?php /*?><th rowspan="2">Inspection tool</th>
	                	<th rowspan="2">Frequency of test</th>
	                	<th rowspan="2">Inspection quantity</th><?php */?>
	                	<th width="60" rowspan="2">1</th>
	                	<th width="60" rowspan="2">2</th>
	                	<th width="60" rowspan="2">3</th>
	                	<th width="60" rowspan="2">4</th>
	                	<th width="60" rowspan="2">5</th>
	                	<th colspan="2">Overall result</th>
	                	<th width="150" rowspan="2">Remark</th>
	                	
	                </tr>
                    <tr>
	                	
	                	<th width="40">Pass</th>
	                	<th width="40">No</th>
	            
	                	
	                </tr>
                	</thead>
                	<tbody>
	                <?PHP	$i = 0; 
                            foreach( $std_list->result_array() AS $row ){  
                            $final = $this->final_test_model->get_final_taper_test_info($final_info['final_id'],$row['ps_id']);
                            
                            ?>
	                <tr>
		                <td align="center"><?php echo $i+1;?><input type="hidden" name="taper_testing_id[]" value="<?php echo $final['taper_testing_id'];?>" /><input type="hidden" name="taper_standard_id[]" value="<?php echo $row['ps_id'];?>" /></td>
		                <td align="center"><?php echo $row['standard_description'];?></td>
		                <td align="center"><?php echo $row['ps_min']. $row['ps_min_unit']."<br/> ".$row['ps_max']. $row['ps_max_unit'];?></td>
		                <?php /*?><td align="center"><?php $ins = $this->final_test_model->get_inspection($row['inspection_id']);  echo $ins['inspection_name']; ?></td>
		                <td align="center">Before Delivery</td>
		                <td align="center">5pcs/lot</td><?php */?>
		                <td class="center"><input class="span12" name="test1[]" id="test1[]" type="text" value="<?php echo set_value('test1[]',$final['taper_testing1']); ?>" /></td>
		                <td class="center"><input class="span12" name="test2[]" id="test2[]" type="text" value="<?php echo set_value('test2[]',$final['taper_testing2']); ?>" /></td>
		                <td class="center"><input class="span12" name="test3[]" id="test3[]" type="text" value="<?php echo set_value('test3[]',$final['taper_testing3']); ?>" /></td>
		                <td class="center"><input class="span12" name="test4[]" id="test4[]" type="text" value="<?php echo set_value('test4[]',$final['taper_testing4']); ?>" /></td>
		                <td class="center"><input class="span12" name="test5[]" id="test5[]" type="text" value="<?php echo set_value('test5[]',$final['taper_testing5']); ?>" /></td>
		                <td class="center">
		                	<input name="testing_status[<?php echo $i; ?>]" <?php if($final['taper_testing_status']=="pass"){ echo "checked"; } ?> value="pass" type="radio" style="margin-left:0 !important;" />
		                </td>
		                <td class="center">
		                	<input name="testing_status[<?php echo $i; ?>]" <?php if($final['taper_testing_status']=="not_pass"){ echo "checked"; } ?> value="not_pass" type="radio" style="margin-left:0 !important;" />
		                </td>
		                <td><input class="input span12" name="remark[]" id="remark[]" type="text" value="<?php echo set_value('remark[]',$final['taper_testing_remark']); ?>" /></td>
	                </tr>
	                 <?php
                         $i = $i+1;   }
                        ?>
                	</tbody>
                	<thead>
                	 <tr><th colspan="14">Size Test</th></tr>
	                <tr>
	                	<th width="10" rowspan="2">No.</th>
	                	<th rowspan="2">Description</th>
	                	<th rowspan="2">Standard</th>
	                	<?php /*?><th rowspan="2">Inspection tool</th>
	                	<th rowspan="2">Frequency of test</th>
	                	<th rowspan="2">Inspection quantity</th><?php */?>
	                	<th width="30" rowspan="2">1</th>
	                	<th width="30" rowspan="2">2</th>
	                	<th width="30" rowspan="2">3</th>
	                	<th width="30" rowspan="2">4</th>
	                	<th width="30" rowspan="2">5</th>
	                	<th colspan="2">Overall result</th>
	                	<th width="150" rowspan="2">Remark</th>
	                	
	                </tr>
                    <tr>
	                	
	                	<th width="40">Pass</th>
	                	<th width="40">No</th>
	            
	                	
	                </tr>
                	</thead>
                	<tbody>
	                <?PHP	$i = 0; 
                            foreach( $cer_size_list->result_array() AS $row1 ){ 
                           
                            $final_size = $this->final_test_model->get_final_taper_size_test_info($final_info['final_id'],$row1['ps_id']);
 
                            ?>
	                <tr>
		                <td align="center"><?php echo $i+1;?><input type="hidden" name="taper_testing_final_id[]" value="<?php echo $final_size['taper_testing_id'];?>" /><input type="hidden" name="taper_cer_size_id[]" value="<?php echo $row1['ps_id'];?>" /></td>
		                <td align="center"><?php echo $row1['ps_description'];?></td>
		                <td align="center"><?php echo $row1['ps_min']. $row1['ps_min_unit']."<br/> ".$row1['ps_max']. $row1['ps_max_unit'];?></td>
		               <?php /*?> <td align="center"><?php $ins1 = $this->final_test_model->get_inspection($row1['inspection_id']);  echo $ins1['inspection_name']; ?></td>
		                <td align="center">Before Delivery</td>
		                <td align="center">5pcs/lot</td><?php */?>
		                <td class="center"><input class="span12" name="ctest1[]" id="ctest1[]" type="text" value="<?php echo set_value('ctest1[]',$final_size['taper_testing1']); ?>" /></td>
		                <td class="center"><input class="span12" name="ctest2[]" id="ctest2[]" type="text" value="<?php echo set_value('ctest2[]',$final_size['taper_testing2']); ?>" /></td>
		                <td class="center"><input class="span12" name="ctest3[]" id="ctest3[]" type="text" value="<?php echo set_value('ctest3[]',$final_size['taper_testing3']); ?>" /></td>
		                <td class="center"><input class="span12" name="ctest4[]" id="ctest4[]" type="text" value="<?php echo set_value('ctest4[]',$final_size['taper_testing4']); ?>" /></td>
		                <td class="center"><input class="span12" name="ctest5[]" id="ctest5[]" type="text" value="<?php echo set_value('ctest5[]',$final_size['taper_testing5']); ?>" /></td>
		                <td class="center">
		                	<input name="ctesting_status[<?php echo $i; ?>]" value="pass" <?php if($final_size['taper_testing_status']=="pass"){ echo "checked"; } ?> type="radio" style="margin-left:0 !important;" />
		                </td>
		                <td class="center">
		                	<input name="ctesting_status[<?php echo $i; ?>]" value="not_pass" <?php if($final_size['taper_testing_status']=="not_pass"){ echo "checked"; } ?> type="radio" style="margin-left:0 !important;" />
		                </td>
		                <td><input class="input span12" name="cremark[]" id="cremark[]" type="text" value="<?php echo set_value('cremark[]',$final_size['taper_testing_remark']); ?>" /></td>
	                </tr>
	                 <?php
                         $i = $i+1;   }
                        ?>
					</tbody>
                </table>                
                           
                 
                 </div>
            </div>
            

            <div class="form-actions">
                <button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> Save &amp Chemical  Testing</button>
               <?php if($this->uri->segment('2') =="edit_final_main_taper_testing"){?><a class="btn btn-mini" href="<?php echo admin_url("final_manager/final_taper_list"); ?>"><?php }else{?><a class="btn btn-mini" href="<?php echo admin_url("final_manager/final_bolt_list"); ?>"><?php }?><i class="icon-reply"></i> ยกเลิก</a>
            </div>
    	</form>
    	
    </div>
  </div>
  <!-- END RECENT ORDERS PORTLET--> 
</div>
<style>
.table thead th{
	text-align: center;
}
.center{
	text-align: center;
	vertical-align: middle;
}
</style>