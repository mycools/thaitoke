<div class="span12">
	<div class="widget">
		<div class="widget-title">
			<h4><i class="icon-cogs"></i>ทดสอบค่า Chemical ของ Spring Washer</h4>
			<span class="tools">
			<a href="javascript:;" class="icon-chevron-down"></a>
			<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>		
			</span>							
		</div>
		<div class="widget-body form">
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
				
			<form method="post" class="form-vertical">
				<table class="table table-striped table-bordered table-advance table-hover"> 
					<tr>
						<?php
						if($row){
							foreach($row as $r){
								?>
								<th><?php echo $r['chemical_value']; ?></th>
								<?php
							}
						}
						?>
					</tr>
					<tr>
						<?php
						if($row){
							foreach($row as $r){
								?>
								<th><?php echo 'MIN : '.$r['ps_min'].'<br />'.'MAX : '.$r['ps_max']; ?></th>
								<?php
							}
						}
						?>
					</tr>
					<tr>
						<?php
						if($row){
							foreach($row as $r){
								if($tested_status<=0){
									$testvalue = $this->chemical_test_final_model->get_spring_untest_value($r['income_id'], $r['chemical_id']);
								}else{
									$testvalue = $this->chemical_test_final_model->get_spring_test_value($r['income_id'], $this->uri->segment(4), $r['chemical_id']);
								}
								?>
								<th>
									<input type="text" name="test_value[]" value="<?php echo set_value('test_value', @$testvalue['test_value']); ?>" class="form-control input-sm" />
									<input type="hidden" name="test_id[]" value="<?php echo @$testvalue['test_id']; ?>" />
									<input type="hidden" name="chemical_id[]" value="<?php echo @$testvalue['chemical_id']; ?>" />
									<input type="hidden" name="chemical_value[]" value="<?php echo @$testvalue['chemical_value']; ?>" />
									<input type="hidden" name="income_id[]" value="<?php echo @$testvalue['income_id']; ?>" />
								</th>
								<?php
							}
						}
						?>
					</tr>
				</table><br>
				<table class="table table-striped table-bordered table-advance table-hover">
					<tr>
						<td>THICKNESS GALVANIZED</td>
						<td>STANDARD GALVANIZED</td>
						<td></td>
					</tr>
					<tr>
						<td>
							<select name="thickness_id" id="thickness_id">
								<option value="">เลือก Thickness Galvarnized</option>
								<?php
								if($thicknesslist){
									foreach($thicknesslist as $tlist){
										?>
										<option value="<?php echo $tlist['id']; ?>" <?php echo set_select('thickness_id', $tlist['id'], $tlist['id']==$testthicknessinfo['thickness_id']); ?>><?php echo $tlist['name']; ?></option>
										<?php
									}
								}
								?>
							</select>
						</td>
						<td><span id="thickness-condition-value">&nbsp;</span></td>
						<td><input type="text" name="thickness_value" value="<?php echo set_value('thickness_value', $testthicknessinfo['thickness_value']); ?>" /></td>
						<input type="hidden" name="thickness_test_id" id="thickness_test_id" value="<?php echo $testthicknessinfo['thickness_test_id']; ?>" />
					</tr>
				</table>
				<p>
					<label><b>สถานะการทดสอบ : </b></label>
					<label for="test_status_pass"><input type="radio" name="test_status" id="test_status_pass" value="2" /> ผ่าน</label>
					<label for="test_status_failed"><input type="radio" name="test_status" id="test_status_failed" value="1" /> ไม่ผ่าน</label>
					<label for="test_status_undefied"><input type="radio" name="test_status" id="test_status_undefied" value="0" /> ยังไม่ได้ทดสอบ</label>
				</p>
				<p style="clear:both;">&nbsp;</p>
				<div class="form-actions">
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-mini" href="<?php echo admin_url("final_manager/final_bolt_list"); ?>"><i class="icon-reply"></i> ยกเลิก</a>
				</div>
			 </form>
		</div>
	</div>
</div>
<style type="text/css">
#menu_icon_chzn{
	font-family: 'FontAwesome',Tahoma;
}
</style>

<script type="text/javascript">
$(document).ready(function(){

	$('#thickness_id').change(function(){
		$.post('<?php echo site_url('chemical_test/get_thickness_value'); ?>', { thickness_id : $(this).val() }, function(data){
			if(data){
				$('#thickness-condition-value').html(data);
			}
		});
	});
});
</script>