<div class="span12">
	<div class="widget">
		<div class="widget-title">
			<h4><i class="icon-plus"></i> สร้างข้อมูลใหม่</h4>
			<span class="tools">
			<a href="javascript:;" class="icon-chevron-down"></a>
			<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>		
			</span>							
		</div>
		<div class="widget-body form">
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
			<form method="post" class="form-vertical">
				<fieldset>
					<legend>ผู้จัดจำหน่าย</legend>
					<div class="form-group">
						<label  >Name of Shiper * :</label>
						<div class="controls">
							<select name="supplier_id" id="supplier_id" class="span4 chosen" data-placeholder="เลือก SUPPLIER">
								<option value=""></option>
							   <?php foreach($this->anchor_income->getSupplier() as $rs){ ?>
						       <option value="<?php echo $rs['supplier_id']; ?>" <?php if(set_value("supplier_id")==$rs['supplier_id']){ ?> selected="selected" <?php } ?>><?php echo $rs['supplier_name']; ?></option>
						       <?php } ?>
							</select>
						</div>
				</div>

					<div class="form-group">
						<label  for="income_perchase_order_no">Invoice NO * :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_invoice_no" id="income_invoice_no" type="text" placeholder="" value="<?php echo set_value('income_invoice_no'); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_perchase_order_no">P/O Number  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_po_no" id="income_po_no" type="text" placeholder="" value="<?php echo set_value('income_po_no'); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_perchase_order_no">Test NO  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_test_no" id="income_test_no" type="text" placeholder="" value="<?php echo set_value('income_test_no'); ?>" />	
							</div>
						
						</div>
					</div>
					<div class="form-group">
						<label  for="income_perchase_order_no">Heat NO  :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="income_heat_no" id="income_heat_no" type="text" placeholder="" value="<?php echo set_value('income_heat_no'); ?>" />	
							</div>
						
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend>สินค้า</legend>
					<div class="form-group">
						<label  >anchor :</label>
						<div class="controls">
							<select name="product_id" id="product_id" class="span4" data-placeholder="เลือก anchor">
								<option value=""></option>
							   <?php foreach($this->anchor_income->getProduct($this->_data['type']) as $rs){ ?>
							  
						       <option value="<?php echo $rs['product_id']; ?>" <?php if(set_value("product_id")==$rs['product_id']){ ?> selected="selected" <?php } ?>><?php echo $rs['product_grade']." ".$rs['product_type']; ?></option>
						       <?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label  >ขนาด :</label>
						<div class="controls">
							<select name="size_id" id="size_id" class="span4" data-placeholder="เลือก Size">
								<option value=""></option>
							</select>
						</div>
					</div>
					
					
<div class="form-group">
						<label  for="income_perchase_order_no">Quantity :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="quantity" id="quantity" type="text" placeholder="" value="<?php echo set_value('quantity'); ?>" />
							</div>
						
						</div>
					</div>

				</fieldset>
                <fieldset>
					<legend>Mechanical</legend>
                <table class="table table-striped table-bordered table-advance table-hover"> 
               
					<tr>
						<th style="width:30%;">Description</th>
						<th style="width:35%;">Standard value</th>
						<th style="width:35%;">Result</th>
					</tr>
					<?php
					if($row){
						foreach($row as $index=>$r){
							?>
							<tr>
								<td><input readonly type="text" name="standard_description[<?php echo $index; ?>]" value="<?php echo set_value('standard_description['.$index.']', $r['standard_description']); ?>" class="form-control input-md" /></td>
								<td>
									MIN <input type="text" name="ps_min[<?php echo $index; ?>]" value="<?php echo trim(set_value('ps_min[]', @$r['ps_min'])); ?>" class="span2 numericonly" placeholder="ค่า" /><input type="text" name="ps_min_unit[]" value="<?php echo trim(set_value('ps_min_unit[]', @$r['ps_min_unit'])); ?>" class="span2" placeholder="หน่วย" />
									MAX <input type="text" name="ps_max[<?php echo $index; ?>]" value="<?php echo trim(set_value('ps_max[]', @$r['ps_max'])); ?>" class="span2 numericonly" placeholder="ค่า" /><input type="text" name="ps_max_unit[]" value="<?php echo trim(set_value('ps_max_unit[]', @$r['ps_max_unit'])); ?>" class="span2" placeholder="หน่วย" />
								</td>
								<td>
									<input type="text" name="ps_result[]" value="<?php echo trim(set_value('ps_result[]', @$r['ps_result'])); ?>" class="span2" placeholder="ค่า" />
								</td>
							</tr>
							<input type="hidden" name="ps_id[<?php echo $index; ?>]" value="<?php echo @$r['ps_id']; ?>" />
							<?php
						}
					}
					?>
				</table>
                </fieldset>
				<div class="form-actions">
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-mini" href="<?php echo admin_url($this->router->fetch_class() . "/anchor_list/".$type); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			 </form>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	
	$('#product_id').change(function(){
		var anchorid = parseInt($('#product_id').val());
		var type = '<?php echo $type; ?>';
		$.post('<?php echo site_url('income_manager_anchor/get_size_anchor_byid'); ?>/'+type, { anchor_id : anchorid }, function(data){
			$('#size_id').html(data);
		});
	});
});	
</script>