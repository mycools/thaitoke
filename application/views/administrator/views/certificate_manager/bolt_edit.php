<div class="col-sm-12">
	<!-- BEGIN SAMPLE TABLE PORTLET-->
	<div class="panel panel-white" id="panel4">
		<div class="panel-heading">
			<h4 class="panel-title text-primary"><i class="fa fa-wrench"></i> BOLT CERTIFICATE SETTINGS</h4>
			<div class="panel-tools">
				
				<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>	
			</div>
		</div>
		<div class="panel-body">
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
			<form method="post" >
				<input name="final_id" id="final_id" type="hidden" value="<?php echo set_value('final_id',$this->_data['row']['final_id']); ?>" />

					<div class="form-group">
						<label  for="final_text_set">SET :</label>
						
							   <input class="form-control input-sm" name="final_text_set" id="final_text_set" type="text" placeholder="" value="<?php echo set_value('final_text_set',$this->_data['row']['final_text_set']); ?>" />
					</div>
					<div class="form-group">
						<label  for="final_inspection_no">Inspection No. :</label>
						
							   <input class="form-control input-sm" name="final_inspection_no" id="final_inspection_no" type="text" placeholder="" value="<?php echo set_value('final_inspection_no',$this->_data['row']['final_inspection_no']); ?>" />
					</div>
					<div class="form-group">
						<label  for="final_text_remark">Remark  :</label>
							<textarea name="final_text_remark" id="final_text_remark" cols="5" class="form-control autosize"><?php echo set_value('final_text_set',$this->_data['row']['final_text_remark']); ?></textarea>
						
						
					</div>
					<div class="row">
						<div class="col-sm-4">
							<fieldset>
								<legend>Bolt Settings</legend>
								<div class="form-group">
									<label  for="final_bolt_hardness_n">Hardness n=? :</label>

											<input class="form-control text-center input-sm numberspin" name="final_bolt_hardness_n" id="final_bolt_hardness_n" type="text" placeholder="" value="<?php echo set_value('final_bolt_hardness_n',$this->_data['row']['final_bolt_hardness_n']); ?>" touchspin data-min="0" data-max="100" data-step="1" data-decimals="0" data-boostat="5" data-maxboostedstep="10" />

								</div>
								<div class="form-group">
									<label  for="final_bolt_specimen_tensile_n">Specimen Tensile n=? :</label>

											<input class="form-control text-center input-sm numberspin" name="final_bolt_specimen_tensile_n" id="final_bolt_specimen_tensile_n" type="text" placeholder="" value="<?php echo set_value('final_bolt_specimen_tensile_n',$this->_data['row']['final_bolt_specimen_tensile_n']); ?>" touchspin data-min="0" data-max="100" data-step="1" data-decimals="0" data-boostat="5" data-maxboostedstep="10" />

								</div>	

								<div class="form-group">
									<label  for="proof_load_n">Proof Load n=? :</label>

											<input class="form-control text-center input-sm numberspin" name="final_bolt_proof_load_n" id="final_bolt_proof_load_n" type="text" placeholder="" value="<?php echo set_value('final_bolt_proof_load_n',$this->_data['row']['final_bolt_proof_load_n']); ?>" touchspin data-min="0" data-max="100" data-step="1" data-decimals="0" data-boostat="5" data-maxboostedstep="10" />

								</div>	
								<div class="form-group">
									<label  for="wedge_tensile_load_n">Wedge Tensile Load n=? :</label>

											<input class="form-control text-center input-sm numberspin" name="final_bolt_wedge_tensile_load_n" id="final_bolt_wedge_tensile_load_n" type="text" placeholder="" value="<?php echo set_value('final_bolt_wedge_tensile_load_n',$this->_data['row']['final_bolt_wedge_tensile_load_n']); ?>" touchspin data-min="0" data-max="100" data-step="1" data-decimals="0" data-boostat="5" data-maxboostedstep="10" />

								</div>	
							</fieldset>
					</div>
					<div class="col-sm-4">
						<fieldset>
							<legend>Nut Settings</legend>
								<div class="form-group">
									<label  for="final_nut_hardness_n">Hardness n=? :</label>
											<input class="form-control text-center input-sm numberspin" name="final_nut_hardness_n" id="final_nut_hardness_n" type="text" placeholder="" value="<?php echo set_value('final_nut_hardness_n',$this->_data['row']['final_nut_hardness_n']); ?>" touchspin data-min="0" data-max="100" data-step="1" data-decimals="0" data-boostat="5" data-maxboostedstep="10" />
								</div>
								<div class="form-group">
									<label  for="final_nut_proof_n">Proof n=? :</label>
											<input class="form-control text-center input-sm numberspin" name="final_nut_proof_n" id="final_nut_proof_n" type="text" placeholder="" value="<?php echo set_value('final_nut_proof_n',$this->_data['row']['final_nut_proof_n']); ?>" touchspin data-min="0" data-max="100" data-step="1" data-decimals="0" data-boostat="5" data-maxboostedstep="10" />
								</div>
						</fieldset>
					</div>
					<div class="col-sm-4">
						<fieldset>
							<legend>Washer Settings</legend>
								<div class="form-group">
									<label  for="final_washer_hardness_n">Hardness n=? :</label>
											<input class="form-control text-center input-sm numberspin" name="final_washer_hardness_n" id="final_washer_hardness_n" type="text" placeholder="" value="<?php echo set_value('final_washer_hardness_n',$this->_data['row']['final_washer_hardness_n']); ?>" touchspin data-min="0" data-max="100" data-step="1" data-decimals="0" data-boostat="5" data-maxboostedstep="10" />
								</div>
						</fieldset>
					</div>
				</div>			
				<div class="form-actions">
				 	<button type="submit" class="btn btn-sm btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-warning btn-sm" href="<?php echo admin_url($this->router->fetch_class() . "/certificate_bolt_list"); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			 </form>
		</div>
	</div>
</div>
