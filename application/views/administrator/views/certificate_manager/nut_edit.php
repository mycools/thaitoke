<div class="span12">
	<div class="widget">
		<div class="widget-title">
			<h4><i class="icon-plus"></i> แก้ไขข้อมูล</h4>
			<span class="tools">
			<a href="javascript:;" class="icon-chevron-down"></a>
			<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>		
			</span>							
		</div>
		<div class="widget-body form">
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
			<form method="post" class="form-vertical">
				<input name="final_id" id="final_id" type="hidden" value="<?php echo set_value('final_id',$this->_data['row']['final_id']); ?>" />

					<div class="form-group">
						<label  for="final_text_set">SET :</label>
						<div class="controls">
							<div class="input-prepend">
							   
							   <input class="form-control input-md" name="final_text_set" id="final_text_set" type="text" placeholder="" value="<?php echo set_value('final_text_set',$this->_data['row']['final_text_set']); ?>" />	
							</div>
						
						</div>
					</div>
					
					<div class="form-group">
						<label  for="final_text_remark">Remark  :</label>
						<div class="controls">
							<textarea name="final_text_remark" id="final_text_remark" cols="5" class="span12"><?php echo set_value('final_text_set',$this->_data['row']['final_text_remark']); ?></textarea>
						
						</div>
					</div>
				
											
										
				<div class="form-actions">
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-mini" href="<?php echo admin_url($this->router->fetch_class() . "/certificate_nut_list"); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			 </form>
		</div>
	</div>
</div>
