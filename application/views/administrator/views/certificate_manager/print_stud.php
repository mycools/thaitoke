<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 14">
<link rel=File-List href="CER.files/filelist.xml">
<style id="CER_9739_Styles">
<!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
.font59739
	{color:black;
	font-size:14.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;}
.xl159739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl639739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:16.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl649739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:16.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:left;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl659739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:16.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl669739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:16.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:general;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl679739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:16.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:right;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl689739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:12.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl699739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl709739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:12.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl719739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:13.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:center;
	vertical-align:middle;
	border:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl729739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:16.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl739739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:16.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:right;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl749739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:14.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:center;
	vertical-align:middle;
	border:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl759739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:14.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:center;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl769739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:14.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:general;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl779739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:14.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl789739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:14.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:general;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl799739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:14.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:general;
	vertical-align:top;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl809739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:14.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:center;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl819739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:14.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:left;
	vertical-align:top;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl829739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:14.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl839739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:#E26B0A;
	font-size:18.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl849739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:16.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:left;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl859739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:16.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"Medium Date";
	text-align:left;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl869739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:16.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl879739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:12.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:right;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl889739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:12.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:center;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl899739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:16.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:center;
	vertical-align:middle;
	background:#D9D9D9;
	mso-pattern:black none;
	white-space:nowrap;}
.xl909739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:12.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:left;
	vertical-align:middle;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl919739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:14.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:center;
	vertical-align:bottom;
	border:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl929739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:14.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:center;
	vertical-align:bottom;
	border:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl939739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:12.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:top;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl949739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:14.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:center;
	vertical-align:middle;
	border:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl959739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:14.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:center;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl969739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:14.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:general;
	vertical-align:top;
	border:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl979739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:14.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:general;
	vertical-align:middle;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl989739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:14.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:center;
	vertical-align:middle;
	border:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl999739
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:14.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Angsana New", serif;
	mso-font-charset:0;
	mso-number-format:"\@";
	text-align:general;
	vertical-align:top;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
-->
</style>
</head>

<body>
<!--[if !excel]>&nbsp;&nbsp;<![endif]-->
<!--&#3586;&#3657;&#3629;&#3617;&#3641;&#3621;&#3605;&#3656;&#3629;&#3652;&#3611;&#3609;&#3637;&#3657;&#3606;&#3641;&#3585;&#3626;&#3619;&#3657;&#3634;&#3591;&#3650;&#3604;&#3618;
&#3605;&#3633;&#3623;&#3594;&#3656;&#3623;&#3618;&#3626;&#3619;&#3657;&#3634;&#3591;&#3585;&#3634;&#3619;&#3611;&#3619;&#3632;&#3585;&#3634;&#3624;&#3648;&#3611;&#3655;&#3609;&#3648;&#3623;&#3655;&#3610;&#3648;&#3614;&#3592;&#3586;&#3629;&#3591;
Microsoft Excel-->
<!--&#3606;&#3657;&#3634;&#3619;&#3634;&#3618;&#3585;&#3634;&#3619;&#3648;&#3604;&#3637;&#3618;&#3623;&#3585;&#3633;&#3609;&#3606;&#3641;&#3585;&#3611;&#3619;&#3632;&#3585;&#3634;&#3624;&#3629;&#3637;&#3585;&#3588;&#3619;&#3633;&#3657;&#3591;&#3592;&#3634;&#3585;
Excel
&#3586;&#3657;&#3629;&#3617;&#3641;&#3621;&#3607;&#3633;&#3657;&#3591;&#3627;&#3617;&#3604;&#3619;&#3632;&#3627;&#3623;&#3656;&#3634;&#3591;&#3649;&#3607;&#3655;&#3585;
DIV &#3592;&#3632;&#3606;&#3641;&#3585;&#3649;&#3607;&#3609;&#3607;&#3637;&#3656;-->
<!----------------------------->
<!--&#3648;&#3619;&#3636;&#3656;&#3617;&#3585;&#3634;&#3619;&#3649;&#3626;&#3604;&#3591;&#3612;&#3621;&#3592;&#3634;&#3585;&#3605;&#3633;&#3623;&#3594;&#3656;&#3623;&#3618;&#3626;&#3619;&#3657;&#3634;&#3591;&#3585;&#3634;&#3619;&#3611;&#3619;&#3632;&#3585;&#3634;&#3624;&#3648;&#3611;&#3655;&#3609;&#3648;&#3623;&#3655;&#3610;&#3648;&#3614;&#3592;&#3586;&#3629;&#3591;
Excel -->
<!----------------------------->

<div id="CER_9739" align=center x:publishsource="Excel">

<table border=0 cellpadding=0 cellspacing=0 width=1543 style='border-collapse:
 collapse;table-layout:fixed;width:1157pt'>
 <col width=64 span=2 style='width:48pt'>
 <col width=7 style='mso-width-source:userset;mso-width-alt:256;width:5pt'>
 <col width=64 span=22 style='width:48pt'>
 <tr height=35 style='height:26.25pt'>
  <td colspan=25 height=35 class=xl839739 width=1543 style='height:26.25pt;
  width:1157pt'>QUALITY CERTIFICATE</td>
 </tr>
 <tr height=31 style='height:23.25pt'>
  <td colspan=2 height=31 class=xl869739 style='height:23.25pt'>CONSIGNEE</td>
  <td class=xl729739>:</td>
  <td colspan=17 class=xl849739><?php echo $shipper['customer_name']; ?></td>
  <td class=xl679739>NO :</td>
  <td colspan=4 class=xl849739>TOKE-<?php echo @$income_info['final_job_id']; ?></td>
 </tr>
 <tr height=31 style='height:23.25pt'>
  <td colspan=2 height=31 class=xl869739 style='height:23.25pt'>CONTRACT JOB</td>
  <td class=xl729739>:</td>
  <td colspan=13 class=xl849739><?php echo @$income_info['final_sale_order_no']; ?></td>
  <td class=xl639739></td>
  <td class=xl639739></td>
  <td class=xl649739></td>
  <td colspan=2 class=xl739739>INSPECTION NO. :</td>
  <td colspan=4 class=xl849739><?php echo substr(@$income_info['final_job_id'],-6,6); ?></td>
 </tr>
 <tr height=31 style='height:23.25pt'>
  <td colspan=2 height=31 class=xl869739 style='height:23.25pt'>INVOICE NO<span
  style='mso-spacerun:yes'> </span></td>
  <td class=xl729739>:</td>
  <td colspan=15 class=xl849739><?php echo @$income_info['final_invoice_no']; ?></td>
  <td class=xl649739></td>
  <td class=xl639739></td>
  <td class=xl679739>DATE :</td>
  <td colspan=4 class=xl859739><?php echo date("d-M-Y",strtotime(@$income_info['final_date_edit'])); ?></td>
 </tr>
 <tr height=31 style='height:23.25pt'>
  <td colspan=2 height=31 class=xl869739 style='height:23.25pt'>QUANTITY</td>
  <td class=xl729739>:</td>
  <td colspan=4 class=xl849739><?php echo @$income_info['final_stud_qty']; ?></td>
  <td class=xl739739></td>
  <td colspan=4 class=xl849739></td>
  <td class=xl669739></td>
  <td class=xl669739></td>
  <td class=xl699739></td>
  <td class=xl669739></td>
  <td class=xl699739></td>
  <td class=xl669739></td>
  <td class=xl669739></td>
  <td class=xl669739></td>
  <td class=xl669739></td>
  <td class=xl669739></td>
  <td class=xl659739></td>
  <td class=xl659739></td>
  <td class=xl659739></td>
 </tr>
 <tr height=31 style='height:23.25pt'>
  <td colspan=2 height=31 class=xl869739 style='height:23.25pt'>SIZE</td>
  <td class=xl729739>:</td>
  <td colspan=13 class=xl849739><?php echo @$size['size_m']; ?>X<?php echo @$size['size_p']; ?>X<?php echo @$size['size_length']; ?></td>
  <td class=xl649739></td>
  <td class=xl639739></td>
  <td class=xl639739></td>
  <td class=xl699739></td>
  <td colspan=5 class=xl869739>Test Certificate according to EN -10204:3.1</td>
 </tr>
 <tr height=31 style='height:23.25pt'>
  <td colspan=25 height=31 class=xl899739 style='height:23.25pt'>INSPECTION
  CERTIFICATE OF SET<span style='mso-spacerun:yes'>  </span>STUD BOLT
  GRADE<span style='mso-spacerun:yes'>  </span><?php echo @$stud_product['product_name']; ?><span
  style='mso-spacerun:yes'>  </span>(COVER SET COATING BY <?php echo @$stud_product['product_type']; ?>)</td>
 </tr>
 <tr height=28 style='height:21.0pt'>
  <td height=28 class=xl769739 style='height:21.0pt'></td>
  <td class=xl769739></td>
  <td class=xl769739></td>
  <td class=xl769739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td colspan=6 class=xl959739>MECHANICAL PROPERTIES</td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
 </tr>
 <tr height=24 style='mso-height-source:userset;height:18.0pt'>
  <td height=24 class=xl779739 style='height:18.0pt'></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td colspan=3 rowspan=2 class=xl749739>Item</td>
  <td colspan=10 class=xl949739 style='border-left:none'>Tensile Test</td>
  <td colspan=5 class=xl929739 style='border-left:none'>TEST : <font
  class="font59739">GALVANIZED THICKNESS</font></td>
  <td class=xl789739></td>
  <td class=xl789739></td>
  <td class=xl789739></td>
 </tr>
 <tr height=28 style='height:21.0pt'>
  <td height=28 class=xl779739 style='height:21.0pt'></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td colspan=2 class=xl919739 style='border-left:none'>Tensile Strength</td>
  <td colspan=2 class=xl919739 style='border-left:none'>Yield Strength</td>
  <td colspan=2 class=xl919739 style='border-left:none'>Elongation</td>
  <td colspan=2 class=xl919739 style='border-left:none'>Reduction of Area</td>
  <td colspan=2 class=xl919739 style='border-left:none'>Hardness (n=3pcs)</td>
  <td colspan=5 class=xl919739 style='border-left:none'><?php echo @$stud_thickness_info['grade']; ?> <?php echo @$stud_thickness_info['name']; ?></td>
  <td class=xl789739></td>
  <td class=xl789739></td>
  <td class=xl789739></td>
 </tr>
 <tr height=28 style='height:21.0pt'>
  <td height=28 class=xl779739 style='height:21.0pt'></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td colspan=3 rowspan=2 class=xl749739>Spec.</td>
  <td class=xl749739 style='border-top:none;border-left:none'>min (<?php echo @$std_stud['Tensile Strength']['info']['ps_min_unit']; ?>)</td>
  <td class=xl749739 style='border-top:none;border-left:none'>max (<?php echo @$std_stud['Tensile Strength']['info']['ps_max_unit']; ?>)</td>
  <td class=xl749739 style='border-top:none;border-left:none'>min (<?php echo @$std_stud['Yield Strength']['info']['ps_min_unit']; ?>)</td>
  <td class=xl749739 style='border-top:none;border-left:none'>max (<?php echo @$std_stud['Yield Strength']['info']['ps_max_unit']; ?>)</td>
  <td class=xl749739 style='border-top:none;border-left:none'>min (<?php echo @$std_stud['Elongation']['info']['ps_min_unit']; ?>)</td>
  <td class=xl749739 style='border-top:none;border-left:none'>max (<?php echo @$std_stud['Elongation']['info']['ps_max_unit']; ?>)</td>
  <td class=xl749739 style='border-top:none;border-left:none'>min (<?php echo @$std_stud['Reduction of Area']['info']['ps_min_unit']; ?>)</td>
  <td class=xl749739 style='border-top:none;border-left:none'>max (<?php echo @$std_stud['Reduction of Area']['info']['ps_max_unit']; ?>)</td>
  <td class=xl749739 style='border-top:none;border-left:none'>min (<?php echo @$std_stud['Hardness']['info']['ps_min_unit']; ?>)</td>
  <td class=xl749739 style='border-top:none;border-left:none'>max (<?php echo @$std_stud['Hardness']['info']['ps_max_unit']; ?>)</td>
  <td colspan=5 class=xl919739 style='border-left:none'>GALVANIZED STANDDARD
  <?php echo @$stud_thickness_info['condition']; ?> <?php echo @$stud_thickness_info['condition_value']; ?> <?php if(@$stud_thickness_info['condition_value'] != ""){?>Micron<?php }?></td>
  <td class=xl799739></td>
  <td class=xl799739></td>
  <td class=xl799739></td>
 </tr>
 <tr height=28 style='height:21.0pt'>
  <td height=28 class=xl779739 style='height:21.0pt'></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl749739 style='border-top:none;border-left:none'><?php echo @$std_stud['Tensile Strength']['info']['ps_min']; ?></td>
  <td class=xl749739 style='border-top:none;border-left:none'><?php echo @$std_stud['Tensile Strength']['info']['ps_max']; ?></td>
  <td class=xl749739 style='border-top:none;border-left:none'><?php echo @$std_stud['Yield Strength']['info']['ps_min']; ?></td>
  <td class=xl749739 style='border-top:none;border-left:none'><?php echo @$std_stud['Yield Strength']['info']['ps_max']; ?></td>
  <td class=xl749739 style='border-top:none;border-left:none'><?php echo @$std_stud['Elongation']['info']['ps_min']; ?></td>
  <td class=xl749739 style='border-top:none;border-left:none'><?php echo @$std_stud['Elongation']['info']['ps_max']; ?></td>
  <td class=xl749739 style='border-top:none;border-left:none'><?php echo @$std_stud['Reduction of Area']['info']['ps_min']; ?></td>
  <td class=xl749739 style='border-top:none;border-left:none'><?php echo @$std_stud['Reduction of Area']['info']['ps_max']; ?></td>
  <td class=xl749739 style='border-top:none;border-left:none'><?php echo @$std_stud['Hardness']['info']['ps_min']; ?></td>
  <td class=xl749739 style='border-top:none;border-left:none'><?php echo @$std_stud['Hardness']['info']['ps_max']; ?></td>
  <td colspan=5 rowspan=2 class=xl749739><?php echo @$stud_thickness['thickness_value']; ?></td>
  <td class=xl799739></td>
  <td class=xl799739></td>
  <td class=xl799739></td>
 </tr>
 <tr height=28 style='height:21.0pt'>
  <td height=28 class=xl779739 style='height:21.0pt'></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td colspan=3 class=xl749739>Mean Value</td>
  <td colspan=2 class=xl749739 style='border-left:none'><?php echo @$std_stud['Tensile Strength']['test']['stud_testing1']; ?></td>
  <td colspan=2 class=xl749739 style='border-left:none'><?php echo @$std_stud['Yield Strength']['test']['stud_testing1']; ?></td>
  <td colspan=2 class=xl749739 style='border-left:none'><?php echo @$std_stud['Elongation']['test']['stud_testing1']; ?></td>
  <td colspan=2 class=xl749739 style='border-left:none'><?php echo @$std_stud['Reduction of Area']['test']['stud_testing1']; ?></td>
  <td colspan=2 class=xl749739 style='border-left:none'><?php echo @$std_stud['Hardness']['test']['stud_testing1']; ?></td>
  <td class=xl799739></td>
  <td class=xl799739></td>
  <td class=xl799739></td>
 </tr>
 <tr height=28 style='height:21.0pt'>
  <td height=28 class=xl759739 style='height:21.0pt'></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl799739></td>
  <td class=xl799739></td>
  <td class=xl799739></td>
  <td class=xl799739></td>
 </tr>
 <tr height=28 style='height:21.0pt'>
  <td height=28 class=xl759739 style='height:21.0pt'></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td colspan=6 class=xl959739>CHEMICAL COMPOSITION (%)</td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl809739></td>
  <td class=xl809739></td>
  <td class=xl819739></td>
  <td class=xl819739></td>
  <td class=xl819739></td>
  <td class=xl819739></td>
 </tr>
 <tr height=28 style='height:21.0pt'>
  <td height=28 class=xl159739 style='height:21.0pt'></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td colspan=4 class=xl749739>HEAT NO.</td>
  <?php foreach($stud_chemical as $index=>$row){ ?>
  <td class=xl719739 style='border-left:none'><?php echo $row['chemical_value']; ?></td>
  <?php }?>
  <td class=xl979739></td>
  <td class=xl159739></td>
  <td class=xl999739></td>
  <td class=xl819739></td>
 </tr>
 <tr height=28 style='height:21.0pt'>
  <td height=28 class=xl159739 style='height:21.0pt'></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td colspan=2 rowspan=2 class=xl749739>SPEC.</td>
  <td colspan=2 class=xl749739 style='border-left:none'>Min</td>
  <?php foreach($stud_chemical as $index=>$row){ ?>
  <td class=xl749739 style='border-top:none;border-left:none'><?php echo @$row['ps_min']; ?></td>
  <?php }?>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl819739></td>
 </tr>
 <tr height=28 style='height:21.0pt'>
  <td height=28 class=xl159739 style='height:21.0pt'></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td colspan=2 class=xl749739 style='border-left:none'>Max</td>
  <?php foreach($stud_chemical as $index=>$row){ ?>
  <td class=xl749739 style='border-top:none;border-left:none'><?php echo @$row['ps_max']; ?></td>
  <?php }?>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl779739></td>
 </tr>
 <tr height=28 style='height:21.0pt'>
  <td height=28 class=xl159739 style='height:21.0pt'></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td colspan=2 class=xl749739><?php echo $stud_info['income_heat_no']; ?></td>
  <td colspan=2 class=xl749739 style='border-left:none'>Result</td>
  <?php foreach($stud_chemical as $index=>$row){ ?>
  <td class=xl749739 style='border-top:none;border-left:none'><?php echo @$row['test_value']; ?></td>
  <?php }?>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
 </tr>
 <tr height=28 style='height:21.0pt'>
  <td height=28 class=xl789739 style='height:21.0pt'></td>
  <td class=xl789739></td>
  <td class=xl789739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl789739></td>
  <td class=xl789739></td>
  <td class=xl789739></td>
  <td class=xl789739></td>
  <td class=xl789739></td>
  <td class=xl789739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
 </tr>
 <tr height=28 style='height:21.0pt'>
  <td height=28 class=xl789739 style='height:21.0pt'></td>
  <td class=xl789739></td>
  <td class=xl789739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td colspan=6 class=xl959739>HEAT TREMENT</td>
  <td class=xl789739></td>
  <td class=xl789739></td>
  <td class=xl789739></td>
  <td class=xl789739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
 </tr>
 <tr height=28 style='height:21.0pt'>
  <td height=28 class=xl789739 style='height:21.0pt'></td>
  <td class=xl789739></td>
  <td class=xl789739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td colspan=3 rowspan=2 class=xl749739>Heat no.</td>
  <td colspan=4 class=xl749739 style='border-left:none'>Quenching</td>
  <td colspan=4 class=xl749739 style='border-left:none'>Tempering</td>
  <td class=xl789739></td>
  <td class=xl789739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
 </tr>
 <tr height=28 style='height:21.0pt'>
  <td height=28 class=xl789739 style='height:21.0pt'></td>
  <td class=xl789739></td>
  <td class=xl789739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td colspan=4 class=xl749739 style='border-left:none'><?php if($std_stud['Quenching']['info']['ps_min_unit']){ echo @$std_stud['Quenching']['info']['ps_min_unit']; }else{ echo @$std_stud['Quenching']['info']['ps_max_unit']; }?></td>
  <td colspan=4 class=xl919739 style='border-left:none'><?php if($std_stud['Tempering']['info']['ps_min_unit']){ echo @$std_stud['Tempering']['info']['ps_min_unit']; }else{ echo @$std_stud['Tempering']['info']['ps_max_unit']; }?></td>
  <td class=xl789739></td>
  <td class=xl789739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
 </tr>
 <tr height=28 style='height:21.0pt'>
  <td height=28 class=xl829739 style='height:21.0pt'></td>
  <td class=xl829739></td>
  <td class=xl829739></td>
  <td class=xl759739></td>
  <td class=xl759739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td colspan=3 class=xl749739>Spec</td>
  <td colspan=2 class=xl749739 style='border-left:none'>Min <?php echo @$std_stud['Quenching']['info']['ps_min'];?></td>
  <td colspan=2 class=xl749739 style='border-left:none'>Max <?php echo @$std_stud['Quenching']['info']['ps_max'];?></td>
  <td colspan=2 class=xl749739 style='border-left:none'>Min <?php echo @$std_stud['Tempering']['info']['ps_min'];?></td>
  <td colspan=2 class=xl749739 style='border-left:none'>Max <?php echo @$std_stud['Tempering']['info']['ps_max'];?></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
 </tr>
 <tr height=28 style='height:21.0pt'>
  <td height=28 class=xl789739 style='height:21.0pt'></td>
  <td class=xl789739></td>
  <td class=xl789739></td>
  <td class=xl789739></td>
  <td class=xl789739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
  <td colspan=3 class=xl749739><?php echo $stud_info['income_heat_no']; ?></td>
  <td colspan=4 class=xl749739 style='border-left:none'><?php echo @$std_stud['Quenching']['test']['stud_testing1']; ?></td>
  <td colspan=4 class=xl749739 style='border-left:none'><?php echo @$std_stud['Tempering']['test']['stud_testing1']; ?></td>
  <td class=xl789739></td>
  <td class=xl789739></td>
  <td class=xl789739></td>
  <td class=xl789739></td>
  <td class=xl779739></td>
  <td class=xl779739></td>
 </tr>
 <tr height=24 style='height:18.0pt'>
  <td height=24 class=xl689739 style='height:18.0pt'></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
 </tr>
 <tr height=24 style='height:18.0pt'>
  <td height=24 class=xl689739 style='height:18.0pt'></td>
  <td colspan=18 rowspan=3 class=xl939739><?php echo @$income_info['final_text_remark']; ?></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
 </tr>
 <tr height=24 style='height:18.0pt'>
  <td height=24 class=xl159739 style='height:18.0pt'></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
 </tr>
 <tr height=24 style='height:18.0pt'>
  <td height=24 class=xl159739 style='height:18.0pt'></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
 </tr>
 <tr height=24 style='height:18.0pt'>
  <td height=24 class=xl159739 style='height:18.0pt'></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
 </tr>
 <tr height=24 style='height:18.0pt'>
  <td height=24 class=xl689739 style='height:18.0pt'></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td colspan=6 class=xl879739>Inspected and approved by :</td>
  <td colspan=4 class=xl909739>&nbsp;</td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
 </tr>
 <tr height=24 style='height:18.0pt'>
  <td height=24 class=xl689739 style='height:18.0pt'></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td colspan=5 class=xl889739>Quality Control Manager</td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
 </tr>
 <tr height=24 style='height:18.0pt'>
  <td height=24 class=xl689739 style='height:18.0pt'></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
  <td class=xl689739></td>
 </tr>
 <tr height=24 style='height:18.0pt'>
  <td height=24 class=xl709739 style='height:18.0pt'></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
 </tr>
 <tr height=24 style='height:18.0pt'>
  <td height=24 class=xl709739 style='height:18.0pt'></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
 </tr>
 <tr height=24 style='height:18.0pt'>
  <td height=24 class=xl709739 style='height:18.0pt'></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl709739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
  <td class=xl159739></td>
 </tr>
 <![if supportMisalignedColumns]>
 <tr height=0 style='display:none'>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=7 style='width:5pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
 </tr>
 <![endif]>
</table>

</div>


<!----------------------------->
<!--&#3626;&#3636;&#3657;&#3609;&#3626;&#3640;&#3604;&#3585;&#3634;&#3619;&#3649;&#3626;&#3604;&#3591;&#3612;&#3621;&#3592;&#3634;&#3585;&#3605;&#3633;&#3623;&#3594;&#3656;&#3623;&#3618;&#3626;&#3619;&#3657;&#3634;&#3591;&#3585;&#3634;&#3619;&#3611;&#3619;&#3632;&#3585;&#3634;&#3624;&#3648;&#3611;&#3655;&#3609;&#3648;&#3623;&#3655;&#3610;&#3648;&#3614;&#3592;&#3586;&#3629;&#3591;
Excel-->
<!----------------------------->
</body>

</html>
