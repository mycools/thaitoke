<div class="col-sm-12">
	<!-- BEGIN SAMPLE TABLE PORTLET-->
	<div class="panel panel-white" id="panel4">
		<div class="panel-heading">
			<h4 class="panel-title text-primary"><i class="icon-pencil"></i> เพิ่มข้อมูล</h4>
			<div class="panel-tools">
				
				<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>	
			</div>
		</div>
		<div class="panel-body">
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
				
			<form method="post" class="form-vertical" enctype="multipart/form-data">
				
				<div class="form-group">
						<label  >กลุ่มสินค้า * :</label>
						<div class="controls">
							<select name="product_cate" id="product_cate" class="chosen" data-placeholder="เลือก กลุ่มสินค้า">
								<option value="bolt">bolt</option>
							   <option value="nut">nut</option>
                               <option value="washer">washer</option>
                               <option value="spring">spring</option>
                               <option value="stud">stud</option>
                               <option value="tapper">tapper</option>
							</select>
						</div>
				</div>
                
                
                <div class="form-group">
					<label  for="product_grade">ชื่อเกรด : *</label>
					<div class="controls">
						<input type="text" name="product_grade" id="product_grade" value="<?php echo set_value('product_grade'); ?>" class="form-control input-md" />
					</div>
				</div>
				<div class="form-group">
					<label  for="product_low_carbon">Standard : </label>
					<div class="controls">
						<input type="text" name="product_std" id="product_std" value="<?php echo set_value('product_std'); ?>" class="form-control input-md" />
					</div>
				</div>
				<div class="form-actions">
					<input type="hidden" name="product_type" id="product_type" value="<?php echo $type; ?>" />
                    <input type="hidden" name="size_id" id="size_id" value="<?php echo $size_id; ?>" />
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-mini" href="<?php echo admin_url("anchorbolt_product/product_list/".$type."/".$size_id); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			</form>
		</div>
	</div>
</div>
<style type="text/css">
#menu_icon_chzn{
	font-family: 'FontAwesome',Tahoma;
}
</style>