<div class="span12">
	<div class="widget">
		<div class="widget-title">
			<h4><i class="icon-cogs"></i>Anchor Bolt Standard</h4>
			<span class="tools">
			<a href="javascript:;" class="icon-chevron-down"></a>
			<a href="<?php echo current_url(); ?>" class="icon-refresh"></a>		
			</span>							
		</div>
		<div class="widget-body form">
			<?php echo validation_errors('<div class="alert alert-error">
				<button class="close" data-dismiss="alert">×</button>
				<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
				
			<form method="post" class="form-vertical"  enctype="multipart/form-data">
				<table class="table table-striped table-bordered table-advance table-hover"> 
					<tr>
						<th style="width:30%;">Description</th>
						<th style="width:70%;">Standard value</th>
					</tr>
					<tr>
						<td>Hardness
							<input type="hidden" name="standard_description" id="standard_description" value="Hardness" class="form-control input-md" />
						</td>
						<td>
							MIN <input type="text" name="ps_min" id="ps_min" value="<?php echo trim(set_value('ps_min', @$row['ps_min'])); ?>" class="span2 numericonly" placeholder="ค่า" /><input type="text" name="ps_min_unit" id="ps_min_unit" value="<?php echo trim(set_value('ps_min_unit', @$row['ps_min_unit'])); ?>" class="span2" placeholder="หน่วย" />
							MAX <input type="text" name="ps_max" id="ps_max" value="<?php echo trim(set_value('ps_max', @$row['ps_max'])); ?>" class="span2 numericonly" placeholder="ค่า" /><input type="text" name="ps_max_unit" id="ps_max_unit" value="<?php echo trim(set_value('ps_max_unit', @$row['ps_max_unit'])); ?>" class="span2" placeholder="หน่วย" />
						</td>
					</tr>
				</table>
				<div class="form-actions">
					<input type="hidden" name="ps_id" id="ps_id" value="<?php echo @$row['ps_id']; ?>" />
					<input type="hidden" name="product_id" id="product_id" value="<?php echo $product_id; ?>" />
					<input type="hidden" name="update_status" id="update_status" value="<?php echo $update_status; ?>" />
				 	<button type="submit" class="btn btn-mini btn-primary"><i class="icon-save"></i> บันทึกการเปลี่ยนแปลง</button>
				 	<a class="btn btn-mini" href="<?php echo admin_url("manage_anchorbolt/index/".$type); ?>"><i class="icon-reply"></i> ยกเลิกการแก้ไข</a>
				</div>
			</form>
			
		</div>
	</div>
</div>