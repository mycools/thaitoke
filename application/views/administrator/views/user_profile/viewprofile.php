<!-- start: USER PROFILE -->

<div class="row">
	<div class="col-md-12">
		<?php $success = $this->session->flashdata("message-success"); ?>
		<?php if($success){ ?>
		<div class="alert alert-success">
			<button class="close" data-dismiss="alert">×</button>
			<strong>การทำรายการเสร็จสมบูรณ์ </strong> <?php echo $success; ?>
		</div>
		<?php } ?>

		<?php $info = $this->session->flashdata("message-info"); ?>
		<?php if($info){ ?>
		<div class="alert alert-info">
			<button class="close" data-dismiss="alert">×</button>
			<strong>ข้อมูลเพิ่มเติม </strong> <?php echo $info; ?>
		</div>
		<?php } ?>

		<?php $error = $this->session->flashdata("message-error"); ?>
		<?php if($error){ ?>
		<div class="alert alert-danger">
			<button class="close" data-dismiss="alert">×</button>
			<strong>เกิดข้อผิดพลาด </strong> <?php echo $error; ?>
		</div>
		<?php } ?>
		<?php $warning = $this->session->flashdata("message-warning"); ?>
		<?php if($warning){ ?>
		<div class="alert">
			<button class="close" data-dismiss="alert">×</button>
			<strong>คำเตือน </strong> <?php echo $warning; ?>
		</div>
		<?php } ?>
		<?php echo validation_errors('<div class="alert alert-danger">
			<button class="close" data-dismiss="alert">×</button>
			<strong>เกิดข้อผิดพลาด </strong>','</div>'); ?>
		<?php echo @$upload_error; ?>
		<div class="tabbable">
			<ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
				<li class="active">
					<a data-toggle="tab" href="#panel_overview">
						Overview
					</a>
				</li>
				<li>
					<a data-toggle="tab" href="#panel_edit_account">
						Edit Account
					</a>
				</li>
				
			</ul>
			<div class="tab-content">
				<div id="panel_overview" class="tab-pane fade in active">
					<div class="row">
						<div class="col-sm-5 col-md-4">
							<div class="user-left">
								<div class="center">
									<h4><?php echo $row['user_fullname']; ?></h4>
									<div class="fileinput fileinput-new" data-provides="fileinput">
										<div class="user-image">
											<div class="fileinput-new thumbnail">
												<?php if($row['user_avatar'] != ""){ ?>
												<img src="<?php echo site_url("src/150/user_admin/".$row['user_avatar']); ?>" />
												<?php } ?>
											</div>
											
										</div>
									</div>
									
									<hr>
								</div>
								<table class="table table-condensed">
									<thead>
										<tr>
											<th colspan="3">Contact Information</th>
										</tr>
									</thead>
									<tbody>
										
										<tr>
											<td>email :</td>
											<td>
											<a href="">
												<?php echo $row['user_email']; ?>
											</a></td>
											<td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
										</tr>
										<tr>
											<td>phone :</td>
											<td><?php echo $row['user_mobileno']; ?></td>
											<td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
										</tr>
										<tr>
											<td>username :</td>
											<td>
											<a href="">
												<?php echo $row['username']; ?>
											</a></td>
											<td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
										</tr>
									</tbody>
								</table>
								<table class="table">
									<thead>
										<tr>
											<th colspan="3">General information</th>
										</tr>
									</thead>
									<tbody>
										
										<tr>
											<td>Last Logged In</td>
											<td><?php echo timeago($row['user_lastlogin']); ?></td>
											<td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
										</tr>
										<tr>
											<td>Position</td>
											<td><?php echo ucfirst($row['user_position']); ?></td>
											<td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
										</tr>
										
										<tr>
											<td>Status</td>
											<td>
											<?php echo ($row['user_status'])?'<span class="label label-sm label-info">Normal Account</span>':'<span class="label label-sm label-warning">Suspend Account</span>'; ?>
											</td>
											<td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
										</tr>
									</tbody>
								</table>
								<table class="table">
									<thead>
										<tr>
											<th colspan="3">Additional information</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Birth</td>
											<td><?php echo ldateth($row['user_birthday']); ?></td>
											<td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
										</tr>
										<tr>
											<td>Gender</td>
											<td><?php echo ucfirst($row['user_gender']); ?></td>
											<td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
										</tr>
										<tr>
											<td>Access Group</td>
											<td><?php echo $this->admin_library->getGroupName($row['user_group']); ?></td>
											<td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="col-sm-7 col-md-8">
							
							<div class="panel panel-white" id="activities">
								<div class="panel-heading border-light">
									<h4 class="panel-title text-primary">Recent Activities</h4>
									<paneltool class="panel-tools" tool-collapse="tool-collapse" tool-refresh="load1" tool-dismiss="tool-dismiss"></paneltool>
								</div>
								<div collapse="activities" ng-init="activities=false" class="panel-wrapper">
									<div class="panel-body">
										<ul class="timeline-xs">
											<li class="timeline-item success">
												<div class="margin-left-15">
													<div class="text-muted text-small">
														<?php echo timeago($row['user_joindate']); ?>
													</div>
													<p>
														<a class="text-info">
															<?php echo $row['user_fullname']; ?>
														</a>
														has completed his account.
													</p>
												</div>
											</li>
											<?php if($row['user_updatedate']){ ?>
											<li class="timeline-item success">
												<div class="margin-left-15">
													<div class="text-muted text-small">
														<?php echo timeago($row['user_updatedate']); ?>
													</div>
													<p>
														<a class="text-info">
															<?php echo $row['user_fullname']; ?>
														</a>
														has update his account.
													</p>
												</div>
											</li>
											<?php } ?>
										</ul>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<div id="panel_edit_account" class="tab-pane fade">
					<form action="<?php echo admin_url("user_profile/edit"); ?>" method="post" role="form" id="form" enctype="multipart/form-data">
						<fieldset>
							<legend>
								Account Info
							</legend>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label >
											Name
										</label>
										<input type="text" placeholder="<?php echo $row['user_fullname']; ?>" class="form-control" id="user_fullname" name="user_fullname" value="<?php echo set_value("user_fullname",$row['user_fullname']); ?>">
									</div>
									
									<div class="form-group">
										<label >
											Email Address
										</label>
										<input type="email" placeholder="<?php echo $row['user_email']; ?>" class="form-control" id="user_email" name="user_email" value="<?php echo set_value("user_email",$row['user_email']); ?>">
									</div>
									<div class="form-group">
										<label >
											Phone
										</label>
										<input type="tel" placeholder="<?php echo $row['user_mobileno']; ?>" class="form-control" id="user_mobileno" name="user_mobileno" value="<?php echo set_value("user_mobileno",$row['user_mobileno']); ?>">
									</div>
									<div class="form-group">
										<label >
											Username
										</label>
										<input type="tel" placeholder="<?php echo $row['username']; ?>" class="form-control" id="username" name="username" value="<?php echo set_value("username",$row['username']); ?>">
									</div>
									<div class="form-group">
										<label >
											Password
										</label>
										<input type="password" placeholder="password" class="form-control" name="password" id="password">
									</div>
									<div class="form-group">
										<label >
											Confirm Password
										</label>
										<input type="password"  placeholder="password" class="form-control" id="repassword" name="repassword">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label >
											Gender
										</label>
										<div class="clip-radio radio-primary">
											<input type="radio" value="female" id="user_gender" name="user_gender" id="us-female" <?php echo (set_value("user_gender",$row['user_gender'])=="female")?'checked':''; ?>>
											<label for="us-female">
												Female
											</label>
											<input type="radio" value="male" name="user_gender" id="us-male"  <?php echo (set_value("user_gender",$row['user_gender'])=="male")?'checked':''; ?>>
											<label for="us-male">
												Male
											</label>
										</div>
									</div>
									<div class="form-group">
										<label >
											Birthday
										</label>
										<p class="input-group input-append datepicker date">
											<input type="text" name="user_birthday" id="user_birthday" class="form-control" value="<?php echo set_value("user_birthday",$row['user_birthday']); ?>" />
											<span class="input-group-btn">
												<button type="button" class="btn btn-default">
													<i class="glyphicon glyphicon-calendar"></i>
												</button> </span>
										</p>

									</div>
									
									<div class="form-group">
										<label>
											Image Upload
										</label>
										<div class="fileinput fileinput-new" data-provides="fileinput">
											<div class="fileinput-new thumbnail"><?php if($row['user_avatar'] != ""){ ?>
												<img src="<?php echo site_url("src/150/user_admin/".$row['user_avatar']); ?>" />
												<?php } ?>
											</div>
											<div class="fileinput-preview fileinput-exists thumbnail"></div>
											<div class="user-edit-image-buttons">
												<span class="btn btn-azure btn-file"><span class="fileinput-new"><i class="fa fa-picture"></i> Select image</span><span class="fileinput-exists"><i class="fa fa-picture"></i> Change</span>
													<input type="file" name="avatar_file" id="avatar_file">
												</span>
												<a href="#" class="btn fileinput-exists btn-red" data-dismiss="fileinput">
													<i class="fa fa-times"></i> Remove
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</fieldset>
						
						
						<div class="row">
							<div class="col-md-8">
								<p>
									By clicking UPDATE, you are agreeing to the Policy and Terms &amp; Conditions.
								</p>
							</div>
							<div class="col-md-4">
								<button class="btn btn-primary pull-right" type="submit">
									Update <i class="fa fa-arrow-circle-right"></i>
								</button>
							</div>
						</div>
					</form>
				</div>
				
			</div>
		</div>
	</div>
</div>

<!-- end: USER PROFILE -->