<?php
$table_unique_id = "list_".time();

?>
<style type="text/css">
	.table tbody tr td{
		font-size: 10px;
	}
	.table thead tr th{
		font-size: 12px;
	}
	.table tbody tr td .btn-group .btn{
		font-size: 12px !important;
	}
	.table tbody tr td .label{
		font-size: 11px !important;
	}
	.table tbody tr td .dropdown-toggle{
		font-size: 12px;
	}
	.table tbody tr td .dropdown-menu li{
		font-size: 12px;
	}
	.dropdown-tools{
		background-color: #fff !important;
	}
	.dropdown-tools.disabled{
		 color: #e0e0e0;
		cursor: not-allowed;
	}
	.dropdown-tools.disabled:hover{
		color: #fff;
		background-color: #e0e0e0  !important;
	}
	.dropdown-tools.btn-danger{
		color: #DA0003;
	}
	.dropdown-tools.btn-danger:hover{
		background-color: #DA0003;
		color: #fff;
	}
</style>
<div class="col-md-12">
	<h5 class="over-title margin-bottom-15"><span class="text-bold"><i class="<?php echo $icon; ?>"></i> <?php echo $title; ?></span></h5>
	<?php $success = $this->session->flashdata("message-success"); ?>
	<?php if($success){ ?>
	<div class="alert alert-success">
		<button class="close" data-dismiss="alert">×</button>
		<strong>การทำรายการเสร็จสมบูรณ์ </strong> <?php echo $success; ?>
	</div>
	<?php } ?>

	<?php $info = $this->session->flashdata("message-info"); ?>
	<?php if($info){ ?>
	<div class="alert alert-info">
		<button class="close" data-dismiss="alert">×</button>
		<strong>ข้อมูลเพิ่มเติม </strong> <?php echo $info; ?>
	</div>
	<?php } ?>

	<?php $error = $this->session->flashdata("message-error"); ?>
	<?php if($error){ ?>
	<div class="alert alert-danger">
		<button class="close" data-dismiss="alert">×</button>
		<strong>เกิดข้อผิดพลาด </strong> <?php echo $error; ?>
	</div>
	<?php } ?>
	<?php $warning = $this->session->flashdata("message-warning"); ?>
	<?php if($warning){ ?>
	<div class="alert alert-warning">
		<button class="close" data-dismiss="alert">×</button>
		<strong>คำเตือน </strong> <?php echo $warning; ?>
	</div>
	<?php } ?>
	<div class="row">
		<div class="col-sm-12">
			<?php if(@$custom_tools){ echo $custom_tools; } ?>
			<form method="get" enctype="multipart/form-data" class="formsearch_sample_<?php echo $table_unique_id ; ?>">
			<?php if(@$search_date){ ?>
				<div class="control-group">
				 <label class="control-label" for="username">เลือกช่วงวันที่สมัคร :</label>
				 <div class="controls">
				    <span class="input-append date date-picker" data-date="<?php echo set_value("start_date",date("d-m-Y")); ?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                       <input class="input-small date-picker" size="16" type="text" name="start_date" id="start_date" value="<?php echo set_get_value("start_date",date("d-m-Y")); ?>" /><span class="add-on"><i class="icon-calendar"></i></span>
                    </span>
                    <span>&nbsp;&nbsp;ถึงวันที่&nbsp;&nbsp;</span>
                    <span class="input-append date date-picker" data-date="<?php echo set_value("start_date",date("d-m-Y")); ?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                       <input class="input-small date-picker" size="16" type="text" name="end_date" id="end_date" value="<?php echo set_get_value("end_date",date("d-m-Y")); ?>" /><span class="add-on"><i class="icon-calendar"></i></span>
                    </span>
                    
				 </div>
				</div>
			<?php } ?>
			<?php if(@$search_date){ ?>
			<div class="control-group">
				<div class="controls">
				<input type="submit" class="btn btn-info" value="Search" />
				</div>
			</div>	
			
			<?php } ?>
			</form>
		</div>
		<div class="col-sm-12">
			<form method="get" enctype="multipart/form-data" class="form_sample_<?php echo $table_unique_id ; ?>" action="<?php echo (!@$search_text_url)?current_url():admin_url(@$search_text_url); ?>">
			<?php if(count($checkall_dropdown)+count($top_button) > 0 || @$search_text==true){ ?>
			<div class="row-fluid">
				
				<div class="span8">
					<div class="<?php if(count($checkall_dropdown)+count($top_button) > 1){ ?>input-prepend input-append btn-group<?php } ?> checktools">
					<?php if($checkall && count($checkall_dropdown) > 0){ ?>
					<button type="button" class="hidden-phone btn btn-normal dropdown-toggle checkable-dropdown-<?php echo $table_unique_id; ?>" data-toggle="dropdown"><i class="icon-check"></i> การกระทำกับรายการที่เลือก (<span class="select_count">0</span>) <span class="icon-caret-down"></span></button>
					  <ul class="dropdown-menu">
					     
					     <?php foreach($checkall_dropdown as $but){ ?>
					     <?php if($this->admin_model->check_permision($but['permision'])){ ?>
					     <?php $confirm_del = ($but['permision']=="d")?'true':'false'; ?>
					     <li><a  onclick="checkall_take_action('<?php echo admin_url($but['url']); ?>',<?php echo $confirm_del; ?>);"><?php echo $but['name']; ?></a></li>
					     <?php } ?>
					     <?php } ?>
					  </ul>
					  <?php } ?>
					<?php foreach($top_button as $but){ ?>
					<?php if($this->admin_model->check_permision($but['permision'])){ ?>
					<?php $confirm_del = ($but['permision']=="d")?'onclick="return confirm(\'ยืนยันการลบรายการที่เลือกนี้ ?\');"':''; ?>
							<a href="<?php echo admin_url($but['url']); ?>" class="btn btn-normal btn-sm <?php echo $but['type']; ?> <?php echo $but['class']; ?>" <?php echo $confirm_del; ?>><i class="<?php echo $but['icon']; ?>"></i> <?php echo $but['name']; ?></a>
					<?php }else{ ?>
							<a  class="btn btn-normal  btn-sm disabled <?php echo $but['class']; ?>"><i class="<?php echo $but['icon']; ?>"></i> <?php echo $but['name']; ?></a>
					<?php } ?>
					<?php } ?>
					
					</div>
				</div>
				<?php if(@$search_text){ ?>
				<div class="col-md-4">
					
					<div class="input-group">
						
						<input type="text" name="q" class="form-control input-sm" value="<?php echo $this->input->get("q"); ?>" />
						<span class="input-group-btn">
							<button type="submit" class="btn btn-primary btn-sm">
								<i class="fa fa-search"></i>
								ค้นหา
							</button> </span>
					</div>
				</div>
				<?php } ?>
			</div>
			<p></p>
			<?php } ?>
			<table class="table table-striped table-bordered table-advance table-hover"> 
				<thead>
					<tr>
						<?php if($checkall && count($checkall_dropdown) > 0){ ?>
						<th width="20" class="hidden-phone">
							<input type="checkbox"  class="group-checkable-<?php echo $table_unique_id; ?>" data-set=".<?php echo $table_unique_id; ?>"  />
						</th>
						<?php } ?>
						<?php foreach($column as $col){ ?>
						<?php $th_width = ($col['width'] > 0)?'width="'.$col['width'].'"':""; ?>
						<th class="<?php echo $col['class']; ?>" <?php echo $th_width; ?> ><i class="<?php echo $col['icon']; ?>"></i> <span class="hidden-phone"><?php echo $col['name']; ?></span></th>
						<?php } ?>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php
						if(is_array($datatable)){
							$data_res = $datatable;
						}else{
							$data_res = $datatable->result_array();
						}
					?>
					<?php foreach($data_res as $row){ $row_ori = $row;?>
					<tr>
						<?php if($checkall && count($checkall_dropdown) > 0){ ?>
						<td class="hidden-phone">
							<input type="checkbox" name="select_item[]" class="<?php echo $table_unique_id; ?>" id="<?php echo $table_unique_id; ?>" value="<?php echo $row[$checkall_key]; ?>" />
						</td>
						<?php } ?>
						<?php foreach($column as $col){ ?>
						<?php 
							// if(isset($column_callback[$col['field']])){
							// 	if(method_exists($this->admin_model->me,$column_callback[$col['field']])){
							// 		$row[$col['field']] = @$this->admin_model->me->$column_callback[$col['field']](@$row[$col['field']],$row_ori);
							// 	}else if(function_exists($column_callback[$col['field']])){
							// 		$row[$col['field']] = $column_callback[$col['field']](@$row[$col['field']]);
							// 	}
								
                            // }
							if(isset($column_callback[$col['field']])){
								$field_val = call_user_func(array($this->admin_model->me,$column_callback[@$col['field']]),@$row[$col['field']],$row_ori);
							}else{
								$field_val = $row[$col['field']];
							}
						?>
						<td class="<?php echo $col['class']; ?>"><div class="hidden-overflow"><?php echo @$row[$col['field']]; ?></div></td>
						<?php } ?>
						<td style="text-align:right">
						<div class="btn-group">
						<?php if(count($button) > 1){ ?>
							<a href="#" data-toggle="dropdown" class="btn btn-dark-beige btn-xs dropdown-toggle">
								<i class="fa fa-bars" aria-hidden="true"></i> เครื่องมือ <span class="caret"></span>
							</a>
							<ul class="dropdown-menu" role="menu">
							
								<?php foreach($button as $but){ ?>
								<?php 
									$allow = true;
									if(isset($action_button_callback[$but['name']])){
										$allow = $this->admin_model->me->$action_button_callback[$but['name']]($row_ori);
									}
								?>
								<?php if($this->admin_model->check_permision($but['permision']) && $allow){ ?>
								
								<?php $confirm_del = ($but['permision']=="d")?'onclick="return confirm(\'ยืนยันการลบรายการนี้ ?\');"':''; ?>
								<li>
									<a href="<?php echo admin_url($this->admin_model->assign_param($but['url'],$row)); ?>" class="dropdown-tools <?php echo $but['class']; ?> <?php echo $but['type']; ?>" <?php echo $confirm_del; ?>><i class="<?php echo $but['icon']; ?>"></i> <?php echo $but['name']; ?></a>
								</li>
								<?php }else{ ?>
								<li>
									<a href="javascript:;" class="dropdown-tools <?php echo $but['class']; ?> <?php echo $but['type']; ?> disabled" ><i class="<?php echo $but['icon']; ?>"></i> <?php echo $but['name']; ?></a>
								</li>	
								<?php } ?>
								<?php } ?>
							
							</ul>
							
							<?php }else{ ?>
								<?php foreach($button as $but){ ?> 
									<?php if($this->admin_model->check_permision($but['permision'])){ ?>
									<?php $confirm_del = ($but['permision']=="d")?'onclick="return confirm(\'ยืนยันการลบรายการนี้ ?\');"':''; ?>
									<a href="<?php echo admin_url($this->admin_model->assign_param($but['url'],$row)); ?>" class="btn btn-sm <?php echo $but['type']; ?> <?php echo $but['class']; ?>" <?php echo $confirm_del; ?>><i class="<?php echo $but['icon']; ?>"></i> <?php echo $but['name']; ?></a>						
									<?php }else{ ?>
									<a  class="btn btn-sm disabled <?php echo $but['class']; ?>"><i class="<?php echo $but['icon']; ?>"></i> <?php echo $but['name']; ?></a>
									<?php } ?>
								<?php } ?>
							<?php } ?>
						</div>

						
						</td>
					</tr>
					<?php } ?>
					<?php if(count($data_res)==0){ ?> 
					<tr>
						<td colspan="<?php echo count($column)+1; ?>" style="text-align:center;">== ไม่มีข้อมูล ==</td>
						
					</tr>
					<?php } ?>
				</tbody>
			</table>
			<p>
				<?php
				if($pagination){
					create_pagination($url,$total_rows,$perpage,$uri_segment);
				}
				?>
			</p>
			</form>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		$('a._blank').prop('target','_blank');				  
	});
</script>

