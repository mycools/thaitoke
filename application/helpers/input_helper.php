<?php
function set_input_selected($select_value,$default_value,$post_value)
{
	$value = "";
	if($_POST){
		$value =  $post_value;
	}else{
		$value =  $default_value;
	}
	if($select_value==$value){
		return ' selected="selected" ';
	}else{
		return '';
	}
}
function set_input_value($post_value,$default_value)
{
	if($_POST){
		return $post_value;
	}else{
		return $default_value;
	}
}

function set_get_value($get_value,$default_value)
{
	if(isset($_GET[$get_value])){
		return @$_GET[$get_value];
	}else{
		return $default_value;
	}
}
if(!function_exists('setUTF8')){
	function setUTF8($text)
	{
		return iconv( 'UTF-8','cp874//IGNORE',$text);
	}
}