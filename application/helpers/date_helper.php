<?php
function todbdate($in_date='01/01/1970')
{
	if(count(explode("/",$in_date))< 3){
		return '1970-01-01';
	}
	list($d,$m,$y) = explode("/",$in_date);
	$odate = "$y-$m-$d";
	return $odate;
}
function dbtodate($in_date='')
{
	if(count(explode("-",$in_date))< 3){
		return '01/01/1970';
	}
	list($d,$m,$y) = explode("-",$in_date);
	$date = "$d/$m/$y";
	return $date;
}