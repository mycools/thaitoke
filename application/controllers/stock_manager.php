<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
File Name 	: stock_manager.php
Controller	: Stock_manager
Create By 	: Jarak Kritkiattisak
Create Date 	: 7/6/2557 BE
Project 	: iAon Project
Version 		: 1.0
*/
class Stock_manager extends CI_Controller {
	var $_data=array();
	public function __construct()
	{
		parent::__construct();
		$this->load->library('admin_library');
		$this->load->model('administrator/admin_model');
		$this->load->model('administrator/product_model');
		$this->admin_library->forceLogin();
		$this->admin_model->set_menu_key('61e7b859f2b051604b77401be1ed3ffc');
	}
	public function index()
	{
		admin_redirect("stock_manager/bolt_list");
	}
	/*
		Bolt Manager - Start
	*/
	public function bolt_list($page=1)
	{
		$page = ((int) $page < 1)?1:(int) $page;
		$offset = ($page-1)*25;
		
		$this->seq=$offset;
		$this->admin_model->set_menu_key('4b7caa022eefa2b630763fd0f05ec4d1');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		
		//$this->admin_model->set_top_button("เพิ่ม Bolt","manageproduct/bolt_add",'icon-plus','btn-success','w');
		/* $this->admin_model->set_top_button("สำรองข้อมูล","menu_manager/backup",'icon-download','btn-primary','s'); */
		$this->admin_model->set_datatable($this->product_model->bolt_dataTable(25,$offset));
		$this->admin_model->set_column("product_no","ลำดับ",'5%');
		$this->admin_model->set_column("product_thumbnail","รูปภาพ",'15%');
		$this->admin_model->set_column("product_name","ชื่อสินค้า",'30%');
		$this->admin_model->set_column("product_type","ประเภท",'20%');
		$this->admin_model->set_column_callback('product_thumbnail','get_image');
		$this->admin_model->set_column_callback("product_no","show_seq");
		
		
		$this->admin_model->set_action_button("ดูข้อมูล","stock_manager/bolt_size_list/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->show_search_text("stock_manager/bolt_list");
		$this->admin_model->set_pagination("stock_manager/bolt_list",$this->product_model->bolt_count(),25,3);
		
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function bolt_size_list($productid=0,$page=1){
		$page = ((int) $page < 1)?1:(int) $page;
		$offset = ($page-1)*25;
		
		$this->seq=$offset;
		$this->admin_model->set_menu_key('4b7caa022eefa2b630763fd0f05ec4d1');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		 
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('รายการสินค้า Bolt','stock_manager/bolt_list','');
		$this->admin_library->add_breadcrumb('จัดการ Size',current_url(),'');
		
		//$this->admin_model->set_top_button("เพิ่ม Bolt size","manageproduct/bolt_size_add/".$productid,'icon-plus','btn-success','w');
		//$this->admin_model->set_top_button("กลับไปยังหน้ารายการสินค้า Bolt",'manageproduct/bolt_list','icon-reply','btn-primary','w');
		
		$this->admin_model->set_datatable($this->product_model->bolt_size_dataTable($productid,25,$offset));
		$this->admin_model->set_column("size_no","ลำดับ",0);
		$this->admin_model->set_column("size_m","M",0);
		$this->admin_model->set_column("size_p","P",0);
		$this->admin_model->set_column("size_length","Length",0);
		$this->admin_model->set_column("size_qty","Quantity",0);
		$this->admin_model->set_column_callback("size_no","show_seq");
		$this->admin_model->set_column_callback("size_qty","show_qty");
		$this->admin_model->set_action_button("ปรับปรุงสต็อก","stock_manager/edit_stock/[size_id]",'icon-trash-o','btn-info','w');
		$this->admin_model->set_action_button("ดูประวัติ","stock_manager/view_bolt_history/[size_id]/".$productid,'icon-trash-o','btn-info','w');
		$this->admin_model->set_action_button('Income Log',"stock_manager/bolt_incoming_log/[size_id]/".$productid,'icon-trash-o','btn-info','w');
		$this->admin_model->show_search_text("stock_manager/bolt_size_list/".$productid);
		$this->admin_model->set_pagination("stock_manager/bolt_size_list",$this->product_model->bolt_size_count($productid),25,3);
		
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	
	public function edit_stock($size_id=0){
		
		$this->admin_model->set_menu_key('4b7caa022eefa2b630763fd0f05ec4d1');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('qty','จำนวน','trim|required');
		$this->form_validation->set_rules('remark','หมายเหตุ','trim|required');
		$this->form_validation->set_message('required', '"%s" is required.');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("แก้ไข stock","stock_manager/edit_stock/".$size_id,"icon-plus");
			$this->_data['row'] = $this->product_model->size_detail($size_id);
			$this->_data['size_id'] = $size_id;
			$this->admin_library->view("stock_manager/bolt_edit", $this->_data);
			$this->admin_library->output();
		}else{
			
			$pro_id = $this->product_model->stock_edit($size_id);
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("stock_manager/bolt_size_list/".$this->input->post('product_id'));
		}
	}
	
	public function view_bolt_history($size_id=0,$pro_id=0){
		
		$this->admin_model->set_menu_key('4b7caa022eefa2b630763fd0f05ec4d1');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("ดูข้อมูล","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('กลับ','stock_manager/bolt_size_list/'.$pro_id,'');
		//$this->admin_library->add_breadcrumb('จัดการ Size',current_url(),'');
		
		//$this->admin_model->set_top_button("เพิ่ม Bolt size","manageproduct/bolt_size_add/".$productid,'icon-plus','btn-success','w');
		//$this->admin_model->set_top_button("กลับไปยังหน้ารายการสินค้า Bolt",'manageproduct/bolt_list','icon-reply','btn-primary','w');
		
		$this->admin_model->set_datatable($this->product_model->get_bolt_history_dataTable($size_id));
		$this->admin_model->set_column("log_no","ลำดับ",0);
		$this->admin_model->set_column("qty_old","จำนวนเดิม",0);
		$this->admin_model->set_column("qty","จำนวนใหม่",0);
		$this->admin_model->set_column("remark","หมายเหตุ",0);
		$this->admin_model->set_column("createdate","วันที่แก้ไข",0);
		$this->admin_model->set_column_callback("log_no","show_seq");
		$this->admin_model->set_column_callback("createdate","show_date");

				
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function bolt_incoming_log($size_id=0,$pro_id=0){
		$this->admin_model->set_menu_key('4b7caa022eefa2b630763fd0f05ec4d1');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("ดูข้อมูล","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('กลับ','stock_manager/bolt_size_list/'.$pro_id,'');
		
		$this->admin_model->set_datatable($this->product_model->get_bolt_incoming_dataTable($size_id, $pro_id, 'logs'));
		$this->admin_model->set_column('income_job_id','Income No.',0);
		$this->admin_model->set_column('income_invoice_no','Invoice No.',0);
		$this->admin_model->set_column('income_supplier_id','Supplier',0);
		$this->admin_model->set_column('income_quantity','Quantity',0);
		$this->admin_model->set_column_callback('income_supplier_id','show_supplier');
		
		$this->admin_model->make_list();
		$this->admin_library->output();
	}

	
	public function nut_list($page=1)
	{
		$page = ((int) $page < 1)?1:(int) $page;
		$offset = ($page-1)*25;
		$this->seq=$offset;
		$this->admin_model->set_menu_key('0b9c56027f5f70a52246f612084e294c');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		
		//$this->admin_model->set_top_button("เพิ่ม Bolt","manageproduct/bolt_add",'icon-plus','btn-success','w');
		/* $this->admin_model->set_top_button("สำรองข้อมูล","menu_manager/backup",'icon-download','btn-primary','s'); */
		$this->admin_model->set_datatable($this->product_model->nut_dataTable(25,$offset));
		$this->admin_model->set_column("product_no","ลำดับ",'5%');
		$this->admin_model->set_column("product_thumbnail","รูปภาพ",'15%');
		$this->admin_model->set_column("product_name","ชื่อสินค้า",'30%');
		$this->admin_model->set_column("product_type","ประเภท",'20%');
		$this->admin_model->set_column_callback('product_thumbnail','get_image_nut');
		$this->admin_model->set_column_callback("product_no","show_seq");
		
		
		$this->admin_model->set_action_button("ดูข้อมูล","stock_manager/nut_size_list/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->show_search_text("stock_manager/nut_list");
		$this->admin_model->set_pagination("stock_manager/nut_list",$this->product_model->nut_count(),25,3);
		
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function nut_size_list($productid=0,$page=1){
		$page = ((int) $page < 1)?1:(int) $page;
		$offset = ($page-1)*25;
		
		$this->seq=$offset;
		$this->admin_model->set_menu_key('0b9c56027f5f70a52246f612084e294c');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('รายการสินค้า Nut','stock_manager/nut_list','');
		$this->admin_library->add_breadcrumb('จัดการ Size',current_url(),'');
		
		//$this->admin_model->set_top_button("เพิ่ม Bolt size","manageproduct/bolt_size_add/".$productid,'icon-plus','btn-success','w');
		//$this->admin_model->set_top_button("กลับไปยังหน้ารายการสินค้า Bolt",'manageproduct/bolt_list','icon-reply','btn-primary','w');
		
		$this->admin_model->set_datatable($this->product_model->nut_size_dataTable($productid,25,$offset));
		$this->admin_model->set_column("size_no","ลำดับ",0);
		$this->admin_model->set_column("size_m","M",0);
		$this->admin_model->set_column("size_p","P",0);
		/* $this->admin_model->set_column("size_length","Length",0); */
		$this->admin_model->set_column("size_qty","Quantity",0);
		$this->admin_model->set_column_callback("size_no","show_seq");
		$this->admin_model->set_column_callback("size_qty","show_qty");
		$this->admin_model->set_action_button("ปรับปรุงสต็อก","stock_manager/edit_stock_nut/[size_id]",'icon-trash-o','btn-info','w');
		$this->admin_model->set_action_button("ดูประวัติ","stock_manager/view_nut_history/[size_id]/".$productid,'icon-trash-o','btn-info','w');
		$this->admin_model->set_action_button("Incoming log","stock_manager/nut_incoming_log/[size_id]/".$productid,'icon-trash-o','btn-info','w');
		$this->admin_model->show_search_text("stock_manager/nut_size_list/".$productid);
		$this->admin_model->set_pagination("stock_manager/nut_size_list",$this->product_model->nut_size_count($productid),25,3);
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	
	public function edit_stock_nut($size_id=0){
		
		$this->admin_model->set_menu_key('0b9c56027f5f70a52246f612084e294c');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('qty','จำนวน','trim|required');
		$this->form_validation->set_rules('remark','หมายเหตุ','trim|required');
		$this->form_validation->set_message('required', '"%s" is required.');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("แก้ไข stock","stock_manager/edit_stock_nut/".$size_id,"icon-plus");
			$this->_data['row'] = $this->product_model->size_detail_nut($size_id);
			$this->_data['size_id'] = $size_id;
			$this->admin_library->view("stock_manager/nut_edit", $this->_data);
			$this->admin_library->output();
		}else{
			
			$pro_id = $this->product_model->stock_edit_nut($size_id);
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("stock_manager/nut_size_list/".$this->input->post('product_id'));
		}
	}
	
	public function view_nut_history($size_id=0,$pro_id=0){
		
		$this->admin_model->set_menu_key('0b9c56027f5f70a52246f612084e294c');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("ดูข้อมูล","icon-shopping-cart");
		//$this->admin_library->add_breadcrumb('รายการสินค้า nut','stock_manager/nut_list','');
		//$this->admin_library->add_breadcrumb('จัดการ Size',current_url(),'');
		$this->admin_library->add_breadcrumb('กลับ','stock_manager/nut_size_list/'.$pro_id,'');
		//$this->admin_model->set_top_button("เพิ่ม nut size","manageproduct/nut_size_add/".$productid,'icon-plus','btn-success','w');
		//$this->admin_model->set_top_button("กลับไปยังหน้ารายการสินค้า nut",'manageproduct/nut_list','icon-reply','btn-primary','w');
		
		$this->admin_model->set_datatable($this->product_model->get_nut_history_dataTable($size_id));
		$this->admin_model->set_column("log_no","ลำดับ",0);
		$this->admin_model->set_column("qty_old","จำนวนเดิม",0);
		$this->admin_model->set_column("qty","จำนวนใหม่",0);
		$this->admin_model->set_column("remark","หมายเหตุ",0);
		$this->admin_model->set_column("createdate","วันที่แก้ไข",0);
		$this->admin_model->set_column_callback("log_no","show_seq");
		$this->admin_model->set_column_callback("createdate","show_date");

				
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function nut_incoming_log($size_id=0,$pro_id=0){
		$this->admin_model->set_menu_key('0b9c56027f5f70a52246f612084e294c');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("ดูข้อมูล","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('กลับ','stock_manager/nut_size_list/'.$pro_id,'');
		
		$this->admin_model->set_datatable($this->product_model->get_nut_incoming_dataTable($size_id, $pro_id, 'logs'));
		$this->admin_model->set_column('income_job_id','Income No.',0);
		$this->admin_model->set_column('income_invoice_no','Invoice No.',0);
		$this->admin_model->set_column('income_supplier_id','Supplier',0);
		$this->admin_model->set_column('income_quantity','Quantity',0);
		$this->admin_model->set_column_callback('income_supplier_id','show_supplier');
		
		$this->admin_model->make_list();
		$this->admin_library->output();
	}

	public function washer_list($page=1)
	{
		$page = ((int) $page < 1)?1:(int) $page;
		$offset = ($page-1)*25;
		$this->seq=$offset;
		$this->admin_model->set_menu_key('ab6dce64cc08c66c7bf1b83acd4215f9');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		
		//$this->admin_model->set_top_button("เพิ่ม Bolt","manageproduct/bolt_add",'icon-plus','btn-success','w');
		/* $this->admin_model->set_top_button("สำรองข้อมูล","menu_manager/backup",'icon-download','btn-primary','s'); */
		$this->admin_model->set_datatable($this->product_model->washer_dataTable(25,$offset));
		$this->admin_model->set_column("product_no","ลำดับ",'5%');
		$this->admin_model->set_column("product_thumbnail","รูปภาพ",'15%');
		$this->admin_model->set_column("product_name","ชื่อสินค้า",'30%');
		$this->admin_model->set_column("product_type","ประเภท",'20%');
		$this->admin_model->set_column_callback('product_thumbnail','get_image_washer');
		$this->admin_model->set_column_callback("product_no","show_seq");
		
		
		$this->admin_model->set_action_button("ดูข้อมูล","stock_manager/washer_size_list/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->show_search_text("stock_manager/washer_list");
		$this->admin_model->set_pagination("stock_manager/washer_list",$this->product_model->washer_count(),25,3);
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function washer_size_list($productid=0,$page=1){
		$page = ((int) $page < 1)?1:(int) $page;
		$offset = ($page-1)*25;
		
		$this->seq=$offset;
		$this->admin_model->set_menu_key('ab6dce64cc08c66c7bf1b83acd4215f9');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('รายการสินค้า Washer','stock_manager/washer_list','');
		$this->admin_library->add_breadcrumb('จัดการ Size',current_url(),'');
		
		//$this->admin_model->set_top_button("เพิ่ม Bolt size","manageproduct/bolt_size_add/".$productid,'icon-plus','btn-success','w');
		//$this->admin_model->set_top_button("กลับไปยังหน้ารายการสินค้า Bolt",'manageproduct/bolt_list','icon-reply','btn-primary','w');
		
		$this->admin_model->set_datatable($this->product_model->washer_size_dataTable($productid,25,$offset));
		$this->admin_model->set_column("size_no","ลำดับ",0);
		$this->admin_model->set_column("size_m","M",0);
		$this->admin_model->set_column("size_p","P",0);
		/* $this->admin_model->set_column("size_length","Length",0); */
		$this->admin_model->set_column("size_qty","Quantity",0);
		$this->admin_model->set_column_callback("size_no","show_seq");
		$this->admin_model->set_column_callback("size_qty","show_qty");
		$this->admin_model->set_action_button("ปรับปรุงสต็อก","stock_manager/edit_stock_washer/[size_id]",'icon-trash-o','btn-info','w');
		$this->admin_model->set_action_button("ดูประวัติ","stock_manager/view_washer_history/[size_id]/".$productid,'icon-trash-o','btn-info','w');
		$this->admin_model->set_action_button("Incoming log","stock_manager/washer_incoming_log/[size_id]/".$productid,'icon-trash-o','btn-info','w');
		$this->admin_model->show_search_text("stock_manager/washer_size_list/".$productid);
		$this->admin_model->set_pagination("stock_manager/washer_size_list",$this->product_model->washer_size_count($productid),25,3);
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	
	public function edit_stock_washer($size_id=0){
		
		$this->admin_model->set_menu_key('ab6dce64cc08c66c7bf1b83acd4215f9');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('qty','จำนวน','trim|required');
		$this->form_validation->set_rules('remark','หมายเหตุ','trim|required');
		$this->form_validation->set_message('required', '"%s" is required.');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("แก้ไข stock","stock_manager/edit_stock_washer/".$size_id,"icon-plus");
			$this->_data['row'] = $this->product_model->size_detail_washer($size_id);
			$this->_data['size_id'] = $size_id;
			$this->admin_library->view("stock_manager/washer_edit", $this->_data);
			$this->admin_library->output();
		}else{
			
			$pro_id = $this->product_model->stock_edit_washer($size_id);
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("stock_manager/washer_size_list/".$this->input->post('product_id'));
		}
	}
	
	public function view_washer_history($size_id=0,$pro_id=0){
		
		$this->admin_model->set_menu_key('ab6dce64cc08c66c7bf1b83acd4215f9');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("ดูข้อมูล","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('กลับ','stock_manager/washer_size_list/'.$pro_id,'');
		//$this->admin_library->add_breadcrumb('รายการสินค้า washer','stock_manager/washer_list','');
		//$this->admin_library->add_breadcrumb('จัดการ Size',current_url(),'');
		
		//$this->admin_model->set_top_button("เพิ่ม washer size","manageproduct/washer_size_add/".$productid,'icon-plus','btn-success','w');
		//$this->admin_model->set_top_button("กลับไปยังหน้ารายการสินค้า washer",'manageproduct/washer_list','icon-reply','btn-primary','w');
		
		$this->admin_model->set_datatable($this->product_model->get_washer_history_dataTable($size_id));
		$this->admin_model->set_column("log_no","ลำดับ",0);
		$this->admin_model->set_column("qty_old","จำนวนเดิม",0);
		$this->admin_model->set_column("qty","จำนวนใหม่",0);
		$this->admin_model->set_column("remark","หมายเหตุ",0);
		$this->admin_model->set_column("createdate","วันที่แก้ไข",0);
		$this->admin_model->set_column_callback("log_no","show_seq");
		$this->admin_model->set_column_callback("createdate","show_date");

				
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function washer_incoming_log($size_id=0,$pro_id=0){
		$this->admin_model->set_menu_key('ab6dce64cc08c66c7bf1b83acd4215f9');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("ดูข้อมูล","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('กลับ','stock_manager/washer_size_list/'.$pro_id,'');
		
		$this->admin_model->set_datatable($this->product_model->get_washer_incoming_dataTable($size_id, $pro_id));
		$this->admin_model->set_column('income_job_id','Income No.',0);
		$this->admin_model->set_column('income_invoice_no','Invoice No.',0);
		$this->admin_model->set_column('income_supplier_id','Supplier',0);
		$this->admin_model->set_column('income_quantity','Quantity',0);
		$this->admin_model->set_column_callback('income_supplier_id','show_supplier');
		
		$this->admin_model->make_list();
		$this->admin_library->output();
	}
	
	// Spring
	public function spring_list($page=1)
	{
		$page = ((int) $page < 1)?1:(int) $page;
		$offset = ($page-1)*25;
		$this->seq=$offset;
		$this->admin_model->set_menu_key('dd448d46309095a11296e733f48909f4');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		
		//$this->admin_model->set_top_button("เพิ่ม Bolt","manageproduct/bolt_add",'icon-plus','btn-success','w');
		/* $this->admin_model->set_top_button("สำรองข้อมูล","menu_manager/backup",'icon-download','btn-primary','s'); */
		$this->admin_model->set_datatable($this->product_model->spring_dataTable(25,$offset));
		$this->admin_model->set_column("product_no","ลำดับ",'5%');
		$this->admin_model->set_column("product_thumbnail","รูปภาพ",'15%');
		$this->admin_model->set_column("product_name","ชื่อสินค้า",'30%');
		$this->admin_model->set_column("product_type","ประเภท",'20%');
		$this->admin_model->set_column_callback('product_thumbnail','get_image_spring');
		$this->admin_model->set_column_callback("product_no","show_seq");
		
		
		$this->admin_model->set_action_button("ดูข้อมูล","stock_manager/spring_size_list/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->show_search_text("stock_manager/spring_list");
		$this->admin_model->set_pagination("stock_manager/spring_list",$this->product_model->spring_count(),25,3);
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function spring_size_list($productid=0,$page=1){
		$page = ((int) $page < 1)?1:(int) $page;
		$offset = ($page-1)*25;
		
		$this->seq=$offset;
		$this->admin_model->set_menu_key('dd448d46309095a11296e733f48909f4');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('รายการสินค้า Spring Washer','stock_manager/spring_list','');
		$this->admin_library->add_breadcrumb('จัดการ Size',current_url(),'');
		
		//$this->admin_model->set_top_button("เพิ่ม Bolt size","manageproduct/bolt_size_add/".$productid,'icon-plus','btn-success','w');
		//$this->admin_model->set_top_button("กลับไปยังหน้ารายการสินค้า Bolt",'manageproduct/bolt_list','icon-reply','btn-primary','w');
		
		$this->admin_model->set_datatable($this->product_model->spring_size_dataTable($productid,25,$offset));
		$this->admin_model->set_column("size_no","ลำดับ",0);
		$this->admin_model->set_column("size_m","M",0);
		$this->admin_model->set_column("size_p","P",0);
		/* $this->admin_model->set_column("size_length","Length",0); */
		$this->admin_model->set_column("size_qty","Quantity",0);
		$this->admin_model->set_column_callback("size_no","show_seq");
		$this->admin_model->set_column_callback("size_qty","show_qty");
		$this->admin_model->set_action_button("ปรับปรุงสต็อก","stock_manager/edit_stock_spring/[size_id]",'icon-trash-o','btn-info','w');
		$this->admin_model->set_action_button("ดูประวัติ","stock_manager/view_spring_history/[size_id]/".$productid,'icon-trash-o','btn-info','w');
		$this->admin_model->set_action_button("Incoming log","stock_manager/spring_incoming_log/[size_id]/".$productid,'icon-trash-o','btn-info','w');
		$this->admin_model->show_search_text("stock_manager/spring_size_list/".$productid);
		$this->admin_model->set_pagination("stock_manager/spring_size_list",$this->product_model->spring_size_count($productid),25,3);
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	
	public function edit_stock_spring($size_id=0){
		
		$this->admin_model->set_menu_key('dd448d46309095a11296e733f48909f4');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('qty','จำนวน','trim|required');
		$this->form_validation->set_rules('remark','หมายเหตุ','trim|required');
		$this->form_validation->set_message('required', '"%s" is required.');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("แก้ไข stock","stock_manager/edit_stock_spring/".$size_id,"icon-plus");
			$this->_data['row'] = $this->product_model->size_detail_spring($size_id);
			$this->_data['size_id'] = $size_id;
			$this->admin_library->view("stock_manager/spring_edit", $this->_data);
			$this->admin_library->output();
		}else{
			
			$pro_id = $this->product_model->stock_edit_spring($size_id);
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("stock_manager/spring_size_list/".$this->input->post('product_id'));
		}
	}
	
	public function view_spring_history($size_id=0,$pro_id=0){
		
		$this->admin_model->set_menu_key('dd448d46309095a11296e733f48909f4');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("ดูข้อมูล","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('กลับ','stock_manager/spring_size_list/'.$pro_id,'');
		//$this->admin_library->add_breadcrumb('รายการสินค้า spring','stock_manager/spring_list','');
		//$this->admin_library->add_breadcrumb('จัดการ Size',current_url(),'');
		
		//$this->admin_model->set_top_button("เพิ่ม spring size","manageproduct/spring_size_add/".$productid,'icon-plus','btn-success','w');
		//$this->admin_model->set_top_button("กลับไปยังหน้ารายการสินค้า spring",'manageproduct/spring_list','icon-reply','btn-primary','w');
		
		$this->admin_model->set_datatable($this->product_model->get_spring_history_dataTable($size_id));
		$this->admin_model->set_column("log_no","ลำดับ",0);
		$this->admin_model->set_column("qty_old","จำนวนเดิม",0);
		$this->admin_model->set_column("qty","จำนวนใหม่",0);
		$this->admin_model->set_column("remark","หมายเหตุ",0);
		$this->admin_model->set_column("createdate","วันที่แก้ไข",0);
		$this->admin_model->set_column_callback("log_no","show_seq");
		$this->admin_model->set_column_callback("createdate","show_date");

				
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function spring_incoming_log($size_id=0,$pro_id=0){
		$this->admin_model->set_menu_key('dd448d46309095a11296e733f48909f4');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("ดูข้อมูล","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('กลับ','stock_manager/spring_size_list/'.$pro_id,'');
		
		$this->admin_model->set_datatable($this->product_model->get_spring_incoming_dataTable($size_id, $pro_id));
		$this->admin_model->set_column('income_job_id','Income No.',0);
		$this->admin_model->set_column('income_invoice_no','Invoice No.',0);
		$this->admin_model->set_column('income_supplier_id','Supplier',0);
		$this->admin_model->set_column('income_quantity','Quantity',0);
		$this->admin_model->set_column_callback('income_supplier_id','show_supplier');
		
		$this->admin_model->make_list();
		$this->admin_library->output();
	}
	
	// Taper
	public function taper_list($page=1)
	{
		$page = ((int) $page < 1)?1:(int) $page;
		$offset = ($page-1)*25;
		$this->seq=$offset;
		$this->admin_model->set_menu_key('39d76554ec6424a9e596c0e8f5a69032');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		
		//$this->admin_model->set_top_button("เพิ่ม Bolt","manageproduct/bolt_add",'icon-plus','btn-success','w');
		/* $this->admin_model->set_top_button("สำรองข้อมูล","menu_manager/backup",'icon-download','btn-primary','s'); */
		$this->admin_model->set_datatable($this->product_model->taper_dataTable(25,$offset));
		$this->admin_model->set_column("product_no","ลำดับ",'5%');
		$this->admin_model->set_column("product_thumbnail","รูปภาพ",'15%');
		$this->admin_model->set_column("product_name","ชื่อสินค้า",'30%');
		$this->admin_model->set_column("product_type","ประเภท",'20%');
		$this->admin_model->set_column_callback('product_thumbnail','get_image_taper');
		$this->admin_model->set_column_callback("product_no","show_seq");
		
		
		$this->admin_model->set_action_button("ดูข้อมูล","stock_manager/taper_size_list/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->show_search_text("stock_manager/taper_list");
		$this->admin_model->set_pagination("stock_manager/taper_list",$this->product_model->taper_count(),25,3);
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function taper_size_list($productid=0,$page=1){
		$page = ((int) $page < 1)?1:(int) $page;
		$offset = ($page-1)*25;
		
		$this->seq=$offset;
		$this->admin_model->set_menu_key('39d76554ec6424a9e596c0e8f5a69032');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('รายการสินค้า Taper Washer','stock_manager/taper_list','');
		$this->admin_library->add_breadcrumb('จัดการ Size',current_url(),'');
		
		//$this->admin_model->set_top_button("เพิ่ม Bolt size","manageproduct/bolt_size_add/".$productid,'icon-plus','btn-success','w');
		//$this->admin_model->set_top_button("กลับไปยังหน้ารายการสินค้า Bolt",'manageproduct/bolt_list','icon-reply','btn-primary','w');
		
		$this->admin_model->set_datatable($this->product_model->taper_size_dataTable($productid,25,$offset));
		$this->admin_model->set_column("size_no","ลำดับ",0);
		$this->admin_model->set_column("size_m","M",0);
		$this->admin_model->set_column("size_p","P",0);
		/* $this->admin_model->set_column("size_length","Length",0); */
		$this->admin_model->set_column("size_qty","Quantity",0);
		$this->admin_model->set_column_callback("size_no","show_seq");
		$this->admin_model->set_column_callback("size_qty","show_qty");
		$this->admin_model->set_action_button("ปรับปรุงสต็อก","stock_manager/edit_stock_taper/[size_id]",'icon-trash-o','btn-info','w');
		$this->admin_model->set_action_button("ดูประวัติ","stock_manager/view_taper_history/[size_id]/".$productid,'icon-trash-o','btn-info','w');
		$this->admin_model->set_action_button("Incoming log","stock_manager/taper_incoming_log/[size_id]/".$productid,'icon-trash-o','btn-info','w');
		$this->admin_model->show_search_text("stock_manager/taper_size_list/".$productid);
		$this->admin_model->set_pagination("stock_manager/taper_size_list",$this->product_model->taper_size_count($productid),25,3);

		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	
	public function edit_stock_taper($size_id=0){
		
		$this->admin_model->set_menu_key('39d76554ec6424a9e596c0e8f5a69032');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('qty','จำนวน','trim|required');
		$this->form_validation->set_rules('remark','หมายเหตุ','trim|required');
		$this->form_validation->set_message('required', '"%s" is required.');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("แก้ไข stock","stock_manager/edit_stock_taper/".$size_id,"icon-plus");
			$this->_data['row'] = $this->product_model->size_detail_taper($size_id);
			$this->_data['size_id'] = $size_id;
			$this->admin_library->view("stock_manager/taper_edit", $this->_data);
			$this->admin_library->output();
		}else{
			
			$pro_id = $this->product_model->stock_edit_taper($size_id);
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("stock_manager/taper_size_list/".$this->input->post('product_id'));
		}
	}
	
	public function view_taper_history($size_id=0,$pro_id=0){
		
		$this->admin_model->set_menu_key('39d76554ec6424a9e596c0e8f5a69032');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("ดูข้อมูล","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('กลับ','stock_manager/taper_size_list/'.$pro_id,'');
		//$this->admin_library->add_breadcrumb('รายการสินค้า taper','stock_manager/taper_list','');
		//$this->admin_library->add_breadcrumb('จัดการ Size',current_url(),'');
		
		//$this->admin_model->set_top_button("เพิ่ม taper size","manageproduct/taper_size_add/".$productid,'icon-plus','btn-success','w');
		//$this->admin_model->set_top_button("กลับไปยังหน้ารายการสินค้า taper",'manageproduct/taper_list','icon-reply','btn-primary','w');
		
		$this->admin_model->set_datatable($this->product_model->get_taper_history_dataTable($size_id));
		$this->admin_model->set_column("log_no","ลำดับ",0);
		$this->admin_model->set_column("qty_old","จำนวนเดิม",0);
		$this->admin_model->set_column("qty","จำนวนใหม่",0);
		$this->admin_model->set_column("remark","หมายเหตุ",0);
		$this->admin_model->set_column("createdate","วันที่แก้ไข",0);
		$this->admin_model->set_column_callback("log_no","show_seq");
		$this->admin_model->set_column_callback("createdate","show_date");

				
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function taper_incoming_log($size_id=0,$pro_id=0){
		$this->admin_model->set_menu_key('39d76554ec6424a9e596c0e8f5a69032');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("ดูข้อมูล","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('กลับ','stock_manager/taper_size_list/'.$pro_id,'');
		
		$this->admin_model->set_datatable($this->product_model->get_taper_incoming_dataTable($size_id, $pro_id));
		$this->admin_model->set_column('income_job_id','Income No.',0);
		$this->admin_model->set_column('income_invoice_no','Invoice No.',0);
		$this->admin_model->set_column('income_supplier_id','Supplier',0);
		$this->admin_model->set_column('income_quantity','Quantity',0);
		$this->admin_model->set_column_callback('income_supplier_id','show_supplier');
		
		$this->admin_model->make_list();
		$this->admin_library->output();
	}
	
	// Stud
	public function stud_list($page=1)
	{
		$page = ((int) $page < 1)?1:(int) $page;
		$offset = ($page-1)*25;
		$this->seq=$offset;
		$this->admin_model->set_menu_key('2bd6cdf535a7c79df633d5434abea895');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		
		//$this->admin_model->set_top_button("เพิ่ม Bolt","manageproduct/bolt_add",'icon-plus','btn-success','w');
		/* $this->admin_model->set_top_button("สำรองข้อมูล","menu_manager/backup",'icon-download','btn-primary','s'); */
		$this->admin_model->set_datatable($this->product_model->stud_dataTable(25,$offset));
		$this->admin_model->set_column("product_no","ลำดับ",'5%');
		$this->admin_model->set_column("product_thumbnail","รูปภาพ",'15%');
		$this->admin_model->set_column("product_name","ชื่อสินค้า",'30%');
		$this->admin_model->set_column("product_type","ประเภท",'20%');
		$this->admin_model->set_column_callback('product_thumbnail','get_image_stud');
		$this->admin_model->set_column_callback("product_no","show_seq");
		
		
		$this->admin_model->set_action_button("ดูข้อมูล","stock_manager/stud_size_list/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->show_search_text("stock_manager/stud_list");
		$this->admin_model->set_pagination("stock_manager/stud_list",$this->product_model->stud_count(),25,3);
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function stud_size_list($productid=0,$page=1){
		$page = ((int) $page < 1)?1:(int) $page;
		$offset = ($page-1)*25;
		
		$this->seq=$offset;
		$this->admin_model->set_menu_key('2bd6cdf535a7c79df633d5434abea895');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('รายการสินค้า Stud','stock_manager/stud_list','');
		$this->admin_library->add_breadcrumb('จัดการ Size',current_url(),'');
		
		//$this->admin_model->set_top_button("เพิ่ม Bolt size","manageproduct/bolt_size_add/".$productid,'icon-plus','btn-success','w');
		//$this->admin_model->set_top_button("กลับไปยังหน้ารายการสินค้า Bolt",'manageproduct/bolt_list','icon-reply','btn-primary','w');
		
		$this->admin_model->set_datatable($this->product_model->stud_size_dataTable($productid,25,$offset));
		$this->admin_model->set_column("size_no","ลำดับ",0);
		$this->admin_model->set_column("size_m","M",0);
		$this->admin_model->set_column("size_p","P",0);
		/* $this->admin_model->set_column("size_length","Length",0); */
		$this->admin_model->set_column("size_qty","Quantity",0);
		$this->admin_model->set_column_callback("size_no","show_seq");
		$this->admin_model->set_column_callback("size_qty","show_qty");
		$this->admin_model->set_action_button("ปรับปรุงสต็อก","stock_manager/edit_stock_stud/[size_id]",'icon-trash-o','btn-info','w');
		$this->admin_model->set_action_button("ดูประวัติ","stock_manager/view_stud_history/[size_id]/".$productid,'icon-trash-o','btn-info','w');
		$this->admin_model->set_action_button("Incoming","stock_manager/stud_incoming_log/[size_id]/".$productid,'icon-trash-o','btn-info','w');
		$this->admin_model->show_search_text("stock_manager/stud_size_list/".$productid);
		$this->admin_model->set_pagination("stock_manager/stud_size_list",$this->product_model->stud_size_count($productid),25,3);
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	
	public function edit_stock_stud($size_id=0){
		
		$this->admin_model->set_menu_key('2bd6cdf535a7c79df633d5434abea895');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('qty','จำนวน','trim|required');
		$this->form_validation->set_rules('remark','หมายเหตุ','trim|required');
		$this->form_validation->set_message('required', '"%s" is required.');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("แก้ไข stock","stock_manager/edit_stock_stud/".$size_id,"icon-plus");
			$this->_data['row'] = $this->product_model->size_detail_stud($size_id);
			$this->_data['size_id'] = $size_id;
			$this->admin_library->view("stock_manager/stud_edit", $this->_data);
			$this->admin_library->output();
		}else{
			
			$pro_id = $this->product_model->stock_edit_stud($size_id);
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("stock_manager/stud_size_list/".$this->input->post('product_id'));
		}
	}
	
	public function view_stud_history($size_id=0,$pro_id=0){
		
		$this->admin_model->set_menu_key('2bd6cdf535a7c79df633d5434abea895');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("ดูข้อมูล","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('กลับ','stock_manager/stud_size_list/'.$pro_id,'');
		//$this->admin_library->add_breadcrumb('รายการสินค้า stud','stock_manager/stud_list','');
		//$this->admin_library->add_breadcrumb('จัดการ Size',current_url(),'');
		
		//$this->admin_model->set_top_button("เพิ่ม stud size","manageproduct/stud_size_add/".$productid,'icon-plus','btn-success','w');
		//$this->admin_model->set_top_button("กลับไปยังหน้ารายการสินค้า stud",'manageproduct/stud_list','icon-reply','btn-primary','w');
		
		$this->admin_model->set_datatable($this->product_model->get_stud_history_dataTable($size_id));
		$this->admin_model->set_column("log_no","ลำดับ",0);
		$this->admin_model->set_column("qty_old","จำนวนเดิม",0);
		$this->admin_model->set_column("qty","จำนวนใหม่",0);
		$this->admin_model->set_column("remark","หมายเหตุ",0);
		$this->admin_model->set_column("createdate","วันที่แก้ไข",0);
		$this->admin_model->set_column_callback("log_no","show_seq");
		$this->admin_model->set_column_callback("createdate","show_date");

				
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}

	public function stud_incoming_log($size_id=0,$pro_id=0){
		$this->admin_model->set_menu_key('2bd6cdf535a7c79df633d5434abea895');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("ดูข้อมูล","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('กลับ','stock_manager/stud_size_list/'.$pro_id,'');
		
		$this->admin_model->set_datatable($this->product_model->get_stud_incoming_dataTable($size_id, $pro_id));
		$this->admin_model->set_column('income_job_id','Income No.',0);
		$this->admin_model->set_column('income_invoice_no','Invoice No.',0);
		$this->admin_model->set_column('income_supplier_id','Supplier',0);
		$this->admin_model->set_column('income_quantity','Quantity',0);
		$this->admin_model->set_column_callback('income_supplier_id','show_supplier');
		
		$this->admin_model->make_list();
		$this->admin_library->output();
	}

	
	
	/* Default function - Start */
	public function show_seq($text,$row)
	{
		$this->seq++;
		return $this->seq;
		
	}
	
	public function show_qty($text,$row){
		return number_format($text);
	}
	
	public function show_date($text,$row){
		
		$date = date("d-m-Y H:i:s", strtotime($text));
		return $date;
	}
	
	public function get_image($text,$row){
		if($row['product_image']!=''){
			return '<img src="'.site_url("src/60/product/bolt/".$row['product_image']).'" alt="" />';
		}else{
			return 'No image';
		}
	}
	
	public function get_image_nut($text,$row){
		if($row['product_image']!=''){
			return '<img src="'.site_url("src/60/product/nut/".$row['product_image']).'" alt="" />';
		}else{
			return 'No image';
		}
	}
	
	public function get_image_washer($text,$row){
		if($row['product_image']!=''){
			return '<img src="'.site_url("src/60/product/washer/".$row['product_image']).'" alt="" />';
		}else{
			return 'No image';
		}
	}
	
	public function get_image_spring($text,$row){
		if($row['product_image']!=''){
			return '<img src="'.site_url("src/60/product/spring/".$row['product_image']).'" alt="" />';
		}else{
			return 'No image';
		}
	}
	
	public function get_image_taper($text,$row){
		if($row['product_image']!=''){
			return '<img src="'.site_url("src/60/product/taper/".$row['product_image']).'" alt="" />';
		}else{
			return 'No image';
		}
	}
	
	public function get_image_stud($text,$row){
		if($row['product_image']!=''){
			return '<img src="'.site_url("src/60/product/stud/".$row['product_image']).'" alt="" />';
		}else{
			return 'No image';
		}
	}
	
	public function show_supplier($text, $row){
		$supplierinfo = $this->product_model->get_supplierinfo_byid($row['income_supplier_id']);
		return $supplierinfo['supplier_name'];
	}
	/* Default function - End */
}