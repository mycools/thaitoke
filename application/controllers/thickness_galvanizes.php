<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Thickness_galvanizes extends CI_Controller {
	var $_data = array();
	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->library('image_lib');
		$this->load->library('admin_library');	
		$this->admin_library->forceLogin();
		$this->load->model('thickness_galvanizes_model');
		$this->load->model('administrator/admin_model');
	}
	public function index()
	{
			
	}
	
	#############Thickness Galvanizes #######################
	
	public function tg_list($page=1)
	{
		$this->admin_model->set_menu_key('f5110dd9cda85dc46eda63e3ebafbc7c');
		if(!$this->admin_model->check_permision("r")){
			show_error("สิทธิการเข้าถึงของคุณไม่ถูกต้อง.");
		}
		$this->admin_library->setTitle('Manage Thickness Galvanizes','icon-group');
		$this->admin_library->setDetail("การจัดการ Thickness Galvanizes");
		
		
		
		$this->admin_model->initd($this);
		
		$limit = 25;
		$offset = ($page-1)*$limit;
		$this->admin_model->set_pagination("thickness_galvanizes/tg_list",$this->thickness_galvanizes_model->dataTable_Count(),$limit,4);
		
		$this->admin_model->set_title("การจัดการ Thickness Galvanizes",'icon-group');
		
		$this->admin_model->set_top_button("เพิ่ม","thickness_galvanizes/add_tg",'icon-plus','btn-success','w');
		
		$this->admin_model->set_datatable($this->thickness_galvanizes_model->dataTable($limit,$offset));
		$this->admin_model->set_column("name","ชื่อ",0,'icon-group');
		$this->admin_model->set_column("grade","เกรด",100,'icon-check-square');
		$this->admin_model->set_column("condition","เงื่อนไข",0,'icon-list-ol');
		$this->admin_model->set_column("condition_value","ค่า",0,'icon-list-ol');
		$this->admin_model->set_column("unit","หน่วย",0,'icon-list-ol');
		$this->admin_model->set_column("tools","เครื่องมือ",0,'icon-list-ol');
 		//$this->admin_model->set_column_callback("thickness_galvanizes_status","thickness_galvanizes_status_callback");
 		
		//$this->admin_model->set_action_button("ข้อมูลกลุ่ม","user_group/view/[group_id]",'icon-building','btn-success','r');
		$this->admin_model->set_action_button("แก้ไข","thickness_galvanizes/edit_tg/[id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("ลบ","thickness_galvanizes/delete_tg/[id]",'icon-trash-o','btn-danger','d');
		
		
		$this->admin_model->make_list();
		
		
		$this->admin_library->output();
		
	}
	
	public function add_tg()
	{
		$this->admin_model->set_menu_key('f5110dd9cda85dc46eda63e3ebafbc7c');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("thickness_galvanizes/tg_list");
		}
		$menu_name = "การจัดการ Thickness Galvanizes";
		$this->_data['_menu_name']="Add Data";
		
		if(!$_POST){
			$this->admin_library->view("thickness_galvanizes/add_tg",$this->_data);
			$this->admin_library->output();
		}else{
			
			$data['name'] = $this->input->post('name');
			$data['grade'] = $this->input->post('grade');
			$data['condition'] = $this->input->post('condition');
			$data['condition_value'] = $this->input->post('condition_value');
			$data['unit'] = $this->input->post('unit');
			$data['tools'] = $this->input->post('tools');
			
		/*var_dump($data);
		exit();*/
					
			
			$tg_id=$this->thickness_galvanizes_model->add_tg($data);
			
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("thickness_galvanizes/tg_list");
		}
	}
	
	public function edit_tg($tg_id)
	{
		$this->admin_model->set_menu_key('f5110dd9cda85dc46eda63e3ebafbc7c');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการแก้ไขข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("thickness_galvanizes/tg_list");
		}
		$menu_name = "การจัดการ Thickness Galvanizes";
		$this->_data['row']=$this->thickness_galvanizes_model->get_tg_detail($tg_id);
		$this->_data['_menu_name']="Edit tg";
		
		if(!$_POST){
			$this->admin_library->view("thickness_galvanizes/edit_tg",$this->_data);
			$this->admin_library->output();
		}else{
			$data['id'] = $this->input->post('id');
			$data['name'] = $this->input->post('name');
			$data['grade'] = $this->input->post('grade');
			$data['condition'] = $this->input->post('condition');
			$data['condition_value'] = $this->input->post('condition_value');
			$data['unit'] = $this->input->post('unit');
			$data['tools'] = $this->input->post('tools');
		/*var_dump($data);
		exit();*/
		
			
			$tg_id=$this->thickness_galvanizes_model->edit_tg($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("thickness_galvanizes/tg_list");
		}
	}
	
	
	function delete_tg($tg_id)
	{
		$this->admin_model->set_menu_key('f5110dd9cda85dc46eda63e3ebafbc7c');
		if(!$this->admin_model->check_permision("d")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการลบข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("thickness_galvanizes/tg_list");
		}else{
	
		$this->thickness_galvanizes_model->delete_tg($tg_id);
		admin_redirect("thickness_galvanizes/tg_list");
		
		}
	}

	########## end tg ###############################
	
	
	
	
}