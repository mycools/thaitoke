<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Final_test_anchor extends CI_Controller {

	function __construct(){
		parent::__construct();
		
		$this->load->library('upload');
		$this->load->library('image_lib');
		$this->load->library('admin_library');
		
		$this->admin_library->forceLogin();
		$this->load->model('administrator/admin_model');
		$this->load->model('administrator/finaltestanchormodel');
	}
	
	function final_main_testing($productid=0, $finalid=0){
		
		$this->admin_model->set_menu_key('a74aac1ad23b00c99681f7258c897eed');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_library->setTitle('Final Test Anchor','icon-folder-open');
		
		/* Check Edit - Start */
		$finaltestinfo = $this->finaltestanchormodel->get_finaltestinfo($productid, $finalid);
		
		if($finaltestinfo){
			admin_redirect('final_test_anchor/final_main_edit_testing/'.$productid.'/'.$finalid.'/'.$finaltestinfo->final_test_id);
		}
		/* Check Edit - End */
		
		$this->form_validation->set_rules('income_id','Income ID','trim|required');
		$this->form_validation->set_rules('supplier_id','Supplier ID','trim|required');
		$this->form_validation->set_rules('income_test_date','Income test date','trim|required');
		$this->form_validation->set_rules('product_type','Product Type','trim|required');
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			
			$this->admin_library->add_breadcrumb("Final test Anchor Bolt","final_test_anchor/final_main_testing/".$productid."/".$finalid,"icon-plus");
			$this->_data['productid'] = $productid;
			$this->_data['finalid'] = $finalid;
			$this->_data['productinfo'] = $this->finaltestanchormodel->get_productinfo_byid($productid);
			$this->_data['incomelist'] = $this->finaltestanchormodel->get_incomeid_byproductid($productid);
			
			$this->admin_library->view("final_test_anchor/main", $this->_data);
			$this->admin_library->output();
			
		}else{
			
			$testid = $this->finaltestanchormodel->add_income_anchor_test();
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("final_test_anchor/final_chemical_testing/".$productid."/".$finalid."/".$testid);
			
		}
		
	}
	
	function final_main_edit_testing($productid=0, $finalid=0, $testid=0){
		
		$this->admin_model->set_menu_key('a74aac1ad23b00c99681f7258c897eed');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_library->setTitle('Final Test Anchor','icon-folder-open');
		
		$this->form_validation->set_rules('income_id','Income ID','trim|required');
		$this->form_validation->set_rules('supplier_id','Supplier ID','trim|required');
		$this->form_validation->set_rules('income_test_date','Income test date','trim|required');
		$this->form_validation->set_rules('product_type','Product Type','trim|required');
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			
			$this->admin_library->add_breadcrumb("Final test Anchor Bolt","final_test_anchor/final_main_testing/".$productid."/".$finalid,"icon-plus");
			$this->_data['testinfo'] = $this->finaltestanchormodel->get_testinfo_byid($testid);
			$this->_data['productid'] = $productid;
			$this->_data['finalid'] = $finalid;
			$this->_data['productinfo'] = $this->finaltestanchormodel->get_productinfo_byid($productid);
			$this->_data['incomelist'] = $this->finaltestanchormodel->get_incomeid_byproductid($productid);
			$this->_data['supplierinfo'] = $this->finaltestanchormodel->get_supplierinfo_byid($this->_data['testinfo']->supplier_id);
			
			$this->admin_library->view("final_test_anchor/main_edit", $this->_data);
			$this->admin_library->output();
			
		}else{
			
			$this->finaltestanchormodel->edit_income_anchor_test($testid);
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("final_test_anchor/final_chemical_testing/".$productid."/".$finalid."/".$testid);
			
		}
		
	}
	
	function final_chemical_testing($productid=0, $finalid=0, $testid=0){
			
		$this->admin_model->set_menu_key('a74aac1ad23b00c99681f7258c897eed');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_library->setTitle('Final Test Anchor','icon-folder-open');
		
		$this->form_validation->set_rules('chemical_test_using','Use?','trim|required');
		$this->form_validation->set_rules('test_value[]','Value','trim');
		$this->form_validation->set_rules('chemical_id[]','Chemical ID','trim');
		$this->form_validation->set_rules('chemical_value[]','Chemical Value','trim');
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			$this->_data['testinfo'] = $this->finaltestanchormodel->get_testinfo_byid($testid);
			$this->_data['productinfo'] = $this->finaltestanchormodel->get_productinfo_byid($productid);
			$this->_data['productid'] = $productid;
			$this->_data['finalid'] = $finalid;
			$this->_data['testid'] = $testid;
			$test_status = $this->finaltestanchormodel->get_chemical_final_status_bytestid($finalid, $testid);
			if($test_status>0){
				$this->_data['test_status'] = 'update';
				$this->_data['chemicallist'] = $this->finaltestanchormodel->get_chemicaltestlist_bytestid($testid);
				$this->admin_library->view("final_test_anchor/chemical_edit", $this->_data);
			}else{
				$this->_data['chemicallist'] = $this->finaltestanchormodel->get_chemicallist_byincomeid($this->_data['testinfo']->income_id);
				$this->_data['test_status'] = 'insert';
				$this->admin_library->view("final_test_anchor/chemical", $this->_data);
			}
			$this->admin_library->output();
			
		}else{
			
			$this->finaltestanchormodel->add_chemical_anchor_test();
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("final_test_anchor/final_material_testing/".$productid."/".$finalid."/".$testid);
			
		}
	}
	
	function final_material_testing($productid=0, $finalid=0, $testid=0){
		
		$this->admin_model->set_menu_key('a74aac1ad23b00c99681f7258c897eed');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_library->setTitle('Final Test Anchor','icon-folder-open');
		
		$this->form_validation->set_rules('material_test_using','Use ?','trim|required');
		$this->form_validation->set_rules('description[]','','trim');
		$this->form_validation->set_rules('ps_min_unit[]','','trim');
		$this->form_validation->set_rules('ps_max_unit[]','','trim');
		$this->form_validation->set_rules('ps_min[]','','trim');
		$this->form_validation->set_rules('ps_max[]','','trim');
		$this->form_validation->set_rules('test_result[]','','trim');
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			
			$this->_data['testinfo'] = $this->finaltestanchormodel->get_testinfo_byid($testid);
			
			$this->_data['anchortesting'] = $this->finaltestanchormodel->get_anchortestlist_byincomeid($this->_data['testinfo']->income_id);
			$this->_data['productinfo'] = $this->finaltestanchormodel->get_productinfo_byid($productid);
			$this->_data['productid'] = $productid;
			$this->_data['finalid'] = $finalid;
			$this->_data['testid'] = $testid;
			
			$test_status = $this->finaltestanchormodel->get_material_final_status_bytestid($testid);
			if($test_status>0){
				$this->_data['materiallist'] = $this->finaltestanchormodel->get_materiallist_bytestid($testid);
				$this->_data['test_status'] = 'update';
				$this->admin_library->view('final_test_anchor/material_edit', $this->_data);
			}else{
				$this->_data['materiallist'] = $this->finaltestanchormodel->get_materiallist_byincomeid($this->_data['testinfo']->income_id);
				$this->_data['test_status'] = 'insert';
				$this->admin_library->view('final_test_anchor/material', $this->_data);
			}
			
			$this->admin_library->output();
			
		}else{
			
			$this->finaltestanchormodel->add_material_anchor_test($testid);
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("final_test_anchor/final_galvanized_testing/".$productid."/".$finalid."/".$testid);
			
		}
		
	}
	
	function final_material_edit_testing($productid=0, $finalid=0, $testid=0){
		
		$this->admin_model->set_menu_key('a74aac1ad23b00c99681f7258c897eed');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_library->setTitle('Final Test Anchor','icon-folder-open');
		
		$this->form_validation->set_rules('material_test_using','Use ?','trim|required');
		$this->form_validation->set_rules('test_value[]','','trim');
		$this->form_validation->set_rules('ps_id[]','','trim');
		$this->form_validation->set_rules('description[]','','trim');
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			
			$this->_data['testinfo'] = $this->finaltestanchormodel->get_testinfo_byid($testid);
			$this->_data['materiallist'] = $this->finaltestanchormodel->get_materiallist_byincomeid($this->_data['testinfo']->income_id);
			$this->_data['productid'] = $productid;
			$this->_data['finalid'] = $finalid;
			
			$this->admin_library->view('final_test_anchor/edit_material', $this->_data);
			$this->admin_library->output();
			
		}else{
			
			$this->finaltestanchormodel->edit_material_anchor_test($testid);
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("final_test_anchor/final_galvanized_edit_testing/".$productid."/".$finalid."/".$testid);
			
		}
		
	}
	
	function final_galvanized_testing($productid=0, $finalid=0, $testid=0){
		
		$this->admin_model->set_menu_key('a74aac1ad23b00c99681f7258c897eed');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$productinfo = $this->finaltestanchormodel->get_productinfo_byid($productid);
		
		$this->admin_library->setTitle('Final Test Anchor','icon-folder-open');
		
		$this->form_validation->set_rules('galvanized_test_using','Galvanized Use ?','trim');
		$this->form_validation->set_rules('standard_id','Standard','trim');
		$this->form_validation->set_rules('thickness_1','','trim');
		$this->form_validation->set_rules('thickness_2','','trim');
		$this->form_validation->set_rules('thickness_3','','trim');
		$this->form_validation->set_rules('thickness_4','','trim');
		$this->form_validation->set_rules('thickness_5','','trim');
		$this->form_validation->set_rules('average','','trim');
		$this->form_validation->set_rules('heattreatment_test_using','Heat Treatment Use ?','trim');
		$this->form_validation->set_rules('quenching_temperature','Quenching Temperature','trim');
		$this->form_validation->set_rules('quenching_min','Quenching Min','trim');
		$this->form_validation->set_rules('quenching_max','Quenching Max','trim');
		$this->form_validation->set_rules('quenching_test','Quenching Test','trim');
		$this->form_validation->set_rules('tempering_temperature','Tempering Temperature','trim');
		$this->form_validation->set_rules('tempering_min','Tempering Min','trim');
		$this->form_validation->set_rules('tempering_max','Tempering Max','trim');
		$this->form_validation->set_rules('tempering_test','Tempering Test','trim');
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			
			$this->_data['testinfo'] = $this->finaltestanchormodel->get_testinfo_byid($testid);
			$this->_data['standardlist'] = $this->finaltestanchormodel->get_standardlist();
			$this->_data['productinfo'] = $this->finaltestanchormodel->get_productinfo_byid($productid);
			$this->_data['test_status'] = $this->finaltestanchormodel->get_teststatus_byproductidandfinalid($productid, $finalid);
			$this->_data['productid'] = $productid;
			$this->_data['finalid'] = $finalid;
			$this->_data['testid'] = $testid;
			
			$this->admin_library->view('final_test_anchor/galvanized', $this->_data);
			$this->admin_library->output();
			
		}else{
			
			$this->finaltestanchormodel->add_galvanized_anchor_test($testid);
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("final_manager_anchor/final_list/".$productinfo->product_type."/".$productinfo->product_id);
			
		}
		
	}
	
	function final_galvanized_edit_testing($productid=0, $finalid=0, $testid=0){
		
		$this->admin_model->set_menu_key('a74aac1ad23b00c99681f7258c897eed');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$productinfo = $this->finaltestanchormodel->get_productinfo_byid($productid);
		
		$this->admin_library->setTitle('Final Test Anchor','icon-folder-open');
		
		$this->form_validation->set_rules('standard_id','Standard','trim|required');
		$this->form_validation->set_rules('thickness[]','','trim');
		$this->form_validation->set_rules('average','','trim|required');
		$this->form_validation->set_rules('quenching_temperature','Quenching Temperature','trim|required');
		$this->form_validation->set_rules('quenching_min','Quenching Min','trim|required');
		$this->form_validation->set_rules('quenching_max','Quenching Max','trim|required');
		$this->form_validation->set_rules('quenching_test','Quenching Test','trim|required');
		$this->form_validation->set_rules('tempering_temperature','Tempering Temperature','trim|required');
		$this->form_validation->set_rules('tempering_min','Tempering Min','trim|required');
		$this->form_validation->set_rules('tempering_max','Tempering Max','trim|required');
		$this->form_validation->set_rules('tempering_test','Tempering Test','trim|required');
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			
			$this->_data['testinfo'] = $this->finaltestanchormodel->get_testinfo_byid($testid);
			$this->_data['productid'] = $productid;
			$this->_data['finalid'] = $finalid;
			
			$this->admin_library->view('final_test_anchor/edit_galvanized', $this->_data);
			$this->admin_library->output();
			
		}else{
			
			$this->finaltestanchormodel->add_galvanized_anchor_test($testid);
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("final_manager_anchor/final_list/".$productinfo->product_type."/".$productinfo->product_id);
			
		}
		
	}
	
	function get_incomeinfo($incomeid=0){
		$incomeinfo = $this->finaltestanchormodel->get_incomeinfo_byincomeid($incomeid);
		
		$this->json->set('income_info', $incomeinfo);
		$this->json->send();
	}
}

?>