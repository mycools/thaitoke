<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Chemical_test extends CI_Controller {
	var $seq = 0;
	var $_data=array();
	function __construct(){
		parent::__construct();
		
		$this->load->library('admin_library');
		$this->load->model('administrator/admin_model');
		$this->load->model('administrator/menu_model');
		$this->load->model('administrator/chemical_test_model');
		$this->admin_library->forceLogin();
		
	}
	
	public function test_bolt($incomeid=0){
		
		$this->admin_model->set_menu_key('a8ff0b967d8e9cda6122183af245d1ae');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('test_value[]','Value','trim');
		$this->form_validation->set_rules('chemical_id[]','Chemical ID','trim');
		$this->form_validation->set_rules('chemical_value[]','Chemical Value','trim');
		$this->form_validation->set_rules('test_status','Test Status','trim');
		
		$incomeinfo = $this->chemical_test_model->get_incomeinfo_bolt_byid($incomeid);
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("Bolt incoming",'income_manager/bolt_list',"icon-folder-open");
			$this->admin_library->add_breadcrumb("ค่าทดสอบ Chemical ของ Bolt",current_url(),"icon-plus");
			$this->_data['row'] = $this->chemical_test_model->get_chemical_bolt($incomeinfo['income_product_id']);
			
			$this->_data['list'] = $this->chemical_test_model->get_thickness();
			
			$this->_data['income_id'] = $incomeid;
			$this->_data['incomeinfo'] = $this->chemical_test_model->get_incomeinfo_bolt_byid($incomeid);
			$this->_data['thicknessinfo'] = $this->chemical_test_model->get_boltthicknessinfo_byid($incomeid);
			
			$this->admin_library->view("chemical_test/test_bolt", $this->_data);
			$this->admin_library->output();
		}else{
			$this->chemical_test_model->update_income_status($incomeid);
			$this->chemical_test_model->update_product_qty($incomeinfo['size_id'],$incomeinfo['income_quantity'], $incomeid, $this->input->post('test_status'));
			$this->chemical_test_model->bolt_test_add($incomeid);
			$this->chemical_test_model->bolt_thickness_add($incomeid);
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("income_manager/bolt_list/");
		}
		
	}
	
	public function test_nut($incomeid=0){
	
		$this->admin_model->set_menu_key('1bdb9da104e5796f401cca761ef1c9f2');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('test_value[]','Value','trim');
		$this->form_validation->set_rules('chemical_id[]','Chemical ID','trim');
		$this->form_validation->set_rules('chemical_value[]','Chemical Value','trim');
		$this->form_validation->set_rules('test_status','Test Status','trim');
		
		$incomeinfo = $this->chemical_test_model->get_incomeinfo_nut_byid($incomeid);
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("Nut incoming",'income_manager/nut_list',"icon-folder-open");
			$this->admin_library->add_breadcrumb("ค่าทดสอบ Chemical ของ nut",current_url(),"icon-plus");
			$this->_data['row'] = $this->chemical_test_model->get_chemical_nut($incomeinfo['income_product_id']);
			
			$this->_data['list'] = $this->chemical_test_model->get_thickness();
			
			$this->_data['income_id'] = $incomeid;
			$this->_data['incomeinfo'] = $this->chemical_test_model->get_incomeinfo_nut_byid($incomeid);
			$this->_data['thicknessinfo'] = $this->chemical_test_model->get_nutthicknessinfo_byid($incomeid);
			
			$this->admin_library->view("chemical_test/test_nut", $this->_data);
			$this->admin_library->output();
		}else{
			
			$this->chemical_test_model->update_income_status_nut($incomeid);
			$value = $this->chemical_test_model->check_chemical_test_nut_by_incomeid($incomeid);
			$this->chemical_test_model->update_product_nut_qty($incomeinfo['size_id'],$incomeinfo['income_quantity'], $incomeid, $this->input->post('test_status'));
			$this->chemical_test_model->nut_test_add($incomeid);
			$this->chemical_test_model->nut_thickness_add($incomeid);
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("income_manager/nut_list/");
		}
	}
	
	public function test_washer($incomeid=0){
	
		$this->admin_model->set_menu_key('833675c0ae6c061f4e8d87c294ebb1ec');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('test_value[]','Value','trim');
		$this->form_validation->set_rules('chemical_id[]','Chemical ID','trim');
		$this->form_validation->set_rules('chemical_value[]','Chemical Value','trim');
		$this->form_validation->set_rules('test_status','Test Status','trim');
		
		$incomeinfo = $this->chemical_test_model->get_incomeinfo_washer_byid($incomeid);
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("Washer incoming",'income_manager/washer_list',"icon-folder-open");
			$this->admin_library->add_breadcrumb("ค่าทดสอบ Chemical ของ washer",current_url(),"icon-plus");
			$this->_data['row'] = $this->chemical_test_model->get_chemical_washer($incomeinfo['income_product_id']);
			
			$this->_data['list'] = $this->chemical_test_model->get_thickness();
			
			$this->_data['income_id'] = $incomeid;
			$this->_data['incomeinfo'] = $this->chemical_test_model->get_incomeinfo_washer_byid($incomeid);
			$this->_data['thicknessinfo'] = $this->chemical_test_model->get_washerthicknessinfo_byid($incomeid);
			
			$this->admin_library->view("chemical_test/test_washer", $this->_data);
			$this->admin_library->output();
		}else{
		
			$this->chemical_test_model->update_income_status_washer($incomeid);
			$value = $this->chemical_test_model->check_chemical_test_washer_by_incomeid($incomeid);
			$this->chemical_test_model->update_product_washer_qty($incomeinfo['size_id'],$incomeinfo['income_quantity'], $incomeid, $this->input->post('test_status'));
			$this->chemical_test_model->washer_test_add($incomeid);
			$this->chemical_test_model->washer_thickness_add($incomeid);
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("income_manager/washer_list/");
		}
	}
	
	public function test_spring($incomeid=0){
	
		$this->admin_model->set_menu_key('833675c0ae6c061f4e8d87c294ebb1ec');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('test_value[]','Value','trim');
		$this->form_validation->set_rules('chemical_id[]','Chemical ID','trim');
		$this->form_validation->set_rules('chemical_value[]','Chemical Value','trim');
		$this->form_validation->set_rules('test_status','Test Status','trim');
		
		$incomeinfo = $this->chemical_test_model->get_incomeinfo_spring_byid($incomeid);
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("spring incoming",'income_manager/spring_list',"icon-folder-open");
			$this->admin_library->add_breadcrumb("ค่าทดสอบ Chemical ของ spring",current_url(),"icon-plus");
			$this->_data['row'] = $this->chemical_test_model->get_chemical_spring($incomeinfo['income_product_id']);
			
			$this->_data['list'] = $this->chemical_test_model->get_thickness();
			
			$this->_data['income_id'] = $incomeid;
			$this->_data['incomeinfo'] = $this->chemical_test_model->get_incomeinfo_bolt_byid($incomeid);
			$this->_data['thicknessinfo'] = $this->chemical_test_model->get_springthicknessinfo_byid($incomeid);
			
			$this->admin_library->view("chemical_test/test_spring", $this->_data);
			$this->admin_library->output();
		}else{
		
			$this->chemical_test_model->update_income_status_spring($incomeid);
			$value = $this->chemical_test_model->check_chemical_test_spring_by_incomeid($incomeid);
			if($value<=0){
				$this->chemical_test_model->update_product_spring_qty($incomeinfo['size_id'],$incomeinfo['income_quantity']);
			}
			$this->chemical_test_model->spring_test_add($incomeid);
			$this->chemical_test_model->spring_thickness_add($incomeid);
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("income_manager/spring_list/");
		}
	}
	
	public function test_taper($incomeid=0){
	
		$this->admin_model->set_menu_key('88a9d3d54bc47e5eb550ab1eb54a43ba');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('test_value[]','Value','trim');
		$this->form_validation->set_rules('chemical_id[]','Chemical ID','trim');
		$this->form_validation->set_rules('chemical_value[]','Chemical Value','trim');
		$this->form_validation->set_rules('test_status','Test Status','trim');
		
		$incomeinfo = $this->chemical_test_model->get_incomeinfo_taper_byid($incomeid);
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("Taper incoming",'income_manager/taper_list',"icon-folder-open");
			$this->admin_library->add_breadcrumb("ค่าทดสอบ Chemical ของ Taper washer",current_url(),"icon-plus");
			$this->_data['row'] = $this->chemical_test_model->get_chemical_taper($incomeinfo['income_product_id']);
			
			$this->_data['list'] = $this->chemical_test_model->get_thickness();
			
			$this->_data['income_id'] = $incomeid;
			$this->_data['incomeinfo'] = $this->chemical_test_model->get_incomeinfo_bolt_byid($incomeid);
			$this->_data['thicknessinfo'] = $this->chemical_test_model->get_taperthicknessinfo_byid($incomeid);
			
			$this->admin_library->view("chemical_test/test_taper", $this->_data);
			$this->admin_library->output();
		}else{
			$this->chemical_test_model->update_income_status_taper($incomeid);
			$value = $this->chemical_test_model->check_chemical_test_taper_by_incomeid($incomeid);
			if($value<=0){
				$this->chemical_test_model->update_product_taper_qty($incomeinfo['size_id'],$incomeinfo['income_quantity']);
			}
			$this->chemical_test_model->taper_test_add($incomeid);
			$this->chemical_test_model->taper_thickness_add($incomeid);
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("income_manager/taper_list/");
		}
	}
	
	public function test_stud($incomeid=0){
	
		$this->admin_model->set_menu_key('7b21c48a8493e416f7bf0a1e2e17f1da');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('test_value[]','Value','trim');
		$this->form_validation->set_rules('chemical_id[]','Chemical ID','trim');
		$this->form_validation->set_rules('chemical_value[]','Chemical Value','trim');
		$this->form_validation->set_rules('test_status','Test Status','trim');
		
		$incomeinfo = $this->chemical_test_model->get_incomeinfo_stud_byid($incomeid);
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("Stud incoming",'income_manager/stud_list',"icon-folder-open");
			$this->admin_library->add_breadcrumb("ค่าทดสอบ Chemical ของ Stud",current_url(),"icon-plus");
			$this->_data['row'] = $this->chemical_test_model->get_chemical_stud($incomeinfo['income_product_id']);
			
			$this->_data['list'] = $this->chemical_test_model->get_thickness();
			
			$this->_data['income_id'] = $incomeid;
			$this->_data['incomeinfo'] = $this->chemical_test_model->get_incomeinfo_bolt_byid($incomeid);
			$this->_data['thicknessinfo'] = $this->chemical_test_model->get_studthicknessinfo_byid($incomeid);
			
			$this->admin_library->view("chemical_test/test_stud", $this->_data);
			$this->admin_library->output();
		}else{
			$this->chemical_test_model->update_income_status_stud($incomeid);
			$value = $this->chemical_test_model->check_chemical_test_stud_by_incomeid($incomeid);
			if($value<=0){
				$this->chemical_test_model->update_product_stud_qty($incomeinfo['size_id'],$incomeinfo['income_quantity']);
			}
			$this->chemical_test_model->stud_test_add($incomeid);
			$this->chemical_test_model->stud_thickness_add($incomeid);
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("income_manager/stud_list/");
		}
	}
	
	public function get_thickness_value(){
		$thicknessid = $this->input->post('thickness_id');
		$thicknessinfo = $this->chemical_test_model->get_thickness_value_byid($thicknessid);
		if($thicknessinfo){
			echo $thicknessinfo['condition'].' '.$thicknessinfo['condition_value'];
		}
	}
	
}