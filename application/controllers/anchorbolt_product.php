<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Anchorbolt_product extends CI_Controller {
	var $seq = 0;
	var $_data=array();
	function __construct(){
		parent::__construct();
		
		$this->load->library('admin_library');
		$this->load->model('administrator/admin_model');
		$this->load->model('administrator/menu_model');
		$this->load->model('administrator/anchorbolt_productmodel');
		$this->admin_library->forceLogin();
	}
	
	public function product_list($type='axel', $sizeid=0, $page=1){
		$page = ((int) $page < 1)?1:(int) $page;
		$offset = ($page-1)*25;
		$this->seq=$offset;
		$this->admin_model->set_menu_key('c79e7f7bcabf6bb485f3fc0fa8d3ca01');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$back_id = $this->anchorbolt_productmodel->get_back_product_id($sizeid);
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		$this->admin_model->set_top_button("เพิ่ม สินค้า","anchorbolt_product/add/".$type."/".$sizeid ,'icon-plus','btn-success','w');
		$this->admin_model->set_top_button("กลับไปยังหน้าsize","manage_anchorbolt/size_list/".$type."/".$back_id ,'icon-reply','btn-primary','w');
		/* $this->admin_model->set_top_button("สำรองข้อมูล","menu_manager/backup",'icon-download','btn-primary','s'); */
		$this->admin_model->set_datatable($this->anchorbolt_productmodel->dataTable($sizeid,$type, 25,$offset));
		$this->admin_model->set_column("product_no","ลำดับ",'5%');
		$this->admin_model->set_column("product_grade","ชื่อ",'15%');
		$this->admin_model->set_column("product_cate","กลุ่มสินค้า",'15%');
		$this->admin_model->set_column_callback("product_no","show_seq");
		
		$this->admin_model->set_action_button("Final","final_manager_anchor/final_list/".$type."/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("แก้ไข","anchorbolt_product/edit/".$type."/".$sizeid."/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("ลบ","anchorbolt_product/getdelete/".$type."/[product_id]",'icon-trash-o','btn-danger','d');
		
		
		//$this->admin_model->set_action_button("Chemical","manageproduct/bolt_chemical/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->show_search_text("anchorbolt_product/product_list/".$type."/".$sizeid);
		$this->admin_model->set_pagination("anchorbolt_product/product_list/".$type."/".$sizeid,$this->anchorbolt_productmodel->get_count($sizeid, $type),25,3);
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function add($type='axel', $sizeid=0){
		
		$this->admin_model->set_menu_key('c79e7f7bcabf6bb485f3fc0fa8d3ca01');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('product_grade','Grade','trim|required');
		$this->form_validation->set_rules('product_std','Standard','trim|required');
		$this->form_validation->set_rules('product_cate','Type','trim|required');
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("เพิ่มสินค้า","anchorbolt_product/add/".$type."/".$sizeid,"icon-plus");
			$this->_data['type'] = $type;
			$this->_data['size_id'] = $sizeid;
			$this->admin_library->view("anchorbolt_product/add", $this->_data);
			$this->admin_library->output();
		}else{
			$this->anchorbolt_productmodel->add();
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("anchorbolt_product/product_list/".$type."/".$sizeid);
		}
	}
	
	public function edit($type='axel', $sizeid=0, $productid=0){
		
		$this->admin_model->set_menu_key('c79e7f7bcabf6bb485f3fc0fa8d3ca01');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('product_grade','Grade','trim|required');
		$this->form_validation->set_rules('product_std','Standard','trim|required');
		$this->form_validation->set_rules('product_cate','Type','trim|required');
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("แก้ไขสินค้า","anchorbolt_product/edit/".$type."/".$sizeid."/".$productid,"icon-plus");
			$this->_data['type'] = $type;
			$this->_data['size_id'] = $sizeid;
			$this->_data['row'] = $this->anchorbolt_productmodel->getinfo($productid);
			$this->admin_library->view("anchorbolt_product/edit", $this->_data);
			$this->admin_library->output();
		}else{
			$update = $this->anchorbolt_productmodel->edit($productid);
			if(!$update){
				$this->_data['error_message'] =  $this->mobile_upload->display_errors();
				$this->_data['row'] = $this->anchorbolt_productmodel->getinfo($productid);
				$this->admin_library->view("anchorbolt_product/edit", $this->_data);
				$this->admin_library->output();
			}else{
				$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
				admin_redirect("anchorbolt_product/product_list/".$type."/".$sizeid);
			}
			
		}
	}
	
	public function getdelete($type='axel', $productid=0){
		$this->anchorbolt_productmodel->getdelete($productid);
		$this->session->set_flashdata("message-success",$message);
		admin_redirect("anchorbolt_product/index/".$type);
	}
	
	public function size_list($type='axel', $productid=0, $page=1){
		$page = ((int) $page < 1)?1:(int) $page;
		$offset = ($page-1)*25;
		$this->seq=$offset;
		$this->admin_model->set_menu_key('c79e7f7bcabf6bb485f3fc0fa8d3ca01');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		if($type == "axel"){
		$this->admin_model->set_top_button("เพิ่ม Anchor Bolt Size","anchorbolt_product/size_add/".$type."/".$productid,'icon-plus','btn-success','w');
		}else{
		$this->admin_model->set_top_button("เพิ่ม Anchor Bolt Size","anchorbolt_product/size_add_sheet/".$type."/".$productid,'icon-plus','btn-success','w');	
		}
		$this->admin_model->set_top_button("กลับไปยังหน้ารายการสินค้า Anchor Bolt",'anchorbolt_product/index/'.$type,'icon-reply','btn-primary','w');
		/* $this->admin_model->set_top_button("สำรองข้อมูล","menu_manager/backup",'icon-download','btn-primary','s'); */
		$this->admin_model->set_datatable($this->anchorbolt_productmodel->size_dataTable($productid, $type, 25,$offset));
		$this->admin_model->set_column("product_no","ลำดับ",'5%');
		if($type == "axel"){
		$this->admin_model->set_column("size_m","M",'15%');
		$this->admin_model->set_column("size_length","Length",'15%');
		}else{
		$this->admin_model->set_column("size_t","T",'15%');
		$this->admin_model->set_column("size_w","W",'15%');
		$this->admin_model->set_column("size_length","Length",'15%');	
		}
		//$this->admin_model->set_column("product_standard","Standard",'50%');
		$this->admin_model->set_column_callback("product_no","show_seq");
		
		if($type == "axel"){
		$this->admin_model->set_action_button("แก้ไข","anchorbolt_product/size_edit/".$type."/[product_id]/[size_id]",'icon-edit','btn-info','w');
		}else{
		$this->admin_model->set_action_button("แก้ไข","anchorbolt_product/size_edit_sheet/".$type."/[product_id]/[size_id]",'icon-edit','btn-info','w');	
		}
		$this->admin_model->set_action_button("ลบ","anchorbolt_product/size_delete/".$type."/[size_id]",'icon-trash-o','btn-danger','d');
		//$this->admin_model->set_action_button("Chemical","anchorbolt_product/anchor_chemical/".$type."/[product_id]/[size_id]",'icon-edit','btn-info','w');
		$this->admin_model->show_search_text("anchorbolt_product/size_list/".$type."/".$productid);
		$this->admin_model->set_pagination("anchorbolt_product/size_list/".$type."/".$productid,$this->anchorbolt_productmodel->get_size_count($productid, $type),25,3);
		$this->admin_model->make_list();
		$this->admin_library->output();
	}
	
	public function size_add($type='axel', $productid=0){
		
		$this->admin_model->set_menu_key('c79e7f7bcabf6bb485f3fc0fa8d3ca01');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('size_m','M','trim|required');
		$this->form_validation->set_rules('size_length','Length','trim|required');
		
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("เพิ่ม Anchor Bolt Size","anchorbolt_product/add","icon-plus");
			$this->_data['product_id'] = $productid;
			$this->_data['type'] = $type;
			//$this->_data['row'] = $this->anchorbolt_productmodel->get_cer_std("1");
			$this->admin_library->view("anchorbolt_product/size_add", $this->_data);
			$this->admin_library->output();
		}else{
			$size_id = $this->anchorbolt_productmodel->addsize();
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("anchorbolt_product/size_list/".$type."/".$productid);
		}
		
	}
	
	public function size_edit($type='axel', $productid=0, $sizeid=0){
		$this->admin_model->set_menu_key('c79e7f7bcabf6bb485f3fc0fa8d3ca01');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('size_m','M','trim|required');
		$this->form_validation->set_rules('size_length','Length','trim|required');
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("แก้ไข Anchor Bolt Size","anchorbolt_product/edit/".$productid.'/'.$sizeid,"icon-plus");
			$this->_data['row'] = $this->anchorbolt_productmodel->get_sizeinfo_byid($sizeid);
			$this->_data['type'] = $type;
			$this->admin_library->view("anchorbolt_product/size_edit", $this->_data);
			$this->admin_library->output();
		}else{
			$this->anchorbolt_productmodel->editsize($sizeid);
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("anchorbolt_product/size_list/".$type."/".$productid);
		}
		
	}
	
	
	public function size_add_sheet($type='sheet', $productid=0){
		
		$this->admin_model->set_menu_key('c79e7f7bcabf6bb485f3fc0fa8d3ca01');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('size_t','T','trim|required');
		$this->form_validation->set_rules('size_w','W','trim|required');
		$this->form_validation->set_rules('size_length','Length','trim|required');	
	
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("เพิ่ม Anchor Bolt Size","anchorbolt_product/add","icon-plus");
			$this->_data['product_id'] = $productid;
			$this->_data['type'] = $type;
			$this->_data['row'] = $this->anchorbolt_productmodel->get_cer_std("1");
			$this->admin_library->view("anchorbolt_product/size_add_sheet", $this->_data);
			$this->admin_library->output();
		}else{
			$size_id = $this->anchorbolt_productmodel->addsize_sheet();
			$this->anchorbolt_productmodel->anchor_standard_add($size_id);
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("anchorbolt_product/size_list/".$type."/".$productid);
		}
		
	}
	
	public function size_edit_sheet($type='sheet', $productid=0, $sizeid=0){
		$this->admin_model->set_menu_key('c79e7f7bcabf6bb485f3fc0fa8d3ca01');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$this->form_validation->set_rules('size_t','T','trim|required');
		$this->form_validation->set_rules('size_w','W','trim|required');
		$this->form_validation->set_rules('size_length','Length','trim|required');
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("แก้ไข Anchor Bolt Size","anchorbolt_product/edit/".$productid.'/'.$sizeid,"icon-plus");
			$this->_data['row'] = $this->anchorbolt_productmodel->get_sizeinfo_byid($sizeid);
			$this->_data['type'] = $type;
			$this->_data['list'] = $this->anchorbolt_productmodel->get_cer_std_foredit($sizeid);
			$this->admin_library->view("anchorbolt_product/size_edit_sheet", $this->_data);
			$this->admin_library->output();
		}else{
			$this->anchorbolt_productmodel->editsize_sheet($sizeid);
			$this->anchorbolt_productmodel->anchor_standard_edit($sizeid);
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("anchorbolt_product/size_list/".$type."/".$productid);
		}
		
	}
	
	
	public function size_delete($type='axel', $productid=0, $size_id=0){
		$this->anchorbolt_productmodel->deletesize($size_id);
		$this->session->set_flashdata("message-success",$message);
		admin_redirect("anchorbolt_product/size_list/".$type."/".$productid);
	}
	
	public function standard($type='axel', $productid=0){
		$this->admin_model->set_menu_key('c79e7f7bcabf6bb485f3fc0fa8d3ca01');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('ps_min','ค่า MIN','trim|required');
		$this->form_validation->set_rules('ps_min_unit','หน่วยของ MIN','trim|required');
		$this->form_validation->set_rules('ps_max','ค่า MAX','trim|required');
		$this->form_validation->set_rules('ps_max_unit','หน่วยของ MAX','trim|required');
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("Anchor Bolt Size Standard","anchorbolt_product/standard/".$productid,"icon-plus");
			$this->_data['row'] = $this->anchorbolt_productmodel->get_standardinfo_byid($productid);
			if($this->_data['row']){
				$this->_data['product_id'] = $this->_data['row']['product_id'];
				$this->_data['update_status'] = 'update';
			}else{
				$this->_data['product_id'] = $productid;
				$this->_data['update_status'] = 'insert';
			}
			$this->_data['type'] = $type;
			$this->admin_library->view("anchorbolt_product/standard", $this->_data);
			$this->admin_library->output();
		}else{
			$this->anchorbolt_productmodel->update_standard();
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("anchorbolt_product/standard/".$type."/".$productid);
		}
	}
	
	public function anchor_chemical($type='axel',$incomeid=0){
		
		if($type=="axel"){
			$this->admin_model->set_menu_key('ca4e503a3c9fe5af42d1e7d4c18e1abc');
		}else{
			$this->admin_model->set_menu_key('91f3758c72ce63009ee6d3ee769df48e');
		}
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('ps_min[]','Chemical value (%) [MIN]','trim');
		$this->form_validation->set_rules('ps_max[]','Chemical value (%) [MAX]','trim');
		$this->form_validation->set_rules('chemical_result[]','Chemical result','trim');
		/*
$this->form_validation->set_rules('chemical_value[]','Chemical (%)','trim');
		$this->form_validation->set_rules('chemical_composition[]','Composition','trim');
*/
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("การจัดการค่า Chemical ของ Anchor",current_url(),"icon-plus");
			$chemicallist = $this->anchorbolt_productmodel->anchor_chemicallist($incomeid);
			if($chemicallist){
				$this->_data['status'] = 'edit';
				//$this->_data['product_id'] = $productid;
				//$pro = $this->anchorbolt_productmodel->anchor_getinfo($productid);
				$this->_data['row'] = $this->anchorbolt_productmodel->get_cer_chem_foredit($incomeid);
			}else{
				$this->_data['status'] = 'add';
				//$this->_data['product_id'] = $productid;
				//$pro = $this->anchorbolt_productmodel->anchor_getinfo($productid);
				$this->_data['row'] = $this->anchorbolt_productmodel->get_cer_chem("10");
			}
			$this->_data['type'] = $type;
			$this->admin_library->view('anchorbolt_product/anchor_chemical', $this->_data);
			$this->admin_library->output();
		}else{
			if($_POST['process_status']=='edit'){
				$this->anchorbolt_productmodel->anchor_chemical_edit($incomeid);
			}else{
				$this->anchorbolt_productmodel->anchor_chemical_add($incomeid);	
			}
			$this->session->set_flashdata("message-success","<div class='alert alert-success'><button class='close' data-dismiss='alert'>x</button><strong>การทำรายการเสร็จสมบูรณ์</strong>บันทึกเรียบร้อยแล้ว</div>");
			admin_redirect("income_manager_anchor/anchor_list/".$type);
		}
		
	}
	
	/* Default function - Start */
	public function show_seq($text,$row)
	{
		$this->seq++;
		return $this->seq;
		
	}
	
	public function get_image($text,$row){
		if($row['product_thumbnail']!=''){
			return '<img src="'.site_url("src/60/product/anchor_bolt/".$row['product_thumbnail']).'" alt="" />';
		}else{
			return '<img src="'.site_url("src/60/product/anchor_bolt/default.jpg").'" alt="" />';
		}
	}
	
	public function get_type($text,$row){
		if($row['product_type']=='axel'){
			return 'Axel';
		}else if($row['product_type']=='sheet'){
			return 'Sheet';
		}else{
			return 'No type';
		}
	}
	
	public function get_carbon_status($text,$row){
		if($row['product_low_carbon']==1){
			return '<i class="icon-check"></i>';
		}else{
			return '';
		}
	}
	
	
	
	/* Default function - End */
}

?>