<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
File Name 	: income_manager.php
Controller	: Income_manager
Create By 	: Jarak Kritkiattisak
Create Date 	: 7/6/2557 BE
Project 	: iAon Project
Version 		: 1.0
*/
class Income_manager_anchor extends CI_Controller {
	var $_data=array();
	public function __construct()
	{
		parent::__construct();
		$this->load->library('admin_library');
		$this->load->model('administrator/admin_model');
		$this->load->model('administrator/anchor_income');
		//$this->load->model('administrator/anchor_stock');
		
		$this->admin_library->forceLogin();
		$this->admin_model->set_menu_key('56a6cb0114f46400034d578425adc423');
	}
	public function index()
	{
		redirect("income_manager_anchor/anchor_list");
	}
	/* 
	anchor manager - Start 
	*/
	public function anchor_list($type,$page=1)
	{
		
		$QUERY_STRING = @$_SERVER['QUERY_STRING'];
		$QUERY_STRING = (trim($QUERY_STRING))?"?".$QUERY_STRING:"";
		if($type=="axel"){
			$this->admin_model->set_menu_key('ca4e503a3c9fe5af42d1e7d4c18e1abc');
		}else{
			$this->admin_model->set_menu_key('91f3758c72ce63009ee6d3ee769df48e');
		}
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$limit=25;
		$offset = ($page-1)*$limit;
		$offset = ($offset <0)?0:$offset;
		
		$this->admin_library->setTitle('anchor Incoming','icon-folder-open');
		
		$this->admin_library->setDetail("anchor incoming manager");
		$this->admin_model->initd($this);
		$this->admin_model->set_title('anchor incoming manager','icon-book');
		
		$this->admin_model->set_datatable($this->anchor_income->dataTable($type,$limit,$offset));
		
		$this->admin_model->set_column("income_id","No.",0,"icon-sort-alpha-asc");
		$this->admin_model->set_column("income_job_id","INCOMING ID.",0,"icon-folder-open");
		$this->admin_model->set_column("income_product_id","Product",0,"icon-shopping-cart");
		$this->admin_model->set_column("size_id","Size",0,"icon-shopping-cart");
		$this->admin_model->set_column("income_supplier_id","Supplier",0,"icon-shopping-cart");
		
		$this->admin_model->set_column("income_createdtime","Create Date",0,"icon-calendar");
		$this->admin_model->set_column("income_manager","ผู้จัดการข้อมูล",0,"icon-user");
		$this->admin_model->set_column("test_status","สถานะการทดสอบ",0,'icon-code');
		
		$this->admin_model->set_column_callback("income_product_id","product_name_callback");
		$this->admin_model->set_column_callback("income_supplier_id","supplier_name_callback");
		$this->admin_model->set_column_callback("income_manager","lot_manager_callback");
		$this->admin_model->set_column_callback("income_createdtime","date_callback");
		$this->admin_model->set_column_callback("test_status","test_status_callback");
		$this->admin_model->set_column_callback("size_id","product_size_callback");

		
		$this->admin_model->set_top_button("เพิ่มข้อมูล","income_manager_anchor/anchor_add/".$type,"icon-plus","btn-info","r");
		
		$this->admin_model->set_action_button("แก้ไข","income_manager_anchor/anchor_edit/[income_id]/".$type,"icon-eye","btn-success","r");
		$this->admin_model->set_action_button("ทดสอบ","income_test_anchor/add_anchor_testing/[income_id]/".$type,"icon-eye","btn-success","r");
		$this->admin_model->set_action_button("Chemical","manage_anchorbolt/anchor_chemical/".$type."/[income_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("พิมพ์","report_anchorbolt/income_full/[income_id]/".$type,"icon-edit","btn-info","w");
		$this->admin_model->set_action_button("ลบ","income_manager_anchor/anchor_delete/[income_id]/".$type,"icon-trash","btn-danger","d");
		$this->admin_model->set_pagination("income_manager_anchor/anchor_list/".$type,$this->anchor_income->export_data($type)->num_rows(),$limit,3);
		$this->admin_model->show_search_text("income_manager_anchor/anchor_list/".$type);
		
		$this->admin_model->make_list();
		$this->admin_library->output();		
	}
	public function anchor_add($type)
	{
		$this->load->model('administrator/manage_anchorboltmodel');
		if($type=="axel"){
			$this->admin_model->set_menu_key('ca4e503a3c9fe5af42d1e7d4c18e1abc');
		}else{
			$this->admin_model->set_menu_key('91f3758c72ce63009ee6d3ee769df48e');
		}
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules("supplier_id","Supplier ID","trim|required");
		$this->form_validation->set_rules("product_id","Product ID ","trim|required");
		
		if($this->form_validation->run()===false)
		{
			$this->admin_library->setTitle('anchor Incoming','icon-folder-open');
			$this->admin_library->setDetail("anchor incoming manager");
			$this->admin_library->add_breadcrumb("anchor Incoming","income_manager_anchor/anchor_list","icon-folder-open");
			$this->admin_library->add_breadcrumb("Add Incoming","income_manager_anchor/anchor_add","icon-plus");
			$this->_data['row'] = $this->manage_anchorboltmodel->get_cer_std("1");
			$this->_data['type'] = $type;
			$this->admin_library->view("income_manager_anchor/anchor_add",$this->_data);
			$this->admin_library->output();
		}else{
			$income_id = $this->anchor_income->add_income();
			$this->manage_anchorboltmodel->anchor_standard_add($income_id);
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("income_manager_anchor/anchor_list/".$type);
		}
	}
	public function anchor_edit($income_id=0,$type)
	{
		$this->load->model('administrator/manage_anchorboltmodel');
		if($type=="axel"){
			$this->admin_model->set_menu_key('ca4e503a3c9fe5af42d1e7d4c18e1abc');
		}else{
			$this->admin_model->set_menu_key('91f3758c72ce63009ee6d3ee769df48e');
		}
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		
		$this->form_validation->set_rules("supplier_id","Supplier ID","trim|required");
		//$this->form_validation->set_rules("product_id","Product ID ","trim|required");
		if($this->form_validation->run()===false)
		{
			$this->_data['row'] = $this->anchor_income->getIncome($income_id);
			$this->_data['list'] = $this->manage_anchorboltmodel->get_cer_std_foredit($income_id);
			if(!$this->_data['row']){
				$this->session->set_flashdata("message-error","ไม่พบข้อมูลที่ต้องการแก้ไข.");
				admin_redirect("income_manager_anchor/anchor_list");
			}
			$this->admin_library->setTitle('anchor Incoming','icon-folder-open');
			$this->admin_library->setDetail("anchor incoming manager");
			$this->admin_library->add_breadcrumb("anchor Incoming","income_manager_anchor/anchor_list","icon-folder-open");
			$this->admin_library->add_breadcrumb("Edit Incoming","income_manager_anchor/anchor_edit/".$income_id,"icon-edit");
			$this->_data['type'] = $type;
			$this->admin_library->view("income_manager_anchor/anchor_edit",$this->_data);
			$this->admin_library->output();
		}else{
			$this->anchor_income->edit_income();
			$this->manage_anchorboltmodel->anchor_standard_edit($income_id);
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("income_manager_anchor/anchor_list/".$type);
		}
	}
	function anchor_delete($income_id,$type){
		$this->admin_model->set_menu_key('56a6cb0114f46400034d578425adc423');
		if(!$this->admin_model->check_permision("d")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("income_manager_anchor/anchor_list/".$type);
		}
		
		$this->anchor_income->delete_income($income_id);
		$this->session->set_flashdata("message-success","ลบข้อมูลเรียบร้อยแล้ว.");
		admin_redirect("income_manager_anchor/anchor_list/".$type);
	}
	
	
	function print_anchor1(){
		$this->admin_model->set_menu_key('56a6cb0114f46400034d578425adc423');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$product = $this->income_test_model->get_anchor_product($this->_data['income_id']);
		
		$this->_data['income_info'] =  $this->income_test_model->get_income_anchor($this->_data['income_id']);
		$this->_data['std_list'] = $this->income_test_model->get_product_standard_anchor($product['product_id']);
		$this->_data['cer_size_list'] = $this->income_test_model->get_product_standard_size_anchor($product['product_id'],$this->_data['income_info']['size_id']);

	
	}
	
	/* 
	anchor manager - End 
	*/

	public function numeric($text,$row)
	{
		return number_format($text);
	}
	public function date_callback($text,$row)
	{
		if($text){
			$text = thai_convert_shortdate($text);
/*
			$text = strtotime($text);
			$text = date("d M Y",$text);
*/
		}else{
			$text = "(ไม่ระบุ)";
		}
		return $text;
	}
	
	public function product_name_callback($text,$row)
	{
		//$row = $this->anchor_income->getproduct_name($row['income_product_id']);
		$text = @$row['product_grade'] ." ". @$row['product_type'];
		return $text;
	}
	
	public function supplier_name_callback($text,$row)
	{
		//$row = $this->anchor_income->getsupplier_name($row['income_supplier_id']);
		$text = $row['supplier_name'];
		return $text;
	}
	
	public function product_size_callback($text,$row){
		//$row = $this->anchor_income->get_size($text);
		if($row['product_type'] == "axel"){
			$text = @$row['size_m'].'x'.@$row['size_length'];
		}else{
			$text = @$row['size_t'].'x'.@$row['size_w'].'x'.@$row['size_length'];	
		}
		return $text;
	}
	
	
	public function lot_manager_callback($text,$row)
	{
		if(intval(@$row['income_createdid']) > 0){
			$user = $this->admin_library->getuserinfo($row['income_createdid']);
		}
		if(intval(@$row['income_updatedid']) > 0){
			$user = $this->admin_library->getuserinfo($row['income_updatedid']);
		}
		if(@$user){
			$text = explode(" ", $user['user_fullname']);
			$text = $text[0];
		}else{
			$text = "(ไม่ระบุ)";
		}
		return $text;	
	}
	
	public function test_status_callback($text, $row){
		
		if($text==2){
			return 'ผ่านการทดสอบ';
		}else if($text==1){
			return 'ไม่ผ่านการทดสอบ';
		}else{
			return 'ยังไม่ได้ทดสอบ';
		}
	}
	function testview()
	{
		$this->admin_library->view("income_manager_anchor/excel");
		$this->admin_library->output();
	}
	
	public function get_size_anchor_byid($type='axel'){
		$anchorid = $this->input->post('anchor_id');
		
		$this->_data['sizelist'] = $this->anchor_income->get_anchorsize_byid($anchorid);
		$this->_data['type'] = $type;
		
		$this->load->view('administrator/views/income_manager_anchor/sizelist_anchor', $this->_data);
	}	
	
	
	
	public function print_anchor($income_id)
	{
		$this->load->model('income_test_model');
		
		$this->_data['income_id'] = $income_id;
		$checked = $this->income_test_model->check_income_tested($this->_data['income_id']);
		if($checked > 0){
			$product = $this->income_test_model->get_anchor_product($this->_data['income_id']);
			$this->_data['std_list'] = $this->income_test_model->get_product_standard_anchor($product['product_id']);
			$this->_data['product'] = $product;
			$this->_data['income_info'] =  $this->income_test_model->get_income_anchor($this->_data['income_id']);
			//$this->_data['income_data'] =  $this->income_test_model->get_income_tested($this->_data['income_id']);
			$this->_data['shipper'] =  $this->anchor_income->getsupplier_name($this->_data['income_info']['income_supplier_id']);
			$this->_data['size'] = $this->anchor_income->get_size($this->_data['income_info']['size_id']);
			$this->_data['cer_size_list'] = $this->income_test_model->get_product_standard_size_anchor($product['product_id'],$this->_data['income_info']['size_id']);
			
			$this->_data['anchor_thickness'] = $this->income_test_model->get_thicknesstest_info_byincomeid_anchor($income_id);
			
			$this->_data['anchor_thickness_info'] = $this->income_test_model->get_thickness_by_id_income($this->_data['anchor_thickness']['thickness_id']); 
			//$this->_data['income_cer_size_data'] =  $this->income_test_model->get_income_cer_size_tested($this->_data['income_id']);
			header("Content-type: application/vnd.ms-excel");
			header("Content-Disposition: attachment;Filename=income_anchor_report_".$income_id.".xls");
			$this->load->view('administrator/views/income_manager_anchor/print_anchor', $this->_data);
		}else{
			$this->session->set_flashdata("message-warning","ข้อมูลยังไม่ได้ถูกทดสอบ.");
			admin_redirect("income_manager_anchor/anchor_list");
		}
				
	}
	
	
}