<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Project Name : iAON
* Build Date : 8/21/2558 BE
* Author Name : Jarak Kritkiattisak
* File Name : migrate.php
* File Location : /Volumes/Macintosh HD/Users/mycools/Library/Caches/Coda 2/4E2A0EF1-C1E0-4007-9071-D7F6D982C6AD
* File Type : Controller	
* Remote URL : http://192.168.100.20/application/controllers/migrate.php
*/
class Migrate extends CI_Controller {
	/**
	* migrate
	*
	* Index Page for migrate Controller
	*
	* @return	nulled
	*/
	public function index()
	{
		$this->load->library('migration');

		if ( ! $this->migration->current())
		{
			show_error($this->migration->error_string());
		}else{
			echo "Migate to version ".$this->migration->latest()." complete.";
		}
	}
}

/* End of file migrate.php */
/* Location: ./application/controllers/migrate.php */