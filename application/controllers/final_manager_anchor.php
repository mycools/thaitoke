<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Final_manager_anchor extends CI_Controller {
	var $_data = array();
	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->library('image_lib');
		$this->load->library('admin_library');
		
		$this->admin_library->forceLogin();
		$this->load->model('administrator/final_manager_anchor_model');
		$this->load->model('administrator/admin_model');
	}
	public function index()
	{
			
	}	
	
	################ start #####################################
	public function final_list($type='axel', $productid=0,$page=1)
	{
		$QUERY_STRING = @$_SERVER['QUERY_STRING'];
		$QUERY_STRING = (trim($QUERY_STRING))?"?".$QUERY_STRING:"";
		$this->admin_model->set_menu_key('b23450a1f5c3e4aa95f922ce089b1597');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$limit=25;
		$offset = ($page-1)*$limit;
		$offset = ($offset <0)?0:$offset;
		
		$this->admin_library->setTitle('Final','icon-folder-open');
		
		$this->admin_library->setDetail("Final manager");
		$this->admin_model->initd($this);
		$this->admin_model->set_title('Final manager','icon-book');
		
		$info = $this->final_manager_anchor_model->dataTable($productid,$type,$limit,$offset);
		$this->admin_model->set_datatable($info);
		
		$this->admin_model->set_column("final_id","No.",0,"icon-sort-alpha-asc");
		$this->admin_model->set_column("final_invoice_no","Invoice ID.",0,"icon-folder-open");
		$this->admin_model->set_column("final_customer_id","Customer",0,"icon-shopping-cart");
		
		$this->admin_model->set_column("final_createdtime","Create Date",0,"icon-calendar");
		//$this->admin_model->set_column("final_manager","ผู้จัดการข้อมูล",0,"icon-user");
		$this->admin_model->set_column("test_status","สถานะการทดสอบ",0,'icon-code');
		
		//$this->admin_model->set_column_callback("final_nut_id","product_nut_name_callback");
		$this->admin_model->set_column_callback("final_customer_id","customer_name_callback");
		//$this->admin_model->set_column_callback("final_manager","lot_manager_callback");
		$this->admin_model->set_column_callback("final_createdtime","date_callback");
		$this->admin_model->set_column_callback("test_status","manager_status_callback");
		//$this->admin_model->set_column_callback("final_nut_size_id","product_size_nut_callback");
		$size_id = $this->final_manager_anchor_model->get_size_id_for_back($productid);
		
		$this->admin_model->set_top_button("เพิ่มข้อมูล","final_manager_anchor/add/".$type."/".$productid,"icon-plus","btn-info","r");
		$this->admin_model->set_top_button("กลับไปยังหน้าสินค้า","anchorbolt_product/product_list/".$type."/".$size_id,"icon-reply","btn-primary","r");
		$this->admin_model->set_action_button("แก้ไข","final_manager_anchor/edit/".$type."/".$productid."/[final_id]","icon-eye","btn-success","r");
	
		$this->admin_model->set_action_button("ทดสอบ","final_test_anchor/final_main_testing/".$productid."/[final_id]","icon-eye","btn-success","r");
		$this->admin_model->set_action_button("พิมพ์ Cer","report_final_anchorbolt/print_final/[final_id]","icon-edit","btn-info","w");
		$this->admin_model->set_action_button("ลบ","final_manager_anchor/delete/".$type."/".$productid."/[final_id]","icon-trash","btn-danger","d");
		$this->admin_model->set_pagination("final_manager_anchor/final_list/".$type."/".$productid,$this->final_manager_anchor_model->get_count($type,$productid),$limit,3);
		$this->admin_model->show_search_text("final_manager_anchor/final_list/".$type."/".$productid);
		
		$this->admin_model->make_list();
		$this->admin_library->output();		
		
	}
	
	public function add($type='axel', $productid=0)
	{
		$this->admin_model->set_menu_key('b23450a1f5c3e4aa95f922ce089b1597');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		//$this->load->model('administrator/final_manager_anchor_model');
		$this->form_validation->set_rules("customer_id","Customer ID","trim|required");
		//$this->form_validation->set_rules("final_nut_id","Nut ID ","trim|required");
		
		if($this->form_validation->run()===false)
		{
			$this->admin_library->setTitle('Final','icon-folder-open');
			$this->admin_library->setDetail("Final manager");
			$this->admin_library->add_breadcrumb("Final","final_manager_anchor/final_list/".$type."/".$productid,"icon-folder-open");
			$this->admin_library->add_breadcrumb("Add Final","final_manager_anchor/add/".$type."/".$productid,"icon-plus");
			$this->_data['type'] = $type;
			$this->_data['product_id'] = $productid;
			$this->admin_library->view("final_manager_anchor/add",$this->_data);
			$this->admin_library->output();
		}else{
			$this->final_manager_anchor_model->add_final();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("final_manager_anchor/final_list/".$type."/".$productid);
		}
	}
	public function edit($type='axel', $productid=0,$final_id=0)
	{
		$this->admin_model->set_menu_key('b23450a1f5c3e4aa95f922ce089b1597');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		

		$this->form_validation->set_rules("customer_id","Customer ID","trim|required");
		//$this->form_validation->set_rules("product_id","Product ID ","trim|required");
		if($this->form_validation->run()===false)
		{
			$this->_data['type'] = $type;
			$this->_data['product_id'] = $productid;
			$this->_data['row'] = $this->final_manager_anchor_model->getfinal($final_id);
			if(!$this->_data['row']){
				$this->session->set_flashdata("message-error","ไม่พบข้อมูลที่ต้องการแก้ไข.");
				admin_redirect("final_manager_anchor/final_list/".$type."/".$productid);
			}
			
			$this->admin_library->add_breadcrumb("Final","final_manager_anchor/final_list/".$type."/".$productid,"icon-folder-open");
			$this->admin_library->add_breadcrumb("Edit final","final_manager_anchor/edit/".$final_id,"icon-edit");
			
			$this->admin_library->view("final_manager_anchor/edit",$this->_data);
			$this->admin_library->output();
		}else{
			$result = $this->final_manager_anchor_model->edit_final();
			
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("final_manager_anchor/final_list/".$type."/".$productid);
		}
	}
	function delete($type='axel', $productid=0,$final_id){
		$this->admin_model->set_menu_key('b23450a1f5c3e4aa95f922ce089b1597');
		if(!$this->admin_model->check_permision("d")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("final_manager_anchor/final_list/".$type."/".$productid);
		}
		$this->load->model('administrator/final_manager_anchor_model');
		$this->final_manager_anchor_model->delete_final($final_id);
		$this->session->set_flashdata("message-success","ลบข้อมูลเรียบร้อยแล้ว.");
		admin_redirect("final_manager_anchor/final_list/".$type."/".$productid);
	}

	
	###################start public #################################

	public function customer_name_callback($text,$row)
	{
		$row = $this->final_manager_anchor_model->getcustomer_name($row['customer_id']);
		$text = $row['customer_name'];
		return $text;
	}
	
	
	
	public function date_callback($text,$row)
	{
		if($text){
			$text = strtotime($text);
			$text = date("d M Y",$text);
		}else{
			$text = "(ไม่ระบุ)";
		}
		return $text;
	}
	
	public function manager_status_callback($text, $row){
		if($text==2){
			return 'ผ่านการทดสอบ';
		}else if($text==1){
			return 'ไม่ผ่านการทดสอบ';
		}else{
			return 'ยังไม่ได้ทดสอบ';
		}
	}
	
	
	
	
}