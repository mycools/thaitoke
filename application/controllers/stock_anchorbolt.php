<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stock_anchorbolt extends CI_Controller {
	var $_data=array();
	function __construct(){
		parent::__construct();
		
		$this->load->library('admin_library');
		$this->load->model('administrator/admin_model');
		$this->load->model('administrator/manage_anchorboltmodel');
		$this->admin_library->forceLogin();
	}
	
	public function index(){
		admin_redirect("stock_anchorbolt/stock_full/axel");
	}
	
	/* Stock Full - Start */
	public function stock_full($type='axel', $page=1){
		
		$page = ((int) $page < 1)?1:(int) $page;
		$offset = ($page-1)*25;
		
		$this->seq=$offset;
		if($type=="axel"){
			$this->admin_model->set_menu_key('f309cf04da256a2de9686a4260b53055');
		}else{
			$this->admin_model->set_menu_key('3b66302928e34675e86000bcb54fcc3f');
		}
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('รายการสินค้า Anchor Bolt','stock_anchorbolt/stock_full/'.$type,'');
		
		$this->admin_model->set_datatable($this->manage_anchorboltmodel->dataTable($type, 25,$offset));
		$this->admin_model->set_column("product_no","ลำดับ",'5%');
		$this->admin_model->set_column("product_thumbnail","รูปภาพ",'15%');
		$this->admin_model->set_column("product_grade","ชื่อสินค้า",'30%');
		$this->admin_model->set_column_callback('product_thumbnail','get_image');
		$this->admin_model->set_column_callback("product_no","show_seq");
		
		
		$this->admin_model->set_action_button("ดูข้อมูล","stock_anchorbolt/stock_full_list/".$type."/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->show_search_text("stock_anchorbolt/stock_full/".$type);
		$this->admin_model->set_pagination("stock_anchorbolt/stock_full/".$type,$this->manage_anchorboltmodel->get_count($type),25,3);
		
		$this->admin_model->make_list();
		$this->admin_library->output();
	}
	
	public function stock_full_list($type='axel', $productid=0, $page=1){
		
		$page = ((int) $page < 1)?1:(int) $page;
		$offset = ($page-1)*25;
		$this->seq=$offset;
		if($type=="axel"){
			$this->admin_model->set_menu_key('f309cf04da256a2de9686a4260b53055');
		}else{
			$this->admin_model->set_menu_key('3b66302928e34675e86000bcb54fcc3f');
		}
		
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		 
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('รายการสินค้า Anchor Bolt','stock_anchorbolt/stock_full/'.$type,'');
		$this->admin_library->add_breadcrumb('จัดการ Size',current_url(),'');
		
		$this->admin_model->set_datatable($this->manage_anchorboltmodel->size_dataTable($productid, $type, 25,$offset));
		$this->admin_model->set_column("size_no","ลำดับ",0);
		if($type=='axel'){
			$this->admin_model->set_column("size_m","M",0);
		}else{
			$this->admin_model->set_column("size_t","T",0);
			$this->admin_model->set_column("size_w","W",0);
		}
		
		$this->admin_model->set_column("size_length","Length",0);
		$this->admin_model->set_column("size_qty","Quantity",0);
		$this->admin_model->set_column_callback("size_no","show_seq");
/*
		$this->admin_model->set_action_button("ปรับปรุงสต็อก","stock_manager/edit_stock/[size_id]",'icon-trash-o','btn-info','w');
		$this->admin_model->set_action_button("ดูประวัติ","stock_manager/view_bolt_history/[size_id]/".$productid,'icon-trash-o','btn-info','w');
		$this->admin_model->set_action_button('Income Log',"stock_manager/bolt_incoming_log/[size_id]/".$productid,'icon-trash-o','btn-info','w');
*/

		$this->admin_model->set_action_button("ปรับปรุง","stock_anchorbolt/edit_stock_full/".$type."/[product_id]/[size_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("ดูประวัติ","stock_anchorbolt/view_anchor_history/".$type."/[product_id]/[size_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("Incoming Log","stock_anchorbolt/anchor_incoming_log/".$type."/[product_id]/[size_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("ใบงาน","stock_anchorbolt/doc_list_full/".$type."/[product_id]/[size_id]",'icon-edit','btn-info','w');
		$this->admin_model->show_search_text("stock_anchorbolt/stock_full_list/".$type."/".$productid);
		$this->admin_model->set_pagination("stock_anchorbolt/stock_full_list/".$type."/".$productid,$this->manage_anchorboltmodel->get_size_count($productid, $type),25,3);
		
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function edit_stock_full($type='', $productid=0, $sizeid=0){
		
		if($type=="axel"){
			$this->admin_model->set_menu_key('f309cf04da256a2de9686a4260b53055');
		}else{
			$this->admin_model->set_menu_key('3b66302928e34675e86000bcb54fcc3f');
		}
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('qty','Quantity','trim|required');
		$this->form_validation->set_rules('remark','Remark','trim');
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->setTitle('Stock','icon-folder-open');
			$this->admin_library->setDetail("Edit");
			$this->admin_library->add_breadcrumb("Anchor bolt stock","stock_anchorbolt/stock_full_list/".$type."/".$productid,"icon-folder-open");
			$this->admin_library->add_breadcrumb("Anchor bolt stock edit","stock_anchorbolt/edit_stock_full".$type."/".$productid."/".$sizeid,"icon-plus");
			$this->_data['size_id'] = $sizeid;
			$this->_data['type'] = $type;
			$this->_data['row'] = $this->manage_anchorboltmodel->size_detail($sizeid);
			$this->admin_library->view("stock_anchorbolt/edit_stock",$this->_data);
			$this->admin_library->output();
		}else{
			$pro_id = $this->manage_anchorboltmodel->stock_edit($sizeid);
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect('stock_anchorbolt/stock_full_list/'.$type.'/'.$productid);
		}
		
	}
	
	public function view_anchor_history($type='axel', $productid=0, $sizeid=0){
		$this->admin_model->set_menu_key('98d58e3bb8b44c8b9d68f8d933b37d0d');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("ดูข้อมูล","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('กลับ','stock_anchorbolt/stock_full_list/'.$type.'/'.$productid,'');
		//$this->admin_library->add_breadcrumb('จัดการ Size',current_url(),'');
		
		//$this->admin_model->set_top_button("เพิ่ม Bolt size","manageproduct/bolt_size_add/".$productid,'icon-plus','btn-success','w');
		//$this->admin_model->set_top_button("กลับไปยังหน้ารายการสินค้า Bolt",'manageproduct/bolt_list','icon-reply','btn-primary','w');
		
		$this->admin_model->set_datatable($this->manage_anchorboltmodel->get_anchor_history_dataTable($sizeid));
		$this->admin_model->set_column("log_no","ลำดับ",0);
		$this->admin_model->set_column("qty_old","จำนวนเดิม",0);
		$this->admin_model->set_column("qty","จำนวนใหม่",0);
		$this->admin_model->set_column("remark","หมายเหตุ",0);
		$this->admin_model->set_column("createdate","วันที่แก้ไข",0);
		$this->admin_model->set_column_callback("log_no","show_seq");
		$this->admin_model->set_column_callback("createdate","show_date");

				
		$this->admin_model->make_list();
		$this->admin_library->output();
	}
	
	public function anchor_incoming_log($type='axel', $pro_id=0,$size_id=0){
		$this->admin_model->set_menu_key('98d58e3bb8b44c8b9d68f8d933b37d0d');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("ดูข้อมูล","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('กลับ','stock_anchorbolt/stock_full_list/'.$type.'/'.$pro_id,'');
		
		$this->admin_model->set_datatable($this->manage_anchorboltmodel->get_anchor_incoming_dataTable($size_id, $pro_id));
		$this->admin_model->set_column('income_job_id','Income No.',0);
		$this->admin_model->set_column('income_invoice_no','Invoice No.',0);
		$this->admin_model->set_column('income_supplier_id','Supplier',0);
		$this->admin_model->set_column('income_quantity','Quantity',0);
		$this->admin_model->set_column_callback('income_supplier_id','show_supplier');
		
		$this->admin_model->make_list();
		$this->admin_library->output();
	}
	
	public function doc_list_full($type='axel', $pro_id=0, $size_id=0){
		$this->admin_model->set_menu_key('98d58e3bb8b44c8b9d68f8d933b37d0d');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("รายการใบงาน","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('กลับ','stock_anchorbolt/stock_full_list/'.$type.'/'.$pro_id,'');
		$this->admin_model->set_top_button("เพิ่มใบงาน","stock_anchorbolt/doc_add_full/".$type.'/'.$pro_id.'/'.$size_id,'icon-plus','btn-success','w');
		
		$this->admin_model->set_datatable($this->manage_anchorboltmodel->get_doclist($size_id, $pro_id));
		$this->admin_model->set_column('doc_no','เลขที่ใบงาน',0);
		$this->admin_model->set_column('doc_qty','จำนวนเหล็ก',0);
		$this->admin_model->set_column('doc_customer','บริษัท',0);
		$this->admin_model->set_column('doc_due','กำหนดส่ง',0);
		$this->admin_model->set_action_button("ดูข้อมูล","stock_anchorbolt/doc_info_full/".$type."/[product_id]/[size_id]/[doc_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("แก้ไขข้อมูล","stock_anchorbolt/doc_edit_full/".$type."/[product_id]/[size_id]/[doc_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("ลบ","stock_anchorbolt/doc_delete_full/".$type."/[product_id]/[size_id]/[doc_id]",'icon-trash-o','btn-danger','d');
		
		$this->admin_model->make_list();
		$this->admin_library->output();
	}
	
	public function doc_add_full($type='axel', $pro_id=0, $size_id=0){
		$this->admin_model->set_menu_key('98d58e3bb8b44c8b9d68f8d933b37d0d');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('doc_no','เลขที่ใบงาน','trim|required');
		$this->form_validation->set_rules('doc_qty','จำนวน','trim|required');
		$this->form_validation->set_rules('doc_outcome','ผลิตเป็นสินค้า','trim|required');
		$this->form_validation->set_rules('doc_compound','ชุดประกอบ','trim|required');
		$this->form_validation->set_rules('doc_amount','จำนวนผลิต','trim|required');
		$this->form_validation->set_rules('doc_customer','ชื่อบริษัทลูกค้า','trim|required');
		$this->form_validation->set_rules('doc_due','กำหนดส่ง','trim|required');
		$this->form_validation->set_rules('doc_remark','หมายเหตุ','trim');
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->setTitle('Stock','icon-folder-open');
			$this->admin_library->setDetail("เพิ่มใบงาน");
			$this->admin_library->add_breadcrumb("Anchor bolt stock","stock_anchorbolt/stock_full_list/".$type."/".$pro_id,"icon-folder-open");
			$this->admin_library->add_breadcrumb("Anchor bolt stock document","stock_anchorbolt/doc_add_full/".$type."/".$pro_id."/".$size_id,"icon-plus");
			$this->_data['pro_id'] = $pro_id;
			$this->_data['size_id'] = $size_id;
			$this->_data['type'] = $type;
			$this->admin_library->view("stock_anchorbolt/doc_add_full",$this->_data);
			$this->admin_library->output();
		}else{
			$state = $this->manage_anchorboltmodel->doc_add_full($size_id, $pro_id);
			
			if($state==1){
				$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			}else{
				$this->session->set_flashdata("message-error","ไม่สามารถตัดสต็อคได้ เนื่องจากจำนวนไม่พอ");
			}
			admin_redirect("stock_anchorbolt/doc_list_full/".$type."/".$pro_id."/".$size_id);
		}
	}
	
	public function doc_edit_full($type='axel', $pro_id=0, $size_id=0, $doc_id=0){
		$this->admin_model->set_menu_key('98d58e3bb8b44c8b9d68f8d933b37d0d');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('doc_no','เลขที่ใบงาน','trim|required');
		$this->form_validation->set_rules('doc_outcome','ผลิตเป็นสินค้า','trim|required');
		$this->form_validation->set_rules('doc_compound','ชุดประกอบ','trim|required');
		$this->form_validation->set_rules('doc_amount','จำนวนผลิต','trim');
		$this->form_validation->set_rules('doc_customer','ชื่อบริษัทลูกค้า','trim|required');
		$this->form_validation->set_rules('doc_due','กำหนดส่ง','trim|required');
		$this->form_validation->set_rules('doc_remark','หมายเหตุ','trim');
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->setTitle('Stock','icon-folder-open');
			$this->admin_library->setDetail("แก้ไขใบงาน");
			$this->admin_library->add_breadcrumb("Anchor bolt stock","stock_anchorbolt/stock_full_list/".$type."/".$pro_id,"icon-folder-open");
			$this->admin_library->add_breadcrumb("Anchor bolt stock document","stock_anchorbolt/doc_edit_full/".$type."/".$pro_id."/".$size_id,"icon-plus");
			$this->_data['pro_id'] = $pro_id;
			$this->_data['size_id'] = $size_id;
			$this->_data['type'] = $type;
			$this->_data['row'] = $this->manage_anchorboltmodel->getdoc_info($doc_id);
			$this->admin_library->view("stock_anchorbolt/doc_edit_full",$this->_data);
			$this->admin_library->output();
		}else{
			$state = $this->manage_anchorboltmodel->doc_edit_full($size_id, $pro_id, $doc_id);
			
			if($state==1){
				$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			}else{
				$this->session->set_flashdata("message-error","ไม่สามารถตัดสต็อคได้ เนื่องจากจำนวนไม่พอ");
			}
			admin_redirect("stock_anchorbolt/doc_list_full/".$type."/".$pro_id."/".$size_id);
		}
	}
	
	public function doc_info_full($type='axel', $pro_id=0, $size_id=0, $doc_id=0){
		$this->admin_model->set_menu_key('98d58e3bb8b44c8b9d68f8d933b37d0d');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_library->setTitle('Stock','icon-folder-open');
		$this->admin_library->setDetail("รายละเอียดใบงาน");
		$this->admin_library->add_breadcrumb("Anchor bolt stock","stock_anchorbolt/stock_full_list/".$type."/".$pro_id,"icon-folder-open");
		$this->admin_library->add_breadcrumb("Anchor bolt stock document","stock_anchorbolt/doc_info_full/".$type."/".$pro_id."/".$size_id,"icon-plus");
		$this->_data['pro_id'] = $pro_id;
		$this->_data['size_id'] = $size_id;
		$this->_data['type'] = $type;
		$this->_data['row'] = $this->manage_anchorboltmodel->getdoc_info($doc_id);
		$this->admin_library->view("stock_anchorbolt/doc_info_full",$this->_data);
		$this->admin_library->output();
		
	}
	
	public function doc_delete_full($type='axel', $pro_id=0, $size_id=0, $doc_id=0){
		$this->manage_anchorboltmodel->doc_delete_full($doc_id);
		
		$this->session->set_flashdata("message-success","ลบข้อมูลเรียบร้อยแล้ว");
		admin_redirect("stock_anchorbolt/doc_list_full/".$type."/".$pro_id."/".$size_id);
	}
	/* Stock Full - End */
	
	/* Stock Left - Start */
	public function stock_left($type='axel', $page=1){
		
		$page = ((int) $page < 1)?1:(int) $page;
		$offset = ($page-1)*25;
		
		$this->seq=$offset;
		if($type=="axel"){
			$this->admin_model->set_menu_key('db18f4e2defebd5dc05fb0cddc437952');
		}else{
			$this->admin_model->set_menu_key('8c38a6883c77cad1fe52c489423b49ec');
		}
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('รายการสินค้า Anchor Bolt','stock_anchorbolt/stock_left/'.$type,'');
		
		$this->admin_model->set_datatable($this->manage_anchorboltmodel->dataTable($type, 25,$offset));
		$this->admin_model->set_column("product_no","ลำดับ",'5%');
		$this->admin_model->set_column("product_thumbnail","รูปภาพ",'15%');
		$this->admin_model->set_column("product_grade","ชื่อสินค้า",'30%');
		$this->admin_model->set_column_callback('product_thumbnail','get_image');
		$this->admin_model->set_column_callback("product_no","show_seq");
		
		
		$this->admin_model->set_action_button("ดูข้อมูล","stock_anchorbolt/stock_left_list/".$type."/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->show_search_text("stock_anchorbolt/stock_left/".$type);
		$this->admin_model->set_pagination("stock_anchorbolt/stock_left/".$type,$this->manage_anchorboltmodel->get_count($type),25,3);
		
		$this->admin_model->make_list();
		$this->admin_library->output();
	}
	
	public function stock_left_list($type='axel', $productid=0, $page=1){
		
		$page = ((int) $page < 1)?1:(int) $page;
		$offset = ($page-1)*25;
		
		$this->seq=$offset;
		if($type=="axel"){
			$this->admin_model->set_menu_key('db18f4e2defebd5dc05fb0cddc437952');
		}else{
			$this->admin_model->set_menu_key('8c38a6883c77cad1fe52c489423b49ec');
		}
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		 
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('รายการสินค้า Anchor Bolt','stock_anchorbolt/stock_left/'.$type,'');
		$this->admin_library->add_breadcrumb('จัดการ Size',current_url(),'');
		
		$this->admin_model->set_datatable($this->manage_anchorboltmodel->size_dataTable($productid, $type, 25,$offset));
		$this->admin_model->set_column("size_no","ลำดับ",0);
		if($type=='axel'){
			$this->admin_model->set_column("size_m","M",0);
		}else{
			$this->admin_model->set_column("size_t","T",0);
			$this->admin_model->set_column("size_w","W",0);
		}
		$this->admin_model->set_column("size_length","Length",0);
		$this->admin_model->set_column("size_left_qty","Quantity",0);
		$this->admin_model->set_column_callback("size_no","show_seq");
/*
		$this->admin_model->set_action_button("ปรับปรุงสต็อก","stock_manager/edit_stock/[size_id]",'icon-trash-o','btn-info','w');
		$this->admin_model->set_action_button("ดูประวัติ","stock_manager/view_bolt_history/[size_id]/".$productid,'icon-trash-o','btn-info','w');
		$this->admin_model->set_action_button('Income Log',"stock_manager/bolt_incoming_log/[size_id]/".$productid,'icon-trash-o','btn-info','w');
*/
		$this->admin_model->set_action_button("ปรับปรุง","stock_anchorbolt/edit_stock_left/".$type."/[product_id]/[size_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("ดูประวัติ","stock_anchorbolt/view_anchor_left_history/".$type."/[product_id]/[size_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("ใบงาน","stock_anchorbolt/doc_list_left/".$type."/[product_id]/[size_id]",'icon-edit','btn-info','w');
		$this->admin_model->show_search_text("stock_anchorbolt/stock_left_list/".$type."/".$productid);
		$this->admin_model->set_pagination("stock_anchorbolt/stock_left_list/".$type,$this->manage_anchorboltmodel->get_size_count(),25,3);
		
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function edit_stock_left($type='', $productid=0, $sizeid=0){
		
		if($type=="axel"){
			$this->admin_model->set_menu_key('db18f4e2defebd5dc05fb0cddc437952');
		}else{
			$this->admin_model->set_menu_key('8c38a6883c77cad1fe52c489423b49ec');
		}
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('qty','Quantity','trim|required');
		$this->form_validation->set_rules('remark','Remark','trim');
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->setTitle('Stock','icon-folder-open');
			$this->admin_library->setDetail("Edit");
			$this->admin_library->add_breadcrumb("Anchor bolt stock","stock_anchorbolt/stock_left_list/".$type."/".$productid,"icon-folder-open");
			$this->admin_library->add_breadcrumb("Anchor bolt stock edit","stock_anchorbolt/edit_stock_left".$type."/".$productid."/".$sizeid,"icon-plus");
			$this->_data['size_id'] = $sizeid;
			$this->_data['type'] = $type;
			$this->_data['row'] = $this->manage_anchorboltmodel->size_detail($sizeid);
			$this->admin_library->view("stock_anchorbolt/edit_stock_left",$this->_data);
			$this->admin_library->output();
		}else{
			$pro_id = $this->manage_anchorboltmodel->stock_edit_left($sizeid);
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect('stock_anchorbolt/stock_left_list/'.$type.'/'.$productid);
		}
		
	}
	
	public function view_anchor_left_history($type='axel', $productid=0, $sizeid=0){
		if($type=="axel"){
			$this->admin_model->set_menu_key('db18f4e2defebd5dc05fb0cddc437952');
		}else{
			$this->admin_model->set_menu_key('8c38a6883c77cad1fe52c489423b49ec');
		}
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("ดูข้อมูล","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('กลับ','stock_anchorbolt/stock_left_list/'.$type.'/'.$productid,'');
		//$this->admin_library->add_breadcrumb('จัดการ Size',current_url(),'');
		
		//$this->admin_model->set_top_button("เพิ่ม Bolt size","manageproduct/bolt_size_add/".$productid,'icon-plus','btn-success','w');
		//$this->admin_model->set_top_button("กลับไปยังหน้ารายการสินค้า Bolt",'manageproduct/bolt_list','icon-reply','btn-primary','w');
		
		$this->admin_model->set_datatable($this->manage_anchorboltmodel->get_anchor_left_history_dataTable($sizeid));
		$this->admin_model->set_column("log_no","ลำดับ",0);
		$this->admin_model->set_column("qty_old","จำนวนเดิม",0);
		$this->admin_model->set_column("qty","จำนวนใหม่",0);
		$this->admin_model->set_column("remark","หมายเหตุ",0);
		$this->admin_model->set_column("createdate","วันที่แก้ไข",0);
		$this->admin_model->set_column_callback("log_no","show_seq");
		$this->admin_model->set_column_callback("createdate","show_date");

				
		$this->admin_model->make_list();
		$this->admin_library->output();
	}
	
	public function doc_list_left($type='axel', $pro_id=0, $size_id=0){
		$this->admin_model->set_menu_key('a7dcbf064fa200b7e470bbe03861eecf');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("รายการใบงาน","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('กลับ','stock_anchorbolt/stock_left_list/'.$type.'/'.$pro_id,'');
		$this->admin_model->set_top_button("เพิ่มใบงาน","stock_anchorbolt/doc_add_left/".$type.'/'.$pro_id.'/'.$size_id,'icon-plus','btn-success','w');
		
		$this->admin_model->set_datatable($this->manage_anchorboltmodel->get_doclist_left($size_id, $pro_id));
		$this->admin_model->set_column('doc_no','เลขที่ใบงาน',0);
		$this->admin_model->set_column('doc_qty','จำนวนเหล็ก',0);
		$this->admin_model->set_column('doc_customer','บริษัท',0);
		$this->admin_model->set_column('doc_due','กำหนดส่ง',0);
		$this->admin_model->set_action_button("ดูข้อมูล","stock_anchorbolt/doc_info_left/".$type."/[product_id]/[size_id]/[doc_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("แก้ไขข้อมูล","stock_anchorbolt/doc_edit_left/".$type."/[product_id]/[size_id]/[doc_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("ลบ","stock_anchorbolt/doc_delete_left/".$type."/[product_id]/[size_id]/[doc_id]",'icon-trash-o','btn-danger','d');
		
		$this->admin_model->make_list();
		$this->admin_library->output();
	}
	
	public function doc_add_left($type='axel', $pro_id=0, $size_id=0){
		$this->admin_model->set_menu_key('a7dcbf064fa200b7e470bbe03861eecf');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('doc_no','เลขที่ใบงาน','trim|required');
		$this->form_validation->set_rules('doc_qty','จำนวน','trim|required');
		$this->form_validation->set_rules('doc_outcome','ผลิตเป็นสินค้า','trim|required');
		$this->form_validation->set_rules('doc_compound','ชุดประกอบ','trim|required');
		$this->form_validation->set_rules('doc_amount','จำนวนผลิต','trim|required');
		$this->form_validation->set_rules('doc_customer','ชื่อบริษัทลูกค้า','trim|required');
		$this->form_validation->set_rules('doc_due','กำหนดส่ง','trim|required');
		$this->form_validation->set_rules('doc_remark','หมายเหตุ','trim');
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->setTitle('Stock','icon-folder-open');
			$this->admin_library->setDetail("เพิ่มใบงาน");
			$this->admin_library->add_breadcrumb("Anchor bolt stock","stock_anchorbolt/stock_left_list/".$type."/".$pro_id,"icon-folder-open");
			$this->admin_library->add_breadcrumb("Anchor bolt stock document","stock_anchorbolt/doc_add_left/".$type."/".$pro_id."/".$size_id,"icon-plus");
			$this->_data['pro_id'] = $pro_id;
			$this->_data['size_id'] = $size_id;
			$this->_data['type'] = $type;
			$this->admin_library->view("stock_anchorbolt/doc_add_left",$this->_data);
			$this->admin_library->output();
		}else{
			$state = $this->manage_anchorboltmodel->doc_add_left($size_id, $pro_id);
			
			if($state==1){
				$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			}else{
				$this->session->set_flashdata("message-error","ไม่สามารถตัดสต็อคได้ เนื่องจากจำนวนไม่พอ");
			}
			admin_redirect("stock_anchorbolt/doc_list_left/".$type."/".$pro_id."/".$size_id);
		}
	}
	
	public function doc_edit_left($type='axel', $pro_id=0, $size_id=0, $doc_id=0){
		$this->admin_model->set_menu_key('a7dcbf064fa200b7e470bbe03861eecf');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('doc_no','เลขที่ใบงาน','trim|required');
		$this->form_validation->set_rules('doc_outcome','ผลิตเป็นสินค้า','trim|required');
		$this->form_validation->set_rules('doc_compound','ชุดประกอบ','trim|required');
		$this->form_validation->set_rules('doc_amount','จำนวนผลิต','trim');
		$this->form_validation->set_rules('doc_customer','ชื่อบริษัทลูกค้า','trim|required');
		$this->form_validation->set_rules('doc_due','กำหนดส่ง','trim|required');
		$this->form_validation->set_rules('doc_remark','หมายเหตุ','trim');
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->setTitle('Stock','icon-folder-open');
			$this->admin_library->setDetail("แก้ไขใบงาน");
			$this->admin_library->add_breadcrumb("Anchor bolt stock","stock_anchorbolt/stock_left_list/".$type."/".$pro_id,"icon-folder-open");
			$this->admin_library->add_breadcrumb("Anchor bolt stock document","stock_anchorbolt/doc_edit_left/".$type."/".$pro_id."/".$size_id,"icon-plus");
			$this->_data['pro_id'] = $pro_id;
			$this->_data['size_id'] = $size_id;
			$this->_data['type'] = $type;
			$this->_data['row'] = $this->manage_anchorboltmodel->getdoc_info_left($doc_id);
			$this->admin_library->view("stock_anchorbolt/doc_edit_left",$this->_data);
			$this->admin_library->output();
		}else{
			$state = $this->manage_anchorboltmodel->doc_edit_left($size_id, $pro_id, $doc_id);
			
			if($state==1){
				$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			}else{
				$this->session->set_flashdata("message-error","ไม่สามารถตัดสต็อคได้ เนื่องจากจำนวนไม่พอ");
			}
			admin_redirect("stock_anchorbolt/doc_list_left/".$type."/".$pro_id."/".$size_id);
		}
	}
	
	public function doc_info_left($type='axel', $pro_id=0, $size_id=0, $doc_id=0){
		$this->admin_model->set_menu_key('a7dcbf064fa200b7e470bbe03861eecf');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_library->setTitle('Stock','icon-folder-open');
		$this->admin_library->setDetail("รายละเอียดใบงาน");
		$this->admin_library->add_breadcrumb("Anchor bolt stock","stock_anchorbolt/stock_left_list/".$type."/".$pro_id,"icon-folder-open");
		$this->admin_library->add_breadcrumb("Anchor bolt stock document","stock_anchorbolt/doc_info_left/".$type."/".$pro_id."/".$size_id,"icon-plus");
		$this->_data['pro_id'] = $pro_id;
		$this->_data['size_id'] = $size_id;
		$this->_data['type'] = $type;
		$this->_data['row'] = $this->manage_anchorboltmodel->getdoc_info_left($doc_id);
		$this->admin_library->view("stock_anchorbolt/doc_info_left",$this->_data);
		$this->admin_library->output();
		
	}
	
	public function doc_delete_left($type='axel', $pro_id=0, $size_id=0, $doc_id=0){
		$this->manage_anchorboltmodel->doc_delete_left($doc_id);
		
		$this->session->set_flashdata("message-success","ลบข้อมูลเรียบร้อยแล้ว");
		admin_redirect("stock_anchorbolt/doc_list_left/".$type."/".$pro_id."/".$size_id);
	}
	
	/* Stock Left - End */
	
	/* Default function - Start */
	public function show_seq($text,$row)
	{
		$this->seq++;
		return $this->seq;
		
	}
	
	public function show_date($text,$row){
		
		$date = date("d-m-Y H:i:s", strtotime($text));
		return $date;
	}
	
	public function get_image($text,$row){
		if($row['product_thumbnail']!=''){
			return '<img src="'.site_url("src/60/product/anchor_bolt/".$row['product_thumbnail']).'" alt="" />';
		}else{
			return '<img src="'.site_url("src/60/product/anchor_bolt/default.jpg").'" alt="" />';
		}
	}
	
	public function get_type($text,$row){
		if($row['product_type']=='axel'){
			return 'Axel';
		}else if($row['product_type']=='sheet'){
			return 'Sheet';
		}else{
			return 'No type';
		}
	}
	
	public function get_carbon_status($text,$row){
		if($row['product_low_carbon']==1){
			return '<i class="icon-check"></i>';
		}else{
			return '';
		}
	}
	
	public function show_supplier($text, $row){
		$supplierinfo = $this->manage_anchorboltmodel->get_supplierinfo_byid($row['income_supplier_id']);
		return $supplierinfo['supplier_name'];
	}
	/* Default function - End */
}

?>