<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Final_manager extends CI_Controller {
	var $_data = array();
	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->library('image_lib');
		$this->load->library('admin_library');
		$this->load->model('administrator/bolt_stock');
		$this->load->model('administrator/nut_stock');
		$this->load->model('administrator/washer_stock');
		$this->load->model('administrator/spring_stock');
		$this->load->model('administrator/taper_stock');
		$this->load->model('administrator/stud_stock');
		$this->admin_library->forceLogin();
		//$this->load->model('final_manager_model');
		$this->load->model('administrator/admin_model');
	}
	public function index()
	{
			
	}
	
	############# bolt #######################
	public function final_bolt_list($page=1)
	{
		
		$QUERY_STRING = @$_SERVER['QUERY_STRING'];
		$QUERY_STRING = (trim($QUERY_STRING))?"?".$QUERY_STRING:"";
		$this->admin_model->set_menu_key('8a11fc1831fdfa2b49e8203ff134f4ec');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$limit=25;
		$offset = ($page-1)*$limit;
		$offset = ($offset <0)?0:$offset;
		
		$this->admin_library->setTitle('Bolt Final','icon-folder-open');
		
		$this->admin_library->setDetail("Bolt final manager");
		$this->admin_model->initd($this);
		$this->admin_model->set_title('Bolt final manager','icon-book');
		$this->load->model('administrator/bolt_final');
		$this->admin_model->set_datatable($this->bolt_final->dataTable($limit,$offset));
		
		$this->admin_model->set_column("final_id","No.",0,"icon-sort-alpha-asc");
		$this->admin_model->set_column("final_invoice_no","Invoice ID.",0,"icon-folder-open");
		$this->admin_model->set_column("final_bolt_id","Product",0,"icon-shopping-cart");
		$this->admin_model->set_column("final_bolt_size_id","Size",0,"icon-shopping-cart");
		$this->admin_model->set_column("final_customer_id","Customer",0,"icon-shopping-cart");
		
		$this->admin_model->set_column("final_createdtime","Create Date",0,"icon-calendar");
		$this->admin_model->set_column("final_manager","ผู้จัดการข้อมูล",0,"icon-user");
		$this->admin_model->set_column("test_bolt_status","สถานะการทดสอบ",0,'icon-code');
		
		$this->admin_model->set_column_callback("final_bolt_id","product_name_callback");
		$this->admin_model->set_column_callback("final_customer_id","customer_name_callback");
		$this->admin_model->set_column_callback("final_manager","lot_manager_callback");
		$this->admin_model->set_column_callback("final_createdtime","date_callback");
		$this->admin_model->set_column_callback("test_bolt_status","manager_bolt_status_callback");
		$this->admin_model->set_column_callback("final_bolt_size_id","product_size_callback");

		
		$this->admin_model->set_top_button("เพิ่มข้อมูล","final_manager/bolt_add","icon-plus","btn-info","r");
		
		$this->admin_model->set_action_button("แก้ไข","final_manager/bolt_edit/[final_id]","icon-eye","btn-success","r");
		$this->admin_model->set_action_button("ทดสอบ bolt","final_test/final_bolt_testing/[final_bolt_income_id]/[final_id]","icon-eye","btn-success","r");
		$this->admin_model->set_action_button("ทดสอบ nut","final_test/final_nut_testing/[final_nut_income_id]/[final_id]","icon-eye","btn-success","r");
		$this->admin_model->set_action_button("ทดสอบ washer","final_test/final_washer_testing/[final_washer_income_id]/[final_id]","icon-eye","btn-success","r");
		$this->admin_model->set_action_button("พิมพ์ bolt","final_manager/print_bolt/[final_bolt_income_id]/[final_nut_income_id]/[final_washer_income_id]/[final_id]","icon-edit","btn-info","w");
		$this->admin_model->set_action_button("ลบ","final_manager/bolt_delete/[final_id]","icon-trash","btn-danger","d");
		$this->admin_model->set_pagination("final_manager/final_bolt_list",$this->bolt_final->export_data()->num_rows(),$limit,3);
		$this->admin_model->show_search_text();
		$search_q = trim(strip_tags($this->input->get("q")));
		if($search_q != ""){
			$this->admin_library->add_breadcrumb("ค้นหาคำว่า '{$search_q}'","final_manager/final_bolt_list{$QUERY_STRING}","icon-search");
		}
		$this->admin_model->make_list();
		$this->admin_library->output();		
		
	}
	
	public function bolt_add()
	{
		$this->admin_model->set_menu_key('8a11fc1831fdfa2b49e8203ff134f4ec');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$this->load->model('administrator/bolt_final');
		$this->form_validation->set_rules("customer_id","Customer ID","trim|required");
		$this->form_validation->set_rules("final_bolt_id","Bolt ID ","trim|required");
		
		if($this->form_validation->run()===false)
		{
			$this->admin_library->setTitle('Bolt Final','icon-folder-open');
			$this->admin_library->setDetail("Bolt final manager");
			$this->admin_library->add_breadcrumb("Bolt Final","final_manager/final_bolt_list","icon-folder-open");
			$this->admin_library->add_breadcrumb("Add Final","final_manager/bolt_add","icon-plus");
			
			$this->admin_library->view("final_manager/bolt_add");
			$this->admin_library->output();
		}else{
			$this->bolt_final->add_final();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("final_manager/final_bolt_list");
		}
	}
	public function bolt_edit($final_id=0)
	{
		$this->admin_model->set_menu_key('8a11fc1831fdfa2b49e8203ff134f4ec');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->load->model('administrator/bolt_final');
		$this->form_validation->set_rules("customer_id","Supplier ID","trim|required");
		//$this->form_validation->set_rules("product_id","Product ID ","trim|required");
		if($this->form_validation->run()===false)
		{
			$this->_data['row'] = $this->bolt_final->getfinal($final_id);
			if(!$this->_data['row']){
				$this->session->set_flashdata("message-error","ไม่พบข้อมูลที่ต้องการแก้ไข.");
				admin_redirect("final_manager/final_bolt_list");
			}
			$this->admin_library->setTitle('Bolt Incoming','icon-folder-open');
			$this->admin_library->setDetail("Bolt incoming manager");
			$this->admin_library->add_breadcrumb("Bolt Incoming","final_manager/final_bolt_list","icon-folder-open");
			$this->admin_library->add_breadcrumb("Edit Incoming","final_manager/bolt_edit/".$final_id,"icon-edit");
			
			$this->admin_library->view("final_manager/bolt_edit");
			$this->admin_library->output();
		}else{
			$this->bolt_final->edit_final();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("final_manager/final_bolt_list");
		}
	}
	function bolt_delete($final_id){
		$this->admin_model->set_menu_key('8a11fc1831fdfa2b49e8203ff134f4ec');
		if(!$this->admin_model->check_permision("d")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("final_manager/final_bolt_list");
		}
		$this->load->model('administrator/bolt_final');
		$this->bolt_final->delete_final($final_id);
		$this->session->set_flashdata("message-success","ลบข้อมูลเรียบร้อยแล้ว.");
		admin_redirect("final_manager/final_bolt_list");
	}

		
	public function final_bolt_managering($final_id=0)
	{
		$this->admin_model->set_menu_key('a8ff0b967d8e9cda6122183af245d1ae');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("final_manager/final_bolt_list");
		}
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['final_id'] = $final_id;
		$product = $this->final_manager_model->get_bolt_product($this->_data['final_id']);
		$this->_data['std_list'] = $this->final_manager_model->get_product_standard_bolt($product['product_id']);
		$this->_data['product'] = $product;
		$this->_data['final_info'] =  $this->final_manager_model->get_final_bolt($this->_data['final_id']);
		
		//var_dump($this->_data['final_info']);
		
		if(!$_POST){
			$this->admin_library->view("final_manager/final_bolt_managering",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['bolt_managering_post_user_id'] = "1";
			$data['final_id'] = $this->input->post('final_id');
			$data['bolt_standard_id'] = $this->input->post('bolt_standard_id');
			$data['bolt_managering1'] = $this->input->post('manager1');
			$data['bolt_managering2'] = $this->input->post('manager2');
			$data['bolt_managering3'] = $this->input->post('manager3');
			$data['bolt_managering4'] = $this->input->post('manager4');
			$data['bolt_managering5'] = $this->input->post('manager5');
			$data['bolt_managering_status'] = $this->input->post('managering_status');
			$data['bolt_managering_remark'] = $this->input->post('remark');
							
			
			$bolt_managering_std=$this->final_manager_model->add_bolt_managering_cer($data);
			$bolt_managering_final = $this->final_manager_model->update_bolt_managering_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_manager/manager_bolt/$final_id");
		}
	}
	############ end bolt ##########################################
	
	
	################ start nut #####################################
	public function final_nut_list($page=1)
	{
		$QUERY_STRING = @$_SERVER['QUERY_STRING'];
		$QUERY_STRING = (trim($QUERY_STRING))?"?".$QUERY_STRING:"";
		$this->admin_model->set_menu_key('0167c858306624390a0c7ac5041aadb1');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$limit=25;
		$offset = ($page-1)*$limit;
		$offset = ($offset <0)?0:$offset;
		
		$this->admin_library->setTitle('Nut Final','icon-folder-open');
		
		$this->admin_library->setDetail("Nut final manager");
		$this->admin_model->initd($this);
		$this->admin_model->set_title('Nut final manager','icon-book');
		$this->load->model('administrator/nut_final');
		$this->admin_model->set_datatable($this->nut_final->dataTable($limit,$offset));
		
		$this->admin_model->set_column("final_id","No.",0,"icon-sort-alpha-asc");
		$this->admin_model->set_column("final_invoice_no","Invoice ID.",0,"icon-folder-open");
		$this->admin_model->set_column("final_nut_id","Product",0,"icon-shopping-cart");
		$this->admin_model->set_column("final_nut_size_id","Size",0,"icon-shopping-cart");
		$this->admin_model->set_column("final_customer_id","Customer",0,"icon-shopping-cart");
		
		$this->admin_model->set_column("final_createdtime","Create Date",0,"icon-calendar");
		$this->admin_model->set_column("final_manager","ผู้จัดการข้อมูล",0,"icon-user");
		$this->admin_model->set_column("test_status","สถานะการทดสอบ",0,'icon-code');
		
		$this->admin_model->set_column_callback("final_nut_id","product_nut_name_callback");
		$this->admin_model->set_column_callback("final_customer_id","customer_nut_name_callback");
		$this->admin_model->set_column_callback("final_manager","lot_manager_callback");
		$this->admin_model->set_column_callback("final_createdtime","date_callback");
		$this->admin_model->set_column_callback("test_status","manager_status_callback");
		$this->admin_model->set_column_callback("final_nut_size_id","product_size_nut_callback");

		
		$this->admin_model->set_top_button("เพิ่มข้อมูล","final_manager/nut_add","icon-plus","btn-info","r");
		
		$this->admin_model->set_action_button("แก้ไข","final_manager/nut_edit/[final_id]","icon-eye","btn-success","r");
	
		$this->admin_model->set_action_button("ทดสอบ nut","final_test/final_main_nut_testing/[final_nut_income_id]/[final_id]","icon-eye","btn-success","r");
		
		$this->admin_model->set_action_button("พิมพ์ nut","final_manager/print_nut/[final_nut_income_id]/[final_id]","icon-edit","btn-info","w");
		$this->admin_model->set_action_button("ลบ","final_manager/nut_delete/[final_id]","icon-trash","btn-danger","d");
		$this->admin_model->set_pagination("final_manager/final_nut_list{$QUERY_STRING}",$this->nut_final->export_data()->num_rows(),$limit,3);
		$this->admin_model->show_search_text();
		$search_q = trim(strip_tags($this->input->get("q")));
		if($search_q != ""){
			$this->admin_library->add_breadcrumb("ค้นหาคำว่า '{$search_q}'","final_manager/final_nut_list{$QUERY_STRING}","icon-search");
		}
		$this->admin_model->make_list();
		$this->admin_library->output();		
		
	}
	
	public function nut_add()
	{
		$this->admin_model->set_menu_key('0167c858306624390a0c7ac5041aadb1');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$this->load->model('administrator/nut_final');
		$this->form_validation->set_rules("customer_id","Customer ID","trim|required");
		$this->form_validation->set_rules("final_nut_id","Nut ID ","trim|required");
		
		if($this->form_validation->run()===false)
		{
			$this->admin_library->setTitle('Nut Final','icon-folder-open');
			$this->admin_library->setDetail("Nut final manager");
			$this->admin_library->add_breadcrumb("Nut Final","final_manager/final_nut_list","icon-folder-open");
			$this->admin_library->add_breadcrumb("Add Final","final_manager/nut_add","icon-plus");
			
			$this->admin_library->view("final_manager/nut_add");
			$this->admin_library->output();
		}else{
			$this->nut_final->add_final();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("final_manager/final_nut_list");
		}
	}
	public function nut_edit($final_id=0)
	{
		$this->admin_model->set_menu_key('0167c858306624390a0c7ac5041aadb1');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->load->model('administrator/nut_final');
		$this->form_validation->set_rules("customer_id","Supplier ID","trim|required");
		//$this->form_validation->set_rules("product_id","Product ID ","trim|required");
		if($this->form_validation->run()===false)
		{
			$this->_data['row'] = $this->nut_final->getfinal($final_id);
			if(!$this->_data['row']){
				$this->session->set_flashdata("message-error","ไม่พบข้อมูลที่ต้องการแก้ไข.");
				admin_redirect("final_manager/final_nut_list");
			}
			$this->admin_library->setTitle('Nut Incoming','icon-folder-open');
			$this->admin_library->setDetail("Nut incoming manager");
			$this->admin_library->add_breadcrumb("Nut Incoming","final_manager/final_nut_list","icon-folder-open");
			$this->admin_library->add_breadcrumb("Edit Incoming","final_manager/nut_edit/".$final_id,"icon-edit");
			
			$this->admin_library->view("final_manager/nut_edit");
			$this->admin_library->output();
		}else{
			$this->nut_final->edit_final();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("final_manager/final_nut_list");
		}
	}
	function nut_delete($final_id){
		$this->admin_model->set_menu_key('0167c858306624390a0c7ac5041aadb1');
		if(!$this->admin_model->check_permision("d")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("final_manager/final_nut_list");
		}
		$this->load->model('administrator/nut_final');
		$this->nut_final->delete_final($final_id);
		$this->session->set_flashdata("message-success","ลบข้อมูลเรียบร้อยแล้ว.");
		admin_redirect("final_manager/final_nut_list");
	}

	
	################ end nut #######################################
	
	################ start washer #######################################
		public function final_washer_list($page=1)
	{
		$QUERY_STRING = @$_SERVER['QUERY_STRING'];
		$QUERY_STRING = (trim($QUERY_STRING))?"?".$QUERY_STRING:"";
		$this->admin_model->set_menu_key('ce01da7485b1b183e6b32a08d4d64f47');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$limit=25;
		$offset = ($page-1)*$limit;
		$offset = ($offset <0)?0:$offset;
		
		$this->admin_library->setTitle('Washer Final','icon-folder-open');
		
		$this->admin_library->setDetail("Washer final manager");
		$this->admin_model->initd($this);
		$this->admin_model->set_title('Washer final manager','icon-book');
		$this->load->model('administrator/washer_final');
		$this->admin_model->set_datatable($this->washer_final->dataTable($limit,$offset));
		
		$this->admin_model->set_column("final_id","No.",0,"icon-sort-alpha-asc");
		$this->admin_model->set_column("final_invoice_no","Invoice ID.",0,"icon-folder-open");
		$this->admin_model->set_column("final_washer_id","Product",0,"icon-shopping-cart");
		$this->admin_model->set_column("final_washer_size_id","Size",0,"icon-shopping-cart");
		$this->admin_model->set_column("final_customer_id","Customer",0,"icon-shopping-cart");
		
		$this->admin_model->set_column("final_createdtime","Create Date",0,"icon-calendar");
		$this->admin_model->set_column("final_manager","ผู้จัดการข้อมูล",0,"icon-user");
		$this->admin_model->set_column("test_status","สถานะการทดสอบ",0,'icon-code');
		
		$this->admin_model->set_column_callback("final_washer_id","product_washer_name_callback");
		$this->admin_model->set_column_callback("final_customer_id","customer_washer_name_callback");
		$this->admin_model->set_column_callback("final_manager","lot_manager_callback");
		$this->admin_model->set_column_callback("final_createdtime","date_callback");
		$this->admin_model->set_column_callback("test_status","manager_status_callback");
		$this->admin_model->set_column_callback("final_washer_size_id","product_size_washer_callback");

		
		$this->admin_model->set_top_button("เพิ่มข้อมูล","final_manager/washer_add","icon-plus","btn-info","r");
		
		$this->admin_model->set_action_button("แก้ไข","final_manager/washer_edit/[final_id]","icon-eye","btn-success","r");
	
		$this->admin_model->set_action_button("ทดสอบ washer","final_test/final_main_washer_testing/[final_washer_income_id]/[final_id]","icon-eye","btn-success","r");
		
		$this->admin_model->set_action_button("พิมพ์ washer","final_manager/print_washer/[final_washer_income_id]/[final_id]","icon-edit","btn-info","w");
		$this->admin_model->set_action_button("ลบ","final_manager/washer_delete/[final_id]","icon-trash","btn-danger","d");
		$this->admin_model->set_pagination("final_manager/final_washer_list{$QUERY_STRING}",$this->washer_final->export_data()->num_rows(),$limit,3);
		$this->admin_model->show_search_text();
		$search_q = trim(strip_tags($this->input->get("q")));
		if($search_q != ""){
			$this->admin_library->add_breadcrumb("ค้นหาคำว่า '{$search_q}'","final_manager/final_washer_list{$QUERY_STRING}","icon-search");
		}
		$this->admin_model->make_list();
		$this->admin_library->output();		
		
	}
	
	public function washer_add()
	{
		$this->admin_model->set_menu_key('ce01da7485b1b183e6b32a08d4d64f47');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$this->load->model('administrator/washer_final');
		$this->form_validation->set_rules("customer_id","Customer ID","trim|required");
		$this->form_validation->set_rules("final_washer_id","washer ID ","trim|required");
		
		if($this->form_validation->run()===false)
		{
			$this->admin_library->setTitle('Washer Final','icon-folder-open');
			$this->admin_library->setDetail("Washer final manager");
			$this->admin_library->add_breadcrumb("Washer Final","final_manager/final_washer_list","icon-folder-open");
			$this->admin_library->add_breadcrumb("Add Final","final_manager/washer_add","icon-plus");
			
			$this->admin_library->view("final_manager/washer_add");
			$this->admin_library->output();
		}else{
			$this->washer_final->add_final();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("final_manager/final_washer_list");
		}
	}
	public function washer_edit($final_id=0)
	{
		$this->admin_model->set_menu_key('ce01da7485b1b183e6b32a08d4d64f47');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->load->model('administrator/washer_final');
		$this->form_validation->set_rules("customer_id","Supplier ID","trim|required");
		//$this->form_validation->set_rules("product_id","Product ID ","trim|required");
		if($this->form_validation->run()===false)
		{
			$this->_data['row'] = $this->washer_final->getfinal($final_id);
			if(!$this->_data['row']){
				$this->session->set_flashdata("message-error","ไม่พบข้อมูลที่ต้องการแก้ไข.");
				admin_redirect("final_manager/final_washer_list");
			}
			$this->admin_library->setTitle('Washer Incoming','icon-folder-open');
			$this->admin_library->setDetail("Washer incoming manager");
			$this->admin_library->add_breadcrumb("Washer Incoming","final_manager/final_washer_list","icon-folder-open");
			$this->admin_library->add_breadcrumb("Edit Incoming","final_manager/washer_edit/".$final_id,"icon-edit");
			
			$this->admin_library->view("final_manager/washer_edit");
			$this->admin_library->output();
		}else{
			$this->washer_final->edit_final();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("final_manager/final_washer_list");
		}
	}
	function washer_delete($final_id){
		$this->admin_model->set_menu_key('ce01da7485b1b183e6b32a08d4d64f47');
		if(!$this->admin_model->check_permision("d")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("final_manager/final_washer_list");
		}
		$this->load->model('administrator/washer_final');
		$this->washer_final->delete_final($final_id);
		$this->session->set_flashdata("message-success","ลบข้อมูลเรียบร้อยแล้ว.");
		admin_redirect("final_manager/final_washer_list");
	}
	
	################## end washer #######################################
	
	################ start spring #######################################
		public function final_spring_list($page=1)
	{
		$QUERY_STRING = @$_SERVER['QUERY_STRING'];
		$QUERY_STRING = (trim($QUERY_STRING))?"?".$QUERY_STRING:"";
		$this->admin_model->set_menu_key('cf6778ab14ce1643575cb1951b77a926');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$limit=25;
		$offset = ($page-1)*$limit;
		$offset = ($offset <0)?0:$offset;
		
		$this->admin_library->setTitle('Spring Washer Final','icon-folder-open');
		
		$this->admin_library->setDetail("Spring Washer final manager");
		$this->admin_model->initd($this);
		$this->admin_model->set_title('Spring Washer final manager','icon-book');
		$this->load->model('administrator/spring_final');
		$this->admin_model->set_datatable($this->spring_final->dataTable($limit,$offset));
		
		$this->admin_model->set_column("final_id","No.",0,"icon-sort-alpha-asc");
		$this->admin_model->set_column("final_invoice_no","Invoice ID.",0,"icon-folder-open");
		$this->admin_model->set_column("final_spring_id","Product",0,"icon-shopping-cart");
		$this->admin_model->set_column("final_spring_size_id","Size",0,"icon-shopping-cart");
		$this->admin_model->set_column("final_customer_id","Customer",0,"icon-shopping-cart");
		
		$this->admin_model->set_column("final_createdtime","Create Date",0,"icon-calendar");
		$this->admin_model->set_column("final_manager","ผู้จัดการข้อมูล",0,"icon-user");
		$this->admin_model->set_column("test_status","สถานะการทดสอบ",0,'icon-code');
		
		$this->admin_model->set_column_callback("final_spring_id","product_spring_name_callback");
		$this->admin_model->set_column_callback("final_customer_id","customer_spring_name_callback");
		$this->admin_model->set_column_callback("final_manager","lot_manager_callback");
		$this->admin_model->set_column_callback("final_createdtime","date_callback");
		$this->admin_model->set_column_callback("test_status","manager_status_callback");
		$this->admin_model->set_column_callback("final_spring_size_id","product_size_spring_callback");

		
		$this->admin_model->set_top_button("เพิ่มข้อมูล","final_manager/spring_add","icon-plus","btn-info","r");
		
		$this->admin_model->set_action_button("แก้ไข","final_manager/spring_edit/[final_id]","icon-eye","btn-success","r");
	
		$this->admin_model->set_action_button("ทดสอบ spring washer","final_test/final_main_spring_testing/[final_spring_income_id]/[final_id]","icon-eye","btn-success","r");
		
		$this->admin_model->set_action_button("พิมพ์ spring washer","final_manager/print_spring/[final_spring_income_id]/[final_id]","icon-edit","btn-info","w");
		$this->admin_model->set_action_button("ลบ","final_manager/spring_delete/[final_id]","icon-trash","btn-danger","d");
		$this->admin_model->set_pagination("final_manager/final_spring_list{$QUERY_STRING}",$this->spring_final->export_data()->num_rows(),$limit,3);
		$this->admin_model->show_search_text();
		$search_q = trim(strip_tags($this->input->get("q")));
		if($search_q != ""){
			$this->admin_library->add_breadcrumb("ค้นหาคำว่า '{$search_q}'","final_manager/final_spring_list{$QUERY_STRING}","icon-search");
		}
		$this->admin_model->make_list();
		$this->admin_library->output();		
		
	}
	
	public function spring_add()
	{
		$this->admin_model->set_menu_key('cf6778ab14ce1643575cb1951b77a926');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$this->load->model('administrator/spring_final');
		$this->form_validation->set_rules("customer_id","Customer ID","trim|required");
		$this->form_validation->set_rules("final_spring_id","Spring washer ID ","trim|required");
		
		if($this->form_validation->run()===false)
		{
			$this->admin_library->setTitle('Spring Washer Final','icon-folder-open');
			$this->admin_library->setDetail("spring final manager");
			$this->admin_library->add_breadcrumb("Spring Washer Final","final_manager/final_spring_list","icon-folder-open");
			$this->admin_library->add_breadcrumb("Add Final","final_manager/spring_add","icon-plus");
			
			$this->admin_library->view("final_manager/spring_add");
			$this->admin_library->output();
		}else{
			$this->spring_final->add_final();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("final_manager/final_spring_list");
		}
	}
	public function spring_edit($final_id=0)
	{
		$this->admin_model->set_menu_key('cf6778ab14ce1643575cb1951b77a926');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->load->model('administrator/spring_final');
		$this->form_validation->set_rules("customer_id","Supplier ID","trim|required");
		//$this->form_validation->set_rules("product_id","Product ID ","trim|required");
		if($this->form_validation->run()===false)
		{
			$this->_data['row'] = $this->spring_final->getfinal($final_id);
			if(!$this->_data['row']){
				$this->session->set_flashdata("message-error","ไม่พบข้อมูลที่ต้องการแก้ไข.");
				admin_redirect("final_manager/final_spring_list");
			}
			$this->admin_library->setTitle('Spring Washer Incoming','icon-folder-open');
			$this->admin_library->setDetail("Spring Washer incoming manager");
			$this->admin_library->add_breadcrumb("Spring Washer Incoming","final_manager/final_spring_list","icon-folder-open");
			$this->admin_library->add_breadcrumb("Edit Incoming","final_manager/spring_edit/".$final_id,"icon-edit");
			
			$this->admin_library->view("final_manager/spring_edit");
			$this->admin_library->output();
		}else{
			$this->spring_final->edit_final();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("final_manager/final_spring_list");
		}
	}
	function spring_delete($final_id){
		$this->admin_model->set_menu_key('ce01da7485b1b183e6b32a08d4d64f47');
		if(!$this->admin_model->check_permision("d")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("final_manager/final_spring_list");
		}
		$this->load->model('administrator/spring_final');
		$this->spring_final->delete_final($final_id);
		$this->session->set_flashdata("message-success","ลบข้อมูลเรียบร้อยแล้ว.");
		admin_redirect("final_manager/final_spring_list");
	}
	
	################## end spring #######################################
	
	################ start taper #######################################
		public function final_taper_list($page=1)
	{
		$QUERY_STRING = @$_SERVER['QUERY_STRING'];
		$QUERY_STRING = (trim($QUERY_STRING))?"?".$QUERY_STRING:"";
		$this->admin_model->set_menu_key('0f60303cbcadd8fef2c735d46c6159e8');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$limit=25;
		$offset = ($page-1)*$limit;
		$offset = ($offset <0)?0:$offset;
		
		$this->admin_library->setTitle('Taper Washer Final','icon-folder-open');
		
		$this->admin_library->setDetail("Taper Washer final manager");
		$this->admin_model->initd($this);
		$this->admin_model->set_title('Taper Washer final manager','icon-book');
		$this->load->model('administrator/taper_final');
		$this->admin_model->set_datatable($this->taper_final->dataTable($limit,$offset));
		
		$this->admin_model->set_column("final_id","No.",0,"icon-sort-alpha-asc");
		$this->admin_model->set_column("final_invoice_no","Invoice ID.",0,"icon-folder-open");
		$this->admin_model->set_column("final_taper_id","Product",0,"icon-shopping-cart");
		$this->admin_model->set_column("final_taper_size_id","Size",0,"icon-shopping-cart");
		$this->admin_model->set_column("final_customer_id","Customer",0,"icon-shopping-cart");
		
		$this->admin_model->set_column("final_createdtime","Create Date",0,"icon-calendar");
		$this->admin_model->set_column("final_manager","ผู้จัดการข้อมูล",0,"icon-user");
		$this->admin_model->set_column("test_status","สถานะการทดสอบ",0,'icon-code');
		
		$this->admin_model->set_column_callback("final_taper_id","product_taper_name_callback");
		$this->admin_model->set_column_callback("final_customer_id","customer_taper_name_callback");
		$this->admin_model->set_column_callback("final_manager","lot_manager_callback");
		$this->admin_model->set_column_callback("final_createdtime","date_callback");
		$this->admin_model->set_column_callback("test_status","manager_status_callback");
		$this->admin_model->set_column_callback("final_taper_size_id","product_size_taper_callback");

		
		$this->admin_model->set_top_button("เพิ่มข้อมูล","final_manager/taper_add","icon-plus","btn-info","r");
		
		$this->admin_model->set_action_button("แก้ไข","final_manager/taper_edit/[final_id]","icon-eye","btn-success","r");
	
		$this->admin_model->set_action_button("ทดสอบ Taper washer","final_test/final_main_taper_testing/[final_taper_income_id]/[final_id]","icon-eye","btn-success","r");
		
		$this->admin_model->set_action_button("พิมพ์ Taper washer","final_manager/print_taper/[final_taper_income_id]/[final_id]","icon-edit","btn-info","w");
		$this->admin_model->set_action_button("ลบ","final_manager/taper_delete/[final_id]","icon-trash","btn-danger","d");
		$this->admin_model->set_pagination("final_manager/final_taper_list{$QUERY_STRING}",$this->taper_final->export_data()->num_rows(),$limit,3);
		$this->admin_model->show_search_text();
		$search_q = trim(strip_tags($this->input->get("q")));
		if($search_q != ""){
			$this->admin_library->add_breadcrumb("ค้นหาคำว่า '{$search_q}'","final_manager/final_taper_list{$QUERY_STRING}","icon-search");
		}
		$this->admin_model->make_list();
		$this->admin_library->output();		
		
	}
	
	public function taper_add()
	{
		$this->admin_model->set_menu_key('0f60303cbcadd8fef2c735d46c6159e8');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$this->load->model('administrator/taper_final');
		$this->form_validation->set_rules("customer_id","Customer ID","trim|required");
		$this->form_validation->set_rules("final_taper_id","Taper washer ID ","trim|required");
		
		if($this->form_validation->run()===false)
		{
			$this->admin_library->setTitle('Taper Washer Final','icon-folder-open');
			$this->admin_library->setDetail("Taper final manager");
			$this->admin_library->add_breadcrumb("Taper Washer Final","final_manager/final_taper_list","icon-folder-open");
			$this->admin_library->add_breadcrumb("Add Final","final_manager/taper_add","icon-plus");
			
			$this->admin_library->view("final_manager/taper_add");
			$this->admin_library->output();
		}else{
			$this->taper_final->add_final();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("final_manager/final_taper_list");
		}
	}
	public function taper_edit($final_id=0)
	{
		$this->admin_model->set_menu_key('0f60303cbcadd8fef2c735d46c6159e8');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->load->model('administrator/taper_final');
		$this->form_validation->set_rules("customer_id","Supplier ID","trim|required");
		//$this->form_validation->set_rules("product_id","Product ID ","trim|required");
		if($this->form_validation->run()===false)
		{
			$this->_data['row'] = $this->taper_final->getfinal($final_id);
			if(!$this->_data['row']){
				$this->session->set_flashdata("message-error","ไม่พบข้อมูลที่ต้องการแก้ไข.");
				admin_redirect("final_manager/final_taper_list");
			}
			$this->admin_library->setTitle('Taper Washer Incoming','icon-folder-open');
			$this->admin_library->setDetail("Taper Washer incoming manager");
			$this->admin_library->add_breadcrumb("Taper Washer Incoming","final_manager/final_taper_list","icon-folder-open");
			$this->admin_library->add_breadcrumb("Edit Incoming","final_manager/taper_edit/".$final_id,"icon-edit");
			
			$this->admin_library->view("final_manager/taper_edit");
			$this->admin_library->output();
		}else{
			$this->taper_final->edit_final();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("final_manager/final_taper_list");
		}
	}
	function taper_delete($final_id){
		$this->admin_model->set_menu_key('ce01da7485b1b183e6b32a08d4d64f47');
		if(!$this->admin_model->check_permision("d")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("final_manager/final_taper_list");
		}
		$this->load->model('administrator/taper_final');
		$this->taper_final->delete_final($final_id);
		$this->session->set_flashdata("message-success","ลบข้อมูลเรียบร้อยแล้ว.");
		admin_redirect("final_manager/final_taper_list");
	}
	
	################## end Taper #######################################
	
	################ start Stud #######################################
		public function final_stud_list($page=1)
	{
		$QUERY_STRING = @$_SERVER['QUERY_STRING'];
		$QUERY_STRING = (trim($QUERY_STRING))?"?".$QUERY_STRING:"";
		$this->admin_model->set_menu_key('d18409cc6d89636957588a0b5db34553');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$limit=25;
		$offset = ($page-1)*$limit;
		$offset = ($offset <0)?0:$offset;
		
		$this->admin_library->setTitle('Stud Final','icon-folder-open');
		
		$this->admin_library->setDetail("Stud final manager");
		$this->admin_model->initd($this);
		$this->admin_model->set_title('Stud final manager','icon-book');
		$this->load->model('administrator/stud_final');
		$this->admin_model->set_datatable($this->stud_final->dataTable($limit,$offset));
		
		$this->admin_model->set_column("final_id","No.",0,"icon-sort-alpha-asc");
		$this->admin_model->set_column("final_invoice_no","Invoice ID.",0,"icon-folder-open");
		$this->admin_model->set_column("final_stud_id","Product",0,"icon-shopping-cart");
		$this->admin_model->set_column("final_stud_size_id","Size",0,"icon-shopping-cart");
		$this->admin_model->set_column("final_customer_id","Customer",0,"icon-shopping-cart");
		
		$this->admin_model->set_column("final_createdtime","Create Date",0,"icon-calendar");
		$this->admin_model->set_column("final_manager","ผู้จัดการข้อมูล",0,"icon-user");
		$this->admin_model->set_column("test_status","สถานะการทดสอบ",0,'icon-code');
		
		$this->admin_model->set_column_callback("final_stud_id","product_stud_name_callback");
		$this->admin_model->set_column_callback("final_customer_id","customer_stud_name_callback");
		$this->admin_model->set_column_callback("final_manager","lot_manager_callback");
		$this->admin_model->set_column_callback("final_createdtime","date_callback");
		$this->admin_model->set_column_callback("test_status","manager_status_callback");
		$this->admin_model->set_column_callback("final_stud_size_id","product_size_stud_callback");

		
		$this->admin_model->set_top_button("เพิ่มข้อมูล","final_manager/stud_add","icon-plus","btn-info","r");
		
		$this->admin_model->set_action_button("แก้ไข","final_manager/stud_edit/[final_id]","icon-eye","btn-success","r");
	
		$this->admin_model->set_action_button("ทดสอบ Stud","final_test/final_main_stud_testing/[final_stud_income_id]/[final_id]","icon-eye","btn-success","r");
		
		$this->admin_model->set_action_button("พิมพ์ Stud","final_manager/print_stud/[final_stud_income_id]/[final_id]","icon-edit","btn-info","w");
		$this->admin_model->set_action_button("ลบ","final_manager/stud_delete/[final_id]","icon-trash","btn-danger","d");
		$this->admin_model->set_pagination("final_manager/final_stud_list{$QUERY_STRING}",$this->stud_final->export_data()->num_rows(),$limit,3);
		$this->admin_model->show_search_text();
		$search_q = trim(strip_tags($this->input->get("q")));
		if($search_q != ""){
			$this->admin_library->add_breadcrumb("ค้นหาคำว่า '{$search_q}'","final_manager/final_stud_list{$QUERY_STRING}","icon-search");
		}
		$this->admin_model->make_list();
		$this->admin_library->output();		
		
	}
	
	public function stud_add()
	{
		$this->admin_model->set_menu_key('d18409cc6d89636957588a0b5db34553');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$this->load->model('administrator/stud_final');
		$this->form_validation->set_rules("customer_id","Customer ID","trim|required");
		$this->form_validation->set_rules("final_stud_id","Stud ID ","trim|required");
		
		if($this->form_validation->run()===false)
		{
			$this->admin_library->setTitle('Stud Final','icon-folder-open');
			$this->admin_library->setDetail("Stud final manager");
			$this->admin_library->add_breadcrumb("Stud Washer Final","final_manager/final_stud_list","icon-folder-open");
			$this->admin_library->add_breadcrumb("Add Final","final_manager/stud_add","icon-plus");
			
			$this->admin_library->view("final_manager/stud_add");
			$this->admin_library->output();
		}else{
			$this->stud_final->add_final();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("final_manager/final_stud_list");
		}
	}
	public function stud_edit($final_id=0)
	{
		$this->admin_model->set_menu_key('d18409cc6d89636957588a0b5db34553');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->load->model('administrator/stud_final');
		$this->form_validation->set_rules("customer_id","Supplier ID","trim|required");
		//$this->form_validation->set_rules("product_id","Product ID ","trim|required");
		if($this->form_validation->run()===false)
		{
			$this->_data['row'] = $this->stud_final->getfinal($final_id);
			if(!$this->_data['row']){
				$this->session->set_flashdata("message-error","ไม่พบข้อมูลที่ต้องการแก้ไข.");
				admin_redirect("final_manager/final_stud_list");
			}
			$this->admin_library->setTitle('Stud Incoming','icon-folder-open');
			$this->admin_library->setDetail("Stud incoming manager");
			$this->admin_library->add_breadcrumb("Stud Incoming","final_manager/final_stud_list","icon-folder-open");
			$this->admin_library->add_breadcrumb("Edit Incoming","final_manager/stud_edit/".$final_id,"icon-edit");
			
			$this->admin_library->view("final_manager/stud_edit");
			$this->admin_library->output();
		}else{
			$this->stud_final->edit_final();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("final_manager/final_stud_list");
		}
	}
	function stud_delete($final_id){
		$this->admin_model->set_menu_key('d18409cc6d89636957588a0b5db34553');
		if(!$this->admin_model->check_permision("d")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("final_manager/final_stud_list");
		}
		$this->load->model('administrator/stud_final');
		$this->stud_final->delete_final($final_id);
		$this->session->set_flashdata("message-success","ลบข้อมูลเรียบร้อยแล้ว.");
		admin_redirect("final_manager/final_stud_list");
	}
	
	################## end Stud #######################################
	
	
	
	
	###################start public #################################

	public function customer_name_callback($text,$row)
	{
		$row = $this->bolt_final->getcustomer_name($row['customer_id']);
		$text = $row['customer_name'];
		return $text;
	}
	
	public function customer_nut_name_callback($text,$row)
	{
		$row = $this->nut_final->getcustomer_name($row['customer_id']);
		$text = @$row['customer_name'];
		return $text;
	}
	public function customer_washer_name_callback($text,$row)
	{
		$row = $this->washer_final->getcustomer_name($row['customer_id']);
		$text = $row['customer_name'];
		return $text;
	}
	
	public function customer_spring_name_callback($text,$row)
	{
		$row = $this->spring_final->getcustomer_name($row['customer_id']);
		$text = $row['customer_name'];
		return $text;
	}
	
	public function customer_taper_name_callback($text,$row)
	{
		$row = $this->taper_final->getcustomer_name($row['customer_id']);
		$text = $row['customer_name'];
		return $text;
	}
	
	public function customer_stud_name_callback($text,$row)
	{
		$row = $this->stud_final->getcustomer_name($row['customer_id']);
		$text = $row['customer_name'];
		return $text;
	}
	
	public function lot_manager_callback($text,$row)
	{
		if(intval(@$row['final_createdid']) > 0){
			$user = $this->admin_library->getuserinfo($row['final_createdid']);
		}
		if(intval(@$row['final_updatedid']) > 0){
			$user = $this->admin_library->getuserinfo($row['final_updatedid']);
		}
		if(@$user){
			$text = $user['user_fullname'];
		}else{
			$text = "(ไม่ระบุ)";
		}
		return $text;	
	}
	
	public function manager_bolt_status_callback($text, $row){
		
		if($row['final_have_nut'] ==1 && $row['final_have_washer'] ==1){
			if($row['test_bolt_status']==2 && $row['test_nut_status']==2 && $row['test_washer_status']==2){
				return 'ผ่านการทดสอบ';
			}else if($row['test_bolt_status']==1 || $row['test_nut_status']==1 || $row['test_washer_status']==1){
				return 'ไม่ผ่านการทดสอบ';
			}else if($row['test_bolt_status']==0 || $row['test_nut_status']==0 || $row['test_washer_status']==0){
				return 'ยังไม่ได้ทดสอบ';
			}
		}elseif($row['final_have_nut'] ==0 && $row['final_have_washer'] ==1){
			if($row['test_bolt_status']==2 && $row['test_washer_status']==2){
				return 'ผ่านการทดสอบ';
			}else if($row['test_bolt_status']==1 || $row['test_washer_status']==1){
				return 'ไม่ผ่านการทดสอบ';
			}else if($row['test_bolt_status']==0 || $row['test_washer_status']==0){
				return 'ยังไม่ได้ทดสอบ';
			}			
		}elseif($row['final_have_nut'] ==1 && $row['final_have_washer'] ==0){
			if($row['test_bolt_status']==2 && $row['test_nut_status']==2){
				return 'ผ่านการทดสอบ';
			}else if($row['test_bolt_status']==1 || $row['test_nut_status']==1){
				return 'ไม่ผ่านการทดสอบ';
			}else if($row['test_bolt_status']==0 || $row['test_nut_status']==0){
				return 'ยังไม่ได้ทดสอบ';
			}			
		}else{
			if($row['test_bolt_status']==2){
				return 'ผ่านการทดสอบ';
			}else if($row['test_bolt_status']==1){
				return 'ไม่ผ่านการทดสอบ';
			}else if($row['test_bolt_status']==0){
				return 'ยังไม่ได้ทดสอบ';
			}	
			
		}
		
	}
	
	public function manager_status_callback($text, $row){
		if($text==2){
			return 'ผ่านการทดสอบ';
		}else if($text==1){
			return 'ไม่ผ่านการทดสอบ';
		}else{
			return 'ยังไม่ได้ทดสอบ';
		}
	}
	
	public function date_callback($text,$row)
	{
		if($text){
			$text = strtotime($text);
			$text = date("d M Y",$text);
		}else{
			$text = "(ไม่ระบุ)";
		}
		return $text;
	}
	
	public function product_name_callback($text,$row)
	{
		$row = $this->bolt_final->getproduct_name($row['final_bolt_id']);
		$text = @$row['product_name'] ." ". @$row['product_type'];
		return $text;
	}
	
	public function product_nut_name_callback($text,$row){
		$row = $this->nut_final->getproduct_name($row['final_nut_id']);
		$text = @$row['product_name'] ." ". @$row['product_type'];
		return $text;
		
	}
	
	public function product_washer_name_callback($text,$row){
		$row = $this->washer_final->getproduct_name($row['final_washer_id']);
		$text = @$row['product_name'] ." ". @$row['product_type'];
		return $text;
		
	}
	
	public function product_spring_name_callback($text,$row){
		$row = $this->spring_final->getproduct_name($row['final_spring_id']);
		$text = @$row['product_name'] ." ". @$row['product_type'];
		return $text;
		
	}
	
	public function product_taper_name_callback($text,$row){
		$row = $this->taper_final->getproduct_name($row['final_taper_id']);
		$text = @$row['product_name'] ." ". @$row['product_type'];
		return $text;
		
	}
	
	public function product_stud_name_callback($text,$row){
		$row = $this->stud_final->getproduct_name($row['final_stud_id']);
		$text = @$row['product_name'] ." ". @$row['product_type'];
		return $text;
		
	}
	
	public function product_size_callback($text,$row){
		$row = $this->bolt_final->get_size($text);
		$text = @$row['size_m'].'x'.@$row['size_p'].'x'.@$row['size_length'];
		return $text;
	}
	
	public function product_size_nut_callback($text,$row){
		$row = $this->nut_final->get_size($text);
		$text = @$row['size_m'].'x'.@$row['size_p'];
		return $text;
	}
	
	public function product_size_washer_callback($text,$row){
		$row = $this->washer_final->get_size($text);
		$text = @$row['size_m'].'x'.@$row['size_p'];
		return $text;
	}
	
	public function product_size_spring_callback($text,$row){
		$row = $this->spring_final->get_size($text);
		$text = @$row['size_m'].'x'.@$row['size_p'];
		return $text;
	}
	
	public function product_size_taper_callback($text,$row){
		$row = $this->taper_final->get_size($text);
		$text = @$row['size_m'].'x'.@$row['size_p'];
		return $text;
	}
	
	public function product_size_stud_callback($text,$row){
		$row = $this->stud_final->get_size($text);
		$text = @$row['size_m'].'x'.@$row['size_p'];
		return $text;
	}
	
	public function get_size_bolt_byid(){
		$boltid = $this->input->post('bolt_id');
		$this->load->model('administrator/bolt_final');
		$this->_data['sizelist'] = $this->bolt_final->get_boltsize_byid($boltid);
		
		$this->load->view('administrator/views/final_manager/sizelist_bolt', $this->_data);
	}
	
	public function get_income_bolt_byid(){
		$boltsizeid = $this->input->post('bolt_size_id');
		$boltid = $this->input->post('bolt_id');
		
		$this->load->model('administrator/bolt_final');
		$this->_data['imcomelist'] = $this->bolt_final->get_boltincome_byid($boltid,$boltsizeid);
		
		$this->load->view('administrator/views/final_manager/incomelist_bolt', $this->_data);
	
	}
	
	public function get_size_nut_byid(){
		$boltid = $this->input->post('nut_id');
		$this->load->model('administrator/nut_final');
		$this->_data['sizelist'] = $this->nut_final->get_nutsize_byid($boltid);
		
		$this->load->view('administrator/views/final_manager/sizelist', $this->_data);
	}
	
	public function get_income_nut_byid(){
		$nutsizeid = $this->input->post('nut_size_id');
		$nutid = $this->input->post('nut_id');
		
		$this->load->model('administrator/nut_final');
		$this->_data['imcomelist'] = $this->nut_final->get_nutincome_byid($nutid,$nutsizeid);
		
		$this->load->view('administrator/views/final_manager/incomelist_nut', $this->_data);
	}
		
	public function get_size_washer_byid(){
		$washerid = $this->input->post('washer_id');
		$this->load->model('administrator/washer_final');
		$this->_data['sizelist'] = $this->washer_final->get_washersize_byid($washerid);
		
		$this->load->view('administrator/views/final_manager/sizelist', $this->_data);
	}
	
	public function get_income_washer_byid(){
		$washersizeid = $this->input->post('washer_size_id');
		$washerid = $this->input->post('washer_id');
		
		$this->load->model('administrator/washer_final');
		$this->_data['imcomelist'] = $this->washer_final->get_washerincome_byid($washerid,$washersizeid);
		
		$this->load->view('administrator/views/final_manager/incomelist_washer', $this->_data);
	}
	
	public function get_size_spring_byid(){
		$springid = $this->input->post('spring_id');
		$this->load->model('administrator/spring_final');
		$this->_data['sizelist'] = $this->spring_final->get_springsize_byid($springid);
		
		$this->load->view('administrator/views/final_manager/sizelist', $this->_data);
	}
	
	public function get_size_taper_byid(){
		$taperid = $this->input->post('taper_id');
		$this->load->model('administrator/taper_final');
		$this->_data['sizelist'] = $this->taper_final->get_tapersize_byid($taperid);
		
		$this->load->view('administrator/views/final_manager/sizelist', $this->_data);
	}
	
	public function get_size_stud_byid(){
		$studid = $this->input->post('stud_id');
		$this->load->model('administrator/stud_final');
		$this->_data['sizelist'] = $this->stud_final->get_studsize_byid($studid);
		
		$this->load->view('administrator/views/final_manager/sizelist', $this->_data);
	}
	
	public function get_income_spring_byid(){
		$springsizeid = $this->input->post('spring_size_id');
		$springid = $this->input->post('spring_id');
		
		$this->load->model('administrator/spring_final');
		$this->_data['imcomelist'] = $this->spring_final->get_springincome_byid($springid,$springsizeid);
		
		$this->load->view('administrator/views/final_manager/incomelist_spring', $this->_data);
	}
	
	public function get_income_taper_byid(){
		$tapersizeid = $this->input->post('taper_size_id');
		$taperid = $this->input->post('taper_id');
		
		$this->load->model('administrator/taper_final');
		$this->_data['imcomelist'] = $this->taper_final->get_taperincome_byid($taperid,$tapersizeid);
		
		$this->load->view('administrator/views/final_manager/incomelist_taper', $this->_data);
	}
	
	public function get_income_stud_byid(){
		$studsizeid = $this->input->post('stud_size_id');
		$studid = $this->input->post('stud_id');
		
		$this->load->model('administrator/stud_final');
		$this->_data['imcomelist'] = $this->stud_final->get_studincome_byid($studid,$studsizeid);
		
		$this->load->view('administrator/views/final_manager/incomelist_stud', $this->_data);
	}
	
	
	public function print_bolt($income_id,$nut_income,$washer_income,$final_id)
	{
		$this->load->model('final_test_model');
		$this->load->model('income_test_model');
		$this->load->model('administrator/bolt_final');
		$this->load->model('administrator/nut_final');
		$this->load->model('administrator/washer_final');
		$this->load->model('administrator/chemical_test_final_model');
		
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$checked = $this->final_test_model->check_final_tested($this->_data['income_id'],$final_id);
		
		if($checked > 0){
			$product = $this->final_test_model->get_bolt_product($this->_data['income_id'],$final_id);
			$this->_data['std_list'] = $this->final_test_model->get_product_standard_bolt($product['product_id']);
			$this->_data['product'] = $product;
			$this->_data['income_info'] =  $this->final_test_model->get_final_bolt($this->_data['income_id'],$final_id);
			//$this->_data['income_data'] =  $this->final_test_model->get_income_tested($this->_data['income_id']);
			$this->_data['shipper'] =  $this->bolt_final->getcustomer_name($this->_data['income_info']['customer_id']);
			$this->_data['size'] = $this->bolt_final->get_size($this->_data['income_info']['final_bolt_size_id']);
			$this->_data['cer_size_list'] = $this->final_test_model->get_product_standard_size_bolt($product['product_id'],$this->_data['income_info']['final_bolt_size_id']);
			$this->_data['info'] = $this->income_test_model->get_income_bolt($this->_data['income_id']);
			
			$this->_data['bolt_thickness'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_bolt_cer($income_id,$final_id);
			$this->_data['bolt_thickness_info'] = $this->chemical_test_final_model->get_thickness_by_id($this->_data['bolt_thickness']['thickness_id']); 
			
			########### nut #########
			$this->_data['nut_income'] = $nut_income;
			
			if($nut_income != "0"){
				 
				$product_nut = $this->final_test_model->get_nut_product($nut_income,$final_id);
				
				$this->_data['std_list_nut'] = $this->final_test_model->get_product_standard_nut($product_nut['product_id']);
				$this->_data['product_nut'] = $product_nut;
				$this->_data['income_info_nut'] =  $this->final_test_model->get_final_nut($nut_income,$final_id);
			
			//$this->_data['income_data'] =  $this->final_test_model->get_income_nut_tested($this->_data['income_id']);
				//$this->_data['shipper'] =  $this->nut_final->getcustomer_name($this->_data['income_info_nut']['customer_id']);
				$this->_data['size_nut'] = $this->nut_final->get_size($this->_data['income_info_nut']['final_nut_size_id']);
				$this->_data['cer_size_list_nut'] = $this->final_test_model->get_product_standard_size_nut($product_nut['product_id'],$this->_data['income_info_nut']['final_nut_size_id']);
				$this->_data['info_nut'] = $this->income_test_model->get_income_nut($nut_income);
				
				
				$this->_data['nut_thickness'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_nut_cer($income_id,$final_id);
				$this->_data['nut_thickness_info'] = $this->chemical_test_final_model->get_thickness_by_id($this->_data['nut_thickness']['thickness_id']);
				
			}
			############ end nut ##################
			
			################### washer ########################
			$this->_data['washer_income'] = $washer_income;
			if($washer_income != "0"){
				
				$product_washer = $this->final_test_model->get_washer_product($washer_income,$final_id);
			
				$this->_data['std_list_washer'] = $this->final_test_model->get_product_standard_washer($product_washer['product_id']);
				$this->_data['product_washer'] = $product_washer;
				$this->_data['income_info_washer'] =  $this->final_test_model->get_final_washer($washer_income,$final_id);
			
			//$this->_data['income_data'] =  $this->final_test_model->get_income_washer_tested($this->_data['income_id']);
				//$this->_data['shipper'] =  $this->washer_final->getcustomer_name($this->_data['income_info']['customer_id']);
				$this->_data['size_washer'] = $this->washer_final->get_size($this->_data['income_info_washer']['final_washer_size_id']);
				$this->_data['cer_size_list_washer'] = $this->final_test_model->get_product_standard_size_washer($product_washer['product_id'],$this->_data['income_info_washer']['final_washer_size_id']);
				$this->_data['info_washer'] = $this->income_test_model->get_income_washer($washer_income);
			}
			
			$this->_data['washer_thickness'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_washer_cer($income_id,$final_id);
				$this->_data['washer_thickness_info'] = $this->chemical_test_final_model->get_thickness_by_id($this->_data['washer_thickness']['thickness_id']);
			
			//$this->_data['income_nut_info'] =  $this->final_test_model->get_final_nut($this->_data['income_id']);
			
			//$this->_data['income_cer_size_data'] =  $this->final_test_model->get_income_cer_size_tested($this->_data['income_id']);
			header("Content-type: application/vnd.ms-excel");
			header("Content-Disposition: attachment;Filename=final_bolt_report_".$income_id.".xls");
			$this->load->view('administrator/views/final_manager/print_bolt', $this->_data);
		}else{
			$this->session->set_flashdata("message-warning","ข้อมูลยังไม่ได้ถูกทดสอบ.");
			admin_redirect("final_manager/final_bolt_list");
		}
				
	}
	
	public function print_nut($income_id,$final_id)
	{
		$this->load->model('final_test_model');
		$this->load->model('income_test_model');
		//$this->load->model('administrator/bolt_final');
		$this->load->model('administrator/nut_final');
		//$this->load->model('administrator/washer_final');
		$this->load->model('administrator/chemical_test_final_model');
		
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$checked = $this->final_test_model->check_final_nut_tested($this->_data['income_id'],$final_id);
		
		if($checked > 0){
			$product = $this->final_test_model->get_nut_product_main($this->_data['income_id'],$final_id);
			
			$this->_data['product'] = $product;
						
			########### nut #########
			$this->_data['nut_income'] = $income_id;
			

				 
				$product_nut = $this->final_test_model->get_nut_product_main($income_id,$final_id);
				
				$this->_data['std_list_nut'] = $this->final_test_model->get_product_standard_nut($product_nut['product_id']);
				$this->_data['product_nut'] = $product_nut;
				$this->_data['income_info_nut'] =  $this->final_test_model->get_final_nut_main($income_id,$final_id);
				$this->_data['shipper'] =  $this->nut_final->getcustomer_name($this->_data['income_info_nut']['customer_id']);
			//$this->_data['income_data'] =  $this->final_test_model->get_income_nut_tested($this->_data['income_id']);
				//$this->_data['shipper'] =  $this->nut_final->getcustomer_name($this->_data['income_info_nut']['customer_id']);
				$this->_data['size_nut'] = $this->nut_final->get_size($this->_data['income_info_nut']['final_nut_size_id']);
				$this->_data['cer_size_list_nut'] = $this->final_test_model->get_product_standard_size_nut($product_nut['product_id'],$this->_data['income_info_nut']['final_nut_size_id']);
				$this->_data['info_nut'] = $this->income_test_model->get_income_nut($income_id);
				
				
				$this->_data['nut_thickness'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_nut_cer($income_id,$final_id);
				$this->_data['nut_thickness_info'] = $this->chemical_test_final_model->get_thickness_by_id($this->_data['nut_thickness']['thickness_id']);
				
			
			############ end nut ##################

			
			//$this->_data['income_nut_info'] =  $this->final_test_model->get_final_nut($this->_data['income_id']);
			
			//$this->_data['income_cer_size_data'] =  $this->final_test_model->get_income_cer_size_tested($this->_data['income_id']);
			header("Content-type: application/vnd.ms-excel");
			header("Content-Disposition: attachment;Filename=final_nut_report_".$income_id.".xls");
			$this->load->view('administrator/views/final_manager/print_nut', $this->_data);
		}else{
			$this->session->set_flashdata("message-warning","ข้อมูลยังไม่ได้ถูกทดสอบ.");
			admin_redirect("final_manager/final_nut_list");
		}
				
				
	}
	
	public function print_washer($income_id,$final_id)
	{
		$this->load->model('final_test_model');
		$this->load->model('income_test_model');
		//$this->load->model('administrator/bolt_final');
		//$this->load->model('administrator/nut_final');
		$this->load->model('administrator/washer_final');
		$this->load->model('administrator/chemical_test_final_model');
		
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$checked = $this->final_test_model->check_final_washer_tested($this->_data['income_id'],$final_id);
		
		if($checked > 0){
			$product = $this->final_test_model->get_washer_product_main($this->_data['income_id'],$final_id);
			
			$this->_data['product'] = $product;
						
			########### washer #########
			$this->_data['washer_income'] = $income_id;
			

				 
				$product_washer = $this->final_test_model->get_washer_product_main($income_id,$final_id);
				
				$this->_data['std_list_washer'] = $this->final_test_model->get_product_standard_washer($product_washer['product_id']);
				$this->_data['product_washer'] = $product_washer;
				$this->_data['income_info_washer'] =  $this->final_test_model->get_final_washer_main($income_id,$final_id);
				$this->_data['shipper'] =  $this->washer_final->getcustomer_name($this->_data['income_info_washer']['customer_id']);
			//$this->_data['income_data'] =  $this->final_test_model->get_income_washer_tested($this->_data['income_id']);
				//$this->_data['shipper'] =  $this->washer_final->getcustomer_name($this->_data['income_info_washer']['customer_id']);
				$this->_data['size_washer'] = $this->washer_final->get_size($this->_data['income_info_washer']['final_washer_size_id']);
				$this->_data['cer_size_list_washer'] = $this->final_test_model->get_product_standard_size_washer($product_washer['product_id'],$this->_data['income_info_washer']['final_washer_size_id']);
				$this->_data['info_washer'] = $this->income_test_model->get_income_washer($income_id);
				
				
				$this->_data['washer_thickness'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_washer_cer($income_id,$final_id);
				$this->_data['washer_thickness_info'] = $this->chemical_test_final_model->get_thickness_by_id($this->_data['washer_thickness']['thickness_id']);
				
			
			############ end washer ##################

			
			//$this->_data['income_washer_info'] =  $this->final_test_model->get_final_washer($this->_data['income_id']);
			
			//$this->_data['income_cer_size_data'] =  $this->final_test_model->get_income_cer_size_tested($this->_data['income_id']);
			header("Content-type: application/vnd.ms-excel");
			header("Content-Disposition: attachment;Filename=final_washer_report_".$income_id.".xls");
			$this->load->view('administrator/views/final_manager/print_washer', $this->_data);
		}else{
			$this->session->set_flashdata("message-warning","ข้อมูลยังไม่ได้ถูกทดสอบ.");
			admin_redirect("final_manager/final_washer_list");
		}
				
								
	}

	public function print_spring($income_id,$final_id)
	{
		$this->load->model('final_test_model');
		$this->load->model('income_test_model');
		//$this->load->model('administrator/bolt_final');
		//$this->load->model('administrator/nut_final');
		$this->load->model('administrator/spring_final');
		$this->load->model('administrator/chemical_test_final_model');
		
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$checked = $this->final_test_model->check_final_spring_tested($this->_data['income_id'],$final_id);
		
		if($checked > 0){
			$product = $this->final_test_model->get_spring_product_main($this->_data['income_id'],$final_id);
			
			$this->_data['product'] = $product;
						
			########### spring #########
			$this->_data['spring_income'] = $income_id;
			

				 
				$product_spring = $this->final_test_model->get_spring_product_main($income_id,$final_id);
				
				$this->_data['std_list_spring'] = $this->final_test_model->get_product_standard_spring($product_spring['product_id']);
				$this->_data['product_spring'] = $product_spring;
				$this->_data['income_info_spring'] =  $this->final_test_model->get_final_spring_main($income_id,$final_id);
				$this->_data['shipper'] =  $this->spring_final->getcustomer_name($this->_data['income_info_spring']['customer_id']);
			//$this->_data['income_data'] =  $this->final_test_model->get_income_spring_tested($this->_data['income_id']);
				//$this->_data['shipper'] =  $this->spring_final->getcustomer_name($this->_data['income_info_spring']['customer_id']);
				$this->_data['size_spring'] = $this->spring_final->get_size($this->_data['income_info_spring']['final_spring_size_id']);
				$this->_data['cer_size_list_spring'] = $this->final_test_model->get_product_standard_size_spring($product_spring['product_id'],$this->_data['income_info_spring']['final_spring_size_id']);
				$this->_data['info_spring'] = $this->income_test_model->get_income_spring($income_id);
				
				
				$this->_data['spring_thickness'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_spring_cer($income_id,$final_id);
				$this->_data['spring_thickness_info'] = $this->chemical_test_final_model->get_thickness_by_id($this->_data['spring_thickness']['thickness_id']);
				
			
			############ end spring ##################

			
			//$this->_data['income_spring_info'] =  $this->final_test_model->get_final_spring($this->_data['income_id']);
			
			//$this->_data['income_cer_size_data'] =  $this->final_test_model->get_income_cer_size_tested($this->_data['income_id']);
			header("Content-type: application/vnd.ms-excel");
			header("Content-Disposition: attachment;Filename=final_spring_report_".$income_id.".xls");
			$this->load->view('administrator/views/final_manager/print_spring', $this->_data);
		}else{
			$this->session->set_flashdata("message-warning","ข้อมูลยังไม่ได้ถูกทดสอบ.");
			admin_redirect("final_manager/final_spring_list");
		}
				
								
	}
	
	public function print_taper($income_id,$final_id)
	{
		$this->load->model('final_test_model');
		$this->load->model('income_test_model');
		//$this->load->model('administrator/bolt_final');
		//$this->load->model('administrator/nut_final');
		$this->load->model('administrator/taper_final');
		$this->load->model('administrator/chemical_test_final_model');
		
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$checked = $this->final_test_model->check_final_taper_tested($this->_data['income_id'],$final_id);
		
		if($checked > 0){
			$product = $this->final_test_model->get_taper_product_main($this->_data['income_id'],$final_id);
			
			$this->_data['product'] = $product;
						
			########### taper #########
			$this->_data['taper_income'] = $income_id;
			

				 
				$product_taper = $this->final_test_model->get_taper_product_main($income_id,$final_id);
				
				$this->_data['std_list_taper'] = $this->final_test_model->get_product_standard_taper($product_taper['product_id']);
				$this->_data['product_taper'] = $product_taper;
				$this->_data['income_info_taper'] =  $this->final_test_model->get_final_taper_main($income_id,$final_id);
				$this->_data['shipper'] =  $this->taper_final->getcustomer_name($this->_data['income_info_taper']['customer_id']);
			//$this->_data['income_data'] =  $this->final_test_model->get_income_taper_tested($this->_data['income_id']);
				//$this->_data['shipper'] =  $this->taper_final->getcustomer_name($this->_data['income_info_taper']['customer_id']);
				$this->_data['size_taper'] = $this->taper_final->get_size($this->_data['income_info_taper']['final_taper_size_id']);
				$this->_data['cer_size_list_taper'] = $this->final_test_model->get_product_standard_size_taper($product_taper['product_id'],$this->_data['income_info_taper']['final_taper_size_id']);
				$this->_data['info_taper'] = $this->income_test_model->get_income_taper($income_id);
				
				
				$this->_data['taper_thickness'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_taper_cer($income_id,$final_id);
				$this->_data['taper_thickness_info'] = $this->chemical_test_final_model->get_thickness_by_id($this->_data['taper_thickness']['thickness_id']);
				
			
			############ end taper ##################

			
			//$this->_data['income_taper_info'] =  $this->final_test_model->get_final_taper($this->_data['income_id']);
			
			//$this->_data['income_cer_size_data'] =  $this->final_test_model->get_income_cer_size_tested($this->_data['income_id']);
			header("Content-type: application/vnd.ms-excel");
			header("Content-Disposition: attachment;Filename=final_taper_report_".$income_id.".xls");
			$this->load->view('administrator/views/final_manager/print_taper', $this->_data);
		}else{
			$this->session->set_flashdata("message-warning","ข้อมูลยังไม่ได้ถูกทดสอบ.");
			admin_redirect("final_manager/final_taper_list");
		}
				
								
	}
	
	public function print_stud($income_id,$final_id)
	{
		$this->load->model('final_test_model');
		$this->load->model('income_test_model');
		//$this->load->model('administrator/bolt_final');
		//$this->load->model('administrator/nut_final');
		$this->load->model('administrator/stud_final');
		$this->load->model('administrator/chemical_test_final_model');
		
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$checked = $this->final_test_model->check_final_stud_tested($this->_data['income_id'],$final_id);
		
		if($checked > 0){
			$product = $this->final_test_model->get_stud_product_main($this->_data['income_id'],$final_id);
			
			$this->_data['product'] = $product;
						
			########### stud #########
			$this->_data['stud_income'] = $income_id;
			

				 
				$product_stud = $this->final_test_model->get_stud_product_main($income_id,$final_id);
				
				$this->_data['std_list_stud'] = $this->final_test_model->get_product_standard_stud($product_stud['product_id']);
				$this->_data['product_stud'] = $product_stud;
				$this->_data['income_info_stud'] =  $this->final_test_model->get_final_stud_main($income_id,$final_id);
				$this->_data['shipper'] =  $this->stud_final->getcustomer_name($this->_data['income_info_stud']['customer_id']);
			//$this->_data['income_data'] =  $this->final_test_model->get_income_stud_tested($this->_data['income_id']);
				//$this->_data['shipper'] =  $this->stud_final->getcustomer_name($this->_data['income_info_stud']['customer_id']);
				$this->_data['size_stud'] = $this->stud_final->get_size($this->_data['income_info_stud']['final_stud_size_id']);
				$this->_data['cer_size_list_stud'] = $this->final_test_model->get_product_standard_size_stud($product_stud['product_id'],$this->_data['income_info_stud']['final_stud_size_id']);
				$this->_data['info_stud'] = $this->income_test_model->get_income_stud($income_id);
				
				
				$this->_data['stud_thickness'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_stud_cer($income_id,$final_id);
				$this->_data['stud_thickness_info'] = $this->chemical_test_final_model->get_thickness_by_id($this->_data['stud_thickness']['thickness_id']);
				
			
			############ end stud ##################

			
			//$this->_data['income_stud_info'] =  $this->final_test_model->get_final_stud($this->_data['income_id']);
			
			//$this->_data['income_cer_size_data'] =  $this->final_test_model->get_income_cer_size_tested($this->_data['income_id']);
			header("Content-type: application/vnd.ms-excel");
			header("Content-Disposition: attachment;Filename=final_stud_report_".$income_id.".xls");
			$this->load->view('administrator/views/final_manager/print_stud', $this->_data);
		}else{
			$this->session->set_flashdata("message-warning","ข้อมูลยังไม่ได้ถูกทดสอบ.");
			admin_redirect("final_manager/final_stud_list");
		}
				
								
	}
	
	
	
	
}