<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Income_test extends CI_Controller {
	var $_data = array();
	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->library('image_lib');
		$this->load->library('admin_library');	
		$this->admin_library->forceLogin();
		$this->load->model('income_test_model');
		$this->load->model('administrator/admin_model');
	}
	public function index()
	{
			
	}
	
	#############Supplier #######################
	
		
	public function add_bolt_testing($income_id=0)
	{
		$this->admin_model->set_menu_key('a8ff0b967d8e9cda6122183af245d1ae');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("income_manager/bolt_list");
		}
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('test_avg[]','ค่าเฉลี่ยการทดสอบ','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctest_avg[]','ค่าเฉลี่ยนทดสอบ size ','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size','trim');
		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		$checked = $this->income_test_model->check_income_tested($this->_data['income_id']);
		
		
		
		if($checked > 0){
			admin_redirect("income_test/edit_bolt_testing/$income_id");
		}
		
		$product = $this->income_test_model->get_bolt_product($this->_data['income_id']);
		
		$this->_data['income_info'] =  $this->income_test_model->get_income_bolt($this->_data['income_id']);
		$this->_data['std_list'] = $this->income_test_model->get_product_standard_bolt($product['product_id']);
		$this->_data['cer_size_list'] = $this->income_test_model->get_product_standard_size_bolt($product['product_id'],$this->_data['income_info']['size_id']);
		$this->_data['product'] = $product;
		
		if($this->_data['std_list']->num_rows()==0){
			$this->session->set_flashdata("message-warning","ไม่สามารถทดสอบได้ เนื่องจากไม่มีค่า Standard");
			admin_redirect("income_manager/bolt_list/");
		}
		
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("income_test/add_bolt_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['bolt_testing_post_user_id'] = $this->admin_library->user_id();
										
			
			$bolt_testing_std=$this->income_test_model->add_bolt_testing_std($data);
			
			//$bolt_testing_final = $this->income_test_model->add_bolt_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test/test_bolt/$income_id");
		}
	}

	public function edit_bolt_testing($income_id=0)
	{
		$this->admin_model->set_menu_key('a8ff0b967d8e9cda6122183af245d1ae');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("income_manager/bolt_list");
		}
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('test_avg[]','ค่าเฉลี่ยการทดสอบ','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest_avg[]','ค่าเฉลี่ยทดสอบ size','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size','trim');
		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		
		$product = $this->income_test_model->get_bolt_product($this->_data['income_id']);
		
		$this->_data['income_info'] =  $this->income_test_model->get_income_bolt($this->_data['income_id']);
		$this->_data['std_list'] = $this->income_test_model->get_product_standard_bolt($product['product_id']);
		$this->_data['cer_size_list'] = $this->income_test_model->get_product_standard_size_bolt($product['product_id'],$this->_data['income_info']['size_id']);
		$this->_data['product'] = $product;
		
		
		
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("income_test/edit_bolt_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['bolt_testing_post_user_id'] = $this->admin_library->user_id();
										
			
			$bolt_testing_std=$this->income_test_model->update_bolt_testing_std($data);
			
			//$bolt_testing_final = $this->income_test_model->update_bolt_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test/test_bolt/$income_id");
		}
	}


	
	public function add_nut_testing($income_id=0)
	{
		$this->admin_model->set_menu_key('1bdb9da104e5796f401cca761ef1c9f2');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("income_manager/nut_list");
		}
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('test_avg[]','ค่าเฉลี่ยการทดสอบ','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctest_avg[]','ค่าเฉลี่ยการทดสอบ size','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size','trim');
		
		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		
		$product = $this->income_test_model->get_nut_product($this->_data['income_id']);
		
		$this->_data['income_info'] =  $this->income_test_model->get_income_nut($this->_data['income_id']);
		
		$this->_data['std_list'] = $this->income_test_model->get_product_standard_nut($product['product_id']);
		$this->_data['cer_size_list'] = $this->income_test_model->get_product_standard_size_nut($product['product_id'],$this->_data['income_info']['size_id']);
		
		$this->_data['product'] = $product;
		
		$checked = $this->income_test_model->check_income_nut_tested($this->_data['income_id']);
		
		if($this->_data['std_list']->num_rows()==0){
			$this->session->set_flashdata("message-warning","ไม่สามารถทดสอบได้ เนื่องจากไม่มีค่า Standard");
			admin_redirect("income_manager/nut_list/");
		}
		if($checked > 0){
			admin_redirect("income_test/edit_nut_testing/$income_id");
		}
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("income_test/add_nut_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['nut_testing_post_user_id'] = $this->admin_library->user_id();
							
			
			$nut_testing_std=$this->income_test_model->add_nut_testing_std($data);
			
			//$nut_testing_final = $this->income_test_model->add_nut_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test/test_nut/$income_id");
		}
	}
	
	public function edit_nut_testing($income_id=0)
	{
		$this->admin_model->set_menu_key('1bdb9da104e5796f401cca761ef1c9f2');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("income_manager/nut_list");
		}
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('test_avg[]','ค่าเฉลี่ยการทดสอบ','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctest_avg[]','ค่าเฉลี่ยการทดสอบ size','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size','trim');
		
		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		
		$product = $this->income_test_model->get_nut_product($this->_data['income_id']);
		
		$this->_data['income_info'] =  $this->income_test_model->get_income_nut($this->_data['income_id']);
		
		$this->_data['std_list'] = $this->income_test_model->get_product_standard_nut($product['product_id']);
		$this->_data['cer_size_list'] = $this->income_test_model->get_product_standard_size_nut($product['product_id'],$this->_data['income_info']['size_id']);
		
		$this->_data['product'] = $product;
		
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("income_test/edit_nut_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['nut_testing_post_user_id'] = $this->admin_library->user_id();
							
			
			$nut_testing_std=$this->income_test_model->update_nut_testing_std($data);
			
			//$nut_testing_final = $this->income_test_model->update_nut_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test/test_nut/$income_id");
		}
	}


	public function add_washer_testing($income_id=0)
	{
		$this->admin_model->set_menu_key('833675c0ae6c061f4e8d87c294ebb1ec');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("income_manager/washer_list");
		}
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('test_avg[]','ค่าเฉลี่ยการทดสอบ','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctest_avg[]','ค่าเฉลี่ยการทดสอบ size','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');
		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		
		$product = $this->income_test_model->get_washer_product($this->_data['income_id']);
		
		$this->_data['income_info'] =  $this->income_test_model->get_income_washer($this->_data['income_id']);

		$this->_data['std_list'] = $this->income_test_model->get_product_standard_washer($product['product_id']);
		$this->_data['cer_size_list'] = $this->income_test_model->get_product_standard_size_washer($product['product_id'],$this->_data['income_info']['size_id']);
		
		$this->_data['product'] = $product;
				$checked = $this->income_test_model->check_income_washer_tested($this->_data['income_id']);
		//exit($this->admin_library->user_id());
		if($this->_data['std_list']->num_rows()==0){
			$this->session->set_flashdata("message-warning","ไม่สามารถทดสอบได้ เนื่องจากไม่มีค่า Standard");
			admin_redirect("income_manager/washer_list/");
		}
		if($checked > 0){
			admin_redirect("income_test/edit_washer_testing/$income_id");
		}
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("income_test/add_washer_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['washer_testing_post_user_id'] = $this->admin_library->user_id();
										
			
			$washer_testing_std=$this->income_test_model->add_washer_testing_std($data);
			
			//$washer_testing_final = $this->income_test_model->add_washer_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test/test_washer/$income_id");
		}
	}

	public function edit_washer_testing($income_id=0)
	{
		$this->admin_model->set_menu_key('833675c0ae6c061f4e8d87c294ebb1ec');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("income_manager/washer_list");
		}
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('test_avg[]','ค่าเฉลี่ยการทดสอบ','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctest_avg[]','ค่าเฉลี่ยการทดสอบ size','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');
		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		
		$product = $this->income_test_model->get_washer_product($this->_data['income_id']);
		
		$this->_data['income_info'] =  $this->income_test_model->get_income_washer($this->_data['income_id']);

		$this->_data['std_list'] = $this->income_test_model->get_product_standard_washer($product['product_id']);
		$this->_data['cer_size_list'] = $this->income_test_model->get_product_standard_size_washer($product['product_id'],$this->_data['income_info']['size_id']);
		
		$this->_data['product'] = $product;
				$checked = $this->income_test_model->check_income_washer_tested($this->_data['income_id']);
		//exit($this->admin_library->user_id());
				
		if($this->form_validation->run()===false){
			$this->admin_library->view("income_test/edit_washer_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['washer_testing_post_user_id'] = $this->admin_library->user_id();
										
			
			$washer_testing_std=$this->income_test_model->update_washer_testing_std($data);
			
			//$washer_testing_final = $this->income_test_model->update_washer_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test/test_washer/$income_id");
		}
	}

	######## Income test spring - Start ########
	public function add_spring_testing($income_id=0)
	{
		$this->admin_model->set_menu_key('833675c0ae6c061f4e8d87c294ebb1ec');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("income_manager/spring_list");
		}
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');
		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		
		$product = $this->income_test_model->get_spring_product($this->_data['income_id']);
		
		$this->_data['income_info'] =  $this->income_test_model->get_income_spring($this->_data['income_id']);

		$this->_data['std_list'] = $this->income_test_model->get_product_standard_spring($product['product_id']);
		$this->_data['cer_size_list'] = $this->income_test_model->get_product_standard_size_spring($product['product_id'],$this->_data['income_info']['size_id']);
		
		$this->_data['product'] = $product;
				$checked = $this->income_test_model->check_income_spring_tested($this->_data['income_id']);
		//exit($this->admin_library->user_id());
		
		if($this->_data['std_list']->num_rows()==0){
			$this->session->set_flashdata("message-warning","ไม่สามารถทดสอบได้ เนื่องจากไม่มีค่า Standard");
			admin_redirect("income_manager/spring_list/");
		}
		if($checked > 0){
			admin_redirect("income_test/edit_spring_testing/$income_id");
		}
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("income_test/add_spring_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['spring_testing_post_user_id'] = $this->admin_library->user_id();
										
			
			$spring_testing_std=$this->income_test_model->add_spring_testing_std($data);
			
			//$spring_testing_final = $this->income_test_model->add_spring_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test/test_spring/$income_id");
		}
	}

	public function edit_spring_testing($income_id=0)
	{
		$this->admin_model->set_menu_key('833675c0ae6c061f4e8d87c294ebb1ec');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("income_manager/spring_list");
		}
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');
		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		
		$product = $this->income_test_model->get_spring_product($this->_data['income_id']);
		
		$this->_data['income_info'] =  $this->income_test_model->get_income_spring($this->_data['income_id']);

		$this->_data['std_list'] = $this->income_test_model->get_product_standard_spring($product['product_id']);
		$this->_data['cer_size_list'] = $this->income_test_model->get_product_standard_size_spring($product['product_id'],$this->_data['income_info']['size_id']);
		
		$this->_data['product'] = $product;
				$checked = $this->income_test_model->check_income_spring_tested($this->_data['income_id']);
		//exit($this->admin_library->user_id());
				
		if($this->form_validation->run()===false){
			$this->admin_library->view("income_test/edit_spring_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['spring_testing_post_user_id'] = $this->admin_library->user_id();
										
			
			$spring_testing_std=$this->income_test_model->update_spring_testing_std($data);
			
			//$spring_testing_final = $this->income_test_model->update_spring_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test/test_spring/$income_id");
		}
	}
	######## Income test spring - End ######## 
	
	######## Income test taper - Start ########
	public function add_taper_testing($income_id=0)
	{
		$this->admin_model->set_menu_key('88a9d3d54bc47e5eb550ab1eb54a43ba');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("income_manager/taper_list");
		}
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');
		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		
		$product = $this->income_test_model->get_taper_product($this->_data['income_id']);
		
		$this->_data['income_info'] =  $this->income_test_model->get_income_taper($this->_data['income_id']);

		$this->_data['std_list'] = $this->income_test_model->get_product_standard_taper($product['product_id']);
		$this->_data['cer_size_list'] = $this->income_test_model->get_product_standard_size_taper($product['product_id'],$this->_data['income_info']['size_id']);
		
		$this->_data['product'] = $product;
				$checked = $this->income_test_model->check_income_taper_tested($this->_data['income_id']);
		//exit($this->admin_library->user_id());
		if($this->_data['std_list']->num_rows()==0){
			$this->session->set_flashdata("message-warning","ไม่สามารถทดสอบได้ เนื่องจากไม่มีค่า Standard");
			admin_redirect("income_manager/taper_list/");
		}
		if($checked > 0){
			admin_redirect("income_test/edit_taper_testing/$income_id");
		}
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("income_test/add_taper_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['taper_testing_post_user_id'] = $this->admin_library->user_id();
										
			
			$taper_testing_std=$this->income_test_model->add_taper_testing_std($data);
			
			//$taper_testing_final = $this->income_test_model->add_taper_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test/test_taper/$income_id");
		}
	}

	public function edit_taper_testing($income_id=0)
	{
		$this->admin_model->set_menu_key('88a9d3d54bc47e5eb550ab1eb54a43ba');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("income_manager/taper_list");
		}
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');
		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		
		$product = $this->income_test_model->get_taper_product($this->_data['income_id']);
		
		$this->_data['income_info'] =  $this->income_test_model->get_income_taper($this->_data['income_id']);

		$this->_data['std_list'] = $this->income_test_model->get_product_standard_taper($product['product_id']);
		$this->_data['cer_size_list'] = $this->income_test_model->get_product_standard_size_taper($product['product_id'],$this->_data['income_info']['size_id']);
		
		$this->_data['product'] = $product;
				$checked = $this->income_test_model->check_income_taper_tested($this->_data['income_id']);
		//exit($this->admin_library->user_id());
				
		if($this->form_validation->run()===false){
			$this->admin_library->view("income_test/edit_taper_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['taper_testing_post_user_id'] = $this->admin_library->user_id();
										
			
			$taper_testing_std=$this->income_test_model->update_taper_testing_std($data);
			
			//$taper_testing_final = $this->income_test_model->update_taper_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test/test_taper/$income_id");
		}
	}
	######## Income test taper - End ########
	
	######## Income test stud - Start ########
	public function add_stud_testing($income_id=0)
	{
		$this->admin_model->set_menu_key('7b21c48a8493e416f7bf0a1e2e17f1da');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("income_manager/stud_list");
		}
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');
		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		
		$product = $this->income_test_model->get_stud_product($this->_data['income_id']);
		
		$this->_data['income_info'] =  $this->income_test_model->get_income_stud($this->_data['income_id']);

		$this->_data['std_list'] = $this->income_test_model->get_product_standard_stud($product['product_id']);
		$this->_data['cer_size_list'] = $this->income_test_model->get_product_standard_size_stud($product['product_id'],$this->_data['income_info']['size_id']);
		
		$this->_data['product'] = $product;
				$checked = $this->income_test_model->check_income_stud_tested($this->_data['income_id']);
		//exit($this->admin_library->user_id());
		if($this->_data['std_list']->num_rows()==0){
			$this->session->set_flashdata("message-warning","ไม่สามารถทดสอบได้ เนื่องจากไม่มีค่า Standard");
			admin_redirect("income_manager/stud_list/");
		}
		if($checked > 0){
			admin_redirect("income_test/edit_stud_testing/$income_id");
		}
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("income_test/add_stud_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['stud_testing_post_user_id'] = $this->admin_library->user_id();
										
			
			$stud_testing_std=$this->income_test_model->add_stud_testing_std($data);
			
			//$stud_testing_final = $this->income_test_model->add_stud_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test/test_stud/$income_id");
		}
	}

	public function edit_stud_testing($income_id=0)
	{
		$this->admin_model->set_menu_key('7b21c48a8493e416f7bf0a1e2e17f1da');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("income_manager/stud_list");
		}
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');
		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		
		$product = $this->income_test_model->get_stud_product($this->_data['income_id']);
		
		$this->_data['income_info'] =  $this->income_test_model->get_income_stud($this->_data['income_id']);

		$this->_data['std_list'] = $this->income_test_model->get_product_standard_stud($product['product_id']);
		$this->_data['cer_size_list'] = $this->income_test_model->get_product_standard_size_stud($product['product_id'],$this->_data['income_info']['size_id']);
		
		$this->_data['product'] = $product;
				$checked = $this->income_test_model->check_income_stud_tested($this->_data['income_id']);
		//exit($this->admin_library->user_id());
				
		if($this->form_validation->run()===false){
			$this->admin_library->view("income_test/edit_stud_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['stud_testing_post_user_id'] = $this->admin_library->user_id();
										
			
			$stud_testing_std=$this->income_test_model->update_stud_testing_std($data);
			
			//$stud_testing_final = $this->income_test_model->update_stud_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test/test_stud/$income_id");
		}
	}
	######## Income test stud - End ########

	
}