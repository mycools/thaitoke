<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Final_test extends CI_Controller {
	var $_data = array();
	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->library('image_lib');
		$this->load->library('admin_library');	
		$this->admin_library->forceLogin();
		$this->load->model('final_test_model');
		$this->load->model('income_test_model');
		$this->load->model('administrator/admin_model');
	}
	public function index()
	{
			
	}
	
	############# bolt #######################
	
		
	public function final_bolt_testing($income_id=0,$final_id=0)
	{
		$this->admin_model->set_menu_key('8a11fc1831fdfa2b49e8203ff134f4ec');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("final_manager/final_bolt_list");
		}
			$checked = $this->final_test_model->check_final_tested_bolt($income_id,$final_id);
			
			if($checked > "0"){
				admin_redirect("final_test/edit_final_bolt_testing/$income_id/$final_id");
				
			}
			
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('test_avg[]','ค่าเฉลี่ยการทดสอบ','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctest_avg[]','ค่าเฉลี่ยการทดสอบ size','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');

		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$product = $this->final_test_model->get_bolt_product($this->_data['income_id'],$final_id);
		$this->_data['final_info'] =  $this->final_test_model->get_final_bolt($this->_data['income_id'],$final_id);
		$this->_data['std_list'] = $this->final_test_model->get_product_standard_bolt($product['product_id']);
		$this->_data['cer_size_list'] = $this->final_test_model->get_product_standard_size_bolt($product['product_id'],$this->_data['final_info']['final_bolt_size_id']);
		$this->_data['product'] = $product;
		//$this->_data['final_info'] =  $this->final_test_model->get_final_bolt($this->_data['income_id']);
		
		//var_dump($this->_data['final_info']);
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("final_test/final_bolt_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['bolt_testing_post_user_id'] = $this->admin_library->user_id();
			$data['final_id'] = $this->_data['final_id'];				
			
			//$bolt_testing_std=$this->final_test_model->add_bolt_testing_cer($data);
			$bolt_testing_final = $this->final_test_model->add_bolt_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test_final/test_bolt/$income_id/$final_id");
		}
	}
	
	
	public function edit_final_bolt_testing($income_id=0,$final_id=0){
		$this->admin_model->set_menu_key('8a11fc1831fdfa2b49e8203ff134f4ec');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("final_manager/final_bolt_list");
		}
			
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('test_avg[]','ค่าเฉลี่ยการทดสอบ','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctest_avg[]','ค่าเฉลี่ยการทดสอบ size','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');

		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$product = $this->final_test_model->get_bolt_product($this->_data['income_id'],$final_id);
		$this->_data['final_info'] =  $this->final_test_model->get_final_bolt($this->_data['income_id'],$final_id);
		$this->_data['std_list'] = $this->final_test_model->get_product_standard_bolt($product['product_id']);
		$this->_data['cer_size_list'] = $this->final_test_model->get_product_standard_size_bolt($product['product_id'],$this->_data['final_info']['final_bolt_size_id']);
		$this->_data['product'] = $product;
		//$this->_data['final_info'] =  $this->final_test_model->get_final_bolt($this->_data['income_id']);
		
		//var_dump($this->_data['final_info']);
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("final_test/edit_final_bolt_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['bolt_testing_post_user_id'] = $this->admin_library->user_id();
			$data['final_id'] = $this->_data['final_id'];				
			
			//$bolt_testing_std=$this->final_test_model->add_bolt_testing_cer($data);
			$bolt_testing_final = $this->final_test_model->update_bolt_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test_final/test_bolt/$income_id/$final_id");
		}

		
		
	}
	
	############ end bolt ##########################################
	
	
	############# nut #######################
	
		
	public function final_nut_testing($income_id=0,$final_id=0)
	{
		$this->admin_model->set_menu_key('8a11fc1831fdfa2b49e8203ff134f4ec');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("final_manager/final_bolt_list");
		}
		if($income_id == "0"){
			admin_redirect("final_manager/final_bolt_list");
		
		}
		
		$checked = $this->final_test_model->check_final_tested_nut($income_id,$final_id);
			if($checked > "0"){
				admin_redirect("final_test/edit_final_nut_testing/$income_id/$final_id");
				
			}
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('test_avg[]','ค่าเฉลี่ยการทดสอบ','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctest_avg[]','ค่าเฉลี่ยการทดสอบ size','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');

		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$product = $this->final_test_model->get_nut_product($this->_data['income_id'],$final_id);
		$this->_data['final_info'] =  $this->final_test_model->get_final_nut($this->_data['income_id'],$final_id);
		$this->_data['std_list'] = $this->final_test_model->get_product_standard_nut($product['product_id']);
		
		$this->_data['cer_size_list'] = $this->final_test_model->get_product_standard_size_nut($product['product_id'],$this->_data['final_info']['final_nut_size_id']);
		
		
		
		$this->_data['product'] = $product;
		//$this->_data['final_info'] =  $this->final_test_model->get_final_nut($this->_data['income_id']);
		
		//var_dump($this->_data['final_info']);
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("final_test/final_nut_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['nut_testing_post_user_id'] = $this->admin_library->user_id();
			$data['final_id'] = $this->_data['final_id'];				
			
			//$nut_testing_std=$this->final_test_model->add_nut_testing_cer($data);
			$nut_testing_final = $this->final_test_model->add_nut_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test_final/test_nut/$income_id/$final_id");
		}
	}
	
	public function edit_final_nut_testing($income_id=0,$final_id=0)
	{
		$this->admin_model->set_menu_key('8a11fc1831fdfa2b49e8203ff134f4ec');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("final_manager/final_bolt_list");
		}
		if($income_id == "0"){
			admin_redirect("final_manager/final_bolt_list");
		
		}
		
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('test_avg[]','ค่าเฉลี่ยการทดสอบ','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctest_avg[]','ค่าเฉลี่ยการทดสอบ size','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');

		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$product = $this->final_test_model->get_nut_product($this->_data['income_id'],$final_id);
		$this->_data['final_info'] =  $this->final_test_model->get_final_nut($this->_data['income_id'],$final_id);
		$this->_data['std_list'] = $this->final_test_model->get_product_standard_nut($product['product_id']);
		
		$this->_data['cer_size_list'] = $this->final_test_model->get_product_standard_size_nut($product['product_id'],$this->_data['final_info']['final_nut_size_id']);
		
		
		
		$this->_data['product'] = $product;
		//$this->_data['final_info'] =  $this->final_test_model->get_final_nut($this->_data['income_id']);
		
		//var_dump($this->_data['final_info']);
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("final_test/edit_final_nut_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['nut_testing_post_user_id'] = $this->admin_library->user_id();
			$data['final_id'] = $this->_data['final_id'];				
			
			//$nut_testing_std=$this->final_test_model->add_nut_testing_cer($data);
			$nut_testing_final = $this->final_test_model->update_nut_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test_final/test_nut/$income_id/$final_id");
		}
	}
	
	
	public function final_main_nut_testing($income_id=0,$final_id=0)
	{
		$this->admin_model->set_menu_key('0167c858306624390a0c7ac5041aadb1');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("final_manager/final_nut_list");
		}
		if($income_id == "0"){
			admin_redirect("final_manager/final_nut_list");
		
		}
		
		$checked = $this->final_test_model->check_final_tested_nut($income_id,$final_id);
			if($checked > "0"){
				admin_redirect("final_test/edit_final_main_nut_testing/$income_id/$final_id");
				
			}
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('test_avg[]','ค่าเฉลี่ยการทดสอบ','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctest_avg[]','ค่าเฉลี่ยการทดสอบ size','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');

		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$product = $this->final_test_model->get_nut_product_main($this->_data['income_id'],$final_id);
		$this->_data['final_info'] =  $this->final_test_model->get_final_nut_main($this->_data['income_id'],$final_id);
		$this->_data['std_list'] = $this->final_test_model->get_product_standard_nut($product['product_id']);
		
		$this->_data['cer_size_list'] = $this->final_test_model->get_product_standard_size_nut($product['product_id'],$this->_data['final_info']['final_nut_size_id']);
		
		
		
		$this->_data['product'] = $product;
		//$this->_data['final_info'] =  $this->final_test_model->get_final_nut($this->_data['income_id']);
		
		//var_dump($this->_data['final_info']);
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("final_test/final_nut_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['nut_testing_post_user_id'] = $this->admin_library->user_id();
			$data['final_id'] = $this->_data['final_id'];				
			
			//$nut_testing_std=$this->final_test_model->add_nut_testing_cer($data);
			$nut_testing_final = $this->final_test_model->add_nut_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test_final/test_main_nut/$income_id/$final_id");
		}
	}
	
	public function edit_final_main_nut_testing($income_id=0,$final_id=0)
	{
		$this->admin_model->set_menu_key('0167c858306624390a0c7ac5041aadb1');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("final_manager/final_bolt_list");
		}
		if($income_id == "0"){
			admin_redirect("final_manager/final_bolt_list");
		
		}
		
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('test_avg[]','ค่าเฉลี่ยการทดสอบ','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctest_avg[]','ค่าเฉลี่ยการทดสอบ size','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');

		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$product = $this->final_test_model->get_nut_product_main($this->_data['income_id'],$final_id);
		$this->_data['final_info'] =  $this->final_test_model->get_final_nut_main($this->_data['income_id'],$final_id);
		$this->_data['std_list'] = $this->final_test_model->get_product_standard_nut($product['product_id']);
		
		$this->_data['cer_size_list'] = $this->final_test_model->get_product_standard_size_nut($product['product_id'],$this->_data['final_info']['final_nut_size_id']);
		
		
		
		$this->_data['product'] = $product;
		//$this->_data['final_info'] =  $this->final_test_model->get_final_nut($this->_data['income_id']);
		
		//var_dump($this->_data['final_info']);
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("final_test/edit_final_nut_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['nut_testing_post_user_id'] = $this->admin_library->user_id();
			$data['final_id'] = $this->_data['final_id'];				
			
			//$nut_testing_std=$this->final_test_model->add_nut_testing_cer($data);
			$nut_testing_final = $this->final_test_model->update_nut_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test_final/test_main_nut/$income_id/$final_id");
		}
	}

	
	############ end nut ##########################################
	
	############# washer #######################
	
		
	public function final_washer_testing($income_id=0,$final_id=0)
	{
		$this->admin_model->set_menu_key('8a11fc1831fdfa2b49e8203ff134f4ec');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("final_manager/final_bolt_list");
		}
		
		if($income_id == "0"){
			admin_redirect("final_manager/final_bolt_list");
		
		}
		
		$checked = $this->final_test_model->check_final_tested_washer($income_id,$final_id);
		
			if($checked > "0"){
				admin_redirect("final_test/edit_final_washer_testing/$income_id/$final_id");
				
			}
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('test_avg[]','ค่าเฉลี่ยการทดสอบ','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctest_avg[]','ค่าเฉลี่ยการทดสอบ size','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');

		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$product = $this->final_test_model->get_washer_product($this->_data['income_id'],$final_id);
		$this->_data['final_info'] =  $this->final_test_model->get_final_washer($this->_data['income_id'],$final_id);
		$this->_data['std_list'] = $this->final_test_model->get_product_standard_washer($product['product_id']);
		$this->_data['cer_size_list'] = $this->final_test_model->get_product_standard_size_washer($product['product_id'],$this->_data['final_info']['final_washer_size_id']);
		
		$this->_data['product'] = $product;
		//$this->_data['final_info'] =  $this->final_test_model->get_final_washer($this->_data['income_id']);
		
		//var_dump($this->_data['final_info']);
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("final_test/final_washer_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['washer_testing_post_user_id'] = $this->admin_library->user_id();
			$data['final_id'] = $this->_data['final_id'];				
			
			//$washer_testing_std=$this->final_test_model->add_washer_testing_cer($data);
			$washer_testing_final = $this->final_test_model->add_washer_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test_final/test_washer/$income_id/$final_id");
		}
	}
	
	public function edit_final_washer_testing($income_id=0,$final_id=0)
	{
		$this->admin_model->set_menu_key('8a11fc1831fdfa2b49e8203ff134f4ec');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("final_manager/final_bolt_list");
		}
		
		if($income_id == "0"){
			admin_redirect("final_manager/final_bolt_list");
		
		}
		
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('test_avg[]','ค่าเฉลี่ยการทดสอบ','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctest_avg[]','ค่าเฉลี่ยการทดสอบ size','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');

		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$product = $this->final_test_model->get_washer_product($this->_data['income_id'],$final_id);
		$this->_data['final_info'] =  $this->final_test_model->get_final_washer($this->_data['income_id'],$final_id);
		$this->_data['std_list'] = $this->final_test_model->get_product_standard_washer($product['product_id']);
		$this->_data['cer_size_list'] = $this->final_test_model->get_product_standard_size_washer($product['product_id'],$this->_data['final_info']['final_washer_size_id']);
		
		$this->_data['product'] = $product;
		//$this->_data['final_info'] =  $this->final_test_model->get_final_washer($this->_data['income_id']);
		
		//var_dump($this->_data['final_info']);
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("final_test/edit_final_washer_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['washer_testing_post_user_id'] = $this->admin_library->user_id();
			$data['final_id'] = $this->_data['final_id'];				
			
			//$washer_testing_std=$this->final_test_model->add_washer_testing_cer($data);
			$washer_testing_final = $this->final_test_model->update_washer_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test_final/test_washer/$income_id/$final_id");
		}
	}


	public function final_main_washer_testing($income_id=0,$final_id=0)
	{
		$this->admin_model->set_menu_key('ce01da7485b1b183e6b32a08d4d64f47');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("final_manager/final_bolt_list");
		}
		
		if($income_id == "0"){
			admin_redirect("final_manager/final_bolt_list");
		
		}
		
		$checked = $this->final_test_model->check_final_tested_washer($income_id,$final_id);
		
			if($checked > "0"){
				admin_redirect("final_test/edit_final_main_washer_testing/$income_id/$final_id");
				
			}
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('test_avg[]','ค่าเฉลี่ยการทดสอบ','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctest_avg[]','ค่าเฉลี่ยการทดสอบ size','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');

		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$product = $this->final_test_model->get_washer_product_main($this->_data['income_id'],$final_id);
		$this->_data['final_info'] =  $this->final_test_model->get_final_washer_main($this->_data['income_id'],$final_id);
		$this->_data['std_list'] = $this->final_test_model->get_product_standard_washer($product['product_id']);
		$this->_data['cer_size_list'] = $this->final_test_model->get_product_standard_size_washer($product['product_id'],$this->_data['final_info']['final_washer_size_id']);
		
		$this->_data['product'] = $product;
		//$this->_data['final_info'] =  $this->final_test_model->get_final_washer($this->_data['income_id']);
		
		//var_dump($this->_data['final_info']);
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("final_test/final_washer_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['washer_testing_post_user_id'] = $this->admin_library->user_id();
			$data['final_id'] = $this->_data['final_id'];				
			
			//$washer_testing_std=$this->final_test_model->add_washer_testing_cer($data);
			$washer_testing_final = $this->final_test_model->add_washer_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test_final/test_main_washer/$income_id/$final_id");
		}
	}
	
	public function edit_final_main_washer_testing($income_id=0,$final_id=0)
	{
		$this->admin_model->set_menu_key('ce01da7485b1b183e6b32a08d4d64f47');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("final_manager/final_bolt_list");
		}
		
		if($income_id == "0"){
			admin_redirect("final_manager/final_bolt_list");
		
		}
		
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('test_avg[]','ค่าเฉลี่ยการทดสอบ','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctest_avg[]','ค่าเฉลี่ยการทดสอบ size','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');

		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$product = $this->final_test_model->get_washer_product_main($this->_data['income_id'],$final_id);
		$this->_data['final_info'] =  $this->final_test_model->get_final_washer_main($this->_data['income_id'],$final_id);
		$this->_data['std_list'] = $this->final_test_model->get_product_standard_washer($product['product_id']);
		$this->_data['cer_size_list'] = $this->final_test_model->get_product_standard_size_washer($product['product_id'],$this->_data['final_info']['final_washer_size_id']);
		
		$this->_data['product'] = $product;
		//$this->_data['final_info'] =  $this->final_test_model->get_final_washer($this->_data['income_id']);
		
		//var_dump($this->_data['final_info']);
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("final_test/edit_final_washer_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['washer_testing_post_user_id'] = $this->admin_library->user_id();
			$data['final_id'] = $this->_data['final_id'];				
			
			//$washer_testing_std=$this->final_test_model->add_washer_testing_cer($data);
			$washer_testing_final = $this->final_test_model->update_washer_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test_final/test_main_washer/$income_id/$final_id");
		}
	}

	
	############ end washer ##########################################
	
	############# spring #######################
	
		
	public function final_spring_testing($income_id=0,$final_id=0)
	{
		$this->admin_model->set_menu_key('8a11fc1831fdfa2b49e8203ff134f4ec');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("final_manager/final_bolt_list");
		}
		
		if($income_id == "0"){
			admin_redirect("final_manager/final_bolt_list");
		
		}
		
		$checked = $this->final_test_model->check_final_tested_spring($income_id,$final_id);
		
			if($checked > "0"){
				admin_redirect("final_test/edit_final_spring_testing/$income_id/$final_id");
				
			}
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');

		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$product = $this->final_test_model->get_spring_product($this->_data['income_id'],$final_id);
		$this->_data['final_info'] =  $this->final_test_model->get_final_spring($this->_data['income_id'],$final_id);
		$this->_data['std_list'] = $this->final_test_model->get_product_standard_spring($product['product_id']);
		$this->_data['cer_size_list'] = $this->final_test_model->get_product_standard_size_spring($product['product_id'],$this->_data['final_info']['final_spring_size_id']);
		
		$this->_data['product'] = $product;
		//$this->_data['final_info'] =  $this->final_test_model->get_final_spring($this->_data['income_id']);
		
		//var_dump($this->_data['final_info']);
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("final_test/final_spring_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['spring_testing_post_user_id'] = $this->admin_library->user_id();
			$data['final_id'] = $this->_data['final_id'];				
			
			//$spring_testing_std=$this->final_test_model->add_spring_testing_cer($data);
			$spring_testing_final = $this->final_test_model->add_spring_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test_final/test_spring/$income_id/$final_id");
		}
	}
	
	public function edit_final_spring_testing($income_id=0,$final_id=0)
	{
		$this->admin_model->set_menu_key('8a11fc1831fdfa2b49e8203ff134f4ec');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("final_manager/final_bolt_list");
		}
		
		if($income_id == "0"){
			admin_redirect("final_manager/final_bolt_list");
		
		}
		
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');

		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$product = $this->final_test_model->get_spring_product($this->_data['income_id'],$final_id);
		$this->_data['final_info'] =  $this->final_test_model->get_final_spring($this->_data['income_id'],$final_id);
		$this->_data['std_list'] = $this->final_test_model->get_product_standard_spring($product['product_id']);
		$this->_data['cer_size_list'] = $this->final_test_model->get_product_standard_size_spring($product['product_id'],$this->_data['final_info']['final_spring_size_id']);
		
		$this->_data['product'] = $product;
		//$this->_data['final_info'] =  $this->final_test_model->get_final_spring($this->_data['income_id']);
		
		//var_dump($this->_data['final_info']);
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("final_test/edit_final_spring_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['spring_testing_post_user_id'] = $this->admin_library->user_id();
			$data['final_id'] = $this->_data['final_id'];				
			
			//$spring_testing_std=$this->final_test_model->add_spring_testing_cer($data);
			$spring_testing_final = $this->final_test_model->update_spring_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test_final/test_spring/$income_id/$final_id");
		}
	}


	public function final_main_spring_testing($income_id=0,$final_id=0)
	{
		$this->admin_model->set_menu_key('cf6778ab14ce1643575cb1951b77a926');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("final_manager/final_bolt_list");
		}
		
		if($income_id == "0"){
			admin_redirect("final_manager/final_bolt_list");
		
		}
		
		$checked = $this->final_test_model->check_final_tested_spring($income_id,$final_id);
		
			if($checked > "0"){
				admin_redirect("final_test/edit_final_main_spring_testing/$income_id/$final_id");
				
			}
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');

		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$product = $this->final_test_model->get_spring_product_main($this->_data['income_id'],$final_id);
		$this->_data['final_info'] =  $this->final_test_model->get_final_spring_main($this->_data['income_id'],$final_id);
		$this->_data['std_list'] = $this->final_test_model->get_product_standard_spring($product['product_id']);
		$this->_data['cer_size_list'] = $this->final_test_model->get_product_standard_size_spring($product['product_id'],$this->_data['final_info']['final_spring_size_id']);
		
		$this->_data['product'] = $product;
		//$this->_data['final_info'] =  $this->final_test_model->get_final_spring($this->_data['income_id']);
		
		//var_dump($this->_data['final_info']);
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("final_test/final_spring_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['spring_testing_post_user_id'] = $this->admin_library->user_id();
			$data['final_id'] = $this->_data['final_id'];				
			
			//$spring_testing_std=$this->final_test_model->add_spring_testing_cer($data);
			$spring_testing_final = $this->final_test_model->add_spring_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test_final/test_main_spring/$income_id/$final_id");
		}
	}
	
	public function edit_final_main_spring_testing($income_id=0,$final_id=0)
	{
		$this->admin_model->set_menu_key('cf6778ab14ce1643575cb1951b77a926');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("final_manager/final_bolt_list");
		}
		
		if($income_id == "0"){
			admin_redirect("final_manager/final_bolt_list");
		
		}
		
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');

		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$product = $this->final_test_model->get_spring_product_main($this->_data['income_id'],$final_id);
		$this->_data['final_info'] =  $this->final_test_model->get_final_spring_main($this->_data['income_id'],$final_id);
		$this->_data['std_list'] = $this->final_test_model->get_product_standard_spring($product['product_id']);
		$this->_data['cer_size_list'] = $this->final_test_model->get_product_standard_size_spring($product['product_id'],$this->_data['final_info']['final_spring_size_id']);
		
		$this->_data['product'] = $product;
		//$this->_data['final_info'] =  $this->final_test_model->get_final_spring($this->_data['income_id']);
		
		//var_dump($this->_data['final_info']);
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("final_test/edit_final_spring_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['spring_testing_post_user_id'] = $this->admin_library->user_id();
			$data['final_id'] = $this->_data['final_id'];				
			
			//$spring_testing_std=$this->final_test_model->add_spring_testing_cer($data);
			$spring_testing_final = $this->final_test_model->update_spring_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test_final/test_main_spring/$income_id/$final_id");
		}
	}

	
	############ end spring ##########################################
	
	############# taper #######################
	
		
	public function final_taper_testing($income_id=0,$final_id=0)
	{
		$this->admin_model->set_menu_key('0f60303cbcadd8fef2c735d46c6159e8');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("final_manager/final_bolt_list");
		}
		
		if($income_id == "0"){
			admin_redirect("final_manager/final_bolt_list");
		
		}
		
		$checked = $this->final_test_model->check_final_tested_taper($income_id,$final_id);
		
			if($checked > "0"){
				admin_redirect("final_test/edit_final_taper_testing/$income_id/$final_id");
				
			}
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');

		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$product = $this->final_test_model->get_taper_product($this->_data['income_id'],$final_id);
		$this->_data['final_info'] =  $this->final_test_model->get_final_taper($this->_data['income_id'],$final_id);
		$this->_data['std_list'] = $this->final_test_model->get_product_standard_taper($product['product_id']);
		$this->_data['cer_size_list'] = $this->final_test_model->get_product_standard_size_taper($product['product_id'],$this->_data['final_info']['final_taper_size_id']);
		
		$this->_data['product'] = $product;
		//$this->_data['final_info'] =  $this->final_test_model->get_final_taper($this->_data['income_id']);
		
		//var_dump($this->_data['final_info']);
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("final_test/final_taper_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['taper_testing_post_user_id'] = $this->admin_library->user_id();
			$data['final_id'] = $this->_data['final_id'];				
			
			//$taper_testing_std=$this->final_test_model->add_taper_testing_cer($data);
			$taper_testing_final = $this->final_test_model->add_taper_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test_final/test_taper/$income_id/$final_id");
		}
	}
	
	public function edit_final_taper_testing($income_id=0,$final_id=0)
	{
		$this->admin_model->set_menu_key('0f60303cbcadd8fef2c735d46c6159e8');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("final_manager/final_bolt_list");
		}
		
		if($income_id == "0"){
			admin_redirect("final_manager/final_bolt_list");
		
		}
		
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');

		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$product = $this->final_test_model->get_taper_product($this->_data['income_id'],$final_id);
		$this->_data['final_info'] =  $this->final_test_model->get_final_taper($this->_data['income_id'],$final_id);
		$this->_data['std_list'] = $this->final_test_model->get_product_standard_taper($product['product_id']);
		$this->_data['cer_size_list'] = $this->final_test_model->get_product_standard_size_taper($product['product_id'],$this->_data['final_info']['final_taper_size_id']);
		
		$this->_data['product'] = $product;
		//$this->_data['final_info'] =  $this->final_test_model->get_final_taper($this->_data['income_id']);
		
		//var_dump($this->_data['final_info']);
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("final_test/edit_final_taper_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['taper_testing_post_user_id'] = $this->admin_library->user_id();
			$data['final_id'] = $this->_data['final_id'];				
			
			//$taper_testing_std=$this->final_test_model->add_taper_testing_cer($data);
			$taper_testing_final = $this->final_test_model->update_taper_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test_final/test_taper/$income_id/$final_id");
		}
	}


	public function final_main_taper_testing($income_id=0,$final_id=0)
	{
		$this->admin_model->set_menu_key('0f60303cbcadd8fef2c735d46c6159e8');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("final_manager/final_bolt_list");
		}
		
		if($income_id == "0"){
			admin_redirect("final_manager/final_bolt_list");
		
		}
		
		$checked = $this->final_test_model->check_final_tested_taper($income_id,$final_id);
		
			if($checked > "0"){
				admin_redirect("final_test/edit_final_main_taper_testing/$income_id/$final_id");
				
			}
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');

		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$product = $this->final_test_model->get_taper_product_main($this->_data['income_id'],$final_id);
		$this->_data['final_info'] =  $this->final_test_model->get_final_taper_main($this->_data['income_id'],$final_id);
		$this->_data['std_list'] = $this->final_test_model->get_product_standard_taper($product['product_id']);
		$this->_data['cer_size_list'] = $this->final_test_model->get_product_standard_size_taper($product['product_id'],$this->_data['final_info']['final_taper_size_id']);
		
		$this->_data['product'] = $product;
		//$this->_data['final_info'] =  $this->final_test_model->get_final_taper($this->_data['income_id']);
		
		//var_dump($this->_data['final_info']);
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("final_test/final_taper_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['taper_testing_post_user_id'] = $this->admin_library->user_id();
			$data['final_id'] = $this->_data['final_id'];				
			
			//$taper_testing_std=$this->final_test_model->add_taper_testing_cer($data);
			$taper_testing_final = $this->final_test_model->add_taper_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test_final/test_main_taper/$income_id/$final_id");
		}
	}
	
	public function edit_final_main_taper_testing($income_id=0,$final_id=0)
	{
		$this->admin_model->set_menu_key('0f60303cbcadd8fef2c735d46c6159e8');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("final_manager/final_bolt_list");
		}
		
		if($income_id == "0"){
			admin_redirect("final_manager/final_bolt_list");
		
		}
		
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');

		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$product = $this->final_test_model->get_taper_product_main($this->_data['income_id'],$final_id);
		$this->_data['final_info'] =  $this->final_test_model->get_final_taper_main($this->_data['income_id'],$final_id);
		$this->_data['std_list'] = $this->final_test_model->get_product_standard_taper($product['product_id']);
		$this->_data['cer_size_list'] = $this->final_test_model->get_product_standard_size_taper($product['product_id'],$this->_data['final_info']['final_taper_size_id']);
		
		$this->_data['product'] = $product;
		//$this->_data['final_info'] =  $this->final_test_model->get_final_taper($this->_data['income_id']);
		
		//var_dump($this->_data['final_info']);
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("final_test/edit_final_taper_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['taper_testing_post_user_id'] = $this->admin_library->user_id();
			$data['final_id'] = $this->_data['final_id'];				
			
			//$taper_testing_std=$this->final_test_model->add_taper_testing_cer($data);
			$taper_testing_final = $this->final_test_model->update_taper_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test_final/test_main_taper/$income_id/$final_id");
		}
	}

	
	############ end taper ##########################################
	
	############# stud #######################
	
		
	public function final_stud_testing($income_id=0,$final_id=0)
	{
		$this->admin_model->set_menu_key('d18409cc6d89636957588a0b5db34553');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("final_manager/final_bolt_list");
		}
		
		if($income_id == "0"){
			admin_redirect("final_manager/final_bolt_list");
		
		}
		
		$checked = $this->final_test_model->check_final_tested_stud($income_id,$final_id);
		
			if($checked > "0"){
				admin_redirect("final_test/edit_final_stud_testing/$income_id/$final_id");
				
			}
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');

		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$product = $this->final_test_model->get_stud_product($this->_data['income_id'],$final_id);
		$this->_data['final_info'] =  $this->final_test_model->get_final_stud($this->_data['income_id'],$final_id);
		$this->_data['std_list'] = $this->final_test_model->get_product_standard_stud($product['product_id']);
		$this->_data['cer_size_list'] = $this->final_test_model->get_product_standard_size_stud($product['product_id'],$this->_data['final_info']['final_stud_size_id']);
		
		$this->_data['product'] = $product;
		//$this->_data['final_info'] =  $this->final_test_model->get_final_stud($this->_data['income_id']);
		
		//var_dump($this->_data['final_info']);
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("final_test/final_stud_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['stud_testing_post_user_id'] = $this->admin_library->user_id();
			$data['final_id'] = $this->_data['final_id'];				
			
			//$stud_testing_std=$this->final_test_model->add_stud_testing_cer($data);
			$stud_testing_final = $this->final_test_model->add_stud_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test_final/test_stud/$income_id/$final_id");
		}
	}
	
	public function edit_final_stud_testing($income_id=0,$final_id=0)
	{
		$this->admin_model->set_menu_key('d18409cc6d89636957588a0b5db34553');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("final_manager/final_bolt_list");
		}
		
		if($income_id == "0"){
			admin_redirect("final_manager/final_bolt_list");
		
		}
		
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');

		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$product = $this->final_test_model->get_stud_product($this->_data['income_id'],$final_id);
		$this->_data['final_info'] =  $this->final_test_model->get_final_stud($this->_data['income_id'],$final_id);
		$this->_data['std_list'] = $this->final_test_model->get_product_standard_stud($product['product_id']);
		$this->_data['cer_size_list'] = $this->final_test_model->get_product_standard_size_stud($product['product_id'],$this->_data['final_info']['final_stud_size_id']);
		
		$this->_data['product'] = $product;
		//$this->_data['final_info'] =  $this->final_test_model->get_final_stud($this->_data['income_id']);
		
		//var_dump($this->_data['final_info']);
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("final_test/edit_final_stud_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['stud_testing_post_user_id'] = $this->admin_library->user_id();
			$data['final_id'] = $this->_data['final_id'];				
			
			//$stud_testing_std=$this->final_test_model->add_stud_testing_cer($data);
			$stud_testing_final = $this->final_test_model->update_stud_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test_final/test_stud/$income_id/$final_id");
		}
	}


	public function final_main_stud_testing($income_id=0,$final_id=0)
	{
		$this->admin_model->set_menu_key('d18409cc6d89636957588a0b5db34553');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("final_manager/final_bolt_list");
		}
		
		if($income_id == "0"){
			admin_redirect("final_manager/final_bolt_list");
		
		}
		
		$checked = $this->final_test_model->check_final_tested_stud($income_id,$final_id);
		
			if($checked > "0"){
				admin_redirect("final_test/edit_final_main_stud_testing/$income_id/$final_id");
				
			}
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');

		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$product = $this->final_test_model->get_stud_product_main($this->_data['income_id'],$final_id);
		$this->_data['final_info'] =  $this->final_test_model->get_final_stud_main($this->_data['income_id'],$final_id);
		$this->_data['std_list'] = $this->final_test_model->get_product_standard_stud($product['product_id']);
		$this->_data['cer_size_list'] = $this->final_test_model->get_product_standard_size_stud($product['product_id'],$this->_data['final_info']['final_stud_size_id']);
		
		$this->_data['product'] = $product;
		//$this->_data['final_info'] =  $this->final_test_model->get_final_stud($this->_data['income_id']);
		
		//var_dump($this->_data['final_info']);
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("final_test/final_stud_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['stud_testing_post_user_id'] = $this->admin_library->user_id();
			$data['final_id'] = $this->_data['final_id'];				
			
			//$stud_testing_std=$this->final_test_model->add_stud_testing_cer($data);
			$stud_testing_final = $this->final_test_model->add_stud_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test_final/test_main_stud/$income_id/$final_id");
		}
	}
	
	public function edit_final_main_stud_testing($income_id=0,$final_id=0)
	{
		$this->admin_model->set_menu_key('d18409cc6d89636957588a0b5db34553');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("final_manager/final_bolt_list");
		}
		
		if($income_id == "0"){
			admin_redirect("final_manager/final_bolt_list");
		
		}
		
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size ','trim');

		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		$this->_data['final_id'] = $final_id;
		$product = $this->final_test_model->get_stud_product_main($this->_data['income_id'],$final_id);
		$this->_data['final_info'] =  $this->final_test_model->get_final_stud_main($this->_data['income_id'],$final_id);
		$this->_data['std_list'] = $this->final_test_model->get_product_standard_stud($product['product_id']);
		$this->_data['cer_size_list'] = $this->final_test_model->get_product_standard_size_stud($product['product_id'],$this->_data['final_info']['final_stud_size_id']);
		
		$this->_data['product'] = $product;
		//$this->_data['final_info'] =  $this->final_test_model->get_final_stud($this->_data['income_id']);
		
		//var_dump($this->_data['final_info']);
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("final_test/edit_final_stud_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['stud_testing_post_user_id'] = $this->admin_library->user_id();
			$data['final_id'] = $this->_data['final_id'];				
			
			//$stud_testing_std=$this->final_test_model->add_stud_testing_cer($data);
			$stud_testing_final = $this->final_test_model->update_stud_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test_final/test_main_stud/$income_id/$final_id");
		}
	}

	
	############ end stud ##########################################
		
	
}