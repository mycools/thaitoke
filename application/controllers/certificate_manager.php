<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class certificate_manager extends CI_Controller {
	var $_data = array();
	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->library('image_lib');
		$this->load->library('admin_library');
		$this->load->model('administrator/bolt_stock');
		$this->load->model('administrator/nut_stock');
		$this->load->model('administrator/washer_stock');
		$this->load->model('administrator/spring_stock');
		$this->load->model('administrator/taper_stock');
		$this->load->model('administrator/stud_stock');
		$this->admin_library->forceLogin();
		//$this->load->model('certificate_manager_model');
		$this->load->model('administrator/admin_model');
	}
	public function index()
	{
		admin_redirect("certificate_manager/certificate_bolt_list");
	}
	
	############# bolt #######################
	public function certificate_bolt_list($page=1)
	{
		$QUERY_STRING = @$_SERVER['QUERY_STRING'];
		$QUERY_STRING = (trim($QUERY_STRING))?"?".$QUERY_STRING:"";
		$this->admin_model->set_menu_key('c17761a8879743cfb9a2219e2d7b0199');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$limit=25;
		$offset = ($page-1)*$limit;
		$offset = ($offset <0)?0:$offset;
		
		$this->admin_library->setTitle('Bolt Certificate','icofont icofont-certificate-alt-1');
		
		$this->admin_library->setDetail("Manage Bolt Certificate & Print-out Certificate Document");
		$this->admin_model->initd($this);
		$this->admin_model->set_title('Bolt Certificate','icofont icofont-certificate-alt-1');
		$this->load->model('administrator/bolt_certificate');
		$this->admin_model->set_datatable($this->bolt_certificate->dataTable($limit,$offset));
		
		$this->admin_model->set_column("final_id","No.",0,"fa fa-sort-alpha-asc");
		$this->admin_model->set_column("final_invoice_no","Invoice ID.",0,"fa fa-folder-open");
		$this->admin_model->set_column("final_bolt_id","Product",0,"fa fa-shopping-cart");
		$this->admin_model->set_column("final_bolt_size_id","Size",0,"fa fa-shopping-cart");
		$this->admin_model->set_column("customer_name","Customer",0,"fa fa-shopping-cart");
		
		
		//$this->admin_model->set_column("final_createdtime","วันที่บันทึก",110,"fa fa-calendar");
		$this->admin_model->set_column("final_manager","ผู้บันทึก",110,"fa fa-user");
		$this->admin_model->set_column("test_bolt_status","การทดสอบ",0,'fa fa-check-square-o');
		
		$this->admin_model->set_column_callback("final_bolt_id","product_name_callback");
		//$this->admin_model->set_column_callback("final_customer_id","customer_name_callback");
		$this->admin_model->set_column_callback("final_manager","lot_manager_callback");
		$this->admin_model->set_column_callback("final_createdtime","sdateth");
		$this->admin_model->set_column_callback("test_bolt_status","manager_status_callback");
		$this->admin_model->set_column_callback("final_bolt_size_id","product_size_callback");

		
		//$this->admin_model->set_top_button("เพิ่มข้อมูล","certificate_manager/bolt_add","fa fa-plus","btn-info","r");
		
		$this->admin_model->set_action_button("ตั้งค่าใบ Certificate","certificate_manager/bolt_edit/[final_id]","fa fa-cog","btn-success btn-xs","r");
		$this->admin_model->set_action_button("พิมพ์","certificate_manager/pdf_bolt/[final_id]/[final_bolt_income_id]/print-[final_id]-[final_bolt_income_id].pdf","fa fa-print","btn-info btn-xs _blank","w");
		$this->admin_model->set_action_button_callback("พิมพ์","print_bolt_callback");
		$this->admin_model->set_pagination("certificate_manager/certificate_bolt_list",$this->bolt_certificate->export_data()->num_rows(),$limit,3);
		$this->admin_model->show_search_text();
		$search_q = trim(strip_tags($this->input->get("q")));
		if($search_q != ""){
			$this->admin_library->add_breadcrumb("ค้นหาคำว่า '{$search_q}'","certificate_manager/certificate_certificate_bolt_list{$QUERY_STRING}","fa fa-search");
		}
		$this->admin_model->make_list();
		$this->admin_library->output();		
		
	}
	
	
	public function bolt_edit($certificate_id=0)
	{
		$this->admin_model->set_menu_key('1539102c49bebd42baa8a47ecbe1b25f');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->load->model('administrator/bolt_certificate');
		$this->form_validation->set_rules("final_text_remark","Remart","trim");
		$this->form_validation->set_rules("final_inspection_no","Inspection No.","trim");
		//$this->form_validation->set_rules("product_id","Product ID ","trim|required");
		if($this->form_validation->run()===false)
		{
			$this->_data['row'] = $this->bolt_certificate->getcertificate($certificate_id);
			if(!$this->_data['row']){
				$this->session->set_flashdata("message-error","ไม่พบข้อมูลที่ต้องการแก้ไข.");
				admin_redirect("certificate_manager/certificate_bolt_list");
			}
			$this->admin_library->setTitle('Bolt certificate','fa fa-folder-open');
			$this->admin_library->setDetail("Bolt certificate manager");
			$this->admin_library->add_breadcrumb("Bolt certificate","certificate_manager/certificate_bolt_list","fa fa-folder-open");
			$this->admin_library->add_breadcrumb("Edit certificate","certificate_manager/bolt_edit/".$certificate_id,"fa fa-edit");
			
			$this->admin_library->view("certificate_manager/bolt_edit");
			$this->admin_library->output();
		}else{
			$this->bolt_certificate->edit_certificate();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("certificate_manager/certificate_bolt_list");
		}
	}
	
	############ end bolt ##########################################
	
	############ start nut ###########################################
		public function certificate_nut_list($page=1)
	{
		$QUERY_STRING = @$_SERVER['QUERY_STRING'];
		$QUERY_STRING = (trim($QUERY_STRING))?"?".$QUERY_STRING:"";
		$this->admin_model->set_menu_key('a74a3a8a887aef50077b8d0bfdc094f2');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$limit=25;
		$offset = ($page-1)*$limit;
		$offset = ($offset <0)?0:$offset;
		
		$this->admin_library->setTitle('Nut certificate','fa fa-folder-open');
		
		$this->admin_library->setDetail("Nut certificate manager");
		$this->admin_model->initd($this);
		$this->admin_model->set_title('Nut certificate manager','fa fa-book');
		$this->load->model('administrator/nut_certificate');
		$this->admin_model->set_datatable($this->nut_certificate->dataTable($limit,$offset));
		
		$this->admin_model->set_column("final_id","No.",0,"fa fa-sort-alpha-asc");
		$this->admin_model->set_column("final_invoice_no","Invoice ID.",0,"fa fa-folder-open");
		$this->admin_model->set_column("final_nut_id","Product",0,"fa fa-shopping-cart");
		$this->admin_model->set_column("final_nut_size_id","Size",0,"fa fa-shopping-cart");
		$this->admin_model->set_column("final_customer_id","Customer",0,"fa fa-shopping-cart");
		
		$this->admin_model->set_column("final_createdtime","Create Date",0,"fa fa-calendar");
		$this->admin_model->set_column("final_manager","ผู้จัดการข้อมูล",0,"fa fa-user");
		$this->admin_model->set_column("test_status","สถานะการทดสอบ",0,'fa fa-code');
		
		$this->admin_model->set_column_callback("final_nut_id","product_nut_name_callback");
		$this->admin_model->set_column_callback("final_customer_id","customer_nut_name_callback");
		$this->admin_model->set_column_callback("final_manager","lot_manager_callback");
		$this->admin_model->set_column_callback("final_createdtime","date_callback");
		$this->admin_model->set_column_callback("test_status","manager_status_callback");
		$this->admin_model->set_column_callback("final_nut_size_id","product_size_nut_callback");

		
		//$this->admin_model->set_top_button("เพิ่มข้อมูล","certificate_manager/nut_add","fa fa-plus","btn-info","r");
		
		$this->admin_model->set_action_button("ตั้งค่าใบ Certificate","certificate_manager/nut_edit/[final_id]","fa fa-eye","btn-success","r");
		$this->admin_model->set_action_button("พิมพ์","certificate_manager/print_nut/[final_id]/[final_nut_income_id]","fa fa-edit","btn-info","w");
		$this->admin_model->set_pagination("certificate_manager/certificate_nut_list",$this->nut_certificate->export_data()->num_rows(),$limit,3);
		$this->admin_model->show_search_text();
		$search_q = trim(strip_tags($this->input->get("q")));
		if($search_q != ""){
			$this->admin_library->add_breadcrumb("ค้นหาคำว่า '{$search_q}'","certificate_manager/certificate_certificate_nut_list{$QUERY_STRING}","fa fa-search");
		}
		$this->admin_model->make_list();
		$this->admin_library->output();		
		
	}
	
	
	public function nut_edit($certificate_id=0)
	{
		$this->admin_model->set_menu_key('a74a3a8a887aef50077b8d0bfdc094f2');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->load->model('administrator/nut_certificate');
		$this->form_validation->set_rules("final_text_remark","Remart","trim|required");
		//$this->form_validation->set_rules("product_id","Product ID ","trim|required");
		if($this->form_validation->run()===false)
		{
			$this->_data['row'] = $this->nut_certificate->getcertificate($certificate_id);
			if(!$this->_data['row']){
				$this->session->set_flashdata("message-error","ไม่พบข้อมูลที่ต้องการแก้ไข.");
				admin_redirect("certificate_manager/certificate_nut_list");
			}
			$this->admin_library->setTitle('nut certificate','fa fa-folder-open');
			$this->admin_library->setDetail("nut certificate manager");
			$this->admin_library->add_breadcrumb("nut certificate","certificate_manager/certificate_nut_list","fa fa-folder-open");
			$this->admin_library->add_breadcrumb("Edit certificate","certificate_manager/nut_edit/".$certificate_id,"fa fa-edit");
			
			$this->admin_library->view("certificate_manager/nut_edit");
			$this->admin_library->output();
		}else{
			$this->nut_certificate->edit_certificate();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("certificate_manager/certificate_nut_list");
		}
	}
	
	############ end nut ###############################################
	
	
	############ start washer ###########################################
		public function certificate_washer_list($page=1)
	{
		$QUERY_STRING = @$_SERVER['QUERY_STRING'];
		$QUERY_STRING = (trim($QUERY_STRING))?"?".$QUERY_STRING:"";
		$this->admin_model->set_menu_key('1d409d4056af5c6f68c9d6075d4fee4a');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$limit=25;
		$offset = ($page-1)*$limit;
		$offset = ($offset <0)?0:$offset;
		
		$this->admin_library->setTitle('washer certificate','fa fa-certificate');
		
		$this->admin_library->setDetail("washer certificate manager");
		$this->admin_model->initd($this);
		$this->admin_model->set_title('washer certificate manager','fa fa-book');
		$this->load->model('administrator/washer_certificate');
		$this->admin_model->set_datatable($this->washer_certificate->dataTable($limit,$offset));
		
		$this->admin_model->set_column("final_id","No.",0,"fa fa-sort-alpha-asc");
		$this->admin_model->set_column("final_invoice_no","Invoice ID.",0,"fa fa-folder-open");
		$this->admin_model->set_column("final_washer_id","Product",0,"fa fa-shopping-cart");
		$this->admin_model->set_column("final_washer_size_id","Size",0,"fa fa-shopping-cart");
		$this->admin_model->set_column("final_customer_id","Customer",0,"fa fa-shopping-cart");
		
		$this->admin_model->set_column("final_createdtime","Create Date",0,"fa fa-calendar");
		$this->admin_model->set_column("final_manager","ผู้จัดการข้อมูล",0,"fa fa-user");
		$this->admin_model->set_column("test_status","สถานะการทดสอบ",0,'fa fa-code');
		
		$this->admin_model->set_column_callback("final_washer_id","product_washer_name_callback");
		$this->admin_model->set_column_callback("final_customer_id","customer_washer_name_callback");
		$this->admin_model->set_column_callback("final_manager","lot_manager_callback");
		$this->admin_model->set_column_callback("final_createdtime","date_callback");
		$this->admin_model->set_column_callback("test_status","manager_status_callback");
		$this->admin_model->set_column_callback("final_washer_size_id","product_size_washer_callback");

		
		//$this->admin_model->set_top_button("เพิ่มข้อมูล","certificate_manager/washer_add","fa fa-plus","btn-info","r");
		
		$this->admin_model->set_action_button("ตั้งค่าใบ Certificate","certificate_manager/washer_edit/[final_id]","fa fa-eye","btn-success","r");
		$this->admin_model->set_action_button("พิมพ์","certificate_manager/print_washer/[final_id]/[final_washer_income_id]","fa fa-edit","btn-info","w");
		$this->admin_model->set_pagination("certificate_manager/certificate_washer_list",$this->washer_certificate->export_data()->num_rows(),$limit,3);
		$this->admin_model->show_search_text();
		$search_q = trim(strip_tags($this->input->get("q")));
		if($search_q != ""){
			$this->admin_library->add_breadcrumb("ค้นหาคำว่า '{$search_q}'","certificate_manager/certificate_certificate_washer_list{$QUERY_STRING}","fa fa-search");
		}
		$this->admin_model->make_list();
		$this->admin_library->output();		
		
	}
	
	
	public function washer_edit($certificate_id=0)
	{
		$this->admin_model->set_menu_key('1d409d4056af5c6f68c9d6075d4fee4a');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->load->model('administrator/washer_certificate');
		$this->form_validation->set_rules("final_text_remark","Remart","trim|required");
		//$this->form_validation->set_rules("product_id","Product ID ","trim|required");
		if($this->form_validation->run()===false)
		{
			$this->_data['row'] = $this->washer_certificate->getcertificate($certificate_id);
			if(!$this->_data['row']){
				$this->session->set_flashdata("message-error","ไม่พบข้อมูลที่ต้องการแก้ไข.");
				admin_redirect("certificate_manager/certificate_washer_list");
			}
			$this->admin_library->setTitle('washer certificate','fa fa-folder-open');
			$this->admin_library->setDetail("washer certificate manager");
			$this->admin_library->add_breadcrumb("washer certificate","certificate_manager/certificate_washer_list","fa fa-folder-open");
			$this->admin_library->add_breadcrumb("Edit certificate","certificate_manager/washer_edit/".$certificate_id,"fa fa-edit");
			
			$this->admin_library->view("certificate_manager/washer_edit");
			$this->admin_library->output();
		}else{
			$this->washer_certificate->edit_certificate();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("certificate_manager/certificate_washer_list");
		}
	}
	
	############ end washer ###############################################	
	
		############ start spring ###########################################
		public function certificate_spring_list($page=1)
	{
		$QUERY_STRING = @$_SERVER['QUERY_STRING'];
		$QUERY_STRING = (trim($QUERY_STRING))?"?".$QUERY_STRING:"";
		$this->admin_model->set_menu_key('6691ff25fe52dd86d4c7e088c598917d');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$limit=25;
		$offset = ($page-1)*$limit;
		$offset = ($offset <0)?0:$offset;
		
		$this->admin_library->setTitle('spring certificate','fa fa-folder-open');
		
		$this->admin_library->setDetail("spring certificate manager");
		$this->admin_model->initd($this);
		$this->admin_model->set_title('spring certificate manager','fa fa-book');
		$this->load->model('administrator/spring_certificate');
		$this->admin_model->set_datatable($this->spring_certificate->dataTable($limit,$offset));
		
		$this->admin_model->set_column("final_id","No.",0,"fa fa-sort-alpha-asc");
		$this->admin_model->set_column("final_invoice_no","Invoice ID.",0,"fa fa-folder-open");
		$this->admin_model->set_column("final_spring_id","Product",0,"fa fa-shopping-cart");
		$this->admin_model->set_column("final_spring_size_id","Size",0,"fa fa-shopping-cart");
		$this->admin_model->set_column("final_customer_id","Customer",0,"fa fa-shopping-cart"/* ,'text-overflow' */);
		
		$this->admin_model->set_column("final_createdtime","Create Date",0,"fa fa-calendar");
		$this->admin_model->set_column("final_manager","ผู้จัดการข้อมูล",0,"fa fa-user");
		$this->admin_model->set_column("test_status","สถานะการทดสอบ",0,'fa fa-code');
		
		$this->admin_model->set_column_callback("final_spring_id","product_spring_name_callback");
		$this->admin_model->set_column_callback("final_customer_id","customer_spring_name_callback");
		$this->admin_model->set_column_callback("final_manager","lot_manager_callback");
		$this->admin_model->set_column_callback("final_createdtime","date_callback");
		$this->admin_model->set_column_callback("test_status","manager_status_callback");
		$this->admin_model->set_column_callback("final_spring_size_id","product_size_spring_callback");

		
		//$this->admin_model->set_top_button("เพิ่มข้อมูล","certificate_manager/spring_add","fa fa-plus","btn-info","r");
		
		$this->admin_model->set_action_button("ตั้งค่าใบ Certificate","certificate_manager/spring_edit/[final_id]","fa fa-eye","btn-success","r");
		$this->admin_model->set_action_button("พิมพ์","certificate_manager/print_spring/[final_id]/[final_spring_income_id]","fa fa-edit","btn-info","w");
		$this->admin_model->set_pagination("certificate_manager/certificate_spring_list",$this->spring_certificate->export_data()->num_rows(),$limit,3);
		$this->admin_model->show_search_text();
		$search_q = trim(strip_tags($this->input->get("q")));
		if($search_q != ""){
			$this->admin_library->add_breadcrumb("ค้นหาคำว่า '{$search_q}'","certificate_manager/certificate_certificate_spring_list{$QUERY_STRING}","fa fa-search");
		}
		$this->admin_model->make_list();
		$this->admin_library->output();		
		
	}
	
	
	public function spring_edit($certificate_id=0)
	{
		$this->admin_model->set_menu_key('6691ff25fe52dd86d4c7e088c598917d');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->load->model('administrator/spring_certificate');
		$this->form_validation->set_rules("final_text_remark","Remart","trim|required");
		//$this->form_validation->set_rules("product_id","Product ID ","trim|required");
		if($this->form_validation->run()===false)
		{
			$this->_data['row'] = $this->spring_certificate->getcertificate($certificate_id);
			if(!$this->_data['row']){
				$this->session->set_flashdata("message-error","ไม่พบข้อมูลที่ต้องการแก้ไข.");
				admin_redirect("certificate_manager/certificate_spring_list");
			}
			$this->admin_library->setTitle('spring certificate','fa fa-folder-open');
			$this->admin_library->setDetail("spring certificate manager");
			$this->admin_library->add_breadcrumb("spring certificate","certificate_manager/certificate_spring_list","fa fa-folder-open");
			$this->admin_library->add_breadcrumb("Edit certificate","certificate_manager/spring_edit/".$certificate_id,"fa fa-edit");
			
			$this->admin_library->view("certificate_manager/spring_edit");
			$this->admin_library->output();
		}else{
			$this->spring_certificate->edit_certificate();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("certificate_manager/certificate_spring_list");
		}
	}
	
	############ end spring ###############################################	
    ############ start taper ###########################################
		public function certificate_taper_list($page=1)
	{
		$QUERY_STRING = @$_SERVER['QUERY_STRING'];
		$QUERY_STRING = (trim($QUERY_STRING))?"?".$QUERY_STRING:"";
		$this->admin_model->set_menu_key('974937235821743204471dd204d0e6a7');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$limit=25;
		$offset = ($page-1)*$limit;
		$offset = ($offset <0)?0:$offset;
		
		$this->admin_library->setTitle('taper certificate','fa fa-folder-open');
		
		$this->admin_library->setDetail("taper certificate manager");
		$this->admin_model->initd($this);
		$this->admin_model->set_title('taper certificate manager','fa fa-book');
		$this->load->model('administrator/taper_certificate');
		$this->admin_model->set_datatable($this->taper_certificate->dataTable($limit,$offset));
		
		$this->admin_model->set_column("final_id","No.",0,"fa fa-sort-alpha-asc");
		$this->admin_model->set_column("final_invoice_no","Invoice ID.",0,"fa fa-folder-open");
		$this->admin_model->set_column("final_taper_id","Product",0,"fa fa-shopping-cart");
		$this->admin_model->set_column("final_taper_size_id","Size",0,"fa fa-shopping-cart");
		$this->admin_model->set_column("final_customer_id","Customer",0,"fa fa-shopping-cart");
		
		$this->admin_model->set_column("final_createdtime","Create Date",0,"fa fa-calendar");
		$this->admin_model->set_column("final_manager","ผู้จัดการข้อมูล",0,"fa fa-user");
		$this->admin_model->set_column("test_status","สถานะการทดสอบ",0,'fa fa-code');
		
		$this->admin_model->set_column_callback("final_taper_id","product_taper_name_callback");
		$this->admin_model->set_column_callback("final_customer_id","customer_taper_name_callback");
		$this->admin_model->set_column_callback("final_manager","lot_manager_callback");
		$this->admin_model->set_column_callback("final_createdtime","date_callback");
		$this->admin_model->set_column_callback("test_status","manager_status_callback");
		$this->admin_model->set_column_callback("final_taper_size_id","product_size_taper_callback");

		
		//$this->admin_model->set_top_button("เพิ่มข้อมูล","certificate_manager/taper_add","fa fa-plus","btn-info","r");
		
		$this->admin_model->set_action_button("ตั้งค่าใบ Certificate","certificate_manager/taper_edit/[final_id]","fa fa-eye","btn-success","r");
		$this->admin_model->set_action_button("พิมพ์","certificate_manager/print_taper/[final_id]/[final_taper_income_id]","fa fa-edit","btn-info","w");
		$this->admin_model->set_pagination("certificate_manager/certificate_taper_list",$this->taper_certificate->export_data()->num_rows(),$limit,3);
		$this->admin_model->show_search_text();
		$search_q = trim(strip_tags($this->input->get("q")));
		if($search_q != ""){
			$this->admin_library->add_breadcrumb("ค้นหาคำว่า '{$search_q}'","certificate_manager/certificate_certificate_taper_list{$QUERY_STRING}","fa fa-search");
		}
		$this->admin_model->make_list();
		$this->admin_library->output();		
		
	}
	
	
	public function taper_edit($certificate_id=0)
	{
		$this->admin_model->set_menu_key('974937235821743204471dd204d0e6a7');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->load->model('administrator/taper_certificate');
		$this->form_validation->set_rules("final_text_remark","Remart","trim|required");
		//$this->form_validation->set_rules("product_id","Product ID ","trim|required");
		if($this->form_validation->run()===false)
		{
			$this->_data['row'] = $this->taper_certificate->getcertificate($certificate_id);
			if(!$this->_data['row']){
				$this->session->set_flashdata("message-error","ไม่พบข้อมูลที่ต้องการแก้ไข.");
				admin_redirect("certificate_manager/certificate_taper_list");
			}
			$this->admin_library->setTitle('taper certificate','fa fa-folder-open');
			$this->admin_library->setDetail("taper certificate manager");
			$this->admin_library->add_breadcrumb("taper certificate","certificate_manager/certificate_taper_list","fa fa-folder-open");
			$this->admin_library->add_breadcrumb("Edit certificate","certificate_manager/taper_edit/".$certificate_id,"fa fa-edit");
			
			$this->admin_library->view("certificate_manager/taper_edit");
			$this->admin_library->output();
		}else{
			$this->taper_certificate->edit_certificate();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("certificate_manager/certificate_taper_list");
		}
	}
	
	############ end taper ###############################################		
	
	############ start stud ###########################################
		public function certificate_stud_list($page=1)
	{
		$QUERY_STRING = @$_SERVER['QUERY_STRING'];
		$QUERY_STRING = (trim($QUERY_STRING))?"?".$QUERY_STRING:"";
		$this->admin_model->set_menu_key('fe8009fb4a0cd1b823acd5b3bdee261d');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$limit=25;
		$offset = ($page-1)*$limit;
		$offset = ($offset <0)?0:$offset;
		
		$this->admin_library->setTitle('stud certificate','fa fa-folder-open');
		
		$this->admin_library->setDetail("stud certificate manager");
		$this->admin_model->initd($this);
		$this->admin_model->set_title('stud certificate manager','fa fa-book');
		$this->load->model('administrator/stud_certificate');
		$this->admin_model->set_datatable($this->stud_certificate->dataTable($limit,$offset));
		
		$this->admin_model->set_column("final_id","No.",0,"fa fa-sort-alpha-asc");
		$this->admin_model->set_column("final_invoice_no","Invoice ID.",0,"fa fa-folder-open");
		$this->admin_model->set_column("final_stud_id","Product",0,"fa fa-shopping-cart");
		$this->admin_model->set_column("final_stud_size_id","Size",0,"fa fa-shopping-cart");
		$this->admin_model->set_column("final_customer_id","Customer",0,"fa fa-shopping-cart");
		
		$this->admin_model->set_column("final_createdtime","Create Date",0,"fa fa-calendar");
		$this->admin_model->set_column("final_manager","ผู้จัดการข้อมูล",0,"fa fa-user"); 
		$this->admin_model->set_column("test_status","สถานะการทดสอบ",0,'fa fa-code');
		
		$this->admin_model->set_column_callback("final_stud_id","product_stud_name_callback");
		$this->admin_model->set_column_callback("final_customer_id","customer_stud_name_callback");
		$this->admin_model->set_column_callback("final_manager","lot_manager_callback");
		$this->admin_model->set_column_callback("final_createdtime","date_callback");
		$this->admin_model->set_column_callback("test_status","manager_status_callback");
		$this->admin_model->set_column_callback("final_stud_size_id","product_size_stud_callback"); 

		
		//$this->admin_model->set_top_button("เพิ่มข้อมูล","certificate_manager/stud_add","fa fa-plus","btn-info","r");
		
		$this->admin_model->set_action_button("ตั้งค่าใบ Certificate","certificate_manager/stud_edit/[final_id]","fa fa-eye","btn-success","r");
		$this->admin_model->set_action_button("พิมพ์","certificate_manager/print_stud/[final_id]/[final_stud_income_id]","fa fa-edit","btn-info","w");
		$this->admin_model->set_pagination("certificate_manager/certificate_stud_list",$this->stud_certificate->export_data()->num_rows(),$limit,3);
		$this->admin_model->show_search_text();
		$search_q = trim(strip_tags($this->input->get("q")));
		if($search_q != ""){
			$this->admin_library->add_breadcrumb("ค้นหาคำว่า '{$search_q}'","certificate_manager/certificate_certificate_stud_list{$QUERY_STRING}","fa fa-search");
		}
		$this->admin_model->make_list();
		$this->admin_library->output();		
		
	}
	
	
	public function stud_edit($certificate_id=0)
	{
		$this->admin_model->set_menu_key('fe8009fb4a0cd1b823acd5b3bdee261d');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->load->model('administrator/stud_certificate');
		$this->form_validation->set_rules("final_text_remark","Remart","trim|required");
		//$this->form_validation->set_rules("product_id","Product ID ","trim|required");
		if($this->form_validation->run()===false)
		{
			$this->_data['row'] = $this->stud_certificate->getcertificate($certificate_id);
			if(!$this->_data['row']){
				$this->session->set_flashdata("message-error","ไม่พบข้อมูลที่ต้องการแก้ไข.");
				admin_redirect("certificate_manager/certificate_stud_list");
			}
			$this->admin_library->setTitle('stud certificate','fa fa-folder-open');
			$this->admin_library->setDetail("stud certificate manager");
			$this->admin_library->add_breadcrumb("stud certificate","certificate_manager/certificate_stud_list","fa fa-folder-open");
			$this->admin_library->add_breadcrumb("Edit certificate","certificate_manager/stud_edit/".$certificate_id,"fa fa-edit");
			
			$this->admin_library->view("certificate_manager/stud_edit");
			$this->admin_library->output();
		}else{
			$this->stud_certificate->edit_certificate();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("certificate_manager/certificate_stud_list");
		}
	}
	
	############ end stud ###############################################
	
	###################start public #################################

	public function customer_name_callback($text,$row)
	{
		$row = $this->bolt_certificate->getcustomer_name($row['customer_id']);
		$text = @$row['customer_name'];
		return $text;
	}
	
	public function customer_nut_name_callback($text,$row)
	{
		$row = $this->nut_certificate->getcustomer_name($row['customer_id']);
		$text = @$row['customer_name'];
		return $text;
	}
	public function customer_washer_name_callback($text,$row)
	{
		$row = $this->washer_certificate->getcustomer_name($row['customer_id']);
		$text = @$row['customer_name'];
		return $text;
	}
	
	public function customer_spring_name_callback($text,$row)
	{
		$row = $this->spring_certificate->getcustomer_name($row['customer_id']);
		$text = @$row['customer_name'];
		return $text;
	}
	
	public function customer_taper_name_callback($text,$row)
	{
		$row = $this->taper_certificate->getcustomer_name($row['customer_id']);
		$text = @$row['customer_name'];
		return $text;
	}
	
	public function customer_stud_name_callback($text,$row)
	{
		$row = $this->stud_certificate->getcustomer_name($row['customer_id']);
		$text = @$row['customer_name'];
		return $text;
	}
	
	public function lot_manager_callback($text,$row)
	{
		if(intval(@$row['final_createdid']) > 0){
			$user = $this->admin_library->getuserinfo($row['final_createdid']);
		}
		if(intval(@$row['final_updatedid']) > 0){
			$user = $this->admin_library->getuserinfo($row['final_updatedid']);
		}
		if(@$user){
			$name = explode(' ',$user['user_fullname']);
			$displayname = $name[0];
			if(count($name) > 1){
				$displayname .= ' '. substr($name[count($name)-1],0,1).'.';
			}
			$text = ucfirst($displayname);
		}else{
			$text = "(ไม่ระบุ)";
		}
		$text .= '<br><small>เมื่อ '. sdateth($row['final_createdtime']).'</small>';
		return $text;
	}
	
	
	
	public function manager_status_callback($text, $row){
		if($text==2){
			return '<span class="label label-success"><i class="fa fa-check-square-o" aria-hidden="true"></i> ผ่านการทดสอบ</span>';
		}else if($text==1){
			return '<span class="label label-warning"><i class="fa fa-times" aria-hidden="true"></i> ไม่ผ่านการทดสอบ</span>';
		}else{
			return '<span class="label label-inverse"><i class="fa fa-square-o" aria-hidden="true"></i> ยังไม่ได้ทดสอบ</span>';
		}
	}
	function print_bolt_callback($row)
	{
		if($row['test_bolt_status']==2){
			return true;
		}else if($row['test_bolt_status']==1){
			return true;
		}else{
			return false;
		}
	}
	
	public function date_callback($text,$row)
	{
		if($text){
			$text = strtotime($text);
			$text = date("d M Y",$text);
		}else{
			$text = "(ไม่ระบุ)";
		}
		return $text;
	}
	
	public function product_name_callback($text,$row)
	{
		$row = $this->bolt_certificate->getproduct_name($row['final_bolt_id']);
		$text = @$row['product_name'] ." ". @$row['product_type'];
		return $text;
	}
	
	public function product_nut_name_callback($text,$row){
		$row = $this->nut_certificate->getproduct_name($row['final_nut_id']);
		$text = @$row['product_name'] ." ". @$row['product_type'];
		return $text;
		
	}
	
	public function product_washer_name_callback($text,$row){
		$row = $this->washer_certificate->getproduct_name($row['final_washer_id']);
		$text = @$row['product_name'] ." ". @$row['product_type'];
		return $text;
		
	}
	
	public function product_spring_name_callback($text,$row){
		$row = $this->spring_certificate->getproduct_name($row['final_spring_id']);
		$text = @$row['product_name'] ." ". @$row['product_type'];
		return $text;
		
	}
	
	public function product_taper_name_callback($text,$row){
		$row = $this->taper_certificate->getproduct_name($row['final_taper_id']);
		$text = @$row['product_name'] ." ". @$row['product_type'];
		return $text;
		
	}
	
	public function product_stud_name_callback($text,$row){
		$row = $this->stud_certificate->getproduct_name($row['final_stud_id']);
		$text = @$row['product_name'] ." ". @$row['product_type'];
		return $text;
		
	}
	
	public function product_size_callback($text,$row){
		//$row = $this->bolt_certificate->get_size($text);
		$text = @$row['size_m'].'x'.@$row['size_p'].'x'.@$row['size_length'];
		return $text;
	}
	
	public function product_size_nut_callback($text,$row){
		$row = $this->nut_certificate->get_size($text);
		$text = @$row['size_m'].'x'.@$row['size_p'];
		return $text;
	}
	
	public function product_size_washer_callback($text,$row){
		$row = $this->washer_certificate->get_size($text);
		$text = @$row['size_m'].'x'.@$row['size_p'];
		return $text;
	}
	
	public function product_size_spring_callback($text,$row){
		$row = $this->spring_certificate->get_size($text);
		$text = @$row['size_m'].'x'.@$row['size_p'];
		return $text;
	}
	
	public function product_size_taper_callback($text,$row){
		$row = $this->taper_certificate->get_size($text);
		$text = @$row['size_m'].'x'.@$row['size_p'];
		return $text;
	}
	
	public function product_size_stud_callback($text,$row){
		$row = $this->stud_certificate->get_size($text);
		$text = @$row['size_m'].'x'.@$row['size_p'];
		return $text;
	}
	
	public function get_size_bolt_byid(){
		$boltid = $this->input->post('bolt_id');
		$this->load->model('administrator/bolt_certificate');
		$this->_data['sizelist'] = $this->bolt_certificate->get_boltsize_byid($boltid);
		
		$this->load->view('administrator/views/certificate_manager/sizelist_bolt', $this->_data);
	}
	
	public function get_income_bolt_byid(){
		$boltsizeid = $this->input->post('bolt_size_id');
		$boltid = $this->input->post('bolt_id');
		
		$this->load->model('administrator/bolt_certificate');
		$this->_data['imcomelist'] = $this->bolt_certificate->get_boltincome_byid($boltid,$boltsizeid);
		
		$this->load->view('administrator/views/certificate_manager/incomelist_bolt', $this->_data);
	
	}
	
	public function get_size_nut_byid(){
		$boltid = $this->input->post('nut_id');
		$this->load->model('administrator/nut_certificate');
		$this->_data['sizelist'] = $this->nut_certificate->get_nutsize_byid($boltid);
		
		$this->load->view('administrator/views/certificate_manager/sizelist', $this->_data);
	}
	
	public function get_income_nut_byid(){
		$nutsizeid = $this->input->post('nut_size_id');
		$nutid = $this->input->post('nut_id');
		
		$this->load->model('administrator/nut_certificate');
		$this->_data['imcomelist'] = $this->nut_certificate->get_nutincome_byid($nutid,$nutsizeid);
		
		$this->load->view('administrator/views/certificate_manager/incomelist_nut', $this->_data);
	}
		
	public function get_size_washer_byid(){
		$washerid = $this->input->post('washer_id');
		$this->load->model('administrator/washer_certificate');
		$this->_data['sizelist'] = $this->washer_certificate->get_washersize_byid($washerid);
		
		$this->load->view('administrator/views/certificate_manager/sizelist', $this->_data);
	}
	
	public function get_income_washer_byid(){
		$washersizeid = $this->input->post('washer_size_id');
		$washerid = $this->input->post('washer_id');
		
		$this->load->model('administrator/washer_certificate');
		$this->_data['imcomelist'] = $this->washer_certificate->get_washerincome_byid($washerid,$washersizeid);
		
		$this->load->view('administrator/views/certificate_manager/incomelist_washer', $this->_data);
	}
	
	public function get_size_spring_byid(){
		$springid = $this->input->post('spring_id');
		$this->load->model('administrator/spring_certificate');
		$this->_data['sizelist'] = $this->spring_certificate->get_springsize_byid($springid);
		
		$this->load->view('administrator/views/certificate_manager/sizelist', $this->_data);
	}
	
	public function get_income_spring_byid(){
		$springsizeid = $this->input->post('spring_size_id');
		$springid = $this->input->post('spring_id');
		
		$this->load->model('administrator/spring_certificate');
		$this->_data['imcomelist'] = $this->spring_certificate->get_springincome_byid($springid,$springsizeid);
		
		$this->load->view('administrator/views/certificate_manager/incomelist_spring', $this->_data);
	}
	
	public function get_size_taper_byid(){
		$taperid = $this->input->post('taper_id');
		$this->load->model('administrator/taper_certificate');
		$this->_data['sizelist'] = $this->taper_certificate->get_tapersize_byid($taperid);
		
		$this->load->view('administrator/views/certificate_manager/sizelist', $this->_data);
	}
	
	public function get_income_taper_byid(){
		$tapersizeid = $this->input->post('taper_size_id');
		$taperid = $this->input->post('taper_id');
		
		$this->load->model('administrator/taper_certificate');
		$this->_data['imcomelist'] = $this->taper_certificate->get_taperincome_byid($taperid,$tapersizeid);
		
		$this->load->view('administrator/views/certificate_manager/incomelist_taper', $this->_data);
	}
	
	public function get_size_stud_byid(){
		$studid = $this->input->post('stud_id');
		$this->load->model('administrator/stud_certificate');
		$this->_data['sizelist'] = $this->stud_certificate->get_studsize_byid($studid);
		
		$this->load->view('administrator/views/certificate_manager/sizelist', $this->_data);
	}
	
	public function get_income_stud_byid(){
		$studsizeid = $this->input->post('stud_size_id');
		$studid = $this->input->post('stud_id');
		
		$this->load->model('administrator/stud_certificate');
		$this->_data['imcomelist'] = $this->stud_certificate->get_studincome_byid($studid,$studsizeid);
		
		$this->load->view('administrator/views/certificate_manager/incomelist_stud', $this->_data);
	}
	
	public function print_bolt($final_id,$income_id)
	{
		$this->load->model('final_test_model');
		$this->load->model('administrator/bolt_certificate');
		$this->load->model('administrator/nut_certificate');
		$this->load->model('administrator/chemical_test_final_model');
		$this->load->model('income_test_model');
		$this->_data['final_id'] = $final_id;
		$this->_data['income_id'] = $income_id;
		$checked = $this->final_test_model->check_final_tested($this->_data['income_id'],$final_id);
		if($checked > 0){
			$product = $this->final_test_model->get_bolt_product($this->_data['income_id'],$final_id);
			
			$this->_data['std_list'] = $this->final_test_model->get_product_standard_bolt($product['product_id']);
			$ps_id=0;
			foreach($this->_data['std_list']->result_array() as $row){
				$this->_data['std_bolt'][$row['standard_description']]['info']=$row;
				$this->_data['std_bolt'][$row['standard_description']]['test']=$this->final_test_model->get_final_bolt_test_info($this->_data['final_id'],$row['ps_id']);
				$ps_id=$row['ps_id'];
			}
			
			$this->_data['product'] = $product;
			$this->_data['income_info'] =  $this->final_test_model->get_final_bolt($this->_data['income_id'],$final_id);
			//$this->_data['income_data'] =  $this->certificate_test_model->get_income_tested($this->_data['income_id']);
			$this->_data['shipper'] =  $this->bolt_certificate->getcustomer_name($this->_data['income_info']['customer_id']);
			$this->_data['size'] = $this->bolt_certificate->get_size($this->_data['income_info']['final_bolt_size_id']);
			//$this->_data['size'] = $this->bolt_certificate->get_cer_size($this->_data['income_info']['final_bolt_size_id']);
			$this->_data['cer_size_list'] = $this->final_test_model->get_product_standard_size_bolt($product['product_id'],$this->_data['income_info']['final_bolt_size_id']);
			
			foreach($this->_data['cer_size_list']->result_array() as $row){
				$this->_data['cer_size'][$row['ps_description']]['info']=$row;
				$this->_data['cer_size'][$row['ps_description']]['test']=$this->final_test_model->get_final_bolt_size_test_info($this->_data['income_info']['final_id'],$row['ps_id']);;
			}
			$this->_data['chemical'] = $this->final_test_model->get_chemical_final_bolt($this->_data['income_id'],$this->_data['final_id']);
			$this->_data['info'] = $this->income_test_model->get_income_bolt($this->_data['income_id']);
			$this->_data['bolt_thickness'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_bolt_cer($this->_data['income_id'],$final_id);
			$this->_data['bolt_thickness_info'] = $this->chemical_test_final_model->get_thickness_by_id($this->_data['bolt_thickness']['thickness_id']); 
			
			##### nut ##########
			
			$this->_data['nut_info'] = $this->income_test_model->get_income_nut($this->_data['income_info']['final_nut_income_id']);
			
			$this->_data['nut_chemical'] = $this->final_test_model->get_chemical_final_nut($this->_data['income_info']['final_nut_income_id'],$final_id);
			
			$this->_data['nut_product'] = $this->final_test_model->get_nut_product($this->_data['income_info']['final_nut_income_id'],$final_id);
			
			$this->_data['std_list_nut'] = $this->final_test_model->get_product_standard_nut($this->_data['nut_product']['product_id']);
			$ps_id_nut=0;
			foreach($this->_data['std_list_nut']->result_array() as $row){

				$this->_data['std_nut'][$row['standard_description']]['info']=$row;
				$this->_data['std_nut'][$row['standard_description']]['test']=$this->final_test_model->get_final_nut_test_info($this->_data['income_info']['final_id'],$row['ps_id']);
				$ps_id_nut=$row['ps_id'];
			}
			//var_dump($this->_data['std_nut'][$rown['standard_description']]['info']);
			$this->_data['nut_size'] = $this->nut_certificate->get_size($this->_data['income_info']['final_nut_size_id']);
			//var_dump($this->_data['nut_product']);exit;
			$this->_data['nut_cer_size_list'] = $this->final_test_model->get_product_standard_size_nut($this->_data['nut_product']['product_id'],$this->_data['income_info']['final_nut_size_id']);
			//var_dump($this->final_test_model->mssql->last_query());exit;
			foreach($this->_data['nut_cer_size_list']->result_array() as $row){
				$this->_data['nut_cer_size'][$row['ps_description']]['info']=$row;
				$this->_data['nut_cer_size'][$row['ps_description']]['test']=$this->final_test_model->get_final_nut_size_test_info($this->_data['income_info']['final_id'],$row['ps_id']);
			}
			$this->_data['nut_thickness'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_nut_cer($this->_data['income_info']['final_nut_income_id'],$final_id);
			
			$this->_data['nut_thickness_info'] = $this->chemical_test_final_model->get_thickness_by_id($this->_data['nut_thickness']['thickness_id']); 
			
			######## washer #####################
			
			$this->_data['washer_info'] = $this->income_test_model->get_income_washer($this->_data['income_info']['final_washer_income_id']);
			
			$this->_data['washer_chemical'] = $this->final_test_model->get_chemical_final_washer($this->_data['income_info']['final_washer_income_id'],$final_id);
			
			$this->_data['washer_product'] = $this->final_test_model->get_washer_product($this->_data['income_info']['final_washer_income_id'],$final_id);
			
			$this->_data['std_list_washer'] = $this->final_test_model->get_product_standard_washer($this->_data['washer_product']['product_id']);
			$ps_id_washer=0;
			foreach($this->_data['std_list_washer']->result_array() as $row){

				$this->_data['std_washer'][$row['standard_description']]['info']=$row;
				$this->_data['std_washer'][$row['standard_description']]['test']=$this->final_test_model->get_final_washer_test_info($this->_data['income_info']['final_id'],$row['ps_id']);
				$ps_id_washer=$row['ps_id'];
			}

			
			
			//var_dump($this->_data['washer_product']);exit;
			$this->_data['washer_cer_size_list'] = $this->final_test_model->get_product_standard_size_washer($this->_data['washer_product']['product_id'],$this->_data['income_info']['final_washer_size_id']);
			//var_dump($this->final_test_model->mssql->last_query());exit;
			
			
			foreach($this->_data['washer_cer_size_list']->result_array() as $row){
				$this->_data['washer_cer_size'][$row['ps_description']]['info']=$row;
				$this->_data['washer_cer_size'][$row['ps_description']]['test']=$this->final_test_model->get_final_washer_size_test_info($this->_data['income_info']['final_id'],$row['ps_id']);;
			}
			$this->_data['washer_thickness'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_washer_cer($this->_data['income_info']['final_washer_income_id'],$final_id);
			$this->_data['washer_thickness_info'] = $this->chemical_test_final_model->get_thickness_by_id($this->_data['washer_thickness']['thickness_id']); 
			
			
			
			if($product['certificate_id'] == "2"){
			//echo $product['certificate_id'];
				header("Content-type: application/vnd.ms-excel");
				header("Content-Disposition: attachment;Filename=certificate_bolt_report_".$final_id.".xls");
				$this->load->view('administrator/views/certificate_manager/print_bolt_din', $this->_data);
			}else if($product['certificate_id'] == "1"){
				header("Content-type: application/vnd.ms-excel");
				header("Content-Disposition: attachment;Filename=certificate_bolt_report_".$final_id.".xls");
				$this->load->view('administrator/views/certificate_manager/print_bolt', $this->_data);
			}else if($product['certificate_id'] == "5"){
				header("Content-type: application/vnd.ms-excel");
				header("Content-Disposition: attachment;Filename=certificate_bolt_report_".$final_id.".xls");
				$this->load->view('administrator/views/certificate_manager/print_bolt_jis_f10t', $this->_data);
			}else if($product['certificate_id'] == "6"){
				header("Content-type: application/vnd.ms-excel");
				header("Content-Disposition: attachment;Filename=certificate_bolt_report_".$final_id.".xls");
				$this->load->view('administrator/views/certificate_manager/print_bolt_jis_s10t', $this->_data);
			}
		}else{
			$this->session->set_flashdata("message-warning","ข้อมูลยังไม่ได้ถูกทดสอบ.");
			admin_redirect("certificate_manager/certificate_bolt_list");
		}
				
	}
	
	public function print_nut($final_id,$income_id)
	{
		$this->load->model('final_test_model');
		$this->load->model('administrator/nut_certificate');
		$this->load->model('administrator/chemical_test_final_model');
		$this->load->model('income_test_model');
		$this->_data['final_id'] = $final_id;
		$this->_data['income_id'] = $income_id;
		$checked = $this->final_test_model->check_final_tested_nut($this->_data['income_id'],$final_id);
		if($checked > 0){
						
			##### nut ##########
			
			
			$this->_data['nut_info'] = $this->income_test_model->get_income_nut($income_id);
			
			$this->_data['nut_chemical'] = $this->final_test_model->get_chemical_final_nut($income_id,$final_id);
			
			$this->_data['nut_product'] = $this->final_test_model->get_nut_product_main($income_id,$final_id);
			
			$this->_data['std_list_nut'] = $this->final_test_model->get_product_standard_nut($this->_data['nut_product']['product_id']);
			$ps_id_nut=0;
			foreach($this->_data['std_list_nut']->result_array() as $row){

				$this->_data['std_nut'][$row['standard_description']]['info']=$row;
				$this->_data['std_nut'][$row['standard_description']]['test']=$this->final_test_model->get_final_nut_test_info($final_id,$row['ps_id']);
				$ps_id_nut=$row['ps_id'];
			}
			//var_dump($this->_data['std_nut'][$rown['standard_description']]['info']);
			$this->_data['income_info'] =  $this->final_test_model->get_final_nut_main($this->_data['income_id'],$final_id);
			$this->_data['shipper'] =  $this->nut_certificate->getcustomer_name($this->_data['income_info']['customer_id']);
			$this->_data['size'] = $this->nut_certificate->get_size($this->_data['income_info']['final_nut_size_id']);
			$this->_data['nut_cer_size_list'] = $this->final_test_model->get_product_standard_size_nut($this->_data['nut_product']['product_id'],$this->_data['income_info']['final_nut_size_id']);
			//var_dump($this->final_test_model->mssql->last_query());exit;
			foreach($this->_data['nut_cer_size_list']->result_array() as $row){
				$this->_data['nut_cer_size'][$row['ps_description']]['info']=$row;
				$this->_data['nut_cer_size'][$row['ps_description']]['test']=$this->final_test_model->get_final_nut_size_test_info($this->_data['income_info']['final_id'],$row['ps_id']);
			}
			$this->_data['nut_thickness'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_nut_cer($this->_data['income_info']['final_nut_income_id'],$final_id);
			
			$this->_data['nut_thickness_info'] = $this->chemical_test_final_model->get_thickness_by_id($this->_data['nut_thickness']['thickness_id']); 
			
						
			header("Content-type: application/vnd.ms-excel");
			header("Content-Disposition: attachment;Filename=certificate_nut_report_".$final_id.".xls");
			$this->load->view('administrator/views/certificate_manager/print_nut', $this->_data);
		}else{
			$this->session->set_flashdata("message-warning","ข้อมูลยังไม่ได้ถูกทดสอบ.");
			admin_redirect("certificate_manager/certificate_nut_list");
		}

				
	}
	
	public function print_washer($final_id,$income_id)
	{
		$this->load->model('final_test_model');
		$this->load->model('administrator/washer_certificate');
		$this->load->model('administrator/chemical_test_final_model');
		$this->load->model('income_test_model');
		$this->_data['final_id'] = $final_id;
		$this->_data['income_id'] = $income_id;
		$checked = $this->final_test_model->check_final_tested_washer($this->_data['income_id'],$final_id);
		if($checked > 0){
						
			##### washer ##########
			
			
			$this->_data['washer_info'] = $this->income_test_model->get_income_washer($income_id);
			
			$this->_data['washer_chemical'] = $this->final_test_model->get_chemical_final_washer($income_id,$final_id);
			
			$this->_data['washer_product'] = $this->final_test_model->get_washer_product_main($income_id,$final_id);
			
			$this->_data['std_list_washer'] = $this->final_test_model->get_product_standard_washer($this->_data['washer_product']['product_id']);
			$ps_id_washer=0;
			foreach($this->_data['std_list_washer']->result_array() as $row){

				$this->_data['std_washer'][$row['standard_description']]['info']=$row;
				$this->_data['std_washer'][$row['standard_description']]['test']=$this->final_test_model->get_final_washer_test_info($final_id,$row['ps_id']);
				$ps_id_washer=$row['ps_id'];
			}
			//var_dump($this->_data['std_washer'][$rown['standard_description']]['info']);
			$this->_data['income_info'] =  $this->final_test_model->get_final_washer_main($this->_data['income_id'],$final_id);
			$this->_data['shipper'] =  $this->washer_certificate->getcustomer_name($this->_data['income_info']['customer_id']);
			$this->_data['size'] = $this->washer_certificate->get_size($this->_data['income_info']['final_washer_size_id']);
			$this->_data['washer_cer_size_list'] = $this->final_test_model->get_product_standard_size_washer($this->_data['washer_product']['product_id'],$this->_data['income_info']['final_washer_size_id']);
			//var_dump($this->final_test_model->mssql->last_query());exit;
			foreach($this->_data['washer_cer_size_list']->result_array() as $row){
				$this->_data['washer_cer_size'][$row['ps_description']]['info']=$row;
				$this->_data['washer_cer_size'][$row['ps_description']]['test']=$this->final_test_model->get_final_washer_size_test_info($this->_data['income_info']['final_id'],$row['ps_id']);
			}
			$this->_data['washer_thickness'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_washer_cer($this->_data['income_info']['final_washer_income_id'],$final_id);
			
			$this->_data['washer_thickness_info'] = $this->chemical_test_final_model->get_thickness_by_id($this->_data['washer_thickness']['thickness_id']); 
			
						
			header("Content-type: application/vnd.ms-excel");
			header("Content-Disposition: attachment;Filename=certificate_washer_report_".$final_id.".xls");
			$this->load->view('administrator/views/certificate_manager/print_washer', $this->_data);
		}else{
			$this->session->set_flashdata("message-warning","ข้อมูลยังไม่ได้ถูกทดสอบ.");
			admin_redirect("certificate_manager/certificate_washer_list");
		}
				
	}

	public function print_spring($final_id,$income_id)
	{
		$this->load->model('final_test_model');
		$this->load->model('administrator/spring_certificate');
		$this->load->model('administrator/chemical_test_final_model');
		$this->load->model('income_test_model');
		$this->_data['final_id'] = $final_id;
		$this->_data['income_id'] = $income_id;
		$checked = $this->final_test_model->check_final_tested_spring($this->_data['income_id'],$final_id);
		if($checked > 0){
						
			##### spring ##########
			
			
			$this->_data['spring_info'] = $this->income_test_model->get_income_spring($income_id);
			
			$this->_data['spring_chemical'] = $this->final_test_model->get_chemical_final_spring($income_id,$final_id);
			
			$this->_data['spring_product'] = $this->final_test_model->get_spring_product_main($income_id,$final_id);
			
			$this->_data['std_list_spring'] = $this->final_test_model->get_product_standard_spring($this->_data['spring_product']['product_id']);
			$ps_id_spring=0;
			foreach($this->_data['std_list_spring']->result_array() as $row){

				$this->_data['std_spring'][$row['standard_description']]['info']=$row;
				$this->_data['std_spring'][$row['standard_description']]['test']=$this->final_test_model->get_final_spring_test_info($final_id,$row['ps_id']);
				$ps_id_spring=$row['ps_id'];
			}
			//var_dump($this->_data['std_spring'][$rown['standard_description']]['info']);
			$this->_data['income_info'] =  $this->final_test_model->get_final_spring_main($this->_data['income_id'],$final_id);
			$this->_data['shipper'] =  $this->spring_certificate->getcustomer_name($this->_data['income_info']['customer_id']);
			$this->_data['size'] = $this->spring_certificate->get_size($this->_data['income_info']['final_spring_size_id']);
			$this->_data['spring_cer_size_list'] = $this->final_test_model->get_product_standard_size_spring($this->_data['spring_product']['product_id'],$this->_data['income_info']['final_spring_size_id']);
			//var_dump($this->final_test_model->mssql->last_query());exit;
			foreach($this->_data['spring_cer_size_list']->result_array() as $row){
				$this->_data['spring_cer_size'][$row['ps_description']]['info']=$row;
				$this->_data['spring_cer_size'][$row['ps_description']]['test']=$this->final_test_model->get_final_spring_size_test_info($this->_data['income_info']['final_id'],$row['ps_id']);
			}
			$this->_data['spring_thickness'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_spring_cer($this->_data['income_info']['final_spring_income_id'],$final_id);
			
			$this->_data['spring_thickness_info'] = $this->chemical_test_final_model->get_thickness_by_id($this->_data['spring_thickness']['thickness_id']); 
			
						
			header("Content-type: application/vnd.ms-excel");
			header("Content-Disposition: attachment;Filename=certificate_spring_report_".$final_id.".xls");
			$this->load->view('administrator/views/certificate_manager/print_spring', $this->_data);
		}else{
			$this->session->set_flashdata("message-warning","ข้อมูลยังไม่ได้ถูกทดสอบ.");
			admin_redirect("certificate_manager/certificate_spring_list");
		}
				
	}
	
	public function print_taper($final_id,$income_id)
	{
		$this->load->model('final_test_model');
		$this->load->model('administrator/taper_certificate');
		$this->load->model('administrator/chemical_test_final_model');
		$this->load->model('income_test_model');
		$this->_data['final_id'] = $final_id;
		$this->_data['income_id'] = $income_id;
		$checked = $this->final_test_model->check_final_tested_taper($this->_data['income_id'],$final_id);
		if($checked > 0){
						
			##### taper ##########
			
			
			$this->_data['taper_info'] = $this->income_test_model->get_income_taper($income_id);
			
			$this->_data['taper_chemical'] = $this->final_test_model->get_chemical_final_taper($income_id,$final_id);
			
			$this->_data['taper_product'] = $this->final_test_model->get_taper_product_main($income_id,$final_id);
			
			$this->_data['std_list_taper'] = $this->final_test_model->get_product_standard_taper($this->_data['taper_product']['product_id']);
			$ps_id_taper=0;
			foreach($this->_data['std_list_taper']->result_array() as $row){

				$this->_data['std_taper'][$row['standard_description']]['info']=$row;
				$this->_data['std_taper'][$row['standard_description']]['test']=$this->final_test_model->get_final_taper_test_info($final_id,$row['ps_id']);
				$ps_id_taper=$row['ps_id'];
			}
			//var_dump($this->_data['std_taper'][$rown['standard_description']]['info']);
			$this->_data['income_info'] =  $this->final_test_model->get_final_taper_main($this->_data['income_id'],$final_id);
			$this->_data['shipper'] =  $this->taper_certificate->getcustomer_name($this->_data['income_info']['customer_id']);
			$this->_data['size'] = $this->taper_certificate->get_size($this->_data['income_info']['final_taper_size_id']);
			$this->_data['taper_cer_size_list'] = $this->final_test_model->get_product_standard_size_taper($this->_data['taper_product']['product_id'],$this->_data['income_info']['final_taper_size_id']);
			//var_dump($this->final_test_model->mssql->last_query());exit;
			foreach($this->_data['taper_cer_size_list']->result_array() as $row){
				$this->_data['taper_cer_size'][$row['ps_description']]['info']=$row;
				$this->_data['taper_cer_size'][$row['ps_description']]['test']=$this->final_test_model->get_final_taper_size_test_info($this->_data['income_info']['final_id'],$row['ps_id']);
			}
			$this->_data['taper_thickness'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_taper_cer($this->_data['income_info']['final_taper_income_id'],$final_id);
			
			$this->_data['taper_thickness_info'] = $this->chemical_test_final_model->get_thickness_by_id($this->_data['taper_thickness']['thickness_id']); 
			
						
			header("Content-type: application/vnd.ms-excel");
			header("Content-Disposition: attachment;Filename=certificate_taper_report_".$final_id.".xls");
			$this->load->view('administrator/views/certificate_manager/print_taper', $this->_data);
		}else{
			$this->session->set_flashdata("message-warning","ข้อมูลยังไม่ได้ถูกทดสอบ.");
			admin_redirect("certificate_manager/certificate_taper_list");
		}
				
	}
	
	public function print_stud($final_id,$income_id)
	{
		$this->load->model('final_test_model');
		$this->load->model('administrator/stud_certificate');
		$this->load->model('administrator/chemical_test_final_model');
		$this->load->model('income_test_model');
		$this->_data['final_id'] = $final_id;
		$this->_data['income_id'] = $income_id;
		$checked = $this->final_test_model->check_final_tested_stud($this->_data['income_id'],$final_id);
		if($checked > 0){
						
			##### stud ##########
			
			
			$this->_data['stud_info'] = $this->income_test_model->get_income_stud($income_id);
			
			$this->_data['stud_chemical'] = $this->final_test_model->get_chemical_final_stud($income_id,$final_id);
			
			$this->_data['stud_product'] = $this->final_test_model->get_stud_product_main($income_id,$final_id);
			
			$this->_data['std_list_stud'] = $this->final_test_model->get_product_standard_stud($this->_data['stud_product']['product_id']);
			$ps_id_stud=0;
			foreach($this->_data['std_list_stud']->result_array() as $row){

				$this->_data['std_stud'][$row['standard_description']]['info']=$row;
				$this->_data['std_stud'][$row['standard_description']]['test']=$this->final_test_model->get_final_stud_test_info($final_id,$row['ps_id']);
				$ps_id_stud=$row['ps_id'];
			}
			//var_dump($this->_data['std_stud'][$rown['standard_description']]['info']);
			$this->_data['income_info'] =  $this->final_test_model->get_final_stud_main($this->_data['income_id'],$final_id);
			$this->_data['shipper'] =  $this->stud_certificate->getcustomer_name($this->_data['income_info']['customer_id']);
			$this->_data['size'] = $this->stud_certificate->get_size($this->_data['income_info']['final_stud_size_id']);
			$this->_data['stud_cer_size_list'] = $this->final_test_model->get_product_standard_size_stud($this->_data['stud_product']['product_id'],$this->_data['income_info']['final_stud_size_id']);
			//var_dump($this->final_test_model->mssql->last_query());exit;
			foreach($this->_data['stud_cer_size_list']->result_array() as $row){
				$this->_data['stud_cer_size'][$row['ps_description']]['info']=$row;
				$this->_data['stud_cer_size'][$row['ps_description']]['test']=$this->final_test_model->get_final_stud_size_test_info($this->_data['income_info']['final_id'],$row['ps_id']);
			}
			$this->_data['stud_thickness'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_stud_cer($this->_data['income_info']['final_stud_income_id'],$final_id);
			
			$this->_data['stud_thickness_info'] = $this->chemical_test_final_model->get_thickness_by_id($this->_data['stud_thickness']['thickness_id']); 
			
						
			header("Content-type: application/vnd.ms-excel");
			header("Content-Disposition: attachment;Filename=certificate_stud_report_".$final_id.".xls");
			$this->load->view('administrator/views/certificate_manager/print_stud', $this->_data);
		}else{
			$this->session->set_flashdata("message-warning","ข้อมูลยังไม่ได้ถูกทดสอบ.");
			admin_redirect("certificate_manager/certificate_stud_list");
		}
				
	}
	
	
	
	public function pdf_bolt($final_id,$income_id)
	{
		$this->load->model("fpdf");
		$this->load->model("pdf_mc_table");
		$this->load->model("pdf-certificate/boltcertificate");
		
		$this->boltcertificate->make($final_id,$income_id);
		
				
	}
}