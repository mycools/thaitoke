<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
/**
* Project Name : iAON
* Build Date : 8/7/2558 BE
* Author Name : Jarak Kritkiattisak
* File Name : report_anchorbolt.php
* File Location : /Volumes/Macintosh HD/Users/mycools/Library/Caches/Coda 2/3A0380CF-F77F-4E56-8DA0-E639650EB21C
* File Type : Controller	
* Remote URL : http://192.168.100.20/application/controllers/report_anchorbolt.php
*/
class Report_final_anchorbolt extends CI_Controller {
	var $_data=array();
	var $report_save_path = './report/';
	var $file_type = "xlsx"; //xlsx
	var $writer_type='Excel2007'; //'Excel2007';
	function __construct(){
		parent::__construct();
		
		$this->load->library('admin_library');
		$this->load->model('administrator/admin_model');
		$this->load->model('administrator/menu_model');
		$this->load->model('report_anchor_model');
		$this->admin_library->forceLogin();
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');
		$this->mssql = $this->load->database("mssql", true);
	}
	public function index()
	{
		redirect("dashboard");
	}
	function print_final($final_id)
	{
		
		$this->mssql->select("final_anchor.final_invoice_no,
										final_anchor.product_id as final_anchor_product_id,
										final_anchor.final_purchase_no,
										final_anchor.final_job_no,
										final_anchor.final_delivery_no,
										final_anchor.final_material_no,
										final_anchor.final_certificate_no,
										final_anchor.final_quantity,
										final_anchor.final_date_delivery,
										customer.customer_name,
										anchor_product.product_grade,
										final_anchor.product_type as final_anchor_product_type,
										product_size_anchor.size_m,
										product_size_anchor.size_length,
										product_size_anchor.size_t,
										product_size_anchor.size_w,
										anchor_product.size_id,
										product_size_anchor.product_id,
										final_anchor_test.product_type as test_product_type,
										final_anchor_test.standard_id,
										final_anchor_test.chemical_test_use,
										final_anchor_test.material_test_use,
										final_anchor_test.galvanized_test_using,
										final_anchor_test.heattreatment_test_using,
										final_anchor_test.thickness_1,
										final_anchor_test.thickness_2,
										final_anchor_test.thickness_3,
										final_anchor_test.thickness_4,
										final_anchor_test.thickness_5,
										final_anchor_test.average as thickness_average,
										final_anchor_test.quenching_temperature,
										final_anchor_test.tempering_temperature,
										final_anchor_test.quenching_min,
										final_anchor_test.quenching_max,
										final_anchor_test.tempering_min,
										final_anchor_test.tempering_max,
										final_anchor_test.quenching_test,
										final_anchor_test.tempering_test,
										product_anchor.product_low_carbon,
										anchor_product.product_cate,
										income_anchor.income_heat_no,
										income_anchor.income_id");
		$this->mssql->join("customer","customer.customer_id=final_anchor.customer_id");
		$this->mssql->join("anchor_product","anchor_product.product_id=final_anchor.product_id");
		$this->mssql->join("product_size_anchor","anchor_product.size_id=product_size_anchor.size_id");
		$this->mssql->join("final_anchor_test","final_anchor_test.final_id=final_anchor.final_id");
		$this->mssql->join("product_anchor","product_anchor.product_id = product_size_anchor.product_id");
		$this->mssql->join("income_anchor","income_anchor.income_id = final_anchor_test.income_id");
		$this->mssql->where("final_anchor.final_id",$final_id);
		$this->mssql->where("final_anchor.test_status","2");
		$this->mssql->limit(1);
		$final_row = $this->mssql->get("final_anchor")->row_array();
		if(!$final_row){
			$this->session->set_flashdata("message-warning","ไม่สามารถออกใบ Certificate ได้เนื่องจากยังไม่ผ่านการทดสอบ.");
			redirect($_SERVER['HTTP_REFERER']);
		}
		$this->mssql->where("chemical_final_anchor.final_id",$final_id);
		$chemical_final_row = $this->mssql->get("chemical_final_anchor")->result_array();

		
		$this->mssql->where("product_mechanical_anchor.income_id",$final_row['income_id']);
		$this->mssql->where("product_mechanical_anchor.ps_status","approved");
		$this->mssql->order_by("product_mechanical_anchor.ps_id","asc");
		$product_mechanical_anchor_row = $this->mssql->get("product_mechanical_anchor")->result_array();
		
		$income_anchor_testing = $this->mssql->where('income_anchor_testing.income_id', $final_row['income_id'])
       ->order_by('income_anchor_testing.anchor_testing_id','asc')
       ->join('product_standard_anchor','income_anchor_testing.anchor_standard_id=product_standard_anchor.ps_id','inner')
       ->limit(1)
       ->get('income_anchor_testing')->row_array();
	   
	   $thickness_galvanizes = $this->mssql->where('id',$final_row['standard_id'])->order_by('id','asc')
       ->get('thickness_galvanizes')->row_array();
	   
	   
		$this->mssql->where("product_mechanical_anchor_testing.final_id",$final_id);
		$product_mechanical_anchor_testing_row = $this->mssql->get("product_mechanical_anchor_testing")->result_array();
		//var_dump($product_mechanical_anchor_testing_row[0]);exit();
		$fontName = "Tahoma";
		$strFileName = "anchorbolt_final_".$final_id.".".$this->file_type;
		$column = range("A","Z");
		$size="";
		if($final_row['final_anchor_product_type']=="sheet")
		{
				$size .= "T".$final_row['size_t'];
				$size .= " x W".$final_row['size_w'];
				$size .= " x L".$final_row['size_length'];
		}else{
				$size .= "M".$final_row['size_m'];
				$size .= " x L".$final_row['size_length'];
		}
		$d10 = "STEEL GRADE ";
		$d10 .= strtoupper($final_row['product_cate'])." ";
		$d10 .= strtoupper($final_row['product_grade'])." ";
		if($final_row['product_low_carbon']=="1"){
			$d10 .= "(LOW CARBON)	";
		}
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		$objPHPExcel = $objReader->load($this->report_save_path."report_anchorbolt_final_template.xlsx");
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue("A10","MATERIAL : ".$final_row['final_material_no']);
		
		$objPHPExcel->getActiveSheet()->setCellValue("D2",$final_row['customer_name']);
		$objPHPExcel->getActiveSheet()->setCellValue("D3",$final_row['final_job_no']);
		$objPHPExcel->getActiveSheet()->setCellValue("D4",$final_row['final_invoice_no']);
		$objPHPExcel->getActiveSheet()->setCellValue("D5","ACHOR BOLT GRADE ".strtoupper($final_row['product_grade'])." ".strtoupper($final_row['test_product_type']));
		$objPHPExcel->getActiveSheet()->setCellValue("D6",$size);
		$objPHPExcel->getActiveSheet()->setCellValue("D7",number_format($final_row['final_quantity']));
		$objPHPExcel->getActiveSheet()->setCellValue("D10",$d10);
		
		$objPHPExcel->getActiveSheet()->setCellValue("J10","HEAT NO. : ".$final_row['income_heat_no']);
		$objPHPExcel->getActiveSheet()->setCellValue("N10","CERTIFICATE NO. : ".$final_row['final_certificate_no']);
		$objPHPExcel->getActiveSheet()->setCellValue("O2",$final_row['final_certificate_no']);
		$objPHPExcel->getActiveSheet()->setCellValue("O3",sprintf(" %04d",$final_id));
		$objPHPExcel->getActiveSheet()->setCellValue("O4",date("d F Y",strtotime($final_row['final_date_delivery'])));
		//var_dump($final_row);exit();
		if($final_row['chemical_test_use'] == "1")
		{
			$col_chem_index =4; 
			foreach($chemical_final_row as $chemical)
			{
					$objPHPExcel->getActiveSheet()->setCellValue($column[$col_chem_index]."11"," ".$chemical['chemical_value']);
					$objPHPExcel->getActiveSheet()->setCellValue($column[$col_chem_index]."12"," ".$chemical['chemical_result']);
					$col_chem_index++;
			}	
		}
		$col_product_mechanical_index =5; 
		foreach($product_mechanical_anchor_row as $product_mechanical){
			$objPHPExcel->getActiveSheet()->setCellValue($column[$col_product_mechanical_index]."15","min (".$product_mechanical['ps_min_unit'].")");
			$objPHPExcel->getActiveSheet()->setCellValue($column[$col_product_mechanical_index]."16"," ".$product_mechanical['ps_min']);
			$objPHPExcel->getActiveSheet()->setCellValue($column[$col_product_mechanical_index]."17"," ".$product_mechanical['ps_result']);
			$col_product_mechanical_index++;
			$objPHPExcel->getActiveSheet()->setCellValue($column[$col_product_mechanical_index]."15","max (".$product_mechanical['ps_max_unit'].")");
			$objPHPExcel->getActiveSheet()->setCellValue($column[$col_product_mechanical_index]."16"," ".$product_mechanical['ps_max']);
			$col_product_mechanical_index++;
		}
		
		$objPHPExcel->getActiveSheet()->setCellValue($column[$col_product_mechanical_index]."15","min (".$income_anchor_testing['ps_min_unit'].")");
		$objPHPExcel->getActiveSheet()->setCellValue($column[$col_product_mechanical_index]."16"," ".$income_anchor_testing['ps_min']);
		$objPHPExcel->getActiveSheet()->setCellValue($column[$col_product_mechanical_index]."17"," ".$income_anchor_testing['anchor_testing1']);
		$col_product_mechanical_index++;
		$objPHPExcel->getActiveSheet()->setCellValue($column[$col_product_mechanical_index]."15","max (".$income_anchor_testing['ps_max_unit'].")");
		$objPHPExcel->getActiveSheet()->setCellValue($column[$col_product_mechanical_index]."16"," ".$income_anchor_testing['ps_max']);
			
			
		if($final_row['material_test_use'] == "1"){
			$col_mechanical_index =5; 
			//var_dump($product_mechanical_anchor_testing_row);exit();
			foreach($product_mechanical_anchor_testing_row as $mechanical)
			{
					$objPHPExcel->getActiveSheet()->setCellValue($column[$col_mechanical_index]."19",$mechanical['description']);
					$objPHPExcel->getActiveSheet()->setCellValue($column[$col_mechanical_index]."20","min (".$mechanical['ps_min_unit'].")");
					$objPHPExcel->getActiveSheet()->setCellValue($column[$col_mechanical_index]."21"," ".$mechanical['ps_min']);
					$objPHPExcel->getActiveSheet()->setCellValue($column[$col_mechanical_index]."22"," ".$mechanical['test_result']);
					$col_mechanical_index++;
					$objPHPExcel->getActiveSheet()->setCellValue($column[$col_mechanical_index]."20","max (".$mechanical['ps_min_unit'].")");
					$objPHPExcel->getActiveSheet()->setCellValue($column[$col_mechanical_index]."21"," ".$mechanical['ps_max']);
					$col_mechanical_index++;
			}
		}
		
		if($final_row['galvanized_test_using'] == "1"){
			$objPHPExcel->getActiveSheet()->setCellValue("A24","THICKNESS GALVANIZED STANDARD ".$thickness_galvanizes['grade']." ".$thickness_galvanizes['name']."  ".$thickness_galvanizes['condition']." ".$thickness_galvanizes['condition_value']." Micron (Test By PosiTector model PosiTector 6000 F Probe)");
			//$objPHPExcel->getActiveSheet()->mergeCells('A24:P24');
			$objPHPExcel->getActiveSheet()->setCellValue("E25",$final_row['thickness_1']." ");
			$objPHPExcel->getActiveSheet()->setCellValue("F25",$final_row['thickness_2']." ");
			$objPHPExcel->getActiveSheet()->setCellValue("G25",$final_row['thickness_3']." ");
			$objPHPExcel->getActiveSheet()->setCellValue("H25",$final_row['thickness_4']." ");
			$objPHPExcel->getActiveSheet()->setCellValue("I25",$final_row['thickness_5']." ");
			$objPHPExcel->getActiveSheet()->setCellValue("K25",$final_row['thickness_average']." ");
		}
		if($final_row['heattreatment_test_using'] == "1"){
			$objPHPExcel->getActiveSheet()->setCellValue("F29","°".$final_row['quenching_temperature']);
			$objPHPExcel->getActiveSheet()->setCellValue("F31",$final_row['quenching_min']." ");
			$objPHPExcel->getActiveSheet()->setCellValue("G31",$final_row['quenching_max']." ");
			$objPHPExcel->getActiveSheet()->setCellValue("F32",$final_row['quenching_test']." ");
			
			$objPHPExcel->getActiveSheet()->setCellValue("H29","°".$final_row['quenching_temperature']);
			$objPHPExcel->getActiveSheet()->setCellValue("H31"," ".$final_row['tempering_max']." ");
			$objPHPExcel->getActiveSheet()->setCellValue("I31"," ".$final_row['tempering_max']." ");
			$objPHPExcel->getActiveSheet()->setCellValue("H32"," ".$final_row['tempering_test']." ");
		}
		/*
		quenching_temperature
		tempering_temperature
		quenching_min
		quenching_max
		tempering_min
		tempering_max
		quenching_test
		tempering_test
		*/
		$removeCount = 0;
		if($final_row['chemical_test_use'] <> "1"){
			$objPHPExcel->getActiveSheet()->removeRow(11);
			$objPHPExcel->getActiveSheet()->removeRow(11);
			$objPHPExcel->getActiveSheet()->removeRow(11);
			$removeCount = $removeCount+3;
		}
		if($final_row['material_test_use'] <> "1"){
			$objPHPExcel->getActiveSheet()->removeRow(19-$removeCount);
			$objPHPExcel->getActiveSheet()->removeRow(19-$removeCount);
			$objPHPExcel->getActiveSheet()->removeRow(19-$removeCount);
			$objPHPExcel->getActiveSheet()->removeRow(19-$removeCount);
			$objPHPExcel->getActiveSheet()->removeRow(19-$removeCount); //23
			$removeCount = $removeCount+5;
		}
		if($final_row['galvanized_test_using'] <> "1"){
			$objPHPExcel->getActiveSheet()->removeRow(24-$removeCount);
			$objPHPExcel->getActiveSheet()->removeRow(24-$removeCount);
			$removeCount = $removeCount+2;
		}else{
			$mergeExcelRow24 = 24-$removeCount;
			$objPHPExcel->getActiveSheet()->mergeCells('A'.$mergeExcelRow24.':P'.$mergeExcelRow24);
		}
		if($final_row['heattreatment_test_using'] <> "1"){
			$objPHPExcel->getActiveSheet()->removeRow(27-$removeCount);
			$objPHPExcel->getActiveSheet()->removeRow(27-$removeCount);
			$objPHPExcel->getActiveSheet()->removeRow(27-$removeCount);
			$objPHPExcel->getActiveSheet()->removeRow(27-$removeCount);
			$objPHPExcel->getActiveSheet()->removeRow(27-$removeCount);
			$objPHPExcel->getActiveSheet()->removeRow(27-$removeCount);
			$removeCount = $removeCount+6;
		}
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $this->writer_type);
		$objWriter->save($this->report_save_path.$strFileName);	
		if(!$this->input->is_cli_request()){
			if($this->file_type=="xlsx"){
				header('Content-Type: application/octet-stream');
				header('Content-Disposition: attachment; filename="'.$strFileName.'"'); 
				header('Content-Transfer-Encoding: binary');
			}else{
				header('Content-Type: text/html;charset:utf-8');
			}
			readfile($this->report_save_path.$strFileName);
		}
		
		
	}
	
}

/* End of file report_anchorbolt.php */
/* Location: ./application/controllers/report_anchorbolt.php */