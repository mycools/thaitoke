<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Chemical_test_final extends CI_Controller {
	var $seq = 0;
	var $_data=array();
	function __construct(){
		parent::__construct();
		
		$this->load->library('admin_library');
		$this->load->model('administrator/admin_model');
		$this->load->model('administrator/menu_model');
		$this->load->model('administrator/chemical_test_final_model');
		$this->load->model('administrator/chemical_test_model');
		$this->admin_library->forceLogin();
		
	}
	
	public function test_bolt($incomeid=0, $finalid=0){
		
		$this->admin_model->set_menu_key('8a11fc1831fdfa2b49e8203ff134f4ec');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->_data['tested_status'] = $this->chemical_test_final_model->check_test_bolt_status($incomeid, $finalid);
		
		$this->form_validation->set_rules('test_value[]','Value','trim');
		$this->form_validation->set_rules('chemical_id[]','Chemical ID','trim');
		$this->form_validation->set_rules('chemical_value[]','Chemical Value','trim');
		$this->form_validation->set_rules('test_status','Test Status','trim');
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("Bolt incoming",'final_manager/final_bolt_list',"icon-folder-open");
			$this->admin_library->add_breadcrumb("ค่าทดสอบ Chemical ของ Bolt",current_url(),"icon-plus");
			$this->_data['row'] = $this->chemical_test_final_model->get_chemical_bolt($incomeid);
			$this->_data['thicknesslist'] = $this->chemical_test_final_model->get_thickness();
			$this->_data['testthicknessinfo'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_bolt($incomeid);
			$this->_data['final_info'] = $this->chemical_test_final_model->get_finalinfo_bolt_byid($finalid);
			
			$this->admin_library->view("chemical_test_final/test_bolt", $this->_data);
			$this->admin_library->output();
		}else{
			
			/* Update Final - Start */
			if($this->_data['tested_status']<=0){
				$this->chemical_test_final_model->save_final_chemical_bolt($incomeid, $finalid);
			}else{
				$this->chemical_test_final_model->update_final_chemical_bolt($finalid);
			}
			/* Update Final - End */
			
			/* Save Cer - Start */
			//$this->chemical_test_final_model->save_cer_chemical_bolt($finalid);
			/* Save Cer - End */
			
			/* Update Thickness final - Start */
			if($this->_data['tested_status']<=0){
				$this->chemical_test_final_model->save_final_thickness_bolt($incomeid, $finalid);
			}else{
				$this->chemical_test_final_model->update_final_thickness_bolt($finalid);
			}
			/* Update Thickness final - End */
			
			/* Update Thickness cer - Start */
			//$this->chemical_test_final_model->save_cer_thickness_bolt($finalid);
			/* Update Thickness cer - End */
			
			/* Update test status - Start */
			$this->chemical_test_final_model->update_test_status_bolt($finalid);
			/* Update test status - End */
			
			/* Update stock - Start */
			$this->chemical_test_final_model->update_stock_bolt($incomeid, $finalid, $this->input->post('test_status'));
			/* Update stock - End */
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("final_manager/final_bolt_list/");
		}
		
	}
	
	public function test_nut($incomeid=0, $finalid=0){
		
		$this->admin_model->set_menu_key('8a11fc1831fdfa2b49e8203ff134f4ec');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->_data['tested_status'] = $this->chemical_test_final_model->check_test_nut_status($incomeid, $finalid);
		
		$this->form_validation->set_rules('test_value[]','Value','trim');
		$this->form_validation->set_rules('chemical_id[]','Chemical ID','trim');
		$this->form_validation->set_rules('chemical_value[]','Chemical Value','trim');
		$this->form_validation->set_rules('test_status','Test Status','trim');
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("Nut incoming",'final_manager/final_nut_list',"icon-folder-open");
			$this->admin_library->add_breadcrumb("ค่าทดสอบ Chemical ของ Nut",current_url(),"icon-plus");
			$this->_data['row'] = $this->chemical_test_final_model->get_chemical_nut($incomeid);
			$this->_data['thicknesslist'] = $this->chemical_test_final_model->get_thickness();
			$this->_data['testthicknessinfo'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_nut($incomeid);
			$this->_data['final_info'] = $this->chemical_test_final_model->get_finalinfo_bolt_byid($finalid);

			$this->admin_library->view("chemical_test_final/test_nut", $this->_data);
			$this->admin_library->output();
		}else{
			
			/* Update Final - Start */
			if($this->_data['tested_status']<=0){
				$this->chemical_test_final_model->save_final_chemical_nut($incomeid, $finalid);
			}else{
				$this->chemical_test_final_model->update_final_chemical_nut($finalid);
			}
			/* Update Final - End */
			
			/* Save Cer - Start */
			//$this->chemical_test_final_model->save_cer_chemical_nut($finalid);
			/* Save Cer - End */
			
			/* Update Thickness final - Start */
			if($this->_data['tested_status']<=0){
				$this->chemical_test_final_model->save_final_thickness_nut($incomeid, $finalid);
			}else{
				$this->chemical_test_final_model->update_final_thickness_nut($finalid);
			}
			/* Update Thickness final - End */
			
			/* Update Thickness cer - Start */
			//$this->chemical_test_final_model->save_cer_thickness_nut($finalid);
			/* Update Thickness cer - End */
			
			/* Update test status - Start */
			$this->chemical_test_final_model->update_test_status_bolt_nut($finalid);
			/* Update test status - End */
			
			/* Update stock - Start */
			$this->chemical_test_final_model->update_stock_bolt_nut($incomeid, $finalid, $this->input->post('test_status'));
			/* Update stock - End */
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("final_manager/final_bolt_list/");
		}
		
	}
	
	public function test_washer($incomeid=0, $finalid=0){
		
		$this->admin_model->set_menu_key('8a11fc1831fdfa2b49e8203ff134f4ec');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->_data['tested_status'] = $this->chemical_test_final_model->check_test_washer_status($incomeid, $finalid);
		
		$this->form_validation->set_rules('test_value[]','Value','trim');
		$this->form_validation->set_rules('chemical_id[]','Chemical ID','trim');
		$this->form_validation->set_rules('chemical_value[]','Chemical Value','trim');
		$this->form_validation->set_rules('test_status','Test Status','trim');
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("Washer incoming",'final_manager/final_washer_list',"icon-folder-open");
			$this->admin_library->add_breadcrumb("ค่าทดสอบ Chemical ของ Washer",current_url(),"icon-plus");
			$this->_data['row'] = $this->chemical_test_final_model->get_chemical_washer($incomeid);
			$this->_data['thicknesslist'] = $this->chemical_test_final_model->get_thickness();
			$this->_data['testthicknessinfo'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_washer($incomeid);
			$this->_data['final_info'] = $this->chemical_test_final_model->get_finalinfo_bolt_byid($finalid);
			
			$this->admin_library->view("chemical_test_final/test_washer", $this->_data);
			$this->admin_library->output();
		}else{
			
			/* Update Final - Start */
			if($this->_data['tested_status']<=0){
				$this->chemical_test_final_model->save_final_chemical_washer($incomeid, $finalid);
			}else{
				$this->chemical_test_final_model->update_final_chemical_washer($finalid);
			}
			/* Update Final - End */
			
			/* Save Cer - Start */
			//$this->chemical_test_final_model->save_cer_chemical_washer($finalid);
			/* Save Cer - End */
			
			/* Update Thickness final - Start */
			if($this->_data['tested_status']<=0){
				$this->chemical_test_final_model->save_final_thickness_washer($incomeid, $finalid);
			}else{
				$this->chemical_test_final_model->update_final_thickness_washer($finalid);
			}
			/* Update Thickness final - End */
			
			/* Update Thickness cer - Start */
			//$this->chemical_test_final_model->save_cer_thickness_washer($finalid);
			/* Update Thickness cer - End */
			
			/* Update test status - Start */
			$this->chemical_test_final_model->update_test_status_bolt_washer($finalid);
			/* Update test status - End */
			
			/* Update stock - Start */
			$this->chemical_test_final_model->update_stock_bolt_washer($incomeid, $finalid, $this->input->post('test_status'));
			/* Update stock - End */
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("final_manager/final_bolt_list/");
		}
		
	}
	
	public function test_spring($incomeid=0, $finalid=0){
		
		$this->admin_model->set_menu_key('8a11fc1831fdfa2b49e8203ff134f4ec');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->_data['tested_status'] = $this->chemical_test_final_model->check_test_spring_status($incomeid, $finalid);
		
		$this->form_validation->set_rules('test_value[]','Value','trim');
		$this->form_validation->set_rules('chemical_id[]','Chemical ID','trim');
		$this->form_validation->set_rules('chemical_value[]','Chemical Value','trim');
		$this->form_validation->set_rules('test_status','Test Status','trim');
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("Spring Washer incoming",'final_manager/final_spring_list',"icon-folder-open");
			$this->admin_library->add_breadcrumb("ค่าทดสอบ Chemical ของ Spring Washer",current_url(),"icon-plus");
			$this->_data['row'] = $this->chemical_test_final_model->get_chemical_spring($incomeid);
			$this->_data['thicknesslist'] = $this->chemical_test_final_model->get_thickness();
			$this->_data['testthicknessinfo'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_spring($incomeid);
			
			$this->admin_library->view("chemical_test_final/test_spring", $this->_data);
			$this->admin_library->output();
		}else{
			/* Update Final - Start */
			if($this->_data['tested_status']<=0){
				$this->chemical_test_final_model->save_final_chemical_spring($incomeid, $finalid);
			}else{
				$this->chemical_test_final_model->update_final_chemical_spring($finalid);
			}
			/* Update Final - End */
			
			/* Save Cer - Start */
			//$this->chemical_test_final_model->save_cer_chemical_spring($finalid);
			/* Save Cer - End */
			
			/* Update Thickness final - Start */
			if($this->_data['tested_status']<=0){
				$this->chemical_test_final_model->save_final_thickness_spring($incomeid, $finalid);
			}else{
				$this->chemical_test_final_model->update_final_thickness_spring($finalid);
			}
			/* Update Thickness final - End */
			
			/* Update Thickness cer - Start */
			//$this->chemical_test_final_model->save_cer_thickness_spring($finalid);
			/* Update Thickness cer - End */
			
			/* Update stock - Start */
			if($this->input->post('test_status')==2 && $this->_data['tested_status']<=0){
				$this->chemical_test_final_model->update_stock_spring($incomeid, $finalid);
			}
			/* Update stock - End */
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("final_manager/final_bolt_list/");
		}
		
	}
	
	public function test_taper($incomeid=0, $finalid=0){
		
		$this->admin_model->set_menu_key('8a11fc1831fdfa2b49e8203ff134f4ec');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->_data['tested_status'] = $this->chemical_test_final_model->check_test_taper_status($incomeid, $finalid);
		
		$this->form_validation->set_rules('test_value[]','Value','trim');
		$this->form_validation->set_rules('chemical_id[]','Chemical ID','trim');
		$this->form_validation->set_rules('chemical_value[]','Chemical Value','trim');
		$this->form_validation->set_rules('test_status','Test Status','trim');
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("Taper Washer incoming",'final_manager/final_taper_list',"icon-folder-open");
			$this->admin_library->add_breadcrumb("ค่าทดสอบ Chemical ของ Taper Washer",current_url(),"icon-plus");
			$this->_data['row'] = $this->chemical_test_final_model->get_chemical_taper($incomeid);
			$this->_data['thicknesslist'] = $this->chemical_test_final_model->get_thickness();
			$this->_data['testthicknessinfo'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_taper($incomeid);
			
			$this->admin_library->view("chemical_test_final/test_taper", $this->_data);
			$this->admin_library->output();
		}else{
			/* Update Final - Start */
			if($this->_data['tested_status']<=0){
				$this->chemical_test_final_model->save_final_chemical_taper($incomeid, $finalid);
			}else{
				$this->chemical_test_final_model->update_final_chemical_taper($finalid);
			}
			/* Update Final - End */
			
			/* Save Cer - Start */
			//$this->chemical_test_final_model->save_cer_chemical_taper($finalid);
			/* Save Cer - End */
			
			/* Update Thickness final - Start */
			if($this->_data['tested_status']<=0){
				$this->chemical_test_final_model->save_final_thickness_taper($incomeid, $finalid);
			}else{
				$this->chemical_test_final_model->update_final_thickness_taper($finalid);
			}
			/* Update Thickness final - End */
			
			/* Update Thickness cer - Start */
			//$this->chemical_test_final_model->save_cer_thickness_taper($finalid);
			/* Update Thickness cer - End */
			
			/* Update stock - Start */
			if($this->input->post('test_status')==2 && $this->_data['tested_status']<=0){
				$this->chemical_test_final_model->update_stock_taper($incomeid, $finalid);
			}
			/* Update stock - End */
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("final_manager/final_bolt_list/");
		}
		
	}
	
	public function test_stud($incomeid=0, $finalid=0){
		
		$this->admin_model->set_menu_key('8a11fc1831fdfa2b49e8203ff134f4ec');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->_data['tested_status'] = $this->chemical_test_final_model->check_test_stud_status($incomeid, $finalid);
		
		$this->form_validation->set_rules('test_value[]','Value','trim');
		$this->form_validation->set_rules('chemical_id[]','Chemical ID','trim');
		$this->form_validation->set_rules('chemical_value[]','Chemical Value','trim');
		$this->form_validation->set_rules('test_status','Test Status','trim');
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("Stud incoming",'final_manager/final_stud_list',"icon-folder-open");
			$this->admin_library->add_breadcrumb("ค่าทดสอบ Chemical ของ Stud",current_url(),"icon-plus");
			$this->_data['row'] = $this->chemical_test_final_model->get_chemical_stud($incomeid);
			$this->_data['thicknesslist'] = $this->chemical_test_final_model->get_thickness();
			$this->_data['testthicknessinfo'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_stud($incomeid);
			
			$this->admin_library->view("chemical_test_final/test_stud", $this->_data);
			$this->admin_library->output();
		}else{
			/* Update Final - Start */
			if($this->_data['tested_status']<=0){
				$this->chemical_test_final_model->save_final_chemical_stud($incomeid, $finalid);
			}else{
				$this->chemical_test_final_model->update_final_chemical_stud($finalid);
			}
			/* Update Final - End */
			
			/* Save Cer - Start */
			//$this->chemical_test_final_model->save_cer_chemical_stud($finalid);
			/* Save Cer - End */
			
			/* Update Thickness final - Start */
			if($this->_data['tested_status']<=0){
				$this->chemical_test_final_model->save_final_thickness_stud($incomeid, $finalid);
			}else{
				$this->chemical_test_final_model->update_final_thickness_stud($finalid);
			}
			/* Update Thickness final - End */
			
			/* Update Thickness cer - Start */
			//$this->chemical_test_final_model->save_cer_thickness_stud($finalid);
			/* Update Thickness cer - End */
			
			/* Update stock - Start */
			if($this->input->post('test_status')==2 && $this->_data['tested_status']<=0){
				$this->chemical_test_final_model->update_stock_stud($incomeid, $finalid);
			}
			/* Update stock - End */
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("final_manager/final_stud_list/");
		}
		
	}
	
	public function get_thickness_value(){
		$thicknessid = $this->input->post('thickness_id');
		$thicknessinfo = $this->chemical_test_final_model->get_thickness_value_byid($thicknessid);
		if($thicknessinfo){
			echo $thicknessinfo['condition'].' '.$thicknessinfo['condition_value'];
		}
	}
	
	public function debug($incomeid=0, $finalid=0){
		$this->chemical_test_final_model->update_stock_nut($incomeid, $finalid);
	}
	
	function test_main_nut($incomeid=0, $finalid=0){
		$this->admin_model->set_menu_key('0167c858306624390a0c7ac5041aadb1');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->_data['tested_status'] = $this->chemical_test_final_model->check_test_nut_main_status($incomeid, $finalid);
		
		$this->form_validation->set_rules('test_value[]','Value','trim');
		$this->form_validation->set_rules('chemical_id[]','Chemical ID','trim');
		$this->form_validation->set_rules('chemical_value[]','Chemical Value','trim');
		$this->form_validation->set_rules('test_status','Test Status','trim');
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("Nut incoming",'final_manager/final_nut_list',"icon-folder-open");
			$this->admin_library->add_breadcrumb("ค่าทดสอบ Chemical ของ Nut",current_url(),"icon-plus");
			$this->_data['row'] = $this->chemical_test_final_model->get_chemical_nut($incomeid);
			$this->_data['thicknesslist'] = $this->chemical_test_final_model->get_thickness();
			$this->_data['testthicknessinfo'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_nut($incomeid);
			
			$this->admin_library->view("chemical_test_final/test_main_nut", $this->_data);
			$this->admin_library->output();
		}else{
		
			/* Update Final - Start */
			if($this->_data['tested_status']<=0){
				$this->chemical_test_final_model->save_final_chemical_nut($incomeid, $finalid);
			}else{
				$this->chemical_test_final_model->update_final_chemical_nut($finalid);
			}
			/* Update Final - End */
			
			/* Save Cer - Start */
			//$this->chemical_test_final_model->save_cer_chemical_nut($finalid);
			/* Save Cer - End */
			
			/* Update Thickness final - Start */
			if($this->_data['tested_status']<=0){
				$this->chemical_test_final_model->save_final_thickness_nut($incomeid, $finalid);
			}else{
				$this->chemical_test_final_model->update_final_thickness_nut($finalid);
			}
			/* Update Thickness final - End */
			
			/* Update Thickness cer - Start */
			//$this->chemical_test_final_model->save_cer_thickness_nut($finalid);
			/* Update Thickness cer - End */
			
			/* Update stock - Start */
			if($this->input->post('test_status')==2 && $this->_data['tested_status']<=0){
				$this->chemical_test_final_model->update_stock_main_nut($incomeid, $finalid);
			}
			
			$this->chemical_test_final_model->update_status_nut($incomeid, $finalid, $this->input->post('test_status'));
			/* Update stock - End */
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("final_manager/final_nut_list/");
		}
	}
	
	public function test_main_washer($incomeid=0, $finalid=0){
		
		$this->admin_model->set_menu_key('ce01da7485b1b183e6b32a08d4d64f47');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->_data['tested_status'] = $this->chemical_test_final_model->check_test_washer_main_status($incomeid, $finalid);
		
		$this->form_validation->set_rules('test_value[]','Value','trim');
		$this->form_validation->set_rules('chemical_id[]','Chemical ID','trim');
		$this->form_validation->set_rules('chemical_value[]','Chemical Value','trim');
		$this->form_validation->set_rules('test_status','Test Status','trim');
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("Washer incoming",'final_manager/final_washer_list',"icon-folder-open");
			$this->admin_library->add_breadcrumb("ค่าทดสอบ Chemical ของ Washer",current_url(),"icon-plus");
			$this->_data['row'] = $this->chemical_test_final_model->get_chemical_washer($incomeid);
			$this->_data['thicknesslist'] = $this->chemical_test_final_model->get_thickness();
			$this->_data['testthicknessinfo'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_washer($incomeid);
			
			$this->admin_library->view("chemical_test_final/test_main_washer", $this->_data);
			$this->admin_library->output();
		}else{
			/* Update Final - Start */
			if($this->_data['tested_status']<=0){
				$this->chemical_test_final_model->save_final_chemical_washer($incomeid, $finalid);
			}else{
				$this->chemical_test_final_model->update_final_chemical_washer($finalid);
			}
			/* Update Final - End */
			
			/* Save Cer - Start */
			//$this->chemical_test_final_model->save_cer_chemical_washer($finalid);
			/* Save Cer - End */
			
			/* Update Thickness final - Start */
			if($this->_data['tested_status']<=0){
				$this->chemical_test_final_model->save_final_thickness_washer($incomeid, $finalid);
			}else{
				$this->chemical_test_final_model->update_final_thickness_washer($finalid);
			}
			/* Update Thickness final - End */
			
			/* Update Thickness cer - Start */
			//$this->chemical_test_final_model->save_cer_thickness_washer($finalid);
			/* Update Thickness cer - End */
			
			/* Update stock - Start */
			if($this->input->post('test_status')==2 && $this->_data['tested_status']<=0){
				$this->chemical_test_final_model->update_stock_main_washer($incomeid, $finalid);
			}
			
			$this->chemical_test_final_model->update_status_washer($incomeid, $finalid, $this->input->post('test_status'));
			/* Update stock - End */
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("final_manager/final_washer_list/");
		}
		
	}
	
	public function test_main_spring($incomeid=0, $finalid=0){
		
		$this->admin_model->set_menu_key('cf6778ab14ce1643575cb1951b77a926');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->_data['tested_status'] = $this->chemical_test_final_model->check_test_spring_main_status($incomeid, $finalid);
		
		$this->form_validation->set_rules('test_value[]','Value','trim');
		$this->form_validation->set_rules('chemical_id[]','Chemical ID','trim');
		$this->form_validation->set_rules('chemical_value[]','Chemical Value','trim');
		$this->form_validation->set_rules('test_status','Test Status','trim');
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("Spring Washer incoming",'final_manager/final_spring_list',"icon-folder-open");
			$this->admin_library->add_breadcrumb("ค่าทดสอบ Chemical ของ Spring Washer",current_url(),"icon-plus");
			$this->_data['row'] = $this->chemical_test_final_model->get_chemical_spring($incomeid);
			$this->_data['thicknesslist'] = $this->chemical_test_final_model->get_thickness();
			$this->_data['testthicknessinfo'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_spring($incomeid);
			
			$this->admin_library->view("chemical_test_final/test_main_spring", $this->_data);
			$this->admin_library->output();
		}else{
			/* Update Final - Start */
			if($this->_data['tested_status']<=0){
				$this->chemical_test_final_model->save_final_chemical_spring($incomeid, $finalid);
			}else{
				$this->chemical_test_final_model->update_final_chemical_spring($finalid);
			}
			/* Update Final - End */
			
			/* Save Cer - Start */
			//$this->chemical_test_final_model->save_cer_chemical_spring($finalid);
			/* Save Cer - End */
			
			/* Update Thickness final - Start */
			if($this->_data['tested_status']<=0){
				$this->chemical_test_final_model->save_final_thickness_spring($incomeid, $finalid);
			}else{
				$this->chemical_test_final_model->update_final_thickness_spring($finalid);
			}
			/* Update Thickness final - End */
			
			/* Update Thickness cer - Start */
			//$this->chemical_test_final_model->save_cer_thickness_spring($finalid);
			/* Update Thickness cer - End */
			
			/* Update stock - Start */
			if($this->input->post('test_status')==2 && $this->_data['tested_status']<=0){
				$this->chemical_test_final_model->update_stock_main_spring($incomeid, $finalid);
			}
			
			$this->chemical_test_final_model->update_status_spring($incomeid, $finalid, $this->input->post('test_status'));
			/* Update stock - End */
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("final_manager/final_spring_list/");
		}
		
	}
	
	public function test_main_taper($incomeid=0, $finalid=0){
		
		$this->admin_model->set_menu_key('ce01da7485b1b183e6b32a08d4d64f47');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->_data['tested_status'] = $this->chemical_test_final_model->check_test_taper_main_status($incomeid, $finalid);
		
		$this->form_validation->set_rules('test_value[]','Value','trim');
		$this->form_validation->set_rules('chemical_id[]','Chemical ID','trim');
		$this->form_validation->set_rules('chemical_value[]','Chemical Value','trim');
		$this->form_validation->set_rules('test_status','Test Status','trim');
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("Taper Washer incoming",'final_manager/final_taper_list',"icon-folder-open");
			$this->admin_library->add_breadcrumb("ค่าทดสอบ Chemical ของ Taper Washer",current_url(),"icon-plus");
			$this->_data['row'] = $this->chemical_test_final_model->get_chemical_taper($incomeid);
			$this->_data['thicknesslist'] = $this->chemical_test_final_model->get_thickness();
			$this->_data['testthicknessinfo'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_taper($incomeid);
			
			$this->admin_library->view("chemical_test_final/test_main_taper", $this->_data);
			$this->admin_library->output();
		}else{
			/* Update Final - Start */
			if($this->_data['tested_status']<=0){
				$this->chemical_test_final_model->save_final_chemical_taper($incomeid, $finalid);
			}else{
				$this->chemical_test_final_model->update_final_chemical_taper($finalid);
			}
			/* Update Final - End */
			
			/* Save Cer - Start */
			//$this->chemical_test_final_model->save_cer_chemical_taper($finalid);
			/* Save Cer - End */
			
			/* Update Thickness final - Start */
			if($this->_data['tested_status']<=0){
				$this->chemical_test_final_model->save_final_thickness_taper($incomeid, $finalid);
			}else{
				$this->chemical_test_final_model->update_final_thickness_taper($finalid);
			}
			/* Update Thickness final - End */
			
			/* Update Thickness cer - Start */
			//$this->chemical_test_final_model->save_cer_thickness_taper($finalid);
			/* Update Thickness cer - End */
			
			/* Update stock - Start */
			if($this->input->post('test_status')==2 && $this->_data['tested_status']<=0){
				$this->chemical_test_final_model->update_stock_main_taper($incomeid, $finalid);
			}
			
			$this->chemical_test_final_model->update_status_taper($incomeid, $finalid, $this->input->post('test_status'));
			/* Update stock - End */
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("final_manager/final_taper_list/");
		}
		
	}
	
	public function test_main_stud($incomeid=0, $finalid=0){
		
		$this->admin_model->set_menu_key('d18409cc6d89636957588a0b5db34553');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->_data['tested_status'] = $this->chemical_test_final_model->check_test_stud_main_status($incomeid, $finalid);
		
		$this->form_validation->set_rules('test_value[]','Value','trim');
		$this->form_validation->set_rules('chemical_id[]','Chemical ID','trim');
		$this->form_validation->set_rules('chemical_value[]','Chemical Value','trim');
		$this->form_validation->set_rules('test_status','Test Status','trim');
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("Stud incoming",'final_manager/final_stud_list',"icon-folder-open");
			$this->admin_library->add_breadcrumb("ค่าทดสอบ Chemical ของ Stud",current_url(),"icon-plus");
			$this->_data['row'] = $this->chemical_test_final_model->get_chemical_stud($incomeid);
			$this->_data['thicknesslist'] = $this->chemical_test_final_model->get_thickness();
			$this->_data['testthicknessinfo'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_stud($incomeid);
			
			$this->admin_library->view("chemical_test_final/test_main_stud", $this->_data);
			$this->admin_library->output();
		}else{
			/* Update Final - Start */
			if($this->_data['tested_status']<=0){
				$this->chemical_test_final_model->save_final_chemical_stud($incomeid, $finalid);
			}else{
				$this->chemical_test_final_model->update_final_chemical_stud($finalid);
			}
			/* Update Final - End */
			
			/* Save Cer - Start */
			//$this->chemical_test_final_model->save_cer_chemical_stud($finalid);
			/* Save Cer - End */
			
			/* Update Thickness final - Start */
			if($this->_data['tested_status']<=0){
				$this->chemical_test_final_model->save_final_thickness_stud($incomeid, $finalid);
			}else{
				$this->chemical_test_final_model->update_final_thickness_stud($finalid);
			}
			/* Update Thickness final - End */
			
			/* Update Thickness cer - Start */
			//$this->chemical_test_final_model->save_cer_thickness_stud($finalid);
			/* Update Thickness cer - End */
			
			/* Update stock - Start */
			if($this->input->post('test_status')==2 && $this->_data['tested_status']<=0){
				$this->chemical_test_final_model->update_stock_main_stud($incomeid, $finalid);
			}
			
			$this->chemical_test_final_model->update_status_stud($incomeid, $finalid, $this->input->post('test_status'));
			/* Update stock - End */
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("final_manager/final_stud_list/");
		}
		
	}
	
}