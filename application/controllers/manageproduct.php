<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manageproduct extends CI_Controller {
	var $seq = 0;
	var $_data=array();
	function __construct(){
		parent::__construct();
		
		$this->load->library('admin_library');
		$this->load->model('administrator/admin_model');
		$this->load->model('administrator/menu_model');
		$this->load->model('administrator/product_model');
		$this->admin_library->forceLogin();
	}
	
	/* Bolt - Start */
	public function bolt_list($page=1){
		
		$page = ((int) $page < 1)?1:(int) $page;
		$offset = ($page-1)*25;
		$this->seq=$offset;
		$this->admin_model->set_menu_key('6b179b4b922e0a2da76762d79f761ff5');
		$this->admin_library->setTitle("Bolt Manager",'icon-dashboard'); 
		$this->admin_library->setDetail("Bolt certificate manager");
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		
		$this->admin_model->set_top_button("เพิ่ม Bolt","manageproduct/bolt_add",'icon-plus','btn-success','w');
		/* $this->admin_model->set_top_button("สำรองข้อมูล","menu_manager/backup",'icon-download','btn-primary','s'); */
		$this->admin_model->set_datatable($this->product_model->bolt_dataTable(25,$offset));
		$this->admin_model->set_column("product_no","ลำดับ",'5%');
		$this->admin_model->set_column("product_thumbnail","รูปภาพ",'15%');
		$this->admin_model->set_column("product_name","ชื่อสินค้า",'');
		//$this->admin_model->set_column("product_standard","Standard",'50%');
		$this->admin_model->set_column_callback('product_thumbnail','get_image');
		$this->admin_model->set_column_callback("product_no","show_seq");
		$this->admin_model->set_column_callback('product_name','show_type');
		
		$this->admin_model->set_action_button("แก้ไข","manageproduct/bolt_edit/[product_id]",'icon-edit','btn-info','w');
		
		$this->admin_model->set_action_button("Standard","manageproduct/bolt_standard/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("Size","manageproduct/bolt_size_list/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("Chemical","manageproduct/bolt_chemical/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("ลบ","manageproduct/bolt_delete/[product_id]",'icon-trash-o','btn-danger','d');
		$this->admin_model->show_search_text("manageproduct/bolt_list");
		$this->admin_model->set_pagination("manageproduct/bolt_list",$this->product_model->bolt_count(),25,3);
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function bolt_add(){
		
		$this->admin_model->set_menu_key('6b179b4b922e0a2da76762d79f761ff5');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('product_name','ชื่อสินค้า','trim|required');
		$this->form_validation->set_rules('certificate_id','Certificate','trim|required');
		$this->form_validation->set_rules('product_type','ประเภท','trim|required');
		$this->form_validation->set_message('required', '"%s" is required.');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("การจัดการ Bolt","manageproduct/bolt_list/","icon-folder-open");
			$this->admin_library->add_breadcrumb("เพิ่ม Bolt","manageproduct/bolt_add","icon-plus");
			$this->admin_library->view("manageproduct/bolt_add");
			$this->admin_library->output();
		}else{
			$this->product_model->bolt_add();
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manageproduct/bolt_list");
		}
	}
	
	public function bolt_edit($productid=0){
		
		$this->admin_model->set_menu_key('6b179b4b922e0a2da76762d79f761ff5');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$this->_data['row'] = $this->product_model->bolt_getinfo($productid);
		$this->admin_library->setTitle("Edit Bolt ",'icon-pencil'); 
		$this->admin_library->setDetail("แก้ไขข้อมูล เกรด ".$this->_data['row']['product_name']);
		
		$this->form_validation->set_rules('product_name','ชื่อเกรด','trim|required');
		$this->form_validation->set_rules('certificate_id','Certificate','trim|required');
		$this->form_validation->set_rules('product_type','ประเภท','trim|required');
		$this->form_validation->set_message('required', '"%s" is required.');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("การจัดการ Bolt","manageproduct/bolt_list/","icon-folder-open");
			$this->admin_library->add_breadcrumb("แก้ไข Bolt","manageproduct/bolt_edit/".$productid,"icon-pencil");
			
			$this->admin_library->view("manageproduct/bolt_edit", $this->_data);
			$this->admin_library->output();
		}else{
			
			$pro_id = $this->product_model->bolt_edit($productid);
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manageproduct/bolt_list");
		}
	}
	
	public function product_boltstatus_link($text,$row){
		if($text=="approved"){
			return '<a href="'.admin_url("manageproduct/bolt_setstatus/pending/".$row['product_id']).'">'.ucfirst($text).'</a>';
		}else{
			return '<a href="'.admin_url("manageproduct/bolt_setstatus/approved/".$row['product_id']).'">'.ucfirst($text).'</a>';
		}
	}
	
	public function bolt_delete($productid=0){
		$this->product_model->bolt_delete($productid);
		$this->session->set_flashdata("message-success",$message);
		admin_redirect("manageproduct/bolt_list");
	}
	
	public function bolt_size_list($productid=0){
		$this->admin_model->set_menu_key('6b179b4b922e0a2da76762d79f761ff5');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('รายการสินค้า Bolt','manageproduct/bolt_list','');
		$this->admin_library->add_breadcrumb('จัดการ Size',current_url(),'');
		
		$this->admin_model->set_top_button("เพิ่ม Bolt size","manageproduct/bolt_size_add/".$productid,'icon-plus','btn-success','w');
		$this->admin_model->set_top_button("กลับไปยังหน้ารายการสินค้า Bolt",'manageproduct/bolt_list','icon-reply','btn-primary','w');
		
		$this->admin_model->set_datatable($this->product_model->bolt_size_dataTable($productid));
		$this->admin_model->set_column("size_no","ลำดับ",0);
		$this->admin_model->set_column("size_m","M",0);
		$this->admin_model->set_column("size_p","P",0);
		$this->admin_model->set_column("size_length","Length",0);
		$this->admin_model->set_column_callback("size_no","show_seq");
		$this->admin_model->set_action_button("แก้ไข","manageproduct/bolt_size_edit/".$productid."/[size_id]",'icon-edit','btn-success','w');
		$this->admin_model->set_action_button("ลบ","manageproduct/bolt_size_delete/".$productid."/[size_id]",'icon-trash-o','btn-danger','d');
		
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function bolt_size_add($productid=0){
		
		$this->admin_model->set_menu_key('6b179b4b922e0a2da76762d79f761ff5');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('size_m','M','trim|required');
		$this->form_validation->set_rules('size_p','P','trim');
		$this->form_validation->set_rules('size_length','Length','trim');
		$this->form_validation->set_message('required', "%s is required.");
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("เพิ่ม Bolt size",current_url(),"icon-plus");
			$this->_data['row'] = array('product_id'=>$productid);
			$pro = $this->product_model->bolt_getinfo($productid);
			$this->_data['cer_list'] = $this->product_model->cer_size($pro['certificate_id']);
			
			
			$this->admin_library->view("manageproduct/bolt_size_add", $this->_data);
			$this->admin_library->output();
		}else{
			$this->product_model->bolt_size_add($productid);
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manageproduct/bolt_size_list/".$productid);
		}
		
	}
	
	public function bolt_size_edit($productid=0, $sizeid=0){
		
		$this->admin_model->set_menu_key('6b179b4b922e0a2da76762d79f761ff5');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('size_m','M','trim|required');
		$this->form_validation->set_rules('size_p','P','trim');
		$this->form_validation->set_rules('size_length','Length','trim');
		$this->form_validation->set_message('required', "%s is required.");
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("แก้ไข Bolt size",current_url(),"icon-plus");
			$this->_data['row'] = array('product_id'=>$productid);
			$pro = $this->product_model->bolt_getinfo($productid);
			$this->_data['sizeinfo'] = $this->product_model->get_sizebolt($sizeid);
			$this->_data['cer_list'] = $this->product_model->cer_size_foredit($sizeid);
			
			$this->admin_library->view("manageproduct/bolt_size_edit", $this->_data);
			$this->admin_library->output();
		}else{
			$this->product_model->bolt_size_edit($productid, $sizeid);
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manageproduct/bolt_size_list/".$productid);
		}
		
	}
	
	public function bolt_size_delete($productid=0){
		$this->product_model->bolt_size_delete($productid);
		$this->session->set_flashdata('message-success','ลบข้อมูลเรียบร้อยแล้ว');
		admin_redirect('manageproduct/bolt_size_list/'.$productid);
	}
	
	public function bolt_standard($productid=0){
		
		$this->admin_model->set_menu_key('6b179b4b922e0a2da76762d79f761ff5');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('standard_description[]','Description','trim');
		$this->form_validation->set_rules('ps_min[]','MIN','trim');
		$this->form_validation->set_rules('ps_min_unit[]','MIN unit','trim');
		$this->form_validation->set_rules('ps_max[]','MAX','trim');
		$this->form_validation->set_rules('ps_max_unit[]','MAX unit','trim');
		$this->form_validation->set_message('required','"%s" is required.');
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("การจัดการมาตรฐานของ Bolt",current_url(),"icon-plus");
			$boltlist = $this->product_model->bolt_standardlist($productid);
			if($boltlist){
				$this->_data['status'] = 'edit';
				$pro = $this->product_model->bolt_getinfo($productid);
				$this->_data['row'] = $this->product_model->get_cer_std_foredit($productid);
			}else{
				$this->_data['status'] = 'add';
				$pro = $this->product_model->bolt_getinfo($productid);
				$this->_data['row'] = $this->product_model->get_cer_std($pro['certificate_id']);
			}
			$this->admin_library->view("manageproduct/bolt_standard", $this->_data);
			$this->admin_library->output();
		}else{
			if($_POST['process_status']=='edit'){
				$this->product_model->bolt_standard_edit($productid);
			}else{
				$this->product_model->bolt_standard_add($productid);
			}
			
			$this->session->set_flashdata("message-success","<div class='alert alert-success'><button class='close' data-dismiss='alert'>x</button><strong>การทำรายการเสร็จสมบูรณ์</strong>บันทึกเรียบร้อยแล้ว</div>");
			admin_redirect("manageproduct/bolt_standard/".$productid);
		}
		
	}
	
	public function bolt_chemical($productid=0){
		
		$this->admin_model->set_menu_key('6b179b4b922e0a2da76762d79f761ff5');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('ps_min[]','Chemical value (%) [MIN]','trim');
		$this->form_validation->set_rules('ps_max[]','Chemical value (%) [MAX]','trim');
		/*
$this->form_validation->set_rules('chemical_value[]','Chemical (%)','trim');
		$this->form_validation->set_rules('chemical_composition[]','Composition','trim');
*/
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("การจัดการค่า Chemical ของ Bolt",current_url(),"icon-plus");
			$chemicallist = $this->product_model->bolt_chemicallist($productid);
			if($chemicallist){
				$this->_data['status'] = 'edit';
				$pro = $this->product_model->bolt_getinfo($productid);
				$this->_data['row'] = $this->product_model->get_cer_chem_foredit($productid);
			}else{
				$this->_data['status'] = 'add';
				$pro = $this->product_model->bolt_getinfo($productid);
				$this->_data['row'] = $this->product_model->get_cer_chem($pro['certificate_id']);
			}
			
			$this->admin_library->view('manageproduct/bolt_chemical', $this->_data);
			$this->admin_library->output();
		}else{
			if($_POST['process_status']=='edit'){
				$this->product_model->bolt_chemical_edit($productid);
			}else{
				$this->product_model->bolt_chemical_add($productid);	
			}
			$this->session->set_flashdata("message-success","<div class='alert alert-success'><button class='close' data-dismiss='alert'>x</button><strong>การทำรายการเสร็จสมบูรณ์</strong>บันทึกเรียบร้อยแล้ว</div>");
			admin_redirect("manageproduct/bolt_chemical/".$productid);
		}
		
	}
	/* Bolt - End */
	
	/* Nut - Start */
	public function nut_list($page=1){
		$page = ((int) $page < 1)?1:(int) $page;
		$offset = ($page-1)*25;
		$this->seq=$offset;
		
		$this->admin_model->set_menu_key('271fee0a6f4ff414ba77fa5ba74db5bf');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		
		$this->admin_model->set_top_button("เพิ่ม Nut","manageproduct/nut_add",'icon-plus','btn-success','w');
		/* $this->admin_model->set_top_button("สำรองข้อมูล","menu_manager/backup",'icon-download','btn-primary','s'); */
		$this->admin_model->set_datatable($this->product_model->nut_dataTable(25,$offset));
		$this->admin_model->set_column("product_no","ลำดับ",'5%');
		$this->admin_model->set_column("product_thumbnail","รูปภาพ",'15%');
		$this->admin_model->set_column("product_name","ชื่อสินค้า",'');
		$this->admin_model->set_column_callback('product_thumbnail','get_image_nut');
		$this->admin_model->set_column_callback("product_no","show_seq");
		$this->admin_model->set_column_callback("product_name","show_type");
		
		$this->admin_model->set_action_button("แก้ไข","manageproduct/nut_edit/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("Standard","manageproduct/nut_standard/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("Size","manageproduct/nut_size_list/[product_id]",'icon-edit','btn-info','w');		
		$this->admin_model->set_action_button("Chemical","manageproduct/nut_chemical/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("ลบ","manageproduct/nut_delete/[product_id]",'icon-trash-o','btn-danger','d');
		
		$this->admin_model->show_search_text("manageproduct/nut_list");
		$this->admin_model->set_pagination("manageproduct/nut_list",$this->product_model->nut_count(),25,3);
		
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function nut_add(){
		
		$this->admin_model->set_menu_key('271fee0a6f4ff414ba77fa5ba74db5bf');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('product_name','ชื่อสินค้า','trim|required');
		$this->form_validation->set_rules('certificate_id','Certificate','trim|required');
		$this->form_validation->set_rules('product_type','ประเภท','trim|required');
		$this->form_validation->set_message('required', '"%s" is required.');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("เพิ่ม Nut","manageproduct/nut_add","icon-plus");
			$this->admin_library->view("manageproduct/nut_add");
			$this->admin_library->output();
		}else{
			$this->product_model->nut_add();
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manageproduct/nut_list");
		}
	}
	
	public function nut_edit($productid=0){
		
		$this->admin_model->set_menu_key('271fee0a6f4ff414ba77fa5ba74db5bf');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('product_name','ชื่อสินค้า','trim|required');
		$this->form_validation->set_rules('certificate_id','Certificate','trim|required');
		$this->form_validation->set_rules('product_type','ประเภท','trim|required');
		$this->form_validation->set_message('required', '"%s" is required.');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("แก้ไข Nut","manageproduct/nut_edit/".$productid,"icon-plus");
			$this->_data['row'] = $this->product_model->nut_getinfo($productid);
			$this->admin_library->view("manageproduct/nut_edit", $this->_data);
			$this->admin_library->output();
		}else{
			
			$pro_id = $this->product_model->nut_edit($productid);
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manageproduct/nut_list");
		}
	}
	
	public function product_nutstatus_link($text,$row){
		if($text=="approved"){
			return '<a href="'.admin_url("manageproduct/nut_setstatus/pending/".$row['product_id']).'">'.ucfirst($text).'</a>';
		}else{
			return '<a href="'.admin_url("manageproduct/nut_setstatus/approved/".$row['product_id']).'">'.ucfirst($text).'</a>';
		}
	}
	
	public function nut_delete($productid=0){
		$this->product_model->nut_delete($productid);
		$this->session->set_flashdata("message-success",$message);
		admin_redirect("manageproduct/nut_list");
	}
	
	public function nut_size_list($productid=0){
		$this->admin_model->set_menu_key('271fee0a6f4ff414ba77fa5ba74db5bf');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('รายการสินค้า Nut','manageproduct/nut_list','');
		$this->admin_library->add_breadcrumb('จัดการ Size',current_url(),'');
		
		$this->admin_model->set_top_button("เพิ่ม Nut size","manageproduct/nut_size_add/".$productid,'icon-plus','btn-success','w');
		$this->admin_model->set_top_button("กลับไปยังหน้ารายการสินค้า Nut",'manageproduct/nut_list','icon-reply','btn-primary','w');
		
		$this->admin_model->set_datatable($this->product_model->nut_size_dataTable($productid));
		$this->admin_model->set_column("size_no","ลำดับ",0);
		$this->admin_model->set_column("size_m","M",0);
		$this->admin_model->set_column("size_p","P",0);
		$this->admin_model->set_column_callback("size_no","show_seq");
		$this->admin_model->set_action_button("แก้ไข","manageproduct/nut_size_edit/".$productid."/[size_id]",'icon-edit','btn-success','w');
		$this->admin_model->set_action_button("ลบ","manageproduct/nut_size_delete/".$productid."/[size_id]",'icon-trash-o','btn-danger','d');
		
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function nut_size_add($productid=0){
		
		$this->admin_model->set_menu_key('271fee0a6f4ff414ba77fa5ba74db5bf');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('size_m','M','trim|required');
		$this->form_validation->set_rules('size_p','P','trim');
		$this->form_validation->set_rules('size_length','Length','trim');
		$this->form_validation->set_message('required', "%s is required.");
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("เพิ่ม Nut size",current_url(),"icon-plus");
			$this->_data['row'] = array('product_id'=>$productid);
			$pro = $this->product_model->nut_getinfo($productid);
			$this->_data['cer_list'] = $this->product_model->cer_size($pro['certificate_id']);
			
			
			$this->admin_library->view("manageproduct/nut_size_add", $this->_data);
			$this->admin_library->output();
		}else{
			$this->product_model->nut_size_add($productid);
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manageproduct/nut_size_list/".$productid);
		}
		
	}
	
	public function nut_size_edit($productid=0, $sizeid=0){
		
		$this->admin_model->set_menu_key('271fee0a6f4ff414ba77fa5ba74db5bf');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('size_m','M','trim|required');
		$this->form_validation->set_rules('size_p','P','trim');
		$this->form_validation->set_rules('size_length','Length','trim');
		$this->form_validation->set_message('required', "%s is required.");
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("เพิ่ม Nut size",current_url(),"icon-plus");
			$this->_data['row'] = array('product_id'=>$productid);
			
			$this->_data['sizeinfo'] = $this->product_model->get_nutsizeinfo_byid($sizeid);
			$this->_data['cer_list'] = $this->product_model->nut_cer_size($sizeid);
			
			$this->admin_library->view("manageproduct/nut_size_edit", $this->_data);
			$this->admin_library->output();
		}else{
			$this->product_model->nut_size_edit($sizeid);
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manageproduct/nut_size_list/".$productid);
		}
		
	}
	
	public function nut_size_delete($productid=0){
		$this->product_model->nut_size_delete($productid);
		$this->session->set_flashdata('message-success','ลบข้อมูลเรียบร้อยแล้ว');
		admin_redirect('manageproduct/nut_size_list/'.$productid);
	}
	
	public function nut_standard($productid=0){
		
		$this->admin_model->set_menu_key('271fee0a6f4ff414ba77fa5ba74db5bf');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('standard_description[]','Description','trim');
		$this->form_validation->set_rules('ps_min[]','MIN','trim');
		$this->form_validation->set_rules('ps_min_unit[]','MIN unit','trim');
		$this->form_validation->set_rules('ps_max[]','MAX','trim');
		$this->form_validation->set_rules('ps_max_unit[]','MAX unit','trim');
		$this->form_validation->set_message('required','"%s" is required.');
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("การจัดการมาตรฐานของ Nut",current_url(),"icon-plus");
			$nutlist = $this->product_model->nut_standardlist($productid);
			if($nutlist){
				$this->_data['status'] = 'edit';
				$this->_data['row'] = $this->product_model->nut_get_cer_std_foredit($productid);
			}else{
				$this->_data['status'] = 'add';
				$pro = $this->product_model->nut_getinfo($productid);
				$this->_data['row'] = $this->product_model->get_cer_std($pro['certificate_id']);
			}
			$this->admin_library->view("manageproduct/nut_standard", $this->_data);
			$this->admin_library->output();
		}else{
			if($_POST['process_status']=='edit'){
				$this->product_model->nut_standard_edit($productid);
			}else{
				$this->product_model->nut_standard_add($productid);	
			}
			$this->session->set_flashdata("message-success","<div class='alert alert-success'><button class='close' data-dismiss='alert'>x</button><strong>การทำรายการเสร็จสมบูรณ์</strong>บันทึกเรียบร้อยแล้ว</div>");
			admin_redirect("manageproduct/nut_standard/".$productid);
		}
		
	}
	
	public function nut_chemical($productid=0){
		
		$this->admin_model->set_menu_key('271fee0a6f4ff414ba77fa5ba74db5bf');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('ps_min[]','Chemical value (%) [MIN]','trim');
		$this->form_validation->set_rules('ps_max[]','Chemical value (%) [MAX]','trim');
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("การจัดการค่า Chemical ของ Nut",current_url(),"icon-plus");
			$chemicallist = $this->product_model->nut_chemicallist($productid);
			if($chemicallist){
				$this->_data['status'] = 'edit';
				$this->_data['row'] = $this->product_model->nut_get_cer_chem($productid);
			}else{
				$this->_data['status'] = 'add';
				$pro = $this->product_model->nut_getinfo($productid);
				$this->_data['row'] = $this->product_model->get_cer_chem($pro['certificate_id']);
			}
			
			$this->admin_library->view('manageproduct/nut_chemical', $this->_data);
			$this->admin_library->output();
		}else{
			if($_POST['process_status']=='edit'){
				$this->product_model->nut_chemical_edit($productid);
			}else{
				$this->product_model->nut_chemical_add($productid);	
			}
			$this->session->set_flashdata("message-success","<div class='alert alert-success'><button class='close' data-dismiss='alert'>x</button><strong>การทำรายการเสร็จสมบูรณ์</strong>บันทึกเรียบร้อยแล้ว</div>");
			admin_redirect("manageproduct/nut_chemical/".$productid);
		}
		
	}
	/* Nut - End */
	
	/* Washer - Start */
	public function washer_list($page=1){
		$page = ((int) $page < 1)?1:(int) $page;
		$offset = ($page-1)*25;
		$this->seq=$offset;
		$this->admin_model->set_menu_key('0173b607ea68c4af5008a0942bb7e951');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		
		$this->admin_model->set_top_button("เพิ่ม Washer","manageproduct/washer_add",'icon-plus','btn-success','w');
		/* $this->admin_model->set_top_button("สำรองข้อมูล","menu_manager/backup",'icon-download','btn-primary','s'); */
		$this->admin_model->set_datatable($this->product_model->washer_dataTable(25,$offset));
		$this->admin_model->set_column("product_no","ลำดับ",'5%');
		$this->admin_model->set_column("product_thumbnail","รูปภาพ",'15%');
		$this->admin_model->set_column("product_name","ชื่อสินค้า",'');
		$this->admin_model->set_column_callback('product_thumbnail','get_image_washer');
		$this->admin_model->set_column_callback("product_no","show_seq");
		$this->admin_model->set_column_callback("product_name","show_type");
		
		$this->admin_model->set_action_button("แก้ไข","manageproduct/washer_edit/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("Standard","manageproduct/washer_standard/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("Size","manageproduct/washer_size_list/[product_id]",'icon-edit','btn-info','w');		
		$this->admin_model->set_action_button("Chemical","manageproduct/washer_chemical/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("ลบ","manageproduct/washer_delete/[product_id]",'icon-trash-o','btn-danger','d');
		$this->admin_model->show_search_text("manageproduct/washer_list");
		$this->admin_model->set_pagination("manageproduct/washer_list",$this->product_model->washer_count(),25,3);
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function washer_add(){
		
		$this->admin_model->set_menu_key('0173b607ea68c4af5008a0942bb7e951');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('product_name','ชื่อสินค้า','trim|required');
		$this->form_validation->set_rules('certificate_id','Certificate','trim|required');
		$this->form_validation->set_rules('product_type','ประเภท','trim|required');
		$this->form_validation->set_message('required', '"%s" is required.');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("เพิ่ม Washer","manageproduct/washer_add","icon-plus");
			$this->admin_library->view("manageproduct/washer_add");
			$this->admin_library->output();
		}else{
			$this->product_model->washer_add();
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manageproduct/washer_list");
		}
	}
	
	public function washer_edit($productid=0){
		
		$this->admin_model->set_menu_key('0173b607ea68c4af5008a0942bb7e951');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('product_name','ชื่อสินค้า','trim|required');
		$this->form_validation->set_rules('certificate_id','Certificate','trim|required');
		$this->form_validation->set_rules('product_type','ประเภท','trim|required');
		$this->form_validation->set_message('required', '"%s" is required.');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("แก้ไข Washer","manageproduct/washer_edit/".$productid,"icon-plus");
			$this->_data['row'] = $this->product_model->washer_getinfo($productid);
			$this->admin_library->view("manageproduct/washer_edit", $this->_data);
			$this->admin_library->output();
		}else{
			
			$pro_id = $this->product_model->washer_edit($productid);
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manageproduct/washer_list");
		}
	}
	
	public function product_washerstatus_link($text,$row){
		if($text=="approved"){
			return '<a href="'.admin_url("manageproduct/washer_setstatus/pending/".$row['product_id']).'">'.ucfirst($text).'</a>';
		}else{
			return '<a href="'.admin_url("manageproduct/washer_setstatus/approved/".$row['product_id']).'">'.ucfirst($text).'</a>';
		}
	}
	
	public function washer_delete($productid=0){
		$this->product_model->washer_delete($productid);
		$this->session->set_flashdata("message-success",$message);
		admin_redirect("manageproduct/washer_list");
	}
	
	public function washer_size_list($productid=0){
		$this->admin_model->set_menu_key('0173b607ea68c4af5008a0942bb7e951');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('รายการสินค้า Washer','manageproduct/washer_list','');
		$this->admin_library->add_breadcrumb('จัดการ Size',current_url(),'');
		
		$this->admin_model->set_top_button("เพิ่ม Washer size","manageproduct/washer_size_add/".$productid,'icon-plus','btn-success','w');
		$this->admin_model->set_top_button("กลับไปยังหน้ารายการสินค้า Washer",'manageproduct/washer_list','icon-reply','btn-primary','w');
		
		$this->admin_model->set_datatable($this->product_model->washer_size_dataTable($productid));
		$this->admin_model->set_column("size_no","ลำดับ",0);
		$this->admin_model->set_column("size_m","M",0);
		$this->admin_model->set_column("size_p","P",0);
		$this->admin_model->set_column_callback("size_no","show_seq");
		$this->admin_model->set_action_button("แก้ไข","manageproduct/washer_size_edit/".$productid."/[size_id]",'icon-edit','btn-success','w');
		$this->admin_model->set_action_button("ลบ","manageproduct/washer_size_delete/".$productid."/[size_id]",'icon-trash-o','btn-danger','d');
		
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function washer_size_add($productid=0){
		
		$this->admin_model->set_menu_key('0173b607ea68c4af5008a0942bb7e951');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('size_m','M','trim|required');
		$this->form_validation->set_rules('size_p','P','trim');
		$this->form_validation->set_rules('size_length','Length','trim');
		$this->form_validation->set_message('required', "%s is required.");
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("เพิ่ม Washer size",current_url(),"icon-plus");
			$this->_data['row'] = array('product_id'=>$productid);
			$pro = $this->product_model->washer_getinfo($productid);
			$this->_data['cer_list'] = $this->product_model->cer_size($pro['certificate_id']);
			
			
			$this->admin_library->view("manageproduct/washer_size_add", $this->_data);
			$this->admin_library->output();
		}else{
			$this->product_model->washer_size_add($productid);
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manageproduct/washer_size_list/".$productid);
		}
		
	}
	
	public function washer_size_edit($productid=0, $sizeid=0){
		
		$this->admin_model->set_menu_key('0173b607ea68c4af5008a0942bb7e951');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('size_m','M','trim|required');
		$this->form_validation->set_rules('size_p','P','trim');
		$this->form_validation->set_rules('size_length','Length','trim');
		$this->form_validation->set_message('required', "%s is required.");
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("เพิ่ม Washer size",current_url(),"icon-plus");
			$this->_data['row'] = array('product_id'=>$productid);
			$this->_data['sizeinfo'] = $this->product_model->get_washer_sizeinfo($sizeid);
			$this->_data['cer_list'] = $this->product_model->washer_cer_size($sizeid);
			
			
			$this->admin_library->view("manageproduct/washer_size_edit", $this->_data);
			$this->admin_library->output();
		}else{
			$this->product_model->washer_size_edit($sizeid);
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manageproduct/washer_size_list/".$productid);
		}
		
	}
	
	public function washer_size_delete($productid=0){
		$this->product_model->washer_size_delete($productid);
		$this->session->set_flashdata('message-success','ลบข้อมูลเรียบร้อยแล้ว');
		admin_redirect('manageproduct/washer_size_list/'.$productid);
	}
	
	public function washer_standard($productid=0){
		
		$this->admin_model->set_menu_key('0173b607ea68c4af5008a0942bb7e951');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('standard_description[]','Description','trim');
		$this->form_validation->set_rules('ps_min[]','MIN','trim');
		$this->form_validation->set_rules('ps_min_unit[]','MIN unit','trim');
		$this->form_validation->set_rules('ps_max[]','MAX','trim');
		$this->form_validation->set_rules('ps_max_unit[]','MAX unit','trim');
		$this->form_validation->set_message('required','"%s" is required.');
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("การจัดการมาตรฐานของ Washer",current_url(),"icon-plus");
			$washerlist = $this->product_model->washer_standardlist($productid);
			if($washerlist){
				$this->_data['status'] = 'edit';
				$this->_data['row'] = $this->product_model->washer_get_cer_std($productid);
			}else{
				$this->_data['status'] = 'add';
				$pro = $this->product_model->washer_getinfo($productid);
				$this->_data['row'] = $this->product_model->get_cer_std($pro['certificate_id']);
			}
			$this->admin_library->view("manageproduct/washer_standard", $this->_data);
			$this->admin_library->output();
		}else{
			if($_POST['process_status']=='edit'){
				$this->product_model->washer_standard_edit($productid);
			}else{
				$this->product_model->washer_standard_add($productid);	
			}
			
			$this->session->set_flashdata("message-success","<div class='alert alert-success'><button class='close' data-dismiss='alert'>x</button><strong>การทำรายการเสร็จสมบูรณ์</strong>บันทึกเรียบร้อยแล้ว</div>");
			admin_redirect("manageproduct/washer_standard/".$productid);
		}
		
	}
	
	public function washer_chemical($productid=0){
		
		$this->admin_model->set_menu_key('0173b607ea68c4af5008a0942bb7e951');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('ps_min[]','Chemical value (%) [MIN]','trim');
		$this->form_validation->set_rules('ps_max[]','Chemical value (%) [MAX]','trim');
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("การจัดการค่า Chemical ของ Washer",current_url(),"icon-plus");
			$chemicallist = $this->product_model->washer_chemicallist($productid);
			if($chemicallist){
				$this->_data['status'] = 'edit';
				$this->_data['row'] = $this->product_model->washer_get_cer_chem($productid);
			}else{
				$this->_data['status'] = 'add';
				$pro = $this->product_model->washer_getinfo($productid);
				$this->_data['row'] = $this->product_model->get_cer_chem($pro['certificate_id']);
			}
			
			$this->admin_library->view('manageproduct/washer_chemical', $this->_data);
			$this->admin_library->output();
		}else{
			if($_POST['process_status']=='edit'){
				$this->product_model->washer_chemical_edit($productid);
			}else{
				$this->product_model->washer_chemical_add($productid);	
			}
			
			$this->session->set_flashdata("message-success","<div class='alert alert-success'><button class='close' data-dismiss='alert'>x</button><strong>การทำรายการเสร็จสมบูรณ์</strong>บันทึกเรียบร้อยแล้ว</div>");
			admin_redirect("manageproduct/washer_chemical/".$productid);
		}
		
	}
	/* Washer - End */
	
	/* Spring - Start */
	public function spring_list($page=1){
		$page = ((int) $page < 1)?1:(int) $page;
		$offset = ($page-1)*25;
		$this->seq=$offset;
		$this->admin_model->set_menu_key('566924a662af93b51abbf510677fc7fe');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		
		$this->admin_model->set_top_button("เพิ่ม Spring Washer","manageproduct/spring_add",'icon-plus','btn-success','w');
		/* $this->admin_model->set_top_button("สำรองข้อมูล","menu_manager/backup",'icon-download','btn-primary','s'); */
		$this->admin_model->set_datatable($this->product_model->spring_dataTable(25,$offset));
		$this->admin_model->set_column("product_no","ลำดับ",'5%');
		$this->admin_model->set_column("product_thumbnail","รูปภาพ",'15%');
		$this->admin_model->set_column("product_name","ชื่อสินค้า",'');
		$this->admin_model->set_column_callback('product_thumbnail','get_image_spring');
		$this->admin_model->set_column_callback("product_no","show_seq");
		$this->admin_model->set_column_callback("product_name","show_type");
		
		$this->admin_model->set_action_button("แก้ไข","manageproduct/spring_edit/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("ลบ","manageproduct/spring_delete/[product_id]",'icon-trash-o','btn-danger','d');
		$this->admin_model->set_action_button("Size","manageproduct/spring_size_list/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("Standard","manageproduct/spring_standard/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("Chemical","manageproduct/spring_chemical/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->show_search_text("manageproduct/spring_list");
		$this->admin_model->set_pagination("manageproduct/spring_list",$this->product_model->spring_count(),25,3);
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function spring_add(){
		
		$this->admin_model->set_menu_key('566924a662af93b51abbf510677fc7fe');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('product_name','ชื่อสินค้า','trim|required');
		$this->form_validation->set_rules('certificate_id','Certificate','trim|required');
		$this->form_validation->set_rules('product_type','ประเภท','trim|required');
		$this->form_validation->set_message('required', '"%s" is required.');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("เพิ่ม Spring Washer","manageproduct/spring_add","icon-plus");
			$this->admin_library->view("manageproduct/spring_add");
			$this->admin_library->output();
		}else{
			$this->product_model->spring_add();
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manageproduct/spring_list");
		}
	}
	
	public function spring_edit($productid=0){
		
		$this->admin_model->set_menu_key('566924a662af93b51abbf510677fc7fe');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('product_name','ชื่อสินค้า','trim|required');
		$this->form_validation->set_rules('certificate_id','Certificate','trim|required');
		$this->form_validation->set_rules('product_type','ประเภท','trim|required');
		$this->form_validation->set_message('required', '"%s" is required.');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("แก้ไข Spring Washer","manageproduct/spring_edit/".$productid,"icon-plus");
			$this->_data['row'] = $this->product_model->spring_getinfo($productid);
			$this->admin_library->view("manageproduct/spring_edit", $this->_data);
			$this->admin_library->output();
		}else{
			
			$pro_id = $this->product_model->spring_edit($productid);
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manageproduct/spring_list");
		}
	}
	
	public function product_springstatus_link($text,$row){
		if($text=="approved"){
			return '<a href="'.admin_url("manageproduct/spring_setstatus/pending/".$row['product_id']).'">'.ucfirst($text).'</a>';
		}else{
			return '<a href="'.admin_url("manageproduct/spring_setstatus/approved/".$row['product_id']).'">'.ucfirst($text).'</a>';
		}
	}
	
	public function spring_delete($productid=0){
		$this->product_model->spring_delete($productid);
		$this->session->set_flashdata("message-success",$message);
		admin_redirect("manageproduct/spring_list");
	}
	
	public function spring_size_list($productid=0){
		$this->admin_model->set_menu_key('566924a662af93b51abbf510677fc7fe');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('รายการสินค้า Spring Washer','manageproduct/spring_list','');
		$this->admin_library->add_breadcrumb('จัดการ Size',current_url(),'');
		
		$this->admin_model->set_top_button("เพิ่ม Spring Washer size","manageproduct/spring_size_add/".$productid,'icon-plus','btn-success','w');
		$this->admin_model->set_top_button("กลับไปยังหน้ารายการสินค้า Spring Washer",'manageproduct/spring_list','icon-reply','btn-primary','w');
		
		$this->admin_model->set_datatable($this->product_model->spring_size_dataTable($productid));
		$this->admin_model->set_column("size_no","ลำดับ",0);
		$this->admin_model->set_column("size_m","M",0);
		$this->admin_model->set_column("size_p","P",0);
		$this->admin_model->set_column_callback("size_no","show_seq");
		$this->admin_model->set_action_button("แก้ไข","manageproduct/spring_size_edit/".$productid."/[size_id]",'icon-edit','btn-success','w');
		$this->admin_model->set_action_button("ลบ","manageproduct/spring_size_delete/".$productid."/[size_id]",'icon-trash-o','btn-danger','d');
		
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function spring_size_add($productid=0){
		
		$this->admin_model->set_menu_key('566924a662af93b51abbf510677fc7fe');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('size_m','M','trim|required');
		$this->form_validation->set_rules('size_p','P','trim');
		$this->form_validation->set_rules('size_length','Length','trim');
		$this->form_validation->set_message('required', "%s is required.");
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("เพิ่ม Spring Washer size",current_url(),"icon-plus");
			$this->_data['row'] = array('product_id'=>$productid);
			$pro = $this->product_model->spring_getinfo($productid);
			$this->_data['cer_list'] = $this->product_model->cer_size($pro['certificate_id']);
			
			
			$this->admin_library->view("manageproduct/spring_size_add", $this->_data);
			$this->admin_library->output();
		}else{
			$this->product_model->spring_size_add($productid);
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manageproduct/spring_size_list/".$productid);
		}
		
	}
	
	public function spring_size_edit($productid=0, $sizeid=0){
		
		$this->admin_model->set_menu_key('566924a662af93b51abbf510677fc7fe');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('size_m','M','trim|required');
		$this->form_validation->set_rules('size_p','P','trim');
		$this->form_validation->set_rules('size_length','Length','trim');
		$this->form_validation->set_message('required', "%s is required.");
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("เพิ่ม Spring Washer size",current_url(),"icon-plus");
			$this->_data['row'] = array('product_id'=>$productid);
			$this->_data['sizeinfo'] = $this->product_model->get_spring_sizeinfo($sizeid);
			$this->_data['cer_list'] = $this->product_model->spring_cer_size($sizeid);
			
			
			$this->admin_library->view("manageproduct/spring_size_edit", $this->_data);
			$this->admin_library->output();
		}else{
			$this->product_model->spring_size_edit($sizeid);
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manageproduct/spring_size_list/".$productid);
		}
		
	}
	
	public function spring_size_delete($productid=0){
		$this->product_model->spring_size_delete($productid);
		$this->session->set_flashdata('message-success','ลบข้อมูลเรียบร้อยแล้ว');
		admin_redirect('manageproduct/spring_size_list/'.$productid);
	}
	
	public function spring_standard($productid=0){
		
		$this->admin_model->set_menu_key('566924a662af93b51abbf510677fc7fe');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('standard_description[]','Description','trim');
		$this->form_validation->set_rules('ps_min[]','MIN','trim');
		$this->form_validation->set_rules('ps_min_unit[]','MIN unit','trim');
		$this->form_validation->set_rules('ps_max[]','MAX','trim');
		$this->form_validation->set_rules('ps_max_unit[]','MAX unit','trim');
		$this->form_validation->set_message('required','"%s" is required.');
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("การจัดการมาตรฐานของ Spring Washer",current_url(),"icon-plus");
			$springlist = $this->product_model->spring_standardlist($productid);
			if($springlist){
				$this->_data['status'] = 'edit';
				$this->_data['row'] = $this->product_model->spring_get_cer_std($productid);
			}else{
				$this->_data['status'] = 'add';
				$pro = $this->product_model->spring_getinfo($productid);
				$this->_data['row'] = $this->product_model->get_cer_std($pro['certificate_id']);
			}
			$this->admin_library->view("manageproduct/spring_standard", $this->_data);
			$this->admin_library->output();
		}else{
			if($_POST['process_status']=='edit'){
				$this->product_model->spring_standard_edit($productid);
			}else{
				$this->product_model->spring_standard_add($productid);	
			}
			
			$this->session->set_flashdata("message-success","<div class='alert alert-success'><button class='close' data-dismiss='alert'>x</button><strong>การทำรายการเสร็จสมบูรณ์</strong>บันทึกเรียบร้อยแล้ว</div>");
			admin_redirect("manageproduct/spring_standard/".$productid);
		}
		
	}
	
	public function spring_chemical($productid=0){
		
		$this->admin_model->set_menu_key('566924a662af93b51abbf510677fc7fe');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('ps_min[]','Chemical value (%) [MIN]','trim');
		$this->form_validation->set_rules('ps_max[]','Chemical value (%) [MAX]','trim');
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("การจัดการค่า Chemical ของ Spring Washer",current_url(),"icon-plus");
			$chemicallist = $this->product_model->spring_chemicallist($productid);
			if($chemicallist){
				$this->_data['status'] = 'edit';
				$this->_data['row'] = $this->product_model->spring_get_cer_chem($productid);
			}else{
				$this->_data['status'] = 'add';
				$pro = $this->product_model->spring_getinfo($productid);
				$this->_data['row'] = $this->product_model->get_cer_chem($pro['certificate_id']);
			}
			
			$this->admin_library->view('manageproduct/spring_chemical', $this->_data);
			$this->admin_library->output();
		}else{
			if($_POST['process_status']=='edit'){
				$this->product_model->spring_chemical_edit($productid);
			}else{
				$this->product_model->spring_chemical_add($productid);	
			}
			
			$this->session->set_flashdata("message-success","<div class='alert alert-success'><button class='close' data-dismiss='alert'>x</button><strong>การทำรายการเสร็จสมบูรณ์</strong>บันทึกเรียบร้อยแล้ว</div>");
			admin_redirect("manageproduct/spring_chemical/".$productid);
		}
		
	}
	/* Spring - End */
	
	/* Taper - Start */
	public function taper_list($page=1){
		$page = ((int) $page < 1)?1:(int) $page;
		$offset = ($page-1)*25;
		$this->seq=$offset;
		$this->admin_model->set_menu_key('88d273f89a74a9d23657c053c3d00057');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		
		$this->admin_model->set_top_button("เพิ่ม Taper Washer","manageproduct/taper_add",'icon-plus','btn-success','w');
		/* $this->admin_model->set_top_button("สำรองข้อมูล","menu_manager/backup",'icon-download','btn-primary','s'); */
		$this->admin_model->set_datatable($this->product_model->taper_dataTable(25,$offset));
		$this->admin_model->set_column("product_no","ลำดับ",'5%');
		$this->admin_model->set_column("product_thumbnail","รูปภาพ",'15%');
		$this->admin_model->set_column("product_name","ชื่อสินค้า",'');
		$this->admin_model->set_column_callback('product_thumbnail','get_image_taper');
		$this->admin_model->set_column_callback("product_no","show_seq");
		$this->admin_model->set_column_callback("product_name","show_type");
		
		$this->admin_model->set_action_button("แก้ไข","manageproduct/taper_edit/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("ลบ","manageproduct/taper_delete/[product_id]",'icon-trash-o','btn-danger','d');
		$this->admin_model->set_action_button("Size","manageproduct/taper_size_list/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("Standard","manageproduct/taper_standard/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("Chemical","manageproduct/taper_chemical/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->show_search_text("manageproduct/taper_list");
		$this->admin_model->set_pagination("manageproduct/taper_list",$this->product_model->taper_count(),25,3);
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function taper_add(){
		
		$this->admin_model->set_menu_key('88d273f89a74a9d23657c053c3d00057');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('product_name','ชื่อสินค้า','trim|required');
		$this->form_validation->set_rules('certificate_id','Certificate','trim|required');
		$this->form_validation->set_rules('product_type','ประเภท','trim|required');
		$this->form_validation->set_message('required', '"%s" is required.');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("เพิ่ม Taper Washer","manageproduct/taper_add","icon-plus");
			$this->admin_library->view("manageproduct/taper_add");
			$this->admin_library->output();
		}else{
			$this->product_model->taper_add();
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manageproduct/taper_list");
		}
	}
	
	public function taper_edit($productid=0){
		
		$this->admin_model->set_menu_key('88d273f89a74a9d23657c053c3d00057');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('product_name','ชื่อสินค้า','trim|required');
		$this->form_validation->set_rules('certificate_id','Certificate','trim|required');
		$this->form_validation->set_rules('product_type','ประเภท','trim|required');
		$this->form_validation->set_message('required', '"%s" is required.');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("แก้ไข Taper Washer","manageproduct/taper_edit/".$productid,"icon-plus");
			$this->_data['row'] = $this->product_model->taper_getinfo($productid);
			$this->admin_library->view("manageproduct/taper_edit", $this->_data);
			$this->admin_library->output();
		}else{
			
			$pro_id = $this->product_model->taper_edit($productid);
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manageproduct/taper_list");
		}
	}
	
	public function product_taperstatus_link($text,$row){
		if($text=="approved"){
			return '<a href="'.admin_url("manageproduct/taper_setstatus/pending/".$row['product_id']).'">'.ucfirst($text).'</a>';
		}else{
			return '<a href="'.admin_url("manageproduct/taper_setstatus/approved/".$row['product_id']).'">'.ucfirst($text).'</a>';
		}
	}
	
	public function taper_delete($productid=0){
		$this->product_model->taper_delete($productid);
		$this->session->set_flashdata("message-success",$message);
		admin_redirect("manageproduct/taper_list");
	}
	
	public function taper_size_list($productid=0){
		$this->admin_model->set_menu_key('566924a662af93b51abbf510677fc7fe');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('รายการสินค้า Taper Washer','manageproduct/taper_list','');
		$this->admin_library->add_breadcrumb('จัดการ Size',current_url(),'');
		
		$this->admin_model->set_top_button("เพิ่ม Taper Washer size","manageproduct/taper_size_add/".$productid,'icon-plus','btn-success','w');
		$this->admin_model->set_top_button("กลับไปยังหน้ารายการสินค้า Taper Washer",'manageproduct/taper_list','icon-reply','btn-primary','w');
		
		$this->admin_model->set_datatable($this->product_model->taper_size_dataTable($productid));
		$this->admin_model->set_column("size_no","ลำดับ",0);
		$this->admin_model->set_column("size_m","M",0);
		$this->admin_model->set_column("size_p","P",0);
		$this->admin_model->set_column_callback("size_no","show_seq");
		$this->admin_model->set_action_button("แก้ไข","manageproduct/taper_size_edit/".$productid."/[size_id]",'icon-edit','btn-success','w');
		$this->admin_model->set_action_button("ลบ","manageproduct/taper_size_delete/".$productid."/[size_id]",'icon-trash-o','btn-danger','d');
		
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function taper_size_add($productid=0){
		
		$this->admin_model->set_menu_key('88d273f89a74a9d23657c053c3d00057');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('size_m','M','trim|required');
		$this->form_validation->set_rules('size_p','P','trim');
		$this->form_validation->set_rules('size_length','Length','trim');
		$this->form_validation->set_message('required', "%s is required.");
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("เพิ่ม Taper Washer size",current_url(),"icon-plus");
			$this->_data['row'] = array('product_id'=>$productid);
			$pro = $this->product_model->taper_getinfo($productid);
			$this->_data['cer_list'] = $this->product_model->cer_size($pro['certificate_id']);
			
			
			$this->admin_library->view("manageproduct/taper_size_add", $this->_data);
			$this->admin_library->output();
		}else{
			$this->product_model->taper_size_add($productid);
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manageproduct/taper_size_list/".$productid);
		}
		
	}
	
	public function taper_size_edit($productid=0, $sizeid=0){
		
		$this->admin_model->set_menu_key('88d273f89a74a9d23657c053c3d00057');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('size_m','M','trim|required');
		$this->form_validation->set_rules('size_p','P','trim');
		$this->form_validation->set_rules('size_length','Length','trim');
		$this->form_validation->set_message('required', "%s is required.");
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("เพิ่ม Taper Washer size",current_url(),"icon-plus");
			$this->_data['row'] = array('product_id'=>$productid);
			$this->_data['sizeinfo'] = $this->product_model->get_taper_sizeinfo($sizeid);
			$this->_data['cer_list'] = $this->product_model->taper_cer_size($sizeid);
			
			
			$this->admin_library->view("manageproduct/taper_size_edit", $this->_data);
			$this->admin_library->output();
		}else{
			$this->product_model->taper_size_edit($sizeid);
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manageproduct/taper_size_list/".$productid);
		}
		
	}
	
	public function taper_size_delete($productid=0){
		$this->product_model->taper_size_delete($productid);
		$this->session->set_flashdata('message-success','ลบข้อมูลเรียบร้อยแล้ว');
		admin_redirect('manageproduct/taper_size_list/'.$productid);
	}
	
	public function taper_standard($productid=0){
		
		$this->admin_model->set_menu_key('88d273f89a74a9d23657c053c3d00057');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('standard_description[]','Description','trim');
		$this->form_validation->set_rules('ps_min[]','MIN','trim');
		$this->form_validation->set_rules('ps_min_unit[]','MIN unit','trim');
		$this->form_validation->set_rules('ps_max[]','MAX','trim');
		$this->form_validation->set_rules('ps_max_unit[]','MAX unit','trim');
		$this->form_validation->set_message('required','"%s" is required.');
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("การจัดการมาตรฐานของ Taper Washer",current_url(),"icon-plus");
			$taperlist = $this->product_model->taper_standardlist($productid);
			if($taperlist){
				$this->_data['status'] = 'edit';
				$this->_data['row'] = $this->product_model->taper_get_cer_std($productid);
			}else{
				$this->_data['status'] = 'add';
				$pro = $this->product_model->taper_getinfo($productid);
				$this->_data['row'] = $this->product_model->get_cer_std($pro['certificate_id']);
			}
			$this->admin_library->view("manageproduct/taper_standard", $this->_data);
			$this->admin_library->output();
		}else{
			if($_POST['process_status']=='edit'){
				$this->product_model->taper_standard_edit($productid);
			}else{
				$this->product_model->taper_standard_add($productid);	
			}
			
			$this->session->set_flashdata("message-success","<div class='alert alert-success'><button class='close' data-dismiss='alert'>x</button><strong>การทำรายการเสร็จสมบูรณ์</strong>บันทึกเรียบร้อยแล้ว</div>");
			admin_redirect("manageproduct/taper_standard/".$productid);
		}
		
	}
	
	public function taper_chemical($productid=0){
		
		$this->admin_model->set_menu_key('88d273f89a74a9d23657c053c3d00057');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('ps_min[]','Chemical value (%) [MIN]','trim');
		$this->form_validation->set_rules('ps_max[]','Chemical value (%) [MAX]','trim');
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("การจัดการค่า Chemical ของ Taper Washer",current_url(),"icon-plus");
			$chemicallist = $this->product_model->taper_chemicallist($productid);
			if($chemicallist){
				$this->_data['status'] = 'edit';
				$this->_data['row'] = $this->product_model->taper_get_cer_chem($productid);
			}else{
				$this->_data['status'] = 'add';
				$pro = $this->product_model->taper_getinfo($productid);
				$this->_data['row'] = $this->product_model->get_cer_chem($pro['certificate_id']);
			}
			
			$this->admin_library->view('manageproduct/taper_chemical', $this->_data);
			$this->admin_library->output();
		}else{
			if($_POST['process_status']=='edit'){
				$this->product_model->taper_chemical_edit($productid);
			}else{
				$this->product_model->taper_chemical_add($productid);	
			}
			
			$this->session->set_flashdata("message-success","<div class='alert alert-success'><button class='close' data-dismiss='alert'>x</button><strong>การทำรายการเสร็จสมบูรณ์</strong>บันทึกเรียบร้อยแล้ว</div>");
			admin_redirect("manageproduct/taper_chemical/".$productid);
		}
		
	}
	/* Taper - End */
	
	/* Stud - Start */
	public function stud_list($page=1){
		$page = ((int) $page < 1)?1:(int) $page;
		$offset = ($page-1)*25;
		$this->seq=$offset;
		$this->admin_model->set_menu_key('bb3b8d4c808b47439cf0509a71505416');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		
		$this->admin_model->set_top_button("เพิ่ม Stud","manageproduct/stud_add",'icon-plus','btn-success','w');
		/* $this->admin_model->set_top_button("สำรองข้อมูล","menu_manager/backup",'icon-download','btn-primary','s'); */
		$this->admin_model->set_datatable($this->product_model->stud_dataTable(25,$offset));
		$this->admin_model->set_column("product_no","ลำดับ",'5%');
		$this->admin_model->set_column("product_thumbnail","รูปภาพ",'15%');
		$this->admin_model->set_column("product_name","ชื่อสินค้า",'');
		$this->admin_model->set_column_callback('product_thumbnail','get_image_stud');
		$this->admin_model->set_column_callback("product_no","show_seq");
		$this->admin_model->set_column_callback("product_name","show_type");
		
		$this->admin_model->set_action_button("แก้ไข","manageproduct/stud_edit/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("ลบ","manageproduct/stud_delete/[product_id]",'icon-trash-o','btn-danger','d');
		$this->admin_model->set_action_button("Size","manageproduct/stud_size_list/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("Standard","manageproduct/stud_standard/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("Chemical","manageproduct/stud_chemical/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->show_search_text("manageproduct/stud_list");
		$this->admin_model->set_pagination("manageproduct/stud_list",$this->product_model->stud_count(),25,3);
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function stud_add(){
		
		$this->admin_model->set_menu_key('bb3b8d4c808b47439cf0509a71505416');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('product_name','ชื่อสินค้า','trim|required');
		$this->form_validation->set_rules('certificate_id','Certificate','trim|required');
		$this->form_validation->set_rules('product_type','ประเภท','trim|required');
		$this->form_validation->set_message('required', '"%s" is required.');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("เพิ่ม Stud","manageproduct/stud_add","icon-plus");
			$this->admin_library->view("manageproduct/stud_add");
			$this->admin_library->output();
		}else{
			$this->product_model->stud_add();
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manageproduct/stud_list");
		}
	}
	
	public function stud_edit($productid=0){
		
		$this->admin_model->set_menu_key('bb3b8d4c808b47439cf0509a71505416');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('product_name','ชื่อสินค้า','trim|required');
		$this->form_validation->set_rules('certificate_id','Certificate','trim|required');
		$this->form_validation->set_rules('product_type','ประเภท','trim|required');
		$this->form_validation->set_message('required', '"%s" is required.');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("แก้ไข Stud","manageproduct/stud_edit/".$productid,"icon-plus");
			$this->_data['row'] = $this->product_model->stud_getinfo($productid);
			$this->admin_library->view("manageproduct/stud_edit", $this->_data);
			$this->admin_library->output();
		}else{
			
			$pro_id = $this->product_model->stud_edit($productid);
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manageproduct/stud_list");
		}
	}
	
	public function product_studstatus_link($text,$row){
		if($text=="approved"){
			return '<a href="'.admin_url("manageproduct/stud_setstatus/pending/".$row['product_id']).'">'.ucfirst($text).'</a>';
		}else{
			return '<a href="'.admin_url("manageproduct/stud_setstatus/approved/".$row['product_id']).'">'.ucfirst($text).'</a>';
		}
	}
	
	public function stud_delete($productid=0){
		$this->product_model->stud_delete($productid);
		$this->session->set_flashdata("message-success",$message);
		admin_redirect("manageproduct/stud_list");
	}
	
	public function stud_size_list($productid=0){
		$this->admin_model->set_menu_key('bb3b8d4c808b47439cf0509a71505416');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		$this->admin_library->add_breadcrumb('รายการสินค้า Stud','manageproduct/stud_list','');
		$this->admin_library->add_breadcrumb('จัดการ Size',current_url(),'');
		
		$this->admin_model->set_top_button("เพิ่ม Stud size","manageproduct/stud_size_add/".$productid,'icon-plus','btn-success','w');
		$this->admin_model->set_top_button("กลับไปยังหน้ารายการสินค้า Stud",'manageproduct/stud_list','icon-reply','btn-primary','w');
		
		$this->admin_model->set_datatable($this->product_model->stud_size_dataTable($productid));
		$this->admin_model->set_column("size_no","ลำดับ",0);
		$this->admin_model->set_column("size_m","M",0);
		$this->admin_model->set_column("size_p","P",0);
		$this->admin_model->set_column_callback("size_no","show_seq");
		$this->admin_model->set_action_button("แก้ไข","manageproduct/stud_size_edit/".$productid."/[size_id]",'icon-edit','btn-success','w');
		$this->admin_model->set_action_button("ลบ","manageproduct/stud_size_delete/".$productid."/[size_id]",'icon-trash-o','btn-danger','d');
		
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function stud_size_add($productid=0){
		
		$this->admin_model->set_menu_key('bb3b8d4c808b47439cf0509a71505416');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('size_m','M','trim|required');
		$this->form_validation->set_rules('size_p','P','trim');
		$this->form_validation->set_rules('size_length','Length','trim');
		$this->form_validation->set_message('required', "%s is required.");
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("เพิ่ม Stud size",current_url(),"icon-plus");
			$this->_data['row'] = array('product_id'=>$productid);
			$pro = $this->product_model->stud_getinfo($productid);
			$this->_data['cer_list'] = $this->product_model->cer_size($pro['certificate_id']);
			
			
			$this->admin_library->view("manageproduct/stud_size_add", $this->_data);
			$this->admin_library->output();
		}else{
			$this->product_model->stud_size_add($productid);
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manageproduct/stud_size_list/".$productid);
		}
		
	}
	
	public function stud_size_edit($productid=0, $sizeid=0){
		
		$this->admin_model->set_menu_key('bb3b8d4c808b47439cf0509a71505416');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('size_m','M','trim|required');
		$this->form_validation->set_rules('size_p','P','trim');
		$this->form_validation->set_rules('size_length','Length','trim');
		$this->form_validation->set_message('required', "%s is required.");
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("เพิ่ม Stud size",current_url(),"icon-plus");
			$this->_data['row'] = array('product_id'=>$productid);
			$this->_data['sizeinfo'] = $this->product_model->get_stud_sizeinfo($sizeid);
			$this->_data['cer_list'] = $this->product_model->stud_cer_size($sizeid);
			
			
			$this->admin_library->view("manageproduct/stud_size_edit", $this->_data);
			$this->admin_library->output();
		}else{
			$this->product_model->stud_size_edit($sizeid);
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manageproduct/stud_size_list/".$productid);
		}
		
	}
	
	public function stud_size_delete($productid=0){
		$this->product_model->stud_size_delete($productid);
		$this->session->set_flashdata('message-success','ลบข้อมูลเรียบร้อยแล้ว');
		admin_redirect('manageproduct/stud_size_list/'.$productid);
	}
	
	public function stud_standard($productid=0){
		
		$this->admin_model->set_menu_key('bb3b8d4c808b47439cf0509a71505416');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('standard_description[]','Description','trim');
		$this->form_validation->set_rules('ps_min[]','MIN','trim');
		$this->form_validation->set_rules('ps_min_unit[]','MIN unit','trim');
		$this->form_validation->set_rules('ps_max[]','MAX','trim');
		$this->form_validation->set_rules('ps_max_unit[]','MAX unit','trim');
		$this->form_validation->set_message('required','"%s" is required.');
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("การจัดการมาตรฐานของ Stud",current_url(),"icon-plus");
			$studlist = $this->product_model->stud_standardlist($productid);
			if($studlist){
				$this->_data['status'] = 'edit';
				$this->_data['row'] = $this->product_model->stud_get_cer_std($productid);
			}else{
				$this->_data['status'] = 'add';
				$pro = $this->product_model->stud_getinfo($productid);
				$this->_data['row'] = $this->product_model->get_cer_std($pro['certificate_id']);
			}
			$this->admin_library->view("manageproduct/stud_standard", $this->_data);
			$this->admin_library->output();
		}else{
			if($_POST['process_status']=='edit'){
				$this->product_model->stud_standard_edit($productid);
			}else{
				$this->product_model->stud_standard_add($productid);	
			}
			
			$this->session->set_flashdata("message-success","<div class='alert alert-success'><button class='close' data-dismiss='alert'>x</button><strong>การทำรายการเสร็จสมบูรณ์</strong>บันทึกเรียบร้อยแล้ว</div>");
			admin_redirect("manageproduct/stud_standard/".$productid);
		}
		
	}
	
	public function stud_chemical($productid=0){
		
		$this->admin_model->set_menu_key('bb3b8d4c808b47439cf0509a71505416');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('ps_min[]','Chemical value (%) [MIN]','trim');
		$this->form_validation->set_rules('ps_max[]','Chemical value (%) [MAX]','trim');
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("การจัดการค่า Chemical ของ Stud",current_url(),"icon-plus");
			$chemicallist = $this->product_model->stud_chemicallist($productid);
			if($chemicallist){
				$this->_data['status'] = 'edit';
				$this->_data['row'] = $this->product_model->stud_get_cer_chem($productid);
			}else{
				$this->_data['status'] = 'add';
				$pro = $this->product_model->stud_getinfo($productid);
				$this->_data['row'] = $this->product_model->get_cer_chem($pro['certificate_id']);
			}
			
			$this->admin_library->view('manageproduct/stud_chemical', $this->_data);
			$this->admin_library->output();
		}else{
			if($_POST['process_status']=='edit'){
				$this->product_model->stud_chemical_edit($productid);
			}else{
				$this->product_model->stud_chemical_add($productid);	
			}
			
			$this->session->set_flashdata("message-success","<div class='alert alert-success'><button class='close' data-dismiss='alert'>x</button><strong>การทำรายการเสร็จสมบูรณ์</strong>บันทึกเรียบร้อยแล้ว</div>");
			admin_redirect("manageproduct/stud_chemical/".$productid);
		}
		
	}
	/* Stud - End */
	
	/* Default function - Start */
	public function show_seq($text,$row)
	{
		$this->seq++;
		return $this->seq;
		
	}
	
	public function get_image($text,$row){
		if($row['product_image']!=''){
			return '<img src="'.site_url("src/60/product/bolt/".$row['product_image']).'" alt="" />';
		}else{
			return '<img src="'.site_url("src/60/product/bolt/default.jpg").'" alt="" />';
		}
	}
	
	public function get_image_nut($text,$row){
		if($row['product_image']!=''){
			return '<img src="'.site_url("src/60/product/nut/".$row['product_image']).'" alt="" />';
		}else{
			return '<img src="'.site_url("src/60/product/nut/default.jpg").'" alt="" />';
		}
	}
	
	public function get_image_washer($text,$row){
		if($row['product_image']!=''){
			return '<img src="'.site_url("src/60/product/washer/".$row['product_image']).'" alt="" />';
		}else{
			return '<img src="'.site_url("src/60/product/washer/default.jpg").'" alt="" />';
		}
	}
	
	public function get_image_spring($text,$row){
		if($row['product_image']!=''){
			return '<img src="'.site_url("src/60/product/spring/".$row['product_image']).'" alt="" />';
		}else{
			return '<img src="'.site_url("src/60/product/spring/default.jpg").'" alt="" />';
		}
	}
	
	public function get_image_taper($text,$row){
		if($row['product_image']!=''){
			return '<img src="'.site_url("src/60/product/taper/".$row['product_image']).'" alt="" />';
		}else{
			return '<img src="'.site_url("src/60/product/taper/default.jpg").'" alt="" />';
		}
	}
	
	public function get_image_stud($text,$row){
		if($row['product_image']!=''){
			return '<img src="'.site_url("src/60/product/stud/".$row['product_image']).'" alt="" />';
		}else{
			return '<img src="'.site_url("src/60/product/stud/default.jpg").'" alt="" />';
		}
	}
	
	public function show_type($text, $row){
		return $row['product_name'].' '.$row['product_type'];
	}
	/* Default function - End */
}

?>