<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Thickness_test extends CI_Controller {
	var $_data = array();
	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->library('image_lib');
		$this->load->library('admin_library');	
		$this->admin_library->forceLogin();
		$this->load->model('thickness_test_model');
		$this->load->model('administrator/admin_model');
	}
	public function index()
	{
			
	}
	
	#############Supplier #######################
	
		
	public function add_thickness_test($income_id=0)
	{
		$this->admin_model->set_menu_key('a8ff0b967d8e9cda6122183af245d1ae');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("income_manager/bolt_list");
		}
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		
		$this->_data['list'] = $this->thickness_test_model->get_thickness();
				
		if(!$_POST){
			$this->admin_library->view("thickness_test/add_bolt_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['bolt_testing_post_user_id'] = "1";
			$data['income_id'] = $this->input->post('income_id');
			$data['bolt_standard_id'] = $this->input->post('bolt_standard_id');
			$data['bolt_testing1'] = $this->input->post('test1');
			$data['bolt_testing2'] = $this->input->post('test2');
			$data['bolt_testing3'] = $this->input->post('test3');
			$data['bolt_testing4'] = $this->input->post('test4');
			$data['bolt_testing5'] = $this->input->post('test5');
			$data['bolt_testing_status'] = $this->input->post('testing_status');
			$data['bolt_testing_remark'] = $this->input->post('remark');
							
			
			$bolt_testing_std=$this->thickness_test_model->add_thickness_test($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("chemical_test/test_bolt/$income_id");
		}
	}
	
	
	
}