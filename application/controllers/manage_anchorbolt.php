<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_anchorbolt extends CI_Controller {
	var $seq = 0;
	var $_data=array();
	function __construct(){
		parent::__construct();
		
		$this->load->library('admin_library');
		$this->load->model('administrator/admin_model');
		$this->load->model('administrator/menu_model');
		$this->load->model('administrator/manage_anchorboltmodel');
		$this->admin_library->forceLogin();
	}
	
	public function index($type='axel', $page=1){
		$page = ((int) $page < 1)?1:(int) $page;
		$offset = ($page-1)*25;
		$this->seq=$offset;
		$this->admin_model->set_menu_key('11343833cf1452d382e91a2df0529c57');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		
		$this->admin_model->set_top_button("เพิ่ม Anchor Bolt","manage_anchorbolt/add/".$type,'icon-plus','btn-success','w');
		/* $this->admin_model->set_top_button("สำรองข้อมูล","menu_manager/backup",'icon-download','btn-primary','s'); */
		$this->admin_model->set_datatable($this->manage_anchorboltmodel->dataTable($type, 25,$offset));
		$this->admin_model->set_column("product_no","ลำดับ",'5%');
		$this->admin_model->set_column("product_thumbnail","รูปภาพ",'15%');
		$this->admin_model->set_column("product_grade","เกรด",'30%');
		$this->admin_model->set_column('product_low_carbon','Low Carbon','20%');
		//$this->admin_model->set_column("product_standard","Standard",'50%');
		$this->admin_model->set_column_callback('product_thumbnail','get_image');
		$this->admin_model->set_column_callback('product_type','get_type');
		$this->admin_model->set_column_callback("product_no","show_seq");
		$this->admin_model->set_column_callback('product_low_carbon','get_carbon_status');
		
		
		$this->admin_model->set_action_button("แก้ไข","manage_anchorbolt/edit/".$type."/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("ลบ","manage_anchorbolt/getdelete/".$type."/[product_id]",'icon-trash-o','btn-danger','d');
		$this->admin_model->set_action_button("Size","manage_anchorbolt/size_list/".$type."/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("Standard","manage_anchorbolt/standard/".$type."/[product_id]",'icon-edit','btn-info','w');
		
		//$this->admin_model->set_action_button("Chemical","manageproduct/bolt_chemical/[product_id]",'icon-edit','btn-info','w');
		$this->admin_model->show_search_text("manage_anchorbolt/index/".$type);
		$this->admin_model->set_pagination("manage_anchorbolt/index/".$type,$this->manage_anchorboltmodel->get_count($type),25,3);
		$this->admin_model->make_list();
		$this->admin_library->output();
		
	}
	
	public function add($type='axel'){
		
		$this->admin_model->set_menu_key('11343833cf1452d382e91a2df0529c57');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('product_grade','Grade','trim|required');
		$this->form_validation->set_rules('product_type','Type','trim|required');
		$this->form_validation->set_rules('product_low_carbon','Low Carbox?','trim');
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("เพิ่ม Anchor Bolt","manage_anchorbolt/add/".$type,"icon-plus");
			$this->_data['type'] = $type;
			$this->admin_library->view("manage_anchorbolt/add", $this->_data);
			$this->admin_library->output();
		}else{
			$this->manage_anchorboltmodel->add();
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manage_anchorbolt/index/".$type);
		}
	}
	
	public function edit($type='axel', $productid=0){
		
		$this->admin_model->set_menu_key('11343833cf1452d382e91a2df0529c57');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('product_grade','Grade','trim|required');
		$this->form_validation->set_rules('product_type','Type','trim|required');
		$this->form_validation->set_rules('product_low_carbon','Low Carbox?','trim');
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("แก้ไข Anchor Bolt","manage_anchorbolt/edit/".$type."/".$productid,"icon-plus");
			$this->_data['row'] = $this->manage_anchorboltmodel->getinfo($productid);
			$this->admin_library->view("manage_anchorbolt/edit", $this->_data);
			$this->admin_library->output();
		}else{
			$update = $this->manage_anchorboltmodel->edit($productid);
			if(!$update){
				$this->_data['error_message'] =  $this->mobile_upload->display_errors();
				$this->_data['row'] = $this->manage_anchorboltmodel->getinfo($productid);
				$this->admin_library->view("manage_anchorbolt/edit", $this->_data);
				$this->admin_library->output();
			}else{
				$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
				admin_redirect("manage_anchorbolt/index/".$type);
			}
			
		}
	}
	
	public function getdelete($type='axel', $productid=0){
		$this->manage_anchorboltmodel->getdelete($productid);
		$this->session->set_flashdata("message-success",$message);
		admin_redirect("manage_anchorbolt/index/".$type);
	}
	
	public function size_list($type='axel', $productid=0, $page=1){
		$page = ((int) $page < 1)?1:(int) $page;
		$offset = ($page-1)*25;
		$this->seq=$offset;
		$this->admin_model->set_menu_key('11343833cf1452d382e91a2df0529c57');
		
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->admin_model->initd($this);
		$this->admin_model->set_title("จัดการสินค้า","icon-shopping-cart");
		if($type == "axel"){
		$this->admin_model->set_top_button("เพิ่ม Anchor Bolt Size","manage_anchorbolt/size_add/".$type."/".$productid,'icon-plus','btn-success','w');
		}else{
		$this->admin_model->set_top_button("เพิ่ม Anchor Bolt Size","manage_anchorbolt/size_add_sheet/".$type."/".$productid,'icon-plus','btn-success','w');	
		}
		$this->admin_model->set_top_button("กลับไปยังหน้ารายการสินค้า Anchor Bolt",'manage_anchorbolt/index/'.$type,'icon-reply','btn-primary','w');
		/* $this->admin_model->set_top_button("สำรองข้อมูล","menu_manager/backup",'icon-download','btn-primary','s'); */
		$this->admin_model->set_datatable($this->manage_anchorboltmodel->size_dataTable($productid, $type, 25,$offset));
		$this->admin_model->set_column("product_no","ลำดับ",'5%');
		if($type == "axel"){
		$this->admin_model->set_column("size_m","M",'15%');
		$this->admin_model->set_column("size_length","Length",'15%');
		}else{
		$this->admin_model->set_column("size_t","T",'15%');
		$this->admin_model->set_column("size_w","W",'15%');
		$this->admin_model->set_column("size_length","Length",'15%');	
		}
		//$this->admin_model->set_column("product_standard","Standard",'50%');
		$this->admin_model->set_column_callback("product_no","show_seq");
		
		$this->admin_model->set_action_button("สร้างสินค้า","anchorbolt_product/product_list/".$type."/[size_id]/",'icon-edit','btn-info','w');
		if($type == "axel"){
		$this->admin_model->set_action_button("แก้ไข","manage_anchorbolt/size_edit/".$type."/[product_id]/[size_id]",'icon-edit','btn-info','w');
		}else{
		$this->admin_model->set_action_button("แก้ไข","manage_anchorbolt/size_edit_sheet/".$type."/[product_id]/[size_id]",'icon-edit','btn-info','w');	
		}
		$this->admin_model->set_action_button("ลบ","manage_anchorbolt/size_delete/".$type."/[size_id]",'icon-trash-o','btn-danger','d');
		//$this->admin_model->set_action_button("Chemical","manage_anchorbolt/anchor_chemical/".$type."/[product_id]/[size_id]",'icon-edit','btn-info','w');
		$this->admin_model->show_search_text("manage_anchorbolt/size_list/".$type."/".$productid);
		$this->admin_model->set_pagination("manage_anchorbolt/size_list/".$type."/".$productid,$this->manage_anchorboltmodel->get_size_count($productid, $type),25,3);
		$this->admin_model->make_list();
		$this->admin_library->output();
	}
	
	public function size_add($type='axel', $productid=0){
		
		$this->admin_model->set_menu_key('11343833cf1452d382e91a2df0529c57');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('size_m','M','trim|required');
		$this->form_validation->set_rules('size_length','Length','trim|required');
		
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("เพิ่ม Anchor Bolt Size","manage_anchorbolt/add","icon-plus");
			$this->_data['product_id'] = $productid;
			$this->_data['type'] = $type;
			//$this->_data['row'] = $this->manage_anchorboltmodel->get_cer_std("1");
			$this->admin_library->view("manage_anchorbolt/size_add", $this->_data);
			$this->admin_library->output();
		}else{
			$size_id = $this->manage_anchorboltmodel->addsize();
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manage_anchorbolt/size_list/".$type."/".$productid);
		}
		
	}
	
	public function size_edit($type='axel', $productid=0, $sizeid=0){
		$this->admin_model->set_menu_key('11343833cf1452d382e91a2df0529c57');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('size_m','M','trim|required');
		$this->form_validation->set_rules('size_length','Length','trim|required');
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("แก้ไข Anchor Bolt Size","manage_anchorbolt/edit/".$productid.'/'.$sizeid,"icon-plus");
			$this->_data['row'] = $this->manage_anchorboltmodel->get_sizeinfo_byid($sizeid);
			$this->_data['type'] = $type;
			$this->admin_library->view("manage_anchorbolt/size_edit", $this->_data);
			$this->admin_library->output();
		}else{
			$this->manage_anchorboltmodel->editsize($sizeid);
			
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manage_anchorbolt/size_list/".$type."/".$productid);
		}
		
	}
	
	
	public function size_add_sheet($type='sheet', $productid=0){
		
		$this->admin_model->set_menu_key('11343833cf1452d382e91a2df0529c57');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('size_t','T','trim|required');
		$this->form_validation->set_rules('size_w','W','trim|required');
		$this->form_validation->set_rules('size_length','Length','trim|required');	
	
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("เพิ่ม Anchor Bolt Size","manage_anchorbolt/add","icon-plus");
			$this->_data['product_id'] = $productid;
			$this->_data['type'] = $type;
			$this->_data['row'] = $this->manage_anchorboltmodel->get_cer_std("1");
			$this->admin_library->view("manage_anchorbolt/size_add_sheet", $this->_data);
			$this->admin_library->output();
		}else{
			$size_id = $this->manage_anchorboltmodel->addsize_sheet();
			$this->manage_anchorboltmodel->anchor_standard_add($size_id);
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manage_anchorbolt/size_list/".$type."/".$productid);
		}
		
	}
	
	public function size_edit_sheet($type='sheet', $productid=0, $sizeid=0){
		$this->admin_model->set_menu_key('11343833cf1452d382e91a2df0529c57');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$this->form_validation->set_rules('size_t','T','trim|required');
		$this->form_validation->set_rules('size_w','W','trim|required');
		$this->form_validation->set_rules('size_length','Length','trim|required');
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("แก้ไข Anchor Bolt Size","manage_anchorbolt/edit/".$productid.'/'.$sizeid,"icon-plus");
			$this->_data['row'] = $this->manage_anchorboltmodel->get_sizeinfo_byid($sizeid);
			$this->_data['type'] = $type;
			$this->_data['list'] = $this->manage_anchorboltmodel->get_cer_std_foredit($sizeid);
			$this->admin_library->view("manage_anchorbolt/size_edit_sheet", $this->_data);
			$this->admin_library->output();
		}else{
			$this->manage_anchorboltmodel->editsize_sheet($sizeid);
			$this->manage_anchorboltmodel->anchor_standard_edit($sizeid);
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manage_anchorbolt/size_list/".$type."/".$productid);
		}
		
	}
	
	
	public function size_delete($type='axel', $productid=0, $size_id=0){
		$this->manage_anchorboltmodel->deletesize($size_id);
		$this->session->set_flashdata("message-success",$message);
		admin_redirect("manage_anchorbolt/size_list/".$type."/".$productid);
	}
	
	public function standard($type='axel', $productid=0){
		$this->admin_model->set_menu_key('11343833cf1452d382e91a2df0529c57');
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('ps_min','ค่า MIN','trim|required');
		$this->form_validation->set_rules('ps_min_unit','หน่วยของ MIN','trim|required');
		$this->form_validation->set_rules('ps_max','ค่า MAX','trim|required');
		$this->form_validation->set_rules('ps_max_unit','หน่วยของ MAX','trim|required');
		$this->form_validation->set_message('required','กรุณาระบุ "%s"');
		$this->form_validation->set_error_delimiters('<div class="message error">','</div>');
		
		if($this->form_validation->run()===FALSE){
			$this->admin_library->add_breadcrumb("Anchor Bolt Size Standard","manage_anchorbolt/standard/".$productid,"icon-plus");
			$this->_data['row'] = $this->manage_anchorboltmodel->get_standardinfo_byid($productid);
			if($this->_data['row']){
				$this->_data['product_id'] = $this->_data['row']['product_id'];
				$this->_data['update_status'] = 'update';
			}else{
				$this->_data['product_id'] = $productid;
				$this->_data['update_status'] = 'insert';
			}
			$this->_data['type'] = $type;
			$this->admin_library->view("manage_anchorbolt/standard", $this->_data);
			$this->admin_library->output();
		}else{
			$this->manage_anchorboltmodel->update_standard();
			$this->session->set_flashdata("message-success","บันทึกเรียบร้อยแล้ว");
			admin_redirect("manage_anchorbolt/standard/".$type."/".$productid);
		}
	}
	
	public function anchor_chemical($type='axel',$incomeid=0){
		
		if($type=="axel"){
			$this->admin_model->set_menu_key('ca4e503a3c9fe5af42d1e7d4c18e1abc');
		}else{
			$this->admin_model->set_menu_key('91f3758c72ce63009ee6d3ee769df48e');
		}
		
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->form_validation->set_rules('ps_min[]','Chemical value (%) [MIN]','trim');
		$this->form_validation->set_rules('ps_max[]','Chemical value (%) [MAX]','trim');
		$this->form_validation->set_rules('chemical_result[]','Chemical result','trim');
		/*
$this->form_validation->set_rules('chemical_value[]','Chemical (%)','trim');
		$this->form_validation->set_rules('chemical_composition[]','Composition','trim');
*/
		
		if($this->form_validation->run()===false){
			$this->admin_library->add_breadcrumb("การจัดการค่า Chemical ของ Anchor",current_url(),"icon-plus");
			$chemicallist = $this->manage_anchorboltmodel->anchor_chemicallist($incomeid);
			if($chemicallist){
				$this->_data['status'] = 'edit';
				//$this->_data['product_id'] = $productid;
				//$pro = $this->manage_anchorboltmodel->anchor_getinfo($productid);
				$this->_data['row'] = $this->manage_anchorboltmodel->get_cer_chem_foredit($incomeid);
			}else{
				$this->_data['status'] = 'add';
				//$this->_data['product_id'] = $productid;
				//$pro = $this->manage_anchorboltmodel->anchor_getinfo($productid);
				$this->_data['row'] = $this->manage_anchorboltmodel->get_cer_chem("10");
			}
			$this->_data['type'] = $type;
			$this->admin_library->view('manage_anchorbolt/anchor_chemical', $this->_data);
			$this->admin_library->output();
		}else{
			if($_POST['process_status']=='edit'){
				$this->manage_anchorboltmodel->anchor_chemical_edit($incomeid);
			}else{
				$this->manage_anchorboltmodel->anchor_chemical_add($incomeid);	
			}
			$this->session->set_flashdata("message-success","<div class='alert alert-success'><button class='close' data-dismiss='alert'>x</button><strong>การทำรายการเสร็จสมบูรณ์</strong>บันทึกเรียบร้อยแล้ว</div>");
			admin_redirect("income_manager_anchor/anchor_list/".$type);
		}
		
	}
	
	/* Default function - Start */
	public function show_seq($text,$row)
	{
		$this->seq++;
		return $this->seq;
		
	}
	
	public function get_image($text,$row){
		if($row['product_thumbnail']!=''){
			return '<img src="'.site_url("src/60/product/anchor_bolt/".$row['product_thumbnail']).'" alt="" />';
		}else{
			return '<img src="'.site_url("src/60/product/anchor_bolt/default.jpg").'" alt="" />';
		}
	}
	
	public function get_type($text,$row){
		if($row['product_type']=='axel'){
			return 'Axel';
		}else if($row['product_type']=='sheet'){
			return 'Sheet';
		}else{
			return 'No type';
		}
	}
	
	public function get_carbon_status($text,$row){
		if($row['product_low_carbon']==1){
			return '<i class="icon-check"></i>';
		}else{
			return '';
		}
	}
	/* Default function - End */
}

?>