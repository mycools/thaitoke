<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Project Name : iAON
* Build Date : 8/7/2558 BE
* Author Name : Jarak Kritkiattisak
* File Name : report_anchorbolt.php
* File Location : /Volumes/Macintosh HD/Users/mycools/Library/Caches/Coda 2/3A0380CF-F77F-4E56-8DA0-E639650EB21C
* File Type : Controller	
* Remote URL : http://192.168.100.20/application/controllers/report_anchorbolt.php
*/
class Report_anchorbolt extends CI_Controller {
	var $_data=array();
	var $report_save_path = './report/';
	var $file_type = "xlsx";
	var $writer_type='Excel2007'; //'Excel2007';
	function __construct(){
		parent::__construct();
		
		$this->load->library('admin_library');
		$this->load->model('administrator/admin_model');
		$this->load->model('administrator/menu_model');
		$this->load->model('report_anchor_model');
		$this->admin_library->forceLogin();
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');
	}
	public function index()
	{
		redirect("dashboard");
	}
	function income_full($id=0, $type)
	{
		$this->_data['type']=$type;
		$this->load->model('income_test_anchor_model');
		$checked = $this->income_test_anchor_model->check_income_tested($id);
		
		if($checked > 0){
		
		
		$fontName = "Tahoma";
		$strFileName = "anchorbolt_full_".$id.".".$this->file_type;
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		$objPHPExcel = $objReader->load($this->report_save_path."report_income_anchorbolt_template.xlsx");
		
		
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->getStyle('A1'.':' . $objPHPExcel->getActiveSheet()->getHighestColumn() . $objPHPExcel->getActiveSheet()->getHighestRow())->getFont()
                                ->setName($fontName)
                                ->setSize(10)
                                ->getColor()->setRGB('000000');
		$objPHPExcel->setActiveSheetIndex(0);
		
		//Query
		$info = $this->report_anchor_model->get_income_full($id)->row_array();
		$std = $this->report_anchor_model->get_standard($info['income_product_id']);
		$test = $this->report_anchor_model->get_test($id);
		$ps_unit = "(".$std['ps_max_unit'].")";
		$objPHPExcel->getActiveSheet()->setCellValue("A40",$ps_unit);
		$objPHPExcel->getActiveSheet()->setCellValue("C12",$info['VENDER']);
		$objPHPExcel->getActiveSheet()->setCellValue("C13",$info['ADDRESS']);
		$objPHPExcel->getActiveSheet()->setCellValue("J12",$info['CONTRACT_JOB']);
		$objPHPExcel->getActiveSheet()->setCellValue("J13","");
		$objPHPExcel->getActiveSheet()->setCellValue("D16",$info['income_heat_no']);
		$objPHPExcel->getActiveSheet()->setCellValue("D17",$info['income_test_no']);
		$objPHPExcel->getActiveSheet()->setCellValue("D18",$info['income_po_no']);
		$objPHPExcel->getActiveSheet()->setCellValue("D19",$info['INVOICE_NO']);
		if($type=="axel"){
			$D20val = "";
			if($info['SIZEM']){
				$D20val .= "M".$info['SIZEM'];
			}
			if($info['SIZEL']){
				if($D20val){ $D20val .= " x "; }
				$D20val .= "L".$info['SIZEL'];
			}
			$objPHPExcel->getActiveSheet()->setCellValue("D20",$D20val);
		}else{
			$D20val = "";
			if($info['SIZET']){
				$D20val .= "T".$info['SIZET'];
			}
			if($info['SIZEW']){
				if($D20val){ $D20val .= " x "; }
				$D20val .= "W".$info['SIZEW'];
			}
			if($info['SIZEL']){
				if($D20val){ $D20val .= " x "; }
				$D20val .= "L".$info['SIZEL'];
			}
			$objPHPExcel->getActiveSheet()->setCellValue("D20",$D20val);
		}
		$objPHPExcel->getActiveSheet()->setCellValue("F20",$info['income_quantity']);
		$objPHPExcel->getActiveSheet()->setCellValue("D21",$info['product_grade']);
		$objPHPExcel->getActiveSheet()->setCellValue("C41"," ".$std['ps_min']);
		$objPHPExcel->getActiveSheet()->setCellValue("D41"," ".$std['ps_max']);
		
		$rowindex=40;
		foreach($test as $row)
		{
			$objPHPExcel->getActiveSheet()->setCellValue("G".$rowindex," ".$row['anchor_testing1']);
			$objPHPExcel->getActiveSheet()->setCellValue("H".$rowindex," ".$row['anchor_testing2']);
			$objPHPExcel->getActiveSheet()->setCellValue("I".$rowindex," ".$row['anchor_testing3']);
			$objPHPExcel->getActiveSheet()->setCellValue("J".$rowindex," ".$row['anchor_testing4']);
			$objPHPExcel->getActiveSheet()->setCellValue("K".$rowindex," ".$row['anchor_testing5']);
			$objPHPExcel->getActiveSheet()->setCellValue("L".$rowindex,($row['anchor_testing_status']=="pass")?"PASS":"NO");
			
			
			$rowindex++;
		}
		$objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName("name");
        $objDrawing->setDescription("Description");
        $objDrawing->setPath('./public/uploads/product/anchor_bolt/'.$info['product_thumbnail']);
        $objDrawing->setHeight(200);
        $objDrawing->setCoordinates('C27');
        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());        
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $this->writer_type);
		//$objWriter->setImagesRoot(base_url());
				
		
		$objWriter->save($this->report_save_path.$strFileName);	
		if(!$this->input->is_cli_request()){
			if($this->file_type=="xlsx"){
				header('Content-Type: application/octet-stream');
				header('Content-Disposition: attachment; filename="'.$strFileName.'"'); 
				header('Content-Transfer-Encoding: binary');
			}else{
				header('Content-Type: text/html;charset:utf-8');
			}
			readfile($this->report_save_path.$strFileName);
		}
		
		}else{
			$this->session->set_flashdata("message-warning","ข้อมูลยังไม่ได้ถูกทดสอบ.");
			admin_redirect("income_manager_anchor/anchor_list/".$type);
			
		}
	}
}

/* End of file report_anchorbolt.php */
/* Location: ./application/controllers/report_anchorbolt.php */