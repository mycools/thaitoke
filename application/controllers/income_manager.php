<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(15);
/*
File Name 	: income_manager.php
Controller	: Income_manager
Create By 	: Jarak Kritkiattisak
Create Date 	: 7/6/2557 BE
Project 	: iAon Project
Version 		: 1.0
*/
class Income_manager extends CI_Controller {
	var $_data=array();
	public function __construct()
	{
		parent::__construct();
		$this->load->library('admin_library');
		$this->load->model('administrator/admin_model');
		$this->load->model('administrator/bolt_stock');
		$this->load->model('administrator/nut_stock');
		$this->load->model('administrator/washer_stock');
		$this->load->model('administrator/spring_stock');
		$this->load->model('administrator/taper_stock');
		$this->load->model('administrator/stud_stock');
		
		$this->admin_library->forceLogin();
		$this->admin_model->set_menu_key('815339ccaea8fdfbb1a2cdd242c19f9c');
	}
	public function index()
	{
		redirect("income_manager/bolt_list");
	}
	/* 
	Bolt manager - Start 
	*/
	public function bolt_list($page=1)
	{
		
		$QUERY_STRING = @$_SERVER['QUERY_STRING'];
		$QUERY_STRING = (trim($QUERY_STRING))?"?".$QUERY_STRING:"";
		$this->admin_model->set_menu_key('a8ff0b967d8e9cda6122183af245d1ae');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$limit=100;
		$offset = ($page-1)*$limit;
		$offset = ($offset <0)?0:$offset;
		
		$this->admin_library->setTitle('Bolt Incoming','tokeicon-bolt');
		
		$this->admin_library->setDetail("Manage Bolt incoming & Print-out Incomming Document");
		$this->admin_model->initd($this);
		$this->admin_model->set_title('Manage Bolt incoming','tokeicon-bolt');
		$this->load->model('administrator/bolt_income');
		$this->admin_model->set_datatable($this->bolt_income->dataTable($limit,$offset));
		
		$this->admin_model->set_column("income_id","No.",0,"icon-sort-alpha-asc");
		$this->admin_model->set_column("income_job_id","INCOMING ID.",0,"icon-folder-open");
		$this->admin_model->set_column("income_product_id","Product",0,"icon-shopping-cart");
		$this->admin_model->set_column("size_id","Size",0,"icon-shopping-cart");
		$this->admin_model->set_column("income_supplier_id","Supplier",0,"icon-shopping-cart");
		$this->admin_model->set_column("income_lot_no","Lot No.",0,"icon-folder-open");
		//$this->admin_model->set_column("income_createdtime","Create Date",0,"icon-calendar");
		//$this->admin_model->set_column("income_manager","ผู้จัดการข้อมูล",0,"icon-user");
		$this->admin_model->set_column("test_status","การทดสอบ",0,'icon-check-square-o');
		
		$this->admin_model->set_column_callback("income_product_id","product_name_callback");
		$this->admin_model->set_column_callback("income_supplier_id","supplier_name_callback");
		$this->admin_model->set_column_callback("income_manager","lot_manager_callback");
		$this->admin_model->set_column_callback("income_createdtime","date_callback");
		$this->admin_model->set_column_callback("test_status","test_status_callback");
		$this->admin_model->set_column_callback("size_id","product_size_callback");

		
		$this->admin_model->set_top_button("เพิ่มข้อมูล","income_manager/bolt_add","icon-plus","btn-info","r");
		
		$this->admin_model->set_action_button("แก้ไข","income_manager/bolt_edit/[income_id]","icon-eye","btn-success","r");
		$this->admin_model->set_action_button("ทดสอบ","income_test/add_bolt_testing/[income_id]","icon-check-square-o","btn-success","r");
		$this->admin_model->set_action_button("พิมพ์","income_manager/pdf_bolt/[income_id]/income-bolt-[income_id].pdf","icon-print","btn-info","w","_blank");
		$this->admin_model->set_action_button("ลบ","income_manager/bolt_delete/[income_id]","icon-trash-o","btn-danger","d");
		
		$this->admin_model->set_action_button_callback("พิมพ์","print_stud_callback");
		
		$this->admin_model->set_pagination("income_manager/bolt_list",$this->bolt_income->export_data()->num_rows(),$limit,3);
		$this->admin_model->show_search_text("income_manager/bolt_list"); 
		
		$this->admin_model->make_list();
		$this->admin_library->output();		
	}
	public function bolt_add()
	{
		$this->admin_model->set_menu_key('a8ff0b967d8e9cda6122183af245d1ae');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$this->load->model('administrator/bolt_income');
		$this->form_validation->set_rules("supplier_id","Supplier ID","trim|required");
		$this->form_validation->set_rules("product_id","Product ID ","trim|required");
		
		if($this->form_validation->run()===false)
		{
			$this->admin_library->setTitle('Bolt Incoming','icon-folder-open');
			$this->admin_library->setDetail("Bolt incoming manager");
			$this->admin_library->add_breadcrumb("Bolt Incoming","income_manager/bolt_list","icon-folder-open");
			$this->admin_library->add_breadcrumb("Add Incoming","income_manager/bolt_add","icon-plus");
			
			$this->admin_library->view("income_manager/bolt_add");
			$this->admin_library->output();
		}else{
			$this->bolt_income->add_income();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("income_manager/bolt_list");
		}
	}
	public function bolt_edit($income_id=0)
	{
		$this->admin_model->set_menu_key('a8ff0b967d8e9cda6122183af245d1ae');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->load->model('administrator/bolt_income');
		$this->form_validation->set_rules("supplier_id","Supplier ID","trim|required");
		//$this->form_validation->set_rules("product_id","Product ID ","trim|required");
		if($this->form_validation->run()===false)
		{
			$this->_data['row'] = $this->bolt_income->getIncome($income_id);
			if(!$this->_data['row']){
				$this->session->set_flashdata("message-error","ไม่พบข้อมูลที่ต้องการแก้ไข.");
				admin_redirect("income_manager/bolt_list");
			}
			$this->admin_library->setTitle('Bolt Incoming','icon-folder-open');
			$this->admin_library->setDetail("Bolt incoming manager");
			$this->admin_library->add_breadcrumb("Bolt Incoming","income_manager/bolt_list","icon-folder-open");
			$this->admin_library->add_breadcrumb("Edit Incoming","income_manager/bolt_edit/".$income_id,"icon-edit");
			
			$this->admin_library->view("income_manager/bolt_edit");
			$this->admin_library->output();
		}else{
			$this->bolt_income->edit_income();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("income_manager/bolt_list");
		}
	}
	function bolt_delete($income_id){
		$this->admin_model->set_menu_key('a8ff0b967d8e9cda6122183af245d1ae');
		if(!$this->admin_model->check_permision("d")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("income_manager/bolt_list");
		}
		$this->load->model('administrator/bolt_income');
		$this->bolt_income->delete_income($income_id);
		$this->session->set_flashdata("message-success","ลบข้อมูลเรียบร้อยแล้ว.");
		admin_redirect("income_manager/bolt_list");
	}
	
	
	function print_bolt1(){
		$this->admin_model->set_menu_key('a8ff0b967d8e9cda6122183af245d1ae');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$product = $this->income_test_model->get_bolt_product($this->_data['income_id']);
		
		$this->_data['income_info'] =  $this->income_test_model->get_income_bolt($this->_data['income_id']);
		$this->_data['std_list'] = $this->income_test_model->get_product_standard_bolt($product['product_id']);
		$this->_data['cer_size_list'] = $this->income_test_model->get_product_standard_size_bolt($product['product_id'],$this->_data['income_info']['size_id']);

	
	}
	
	/* 
	Bolt manager - End 
	*/
	/* 
	Nut manager - Start 
	*/
	public function nut_list($page=1)
	{
		
		$QUERY_STRING = @$_SERVER['QUERY_STRING'];
		$QUERY_STRING = (trim($QUERY_STRING))?"?".$QUERY_STRING:"";
		$this->admin_model->set_menu_key('1bdb9da104e5796f401cca761ef1c9f2');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$limit=100;
		$offset = ($page-1)*$limit;
		$offset = ($offset <0)?0:$offset;
		
		$this->admin_library->setTitle('Nut Incoming','tokeicon-nut');
		
		$this->admin_library->setDetail("Nut incoming manager");
		$this->admin_model->initd($this);
		$this->admin_model->set_title('Nut incoming manager','tokeicon-nut');
		$this->load->model('administrator/nut_income');
		$this->admin_model->set_datatable($this->nut_income->dataTable($limit,$offset));
		
		$this->admin_model->set_column("income_id","No.",0,"icon-sort-alpha-asc");
		$this->admin_model->set_column("income_job_id","INCOMING ID.",0,"icon-folder-open");
		$this->admin_model->set_column("income_product_id","Product",0,"icon-shopping-cart");
		$this->admin_model->set_column("size_id","Size",0,"icon-shopping-cart");
		$this->admin_model->set_column("income_supplier_id","Supplier",0,"icon-shopping-cart");
		$this->admin_model->set_column("income_lot_no","Lot No.",0,"icon-folder-open");
		$this->admin_model->set_column("income_createdtime","Create Date",0,"icon-calendar");
		$this->admin_model->set_column("income_manager","ผู้จัดการข้อมูล",0,"icon-user");
		$this->admin_model->set_column("test_status","สถานะการทดสอบ",0,'icon-code');

		
		$this->admin_model->set_column_callback("income_product_id","product_nut_name_callback");
		$this->admin_model->set_column_callback("income_supplier_id","supplier_nut_name_callback");
		$this->admin_model->set_column_callback("income_manager","lot_manager_callback");
		$this->admin_model->set_column_callback("income_createdtime","date_callback");
		$this->admin_model->set_column_callback("test_status","test_status_callback");
		$this->admin_model->set_column_callback("size_id","product_size_nut_callback");
		
		$this->admin_model->set_top_button("เพิ่มข้อมูล","income_manager/nut_add","icon-plus","btn-info","r");
		
		$this->admin_model->set_action_button("แก้ไข","income_manager/nut_edit/[income_id]","icon-eye","btn-success","r");
		$this->admin_model->set_action_button("ทดสอบ","income_test/add_nut_testing/[income_id]","icon-check-square-o","btn-success","r");
		
		$this->admin_model->set_action_button("พิมพ์","income_manager/pdf_nut/[income_id]/income-nut-[income_id].pdf","icon-print","btn-info","w","_blank");
		$this->admin_model->set_action_button("ลบ","income_manager/nut_delete/[income_id]","icon-trash-o","btn-danger","d");
		
		$this->admin_model->set_action_button_callback("พิมพ์","print_stud_callback");
		
		$this->admin_model->set_pagination("income_manager/nut_list",$this->nut_income->export_data()->num_rows(),$limit,3);
		$this->admin_model->show_search_text();
		$search_q = trim(strip_tags($this->input->get("q")));
		if($search_q != ""){
			$this->admin_library->add_breadcrumb("ค้นหาคำว่า '{$search_q}'","income_manager/nut_list{$QUERY_STRING}","icon-search");
		}
		$this->admin_model->make_list();
		$this->admin_library->output();		
	}
	public function nut_add()
	{
		$this->admin_model->set_menu_key('1bdb9da104e5796f401cca761ef1c9f2');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$this->load->model('administrator/nut_income');
		$this->form_validation->set_rules("supplier_id","Supplier ID","trim|required");
		$this->form_validation->set_rules("product_id","Product ID ","trim|required");
		
		if($this->form_validation->run()===false)
		{
			$this->admin_library->setTitle('Nut Incoming','icon-folder-open');
			$this->admin_library->setDetail("Nut incoming manager");
			$this->admin_library->add_breadcrumb("Nut Incoming","income_manager/nut_list","icon-folder-open");
			$this->admin_library->add_breadcrumb("Add Incoming","income_manager/nut_add","icon-plus");
			
			$this->admin_library->view("income_manager/nut_add");
			$this->admin_library->output();
		}else{
			$this->nut_income->add_income();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("income_manager/nut_list");
		}	}
	public function nut_edit($income_id=0)
	{
		$this->admin_model->set_menu_key('1bdb9da104e5796f401cca761ef1c9f2');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->load->model('administrator/nut_income');
		$this->form_validation->set_rules("supplier_id","Supplier ID","trim|required");
		if($this->form_validation->run()===false)
		{
			$this->_data['row'] = $this->nut_income->getIncome($income_id);
			if(!$this->_data['row']){
				$this->session->set_flashdata("message-error","ไม่พบข้อมูลที่ต้องการแก้ไข.");
				admin_redirect("income_manager/nut_list");
			}
			$this->admin_library->setTitle('Nut Incoming','icon-folder-open');
			$this->admin_library->setDetail("Nut incoming manager");
			$this->admin_library->add_breadcrumb("Nut Incoming","income_manager/nut_list","icon-folder-open");
			$this->admin_library->add_breadcrumb("Edit Incoming","income_manager/nut_edit/".$income_id,"icon-edit");
			
			$this->admin_library->view("income_manager/nut_edit");
			$this->admin_library->output();
		}else{
			$this->nut_income->edit_income();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("income_manager/nut_list");
		}
	}
	function nut_delete($income_id){
		$this->admin_model->set_menu_key('1bdb9da104e5796f401cca761ef1c9f2');
		if(!$this->admin_model->check_permision("d")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("income_manager/nut_list");
		}
		$this->load->model('administrator/nut_income');
		$this->nut_income->delete_income($income_id);
		$this->session->set_flashdata("message-success","ลบข้อมูลเรียบร้อยแล้ว.");
		admin_redirect("income_manager/nut_list");
	}
	/* 
	Nut manager - End 
	*/
	/* 
	Washer manager - Start 
	*/
	public function washer_list($page=1)
	{
		
		$QUERY_STRING = @$_SERVER['QUERY_STRING'];
		$QUERY_STRING = (trim($QUERY_STRING))?"?".$QUERY_STRING:"";
		$this->admin_model->set_menu_key('833675c0ae6c061f4e8d87c294ebb1ec');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$limit=100;
		$offset = ($page-1)*$limit;
		$offset = ($offset <0)?0:$offset;
		
		$this->admin_library->setTitle('Washer Incoming','tokeicon-washer');
		
		$this->admin_library->setDetail("Washer incoming manager");
		$this->admin_model->initd($this);
		$this->admin_model->set_title('Washer incoming manager','tokeicon-washer');
		$this->load->model('administrator/washer_income');
		$this->admin_model->set_datatable($this->washer_income->dataTable($limit,$offset));
		
		$this->admin_model->set_column("income_id","No.",0,"icon-sort-alpha-asc");
		$this->admin_model->set_column("income_job_id","INCOMING ID.",0,"icon-folder-open");
		$this->admin_model->set_column("income_product_id","Product",0,"icon-shopping-cart");
		$this->admin_model->set_column("size_id","Size",0,"icon-shopping-cart");
		$this->admin_model->set_column("income_supplier_id","Supplier",0,"icon-shopping-cart");
		$this->admin_model->set_column("income_lot_no","Lot No.",0,"icon-folder-open");
		$this->admin_model->set_column("income_createdtime","Create Date",0,"icon-calendar");
		$this->admin_model->set_column("income_manager","ผู้จัดการข้อมูล",0,"icon-user");
		$this->admin_model->set_column("test_status","สถานะการทดสอบ",0,'icon-code');

		
		$this->admin_model->set_column_callback("income_product_id","product_washer_name_callback");
		$this->admin_model->set_column_callback("income_supplier_id","supplier_washer_name_callback");
		$this->admin_model->set_column_callback("income_manager","lot_manager_callback");
		$this->admin_model->set_column_callback("income_createdtime","date_callback");
		$this->admin_model->set_column_callback("test_status","test_status_callback");
		$this->admin_model->set_column_callback("size_id","product_size_washer_callback");
		
		$this->admin_model->set_top_button("เพิ่มข้อมูล","income_manager/washer_add","icon-plus","btn-info","r");
		
		$this->admin_model->set_action_button("แก้ไข","income_manager/washer_edit/[income_id]","icon-eye","btn-success","r");
		$this->admin_model->set_action_button("ทดสอบ","income_test/add_washer_testing/[income_id]","icon-check-square-o","btn-success","r");
//		$this->admin_model->set_action_button("พิมพ์","income_manager/print_washer/[income_id]","icon-edit","btn-info","w");
		$this->admin_model->set_action_button("พิมพ์","income_manager/pdf_washer/[income_id]/income-washer-[income_id].pdf","icon-print","btn-info","w","_blank");
		$this->admin_model->set_action_button("ลบ","income_manager/washer_delete/[income_id]","icon-trash-o","btn-danger","d");
		
		$this->admin_model->set_action_button_callback("พิมพ์","print_stud_callback");
		
		$this->admin_model->set_pagination("income_manager/washer_list",$this->washer_income->export_data()->num_rows(),$limit,3);
		$this->admin_model->show_search_text();
		$search_q = trim(strip_tags($this->input->get("q")));
		if($search_q != ""){
			$this->admin_library->add_breadcrumb("ค้นหาคำว่า '{$search_q}'","income_manager/washer_list{$QUERY_STRING}","icon-search");
		}
		$this->admin_model->make_list();
		$this->admin_library->output();		
	}
	public function washer_add()
	{
		$this->admin_model->set_menu_key('833675c0ae6c061f4e8d87c294ebb1ec');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$this->load->model('administrator/washer_income');
		$this->form_validation->set_rules("supplier_id","Supplier ID","trim|required");
		$this->form_validation->set_rules("product_id","Product ID ","trim|required");

		if($this->form_validation->run()===false)
		{
			$this->admin_library->setTitle('Washer Incoming','icon-folder-open');
			$this->admin_library->setDetail("Washer incoming manager");
			$this->admin_library->add_breadcrumb("Washer Incoming","income_manager/washer_list","icon-folder-open");
			$this->admin_library->add_breadcrumb("Add Incoming","income_manager/washer_add","icon-plus");
			
			$this->admin_library->view("income_manager/washer_add");
			$this->admin_library->output();
		}else{
			$this->washer_income->add_income();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("income_manager/washer_list");
		}
	}
	public function washer_edit($income_id=0)
	{
		$this->admin_model->set_menu_key('833675c0ae6c061f4e8d87c294ebb1ec');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->load->model('administrator/washer_income');
		$this->form_validation->set_rules("supplier_id","Supplier ID","trim|required");
		if($this->form_validation->run()===false)
		{
			$this->_data['row'] = $this->washer_income->getIncome($income_id);
			if(!$this->_data['row']){
				$this->session->set_flashdata("message-error","ไม่พบข้อมูลที่ต้องการแก้ไข.");
				admin_redirect("income_manager/washer_list");
			}
			$this->admin_library->setTitle('Washer Incoming','icon-folder-open');
			$this->admin_library->setDetail("Washer incoming manager");
			$this->admin_library->add_breadcrumb("Nut Incoming","income_manager/washer_list","icon-folder-open");
			$this->admin_library->add_breadcrumb("Edit Incoming","income_manager/washer_edit/".$income_id,"icon-edit");
			
			$this->admin_library->view("income_manager/washer_edit");
			$this->admin_library->output();
		}else{
			$this->washer_income->edit_income();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("income_manager/washer_list");
		}
	}
	function washer_delete($income_id){
		$this->admin_model->set_menu_key('833675c0ae6c061f4e8d87c294ebb1ec');
		if(!$this->admin_model->check_permision("d")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("income_manager/washer_list");
		}
		$this->load->model('administrator/washer_income');
		$this->washer_income->delete_income($income_id);
		$this->session->set_flashdata("message-success","ลบข้อมูลเรียบร้อยแล้ว.");
		admin_redirect("income_manager/washer_list");
	}
	/* 
	Washer manager - End 
	*/
	
	/* 
	Spring manager - Start 
	*/
	public function spring_list($page=1)
	{
		
		$QUERY_STRING = @$_SERVER['QUERY_STRING'];
		$QUERY_STRING = (trim($QUERY_STRING))?"?".$QUERY_STRING:"";
		$this->admin_model->set_menu_key('17e7bda6f66d4fe32776e9d97cba36ba');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$limit=100;
		$offset = ($page-1)*$limit;
		$offset = ($offset <0)?0:$offset;
		
		$this->admin_library->setTitle('Spring Washer Incoming','tokeicon-spring-washer');
		
		$this->admin_library->setDetail("Spring Washer incoming manager");
		$this->admin_model->initd($this);
		$this->admin_model->set_title('Spring Washer incoming manager','tokeicon-spring-washer');
		$this->load->model('administrator/spring_income');
		$this->admin_model->set_datatable($this->spring_income->dataTable($limit,$offset));
		
		$this->admin_model->set_column("income_id","No.",0,"icon-sort-alpha-asc");
		$this->admin_model->set_column("income_job_id","INCOMING ID.",0,"icon-folder-open");
		$this->admin_model->set_column("income_product_id","Product",0,"icon-shopping-cart");
		$this->admin_model->set_column("size_id","Size",0,"icon-shopping-cart");
		$this->admin_model->set_column("income_supplier_id","Supplier",0,"icon-shopping-cart");
		$this->admin_model->set_column("income_createdtime","Create Date",0,"icon-calendar");
		$this->admin_model->set_column("income_manager","ผู้จัดการข้อมูล",0,"icon-user");
		$this->admin_model->set_column("test_status","สถานะการทดสอบ",0,'icon-code');
		
		$this->admin_model->set_column_callback("income_product_id","product_spring_name_callback");
		$this->admin_model->set_column_callback("income_supplier_id","supplier_spring_name_callback");
		$this->admin_model->set_column_callback("income_manager","lot_manager_callback");
		$this->admin_model->set_column_callback("income_createdtime","date_callback");
		$this->admin_model->set_column_callback("test_status","test_status_callback");
		$this->admin_model->set_column_callback("size_id","product_size_spring_callback");
		
		$this->admin_model->set_top_button("เพิ่มข้อมูล","income_manager/spring_add","icon-plus","btn-info","r");
		
		$this->admin_model->set_action_button("แก้ไข","income_manager/spring_edit/[income_id]","icon-eye","btn-success","r");
		$this->admin_model->set_action_button("ทดสอบ","income_test/add_spring_testing/[income_id]","icon-check-square-o","btn-success","r");
		$this->admin_model->set_action_button("พิมพ์","income_manager/print_spring/[income_id]","icon-edit","btn-info","w");
		$this->admin_model->set_action_button("ลบ","income_manager/spring_delete/[income_id]","icon-trash-o","btn-danger","d");
		$this->admin_model->set_pagination("income_manager/spring_list",$this->spring_income->export_data()->num_rows(),$limit,3);
		$this->admin_model->show_search_text();
		$search_q = trim(strip_tags($this->input->get("q")));
		if($search_q != ""){
			$this->admin_library->add_breadcrumb("ค้นหาคำว่า '{$search_q}'","income_manager/spring_list{$QUERY_STRING}","icon-search");
		}
		$this->admin_model->make_list();
		$this->admin_library->output();		
	}
	public function spring_add()
	{
		$this->admin_model->set_menu_key('17e7bda6f66d4fe32776e9d97cba36ba');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$this->load->model('administrator/spring_income');
		$this->form_validation->set_rules("supplier_id","Supplier ID","trim|required");
		$this->form_validation->set_rules("product_id","Product ID ","trim|required");

		if($this->form_validation->run()===false)
		{
			$this->admin_library->setTitle('Spring Washer Incoming','icon-folder-open');
			$this->admin_library->setDetail("Spring Washer incoming manager");
			$this->admin_library->add_breadcrumb("Spring Washer Incoming","income_manager/spring_list","icon-folder-open");
			$this->admin_library->add_breadcrumb("Add Incoming","income_manager/spring_add","icon-plus");
			
			$this->admin_library->view("income_manager/spring_add");
			$this->admin_library->output();
		}else{
			$this->spring_income->add_income();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("income_manager/spring_list");
		}
	}
	public function spring_edit($income_id=0)
	{
		$this->admin_model->set_menu_key('17e7bda6f66d4fe32776e9d97cba36ba');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->load->model('administrator/spring_income');
		$this->form_validation->set_rules("supplier_id","Supplier ID","trim|required");
		if($this->form_validation->run()===false)
		{
			$this->_data['row'] = $this->spring_income->getIncome($income_id);
			if(!$this->_data['row']){
				$this->session->set_flashdata("message-error","ไม่พบข้อมูลที่ต้องการแก้ไข.");
				admin_redirect("income_manager/spring_list");
			}
			$this->admin_library->setTitle('Spring Washer Incoming','icon-folder-open');
			$this->admin_library->setDetail("Spring Washer incoming manager");
			$this->admin_library->add_breadcrumb("Nut Incoming","income_manager/spring_list","icon-folder-open");
			$this->admin_library->add_breadcrumb("Edit Incoming","income_manager/spring_edit/".$income_id,"icon-edit");
			
			$this->admin_library->view("income_manager/spring_edit");
			$this->admin_library->output();
		}else{
			$this->spring_income->edit_income();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("income_manager/spring_list");
		}
	}
	function spring_delete($income_id){
		$this->admin_model->set_menu_key('833675c0ae6c061f4e8d87c294ebb1ec');
		if(!$this->admin_model->check_permision("d")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("income_manager/spring_list");
		}
		$this->load->model('administrator/spring_income');
		$this->spring_income->delete_income($income_id);
		$this->session->set_flashdata("message-success","ลบข้อมูลเรียบร้อยแล้ว.");
		admin_redirect("income_manager/spring_list");
	}
	/* 
	Spring Washer manager - End 
	*/
	
	/* 
	Taper manager - Start 
	*/
	public function taper_list($page=1)
	{
		
		$QUERY_STRING = @$_SERVER['QUERY_STRING'];
		$QUERY_STRING = (trim($QUERY_STRING))?"?".$QUERY_STRING:"";
		$this->admin_model->set_menu_key('88a9d3d54bc47e5eb550ab1eb54a43ba');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$limit=100;
		$offset = ($page-1)*$limit;
		$offset = ($offset <0)?0:$offset;
		
		$this->admin_library->setTitle('Taper Washer Incoming','tokeicon-taper-washer');
		
		$this->admin_library->setDetail("Taper Washer incoming manager");
		$this->admin_model->initd($this);
		$this->admin_model->set_title('Taper Washer incoming manager','tokeicon-taper-washer');
		$this->load->model('administrator/taper_income');
		$this->admin_model->set_datatable($this->taper_income->dataTable($limit,$offset));
		
		$this->admin_model->set_column("income_id","No.",0,"icon-sort-alpha-asc");
		$this->admin_model->set_column("income_job_id","INCOMING ID.",0,"icon-folder-open");
		$this->admin_model->set_column("income_product_id","Product",0,"icon-shopping-cart");
		$this->admin_model->set_column("size_id","Size",0,"icon-shopping-cart");
		$this->admin_model->set_column("income_supplier_id","Supplier",0,"icon-shopping-cart");
		$this->admin_model->set_column("income_createdtime","Create Date",0,"icon-calendar");
		$this->admin_model->set_column("income_manager","ผู้จัดการข้อมูล",0,"icon-user");
		$this->admin_model->set_column("test_status","สถานะการทดสอบ",0,'icon-code');
		
		$this->admin_model->set_column_callback("income_product_id","product_taper_name_callback");
		$this->admin_model->set_column_callback("income_supplier_id","supplier_taper_name_callback");
		$this->admin_model->set_column_callback("income_manager","lot_manager_callback");
		$this->admin_model->set_column_callback("income_createdtime","date_callback");
		$this->admin_model->set_column_callback("test_status","test_status_callback");
		$this->admin_model->set_column_callback("size_id","product_size_taper_callback");
		
		$this->admin_model->set_top_button("เพิ่มข้อมูล","income_manager/taper_add","icon-plus","btn-info","r");
		
		$this->admin_model->set_action_button("แก้ไข","income_manager/taper_edit/[income_id]","icon-eye","btn-success","r");
		$this->admin_model->set_action_button("ทดสอบ","income_test/add_taper_testing/[income_id]","icon-check-square-o","btn-success","r");
		$this->admin_model->set_action_button("พิมพ์","income_manager/print_taper/[income_id]","icon-edit","btn-info","w");
		$this->admin_model->set_action_button("ลบ","income_manager/taper_delete/[income_id]","icon-trash-o","btn-danger","d");
		$this->admin_model->set_pagination("income_manager/taper_list",$this->taper_income->export_data()->num_rows(),$limit,3);
		$this->admin_model->show_search_text();
		$search_q = trim(strip_tags($this->input->get("q")));
		if($search_q != ""){
			$this->admin_library->add_breadcrumb("ค้นหาคำว่า '{$search_q}'","income_manager/taper_list{$QUERY_STRING}","icon-search");
		}
		$this->admin_model->make_list();
		$this->admin_library->output();		
	}
	public function taper_add()
	{
		$this->admin_model->set_menu_key('88a9d3d54bc47e5eb550ab1eb54a43ba');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$this->load->model('administrator/taper_income');
		$this->form_validation->set_rules("supplier_id","Supplier ID","trim|required");
		$this->form_validation->set_rules("product_id","Product ID ","trim|required");

		if($this->form_validation->run()===false)
		{
			$this->admin_library->setTitle('taper Washer Incoming','icon-folder-open');
			$this->admin_library->setDetail("Taper Washer incoming manager");
			$this->admin_library->add_breadcrumb("Taper Washer Incoming","income_manager/taper_list","icon-folder-open");
			$this->admin_library->add_breadcrumb("Add Incoming","income_manager/taper_add","icon-plus");
			
			$this->admin_library->view("income_manager/taper_add");
			$this->admin_library->output();
		}else{
			$this->taper_income->add_income();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("income_manager/taper_list");
		}
	}
	public function taper_edit($income_id=0)
	{
		$this->admin_model->set_menu_key('88a9d3d54bc47e5eb550ab1eb54a43ba');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->load->model('administrator/taper_income');
		$this->form_validation->set_rules("supplier_id","Supplier ID","trim|required");
		if($this->form_validation->run()===false)
		{
			$this->_data['row'] = $this->taper_income->getIncome($income_id);
			if(!$this->_data['row']){
				$this->session->set_flashdata("message-error","ไม่พบข้อมูลที่ต้องการแก้ไข.");
				admin_redirect("income_manager/taper_list");
			}
			$this->admin_library->setTitle('Taper Washer Incoming','icon-folder-open');
			$this->admin_library->setDetail("Taper Washer incoming manager");
			$this->admin_library->add_breadcrumb("Nut Incoming","income_manager/taper_list","icon-folder-open");
			$this->admin_library->add_breadcrumb("Edit Incoming","income_manager/taper_edit/".$income_id,"icon-edit");
			
			$this->admin_library->view("income_manager/taper_edit");
			$this->admin_library->output();
		}else{
			$this->taper_income->edit_income();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("income_manager/taper_list");
		}
	}
	function taper_delete($income_id){
		$this->admin_model->set_menu_key('88a9d3d54bc47e5eb550ab1eb54a43ba');
		if(!$this->admin_model->check_permision("d")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("income_manager/taper_list");
		}
		$this->load->model('administrator/taper_income');
		$this->taper_income->delete_income($income_id);
		$this->session->set_flashdata("message-success","ลบข้อมูลเรียบร้อยแล้ว.");
		admin_redirect("income_manager/taper_list");
	}
	/* 
	Taper Washer manager - End 
	*/
	
	/* 
	Stud manager - Start 
	*/
	public function stud_list($page=1)
	{
		
		$QUERY_STRING = @$_SERVER['QUERY_STRING'];
		$QUERY_STRING = (trim($QUERY_STRING))?"?".$QUERY_STRING:"";
		$this->admin_model->set_menu_key('7b21c48a8493e416f7bf0a1e2e17f1da');
		if(!$this->admin_model->check_permision("r")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$limit=100;
		$offset = ($page-1)*$limit;
		$offset = ($offset <0)?0:$offset;
		
		$this->admin_library->setTitle('Stud Incoming','tokeicon-stud-bolt');
		
		$this->admin_library->setDetail("Stud incoming manager");
		$this->admin_model->initd($this);
		$this->admin_model->set_title('Stud incoming manager','tokeicon-stud-bolt');
		$this->load->model('administrator/stud_income');
		$this->admin_model->set_datatable($this->stud_income->dataTable($limit,$offset));
		
		$this->admin_model->set_column("income_id","No.",0,"icon-sort-alpha-asc");
		$this->admin_model->set_column("income_job_id","INCOMING ID.",0,"icon-folder-open");
		$this->admin_model->set_column("income_product_id","Product",0,"icon-shopping-cart");
		$this->admin_model->set_column("size_id","Size",0,"icon-shopping-cart");
		$this->admin_model->set_column("income_supplier_id","Supplier",0,"icon-shopping-cart");
		$this->admin_model->set_column("income_createdtime","Create Date",0,"icon-calendar");
		$this->admin_model->set_column("income_manager","ผู้จัดการข้อมูล",0,"icon-user");
		$this->admin_model->set_column("test_status","สถานะการทดสอบ",0,'icon-code');
		
		$this->admin_model->set_column_callback("income_product_id","product_stud_name_callback");
		$this->admin_model->set_column_callback("income_supplier_id","supplier_stud_name_callback");
		$this->admin_model->set_column_callback("income_manager","lot_manager_callback");
		$this->admin_model->set_column_callback("income_createdtime","date_callback");
		$this->admin_model->set_column_callback("test_status","test_status_callback");
		$this->admin_model->set_column_callback("size_id","product_size_stud_callback");
		
		$this->admin_model->set_top_button("เพิ่มข้อมูล","income_manager/stud_add","icon-plus","btn-info","r");
		
		$this->admin_model->set_action_button("แก้ไข","income_manager/stud_edit/[income_id]","icon-eye","btn-success","r");
		$this->admin_model->set_action_button("ทดสอบ","income_test/add_stud_testing/[income_id]","icon-check-square-o","btn-success","r");
		$this->admin_model->set_action_button("พิมพ์","income_manager/print_stud/[income_id]","icon-edit","btn-info","w");
		$this->admin_model->set_action_button("ลบ","income_manager/stud_delete/[income_id]","icon-trash-o","btn-danger","d");
		
		
		
		$this->admin_model->set_pagination("income_manager/stud_list",$this->stud_income->export_data()->num_rows(),$limit,3);
		$this->admin_model->show_search_text();
		$search_q = trim(strip_tags($this->input->get("q")));
		if($search_q != ""){
			$this->admin_library->add_breadcrumb("ค้นหาคำว่า '{$search_q}'","income_manager/stud_list{$QUERY_STRING}","icon-search");
		}
		$this->admin_model->make_list();
		$this->admin_library->output();		
	}
	
	public function stud_add()
	{
		$this->admin_model->set_menu_key('7b21c48a8493e416f7bf0a1e2e17f1da');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		$this->load->model('administrator/stud_income');
		$this->form_validation->set_rules("supplier_id","Supplier ID","trim|required");
		$this->form_validation->set_rules("product_id","Product ID ","trim|required");

		if($this->form_validation->run()===false)
		{
			$this->admin_library->setTitle('Stud Incoming','icon-folder-open');
			$this->admin_library->setDetail("Stud incoming manager");
			$this->admin_library->add_breadcrumb("Stud Incoming","income_manager/stud_list","icon-folder-open");
			$this->admin_library->add_breadcrumb("Add Incoming","income_manager/stud_add","icon-plus");
			
			$this->admin_library->view("income_manager/stud_add");
			$this->admin_library->output();
		}else{
			$this->stud_income->add_income();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("income_manager/stud_list");
		}
	}
	public function stud_edit($income_id=0)
	{
		$this->admin_model->set_menu_key('7b21c48a8493e416f7bf0a1e2e17f1da');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("dashboard");
		}
		
		$this->load->model('administrator/stud_income');
		$this->form_validation->set_rules("supplier_id","Supplier ID","trim|required");
		if($this->form_validation->run()===false)
		{
			$this->_data['row'] = $this->stud_income->getIncome($income_id);
			if(!$this->_data['row']){
				$this->session->set_flashdata("message-error","ไม่พบข้อมูลที่ต้องการแก้ไข.");
				admin_redirect("income_manager/stud_list");
			}
			$this->admin_library->setTitle('Stud Incoming','icon-folder-open');
			$this->admin_library->setDetail("Stud incoming manager");
			$this->admin_library->add_breadcrumb("Nut Incoming","income_manager/stud_list","icon-folder-open");
			$this->admin_library->add_breadcrumb("Edit Incoming","income_manager/stud_edit/".$income_id,"icon-edit");
			
			$this->admin_library->view("income_manager/stud_edit");
			$this->admin_library->output();
		}else{
			$this->stud_income->edit_income();
			$this->session->set_flashdata("message-success","บันทึกข้อมูลเรียบร้อยแล้ว.");
			admin_redirect("income_manager/stud_list");
		}
	}
	function stud_delete($income_id){
		$this->admin_model->set_menu_key('7b21c48a8493e416f7bf0a1e2e17f1da');
		if(!$this->admin_model->check_permision("d")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลส่วนนี้.");
			admin_redirect("income_manager/stud_list");
		}
		$this->load->model('administrator/stud_income');
		$this->stud_income->delete_income($income_id);
		$this->session->set_flashdata("message-success","ลบข้อมูลเรียบร้อยแล้ว.");
		admin_redirect("income_manager/stud_list");
	}
	/* 
	Stud manager - End 
	*/
	
	
	public function numeric($text,$row)
	{
		return number_format($text);
	}
	public function date_callback($text,$row)
	{
		if($text){
			$text = strtotime($text);
			$text = date("d M Y",$text);
		}else{
			$text = "(ไม่ระบุ)";
		}
		return $text;
	}
	
	public function product_name_callback($text,$row)
	{
		$row = $this->bolt_income->getproduct_name($row['income_product_id']);
		$text = @$row['product_name'] ." ". @$row['product_type'];
		return $text;
	}
	
	public function product_nut_name_callback($text,$row){
		$row = $this->nut_income->getproduct_name($row['income_product_id']);
		$text = $row['product_name'] ." ". $row['product_type'];
		return $text;
		
	}
	
	public function product_washer_name_callback($text,$row){
		$row = $this->washer_income->getproduct_name($row['income_product_id']);
		$text = $row['product_name'] ." ". $row['product_type'];
		return $text;
		
	}
	
	public function product_spring_name_callback($text,$row){
		$row = $this->spring_income->getproduct_name($row['income_product_id']);
		$text = $row['product_name'] ." ". $row['product_type'];
		return $text;
		
	}
	
	public function product_taper_name_callback($text,$row){
		$row = $this->taper_income->getproduct_name($row['income_product_id']);
		$text = $row['product_name'] ." ". $row['product_type'];
		return $text;
		
	}
	
	public function product_stud_name_callback($text,$row){
		$row = $this->stud_income->getproduct_name($row['income_product_id']);
		$text = $row['product_name'] ." ". $row['product_type'];
		return $text;
		
	}
	
	public function supplier_name_callback($text,$row)
	{
		$row = $this->bolt_income->getsupplier_name($row['income_supplier_id']);
		$text = $row['supplier_name'];
		return $text;
	}
	
	public function supplier_nut_name_callback($text,$row)
	{
		$row = $this->nut_income->getsupplier_name($row['income_supplier_id']);
		$text = $row['supplier_name'];
		return $text;
	}
	public function supplier_washer_name_callback($text,$row)
	{
		$row = $this->washer_income->getsupplier_name($row['income_supplier_id']);
		$text = $row['supplier_name'];
		return $text;
	}
	
	public function supplier_spring_name_callback($text,$row)
	{
		$row = $this->spring_income->getsupplier_name($row['income_supplier_id']);
		$text = $row['supplier_name'];
		return $text;
	}
	
	public function supplier_taper_name_callback($text,$row)
	{
		$row = $this->taper_income->getsupplier_name($row['income_supplier_id']);
		$text = $row['supplier_name'];
		return $text;
	}
	
	public function supplier_stud_name_callback($text,$row)
	{
		$row = $this->stud_income->getsupplier_name($row['income_supplier_id']);
		$text = $row['supplier_name'];
		return $text;
	}
	
	public function product_size_callback($text,$row){
		$row = $this->bolt_income->get_size($text);
		$text = @$row['size_m'].'x'.@$row['size_p'].'x'.@$row['size_length'];
		return $text;
	}
	
	public function product_size_nut_callback($text,$row){
		$row = $this->nut_income->get_size($text);
		$text = $row['size_m'].'x'.$row['size_p'];
		return $text;
	}
	
	public function product_size_washer_callback($text,$row){
		$row = $this->washer_income->get_size($text);
		$text = $row['size_m'].'x'.$row['size_p'];
		return $text;
	}
	
	public function product_size_spring_callback($text,$row){
		$row = $this->spring_income->get_size($text);
		$text = @$row['size_m'].'x'.@$row['size_p'];
		return $text;
	}
	
	public function product_size_taper_callback($text,$row){
		$row = $this->taper_income->get_size($text);
		$text = $row['size_m'].'x'.$row['size_p'];
		return $text;
	}
	
	public function product_size_stud_callback($text,$row){
		$row = $this->stud_income->get_size($text);
		$text = $row['size_m'].'x'.$row['size_p'];
		return $text;
	}
	
	public function lot_manager_callback($text,$row)
	{
		if(intval(@$row['income_createdid']) > 0){
			$user = $this->admin_library->getuserinfo($row['income_createdid']);
		}
		if(intval(@$row['income_updatedid']) > 0){
			$user = $this->admin_library->getuserinfo($row['income_updatedid']);
		}
		if(@$user){
			$text = $user['user_fullname'];
		}else{
			$text = "(ไม่ระบุ)";
		}
		return $text;	
	}
	
	public function test_status_callback($text, $row){
		if($text==2){
			return '<span class="label label-success"><i class="fa fa-check-square-o" aria-hidden="true"></i> ผ่านการทดสอบ</span>';
		}else if($text==1){
			return '<span class="label label-warning"><i class="fa fa-times" aria-hidden="true"></i> ไม่ผ่านการทดสอบ</span>';
		}else{
			return '<span class="label label-inverse"><i class="fa fa-square-o" aria-hidden="true"></i> ยังไม่ได้ทดสอบ</span>';
		}
	}
	function print_stud_callback($row)
	{
		if($row['test_status']==2){
			return true;
		}else if($row['test_status']==1){
			return true;
		}else{
			return false;
		}
	}
	function testview()
	{
		$this->admin_library->view("income_manager/excel");
		$this->admin_library->output();
	}
	
	public function get_size_bolt_byid(){
		$boltid = $this->input->post('bolt_id');
		$this->load->model('administrator/bolt_income');
		$this->_data['sizelist'] = $this->bolt_income->get_boltsize_byid($boltid);
		
		$this->load->view('administrator/views/income_manager/sizelist_bolt', $this->_data);
	}	
	
	public function get_size_nut_byid(){
		$boltid = $this->input->post('nut_id');
		$this->load->model('administrator/nut_income');
		$this->_data['sizelist'] = $this->nut_income->get_nutsize_byid($boltid);
		
		$this->load->view('administrator/views/income_manager/sizelist', $this->_data);
	}	
	public function get_size_washer_byid(){
		$washerid = $this->input->post('washer_id');
		$this->load->model('administrator/washer_income');
		$this->_data['sizelist'] = $this->washer_income->get_washersize_byid($washerid);
		
		$this->load->view('administrator/views/income_manager/sizelist', $this->_data);
	}
	public function get_size_spring_byid(){
		$springid = $this->input->post('spring_id');
		$this->load->model('administrator/spring_income');
		$this->_data['sizelist'] = $this->spring_income->get_springsize_byid($springid);
		
		$this->load->view('administrator/views/income_manager/sizelist', $this->_data);
	}
	
	public function get_size_taper_byid(){
		$taperid = $this->input->post('taper_id');
		$this->load->model('administrator/taper_income');
		$this->_data['sizelist'] = $this->taper_income->get_tapersize_byid($taperid);
		
		$this->load->view('administrator/views/income_manager/sizelist', $this->_data);
	}
	
	public function get_size_stud_byid(){
		$studid = $this->input->post('stud_id');
		$this->load->model('administrator/stud_income');
		$this->_data['sizelist'] = $this->stud_income->get_studsize_byid($studid);
		
		$this->load->view('administrator/views/income_manager/sizelist', $this->_data);
	}
	public function pdf_bolt($income_id)
	{
		$this->load->model("fpdf");
		$this->load->model("pdf_mc_table");
		$this->load->model("pdf-income/boltincome");
		$this->boltincome->make($income_id);
	}
	public function print_bolt($income_id)
	{
		$this->load->model('income_test_model');
		$this->load->model('administrator/bolt_income');
		$this->_data['income_id'] = $income_id;
		$checked = $this->income_test_model->check_income_tested($this->_data['income_id']);
		if($checked > 0){
			$product = $this->income_test_model->get_bolt_product($this->_data['income_id']);
			$this->_data['std_list'] = $this->income_test_model->get_product_standard_bolt($product['product_id']);
			$this->_data['product'] = $product;
			$this->_data['income_info'] =  $this->income_test_model->get_income_bolt($this->_data['income_id']);
			//$this->_data['income_data'] =  $this->income_test_model->get_income_tested($this->_data['income_id']);
			$this->_data['shipper'] =  $this->bolt_income->getsupplier_name($this->_data['income_info']['income_supplier_id']);
			$this->_data['size'] = $this->bolt_income->get_size($this->_data['income_info']['size_id']);
			$this->_data['cer_size_list'] = $this->income_test_model->get_product_standard_size_bolt($product['product_id'],$this->_data['income_info']['size_id']);
			
			$this->_data['bolt_thickness'] = $this->income_test_model->get_thicknesstest_info_byincomeid_bolt($income_id);
			
			$this->_data['bolt_thickness_info'] = $this->income_test_model->get_thickness_by_id_income($this->_data['bolt_thickness']['thickness_id']); 
			//$this->_data['income_cer_size_data'] =  $this->income_test_model->get_income_cer_size_tested($this->_data['income_id']);
			header("Content-type: application/vnd.ms-excel");
			header("Content-Disposition: attachment;Filename=income_bolt_report_".$income_id.".xls");
			$this->load->view('administrator/views/income_manager/print_bolt', $this->_data);
		}else{
			$this->session->set_flashdata("message-warning","ข้อมูลยังไม่ได้ถูกทดสอบ.");
			admin_redirect("income_manager/bolt_list");
		}
				
	}
	public function pdf_nut($income_id)
	{
		$this->load->model("fpdf");
		$this->load->model("pdf_mc_table");
		$this->load->model("pdf-income/nutincome");
		$this->nutincome->make($income_id);
	}
	public function print_nut($income_id)
	{
		$this->load->model('income_test_model');
		$this->load->model('administrator/nut_income');
		$this->_data['income_id'] = $income_id;
		$checked = $this->income_test_model->check_income_nut_tested($this->_data['income_id']);
		if($checked > 0){
			$product = $this->income_test_model->get_nut_product($this->_data['income_id']);
			$this->_data['std_list'] = $this->income_test_model->get_product_standard_nut($product['product_id']);
			$this->_data['product'] = $product;
			$this->_data['income_info'] =  $this->income_test_model->get_income_nut($this->_data['income_id']);
			//$this->_data['income_data'] =  $this->income_test_model->get_income_nut_tested($this->_data['income_id']);
			$this->_data['shipper'] =  $this->nut_income->getsupplier_name($this->_data['income_info']['income_supplier_id']);
			$this->_data['size'] = $this->nut_income->get_size($this->_data['income_info']['size_id']);
			$this->_data['cer_size_list'] = $this->income_test_model->get_product_standard_size_nut($product['product_id'],$this->_data['income_info']['size_id']);
			
			$this->_data['nut_thickness'] = $this->income_test_model->get_thicknesstest_info_byincomeid_nut($income_id);
			
			$this->_data['nut_thickness_info'] = $this->income_test_model->get_thickness_by_id_income($this->_data['nut_thickness']['thickness_id']); 
			//$this->_data['income_cer_size_data'] =  $this->income_test_model->get_income_nut_cer_size_tested($this->_data['income_id']);
			header("Content-type: application/vnd.ms-excel");
			header("Content-Disposition: attachment;Filename=income_nut_report_".$income_id.".xls");
			$this->load->view('administrator/views/income_manager/print_nut', $this->_data);
		}else{
			$this->session->set_flashdata("message-warning","ข้อมูลยังไม่ได้ถูกทดสอบ.");
			admin_redirect("income_manager/nut_list");
		}
				
	}
	public function pdf_washer($income_id)
	{
		$this->load->model("fpdf");
		$this->load->model("pdf_mc_table");
		$this->load->model("pdf-income/washerincome");
		$this->washerincome->make($income_id);
	}
	public function print_washer($income_id)
	{
		$this->load->model('income_test_model');
		$this->load->model('administrator/washer_income');
		$this->_data['income_id'] = $income_id;
		$checked = $this->income_test_model->check_income_washer_tested($this->_data['income_id']);
		if($checked > 0){
			$product = $this->income_test_model->get_washer_product($this->_data['income_id']);
			$this->_data['std_list'] = $this->income_test_model->get_product_standard_washer($product['product_id']);
			$this->_data['product'] = $product;
			$this->_data['income_info'] =  $this->income_test_model->get_income_washer($this->_data['income_id']);
			//$this->_data['income_data'] =  $this->income_test_model->get_income_washer_tested($this->_data['income_id']);
			$this->_data['shipper'] =  $this->washer_income->getsupplier_name($this->_data['income_info']['income_supplier_id']);
			$this->_data['size'] = $this->washer_income->get_size($this->_data['income_info']['size_id']);
			$this->_data['cer_size_list'] = $this->income_test_model->get_product_standard_size_washer($product['product_id'],$this->_data['income_info']['size_id']);
			
			$this->_data['washer_thickness'] = $this->income_test_model->get_thicknesstest_info_byincomeid_washer($income_id);
			
			$this->_data['washer_thickness_info'] = $this->income_test_model->get_thickness_by_id_income($this->_data['washer_thickness']['thickness_id']); 
			//$this->_data['income_cer_size_data'] =  $this->income_test_model->get_income_washer_cer_size_tested($this->_data['income_id']);
			header("Content-type: application/vnd.ms-excel");
			header("Content-Disposition: attachment;Filename=income_washer_report_".$income_id.".xls");
			$this->load->view('administrator/views/income_manager/print_washer', $this->_data);
		}else{
			$this->session->set_flashdata("message-warning","ข้อมูลยังไม่ได้ถูกทดสอบ.");
			admin_redirect("income_manager/washer_list");
		}
				
	}
	
	public function print_spring($income_id)
	{
		$this->load->model('income_test_model');
		$this->load->model('administrator/spring_income');
		$this->_data['income_id'] = $income_id;
		$checked = $this->income_test_model->check_income_spring_tested($this->_data['income_id']);
		if($checked > 0){
			$product = $this->income_test_model->get_spring_product($this->_data['income_id']);
			$this->_data['std_list'] = $this->income_test_model->get_product_standard_spring($product['product_id']);
			$this->_data['product'] = $product;
			$this->_data['income_info'] =  $this->income_test_model->get_income_spring($this->_data['income_id']);
			
			$this->_data['shipper'] =  $this->spring_income->getsupplier_name($this->_data['income_info']['income_supplier_id']);
			$this->_data['size'] = $this->spring_income->get_size($this->_data['income_info']['size_id']);
			$this->_data['cer_size_list'] = $this->income_test_model->get_product_standard_size_spring($product['product_id'],$this->_data['income_info']['size_id']);
			
			$this->_data['spring_thickness'] = $this->income_test_model->get_thicknesstest_info_byincomeid_spring($income_id);
			
			$this->_data['spring_thickness_info'] = $this->income_test_model->get_thickness_by_id_income($this->_data['spring_thickness']['thickness_id']); 
			
			header("Content-type: application/vnd.ms-excel");
			header("Content-Disposition: attachment;Filename=income_spring_report_".$income_id.".xls");
			$this->load->view('administrator/views/income_manager/print_spring', $this->_data);
		}else{
			$this->session->set_flashdata("message-warning","ข้อมูลยังไม่ได้ถูกทดสอบ.");
			admin_redirect("income_manager/spring_list");
		}
				
	}
	
	public function print_taper($income_id)
	{
		$this->load->model('income_test_model');
		$this->load->model('administrator/taper_income');
		$this->_data['income_id'] = $income_id;
		$checked = $this->income_test_model->check_income_taper_tested($this->_data['income_id']);
		if($checked > 0){
			$product = $this->income_test_model->get_taper_product($this->_data['income_id']);
			$this->_data['std_list'] = $this->income_test_model->get_product_standard_taper($product['product_id']);
			$this->_data['product'] = $product;
			$this->_data['income_info'] =  $this->income_test_model->get_income_taper($this->_data['income_id']);
			
			$this->_data['shipper'] =  $this->taper_income->getsupplier_name($this->_data['income_info']['income_supplier_id']);
			$this->_data['size'] = $this->taper_income->get_size($this->_data['income_info']['size_id']);
			$this->_data['cer_size_list'] = $this->income_test_model->get_product_standard_size_taper($product['product_id'],$this->_data['income_info']['size_id']);
			
			$this->_data['taper_thickness'] = $this->income_test_model->get_thicknesstest_info_byincomeid_taper($income_id);
			
			$this->_data['taper_thickness_info'] = $this->income_test_model->get_thickness_by_id_income($this->_data['taper_thickness']['thickness_id']); 
			
			header("Content-type: application/vnd.ms-excel");
			header("Content-Disposition: attachment;Filename=income_taper_report_".$income_id.".xls");
			$this->load->view('administrator/views/income_manager/print_taper', $this->_data);
		}else{
			$this->session->set_flashdata("message-warning","ข้อมูลยังไม่ได้ถูกทดสอบ.");
			admin_redirect("income_manager/taper_list");
		}
				
	}
	
	public function print_stud($income_id)
	{
		$this->load->model('income_test_model');
		$this->load->model('administrator/stud_income');
		$this->_data['income_id'] = $income_id;
		$checked = $this->income_test_model->check_income_stud_tested($this->_data['income_id']);
		if($checked > 0){
			$product = $this->income_test_model->get_stud_product($this->_data['income_id']);
			$this->_data['std_list'] = $this->income_test_model->get_product_standard_stud($product['product_id']);
			$this->_data['product'] = $product;
			$this->_data['income_info'] =  $this->income_test_model->get_income_stud($this->_data['income_id']);
			
			$this->_data['shipper'] =  $this->stud_income->getsupplier_name($this->_data['income_info']['income_supplier_id']);
			$this->_data['size'] = $this->stud_income->get_size($this->_data['income_info']['size_id']);
			$this->_data['cer_size_list'] = $this->income_test_model->get_product_standard_size_stud($product['product_id'],$this->_data['income_info']['size_id']);
			
			$this->_data['stud_thickness'] = $this->income_test_model->get_thicknesstest_info_byincomeid_stud($income_id);
			
			$this->_data['stud_thickness_info'] = $this->income_test_model->get_thickness_by_id_income($this->_data['stud_thickness']['thickness_id']); 
			
			header("Content-type: application/vnd.ms-excel");
			header("Content-Disposition: attachment;Filename=income_stud_report_".$income_id.".xls");
			$this->load->view('administrator/views/income_manager/print_stud', $this->_data);
		}else{
			$this->session->set_flashdata("message-warning","ข้อมูลยังไม่ได้ถูกทดสอบ.");
			admin_redirect("income_manager/stud_list");
		}
				
	}
	
	
}