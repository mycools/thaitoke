<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Company extends CI_Controller {
	var $_data = array();
	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->library('image_lib');
		$this->load->library('admin_library');	
		$this->admin_library->forceLogin();
		$this->load->model('company_model');
		$this->load->model('administrator/admin_model');
	}
	public function index()
	{
			
	}
	
	#############Supplier #######################
	
	public function supplier_list($page=1)
	{
		$this->admin_model->set_menu_key('dc21d8fa23801035cb9eb8da98343d92');
		if(!$this->admin_model->check_permision("r")){
			show_error("สิทธิการเข้าถึงของคุณไม่ถูกต้อง.");
		}
		$this->admin_library->setTitle('Manage Supplier','icon-group');
		$this->admin_library->setDetail("การจัดการ Supplier");
		
		
		
		$this->admin_model->initd($this);
		
		$limit = 25;
		$offset = ($page-1)*$limit;
		$this->admin_model->set_pagination("company/supplier_list",$this->company_model->dataTable_Count(),$limit,4);
		
		$this->admin_model->set_title("การจัดการ Supplier",'icon-group');
		
		$this->admin_model->set_top_button("เพิ่ม","company/add_supplier",'icon-plus','btn-success','w');
		
		$this->admin_model->set_datatable($this->company_model->dataTable($limit,$offset));
		$this->admin_model->set_column("supplier_name","Name",0,'icon-group');
		$this->admin_model->set_column("supplier_email","Email",100,'icon-check-square');
		$this->admin_model->set_column("supplier_tel","Telephone",0,'icon-list-ol','hidden-phone');
 		//$this->admin_model->set_column_callback("company_status","company_status_callback");
 		
		//$this->admin_model->set_action_button("ข้อมูลกลุ่ม","user_group/view/[group_id]",'icon-building','btn-success','r');
		$this->admin_model->set_action_button("แก้ไข","company/edit_supplier/[supplier_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("ลบ","company/delete_supplier/[supplier_id]",'icon-trash-o','btn-danger','d');
		
		
		$this->admin_model->make_list();
		
		
		$this->admin_library->output();
		
	}
	
	public function add_supplier()
	{
		$this->admin_model->set_menu_key('dc21d8fa23801035cb9eb8da98343d92');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("Company/supplier_list");
		}
		$menu_name = "การจัดการ Supplier";
		$this->_data['_menu_name']="Add Data";
		$this->_data['province'] = $this->company_model->get_province();
		if(!$_POST){
			$this->admin_library->view("company/add_supplier",$this->_data);
			$this->admin_library->output();
		}else{
			
			$data['supplier_name'] = $this->input->post('supplier_name');
			$data['supplier_address'] = $this->input->post('supplier_address');
			$data['supplier_province'] = $this->input->post('supplier_province');
			$data['supplier_email'] = $this->input->post('supplier_email');
			$data['supplier_tel'] = $this->input->post('supplier_tel');
			$data['supplier_fax'] = $this->input->post('supplier_fax');
			
		/*var_dump($data);
		exit();*/
					
			
			$supplier_id=$this->company_model->add_supplier($data);
			
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("company/supplier_list");
		}
	}
	
	public function edit_supplier($supplier_id)
	{
		$this->admin_model->set_menu_key('dc21d8fa23801035cb9eb8da98343d92');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการแก้ไขข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("company/supplier_list");
		}
		$menu_name = "การจัดการ Supplier";
		$this->_data['row']=$this->company_model->get_supplier_detail($supplier_id);
		$this->_data['_menu_name']="Edit supplier";
		$this->_data['province'] = $this->company_model->get_province();
		if(!$_POST){
			$this->admin_library->view("company/edit_supplier",$this->_data);
			$this->admin_library->output();
		}else{
			$data['supplier_id'] = $this->input->post('supplier_id');
			$data['supplier_name'] = $this->input->post('supplier_name');
			$data['supplier_address'] = $this->input->post('supplier_address');
			$data['supplier_province'] = $this->input->post('supplier_province');
			$data['supplier_email'] = $this->input->post('supplier_email');
			$data['supplier_tel'] = $this->input->post('supplier_tel');
			$data['supplier_fax'] = $this->input->post('supplier_fax');
			
		/*var_dump($data);
		exit();*/
		
			
			$supplier_id=$this->company_model->edit_supplier($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("company/supplier_list");
		}
	}
	
	
	function delete_supplier($supplier_id)
	{
		$this->admin_model->set_menu_key('dc21d8fa23801035cb9eb8da98343d92');
		if(!$this->admin_model->check_permision("d")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการลบข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("company/supplier_list");
		}else{
	
		$this->company_model->delete_supplier($supplier_id);
		admin_redirect("company/supplier_list");
		
		}
	}

	########## end supplier ###############################
	
	############# Customer #################################
	
		public function customer_list($page=1)
	{
		$this->admin_model->set_menu_key('6bda158193aaec74f6409a7f315b3e09');
		if(!$this->admin_model->check_permision("r")){
			show_error("สิทธิการเข้าถึงของคุณไม่ถูกต้อง.");
		}
		$this->admin_library->setTitle('Manage customer','icon-group');
		$this->admin_library->setDetail("การจัดอันดับของ customer");
		
		
		
		$this->admin_model->initd($this);
		
		$limit = 25;
		$offset = ($page-1)*$limit;
		$this->admin_model->set_pagination("company/customer_list",$this->company_model->dataTable_customer_Count(),$limit,4);
		
		$this->admin_model->set_title("การจัดอันดับของ customer",'icon-group');
		
		$this->admin_model->set_top_button("เพิ่ม","company/add_customer",'icon-plus','btn-success','w');
		
		$this->admin_model->set_datatable($this->company_model->dataTable_customer($limit,$offset));
		$this->admin_model->set_column("customer_name","Name",0,'icon-group');
		$this->admin_model->set_column("customer_email","Email",100,'icon-check-square');
		$this->admin_model->set_column("customer_tel","Telephone",0,'icon-list-ol','hidden-phone');
 		//$this->admin_model->set_column_callback("company_status","company_status_callback");
 		
		//$this->admin_model->set_action_button("ข้อมูลกลุ่ม","user_group/view/[group_id]",'icon-building','btn-success','r');
		$this->admin_model->set_action_button("แก้ไข","company/edit_customer/[customer_id]",'icon-edit','btn-info','w');
		$this->admin_model->set_action_button("ลบ","company/delete_customer/[customer_id]",'icon-trash-o','btn-danger','d');
		
		
		$this->admin_model->make_list();
		
		
		$this->admin_library->output();
		
	}
	
	public function add_customer()
	{
		$this->admin_model->set_menu_key('6bda158193aaec74f6409a7f315b3e09');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("Company/customer_list");
		}
		$menu_name = "การจัดอันดับของ customer";
		$this->_data['_menu_name']="Add Data";
		$this->_data['province'] = $this->company_model->get_province();
		if(!$_POST){
			$this->admin_library->view("company/add_customer",$this->_data);
			$this->admin_library->output();
		}else{
			
			$data['customer_name'] = $this->input->post('customer_name');
			$data['customer_address'] = $this->input->post('customer_address');
			$data['customer_province'] = $this->input->post('customer_province');
			$data['customer_email'] = $this->input->post('customer_email');
			$data['customer_tel'] = $this->input->post('customer_tel');
			$data['customer_fax'] = $this->input->post('customer_fax');
			
		/*var_dump($data);
		exit();*/
					
			
			$customer_id=$this->company_model->add_customer($data);
			
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("company/customer_list");
		}
	}
	
	public function edit_customer($customer_id)
	{
		$this->admin_model->set_menu_key('6bda158193aaec74f6409a7f315b3e09');
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการแก้ไขข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("company/customer_list");
		}
		$menu_name = "การจัดอันดับของ customer";
		$this->_data['row']=$this->company_model->get_customer_detail($customer_id);
		$this->_data['_menu_name']="Edit customer";
		$this->_data['province'] = $this->company_model->get_province();
		if(!$_POST){
			$this->admin_library->view("company/edit_customer",$this->_data);
			$this->admin_library->output();
		}else{
			$data['customer_id'] = $this->input->post('customer_id');
			$data['customer_name'] = $this->input->post('customer_name');
			$data['customer_address'] = $this->input->post('customer_address');
			$data['customer_province'] = $this->input->post('customer_province');
			$data['customer_email'] = $this->input->post('customer_email');
			$data['customer_tel'] = $this->input->post('customer_tel');
			$data['customer_fax'] = $this->input->post('customer_fax');
			
		/*var_dump($data);
		exit();*/
		
			
			$customer_id=$this->company_model->edit_customer($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("company/customer_list");
		}
	}
	
	
	function delete_customer($customer_id)
	{
		$this->admin_model->set_menu_key('6bda158193aaec74f6409a7f315b3e09');
		if(!$this->admin_model->check_permision("d")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการลบข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("company/customer_list");
		}else{
	
		$this->company_model->delete_customer($customer_id);
		admin_redirect("company/customer_list");
		
		}
	}
	
	########### end customer ###############################
	
	
	
	
	
}