<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Income_test_anchor extends CI_Controller {
	var $_data = array();
	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->library('image_lib');
		$this->load->library('admin_library');	
		$this->admin_library->forceLogin();
		$this->load->model('income_test_anchor_model');
		$this->load->model('administrator/admin_model');
	}
	public function index()
	{
			
	}
	
	#############Supplier #######################
	
		
	public function add_anchor_testing($income_id=0,$type)
	{
		if($type=="axel"){
			$this->admin_model->set_menu_key('ca4e503a3c9fe5af42d1e7d4c18e1abc');
		}else{
			$this->admin_model->set_menu_key('91f3758c72ce63009ee6d3ee769df48e');
		}
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("income_manager/anchor_list/".$type);
		}
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size','trim');
		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		$checked = $this->income_test_anchor_model->check_income_tested($this->_data['income_id']);
		
		
		
		if($checked > 0){
			admin_redirect("income_test_anchor/edit_anchor_testing/$income_id/".$type);
		}
		
		$product = $this->income_test_anchor_model->get_anchor_product($this->_data['income_id']);
		
		$this->_data['income_info'] =  $this->income_test_anchor_model->get_income_anchor($this->_data['income_id']);
		$this->_data['std_list'] = $this->income_test_anchor_model->get_product_standard_anchor($product['product_id']);
		//$this->_data['cer_size_list'] = $this->income_test_anchor_model->get_product_standard_size_anchor($product['product_id'],$this->_data['income_info']['size_id']);
		$this->_data['product'] = $product;
		
		if($this->_data['std_list']->num_rows()==0){
			$this->session->set_flashdata("message-warning","ไม่สามารถทดสอบได้ เนื่องจากไม่มีค่า Standard");
			admin_redirect("income_manager_anchor/anchor_list/".$type);
		}
		
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("income_test_anchor/add_anchor_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['anchor_testing_post_user_id'] = $this->admin_library->user_id();
										
			
			$anchor_testing_std=$this->income_test_anchor_model->add_anchor_testing_std($data);
		
			
			if($anchor_testing_std == "pass"){
				$this->income_test_anchor_model->update_product_qty($this->_data['income_info']['size_id'],$this->_data['income_info']['income_quantity'], $income_id);
				
			}
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("income_manager_anchor/anchor_list/".$type);
		}
	}

	public function edit_anchor_testing($income_id=0,$type)
	{
		if($type=="axel"){
			$this->admin_model->set_menu_key('ca4e503a3c9fe5af42d1e7d4c18e1abc');
		}else{
			$this->admin_model->set_menu_key('91f3758c72ce63009ee6d3ee769df48e');
		}
		if(!$this->admin_model->check_permision("w")){
			$this->session->set_flashdata("message-warning","สิทธิการเข้าถึงของคุณไม่ถูกต้อง. คุณไม่รับสิทธิในการเพิ่มข้อมูล กรุณาติดต่อผู้ดูแลระบบเพื่อแจ้งถึงปัญหาที่เกิดขึ้น");
			admin_redirect("income_manager_anchor/anchor_list/".$type);
		}
		
		$this->form_validation->set_rules('test1[]','ทดสอบครั้งที่1','trim');
		$this->form_validation->set_rules('test2[]','ทดสอบครั้งที่2','trim');
		$this->form_validation->set_rules('test3[]','ทดสอบครั้งที่3','trim');
		$this->form_validation->set_rules('test4[]','ทดสอบครั้งที่4','trim');
		$this->form_validation->set_rules('test5[]','ทดสอบครั้งที่5','trim');
		$this->form_validation->set_rules('testing_status[]','สถานะการทดสอบ','trim');
		
		$this->form_validation->set_rules('ctest1[]','ทดสอบ size ครั้งที่1','trim');
		$this->form_validation->set_rules('ctest2[]','ทดสอบ size ครั้งที่2','trim');
		$this->form_validation->set_rules('ctest3[]','ทดสอบ size ครั้งที่3','trim');
		$this->form_validation->set_rules('ctest4[]','ทดสอบ size ครั้งที่4','trim');
		$this->form_validation->set_rules('ctest5[]','ทดสอบ size ครั้งที่5','trim');
		$this->form_validation->set_rules('ctesting_status[]','สถานะการทดสอบ size','trim');
		
		$menu_name = "";
		$this->_data['_menu_name']="Add Data";
		$this->_data['income_id'] = $income_id;
		
		$product = $this->income_test_anchor_model->get_anchor_product($this->_data['income_id']);
		
		$this->_data['income_info'] =  $this->income_test_anchor_model->get_income_anchor($this->_data['income_id']);
		$this->_data['std_list'] = $this->income_test_anchor_model->get_product_standard_anchor($product['product_id']);
		//$this->_data['cer_size_list'] = $this->income_test_anchor_model->get_product_standard_size_anchor($product['product_id'],$this->_data['income_info']['size_id']);
		$this->_data['product'] = $product;
		
		
		
		
		if($this->form_validation->run()===false){
			$this->admin_library->view("income_test_anchor/edit_anchor_testing",$this->_data);
			$this->admin_library->output();
		}else{
		
			$data['anchor_testing_post_user_id'] = $this->admin_library->user_id();
										
			
			$anchor_testing_std=$this->income_test_anchor_model->update_anchor_testing_std($data);
			
			if($anchor_testing_std == "pass"){
				$this->income_test_anchor_model->update_product_qty_edit($this->_data['income_info']['size_id'],$this->_data['income_info']['income_quantity'], $income_id);
				
			}
			
			//$anchor_testing_final = $this->income_test_anchor_model->update_anchor_testing_final($data);
			
			
			$this->session->set_flashdata("message-success","บันทึกการเปลี่ยนแปลงแมนู {$menu_name} เรียบร้อยแล้วค่ะ");
			admin_redirect("income_manager_anchor/anchor_list/".$type);
		}
	}
	
}