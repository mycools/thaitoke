<?php
$type = 'TrueType';
$name = 'ArialNarrow-BoldItalic';
$desc = array('Ascent'=>728,'Descent'=>-210,'CapHeight'=>728,'Flags'=>96,'FontBBox'=>'[-204 -307 1000 1107]','ItalicAngle'=>-10,'StemV'=>120,'MissingWidth'=>778);
$up = -106;
$ut = 105;
$cw = array(
	chr(0)=>778,chr(1)=>778,chr(2)=>778,chr(3)=>778,chr(4)=>778,chr(5)=>778,chr(6)=>778,chr(7)=>778,chr(8)=>778,chr(9)=>778,chr(10)=>778,chr(11)=>778,chr(12)=>778,chr(13)=>778,chr(14)=>778,chr(15)=>778,chr(16)=>778,chr(17)=>778,chr(18)=>778,chr(19)=>778,chr(20)=>778,chr(21)=>778,
	chr(22)=>778,chr(23)=>778,chr(24)=>778,chr(25)=>778,chr(26)=>778,chr(27)=>778,chr(28)=>778,chr(29)=>778,chr(30)=>778,chr(31)=>778,' '=>228,'!'=>273,'"'=>389,'#'=>456,'$'=>456,'%'=>729,'&'=>592,'\''=>195,'('=>273,')'=>273,'*'=>319,'+'=>479,
	','=>228,'-'=>273,'.'=>228,'/'=>228,'0'=>456,'1'=>456,'2'=>456,'3'=>456,'4'=>456,'5'=>456,'6'=>456,'7'=>456,'8'=>456,'9'=>456,':'=>273,';'=>273,'<'=>479,'='=>479,'>'=>479,'?'=>501,'@'=>800,'A'=>592,
	'B'=>592,'C'=>592,'D'=>592,'E'=>547,'F'=>501,'G'=>638,'H'=>592,'I'=>228,'J'=>456,'K'=>592,'L'=>501,'M'=>683,'N'=>592,'O'=>638,'P'=>547,'Q'=>638,'R'=>592,'S'=>547,'T'=>501,'U'=>592,'V'=>547,'W'=>774,
	'X'=>547,'Y'=>547,'Z'=>501,'['=>273,'\\'=>228,']'=>273,'^'=>479,'_'=>456,'`'=>273,'a'=>456,'b'=>501,'c'=>456,'d'=>501,'e'=>456,'f'=>273,'g'=>501,'h'=>501,'i'=>228,'j'=>228,'k'=>456,'l'=>228,'m'=>729,
	'n'=>501,'o'=>501,'p'=>501,'q'=>501,'r'=>319,'s'=>456,'t'=>273,'u'=>501,'v'=>456,'w'=>638,'x'=>456,'y'=>456,'z'=>410,'{'=>319,'|'=>230,'}'=>319,'~'=>479,chr(127)=>778,chr(128)=>456,chr(129)=>778,chr(130)=>778,chr(131)=>778,
	chr(132)=>778,chr(133)=>820,chr(134)=>778,chr(135)=>778,chr(136)=>778,chr(137)=>778,chr(138)=>778,chr(139)=>778,chr(140)=>778,chr(141)=>778,chr(142)=>778,chr(143)=>778,chr(144)=>778,chr(145)=>228,chr(146)=>228,chr(147)=>410,chr(148)=>410,chr(149)=>287,chr(150)=>456,chr(151)=>820,chr(152)=>778,chr(153)=>778,
	chr(154)=>778,chr(155)=>778,chr(156)=>778,chr(157)=>778,chr(158)=>778,chr(159)=>778,chr(160)=>228,chr(161)=>778,chr(162)=>778,chr(163)=>778,chr(164)=>778,chr(165)=>778,chr(166)=>778,chr(167)=>778,chr(168)=>778,chr(169)=>778,chr(170)=>778,chr(171)=>778,chr(172)=>778,chr(173)=>778,chr(174)=>778,chr(175)=>778,
	chr(176)=>778,chr(177)=>778,chr(178)=>778,chr(179)=>778,chr(180)=>778,chr(181)=>778,chr(182)=>778,chr(183)=>778,chr(184)=>778,chr(185)=>778,chr(186)=>778,chr(187)=>778,chr(188)=>778,chr(189)=>778,chr(190)=>778,chr(191)=>778,chr(192)=>778,chr(193)=>778,chr(194)=>778,chr(195)=>778,chr(196)=>778,chr(197)=>778,
	chr(198)=>778,chr(199)=>778,chr(200)=>778,chr(201)=>778,chr(202)=>778,chr(203)=>778,chr(204)=>778,chr(205)=>778,chr(206)=>778,chr(207)=>778,chr(208)=>778,chr(209)=>778,chr(210)=>778,chr(211)=>778,chr(212)=>778,chr(213)=>778,chr(214)=>778,chr(215)=>778,chr(216)=>778,chr(217)=>778,chr(218)=>778,chr(219)=>778,
	chr(220)=>778,chr(221)=>778,chr(222)=>778,chr(223)=>778,chr(224)=>778,chr(225)=>778,chr(226)=>778,chr(227)=>778,chr(228)=>778,chr(229)=>778,chr(230)=>778,chr(231)=>778,chr(232)=>778,chr(233)=>778,chr(234)=>778,chr(235)=>778,chr(236)=>778,chr(237)=>778,chr(238)=>778,chr(239)=>778,chr(240)=>778,chr(241)=>778,
	chr(242)=>778,chr(243)=>778,chr(244)=>778,chr(245)=>778,chr(246)=>778,chr(247)=>778,chr(248)=>778,chr(249)=>778,chr(250)=>778,chr(251)=>778,chr(252)=>778,chr(253)=>778,chr(254)=>778,chr(255)=>778);
$enc = 'cp874';
$diff = '130 /.notdef /.notdef /.notdef 134 /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef 142 /.notdef 152 /.notdef /.notdef /.notdef /.notdef /.notdef 158 /.notdef /.notdef 161 /kokaithai /khokhaithai /khokhuatthai /khokhwaithai /khokhonthai /khorakhangthai /ngonguthai /chochanthai /chochingthai /chochangthai /sosothai /chochoethai /yoyingthai /dochadathai /topatakthai /thothanthai /thonangmonthothai /thophuthaothai /nonenthai /dodekthai /totaothai /thothungthai /thothahanthai /thothongthai /nonuthai /bobaimaithai /poplathai /phophungthai /fofathai /phophanthai /fofanthai /phosamphaothai /momathai /yoyakthai /roruathai /ruthai /lolingthai /luthai /wowaenthai /sosalathai /sorusithai /sosuathai /hohipthai /lochulathai /oangthai /honokhukthai /paiyannoithai /saraathai /maihanakatthai /saraaathai /saraamthai /saraithai /saraiithai /sarauethai /saraueethai /sarauthai /sarauuthai /phinthuthai /.notdef /.notdef /.notdef /.notdef /bahtthai /saraethai /saraaethai /saraothai /saraaimaimuanthai /saraaimaimalaithai /lakkhangyaothai /maiyamokthai /maitaikhuthai /maiekthai /maithothai /maitrithai /maichattawathai /thanthakhatthai /nikhahitthai /yamakkanthai /fongmanthai /zerothai /onethai /twothai /threethai /fourthai /fivethai /sixthai /seventhai /eightthai /ninethai /angkhankhuthai /khomutthai /.notdef /.notdef /.notdef /.notdef';
$uv = array(0=>array(0,128),128=>8364,133=>8230,145=>array(8216,2),147=>array(8220,2),149=>8226,150=>array(8211,2),160=>160,161=>array(3585,58),223=>array(3647,29));
$file = 'arialnarrowbi.z';
$originalsize = 40820;
$subsetted = true;
?>
