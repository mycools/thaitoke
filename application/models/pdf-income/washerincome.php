<?php
error_reporting(0);
class Washerincome extends CI_Model{
	var $_data = array();
	var $haswasherInfo = false;
	var $hasWasherInfo = false;
	function make($income_id)
	{
		$this->fpdf = new PDF_MC_Table();
		$this->fpdf->SetMargins(10,5,5);
		$this->fpdf->AddFont('angsa','','angsa.php');
		$this->fpdf->AddFont('angsa','B','angsab.php');
		
		$this->fpdf->AddPage('L');	
		$this->fpdf->SetAutoPageBreak(false);
		$this->getData($income_id);
		$this->_Header($income_id);
		$this->_Body($income_id);
		$this->_Footer($income_id);
		//Output
		$this->fpdf->Output('pdf/washer-income-'.$income_id.'.pdf',"I");
	}
	function getData($income_id)
	{
		
		$this->_data['income_id'] = $income_id;
		
		
		$this->load->model('income_test_model');
		$this->load->model('administrator/washer_income');
		
		$checked = $this->income_test_model->check_income_washer_tested($this->_data['income_id']);
		if($checked > 0){
			$product = $this->income_test_model->get_washer_product($this->_data['income_id']);
			$this->_data['std_list'] = $this->income_test_model->get_product_standard_washer($product['product_id'])->result_array();
			$this->_data['product'] = $product;
			$this->_data['income_info'] =  $this->income_test_model->get_income_washer($this->_data['income_id']);
			//$this->_data['income_data'] =  $this->income_test_model->get_income_washer_tested($this->_data['income_id']);
			$this->_data['shipper'] =  $this->washer_income->getsupplier_name($this->_data['income_info']['income_supplier_id']);
			$this->_data['size'] = $this->washer_income->get_size($this->_data['income_info']['size_id']);
			$this->_data['cer_size_list'] = $this->income_test_model->get_product_standard_size_washer($product['product_id'],$this->_data['income_info']['size_id'])->result_array();
			
			$this->_data['washer_thickness'] = $this->income_test_model->get_thicknesstest_info_byincomeid_washer($income_id);
			
			$this->_data['washer_thickness_info'] = $this->income_test_model->get_thickness_by_id_income($this->_data['washer_thickness']['thickness_id']); 
			//$this->_data['income_cer_size_data'] =  $this->income_test_model->get_income_washer_cer_size_tested($this->_data['income_id']);
			if($this->input->get('debug')=="1"){
				echo @json_encode($this->_data);exit();
			}
		}else{
			$this->session->set_flashdata("message-warning","ข้อมูลยังไม่ได้ถูกทดสอบ.");
			admin_redirect("income_manager/washer_list");
		}
		
	}
	function _Header($income_id)
	{
		$certificate_logo = $this->settings->item('certificate_logo');
		$certificate_company_th = $this->settings->item('certificate_company_th');
		$certificate_company_en = $this->settings->item('certificate_company_en');
		$certificate_company_shortname = $this->settings->item('certificate_company_shortname');
		$certificate_company_address_1 = $this->settings->item('certificate_company_address_1');
		$certificate_company_address_2 = $this->settings->item('certificate_company_address_2');
		$certificate_company_address_3 = $this->settings->item('certificate_company_address_3');
		$certificate_company_address_4 = $this->settings->item('certificate_company_address_4');
		$certificate_factory_address = $this->settings->item('certificate_factory_address');
		
		$this->fpdf->SetFont('angsa','B',18);
		$this->fpdf->Line(5, 5, 5, 205);
		$this->fpdf->Line(292, 5, 292, 205);
		$this->fpdf->Line(5, 5, 292, 5);
		$this->fpdf->Line(5, 205, 292, 205);
		
		$start_Y=6;
		$this->fpdf->SetY($start_Y);
		$this->fpdf->SetTextColor(253,191,45);
		$this->fpdf->Cell(94 , 8, setUTF8($certificate_company_shortname), 0, 0 , 'L');
		$this->fpdf->SetTextColor(0,0,0);
		$this->fpdf->Cell(94 , 8, setUTF8('INCOMING INSPECTION SHEET'), 0, 0 , 'C');
		$this->fpdf->Cell(94  , 8 , $this->fpdf->Image('public/uploads/settings/'.$certificate_logo,279,($this->fpdf->GetY()),12) , 0, 1 , 'R' );
		
		$this->fpdf->Line(5, 16, 292, 16);
		$start_Y=17;
		$this->fpdf->SetY($start_Y);
		$fontSize = 14;
		
		$this->fpdf->SetFont('angsa','B',$fontSize);
		$this->fpdf->Cell(25 , 5, setUTF8('Commodity :'), 0, 0 , 'R');
		$this->fpdf->SetFont('angsa','',$fontSize);
		$this->fpdf->Cell(25 , 5, setUTF8($this->_data['income_info']['income_commodity']), 'B', 0 , 'L');
		
		$this->fpdf->SetFont('angsa','B',$fontSize);
		$this->fpdf->Cell(25 , 5, setUTF8('Incom ID :'), 0, 0 , 'R');
		$this->fpdf->SetFont('angsa','',$fontSize);
		$this->fpdf->Cell(35 , 5, setUTF8($this->_data['income_info']['income_job_id']), 'B', 0 , 'L');
		
		$this->fpdf->SetFont('angsa','B',$fontSize);
		$this->fpdf->Cell(35 , 5, setUTF8('P/O Number :'), 0, 0 , 'R');
		$this->fpdf->SetFont('angsa','',$fontSize);
		$this->fpdf->Cell(35 , 5, setUTF8($this->_data['income_info']['income_po_no']), 'B', 0 , 'L');
		
		$this->fpdf->SetFont('angsa','B',$fontSize);
		$this->fpdf->Cell(25 , 5, setUTF8('Certificate No :'), 0, 0 , 'R');
		$this->fpdf->SetFont('angsa','',$fontSize);
		$this->fpdf->Cell(35 , 5, setUTF8($this->_data['income_info']['income_certificate_no']), 'B', 1 , 'L');
		
		$this->fpdf->Ln(1);
		
		$this->fpdf->SetFont('angsa','B',$fontSize);
		$this->fpdf->Cell(25 , 5, setUTF8('Quantity :'), 0, 0 , 'R');
		$this->fpdf->SetFont('angsa','',$fontSize);
		$this->fpdf->Cell(25 , 5, setUTF8($this->_data['income_info']['income_quantity']), 'B', 0 , 'L');
		
		$this->fpdf->SetFont('angsa','B',$fontSize);
		$this->fpdf->Cell(25 , 5, setUTF8('Invoice No :'), 0, 0 , 'R');
		$this->fpdf->SetFont('angsa','',$fontSize);
		$this->fpdf->Cell(35 , 5, setUTF8($this->_data['income_info']['income_invoice_no']), 'B', 0 , 'L');
		
		$this->fpdf->SetFont('angsa','B',$fontSize);
		$this->fpdf->Cell(35 , 5, setUTF8('Purchase Order No :'), 0, 0 , 'R');
		$this->fpdf->SetFont('angsa','',$fontSize);
		$this->fpdf->Cell(35 , 5, setUTF8($this->_data['income_info']['income_purchase_no']), 'B', 0 , 'L');
		
		$this->fpdf->SetFont('angsa','B',$fontSize);
		$this->fpdf->Cell(25 , 5, setUTF8('Date :'), 0, 0 , 'R');
		$this->fpdf->SetFont('angsa','',$fontSize);
		$this->fpdf->Cell(35 , 5, setUTF8(date("d/m/Y")), 'B', 1 , 'L');
		
		$this->fpdf->Ln(1);
		
		$this->fpdf->SetFont('angsa','B',$fontSize);
		$this->fpdf->Cell(30 , 5, setUTF8('Name Of Supplier :'), 0, 0 , 'R');
		$this->fpdf->SetFont('angsa','',$fontSize);
		$this->fpdf->Cell(80 , 5, setUTF8($this->_data['shipper']['supplier_name']), 'B', 1 , 'L');
		
		$this->fpdf->Line(5, 38, 292, 38);
		
		$start_Y=38;
		$this->fpdf->SetY($start_Y);
		
		$this->fpdf->SetFont('angsa','B',$fontSize);
		$this->fpdf->Cell(35 , 5, setUTF8('Bolt Grade :'), 0, 0 , 'R');
		$this->fpdf->SetFont('angsa','',$fontSize);
		$this->fpdf->Cell(25 , 5, setUTF8($this->_data['product']['product_name']), 'B', 0 , 'L');
		
		$this->fpdf->SetFont('angsa','B',$fontSize);
		$this->fpdf->Cell(35 , 5, setUTF8('Size :'), 0, 0 , 'R');
		$this->fpdf->SetFont('angsa','',$fontSize);
		
		$size_display = implode('x',array($this->_data['size']['size_m'],$this->_data['size']['size_p'],$this->_data['size']['size_length']));
		$this->fpdf->Cell(35 , 5, setUTF8($size_display), 'B', 0 , 'L');
		
		$this->fpdf->SetFont('angsa','B',$fontSize);
		$this->fpdf->Cell(35 , 5, setUTF8('Surface Condition :'), 0, 0 , 'R');
		$this->fpdf->SetFont('angsa','',$fontSize);
		$this->fpdf->Cell(35 , 5, setUTF8($this->_data['product']['product_type']), 'B', 0 , 'L');
		
		$this->fpdf->SetFont('angsa','B',$fontSize);
		$this->fpdf->Cell(35 , 5, setUTF8('Heat No :'), 0, 0 , 'R');
		$this->fpdf->SetFont('angsa','',$fontSize);
		$this->fpdf->Cell(35 , 5, setUTF8($this->_data['income_info']['income_heat_no']), 'B', 1 , 'L');
		
		$this->fpdf->Ln(2);
		
	}
	
	function _Body($income_id)
	{
		$this->fpdf->SetFont('angsa','B',10);
		$row_height=5;
		$start_Y=$this->fpdf->GetY();
		$this->fpdf->Cell( 10  , $row_height*2 , setUTF8('No.') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 43  , $row_height*2 , setUTF8('DESCRIPTION') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 30  , $row_height , setUTF8('STANDARD') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 30  , $row_height*2 , setUTF8('INSECTION TOOL') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 30  , $row_height , setUTF8('FREQUENCY') , 'LTR', 0 , 'C' );
		$this->fpdf->Cell( 30  , $row_height , setUTF8('INSPECTION') , 'LTR', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height*2 , setUTF8('1') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height*2 , setUTF8('2') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height*2 , setUTF8('3') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height*2 , setUTF8('4') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height*2 , setUTF8('5') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height*2 , setUTF8('AVG') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 25  , $row_height*2 , setUTF8('OVERALL RESULT') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height*2 , setUTF8('REASON') , 'LTRB', 0 , 'C' );
		
		$this->fpdf->SetY($start_Y+5);
		$this->fpdf->SetX(63);
		$this->fpdf->Cell( 10  , $row_height , setUTF8('Min') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , setUTF8('Max') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , setUTF8('Unit') , 'LTRB', 0 , 'C' );
		
		$this->fpdf->SetY($start_Y+5);
		$this->fpdf->SetX(123);
		$this->fpdf->Cell( 30  , $row_height , setUTF8('OF TESTING') , 'LRB', 0 , 'C' );
		$this->fpdf->Cell( 30  , $row_height , setUTF8('QUANTITY') , 'LRB', 1 , 'C' );
		$no = 1;
		$this->fpdf->SetFont('angsa','',8);
		$row_height=4;
		foreach($this->_data['std_list'] as $row){
			$income_data =  $this->income_test_model->get_income_washer_tested($income_id,$row['ps_id']);  
			$ins = $this->income_test_model->get_inspection($row['inspection_id']);
			$this->fpdf->Cell( 10  , $row_height , setUTF8($no) , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 43  , $row_height , setUTF8($row['standard_description']) , 'LTRB', 0 , 'L' );
			$this->fpdf->Cell( 10  , $row_height , setUTF8($row['ps_min']) , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 10  , $row_height , setUTF8($row['ps_max']) , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 10  , $row_height , setUTF8($row['ps_max_unit']) , 'LTRB', 0 , 'C' );
			if($row['inspection_id'] != "6"){ 
				$this->fpdf->Cell( 30  , $row_height , setUTF8($ins['inspection_name']) , 'LTRB', 0 , 'L' );
				$this->fpdf->Cell( 30  , $row_height , setUTF8('Before Delivery') , 'LTRB', 0 , 'L' );
				$this->fpdf->Cell( 30  , $row_height , setUTF8('5 PCS/LOT') , 'LTRB', 0 , 'L' );
			}else{
				$this->fpdf->Cell( 30  , $row_height , setUTF8('') , 'LTRB', 0 , 'L' );
				$this->fpdf->Cell( 30  , $row_height , setUTF8('') , 'LTRB', 0 , 'L' );
				$this->fpdf->Cell( 30  , $row_height , setUTF8('') , 'LTRB', 0 , 'L' );
			}
			
			
			
			$this->fpdf->Cell( 10  , $row_height , setUTF8($income_data['washer_testing1']) , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 10  , $row_height , setUTF8($income_data['washer_testing2']) , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 10  , $row_height , setUTF8($income_data['washer_testing3']) , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 10  , $row_height , setUTF8($income_data['washer_testing4']) , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 10  , $row_height , setUTF8($income_data['washer_testing5']) , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 10  , $row_height , setUTF8($income_data['washer_testing_average']) , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 25  , $row_height , setUTF8(($income_data['washer_testing_status']=="pass")?"PASS":"NO") , 'LTRB', 0 , 'C' );
			
			$this->fpdf->Cell( 20  , $row_height , setUTF8($income_data['washer_testing_remark']) , 'LTRB', 1 , 'L' );
			
			$no++;
		}
		
		
		foreach($this->_data['cer_size_list'] as $row){
			$income_cer_size_data =  $this->income_test_model->get_income_washer_cer_size_tested($income_id,$row['ps_id']);
			$ins1 = $this->income_test_model->get_inspection($row['inspection_id']);
			$this->fpdf->Cell( 10  , $row_height , setUTF8($no) , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 43  , $row_height , setUTF8($row['ps_description']) , 'LTRB', 0 , 'L' );
			$this->fpdf->Cell( 10  , $row_height , setUTF8($row['ps_min']) , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 10  , $row_height , setUTF8($row['ps_max']) , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 10  , $row_height , setUTF8($row['ps_max_unit']) , 'LTRB', 0 , 'C' );
			if($row['inspection_id'] != "6"){ 
				$this->fpdf->Cell( 30  , $row_height , setUTF8($ins['inspection_name']) , 'LTRB', 0 , 'L' );
				$this->fpdf->Cell( 30  , $row_height , setUTF8('Before Delivery') , 'LTRB', 0 , 'L' );
				$this->fpdf->Cell( 30  , $row_height , setUTF8('5 PCS/LOT') , 'LTRB', 0 , 'L' );
			}else{
				$this->fpdf->Cell( 30  , $row_height , setUTF8('') , 'LTRB', 0 , 'L' );
				$this->fpdf->Cell( 30  , $row_height , setUTF8('') , 'LTRB', 0 , 'L' );
				$this->fpdf->Cell( 30  , $row_height , setUTF8('') , 'LTRB', 0 , 'L' );
			}
			
			
			
			$this->fpdf->Cell( 10  , $row_height , setUTF8($income_data['washer_testing1']) , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 10  , $row_height , setUTF8($income_data['washer_testing2']) , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 10  , $row_height , setUTF8($income_data['washer_testing3']) , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 10  , $row_height , setUTF8($income_data['washer_testing4']) , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 10  , $row_height , setUTF8($income_data['washer_testing5']) , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 10  , $row_height , setUTF8($income_data['washer_testing_average']) , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 25  , $row_height , setUTF8(($income_data['washer_testing_status']=="pass")?"PASS":"NO") , 'LTRB', 0 , 'C' );
			
			$this->fpdf->Cell( 20  , $row_height , setUTF8($income_data['washer_testing_remark']) , 'LTRB', 1 , 'L' );
			
			$no++;
		}
		
		$this->fpdf->Cell( 10  , $row_height , setUTF8($no) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 43  , $row_height , setUTF8('Grade Mark') , 'LTRB', 0 , 'L' );
		$this->fpdf->Cell( 30  , $row_height , setUTF8($this->_data['income_info']['grade_mark']) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 30  , $row_height , setUTF8('') , 'LTRB', 0 , 'L' );
		$this->fpdf->Cell( 30  , $row_height , setUTF8('') , 'LTRB', 0 , 'L' );
		$this->fpdf->Cell( 30  , $row_height , setUTF8('') , 'LTRB', 0 , 'L' );
		$this->fpdf->Cell( 10  , $row_height , setUTF8($this->_data['income_info']['grade_mark']) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , setUTF8($this->_data['income_info']['grade_mark']) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , setUTF8($this->_data['income_info']['grade_mark']) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , setUTF8($this->_data['income_info']['grade_mark']) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , setUTF8($this->_data['income_info']['grade_mark']) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , setUTF8($this->_data['income_info']['grade_mark']) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 25  , $row_height , setUTF8($this->_data['income_info']['grade_mark']) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , setUTF8('') , 'LTRB', 1 , 'L' );
		$no++;
		
		$this->fpdf->Cell( 10  , $row_height , setUTF8($no) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 43  , $row_height , setUTF8('Pitch') , 'LTRB', 0 , 'L' );
		$this->fpdf->Cell( 30  , $row_height , setUTF8($this->_data['size']['size_p']) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 30  , $row_height , setUTF8('') , 'LTRB', 0 , 'L' );
		$this->fpdf->Cell( 30  , $row_height , setUTF8('') , 'LTRB', 0 , 'L' );
		$this->fpdf->Cell( 30  , $row_height , setUTF8('') , 'LTRB', 0 , 'L' );
		$this->fpdf->Cell( 10  , $row_height , setUTF8($this->_data['size']['size_p']) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , setUTF8($this->_data['size']['size_p']) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , setUTF8($this->_data['size']['size_p']) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , setUTF8($this->_data['size']['size_p']) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , setUTF8($this->_data['size']['size_p']) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , setUTF8($this->_data['size']['size_p']) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 25  , $row_height , setUTF8($this->_data['size']['size_p']) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , setUTF8('') , 'LTRB', 1 , 'L' );
		$no++;
		
		$this->fpdf->Cell( 10  , $row_height , setUTF8($no) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 43  , $row_height , setUTF8('Thickness Galvanized') , 'LTRB', 0 , 'L' );
		$this->fpdf->Cell( 20  , $row_height , setUTF8($this->_data['washer_thickness_info']['condition'].$this->_data['washer_thickness_info']['condition_value']) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , setUTF8($this->_data['washer_thickness_info']['unit']) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 30  , $row_height , setUTF8('') , 'LTRB', 0 , 'L' );
		$this->fpdf->Cell( 30  , $row_height , setUTF8('') , 'LTRB', 0 , 'L' );
		$this->fpdf->Cell( 30  , $row_height , setUTF8('') , 'LTRB', 0 , 'L' );
		$this->fpdf->Cell( 10  , $row_height , setUTF8($this->_data['washer_thickness_info']['condition_value']) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , setUTF8($this->_data['washer_thickness_info']['condition_value']) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , setUTF8($this->_data['washer_thickness_info']['condition_value']) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , setUTF8($this->_data['washer_thickness_info']['condition_value']) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , setUTF8($this->_data['washer_thickness_info']['condition_value']) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , setUTF8($this->_data['washer_thickness_info']['condition_value']) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 25  , $row_height , setUTF8($this->_data['washer_thickness_info']['condition_value']) , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , setUTF8('') , 'LTRB', 1 , 'L' );
		$no++;
		
		//Image site_url('public/uploads/product/bolt/'.$product['product_image'])
		$this->fpdf->Ln(3);
		$image = 'public/uploads/product/washer/'.$this->_data['product']['product_image'];
		if(file_exists($image) && is_file($image)){
			$this->fpdf->Cell(278  , 45 , $this->fpdf->Image($image,$this->fpdf->GetX()+20,($this->fpdf->GetY()),45) , 1, 1 , 'R' );
		}
		$this->fpdf->SetY(190);
		$this->fpdf->SetFont('angsa','B',10);
		$this->fpdf->Cell( 30  , 5 , setUTF8('') , 0, 0 , 'L' );
		$this->fpdf->Cell( 130  , 5 , setUTF8('Reported by : …………………………………………………………') , 0, 0 , 'L' );
		$this->fpdf->Cell( 90  , 5 , setUTF8('Approved by : …………………………………………………………') , 0, 1 , 'L' );
		
		$this->fpdf->Cell( 44  , 5 , setUTF8('') , 0, 0 , 'L' );
		$this->fpdf->Cell( 130  , 5 , setUTF8('Quality Assurance Section Officer') , 0, 0 , 'L' );
		$this->fpdf->Cell( 90  , 5 , setUTF8('Metallurgical and Quality Assurance Manager') , 0, 1 , 'L' );
	}
	function _Footer($income_id)
	{
		
	}
}