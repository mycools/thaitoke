<?php
/**
 * iAON
 *
 * An commercial application for online shopping
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2015 - 2099, At First Byte Company
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	AtFirstShop
 * @author	Mycools Inc.
 * @copyright	Copyright (c) 2008 - 2014, Mycools Inc. (http://mycools.in.th/)
 * @copyright	Copyright (c) 2015 - 2099, At First Byte Company (http://www.atfirstbyte.net/)
 * @license	http://www.atfirstbyte.net/licenses	AFB License
 * @link	http://atfirstbyte.net
 * @since	Version 3.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * report_anchor_model Class
 *
 * @package		iAON
 * @subpackage	report_anchor_model
 * @category	Model
 * @author		Mycools Inc.
 * @link		http://mycools.in.th/guide/general/model.html
 */
class report_anchor_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		$this->mssql = $this->load->database("mssql", true);
	}
	function get_income_full($income_id)
	{
		return $this->mssql->query("SELECT dbo.income_anchor.income_id, 
	dbo.supplier.supplier_name AS VENDER, 
	dbo.supplier.supplier_address AS ADDRESS, 
	dbo.income_anchor.income_job_id AS CONTRACT_JOB, 
	dbo.income_anchor.income_invoice_no AS INVOICE_NO, 
	dbo.product_size_anchor.size_m AS SIZEM, 
	dbo.product_size_anchor.size_length AS SIZEL, 
	dbo.product_size_anchor.size_t AS SIZET, 
	dbo.product_size_anchor.size_w AS SIZEW, 
	dbo.income_anchor.income_po_no, 
	dbo.income_anchor.income_test_no, 
	dbo.income_anchor.income_heat_no, 
	dbo.income_anchor.income_quantity, 
	dbo.product_anchor.product_grade, 
	dbo.product_anchor.product_thumbnail, 
	dbo.income_anchor.income_product_id,
	dbo.product_anchor.product_type, 
	dbo.product_anchor.product_low_carbon
FROM dbo.income_anchor INNER JOIN dbo.supplier ON dbo.supplier.supplier_id = dbo.income_anchor.income_supplier_id
	 INNER JOIN dbo.product_size_anchor ON dbo.product_size_anchor.size_id = dbo.income_anchor.size_id
	 INNER JOIN dbo.product_anchor ON dbo.product_anchor.product_id = dbo.income_anchor.income_product_id
WHERE dbo.income_anchor.income_id=".$income_id);
	}
	function get_standard($product_id)
	{
		$this->mssql->where("product_standard_anchor.product_id",$product_id);
		$this->mssql->limit(1);
		return $this->mssql->get("product_standard_anchor")->row_array();
	}
	function get_test($income_id)
	{
		$this->mssql->where("income_anchor_testing.income_id",$income_id);
		$this->mssql->limit(3);
		return $this->mssql->get("income_anchor_testing")->result_array();
	}
}

/* End of file report_anchor_model.php */
/* Location: ./application/models/report_anchor_model.php */