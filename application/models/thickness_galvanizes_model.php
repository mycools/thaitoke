<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Thickness_galvanizes_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		$this->mssql = $this->load->database("mssql", true);
		
	}
	##################### supplier #############################
	
	public function dataTable($limit=25,$offset = 0)
	{
		$offset = ($offset < 0)?0:$offset;

		$this->mssql->order_by("id","DESC");
		$this->mssql->limit($limit,$offset);
		return $this->mssql->get("thickness_galvanizes");
	}
	
	public function dataTable_Count()
	{
	
		return $this->mssql->count_all_results("thickness_galvanizes");
	}
	
	
	public function get_tg_detail($id)
	{
		$this->mssql->where("id",$id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("thickness_galvanizes")->row_array(); 
		return $row; 
	}
	
	public function add_tg($data)
	{
		$this->mssql->set('name',$data['name']);
		$this->mssql->set('grade',$data['grade']);
		$this->mssql->set('condition',$data['condition']);
		$this->mssql->set('condition_value',$data['condition_value']);
		$this->mssql->set('unit',$data['unit']);
		$this->mssql->set('tools',$data['tools']);
		$this->mssql->insert('thickness_galvanizes');
	}
	
	public function edit_tg($data)
	{
		
		$this->mssql->set('name',$data['name']);
		$this->mssql->set('grade',$data['grade']);
		$this->mssql->set('condition',$data['condition']);
		$this->mssql->set('condition_value',$data['condition_value']);	
		$this->mssql->set('unit',$data['unit']);
		$this->mssql->set('tools',$data['tools']);	
		$this->mssql->where('id',$data['id']);
		$this->mssql->update('thickness_galvanizes');
	}

		public function delete_tg($id)
	{
		

		$this->mssql->where("id",$id);
	    $this->mssql->delete("thickness_galvanizes");	
	}
	
	##################### end supplier #############################
	



}