<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Income_test_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		$this->mssql = $this->load->database("mssql", true);
		
	}
	##################### income test bolt #############################
	
		
	public function get_income_bolt($income_id)
	{
		$this->mssql->where("income_id",$income_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("income_bolt")->row_array(); 
		return $row; 
	}
	public function get_bolt_product($income_id)
	{
		$income = $this->get_income_bolt($income_id);
		
		$this->mssql->where("product_id",$income['income_product_id']);
		$this->mssql->limit(1);
		$row = $this->mssql->get("product_bolt")->row_array(); 
		return $row; 
	}
	
	public function get_income_bolt_test_info($income_id,$std_id)
	{
		$this->mssql->where("income_id",$income_id);
		$this->mssql->where("bolt_standard_id",$std_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("income_bolt_testing")->row_array(); 
		return $row;	
		
		
	}
	
	public function get_income_bolt_size_test_info($income_id,$std_id)
	{
		$this->mssql->where("income_id",$income_id);
		$this->mssql->where("bolt_cer_size_id",$std_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("size_bolt_testing")->row_array(); 
		return $row;	
		
		
	}
	
	public function get_inspection($inspection_id)
	{
		$this->mssql->where("inspection_id",$inspection_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("inspection")->row_array(); 
		return $row; 
	}
	
	public function get_product_standard_bolt($product_id)
	{
		//$this->mssql->where('(ps_min <> NULL OR ps_max <> NULL)');
		$this->mssql->where("product_id",$product_id);
		$row = $this->mssql->get("product_standard_bolt");
		return $row;
	}
	
	public function get_product_standard_size_bolt($product_id,$size_id)
	{
		/* $this->mssql->where('(ps_min <> NULL OR ps_max <> NULL)'); */
		$this->mssql->where("product_id",$product_id);
		$this->mssql->where("size_id",$size_id);
		$row = $this->mssql->get("product_cer_size_bolt"); 
		return $row;
	}
	
	public function add_bolt_testing_std($data)
	{
		/*
var_dump(@$_POST);
		exit();
*/		
		if(@$_POST['bolt_standard_id'] != NULL){
			foreach(@$_POST['bolt_standard_id'] as $key=>$value){
				$this->mssql->set('income_id',@$_POST['income_id']);
				$this->mssql->set('bolt_standard_id',@$_POST['bolt_standard_id'][$key]);
				$this->mssql->set('bolt_testing1',@$_POST['test1'][$key]);
				$this->mssql->set('bolt_testing2',@$_POST['test2'][$key]);
				$this->mssql->set('bolt_testing3',@$_POST['test3'][$key]);
				$this->mssql->set('bolt_testing4',@$_POST['test4'][$key]);
				$this->mssql->set('bolt_testing5',@$_POST['test5'][$key]);
				$this->mssql->set('bolt_testing_average',@$_POST['test_avg'][$key]);
				$this->mssql->set('bolt_testing_status',@$_POST['testing_status'][$key]);
				$this->mssql->set('bolt_testing_remark',@$_POST['remark'][$key]);
				$this->mssql->set('bolt_testing_post_user_id',$data['bolt_testing_post_user_id']);	
				$this->mssql->set('bolt_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('income_bolt_testing');
			}
		}
		
		if(@$_POST['bolt_cer_size_id'] != NULL){
			foreach(@$_POST['bolt_cer_size_id'] as $key=>$value){
				$this->mssql->set('income_id',@$_POST['income_id']);
				$this->mssql->set('bolt_cer_size_id',@$_POST['bolt_cer_size_id'][$key]);
				$this->mssql->set('bolt_testing1',@$_POST['ctest1'][$key]);
				$this->mssql->set('bolt_testing2',@$_POST['ctest2'][$key]);
				$this->mssql->set('bolt_testing3',@$_POST['ctest3'][$key]);
				$this->mssql->set('bolt_testing4',@$_POST['ctest4'][$key]);
				$this->mssql->set('bolt_testing5',@$_POST['ctest5'][$key]);
				$this->mssql->set('bolt_testing_average',@$_POST['ctest_avg'][$key]);
				$this->mssql->set('bolt_testing_status',@$_POST['ctesting_status'][$key]);
				$this->mssql->set('bolt_testing_remark',@$_POST['cremark'][$key]);
				$this->mssql->set('bolt_testing_post_user_id',$data['bolt_testing_post_user_id']);	
				$this->mssql->set('bolt_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('size_bolt_testing');
			}
		}
		
	}
	
	
	
	public function update_bolt_testing_std($data)
	{
		foreach(@$_POST['bolt_standard_id'] as $key=>$value){
				
				$this->mssql->set('bolt_testing1',@$_POST['test1'][$key]);
				$this->mssql->set('bolt_testing2',@$_POST['test2'][$key]);
				$this->mssql->set('bolt_testing3',@$_POST['test3'][$key]);
				$this->mssql->set('bolt_testing4',@$_POST['test4'][$key]);
				$this->mssql->set('bolt_testing5',@$_POST['test5'][$key]);
				$this->mssql->set('bolt_testing_average',@$_POST['test_avg'][$key]);
				$this->mssql->set('bolt_testing_status',@$_POST['testing_status'][$key]);
				$this->mssql->set('bolt_testing_remark',@$_POST['remark'][$key]);
				$this->mssql->set('bolt_testing_post_user_id',$data['bolt_testing_post_user_id']);	
				$this->mssql->set('bolt_testing_post_date',"NOW()",FALSE);
				$this->mssql->where("income_id",@$_POST['income_id']);
				$this->mssql->where("bolt_standard_id",@$_POST['bolt_standard_id'][$key]);
				$this->mssql->update('income_bolt_testing');
			}
			
		foreach(@$_POST['bolt_cer_size_id'] as $key=>$value){
		
				
				$this->mssql->set('bolt_testing1',@$_POST['ctest1'][$key]);
				$this->mssql->set('bolt_testing2',@$_POST['ctest2'][$key]);
				$this->mssql->set('bolt_testing3',@$_POST['ctest3'][$key]);
				$this->mssql->set('bolt_testing4',@$_POST['ctest4'][$key]);
				$this->mssql->set('bolt_testing5',@$_POST['ctest5'][$key]);
				$this->mssql->set('bolt_testing_average',@$_POST['ctest_avg'][$key]);
				$this->mssql->set('bolt_testing_status',@$_POST['ctesting_status'][$key]);
				$this->mssql->set('bolt_testing_remark',@$_POST['cremark'][$key]);
				$this->mssql->set('bolt_testing_post_user_id',$data['bolt_testing_post_user_id']);	
				$this->mssql->set('bolt_testing_post_date',"NOW()",FALSE);
				$this->mssql->where("income_id",@$_POST['income_id']);
				$this->mssql->where("bolt_cer_size_id",@$_POST['bolt_cer_size_id'][$key]);	
				$this->mssql->update('size_bolt_testing');
			}
		
	}

	
	public function delete_supplier($supplier_id)
	{
		

		$this->mssql->where("supplier_id",$supplier_id);
	    $this->mssql->delete("supplier");	
	}
	
	public function check_income_tested($income_id)
	{
			   $this->mssql->where("income_id",$income_id);
		return $this->mssql->count_all_results("income_bolt_testing");
	}
	public function get_income_tested($income_id,$std_id)
	{
		$this->mssql->where("bolt_standard_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("income_bolt_testing")->row_array();
	}
	
	public function get_income_cer_size_tested($income_id,$std_id){
		$this->mssql->where("bolt_cer_size_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("size_bolt_testing")->row_array();	
	}
	
	public function get_thicknesstest_info_byincomeid_bolt($income_id){
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("thickness_test_bolt")->row_array();
		
	}
	
	public function get_thickness_by_id_income($id){
		$this->mssql->where("id",$id);
		return $this->mssql->get("thickness_galvanizes")->row_array();
		
	}
	
	
	
	
	##################### end test bolt #############################
	
	
	
	##################### start test nut ############################
	
		public function get_income_nut($income_id)
	{
		$this->mssql->where("income_id",$income_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("income_nut")->row_array(); 
		return $row; 
	}
	public function get_nut_product($income_id)
	{
		$income = $this->get_income_nut($income_id);
		
		$this->mssql->where("product_id",$income['income_product_id']);
		$this->mssql->limit(1);
		$row = $this->mssql->get("product_nut")->row_array(); 
		return $row; 
	}
	
	public function get_income_nut_test_info($income_id,$std_id)
	{
		$this->mssql->where("income_id",$income_id);
		$this->mssql->where("nut_standard_id",$std_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("income_nut_testing")->row_array(); 
		return $row;	
		
		
	}
	
	public function get_income_nut_size_test_info($income_id,$std_id)
	{
		$this->mssql->where("income_id",$income_id);
		$this->mssql->where("nut_cer_size_id",$std_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("size_nut_testing")->row_array(); 
		return $row;	
		
		
	}
	

	public function get_product_standard_nut($product_id)
	{
		//$this->mssql->where('(ps_min <> NULL OR ps_max <> NULL)');
		$this->mssql->where("product_id",$product_id);
		$row = $this->mssql->get("product_standard_nut"); 
		return $row;
	}
	
	public function get_product_standard_size_nut($product_id,$size_id)
	{
		/* $this->mssql->where('(ps_min <> NULL OR ps_max <> NULL)'); */
		$this->mssql->where("product_id",$product_id);
		$this->mssql->where("size_id",$size_id);
		$row = $this->mssql->get("product_cer_size_nut"); 
		return $row;
	}
	
	public function add_nut_testing_std($data)
	{
		/*
var_dump(@$_POST);
		exit();
*/
		foreach(@$_POST['nut_standard_id'] as $key=>$value){
				$this->mssql->set('income_id',@$_POST['income_id']);
				$this->mssql->set('nut_standard_id',@$_POST['nut_standard_id'][$key]);
				$this->mssql->set('nut_testing1',@$_POST['test1'][$key]);
				$this->mssql->set('nut_testing2',@$_POST['test2'][$key]);
				$this->mssql->set('nut_testing3',@$_POST['test3'][$key]);
				$this->mssql->set('nut_testing4',@$_POST['test4'][$key]);
				$this->mssql->set('nut_testing5',@$_POST['test5'][$key]);
				$this->mssql->set('nut_testing_average',@$_POST['test_avg'][$key]);
				$this->mssql->set('nut_testing_status',@$_POST['testing_status'][$key]);
				$this->mssql->set('nut_testing_remark',@$_POST['remark'][$key]);
				$this->mssql->set('nut_testing_post_user_id',$data['nut_testing_post_user_id']);	
				$this->mssql->set('nut_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('income_nut_testing');
			}
			
		if(@$_POST['nut_cer_size_id'] != NULL){
			foreach(@$_POST['nut_cer_size_id'] as $key=>$value){
				$this->mssql->set('income_id',@$_POST['income_id']);
				$this->mssql->set('nut_cer_size_id',@$_POST['nut_cer_size_id'][$key]);
				$this->mssql->set('nut_testing1',@$_POST['ctest1'][$key]);
				$this->mssql->set('nut_testing2',@$_POST['ctest2'][$key]);
				$this->mssql->set('nut_testing3',@$_POST['ctest3'][$key]);
				$this->mssql->set('nut_testing4',@$_POST['ctest4'][$key]);
				$this->mssql->set('nut_testing5',@$_POST['ctest5'][$key]);
				$this->mssql->set('nut_testing_average',@$_POST['ctest_avg'][$key]);
				$this->mssql->set('nut_testing_status',@$_POST['ctesting_status'][$key]);
				$this->mssql->set('nut_testing_remark',@$_POST['cremark'][$key]);
				$this->mssql->set('nut_testing_post_user_id',$data['nut_testing_post_user_id']);	
				$this->mssql->set('nut_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('size_nut_testing');
			}
		}

		
		
	}
		
	public function update_nut_testing_std($data)
	{
		foreach(@$_POST['nut_standard_id'] as $key=>$value){
				
				$this->mssql->set('nut_testing1',@$_POST['test1'][$key]);
				$this->mssql->set('nut_testing2',@$_POST['test2'][$key]);
				$this->mssql->set('nut_testing3',@$_POST['test3'][$key]);
				$this->mssql->set('nut_testing4',@$_POST['test4'][$key]);
				$this->mssql->set('nut_testing5',@$_POST['test5'][$key]);
				$this->mssql->set('nut_testing_average',@$_POST['test_avg'][$key]);
				$this->mssql->set('nut_testing_status',@$_POST['testing_status'][$key]);
				$this->mssql->set('nut_testing_remark',@$_POST['remark'][$key]);
				$this->mssql->set('nut_testing_post_user_id',$data['nut_testing_post_user_id']);	
				$this->mssql->set('nut_testing_post_date',"NOW()",FALSE);
				$this->mssql->where("income_id",@$_POST['income_id']);
				$this->mssql->where("nut_standard_id",@$_POST['nut_standard_id'][$key]);
				$this->mssql->update('income_nut_testing');
			}
			
		foreach(@$_POST['nut_cer_size_id'] as $key=>$value){
		
				
				$this->mssql->set('nut_testing1',@$_POST['ctest1'][$key]);
				$this->mssql->set('nut_testing2',@$_POST['ctest2'][$key]);
				$this->mssql->set('nut_testing3',@$_POST['ctest3'][$key]);
				$this->mssql->set('nut_testing4',@$_POST['ctest4'][$key]);
				$this->mssql->set('nut_testing5',@$_POST['ctest5'][$key]);
				$this->mssql->set('nut_testing_average',@$_POST['ctest_avg'][$key]);
				$this->mssql->set('nut_testing_status',@$_POST['ctesting_status'][$key]);
				$this->mssql->set('nut_testing_remark',@$_POST['cremark'][$key]);
				$this->mssql->set('nut_testing_post_user_id',$data['nut_testing_post_user_id']);	
				$this->mssql->set('nut_testing_post_date',"NOW()",FALSE);
				$this->mssql->where("income_id",@$_POST['income_id']);
				$this->mssql->where("nut_cer_size_id",@$_POST['nut_cer_size_id'][$key]);	
				$this->mssql->update('size_nut_testing');
			}
		
	}
		
		
	public function check_income_nut_tested($income_id)
	{
			   $this->mssql->where("income_id",$income_id);
		return $this->mssql->count_all_results("income_nut_testing");
	}
	public function get_income_nut_tested($income_id,$std_id)
	{
		$this->mssql->where("nut_standard_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("income_nut_testing")->row_array();
	}

	public function get_income_nut_cer_size_tested($income_id,$std_id){
		$this->mssql->where("nut_cer_size_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("size_nut_testing")->row_array();

		
	}
	
	public function get_thicknesstest_info_byincomeid_nut($income_id){
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("thickness_test_nut")->row_array();
		
	}
	
	
	##################### end test nut ##############################
	
	##################### start test washer ############################
	
		public function get_income_washer($income_id)
	{
		$this->mssql->where("income_id",$income_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("income_washer")->row_array(); 
		return $row; 
	}
	public function get_washer_product($income_id)
	{
		$income = $this->get_income_washer($income_id);
		
		$this->mssql->where("product_id",$income['income_product_id']);
		$this->mssql->limit(1);
		$row = $this->mssql->get("product_washer")->row_array(); 
		return $row; 
	}
	
	public function get_income_washer_test_info($income_id,$std_id)
	{
		$this->mssql->where("income_id",$income_id);
		$this->mssql->where("washer_standard_id",$std_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("income_washer_testing")->row_array(); 
		return $row;	
		
		
	}
	
	public function get_income_washer_size_test_info($income_id,$std_id)
	{
		$this->mssql->where("income_id",$income_id);
		$this->mssql->where("washer_cer_size_id",$std_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("size_washer_testing")->row_array(); 
		return $row;	
		
		
	}

	public function get_product_standard_washer($product_id)
	{
		//$this->mssql->where('(ps_min <> NULL OR ps_max <> NULL)');
		$this->mssql->where("product_id",$product_id);
		$row = $this->mssql->get("product_standard_washer"); 
		return $row;
	}
	
	public function get_product_standard_size_washer($product_id,$size_id)
	{
		/* $this->mssql->where('(ps_min <> NULL OR ps_max <> NULL)'); */
		$this->mssql->where("product_id",$product_id);
		$this->mssql->where("size_id",$size_id);
		$row = $this->mssql->get("product_cer_size_washer"); 
		return $row;
	}
	
	public function add_washer_testing_std($data)
	{
		/*
var_dump(@$_POST);
		
*/		
		foreach(@$_POST['washer_standard_id'] as $key=>$value){
				$this->mssql->set('income_id',@$_POST['income_id']);
				$this->mssql->set('washer_standard_id',@$_POST['washer_standard_id'][$key]);
				$this->mssql->set('washer_testing1',@$_POST['test1'][$key]);
				$this->mssql->set('washer_testing2',@$_POST['test2'][$key]);
				$this->mssql->set('washer_testing3',@$_POST['test3'][$key]);
				$this->mssql->set('washer_testing4',@$_POST['test4'][$key]);
				$this->mssql->set('washer_testing5',@$_POST['test5'][$key]);
				$this->mssql->set('washer_testing_average',@$_POST['test_avg'][$key]);
				$this->mssql->set('washer_testing_status',@$_POST['testing_status'][$key]);
				$this->mssql->set('washer_testing_remark',@$_POST['remark'][$key]);
				$this->mssql->set('washer_testing_post_user_id',$data['washer_testing_post_user_id']);	
				$this->mssql->set('washer_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('income_washer_testing');
			}
			
		if(@$_POST['washer_cer_size_id'] != NULL){
			foreach(@$_POST['washer_cer_size_id'] as $key=>$value){
				$this->mssql->set('income_id',@$_POST['income_id']);
				$this->mssql->set('washer_cer_size_id',@$_POST['washer_cer_size_id'][$key]);
				$this->mssql->set('washer_testing1',@$_POST['ctest1'][$key]);
				$this->mssql->set('washer_testing2',@$_POST['ctest2'][$key]);
				$this->mssql->set('washer_testing3',@$_POST['ctest3'][$key]);
				$this->mssql->set('washer_testing4',@$_POST['ctest4'][$key]);
				$this->mssql->set('washer_testing5',@$_POST['ctest5'][$key]);
				$this->mssql->set('washer_testing_average',@$_POST['ctest_avg'][$key]);
				$this->mssql->set('washer_testing_status',@$_POST['ctesting_status'][$key]);
				$this->mssql->set('washer_testing_remark',@$_POST['cremark'][$key]);
				$this->mssql->set('washer_testing_post_user_id',$data['washer_testing_post_user_id']);	
				$this->mssql->set('washer_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('size_washer_testing');
			}
		}

		
		
	}
	
	
	
	public function update_washer_testing_std($data)
	{
		foreach(@$_POST['washer_standard_id'] as $key=>$value){
				
				$this->mssql->set('washer_testing1',@$_POST['test1'][$key]);
				$this->mssql->set('washer_testing2',@$_POST['test2'][$key]);
				$this->mssql->set('washer_testing3',@$_POST['test3'][$key]);
				$this->mssql->set('washer_testing4',@$_POST['test4'][$key]);
				$this->mssql->set('washer_testing5',@$_POST['test5'][$key]);
				$this->mssql->set('washer_testing_average',@$_POST['test_avg'][$key]);
				$this->mssql->set('washer_testing_status',@$_POST['testing_status'][$key]);
				$this->mssql->set('washer_testing_remark',@$_POST['remark'][$key]);
				$this->mssql->set('washer_testing_post_user_id',$data['washer_testing_post_user_id']);	
				$this->mssql->set('washer_testing_post_date',"NOW()",FALSE);
				$this->mssql->where("income_id",@$_POST['income_id']);
				$this->mssql->where("washer_standard_id",@$_POST['washer_standard_id'][$key]);
				$this->mssql->update('income_washer_testing');
			}
			
		foreach(@$_POST['washer_cer_size_id'] as $key=>$value){
		
				
				$this->mssql->set('washer_testing1',@$_POST['ctest1'][$key]);
				$this->mssql->set('washer_testing2',@$_POST['ctest2'][$key]);
				$this->mssql->set('washer_testing3',@$_POST['ctest3'][$key]);
				$this->mssql->set('washer_testing4',@$_POST['ctest4'][$key]);
				$this->mssql->set('washer_testing5',@$_POST['ctest5'][$key]);
				$this->mssql->set('washer_testing_average',@$_POST['ctest_avg'][$key]);
				$this->mssql->set('washer_testing_status',@$_POST['ctesting_status'][$key]);
				$this->mssql->set('washer_testing_remark',@$_POST['cremark'][$key]);
				$this->mssql->set('washer_testing_post_user_id',$data['washer_testing_post_user_id']);	
				$this->mssql->set('washer_testing_post_date',"NOW()",FALSE);
				$this->mssql->where("income_id",@$_POST['income_id']);
				$this->mssql->where("washer_cer_size_id",@$_POST['washer_cer_size_id'][$key]);	
				$this->mssql->update('size_washer_testing');
			}
		
	}
	
		
	public function check_income_washer_tested($income_id)
	{
			   $this->mssql->where("income_id",$income_id);
		return $this->mssql->count_all_results("income_washer_testing");
	}
	public function get_income_washer_tested($income_id ,$std_id)
	{
		
		$this->mssql->where("income_id",$income_id);
		$this->mssql->where("washer_standard_id",$std_id);
		return $this->mssql->get("income_washer_testing")->row_array();
	}
	
	public function get_income_washer_cer_size_tested($income_id,$std_id){
		
		$this->mssql->where("income_id",$income_id);
		$this->mssql->where("washer_cer_size_id",$std_id);
		return $this->mssql->get("size_washer_testing")->row_array();

		
	}
	
	public function get_thicknesstest_info_byincomeid_washer($income_id){
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("thickness_test_washer")->row_array();
		
	}

	
	
	##################### end test washer ##############################
	
	##################### start test spring ############################
	
		public function get_income_spring($income_id)
	{
		$this->mssql->where("income_id",$income_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("income_spring")->row_array(); 
		return $row; 
	}
	public function get_spring_product($income_id)
	{
		$income = $this->get_income_spring($income_id);
		
		$this->mssql->where("product_id",$income['income_product_id']);
		$this->mssql->limit(1);
		$row = $this->mssql->get("product_spring")->row_array(); 
		return $row; 
	}
	
	public function get_income_spring_test_info($income_id,$std_id)
	{
		$this->mssql->where("income_id",$income_id);
		$this->mssql->where("spring_standard_id",$std_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("income_spring_testing")->row_array(); 
		return $row;	
		
		
	}
	
	public function get_income_spring_size_test_info($income_id,$std_id)
	{
		$this->mssql->where("income_id",$income_id);
		$this->mssql->where("spring_cer_size_id",$std_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("size_spring_testing")->row_array(); 
		return $row;	
		
		
	}

	public function get_product_standard_spring($product_id)
	{
		//$this->mssql->where('(ps_min <> NULL OR ps_max <> NULL)');
		$this->mssql->where("product_id",$product_id);
		$row = $this->mssql->get("product_standard_spring"); 
		return $row;
	}
	
	public function get_product_standard_size_spring($product_id,$size_id)
	{
		/* $this->mssql->where('(ps_min <> NULL OR ps_max <> NULL)'); */
		$this->mssql->where("product_id",$product_id);
		$this->mssql->where("size_id",$size_id);
		$row = $this->mssql->get("product_cer_size_spring"); 
		return $row;
	}
	
	public function add_spring_testing_std($data)
	{
		
		foreach(@$_POST['spring_standard_id'] as $key=>$value){
				$this->mssql->set('income_id',@$_POST['income_id']);
				$this->mssql->set('spring_standard_id',@$_POST['spring_standard_id'][$key]);
				$this->mssql->set('spring_testing1',@$_POST['test1'][$key]);
				$this->mssql->set('spring_testing2',@$_POST['test2'][$key]);
				$this->mssql->set('spring_testing3',@$_POST['test3'][$key]);
				$this->mssql->set('spring_testing4',@$_POST['test4'][$key]);
				$this->mssql->set('spring_testing5',@$_POST['test5'][$key]);
				$this->mssql->set('spring_testing_status',@$_POST['testing_status'][$key]);
				$this->mssql->set('spring_testing_remark',@$_POST['remark'][$key]);
				$this->mssql->set('spring_testing_post_user_id',$data['spring_testing_post_user_id']);	
				$this->mssql->set('spring_testing_post_date',"NOW()",FALSE);		
				if($this->mssql->insert('income_spring_testing')){
					show_error($this->mssql->_error_message());
				}
				
			}
		if(@$_POST['spring_cer_size_id'] != NULL){
			foreach(@$_POST['spring_cer_size_id'] as $key=>$value){
				$this->mssql->set('income_id',@$_POST['income_id']);
				$this->mssql->set('spring_cer_size_id',@$_POST['spring_cer_size_id'][$key]);
				$this->mssql->set('spring_testing1',@$_POST['ctest1'][$key]);
				$this->mssql->set('spring_testing2',@$_POST['ctest2'][$key]);
				$this->mssql->set('spring_testing3',@$_POST['ctest3'][$key]);
				$this->mssql->set('spring_testing4',@$_POST['ctest4'][$key]);
				$this->mssql->set('spring_testing5',@$_POST['ctest5'][$key]);
				$this->mssql->set('spring_testing_status',@$_POST['ctesting_status'][$key]);
				$this->mssql->set('spring_testing_remark',@$_POST['cremark'][$key]);
				$this->mssql->set('spring_testing_post_user_id',$data['spring_testing_post_user_id']);	
				$this->mssql->set('spring_testing_post_date',"NOW()",FALSE);		
				if($this->mssql->insert('size_spring_testing')){
					show_error($this->mssql->_error_message());
				}
			}
		}

		var_dump(@$_POST);
		exit();	
		
	}
	
	
	
	public function update_spring_testing_std($data)
	{
		foreach(@$_POST['spring_standard_id'] as $key=>$value){
				
				$this->mssql->set('spring_testing1',@$_POST['test1'][$key]);
				$this->mssql->set('spring_testing2',@$_POST['test2'][$key]);
				$this->mssql->set('spring_testing3',@$_POST['test3'][$key]);
				$this->mssql->set('spring_testing4',@$_POST['test4'][$key]);
				$this->mssql->set('spring_testing5',@$_POST['test5'][$key]);
				$this->mssql->set('spring_testing_status',@$_POST['testing_status'][$key]);
				$this->mssql->set('spring_testing_remark',@$_POST['remark'][$key]);
				$this->mssql->set('spring_testing_post_user_id',$data['spring_testing_post_user_id']);	
				$this->mssql->set('spring_testing_post_date',"NOW()",FALSE);
				$this->mssql->where("income_id",@$_POST['income_id']);
				$this->mssql->where("spring_standard_id",@$_POST['spring_standard_id'][$key]);
				$this->mssql->update('income_spring_testing');
			}
			
		foreach(@$_POST['spring_cer_size_id'] as $key=>$value){
		
				
				$this->mssql->set('spring_testing1',@$_POST['ctest1'][$key]);
				$this->mssql->set('spring_testing2',@$_POST['ctest2'][$key]);
				$this->mssql->set('spring_testing3',@$_POST['ctest3'][$key]);
				$this->mssql->set('spring_testing4',@$_POST['ctest4'][$key]);
				$this->mssql->set('spring_testing5',@$_POST['ctest5'][$key]);
				$this->mssql->set('spring_testing_status',@$_POST['ctesting_status'][$key]);
				$this->mssql->set('spring_testing_remark',@$_POST['cremark'][$key]);
				$this->mssql->set('spring_testing_post_user_id',$data['spring_testing_post_user_id']);	
				$this->mssql->set('spring_testing_post_date',"NOW()",FALSE);
				$this->mssql->where("income_id",@$_POST['income_id']);
				$this->mssql->where("spring_cer_size_id",@$_POST['spring_cer_size_id'][$key]);	
				$this->mssql->update('size_spring_testing');
			}
		
	}
	
		
	public function check_income_spring_tested($income_id)
	{
			   $this->mssql->where("income_id",$income_id);
		return $this->mssql->count_all_results("income_spring_testing");
	}
	public function get_income_spring_tested($income_id ,$std_id)
	{
		
		$this->mssql->where("income_id",$income_id);
		$this->mssql->where("spring_standard_id",$std_id);
		return $this->mssql->get("income_spring_testing")->row_array();
	}
	
	public function get_income_spring_cer_size_tested($income_id,$std_id){
		
		$this->mssql->where("income_id",$income_id);
		$this->mssql->where("spring_cer_size_id",$std_id);
		return $this->mssql->get("size_spring_testing")->row_array();

		
	}
	
	public function get_thicknesstest_info_byincomeid_spring($income_id){
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("thickness_test_spring")->row_array();
		
	}

	
	
	##################### end test spring ##############################
	
	##################### start test taper ############################
	
		public function get_income_taper($income_id)
	{
		$this->mssql->where("income_id",$income_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("income_taper")->row_array(); 
		return $row; 
	}
	public function get_taper_product($income_id)
	{
		$income = $this->get_income_taper($income_id);
		
		$this->mssql->where("product_id",$income['income_product_id']);
		$this->mssql->limit(1);
		$row = $this->mssql->get("product_taper")->row_array(); 
		return $row; 
	}
	
	public function get_income_taper_test_info($income_id,$std_id)
	{
		$this->mssql->where("income_id",$income_id);
		$this->mssql->where("taper_standard_id",$std_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("income_taper_testing")->row_array(); 
		return $row;	
		
		
	}
	
	public function get_income_taper_size_test_info($income_id,$std_id)
	{
		$this->mssql->where("income_id",$income_id);
		$this->mssql->where("taper_cer_size_id",$std_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("size_taper_testing")->row_array(); 
		return $row;	
		
		
	}

	public function get_product_standard_taper($product_id)
	{
		//$this->mssql->where('(ps_min <> NULL OR ps_max <> NULL)');
		$this->mssql->where("product_id",$product_id);
		$row = $this->mssql->get("product_standard_taper"); 
		return $row;
	}
	
	public function get_product_standard_size_taper($product_id,$size_id)
	{
		/* $this->mssql->where('(ps_min <> NULL OR ps_max <> NULL)'); */
		$this->mssql->where("product_id",$product_id);
		$this->mssql->where("size_id",$size_id);
		$row = $this->mssql->get("product_cer_size_taper"); 
		return $row;
	}
	
	public function add_taper_testing_std($data)
	{
		/*
var_dump(@$_POST);
		
*/		
		foreach(@$_POST['taper_standard_id'] as $key=>$value){
				$this->mssql->set('income_id',@$_POST['income_id']);
				$this->mssql->set('taper_standard_id',@$_POST['taper_standard_id'][$key]);
				$this->mssql->set('taper_testing1',@$_POST['test1'][$key]);
				$this->mssql->set('taper_testing2',@$_POST['test2'][$key]);
				$this->mssql->set('taper_testing3',@$_POST['test3'][$key]);
				$this->mssql->set('taper_testing4',@$_POST['test4'][$key]);
				$this->mssql->set('taper_testing5',@$_POST['test5'][$key]);
				$this->mssql->set('taper_testing_status',@$_POST['testing_status'][$key]);
				$this->mssql->set('taper_testing_remark',@$_POST['remark'][$key]);
				$this->mssql->set('taper_testing_post_user_id',$data['taper_testing_post_user_id']);	
				$this->mssql->set('taper_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('income_taper_testing');
			}
			
		if(@$_POST['taper_cer_size_id'] != NULL){
			foreach(@$_POST['taper_cer_size_id'] as $key=>$value){
				$this->mssql->set('income_id',@$_POST['income_id']);
				$this->mssql->set('taper_cer_size_id',@$_POST['taper_cer_size_id'][$key]);
				$this->mssql->set('taper_testing1',@$_POST['ctest1'][$key]);
				$this->mssql->set('taper_testing2',@$_POST['ctest2'][$key]);
				$this->mssql->set('taper_testing3',@$_POST['ctest3'][$key]);
				$this->mssql->set('taper_testing4',@$_POST['ctest4'][$key]);
				$this->mssql->set('taper_testing5',@$_POST['ctest5'][$key]);
				$this->mssql->set('taper_testing_status',@$_POST['ctesting_status'][$key]);
				$this->mssql->set('taper_testing_remark',@$_POST['cremark'][$key]);
				$this->mssql->set('taper_testing_post_user_id',$data['taper_testing_post_user_id']);	
				$this->mssql->set('taper_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('size_taper_testing');
			}
		}

		
		
	}
	
	
	
	public function update_taper_testing_std($data)
	{
		foreach(@$_POST['taper_standard_id'] as $key=>$value){
				
				$this->mssql->set('taper_testing1',@$_POST['test1'][$key]);
				$this->mssql->set('taper_testing2',@$_POST['test2'][$key]);
				$this->mssql->set('taper_testing3',@$_POST['test3'][$key]);
				$this->mssql->set('taper_testing4',@$_POST['test4'][$key]);
				$this->mssql->set('taper_testing5',@$_POST['test5'][$key]);
				$this->mssql->set('taper_testing_status',@$_POST['testing_status'][$key]);
				$this->mssql->set('taper_testing_remark',@$_POST['remark'][$key]);
				$this->mssql->set('taper_testing_post_user_id',$data['taper_testing_post_user_id']);	
				$this->mssql->set('taper_testing_post_date',"NOW()",FALSE);
				$this->mssql->where("income_id",@$_POST['income_id']);
				$this->mssql->where("taper_standard_id",@$_POST['taper_standard_id'][$key]);
				$this->mssql->update('income_taper_testing');
			}
			
		foreach(@$_POST['taper_cer_size_id'] as $key=>$value){
		
				
				$this->mssql->set('taper_testing1',@$_POST['ctest1'][$key]);
				$this->mssql->set('taper_testing2',@$_POST['ctest2'][$key]);
				$this->mssql->set('taper_testing3',@$_POST['ctest3'][$key]);
				$this->mssql->set('taper_testing4',@$_POST['ctest4'][$key]);
				$this->mssql->set('taper_testing5',@$_POST['ctest5'][$key]);
				$this->mssql->set('taper_testing_status',@$_POST['ctesting_status'][$key]);
				$this->mssql->set('taper_testing_remark',@$_POST['cremark'][$key]);
				$this->mssql->set('taper_testing_post_user_id',$data['taper_testing_post_user_id']);	
				$this->mssql->set('taper_testing_post_date',"NOW()",FALSE);
				$this->mssql->where("income_id",@$_POST['income_id']);
				$this->mssql->where("taper_cer_size_id",@$_POST['taper_cer_size_id'][$key]);	
				$this->mssql->update('size_taper_testing');
			}
		
	}
	
		
	public function check_income_taper_tested($income_id)
	{
			   $this->mssql->where("income_id",$income_id);
		return $this->mssql->count_all_results("income_taper_testing");
	}
	public function get_income_taper_tested($income_id ,$std_id)
	{
		
		$this->mssql->where("income_id",$income_id);
		$this->mssql->where("taper_standard_id",$std_id);
		return $this->mssql->get("income_taper_testing")->row_array();
	}
	
	public function get_income_taper_cer_size_tested($income_id,$std_id){
		
		$this->mssql->where("income_id",$income_id);
		$this->mssql->where("taper_cer_size_id",$std_id);
		return $this->mssql->get("size_taper_testing")->row_array();

		
	}
	
	public function get_thicknesstest_info_byincomeid_taper($income_id){
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("thickness_test_taper")->row_array();
		
	}

	
	
	##################### end test taper ##############################	
	
	##################### start test stud ############################
	
		public function get_income_stud($income_id)
	{
		$this->mssql->where("income_id",$income_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("income_stud")->row_array(); 
		return $row; 
	}
	public function get_stud_product($income_id)
	{
		$income = $this->get_income_stud($income_id);
		
		$this->mssql->where("product_id",$income['income_product_id']);
		$this->mssql->limit(1);
		$row = $this->mssql->get("product_stud")->row_array(); 
		return $row; 
	}
	
	public function get_income_stud_test_info($income_id,$std_id)
	{
		$this->mssql->where("income_id",$income_id);
		$this->mssql->where("stud_standard_id",$std_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("income_stud_testing")->row_array(); 
		return $row;	
		
		
	}
	
	public function get_income_stud_size_test_info($income_id,$std_id)
	{
		$this->mssql->where("income_id",$income_id);
		$this->mssql->where("stud_cer_size_id",$std_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("size_stud_testing")->row_array(); 
		return $row;	
		
		
	}

	public function get_product_standard_stud($product_id)
	{
		//$this->mssql->where('(ps_min <> NULL OR ps_max <> NULL)');
		$this->mssql->where("product_id",$product_id);
		$row = $this->mssql->get("product_standard_stud"); 
		return $row;
	}
	
	public function get_product_standard_size_stud($product_id,$size_id)
	{
		/* $this->mssql->where('(ps_min <> NULL OR ps_max <> NULL)'); */
		$this->mssql->where("product_id",$product_id);
		$this->mssql->where("size_id",$size_id);
		$row = $this->mssql->get("product_cer_size_stud"); 
		return $row;
	}
	
	public function add_stud_testing_std($data)
	{
		/*
var_dump(@$_POST);
		
*/		
		foreach(@$_POST['stud_standard_id'] as $key=>$value){
				$this->mssql->set('income_id',@$_POST['income_id']);
				$this->mssql->set('stud_standard_id',@$_POST['stud_standard_id'][$key]);
				$this->mssql->set('stud_testing1',@$_POST['test1'][$key]);
				$this->mssql->set('stud_testing2',@$_POST['test2'][$key]);
				$this->mssql->set('stud_testing3',@$_POST['test3'][$key]);
				$this->mssql->set('stud_testing4',@$_POST['test4'][$key]);
				$this->mssql->set('stud_testing5',@$_POST['test5'][$key]);
				$this->mssql->set('stud_testing_status',@$_POST['testing_status'][$key]);
				$this->mssql->set('stud_testing_remark',@$_POST['remark'][$key]);
				$this->mssql->set('stud_testing_post_user_id',$data['stud_testing_post_user_id']);	
				$this->mssql->set('stud_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('income_stud_testing');
			}
			
		if(@$_POST['stud_cer_size_id'] != NULL){
			foreach(@$_POST['stud_cer_size_id'] as $key=>$value){
				$this->mssql->set('income_id',@$_POST['income_id']);
				$this->mssql->set('stud_cer_size_id',@$_POST['stud_cer_size_id'][$key]);
				$this->mssql->set('stud_testing1',@$_POST['ctest1'][$key]);
				$this->mssql->set('stud_testing2',@$_POST['ctest2'][$key]);
				$this->mssql->set('stud_testing3',@$_POST['ctest3'][$key]);
				$this->mssql->set('stud_testing4',@$_POST['ctest4'][$key]);
				$this->mssql->set('stud_testing5',@$_POST['ctest5'][$key]);
				$this->mssql->set('stud_testing_status',@$_POST['ctesting_status'][$key]);
				$this->mssql->set('stud_testing_remark',@$_POST['cremark'][$key]);
				$this->mssql->set('stud_testing_post_user_id',$data['stud_testing_post_user_id']);	
				$this->mssql->set('stud_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('size_stud_testing');
			}
		}

		
		
	}
	
	
	
	public function update_stud_testing_std($data)
	{
		foreach(@$_POST['stud_standard_id'] as $key=>$value){
				
				$this->mssql->set('stud_testing1',@$_POST['test1'][$key]);
				$this->mssql->set('stud_testing2',@$_POST['test2'][$key]);
				$this->mssql->set('stud_testing3',@$_POST['test3'][$key]);
				$this->mssql->set('stud_testing4',@$_POST['test4'][$key]);
				$this->mssql->set('stud_testing5',@$_POST['test5'][$key]);
				$this->mssql->set('stud_testing_status',@$_POST['testing_status'][$key]);
				$this->mssql->set('stud_testing_remark',@$_POST['remark'][$key]);
				$this->mssql->set('stud_testing_post_user_id',$data['stud_testing_post_user_id']);	
				$this->mssql->set('stud_testing_post_date',"NOW()",FALSE);
				$this->mssql->where("income_id",@$_POST['income_id']);
				$this->mssql->where("stud_standard_id",@$_POST['stud_standard_id'][$key]);
				$this->mssql->update('income_stud_testing');
			}
			
		foreach(@$_POST['stud_cer_size_id'] as $key=>$value){
		
				
				$this->mssql->set('stud_testing1',@$_POST['ctest1'][$key]);
				$this->mssql->set('stud_testing2',@$_POST['ctest2'][$key]);
				$this->mssql->set('stud_testing3',@$_POST['ctest3'][$key]);
				$this->mssql->set('stud_testing4',@$_POST['ctest4'][$key]);
				$this->mssql->set('stud_testing5',@$_POST['ctest5'][$key]);
				$this->mssql->set('stud_testing_status',@$_POST['ctesting_status'][$key]);
				$this->mssql->set('stud_testing_remark',@$_POST['cremark'][$key]);
				$this->mssql->set('stud_testing_post_user_id',$data['stud_testing_post_user_id']);	
				$this->mssql->set('stud_testing_post_date',"NOW()",FALSE);
				$this->mssql->where("income_id",@$_POST['income_id']);
				$this->mssql->where("stud_cer_size_id",@$_POST['stud_cer_size_id'][$key]);	
				$this->mssql->update('size_stud_testing');
			}
		
	}
	
		
	public function check_income_stud_tested($income_id)
	{
			   $this->mssql->where("income_id",$income_id);
		return $this->mssql->count_all_results("income_stud_testing");
	}
	public function get_income_stud_tested($income_id ,$std_id)
	{
		
		$this->mssql->where("income_id",$income_id);
		$this->mssql->where("stud_standard_id",$std_id);
		return $this->mssql->get("income_stud_testing")->row_array();
	}
	
	public function get_income_stud_cer_size_tested($income_id,$std_id){
		
		$this->mssql->where("income_id",$income_id);
		$this->mssql->where("stud_cer_size_id",$std_id);
		return $this->mssql->get("size_stud_testing")->row_array();

		
	}
	
	public function get_thicknesstest_info_byincomeid_stud($income_id){
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("thickness_test_stud")->row_array();
		
	}

	
	
	##################### end test stud ##############################

}