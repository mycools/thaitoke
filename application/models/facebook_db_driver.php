<?php
class Facebook_db_driver extends CI_Model{ 
	public function set_login()
	{
		$facebook_id = $this->facebook->getUser();
		if(!$facebook_id){
			return true;	
		}
		
		/*$code = @$_SESSION["code"];
		if($code){
			//return false;	
		}else{
			$_SESSION["code"]=$this->input->get("code");
			$_SESSION["state"]=$this->input->get("state");	
		}
		$url = "https://graph.facebook.com/oauth/access_token?client_id=".$this->facebook->app_id()."&client_secret=".$this->facebook->app_secret()."&grant_type=fb_exchange_token&fb_exchange_token=".$this->facebook->getAccessToken();
		@parse_str(@file_get_contents($url));
		$_SESSION["access_token"]=@$access_token;
		setcookie("access_token",@$access_token,time()+@$expires);*/
	}
	public function is_registered()
	{
		$facebook_id = $this->facebook->getUser();
		if(!$facebook_id){
			return true;	
		}
		$this->db->where("fbid",$facebook_id);
		$is_registered = $this->db->count_all_results("users");
		if($is_registered > 0){
			return true;	
		}else{
			return false;	
		}
	}
	function getMemberName()
	{
		$facebook_id = $this->facebook->getUser();
		if(!$facebook_id){
			return "";	
		}
		$this->db->where("fbid",$facebook_id);
		$member = $this->db->get("users")->row_array();
		return $member['fbname'];
	}
	public function is_game_registed()
	{
		$facebook_id = $this->facebook->getUser();
		if(!$facebook_id){
			return true;	
		}
		$this->db->where("user_id",$this->get_userid());
		$is_registered = $this->db->count_all_results("register");
		if($is_registered > 0){
			return true;	
		}else{
			return false;	
		}
	}
	public function get_userid()
	{
		$facebook_id = $this->facebook->getUser();
		if(!$facebook_id){
			return 0;	
		}
		$this->db->where("fbid",$facebook_id);
		$this->db->limit(1);
		$user = $this->db->get("users")->row_array();
		if(!$user){
			return 0;	
		}else{
			return intval($user['id']);	
		}
	}
	public function save_user()
	{
		$account_data = $this->facebook->me();	
		if(!$account_data->length()){
			show_error("ไม่สามารถตรวจสอบ Facebook  Account ของคุณได้");	
		}
		$code = @$_REQUEST["code"];
		if($code){
			$url = "https://graph.facebook.com/oauth/access_token?client_id=".$this->facebook->app_id()."&client_secret=".$this->facebook->app_secret()."&grant_type=fb_exchange_token&fb_exchange_token=".$this->facebook->getAccessToken();
			@parse_str(@file_get_contents($url));
			$this->db->set("access_token",@$access_token);
		}
		$fbInfo = $account_data->toArray();
		$this->db->set("fbid",@$fbInfo['id']);
		$this->db->set("fbname",@$fbInfo['name']);
		$this->db->set("fbfname",@$fbInfo['first_name']);
		$this->db->set("fblname",@$fbInfo['last_name']);
		$this->db->set("fbemail",@$fbInfo['email']);
		$this->db->set("fbgender",@$fbInfo['gender']);
		
		$this->db->set("fbbirthday",date("Y-m-d H:i:s",strtotime(@$fbInfo['birthday'])));
		$this->db->set("join_date",date("Y-m-d H:i:s"));
		$this->db->set("join_ip",$this->input->ip_address());
		$this->db->insert("users");
	}
	function update_user()
	{
		$account_data = $this->facebook->me();	
		if(!$account_data->length()){
			return false;
		}
		$code = @$_REQUEST["code"];
		if($code){
			$url = "https://graph.facebook.com/oauth/access_token?client_id=".$this->facebook->app_id()."&client_secret=".$this->facebook->app_secret()."&grant_type=fb_exchange_token&fb_exchange_token=".$this->facebook->getAccessToken();
			@parse_str(@file_get_contents($url));
			$this->db->set("access_token",@$access_token);
		}
		$fbInfo = $account_data->toArray();
		
		$this->db->set("fbname",@$fbInfo['name']);
		$this->db->set("fbfname",@$fbInfo['first_name']);
		$this->db->set("fblname",@$fbInfo['last_name']);
		$this->db->set("fbemail",@$fbInfo['email']);
		$this->db->set("fbgender",@$fbInfo['gender']);
		
		$this->db->set("fbbirthday",date("Y-m-d H:i:s",strtotime(@$fbInfo['birthday'])));
		$this->db->set("lastlogin_date",date("Y-m-d H:i:s"));
		$this->db->set("lastlogin_ip",$this->input->ip_address());
		$this->db->where("fbid",@$fbInfo['id']);
		$this->db->update("users");
	}
}