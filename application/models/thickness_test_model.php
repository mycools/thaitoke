<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Income_test_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		$this->mssql = $this->load->database("mssql", true);
		
	}
	##################### income test #############################
	
		
	public function get_income_bolt($income_id)
	{
		$this->mssql->where("income_id",$income_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("income_bolt")->row_array(); 
		return $row; 
	}
	public function get_bolt_product($income_id)
	{
		$income = $this->get_income_bolt($income_id);
		$this->mssql->select("product_bolt.*");
		$this->mssql->where("lot_id",$income['income_bolt_lot_id']);
		$this->mssql->limit(1);
		$this->mssql->join("product_bolt","product_bolt.product_id=lot_bolt.product_id");
		$row = $this->mssql->get("lot_bolt")->row_array(); 
		return $row; 
	}
	public function get_inspection($inspection_id)
	{
		$this->mssql->where("inspection_id",$inspection_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("inspection")->row_array(); 
		return $row; 
	}
	
	public function get_product_standard_bolt($product_id)
	{
		$this->mssql->where("ps_min <>","0");
		$this->mssql->or_where("ps_max <>","0");
		$this->mssql->where("product_id",$product_id);
		$row = $this->mssql->get("product_standard_bolt"); 
		return $row;
	}
	
	public function add_bolt_testing_std($data)
	{
		/*
var_dump($_POST);
		exit();
*/
		foreach($_POST['bolt_standard_id'] as $key=>$value){
				$this->mssql->set('income_id',$_POST['income_id']);
				$this->mssql->set('bolt_standard_id',$_POST['bolt_standard_id'][$key]);
				$this->mssql->set('bolt_testing1',$_POST['test1'][$key]);
				$this->mssql->set('bolt_testing2',$_POST['test2'][$key]);
				$this->mssql->set('bolt_testing3',$_POST['test3'][$key]);
				$this->mssql->set('bolt_testing4',$_POST['test4'][$key]);
				$this->mssql->set('bolt_testing5',$_POST['test5'][$key]);
				$this->mssql->set('bolt_testing_status',$_POST['testing_status'][$key]);
				$this->mssql->set('bolt_testing_remark',$_POST['remark'][$key]);
				$this->mssql->set('bolt_testing_post_user_id',$_POST['bolt_testing_post_user_id']);	
				$this->mssql->set('bolt_testing_post_date',"SYSDATETIME()",FALSE);		
				$this->mssql->insert('income_bolt_testing');
			}

		
		
	}
	
	public function add_bolt_testing_final($data)
	{
		foreach($_POST['bolt_standard_id'] as $key=>$value){
				$this->mssql->set('income_id',$_POST['income_id']);
				$this->mssql->set('bolt_standard_id',$_POST['bolt_standard_id'][$key]);
				$this->mssql->set('bolt_testing1',$_POST['test1'][$key]);
				$this->mssql->set('bolt_testing2',$_POST['test2'][$key]);
				$this->mssql->set('bolt_testing3',$_POST['test3'][$key]);
				$this->mssql->set('bolt_testing4',$_POST['test4'][$key]);
				$this->mssql->set('bolt_testing5',$_POST['test5'][$key]);
				$this->mssql->set('bolt_testing_status',$_POST['testing_status'][$key]);
				$this->mssql->set('bolt_testing_remark',$_POST['remark'][$key]);
				$this->mssql->set('bolt_testing_post_user_id',$_POST['bolt_testing_post_user_id']);	
				$this->mssql->set('bolt_testing_post_date',"SYSDATETIME()",FALSE);		
				$this->mssql->insert('final_bolt_testing');
			}
		
	}
	
	public function delete_supplier($supplier_id)
	{
		

		$this->mssql->where("supplier_id",$supplier_id);
	    $this->mssql->delete("supplier");	
	}
	
	public function check_income_tested($income_id)
	{
			   $this->mssql->where("income_id",$income_id);
		return $this->mssql->count_all_results("income_bolt_testing");
	}
	
	##################### end supplier #############################
	
	

}