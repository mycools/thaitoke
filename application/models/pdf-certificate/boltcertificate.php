<?php
class boltcertificate extends CI_Model{
	var $_data = array();
	var $hasNutInfo = false;
	var $hasWasherInfo = false;
	function make($final_id,$income_id)
	{
		$this->fpdf = new PDF_MC_Table();
		$this->fpdf->SetMargins(10,5,5);
		$this->fpdf->AddFont('angsa','','angsa.php');
		$this->fpdf->AddFont('angsa','B','angsab.php');
		
		$this->fpdf->AddPage();	
		$this->fpdf->SetAutoPageBreak(false);
		$this->getData($final_id,$income_id);
		$this->_Header($final_id,$income_id);
		$this->fpdf->setY(82);
		$this->_Chemical();
		$this->_Bolt();
		$curr_Y = $this->fpdf->GetY();
		if($this->hasNutInfo){
			$this->_Nut($curr_Y);
		}
		if($this->hasWasherInfo){
			$this->_Washer($curr_Y);
		}
		$curr_Y = $this->fpdf->GetY();
		$this->_VirtualThird($curr_Y);
		$this->_ThicknessGalvanized($curr_Y);
		$this->_Footer($final_id,$income_id);
		//Output
		$this->fpdf->Output('pdf/bolt-certificate-'.$final_id.'-'.$income_id.'.pdf',"I");
	}
	function getData($final_id,$income_id)
	{
		$this->load->model('final_test_model');
		$this->load->model('administrator/bolt_certificate');
		$this->load->model('administrator/nut_certificate');
		$this->load->model('administrator/chemical_test_final_model');
		$this->load->model('income_test_model');
		$this->_data['final_id'] = $final_id;
		$this->_data['income_id'] = $income_id;
		$checked = $this->final_test_model->check_final_tested($this->_data['income_id'],$final_id);
		if($checked > 0){
			$product = $this->final_test_model->get_bolt_product($this->_data['income_id'],$final_id);
			
			$this->_data['std_list'] = $this->final_test_model->get_product_standard_bolt($product['product_id']);
			
			$ps_id=0;
			foreach($this->_data['std_list']->result_array() as $row){
				$this->_data['std_bolt'][$row['standard_description']]['info']=$row;
				$this->_data['std_bolt'][$row['standard_description']]['test']=$this->final_test_model->get_final_bolt_test_info($this->_data['final_id'],$row['ps_id']);
				$ps_id=$row['ps_id'];
			}
			
			$this->_data['product'] = $product;
			$this->_data['income_info'] =  $this->final_test_model->get_final_bolt($this->_data['income_id'],$final_id);
			//$this->_data['income_data'] =  $this->certificate_test_model->get_income_tested($this->_data['income_id']);
			$this->_data['shipper'] =  $this->bolt_certificate->getcustomer_name($this->_data['income_info']['customer_id']);
			$this->_data['size'] = $this->bolt_certificate->get_size($this->_data['income_info']['final_bolt_size_id']);
			//$this->_data['size'] = $this->bolt_certificate->get_cer_size($this->_data['income_info']['final_bolt_size_id']);
			$this->_data['cer_size_list'] = $this->final_test_model->get_product_standard_size_bolt($product['product_id'],$this->_data['income_info']['final_bolt_size_id']);
			
			foreach($this->_data['cer_size_list']->result_array() as $row){
				$this->_data['cer_size'][$row['ps_description']]['info']=$row;
				$this->_data['cer_size'][$row['ps_description']]['test']=$this->final_test_model->get_final_bolt_size_test_info($this->_data['income_info']['final_id'],$row['ps_id']);;
			}
			$this->_data['bolt_cer_size'] = $this->_data['cer_size'];
			$this->_data['chemical'] = $this->final_test_model->get_chemical_final_bolt($this->_data['income_id'],$this->_data['final_id']);
			
			$this->_data['info'] = $this->income_test_model->get_income_bolt($this->_data['income_id']);
			$this->_data['bolt_thickness'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_bolt_cer($this->_data['income_id'],$final_id);
			$this->_data['bolt_thickness_info'] = $this->chemical_test_final_model->get_thickness_by_id($this->_data['bolt_thickness']['thickness_id']); 
			
			##### nut ##########
			
			$this->_data['nut_info'] = $this->income_test_model->get_income_nut($this->_data['income_info']['final_nut_income_id']);
			
			$this->_data['nut_chemical'] = $this->final_test_model->get_chemical_final_nut($this->_data['income_info']['final_nut_income_id'],$final_id);
			
			$this->_data['nut_product'] = $this->final_test_model->get_nut_product($this->_data['income_info']['final_nut_income_id'],$final_id);
			
			$this->_data['std_list_nut'] = $this->final_test_model->get_product_standard_nut($this->_data['nut_product']['product_id']);
			$ps_id_nut=0;
			foreach($this->_data['std_list_nut']->result_array() as $row){

				$this->_data['std_nut'][$row['standard_description']]['info']=$row;
				$this->_data['std_nut'][$row['standard_description']]['test']=$this->final_test_model->get_final_nut_test_info($this->_data['income_info']['final_id'],$row['ps_id']);
				$ps_id_nut=$row['ps_id'];
			}
			//var_dump($this->_data['std_nut'][$rown['standard_description']]['info']);
			$this->_data['nut_size'] = $this->nut_certificate->get_size($this->_data['income_info']['final_nut_size_id']);
			//var_dump($this->_data['nut_product']);exit;
			$this->_data['nut_cer_size_list'] = $this->final_test_model->get_product_standard_size_nut($this->_data['nut_product']['product_id'],$this->_data['income_info']['final_nut_size_id']);
			//var_dump($this->final_test_model->mssql->last_query());exit;
			foreach($this->_data['nut_cer_size_list']->result_array() as $row){
				$this->_data['nut_cer_size'][$row['ps_description']]['info']=$row;
				$this->_data['nut_cer_size'][$row['ps_description']]['test']=$this->final_test_model->get_final_nut_size_test_info($this->_data['income_info']['final_id'],$row['ps_id']);
			}
			$this->_data['nut_thickness'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_nut_cer($this->_data['income_info']['final_nut_income_id'],$final_id);
			
			$this->_data['nut_thickness_info'] = $this->chemical_test_final_model->get_thickness_by_id($this->_data['nut_thickness']['thickness_id']); 
			
			######## washer #####################
			
			$this->_data['washer_info'] = $this->income_test_model->get_income_washer($this->_data['income_info']['final_washer_income_id']);
			
			$this->_data['washer_chemical'] = $this->final_test_model->get_chemical_final_washer($this->_data['income_info']['final_washer_income_id'],$final_id);
			
			$this->_data['washer_product'] = $this->final_test_model->get_washer_product($this->_data['income_info']['final_washer_income_id'],$final_id);
			
			$this->_data['std_list_washer'] = $this->final_test_model->get_product_standard_washer(@$this->_data['washer_product']['product_id']);
			$ps_id_washer=0;
			foreach($this->_data['std_list_washer']->result_array() as $row){

				$this->_data['std_washer'][$row['standard_description']]['info']=$row;
				$this->_data['std_washer'][$row['standard_description']]['test']=$this->final_test_model->get_final_washer_test_info($this->_data['income_info']['final_id'],$row['ps_id']);
				$ps_id_washer=$row['ps_id'];
			}

			
			
			//var_dump($this->_data['washer_product']);exit;
			$this->_data['washer_cer_size_list'] = $this->final_test_model->get_product_standard_size_washer(@$this->_data['washer_product']['product_id'],$this->_data['income_info']['final_washer_size_id']);
			//var_dump($this->final_test_model->mssql->last_query());exit;
			
			
			foreach($this->_data['washer_cer_size_list']->result_array() as $row){
				$this->_data['washer_cer_size'][$row['ps_description']]['info']=$row;
				$this->_data['washer_cer_size'][$row['ps_description']]['test']=$this->final_test_model->get_final_washer_size_test_info($this->_data['income_info']['final_id'],$row['ps_id']);;
			}
			$this->_data['washer_thickness'] = $this->chemical_test_final_model->get_thicknesstestinfo_byincomeid_washer_cer($this->_data['income_info']['final_washer_income_id'],$final_id);
			$this->_data['washer_thickness_info'] = $this->chemical_test_final_model->get_thickness_by_id(@$this->_data['washer_thickness']['thickness_id']); 
			if($this->input->get('debug')=="1"){
				echo @json_encode($this->_data);exit();
			}
			
		}else{
			$this->session->set_flashdata("message-warning","ข้อมูลยังไม่ได้ถูกทดสอบ.");
			admin_redirect("certificate_manager/certificate_bolt_list");
		}
	}
	function _Header($final_id,$income_id)
	{
		$certificate_logo = $this->settings->item('certificate_logo');
		$certificate_company_th = $this->settings->item('certificate_company_th');
		$certificate_company_en = $this->settings->item('certificate_company_en');
		$certificate_company_address_1 = $this->settings->item('certificate_company_address_1');
		$certificate_company_address_2 = $this->settings->item('certificate_company_address_2');
		$certificate_company_address_3 = $this->settings->item('certificate_company_address_3');
		$certificate_company_address_4 = $this->settings->item('certificate_company_address_4');
		$certificate_factory_address = $this->settings->item('certificate_factory_address');
		
		$this->fpdf->SetFont('angsa','B',18);
		$this->fpdf->Line(5, 5, 5, 290);
		$this->fpdf->Line(205, 5, 205, 290);
		$this->fpdf->Line(5, 5, 205, 5);
		$this->fpdf->Line(5, 290, 205, 290);
		$start_Y=6;
		$this->fpdf->SetY($start_Y);
		$this->fpdf->MultiCell(90 , 7, $this->setUTF8($certificate_company_th."\n".$certificate_company_en));
		$this->fpdf->SetY(10);
		$this->fpdf->SetX(100);
		$this->fpdf->Cell( 80  , 7 , "INSPECTION CERTIFICATE" , 0, 1 , 'C' );
		$this->fpdf->SetY(17);
		$this->fpdf->SetX(100);
		$this->fpdf->SetFont('angsa','',12);
		$this->fpdf->Cell( 80  , 7 , "CERTIFICATE TO EN.10204.3.1" , 0, 1 , 'C' );
		$this->fpdf->SetY($start_Y);
		$this->fpdf->SetFont('angsa','B',18);
		$this->fpdf->Cell( 190  , 16 , $this->fpdf->Image('public/uploads/settings/'.$certificate_logo,180,($this->fpdf->GetY()),20) , 0, 1 , 'R' );
		
		$yhead = $this->fpdf->GetY();
		//Head Office
		$this->fpdf->SetFont('angsa','B',14);
		$this->fpdf->Cell( 80  , 7 , "HEAD OFFICE :" , 0, 1 , 'L' );
		$this->fpdf->SetFont('angsa','',12);
		$this->fpdf->Cell( 80  , 5 , $this->setUTF8($certificate_company_address_1) , 0, 1 , 'L' );
		$this->fpdf->Cell( 80  , 5 , $this->setUTF8($certificate_company_address_2) , 0, 1 , 'L' );
		$this->fpdf->Cell( 80  , 5 , $this->setUTF8($certificate_company_address_3) , 0, 1 , 'L' );
		$this->fpdf->Cell( 80  , 5 , $this->setUTF8($certificate_company_address_4) , 0, 1 , 'L' );
		//Factory
		$this->fpdf->Ln(4);
		$this->fpdf->SetFont('angsa','B',14);
		$this->fpdf->Cell( 80  , 7 , "FACTORY :" , 0, 1 , 'L' );
		$this->fpdf->SetFont('angsa','',12);
		$this->fpdf->Cell( 80  , 5 , $this->setUTF8($certificate_factory_address) , 0, 1 , 'L' );
		//Date
		$this->fpdf->Ln(4);
		$this->fpdf->SetFont('angsa','B',14);
		$this->fpdf->Cell( 80  , 5 , "Date : ".$this->setUTF8(date("d-m-Y",strtotime($this->_data['income_info']['final_date_edit']))) , 0, 1 , 'L' );
		//Inspection No.
		$this->fpdf->SetFont('angsa','',12);
		$this->fpdf->Cell( 80  , 5 ,$this->setUTF8( "Inspection No : ".@$this->_data['income_info']['final_inspection_no']) , 0, 1 , 'L' );
		$this->fpdf->SetY($yhead);
		
		$this->fpdf->SetFont('angsa','B',14);
		$this->fpdf->SetX(100);
		$this->fpdf->Cell( 100  , 5 , "Customer" , 0, 1 , 'L' );
		$this->fpdf->SetX(100);
		$this->fpdf->Cell( 100  , 5 , "" , 0, 1 , 'L' );
		$this->fpdf->SetX(100);
		$this->fpdf->Cell( 100  , 5 , "Certificate No." , 0, 1 , 'L' );
		$this->fpdf->SetX(100);
		$this->fpdf->Cell( 100  , 5 , "Lot No." , 0, 1 , 'L' );
		$this->fpdf->SetX(100);
		$this->fpdf->Cell( 100  , 5 , "P/O No./IV No." , 0, 1 , 'L' );
		$this->fpdf->SetX(100);
		$this->fpdf->Cell( 100  , 5 , "Sale Order No." , 0, 1 , 'L' );
		$this->fpdf->SetX(100);
		$this->fpdf->Cell( 100  , 5 , "Commodity" , 0, 1 , 'L' );
		$this->fpdf->SetX(100);
		$this->fpdf->Cell( 100  , 5 , "Size & Q'ty shipped" , 0, 1 , 'L' );
		$this->fpdf->SetX(100);
		$this->fpdf->Cell( 100  , 5 , "Surface Condition" , 0, 1 , 'L' );
		$this->fpdf->SetX(100);
		$this->fpdf->Cell( 100  , 5 , "Standard" , 0, 1 , 'L' );
		
		$this->fpdf->SetY($yhead);
		$this->fpdf->SetX(135);
		$this->fpdf->Cell( 65  , 5 , ":" , 0, 1 , 'L' );
		$this->fpdf->SetX(135);
		$this->fpdf->Cell( 65  , 5 , "" , 0, 1 , 'L' );
		for($i=1;$i<=8;$i++){
			$this->fpdf->SetX(135);
			$this->fpdf->Cell( 65  , 5 , ":" , 0, 1 , 'L' );
		}
		$this->fpdf->SetFont('angsa','',14);
		$this->fpdf->SetY($yhead);
		for($i=1;$i<=12;$i++){
			$this->fpdf->SetX(137);
			$this->fpdf->Cell( 62  , 5 , str_pad('',35,'_') , 0, 1 , 'L' );
		}
		$this->fpdf->SetY($yhead);
		$this->fpdf->SetFont('angsa','',10);
		//Customer
		$this->fpdf->SetX(137);
		$this->fpdf->MultiCell( 62  , 5 , $this->setUTF8($this->_data['shipper']['customer_name']) );
		//Certificate No.
		$this->fpdf->SetY($yhead+10);
		$this->fpdf->SetX(137);
		
		$this->fpdf->Cell( 62  , 5 , $this->setUTF8('TOKE-'.$this->_data['income_info']['final_job_id']) , 0, 1 , 'L' );
		//Lot No.
		$lot_no = "B(" . $this->_data['info']['income_lot_no'].")";
		if(isset($this->_data['nut_info']['income_lot_no'])){
			$lot_no .= "N(" . $this->_data['nut_info']['income_lot_no'].")";
		}
		if(isset($this->_data['washer_info']['income_lot_no'])){
			$lot_no .= "W(" . $this->_data['washer_info']['income_lot_no'].")";
		}
		$this->fpdf->SetX(137);
		$this->fpdf->Cell( 62  , 5 , $this->setUTF8($lot_no) , 0, 1 , 'L' );
		//P/O No./IV No.
		$this->fpdf->SetX(137);
		$this->fpdf->Cell( 62  , 5 , $this->setUTF8($this->_data['income_info']['final_invoice_no']) , 0, 1 , 'L' );
		//Sale Order No.
		$this->fpdf->SetX(137);
		$this->fpdf->Cell( 62  , 5 , $this->setUTF8($this->_data['income_info']['final_sale_order_no']) , 0, 1 , 'L' );
		//Commodity
		$this->fpdf->SetX(137);
		$this->fpdf->Cell( 62  , 5 , $this->setUTF8($this->_data['info']['income_commodity']) , 0, 1 , 'L' );
		//Size & Q'ty shipped
		$this->fpdf->SetX(137);
		$size_display = implode('x',array($this->_data['size']['size_m'],$this->_data['size']['size_p'],$this->_data['size']['size_length']));
		$this->fpdf->Cell( 62  , 5 , $this->setUTF8(sprintf("%s = %s %s",$size_display,number_format($this->_data['income_info']['final_bolt_qty'],0),$this->_data['income_info']['final_text_set'])) , 0, 1 , 'L' );
		//$this->fpdf->Cell( 62  , 5 , $this->setUTF8(sprintf("%sx%sx%s = %d %s",$this->_data['size']['size_m'],$this->_data['size']['size_p'],$this->_data['size']['size_length'],$this->_data['income_info']['final_bolt_qty'],$this->_data['income_info']['final_text_set'])) , 0, 1 , 'L' );
		//Surface Condition
		$this->fpdf->SetX(137);
		$this->fpdf->Cell( 62  , 5 , $this->setUTF8($this->_data['product']['product_type']) , 0, 1 , 'L' );
		//Standard
		//Bolt
		if($this->_data['product']){
			$this->fpdf->SetX(137);
			$this->fpdf->Cell( 62  , 5 , $this->setUTF8(sprintf("BOLT : %s %s %s",@$this->_data['product']['product_standard'],@$this->_data['product']['product_name'],@$this->_data['product']['product_part_name'])) , 0, 1 , 'L' );	
		}
		if($this->_data['nut_product']){
			$this->fpdf->SetX(137);
			$this->fpdf->Cell( 62  , 5 , $this->setUTF8(sprintf("NUT : %s %s %s",@$this->_data['nut_product']['product_standard'],@$this->_data['nut_product']['product_name'],@$this->_data['nut_product']['product_part_name'])) , 0, 1 , 'L' );	
		}
		if($this->_data['washer_product']){
			$this->fpdf->SetX(137);
			$this->fpdf->Cell( 62  , 5 , $this->setUTF8(sprintf("WASHER : %s %s %s",@$this->_data['washer_product']['product_standard'],@$this->_data['washer_product']['product_name'],@$this->_data['washer_product']['product_part_name'])) , 0, 1 , 'L' );	
		}
		
		
	}
	function _Footer()
	{
		$setY = 265;
		$this->fpdf->Line(5, $setY, 205, $setY);
		$this->fpdf->SetY($setY);
		$this->fpdf->SetFont('angsa','B',12);
		$this->fpdf->Cell( 190  , 5 , $this->setUTF8('**Remark :') , 0, 1 , 'L' );
		$this->fpdf->SetFont('angsa','',8);
		$this->fpdf->MultiCell( 190  , 2 , $this->setUTF8(@$this->_data['income_info']['final_text_remark']));
		$this->fpdf->SetY(280);
		$this->fpdf->SetFont('angsa','B',12);
		$this->fpdf->Cell( 95  , 5 , $this->setUTF8('Print By : ___________________________________') , 0, 0 , 'L' );
		$this->fpdf->Cell( 95  , 5 , $this->setUTF8('Inspected and approved by : ___________________________________') , 0, 1 , 'L' );
		
		$this->fpdf->Cell( 133  , 5 , '' , 0, 0 , 'L' );
		$this->fpdf->Cell( 55  , 5 , $this->setUTF8('Quality Control Manager') , 0, 1 , 'L' );
	}
	function _Chemical()
	{
		$row_height = 5;
		$max_col = 0;
		$this->fpdf->SetFont('angsa','B',12);
		
		$this->fpdf->SetX(10);
		
		$this->fpdf->Ln(1);
		$this->fpdf->Cell( 190  , $row_height , $this->setUTF8('1. Chemical Composition (%)') , 0, 1 , 'L' );
		$this->fpdf->SetFont('angsa','',10);
		$this->fpdf->Cell( 45  , $row_height , $this->setUTF8('Division') , 'LTRB', 0 , 'C' );
		
		$chemical_header = array();
		if(count($this->_data['chemical']) > 0){
			$chemical_header = $this->_data['chemical'];
		}else if(count($this->_data['nut_chemical']) > 0){
			$chemical_header = $this->_data['nut_chemical'];
		}else if(count($this->_data['washer_chemical']) > 0){
			$chemical_header = $this->_data['washer_chemical'];
		}
		
		$lastindexchemical = count($chemical_header)-1;
		if($max_col < $lastindexchemical){ $max_col=$lastindexchemical; }
		
		foreach($chemical_header as $index=>$row){
			$newline = ($lastindexchemical==$index)?1:0;
			$this->fpdf->Cell( 10  , $row_height , $this->setUTF8($row['chemical_value']) , 'LTRB', $newline , 'C' );
		}
		$y = $this->fpdf->GetY();
		if(count($this->_data['chemical']) > 0){
			$this->fpdf->Cell( 15  , $row_height*2 , $this->setUTF8('Bolt') , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 15  , $row_height*2 , $this->setUTF8('Spec.') , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 15  , $row_height , $this->setUTF8('Min.') , 'LTRB', 0 , 'C' );
			foreach($this->_data['chemical'] as $index=>$row){
				$newline = ($lastindexchemical==$index)?1:0;
				$this->fpdf->Cell( 10  , $row_height , (($row['ps_min'])==0)?"":(($row['ps_min'])) , 'LTRB', $newline , 'C' );
			}
			$this->fpdf->SetX(40);
			$this->fpdf->Cell( 15  , $row_height , $this->setUTF8('Max.') , 'LTRB', 0 , 'C' );
			foreach($this->_data['chemical'] as $index=>$row){
				$newline = ($lastindexchemical==$index)?1:0;
				$this->fpdf->Cell( 10  , $row_height , (($row['ps_max'])==0)?"":(($row['ps_max'])) , 'LTRB', $newline , 'C' );
			}
			$this->fpdf->Cell( 15  , $row_height , $this->setUTF8('Heat No.') , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 30  , $row_height , $this->setUTF8(@$this->_data['info']['income_heat_no']) , 'LTRB', 0 , 'C' );
			foreach($this->_data['chemical'] as $index=>$row){
				$newline = ($lastindexchemical==$index)?1:0;
				$this->fpdf->Cell( 10  , $row_height , $row['test_value'] , 'LTRB', $newline , 'C' );
			}
		}
		//Nut
		if($this->_data['nut_chemical']){
			$this->hasNutInfo=true;
			$y = $this->fpdf->GetY();
			$lastindexchemical = count($this->_data['nut_chemical'])-1;
			if($max_col < $lastindexchemical){ $max_col=$lastindexchemical; }
			$this->fpdf->Cell( 15  , $row_height*2 , $this->setUTF8('Nut') , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 15  , $row_height*2 , $this->setUTF8('Spec.') , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 15  , $row_height , $this->setUTF8('Min.') , 'LTRB', 0 , 'C' );
			foreach($this->_data['nut_chemical'] as $index=>$row){
				$newline = ($lastindexchemical==$index)?1:0;
				$this->fpdf->Cell( 10  , $row_height , (($row['ps_min'])==0)?"":(($row['ps_min'])) , 'LTRB', $newline , 'C' );
			}
			$this->fpdf->SetX(40);
			$this->fpdf->Cell( 15  , $row_height , $this->setUTF8('Max.') , 'LTRB', 0 , 'C' );
			foreach($this->_data['nut_chemical'] as $index=>$row){
				$newline = ($lastindexchemical==$index)?1:0;
				$this->fpdf->Cell( 10  , $row_height , (($row['ps_max'])==0)?"":(($row['ps_max'])) , 'LTRB', $newline , 'C' );
			}
			$this->fpdf->Cell( 15  , $row_height , $this->setUTF8('Heat No.') , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 30  , $row_height , $this->setUTF8(@$this->_data['nut_info']['income_heat_no']) , 'LTRB', 0 , 'C' );
			foreach($this->_data['nut_chemical'] as $index=>$row){
				$newline = ($lastindexchemical==$index)?1:0;
				$this->fpdf->Cell( 10  , $row_height , (($row['test_value'])==0)?"":(($row['test_value'])) , 'LTRB', $newline , 'C' );
			}
		}
		//Washer
		
		if($this->_data['washer_chemical']){
			$this->hasWasherInfo=true;
			$y = $this->fpdf->GetY();
			$lastindexchemical = count($this->_data['washer_chemical'])-1;
			if($max_col < $lastindexchemical){ $max_col=$lastindexchemical; }
			$this->fpdf->Cell( 15  , $row_height*2 , $this->setUTF8('WASHER') , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 15  , $row_height*2 , $this->setUTF8('Spec.') , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 15  , $row_height , $this->setUTF8('Min.') , 'LTRB', 0 , 'C' );
			foreach($this->_data['washer_chemical'] as $index=>$row){
				$newline = ($lastindexchemical==$index)?1:0;
				$this->fpdf->Cell( 10  , $row_height , (($row['ps_min'])==0)?"":(($row['ps_min'])) , 'LTRB', $newline , 'C' );
				
			}
			$this->fpdf->SetX(40);
			$this->fpdf->Cell( 15  , $row_height , $this->setUTF8('Max.') , 'LTRB', 0 , 'C' );
			foreach($this->_data['washer_chemical'] as $index=>$row){
				$newline = ($lastindexchemical==$index)?1:0;
				$this->fpdf->Cell( 10  , $row_height , (($row['ps_max'])==0)?"":(($row['ps_max'])) , 'LTRB', $newline , 'C' );
			}
			
			$this->fpdf->Cell( 15  , $row_height , $this->setUTF8('Heat No.') , 'LTRB', 0 , 'C' );
			$this->fpdf->Cell( 30  , $row_height , $this->setUTF8(@$this->_data['washer_info']['income_heat_no']) , 'LTRB', 0 , 'C' );
			foreach($this->_data['washer_chemical'] as $index=>$row){
				$newline = ($lastindexchemical==$index)?1:0;
				$this->fpdf->Cell( 10  , $row_height , (($row['test_value'])==0)?"":(($row['test_value'])) , 'LTRB', $newline , 'C' );
			}
			
		}
//		$max_col++;
//		$close_l = 45+($max_col*10);
//		$this->fpdf->Cell( $close_l  , 1 , '' , 'T', 1 , 'C' );
		
		
		
	}
	function _Bolt()
	{
		//Calculate
		$hardness = $this->_std('bolt','Hardness NO. 1');
		$yield = $this->_std('bolt','Yield Stregth');
		$elongation = $this->_std('bolt','Elongation');
		$reduction = $this->_std('bolt','Reduction of Area');
		$TensileStregth = $this->_std('bolt','Tensile Stregth');
		$Tensile = $this->cer_size("bolt","Wedge Tensile Load");
		$Proof = $this->cer_size("bolt","Proof Load");
		$HeadHeight = $this->cer_size("bolt","Head Height");
		$Flat = $this->cer_size("bolt","Width Across Flat");
		$Corner = $this->cer_size("bolt","Width Across Corner");
		$BodyDiameter = $this->cer_size("bolt","Body Diameter");
		$BodyLength = $this->cer_size("bolt","Body Length");
		
		
		$row_height = 5;
		$this->fpdf->Ln(2);
		$this->fpdf->SetFont('angsa','B',12);
		$this->fpdf->Cell( 30  , $row_height , $this->setUTF8('2.1 BOLT') , 0, 0 , 'L' );
		
		$this->fpdf->Cell( 40  , $row_height , $this->setUTF8('Material : '.@$this->_data['info']['income_material']) , 0, 0 , 'L' );
		$this->fpdf->Cell( 60  , $row_height , $this->setUTF8('Marking : '.@$this->_data['info']['grade_mark']) , 0, 0 , 'L' );
		$this->fpdf->Cell( 40  , $row_height , $this->setUTF8('Cer No : '.@$this->_data['info']['income_certificate_no']) , 0, 1 , 'L' );
		$y = $this->fpdf->GetY();
		$this->fpdf->SetFont('angsa','B',10);
		$this->fpdf->Cell( 20  , $row_height*4 , $this->setUTF8('Division') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height*2 , $this->setUTF8('Hardness') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 60  , $row_height , $this->setUTF8('Specimen Tensile') , 'LTR', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8('Proof Load') , 'LTR', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8('Wedge') , 'LTR', 0 , 'C' );
		$this->fpdf->Cell( 30  , $row_height*2 , $this->setUTF8('Width Across') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height*4 , $this->setUTF8('Head Height') , 'LTRB', 1 , 'C' );
		
		
		$this->fpdf->SetY($y+$row_height);
		$this->fpdf->SetX(50);
		//Specimen Tensile N
		$this->fpdf->Cell( 60  , $row_height , $this->setUTF8((@$this->_data['income_info']['final_bolt_specimen_tensile_n'] < 1)?'':'n='.@$this->_data['income_info']['final_bolt_specimen_tensile_n']) , 'TR', 0 , 'C' );
		
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((@$this->_data['income_info']['final_bolt_proof_load_n'] < 1)?'':'n='.@$this->_data['income_info']['final_bolt_proof_load_n']) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8('Tensile Load') , 'LR', 1 , 'C' );
		$this->fpdf->SetY($y+($row_height*2));
		$this->fpdf->SetX(30);
		//Hardness N
		$this->fpdf->Cell( 20  , $row_height*2 , $this->setUTF8((@$this->_data['income_info']['final_bolt_hardness_n'] < 1)?'':'n='.@$this->_data['income_info']['final_bolt_hardness_n']) , 'TR', 0 , 'C' );
		$this->fpdf->SetFont('angsa','B',10);
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8('Yield') , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8('Tensile') , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8('Elongation') , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8('Reduction') , 'TR', 0 , 'C' );
		$this->fpdf->SetFont('angsa','B',12);
		$this->fpdf->Cell( 20  , $row_height*2 , $this->setUTF8('Load') , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height*2 , $this->setUTF8((@$this->_data['income_info']['final_bolt_wedge_tensile_load_n'] < 1)?'':'n='.@$this->_data['income_info']['final_bolt_wedge_tensile_load_n']) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height*2 , $this->setUTF8('Flat') , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height*2 , $this->setUTF8('Corner') , 'TR', 0 , 'C' );
		
		$this->fpdf->SetY($y+($row_height*3));
		$this->fpdf->SetX(50);
		$this->fpdf->SetFont('angsa','B',10);
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8('strength') , 'R', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8('strength') , 'R', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8('') , 'R', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8('of Area') , 'R', 1 , 'C' );
		
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8('Unit standard.') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8($hardness['unit']) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8($yield['unit']) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8($TensileStregth['unit']) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8($elongation['unit']) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8($reduction['unit']) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8($Proof['unit']) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8($Tensile['unit']) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8($Flat['unit']) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8($Corner['unit']) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8($HeadHeight['unit']) , 'TR', 1 , 'C' );
		
		$this->fpdf->Cell( 10  , $row_height*2 , $this->setUTF8('Spec.') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8('Min') , 'LTRB', 0 , 'C' );
		//Value Min
		$this->fpdf->SetFont('angsa','',10);
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($hardness['min'])==0)?"":($hardness['min'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8((($yield['min'])==0)?"":($yield['min'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8((($TensileStregth['min'])==0)?"":($TensileStregth['min'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8((($elongation['min'])==0)?"":($elongation['min'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8((($reduction['min'])==0)?"":($reduction['min'])) , 'TR', 0 , 'C' );
		
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($Proof['min'])==0)?"":($Proof['min'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($Tensile['min'])==0)?"":($Tensile['min'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8((($Flat['min'])==0)?"":($Flat['min'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8((($Corner['min'])==0)?"":($Corner['min'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($HeadHeight['min'])==0)?"":($HeadHeight['min'])) , 'TR', 1 , 'C' );
		
		$this->fpdf->SetFont('angsa','B',10);
		$this->fpdf->SetX(20);
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8('Max') , 'TR', 0 , 'C' );
		//Value Max
		$this->fpdf->SetFont('angsa','',10);
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($hardness['max'])==0)?"":($hardness['max'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8((($yield['max'])==0)?"":($yield['max'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8((($TensileStregth['max'])==0)?"":($TensileStregth['max'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8((($elongation['max'])==0)?"":($elongation['max'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8((($reduction['max'])==0)?"":($reduction['max'])) , 'TR', 0 , 'C' );
		
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($Proof['max'])==0)?"":($Proof['max'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($Tensile['max'])==0)?"":($Tensile['max'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8((($Flat['max'])==0)?"":($Flat['max'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8((($Corner['max'])==0)?"":($Corner['max'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($HeadHeight['max'])==0)?"":($HeadHeight['max'])) , 'TR', 1 , 'C' );
		
		$this->fpdf->SetFont('angsa','B',10);
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8('Result') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8('AVG') , 'TRB', 0 , 'C' );
		//Value AVG
		$this->fpdf->SetFont('angsa','',10);
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($hardness['avg'])==0)?"":($hardness['avg'])) , 'TRB', 0 ,'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8((($yield['avg'])==0)?"":($yield['avg'])) , 'TRB', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8((($TensileStregth['avg'])==0)?"":($TensileStregth['avg'])) , 'TRB', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8((($elongation['avg'])==0)?"":($elongation['avg'])) , 'TRB', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8((($reduction['avg'])==0)?"":($reduction['avg'])) , 'TRB', 0 , 'C' );
		
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($Proof['avg'])==0)?"":($Proof['avg'])) , 'TRB', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($Tensile['avg'])==0)?"":($Tensile['avg'])) , 'TRB', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8((($Flat['avg'])==0)?"":($Flat['avg'])) , 'TRB', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8((($Corner['avg'])==0)?"":($Corner['avg'])) , 'TRB', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($HeadHeight['avg'])==0)?"":($HeadHeight['avg'])) , 'TRB', 1 , 'C' );
		
	}
	function _Nut()
	{
		$hardness = $this->_std('nut','Hardness NO. 1');
		$Proof = $this->cer_size("nut","Proof Load");
		$Flat = $this->cer_size("nut","Width Across Flat");
		$Corner = $this->cer_size("nut","Width Across Corner");
		$Thickness = $this->cer_size("nut","Thickness");
		
		$row_height = 5;
		$setY = $this->fpdf->GetY();
		$this->fpdf->Ln(2);
		$this->fpdf->SetFont('angsa','B',12);
		$this->fpdf->Cell( 30  , $row_height , $this->setUTF8('2.2 NUT') , 0, 0 , 'L' );
		
		$this->fpdf->Cell( 40  , $row_height , $this->setUTF8('Material : '.@$this->_data['nut_info']['income_material']) , 0, 1 , 'L' );
		$this->fpdf->SetFont('angsa','',10);
		$this->fpdf->Cell( 40  , $row_height , $this->setUTF8('Marking : '.@$this->_data['nut_info']['grade_mark']) , 0, 0 , 'L' );
		$this->fpdf->Cell( 40  , $row_height , $this->setUTF8('Cer No : '.@$this->_data['nut_info']['income_certificate_no']) , 0, 1 , 'L' );
		$setY = $this->fpdf->GetY();
		$this->fpdf->SetFont('angsa','B',10);
		$this->fpdf->Cell( 20  , $row_height*3 , $this->setUTF8('Division') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height*2 , $this->setUTF8('Hardness') , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8('Proof') , 'TR', 0 , 'C' );
		
		$this->fpdf->Cell( 20  , $row_height*2 , $this->setUTF8('Width Across') , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height*3 , $this->setUTF8('Thickness') , 'TR', 1 , 'C' );
		$this->fpdf->SetY($setY+($row_height));
		$this->fpdf->SetX(50);
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8('load') , 'R', 0 , 'C' );
		$this->fpdf->SetY($setY+($row_height*2));
		$this->fpdf->SetX(30);
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((@$this->_data['income_info']['final_nut_hardness_n'] < 1)?'':'n='.@$this->_data['income_info']['final_nut_hardness_n']) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8((@$this->_data['income_info']['final_nut_proof_n'] < 1)?'':'n='.@$this->_data['income_info']['final_nut_proof_n']) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8('Flat') , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8('Corner') , 'TR', 1 , 'C' );
		
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8('Standard') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8($hardness['unit']) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8($Proof['unit']) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8($Flat['unit']) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8($Corner['unit']) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8($Thickness['unit']) , 'TR', 1 , 'C' );
		
		$this->fpdf->Cell( 10  , $row_height*2 , $this->setUTF8('Spec.') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8('Min') , 'LTRB', 0 , 'C' );
		//Value Min
		$this->fpdf->SetFont('angsa','',10);
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($hardness['min'])==0)?"":($hardness['min'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8((($Proof['min'])==0)?"":($Proof['min'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8((($Flat['min'])==0)?"":($Flat['min'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8((($Corner['min'])==0)?"":($Corner['min'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($Thickness['min'])==0)?"":($Thickness['min'])) , 'TR', 1 , 'C' );
		
		
		$this->fpdf->SetFont('angsa','B',10);
		$this->fpdf->SetX(20);
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8('Max') , 'TR', 0 , 'C' );
		//Value Max
		$this->fpdf->SetFont('angsa','',10);
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($hardness['max'])==0)?"":($hardness['max'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8((($Proof['max'])==0)?"":($Proof['max'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8((($Flat['max'])==0)?"":($Flat['max'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8((($Corner['max'])==0)?"":($Corner['max'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($Thickness['max'])==0)?"":($Thickness['max'])) , 'TR', 1 , 'C' );
		
		
		$this->fpdf->SetFont('angsa','B',10);
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8('Result') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8('AVG') , 'TRB', 0 , 'C' );
		//Value AVG
		$this->fpdf->SetFont('angsa','',10);
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($hardness['avg'])==0)?"":($hardness['avg'])) , 'TRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8((($Proof['avg'])==0)?"":($Proof['avg'])) , 'TRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8((($Flat['avg'])==0)?"":($Flat['avg'])) , 'TRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8((($Corner['avg'])==0)?"":($Corner['avg'])) , 'TRB', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($Thickness['avg'])==0)?"":($Thickness['avg'])) , 'TRB', 1 , 'C' );
		
		
		
	}
	function _Washer($setY)
	{
		$hardness = $this->_std('washer','Hardness NO. 1');
		$DiameterID = $this->cer_size("washer","Diameter ID");
		$DiameterOD = $this->cer_size("washer","Diameter OD");
		$Thickness = $this->cer_size("washer","Thickness");
		$this->fpdf->SetY($setY);
		$setX = ($this->hasNutInfo)?110:10;
		
		
		$row_height = 5;
		
		$this->fpdf->Ln(2);
		$this->fpdf->SetFont('angsa','B',12);
		$this->fpdf->SetX($setX);
		$this->fpdf->Cell( 30  , $row_height , $this->setUTF8('2.2 WASHER') , 0, 0 , 'L' );
		
		$this->fpdf->Cell( 40  , $row_height , $this->setUTF8('Material : '.@$this->_data['washer_info']['income_material']) , 0, 1 , 'L' );
		$this->fpdf->SetX($setX);
		$this->fpdf->SetFont('angsa','',10);
		$this->fpdf->Cell( 40  , $row_height , $this->setUTF8('Marking : '.@$this->_data['washer_info']['grade_mark']) , 0, 0 , 'L' );
		$this->fpdf->Cell( 40  , $row_height , $this->setUTF8('Cer No : '.@$this->_data['washer_info']['income_certificate_no']) , 0, 1 , 'L' );
		$setY = $this->fpdf->GetY();
		$this->fpdf->SetFont('angsa','B',10);
		$this->fpdf->SetX($setX);
		$this->fpdf->Cell( 20  , $row_height*3 , $this->setUTF8('Division') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height*2 , $this->setUTF8('Hardness') , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 30  , $row_height*2 , $this->setUTF8('Diameter') , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height*3 , $this->setUTF8('Thickness') , 'TR', 1 , 'C' );
		$this->fpdf->SetY($setY+($row_height*2));
		$this->fpdf->SetX($setX+20);
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((@$this->_data['income_info']['final_washer_hardness_n'] < 1)?'':'n='.@$this->_data['income_info']['final_washer_hardness_n']) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8('ID') , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8('OD') , 'TR', 1 , 'C' );
		$this->fpdf->SetX($setX);
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8('Standard') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8($hardness['unit']) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8($DiameterID['unit']) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8($DiameterOD['unit']) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8($DiameterOD['unit']) , 'TR', 1 , 'C' );
		
		$this->fpdf->SetX($setX);
		$this->fpdf->Cell( 10  , $row_height*2 , $this->setUTF8('Spec.') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8('Min') , 'LTRB', 0 , 'C' );
		//Value Min
		$this->fpdf->SetFont('angsa','',10);
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($hardness['min'])==0)?"":($hardness['min'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8((($DiameterID['min'])==0)?"":($DiameterID['min'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8((($DiameterOD['min'])==0)?"":($DiameterOD['min'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($Thickness['min'])==0)?"":($Thickness['min'])) , 'TR', 1 , 'C' );
		
		
		$this->fpdf->SetFont('angsa','B',10);
		$this->fpdf->SetX($setX+10);
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8('Max') , 'TR', 0 , 'C' );
		//Value Max
		$this->fpdf->SetFont('angsa','',10);
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($hardness['max'])==0)?"":($hardness['max'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8((($DiameterID['max'])==0)?"":($DiameterID['max'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8((($DiameterOD['max'])==0)?"":($DiameterOD['max'])) , 'TR', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($Thickness['max'])==0)?"":($Thickness['max'])) , 'TR', 1 , 'C' );
		
		$this->fpdf->SetX($setX);
		$this->fpdf->SetFont('angsa','B',10);
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8('Result') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 10  , $row_height , $this->setUTF8('AVG') , 'TRB', 0 , 'C' );
		//Value AVG
		$this->fpdf->SetX($setX+20);
		$this->fpdf->SetFont('angsa','',10);
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($hardness['avg'])==0)?"":($hardness['avg'])) , 'TRB', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8((($DiameterID['avg'])==0)?"":($DiameterID['avg'])) , 'TRB', 0 , 'C' );
		$this->fpdf->Cell( 15  , $row_height , $this->setUTF8((($DiameterOD['avg'])==0)?"":($DiameterOD['avg'])) , 'TRB', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($Thickness['avg'])==0)?"":($Thickness['avg'])) , 'TRB', 1 , 'C' );
		
	}
	function _VirtualThird()
	{
		$BoltThread = $this->cer_size_ok("bolt","Thread");
		$NutThread = $this->cer_size_ok("nut","Thread");
		$WasherThread = $this->cer_size_ok("washer","Thread");
		$BoltAppearance = $this->cer_size_ok("bolt","Appearance");
		$NutAppearance = $this->cer_size_ok("nut","Appearance");
		$WasherAppearance = $this->cer_size_ok("washer","Appearance");
		$row_height = 5;
		$this->fpdf->Ln(2);
		$this->fpdf->SetFont('angsa','B',12);
		$this->fpdf->Cell( 30  , $row_height , $this->setUTF8('3. Visual & Thread Inspection') , 0, 1 , 'L' );
		
		$this->fpdf->SetFont('angsa','B',10);
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8('Division') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8('Appearance') , 'TRB', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8('Thread') , 'TRB', 1 , 'C' );
		//Bolt
		$this->fpdf->SetFont('angsa','B',10);
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8('Bolt') , 'LRB', 0 , 'C' );
		$this->fpdf->SetFont('angsa','',10);
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8($BoltAppearance['avg']) , 'RB', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8($BoltThread['avg']) , 'RB', 1 , 'C' );
		//Nut
		$this->fpdf->SetFont('angsa','B',10);
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8('Nut') , 'LRB', 0 , 'C' );
		$this->fpdf->SetFont('angsa','',10);
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8($NutAppearance['avg']) , 'RB', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8($NutThread['avg']) , 'RB', 1 , 'C' );
		//Washer
		$this->fpdf->SetFont('angsa','B',10);
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8('Washer') , 'LRB', 0 , 'C' );
		$this->fpdf->SetFont('angsa','',10);
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8($WasherAppearance['avg']) , 'RB', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8($WasherThread['avg']) , 'RB', 1 , 'C' );
		
	}
	function _ThicknessGalvanized($setY)
	{
		$this->fpdf->SetY($setY);
		$setX = 110;
		$row_height = 5;
		
		$this->fpdf->Ln(2);
		$this->fpdf->SetFont('angsa','B',12);
		$this->fpdf->SetX($setX);
		$this->fpdf->Cell( 30  , $row_height , $this->setUTF8('4. Test Thickness Galvanized '.$this->_data['bolt_thickness_info']['grade']." ".$this->_data['bolt_thickness_info']['name']) , 0, 1 , 'L' );
		
		$this->fpdf->SetX($setX);
		$this->fpdf->SetFont('angsa','B',10);
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8('Division') , 'LTRB', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8('Spec.') , 'TRB', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8('Result') , 'TRB', 1 , 'C' );
		//Bolt
		
		$this->fpdf->SetX($setX);
		$this->fpdf->SetFont('angsa','B',10);
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8('Bolt') , 'LRB', 0 , 'C' );
		$this->fpdf->SetFont('angsa','',10);
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8($this->_data['bolt_thickness_info']['condition'].$this->_data['bolt_thickness_info']['condition_value']) , 'RB', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($this->_data['bolt_thickness']['thickness_value'])==0)?"":($this->_data['bolt_thickness']['thickness_value'])) , 'RB', 1 , 'C' );
		//Nut
		
		$this->fpdf->SetX($setX);
		$this->fpdf->SetFont('angsa','B',10);
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8('Nut') , 'LRB', 0 , 'C' );
		$this->fpdf->SetFont('angsa','',10);
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8($this->_data['nut_thickness_info']['condition'].$this->_data['nut_thickness_info']['condition_value']) , 'RB', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($this->_data['nut_thickness']['thickness_value'])==0)?"":($this->_data['nut_thickness']['thickness_value'])) , 'RB', 1 , 'C' );
		//Washer
		
		$this->fpdf->SetX($setX);
		$this->fpdf->SetFont('angsa','B',10);
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8('Washer') , 'LRB', 0 , 'C' );
		$this->fpdf->SetFont('angsa','',10);
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8($this->_data['washer_thickness_info']['condition'].$this->_data['washer_thickness_info']['condition_value']) , 'RB', 0 , 'C' );
		$this->fpdf->Cell( 20  , $row_height , $this->setUTF8((($this->_data['washer_thickness']['thickness_value'])==0)?"":($this->_data['washer_thickness']['thickness_value'])) , 'RB', 1 , 'C' );
		
	}
	function setUTF8($text)
	{
		return iconv( 'UTF-8','cp874//IGNORE',$text);
	}
	
	function cer_size($product,$type)
	{
		$p_min = (@$this->_data[$product.'_cer_size'][$type]['info']['ps_min']);
		$p_max = (@$this->_data[$product.'_cer_size'][$type]['info']['ps_max']);
		$p_avg = (@$this->_data[$product.'_cer_size'][$type]['test'][$product.'_testing_average']);
		$p_unit = @$this->_data[$product.'_cer_size'][$type]['info']['ps_max_unit'];
		return array(
			"min" => $p_min,
			"max" => $p_max,
			"avg" => $p_avg,
			"unit" => $p_unit,
		);
	}
	function cer_size_ok($product,$type)
	{
		$p_min = strtoupper(@$this->_data[$product.'_cer_size'][$type]['info']['ps_min']);
		$p_max = strtoupper(@$this->_data[$product.'_cer_size'][$type]['info']['ps_max']);
		$p_avg = strtoupper(@$this->_data[$product.'_cer_size'][$type]['test'][$product.'_testing_average']);
		$p_unit = @$this->_data[$product.'_cer_size'][$type]['info']['ps_max_unit'];
		return array(
			"min" => $p_min,
			"max" => $p_max,
			"avg" => $p_avg,
			"unit" => $p_unit,
		);
	}
	function _std($product,$type)
	{
		$p_min = (@$this->_data['std_'.$product][$type]['info']['ps_min']);
		$p_max = (@$this->_data['std_'.$product][$type]['info']['ps_max']);
		$p_avg = (@$this->_data['std_'.$product][$type]['test'][$product.'_testing_average']);
		$p_unit = @$this->_data['std_'.$product][$type]['info']['ps_max_unit'];
		return array(
			"min" => $p_min,
			"max" => $p_max,
			"avg" => $p_avg,
			"unit" => $p_unit,
		);
	}	
	
//	function _HardnessCalculate($type)
//	{
//		$min = 0;
//		$max = 0;
//		$avg = 0;
//		foreach($this->_data[$type] as $key=>$val){
//			if(strpos($key,"Hardness")!==false){
//				$ps_min = (@$this->_data['std_bolt'][$key]['info']['ps_min']);
//				$ps_max = (@$this->_data['std_bolt'][$key]['info']['ps_max']);
//				if($ps_min > $min){ $min = $ps_min; }
//				if($ps_max > $max){ $max = $ps_max; }
//			}
//		}
//		$avg = ($min+$max) / 2;
//		return array(
//			"min" => $min,
//			"max" => $max,
//			"avg" => $avg,
//		);
//		
//	}
//	function _YieldCalculate($type)
//	{
//		$min = 0;
//		$max = 0;
//		$avg = 0;
//		foreach($this->_data[$type] as $key=>$val){
//			if(strpos($key,"Yield")!==false){
//				$ps_min = (@$this->_data['std_bolt'][$key]['info']['ps_min']);
//				$ps_max = (@$this->_data['std_bolt'][$key]['info']['ps_max']);
//				if($ps_min > $min){ $min = $ps_min; }
//				if($ps_max > $max){ $max = $ps_max; }
//			}
//		}
//		$avg = ($min+$max) / 2;
//		return array(
//			"min" => $min,
//			"max" => $max,
//			"avg" => $avg,
//		);
//	}
//	function _ElongationCalculate($type)
//	{
//		$min = 0;
//		$max = 0;
//		$avg = 0;
//		foreach($this->_data[$type] as $key=>$val){
//			if(strpos($key,"Elongation")!==false){
//				$ps_min = (@$this->_data['std_bolt'][$key]['info']['ps_min']);
//				$ps_max = (@$this->_data['std_bolt'][$key]['info']['ps_max']);
//				if($ps_min > $min){ $min = $ps_min; }
//				if($ps_max > $max){ $max = $ps_max; }
//			}
//		}
//		$avg = ($min+$max) / 2;
//		return array(
//			"min" => $min,
//			"max" => $max,
//			"avg" => $avg,
//		);
//	}
//	function _ReductionofAreaCalculate($type)
//	{
//		$min = 0;
//		$max = 0;
//		$avg = 0;
//		foreach($this->_data[$type] as $key=>$val){
//			if(strpos($key,"Reduction of Area")!==false){
//				$ps_min = (@$this->_data['std_bolt'][$key]['info']['ps_min']);
//				$ps_max = (@$this->_data['std_bolt'][$key]['info']['ps_max']);
//				if($ps_min > $min){ $min = $ps_min; }
//				if($ps_max > $max){ $max = $ps_max; }
//			}
//		}
//		$avg = ($min+$max) / 2;
//		return array(
//			"min" => $min,
//			"max" => $max,
//			"avg" => $avg,
//		);
//	}
//	function _TensileStregthCalculate($type)
//	{
//		$min = 0;
//		$max = 0;
//		$avg = 0;
//		foreach($this->_data[$type] as $key=>$val){
//			if(strpos($key,"Tensile Stregth")!==false){
//				$ps_min = (@$this->_data['std_bolt'][$key]['info']['ps_min']);
//				$ps_max = (@$this->_data['std_bolt'][$key]['info']['ps_max']);
//				if($ps_min > $min){ $min = $ps_min; }
//				if($ps_max > $max){ $max = $ps_max; }
//			}
//		}
//		$avg = ($min+$max) / 2;
//		return array(
//			"min" => $min,
//			"max" => $max,
//			"avg" => $avg,
//		);
//	}
//	function _std_bolt($type)
//	{
//		$p_min = (@$this->_data['std_bolt'][$type]['info']['ps_min']);
//		$p_max = (@$this->_data['std_bolt'][$type]['info']['ps_max']);
//		$p_avg = (@$this->_data['std_bolt'][$type]['test']['bolt_testing_average']);
//		$p_unit = @$this->_data['std_bolt'][$type]['info']['ps_max_unit'];
//		return array(
//			"min" => $p_min,
//			"max" => $p_max,
//			"avg" => $p_avg,
//			"unit" => $p_unit,
//		);
//	}
//	function cer_size_bolt($type)
//	{
//		$p_min = (@$this->_data['cer_size'][$type]['info']['ps_min']);
//		$p_max = (@$this->_data['cer_size'][$type]['info']['ps_max']);
//		$p_avg = (@$this->_data['cer_size'][$type]['test']['bolt_testing_average']);
//		$p_unit = @$this->_data['cer_size'][$type]['info']['ps_max_unit'];
//		return array(
//			"min" => $p_min,
//			"max" => $p_max,
//			"avg" => $p_avg,
//			"unit" => $p_unit,
//		);
//	}
//	function cer_size_bolt_ok($type)
//	{
//		$p_min = (@$this->_data['cer_size'][$type]['info']['ps_min']);
//		$p_max = (@$this->_data['cer_size'][$type]['info']['ps_max']);
//		$p_avg = (@$this->_data['cer_size'][$type]['test']['bolt_testing_average']);
//		$p_unit = @$this->_data['cer_size'][$type]['info']['ps_max_unit'];
//		return array(
//			"min" => $p_min,
//			"max" => $p_max,
//			"avg" => $p_avg,
//			"unit" => $p_unit,
//		);
//	}
//	function cer_size($type)
//	{
//		$min = 0;
//		$max = 0;
//		$avg = 0;
//		for($i=1;$i<=5;$i++){
//			$p_val = $this->_data['cer_size'][$type]['test']['bolt_testing'.$i];
//			if($p_val < $min || $min==0){
//				$min=$p_val;
//			}
//			if($p_val > $max){
//				$max=$p_val;
//			}
//		}
//		$avg = ($min+$max) / 2;
//		return array(
//			"min" => $min,
//			"max" => $max,
//			"avg" => $avg,
//		);
//	}
//	function cer_size_bolt_ok($type)
//	{
//		$ok = 0;
//		$notok = 0;
//		$avg = 0;
//		for($i=1;$i<=5;$i++){
//			$p_val = $this->_data['cer_size'][$type]['test']['bolt_testing'.$i];
//			if($p_val == "OK"){
//				$ok++;
//			}else{
//				$notok++;
//			}
//		}
//		$avg = ($ok < $notok)?"NOT OK":"OK";
//		return array(
//			"avg" => $avg,
//		);
//	}
	
}