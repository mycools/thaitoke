<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Company_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		$this->mssql = $this->load->database("mssql", true);
		
	}
	##################### supplier #############################
	
	public function dataTable($limit=25,$offset = 0)
	{
		$offset = ($offset < 0)?0:$offset;

		$this->mssql->order_by("supplier_id","DESC");
		$this->mssql->limit($limit,$offset);
		return $this->mssql->get("supplier");
	}
	
	public function dataTable_Count()
	{
	
		return $this->mssql->count_all_results("supplier");
	}
	
	public function get_province()
	{
		$row = $this->db->get("system_province"); 
		return $row;
				
	}
	
	public function get_supplier_detail($supplier_id)
	{
		$this->mssql->where("supplier_id",$supplier_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("supplier")->row_array(); 
		return $row; 
	}
	
	public function add_supplier($data)
	{
		$this->mssql->set('supplier_name',$data['supplier_name']);
		$this->mssql->set('supplier_address',$data['supplier_address']);
		$this->mssql->set('supplier_province',$data['supplier_province']);
		$this->mssql->set('supplier_email',$data['supplier_email']);
		$this->mssql->set('supplier_tel',$data['supplier_tel']);
		$this->mssql->set('supplier_fax',$data['supplier_fax']);	
		$this->mssql->set('post_date',"NOW()",FALSE);		
		$this->mssql->insert('supplier');
	}
	
	public function edit_supplier($data)
	{
		
		$this->mssql->set('supplier_name',$data['supplier_name']);
		$this->mssql->set('supplier_address',$data['supplier_address']);
		$this->mssql->set('supplier_province',$data['supplier_province']);
		$this->mssql->set('supplier_email',$data['supplier_email']);
		$this->mssql->set('supplier_tel',$data['supplier_tel']);
		$this->mssql->set('supplier_fax',$data['supplier_fax']);
		$this->mssql->set('edit_date',"NOW()",FALSE);		
		$this->mssql->where('supplier_id',$data['supplier_id']);
		$this->mssql->update('supplier');
	}

		public function delete_supplier($supplier_id)
	{
		

		$this->mssql->where("supplier_id",$supplier_id);
	    $this->mssql->delete("supplier");	
	}
	
	##################### end supplier #############################
	
	
	##################### customer #############################
	
		public function dataTable_customer($limit=25,$offset = 0)
	{
		$offset = ($offset < 0)?0:$offset;

		$this->mssql->order_by("customer_id","DESC");
		$this->mssql->limit($limit,$offset);
		return $this->mssql->get("customer");
	}
	
	public function dataTable_customer_Count()
	{
	
		return $this->mssql->count_all_results("customer");
	}

	
	public function get_customer_detail($customer_id)
	{
		$this->mssql->where("customer_id",$customer_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("customer")->row_array(); 
		return $row; 
	}
	
	public function add_customer($data)
	{
		$this->mssql->set('customer_name',$data['customer_name']);
		$this->mssql->set('customer_address',$data['customer_address']);
		$this->mssql->set('customer_province',$data['customer_province']);
		$this->mssql->set('customer_email',$data['customer_email']);
		$this->mssql->set('customer_tel',$data['customer_tel']);
		$this->mssql->set('customer_fax',$data['customer_fax']);	
		$this->mssql->set('post_date',"NOW()",FALSE);		
		$this->mssql->insert('customer');
	}
	
	public function edit_customer($data)
	{
		
		$this->mssql->set('customer_name',$data['customer_name']);
		$this->mssql->set('customer_address',$data['customer_address']);
		$this->mssql->set('customer_province',$data['customer_province']);
		$this->mssql->set('customer_email',$data['customer_email']);
		$this->mssql->set('customer_tel',$data['customer_tel']);
		$this->mssql->set('customer_fax',$data['customer_fax']);
		$this->mssql->set('edit_date',"NOW()",FALSE);		
		$this->mssql->where('customer_id',$data['customer_id']);
		$this->mssql->update('customer');
	}

		public function delete_customer($customer_id)
	{
		

		$this->mssql->where("customer_id",$customer_id);
	    $this->mssql->delete("customer");	
	}
	
	##################### end customer #############################


}