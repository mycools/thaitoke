<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Final_test_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		$this->mssql = $this->load->database("mssql", true);
		
	}
	##################### final test bolt #############################
	
		
	public function get_final_bolt($income_id,$final_id)
	{
		$this->mssql->where("final_bolt_income_id",$income_id);
		$this->mssql->where("final_id",$final_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("final_bolt")->row_array(); 
		return $row; 
	}
	
	public function get_final_bolt_test_info($final_id,$std_id)
	{
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("bolt_standard_id",$std_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("final_bolt_testing")->row_array(); 
		return $row;	
		
		
	}

	public function get_final_bolt_size_test_info($final_id,$std_id)
	{
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("bolt_cer_size_id",$std_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("final_size_bolt_testing")->row_array(); 
		return $row;	
		
		
	}
	
	public function get_bolt_product($income_id,$final_id)
	{
		$final = $this->get_final_bolt($income_id,$final_id);
		
		$this->mssql->where("product_id",$final['final_bolt_id']);
		$this->mssql->limit(1);
		$row = $this->mssql->get("product_bolt")->row_array(); 
		return $row; 
	}
	
	public function get_inspection($inspection_id)
	{
		$this->mssql->where("inspection_id",$inspection_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("inspection")->row_array(); 
		return $row; 
	}
	
	public function get_product_standard_bolt($product_id)
	{
		/* $this->mssql->where('(ps_min <> 0 OR ps_max <> 0)'); */
		$this->mssql->where("product_id",$product_id);
		$row = $this->mssql->get("product_standard_bolt"); 
		return $row;
	}
	
	public function get_product_standard_size_bolt($product_id,$size_id)
	{
		/* $this->mssql->where('(ps_min <> NULL OR ps_max <> NULL)'); */
		$this->mssql->where("product_id",$product_id);
		$this->mssql->where("size_id",$size_id);
		$row = $this->mssql->get("product_cer_size_bolt"); 
		//exit($this->mssql->last_query());
		return $row;
	}
	
	public function add_bolt_testing_cer($data)
	{
		/*
var_dump($_POST);
		exit();
*/		
		if($_POST['bolt_standard_id'] != NULL){
			foreach($_POST['bolt_standard_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('bolt_standard_id',$_POST['bolt_standard_id'][$key]);
				$this->mssql->set('bolt_testing1',$_POST['test1'][$key]);
				$this->mssql->set('bolt_testing2',$_POST['test2'][$key]);
				$this->mssql->set('bolt_testing3',$_POST['test3'][$key]);
				$this->mssql->set('bolt_testing4',$_POST['test4'][$key]);
				$this->mssql->set('bolt_testing5',$_POST['test5'][$key]);
				$this->mssql->set('bolt_testing_status',$_POST['testing_status'][$key]);
				$this->mssql->set('bolt_testing_remark',$_POST['remark'][$key]);
				$this->mssql->set('bolt_testing_post_user_id',$data['bolt_testing_post_user_id']);	
				$this->mssql->set('bolt_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('cer_bolt_testing');
			}
		}
		
		if($_POST['bolt_cer_size_id'] != NULL){
			foreach($_POST['bolt_cer_size_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('income_id',$_POST['income_id']);
				$this->mssql->set('bolt_cer_size_id',$_POST['bolt_cer_size_id'][$key]);
				$this->mssql->set('bolt_testing1',$_POST['ctest1'][$key]);
				$this->mssql->set('bolt_testing2',$_POST['ctest2'][$key]);
				$this->mssql->set('bolt_testing3',$_POST['ctest3'][$key]);
				$this->mssql->set('bolt_testing4',$_POST['ctest4'][$key]);
				$this->mssql->set('bolt_testing5',$_POST['ctest5'][$key]);
				$this->mssql->set('bolt_testing_status',$_POST['ctesting_status'][$key]);
				$this->mssql->set('bolt_testing_remark',$_POST['cremark'][$key]);
				$this->mssql->set('bolt_testing_post_user_id',$data['bolt_testing_post_user_id']);	
				$this->mssql->set('bolt_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('cer_size_bolt_testing');
			}
		}
		
		
	}
	
	public function update_bolt_testing_final($data)
	{
		foreach($_POST['bolt_standard_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('final_status',"tested");
				$this->mssql->set('bolt_testing1',$_POST['test1'][$key]);
				$this->mssql->set('bolt_testing2',$_POST['test2'][$key]);
				$this->mssql->set('bolt_testing3',$_POST['test3'][$key]);
				$this->mssql->set('bolt_testing4',$_POST['test4'][$key]);
				$this->mssql->set('bolt_testing5',$_POST['test5'][$key]);
				$this->mssql->set('bolt_testing_average',@$_POST['test_avg'][$key]);
				$this->mssql->set('bolt_testing_status',$_POST['testing_status'][$key]);
				$this->mssql->set('bolt_testing_remark',$_POST['remark'][$key]);
				$this->mssql->set('bolt_testing_post_user_id',$data['bolt_testing_post_user_id']);	
				$this->mssql->set('bolt_testing_post_date',"NOW()",FALSE);
				$this->mssql->where("bolt_testing_id",$_POST['bolt_testing_id'][$key]);	
				$this->mssql->update('final_bolt_testing');
			}
			
		foreach($_POST['bolt_cer_size_id'] as $key=>$value){
		
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('bolt_testing1',$_POST['ctest1'][$key]);
				$this->mssql->set('bolt_testing2',$_POST['ctest2'][$key]);
				$this->mssql->set('bolt_testing3',$_POST['ctest3'][$key]);
				$this->mssql->set('bolt_testing4',$_POST['ctest4'][$key]);
				$this->mssql->set('bolt_testing5',$_POST['ctest5'][$key]);
				$this->mssql->set('bolt_testing_average',@$_POST['ctest_avg'][$key]);
				$this->mssql->set('bolt_testing_status',$_POST['ctesting_status'][$key]);
				$this->mssql->set('bolt_testing_remark',$_POST['cremark'][$key]);
				$this->mssql->set('bolt_testing_post_user_id',$data['bolt_testing_post_user_id']);	
				$this->mssql->set('bolt_testing_post_date',"NOW()",FALSE);
				$this->mssql->where("bolt_testing_id",$_POST['bolt_testing_final_id'][$key]);	
				$this->mssql->update('final_size_bolt_testing');
			}
		
	}
	
	public function add_bolt_testing_final($data)
	{
		foreach(@$_POST['bolt_standard_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('income_id',@$_POST['income_id']);
				$this->mssql->set('bolt_standard_id',@$_POST['bolt_standard_id'][$key]);
				$this->mssql->set('bolt_testing1',@$_POST['test1'][$key]);
				$this->mssql->set('bolt_testing2',@$_POST['test2'][$key]);
				$this->mssql->set('bolt_testing3',@$_POST['test3'][$key]);
				$this->mssql->set('bolt_testing4',@$_POST['test4'][$key]);
				$this->mssql->set('bolt_testing5',@$_POST['test5'][$key]);
				$this->mssql->set('bolt_testing_average',@$_POST['test_avg'][$key]);
				$this->mssql->set('bolt_testing_status',@$_POST['testing_status'][$key]);
				$this->mssql->set('bolt_testing_remark',@$_POST['remark'][$key]);
				$this->mssql->set('bolt_testing_post_user_id',$data['bolt_testing_post_user_id']);	
				$this->mssql->set('bolt_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('final_bolt_testing');
			}
			
		if(@$_POST['bolt_cer_size_id'] != NULL){
			foreach(@$_POST['bolt_cer_size_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('income_id',@$_POST['income_id']);
				$this->mssql->set('bolt_cer_size_id',@$_POST['bolt_cer_size_id'][$key]);
				$this->mssql->set('bolt_testing1',@$_POST['ctest1'][$key]);
				$this->mssql->set('bolt_testing2',@$_POST['ctest2'][$key]);
				$this->mssql->set('bolt_testing3',@$_POST['ctest3'][$key]);
				$this->mssql->set('bolt_testing4',@$_POST['ctest4'][$key]);
				$this->mssql->set('bolt_testing5',@$_POST['ctest5'][$key]);
				$this->mssql->set('bolt_testing_average',@$_POST['ctest_avg'][$key]);
				$this->mssql->set('bolt_testing_status',@$_POST['ctesting_status'][$key]);
				$this->mssql->set('bolt_testing_remark',@$_POST['cremark'][$key]);
				$this->mssql->set('bolt_testing_post_user_id',$data['bolt_testing_post_user_id']);	
				$this->mssql->set('bolt_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('final_size_bolt_testing');
			}
		}
		
	}
	
	public function delete_supplier($supplier_id)
	{
		

		$this->mssql->where("supplier_id",$supplier_id);
	    $this->mssql->delete("supplier");	
	}
	
	public function check_final_tested($income_id,$final_id)
	{
			   $this->mssql->where("final_id",$final_id);
			   $this->mssql->where("income_id",$income_id);
		return $this->mssql->count_all_results("final_bolt_testing");
	}
	
	
	public function get_final_tested($income_id,$std_id)
	{
		$this->mssql->where("bolt_standard_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("final_bolt_testing")->row_array();
	}
	
	public function get_final_cer_size_tested($income_id,$std_id){
		$this->mssql->where("bolt_cer_size_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("final_size_bolt_testing")->row_array();

		
	}
	
	public function get_final_tested_main($final_id,$income_id,$std_id)
	{
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("bolt_standard_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("final_bolt_testing")->row_array();
	}
	
	public function get_final_cer_size_tested_main($final_id,$income_id,$std_id){
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("bolt_cer_size_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("final_size_bolt_testing")->row_array();

		
	}
	
	public function check_final_tested_bolt($income_id,$final_id)
	{
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->count_all_results("final_bolt_testing");
	}
	public function get_chemical_final_bolt($income_id,$final_id)
	{
		$this->mssql->select("chemical_final_bolt.*,product_chemical_bolt.ps_min,product_chemical_bolt.ps_max");
		$this->mssql->join("product_chemical_bolt","product_chemical_bolt.chemical_id=chemical_final_bolt.chemical_id");
		$this->mssql->where("chemical_final_bolt.income_id",$income_id);
		$this->mssql->where("chemical_final_bolt.final_id",$final_id);
		$this->mssql->order_by("chemical_final_bolt.chemical_id", "ASC");
		return $this->mssql->get("chemical_final_bolt")->result_array();	
	}
	
	##################### end test bolt #############################
	
	
	
	##################### start test nut ############################
		public function get_final_nut_main($income_id,$final_id)
	{
		$this->mssql->where("final_nut_income_id",$income_id);
		$this->mssql->where("final_id",$final_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("final_nut")->row_array(); 
		return $row; 
	}
	public function get_nut_product_main($income_id,$final_id)
	{
		$final = $this->get_final_nut_main($income_id,$final_id);
		
		$this->mssql->where("product_id",$final['final_nut_id']);
		$this->mssql->limit(1);
		$row = $this->mssql->get("product_nut")->row_array(); 
		return $row; 
	}
		public function get_final_nut($income_id,$final_id)
	{
		$this->mssql->where("final_nut_income_id",$income_id);
		$this->mssql->where("final_id",$final_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("final_bolt")->row_array(); 
		return $row; 
	}
	public function get_nut_product($income_id,$final_id)
	{
		$final = $this->get_final_nut($income_id,$final_id);
		
		$this->mssql->where("product_id",$final['final_nut_id']);
		$this->mssql->limit(1);
		$row = $this->mssql->get("product_nut")->row_array(); 
		return $row; 
	}
	
	public function get_final_nut_test_info($final_id,$std_id)
	{
		
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("nut_standard_id",$std_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("final_nut_testing")->row_array(); 
		return $row;	
		
		
	}

	public function get_final_nut_size_test_info($final_id,$std_id)
	{
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("nut_cer_size_id",$std_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("final_size_nut_testing")->row_array(); 
		return $row;	
		
		
	}

	public function get_product_standard_nut($product_id)
	{
		/* $this->mssql->where('(ps_min <> 0 OR ps_max <> 0)'); */
		$this->mssql->where("product_id",$product_id);
		$row = $this->mssql->get("product_standard_nut"); 
		return $row;
	}
	
	public function get_product_standard_size_nut($product_id,$size_id)
	{
		
		/* $this->mssql->where('(ps_min <> NULL OR ps_max <> NULL)'); */
		$this->mssql->where("product_id",$product_id);
		$this->mssql->where("size_id",$size_id);
		$row = $this->mssql->get("product_cer_size_nut"); 
		return $row;
	}
	
	public function add_nut_testing_cer($data)
	{
		/*
var_dump($_POST);
		exit();
*/		
		if($_POST['nut_standard_id'] != NULL){
			foreach($_POST['nut_standard_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('nut_standard_id',$_POST['nut_standard_id'][$key]);
				$this->mssql->set('nut_testing1',$_POST['test1'][$key]);
				$this->mssql->set('nut_testing2',$_POST['test2'][$key]);
				$this->mssql->set('nut_testing3',$_POST['test3'][$key]);
				$this->mssql->set('nut_testing4',$_POST['test4'][$key]);
				$this->mssql->set('nut_testing5',$_POST['test5'][$key]);
				$this->mssql->set('nut_testing_average',$_POST['test_avg'][$key]);
				$this->mssql->set('nut_testing_status',$_POST['testing_status'][$key]);
				$this->mssql->set('nut_testing_remark',$_POST['remark'][$key]);
				$this->mssql->set('nut_testing_post_user_id',$data['nut_testing_post_user_id']);	
				$this->mssql->set('nut_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('cer_nut_testing');
			}
		}
		
		if($_POST['nut_cer_size_id'] != NULL){
			foreach($_POST['nut_cer_size_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('income_id',$_POST['income_id']);
				$this->mssql->set('nut_cer_size_id',$_POST['nut_cer_size_id'][$key]);
				$this->mssql->set('nut_testing1',$_POST['ctest1'][$key]);
				$this->mssql->set('nut_testing2',$_POST['ctest2'][$key]);
				$this->mssql->set('nut_testing3',$_POST['ctest3'][$key]);
				$this->mssql->set('nut_testing4',$_POST['ctest4'][$key]);
				$this->mssql->set('nut_testing5',$_POST['ctest5'][$key]);
				$this->mssql->set('nut_testing_average',$_POST['ctest_avg'][$key]);
				$this->mssql->set('nut_testing_status',$_POST['ctesting_status'][$key]);
				$this->mssql->set('nut_testing_remark',$_POST['cremark'][$key]);
				$this->mssql->set('nut_testing_post_user_id',$data['nut_testing_post_user_id']);	
				$this->mssql->set('nut_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('cer_size_nut_testing');
			}
		}
		
		
	}
	
	public function update_nut_testing_final($data)
	{
		foreach($_POST['nut_standard_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('final_status',"tested");
				$this->mssql->set('nut_testing1',$_POST['test1'][$key]);
				$this->mssql->set('nut_testing2',$_POST['test2'][$key]);
				$this->mssql->set('nut_testing3',$_POST['test3'][$key]);
				$this->mssql->set('nut_testing4',$_POST['test4'][$key]);
				$this->mssql->set('nut_testing5',$_POST['test5'][$key]);
				$this->mssql->set('nut_testing_average',$_POST['test_avg'][$key]);
				$this->mssql->set('nut_testing_status',$_POST['testing_status'][$key]);
				$this->mssql->set('nut_testing_remark',$_POST['remark'][$key]);
				$this->mssql->set('nut_testing_post_user_id',$data['nut_testing_post_user_id']);	
				$this->mssql->set('nut_testing_post_date',"NOW()",FALSE);
				$this->mssql->where("nut_testing_id",$_POST['nut_testing_id'][$key]);	
				$this->mssql->update('final_nut_testing');
			}
			
		foreach($_POST['nut_cer_size_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('nut_testing1',$_POST['ctest1'][$key]);
				$this->mssql->set('nut_testing2',$_POST['ctest2'][$key]);
				$this->mssql->set('nut_testing3',$_POST['ctest3'][$key]);
				$this->mssql->set('nut_testing4',$_POST['ctest4'][$key]);
				$this->mssql->set('nut_testing5',$_POST['ctest5'][$key]);
				$this->mssql->set('nut_testing_average',$_POST['ctest_avg'][$key]);
				$this->mssql->set('nut_testing_status',$_POST['ctesting_status'][$key]);
				$this->mssql->set('nut_testing_remark',$_POST['cremark'][$key]);
				$this->mssql->set('nut_testing_post_user_id',$data['nut_testing_post_user_id']);	
				$this->mssql->set('nut_testing_post_date',"NOW()",FALSE);
				$this->mssql->where("nut_testing_id",$_POST['nut_testing_final_id'][$key]);	
				$this->mssql->update('final_size_nut_testing');
			}
		
	}
	
	public function add_nut_testing_final($data)
	{
		foreach(@$_POST['nut_standard_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('income_id',@$_POST['income_id']);
				$this->mssql->set('nut_standard_id',@$_POST['nut_standard_id'][$key]);
				$this->mssql->set('nut_testing1',@$_POST['test1'][$key]);
				$this->mssql->set('nut_testing2',@$_POST['test2'][$key]);
				$this->mssql->set('nut_testing3',@$_POST['test3'][$key]);
				$this->mssql->set('nut_testing4',@$_POST['test4'][$key]);
				$this->mssql->set('nut_testing5',@$_POST['test5'][$key]);
				$this->mssql->set('nut_testing_average',@$_POST['test_avg'][$key]);
				$this->mssql->set('nut_testing_status',@$_POST['testing_status'][$key]);
				$this->mssql->set('nut_testing_remark',@$_POST['remark'][$key]);
				$this->mssql->set('nut_testing_post_user_id',$data['nut_testing_post_user_id']);	
				$this->mssql->set('nut_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('final_nut_testing');
			}
			
		if(@$_POST['nut_cer_size_id'] != NULL){
			foreach(@$_POST['nut_cer_size_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('income_id',@$_POST['income_id']);
				$this->mssql->set('nut_cer_size_id',@$_POST['nut_cer_size_id'][$key]);
				$this->mssql->set('nut_testing1',@$_POST['ctest1'][$key]);
				$this->mssql->set('nut_testing2',@$_POST['ctest2'][$key]);
				$this->mssql->set('nut_testing3',@$_POST['ctest3'][$key]);
				$this->mssql->set('nut_testing4',@$_POST['ctest4'][$key]);
				$this->mssql->set('nut_testing5',@$_POST['ctest5'][$key]);
				$this->mssql->set('nut_testing_average',@$_POST['ctest_average'][$key]);
				$this->mssql->set('nut_testing_status',@$_POST['ctesting_status'][$key]);
				$this->mssql->set('nut_testing_remark',@$_POST['cremark'][$key]);
				$this->mssql->set('nut_testing_post_user_id',$data['nut_testing_post_user_id']);	
				$this->mssql->set('nut_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('final_size_nut_testing');
			}
		}
		
	}

		
	public function check_final_nut_tested($income_id,$final_id)
	{
			   $this->mssql->where("final_id",$final_id);
			   $this->mssql->where("income_id",$income_id);
		return $this->mssql->count_all_results("final_nut_testing");
	}
	public function get_final_nut_tested($income_id,$std_id)
	{
		$this->mssql->where("nut_standard_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("final_nut_testing")->row_array();
	}
	
	public function get_final_nut_cer_size_tested($income_id,$std_id){
		$this->mssql->where("nut_cer_size_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("final_size_nut_testing")->row_array();

		
	}
	
	public function get_final_nut_tested_main($final_id,$income_id,$std_id)
	{
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("nut_standard_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("final_nut_testing")->row_array();
	}
	
	public function get_final_nut_cer_size_tested_main($final_id,$income_id,$std_id){
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("nut_cer_size_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("final_size_nut_testing")->row_array();

		
	}
	
	public function check_final_tested_nut($income_id,$final_id)
	{
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->count_all_results("final_nut_testing");
	}

	
	public function get_chemical_final_nut($income_id,$final_id)
	{
		$this->mssql->select("chemical_final_nut.*,product_chemical_nut.ps_min,product_chemical_nut.ps_max");
		$this->mssql->join("product_chemical_nut","product_chemical_nut.chemical_id=chemical_final_nut.chemical_id");
		$this->mssql->where("chemical_final_nut.income_id",$income_id);
		$this->mssql->where("chemical_final_nut.final_id",$final_id);
		$this->mssql->ORDER_by("chemical_id", "ASC");
		return $this->mssql->get("chemical_final_nut")->result_array();	
	}
	##################### end test nut ##############################
	
##################### start test washer ############################
	public function get_final_washer_main($income_id,$final_id)
	{
		$this->mssql->where("final_washer_income_id",$income_id);
		$this->mssql->where("final_id",$final_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("final_washer")->row_array(); 
		return $row; 
	}
	public function get_washer_product_main($income_id,$final_id)
	{
		$final = $this->get_final_washer_main($income_id,$final_id);
		
		$this->mssql->where("product_id",$final['final_washer_id']);
		$this->mssql->limit(1);
		$row = $this->mssql->get("product_washer")->row_array(); 
		return $row; 
	}
		public function get_final_washer($income_id,$final_id)
	{
		$this->mssql->where("final_washer_income_id",$income_id);
		$this->mssql->where("final_id",$final_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("final_bolt")->row_array(); 
		return $row; 
	}
	public function get_washer_product($income_id,$final_id)
	{
		$final = $this->get_final_washer($income_id,$final_id);
		
		$this->mssql->where("product_id",$final['final_washer_id']);
		$this->mssql->limit(1);
		$row = $this->mssql->get("product_washer")->row_array(); 
		return $row; 
	}
	
	public function get_final_washer_test_info($final_id,$std_id)
	{
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("washer_standard_id",$std_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("final_washer_testing")->row_array(); 
		return $row;	
		
		
	}

	public function get_final_washer_size_test_info($final_id,$std_id)
	{
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("washer_cer_size_id",$std_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("final_size_washer_testing")->row_array(); 
		return $row;	
		
		
	}

	public function get_product_standard_washer($product_id)
	{
		/* $this->mssql->where('(ps_min <> 0 OR ps_max <> 0)'); */
		$this->mssql->where("product_id",$product_id);
		$row = $this->mssql->get("product_standard_washer"); 
		return $row;
	}
	
	public function get_product_standard_size_washer($product_id,$size_id)
	{
		
		/* $this->mssql->where('(ps_min <> NULL OR ps_max <> NULL)'); */
		$this->mssql->where("product_id",$product_id);
		$this->mssql->where("size_id",$size_id);
		$row = $this->mssql->get("product_cer_size_washer"); 
		return $row;
	}
	
	public function add_washer_testing_cer($data)
	{
		/*
var_dump($_POST);
		exit();
*/		
		if($_POST['washer_standard_id'] != NULL){
			foreach($_POST['washer_standard_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('washer_standard_id',$_POST['washer_standard_id'][$key]);
				$this->mssql->set('washer_testing1',$_POST['test1'][$key]);
				$this->mssql->set('washer_testing2',$_POST['test2'][$key]);
				$this->mssql->set('washer_testing3',$_POST['test3'][$key]);
				$this->mssql->set('washer_testing4',$_POST['test4'][$key]);
				$this->mssql->set('washer_testing5',$_POST['test5'][$key]);
				$this->mssql->set('washer_testing_average',$_POST['test_avg'][$key]);
				$this->mssql->set('washer_testing_status',$_POST['testing_status'][$key]);
				$this->mssql->set('washer_testing_remark',$_POST['remark'][$key]);
				$this->mssql->set('washer_testing_post_user_id',$data['washer_testing_post_user_id']);	
				$this->mssql->set('washer_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('cer_washer_testing');
			}
		}
		
		if($_POST['washer_cer_size_id'] != NULL){
			foreach($_POST['washer_cer_size_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('income_id',$_POST['income_id']);
				$this->mssql->set('washer_cer_size_id',$_POST['washer_cer_size_id'][$key]);
				$this->mssql->set('washer_testing1',$_POST['ctest1'][$key]);
				$this->mssql->set('washer_testing2',$_POST['ctest2'][$key]);
				$this->mssql->set('washer_testing3',$_POST['ctest3'][$key]);
				$this->mssql->set('washer_testing4',$_POST['ctest4'][$key]);
				$this->mssql->set('washer_testing5',$_POST['ctest5'][$key]);
				$this->mssql->set('washer_testing_average',$_POST['ctest_avg'][$key]);
				$this->mssql->set('washer_testing_status',$_POST['ctesting_status'][$key]);
				$this->mssql->set('washer_testing_remark',$_POST['cremark'][$key]);
				$this->mssql->set('washer_testing_post_user_id',$data['washer_testing_post_user_id']);	
				$this->mssql->set('washer_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('cer_size_washer_testing');
			}
		}
		
		
	}
	
	public function update_washer_testing_final($data)
	{
		foreach($_POST['washer_standard_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('final_status',"tested");
				$this->mssql->set('washer_testing1',$_POST['test1'][$key]);
				$this->mssql->set('washer_testing2',$_POST['test2'][$key]);
				$this->mssql->set('washer_testing3',$_POST['test3'][$key]);
				$this->mssql->set('washer_testing4',$_POST['test4'][$key]);
				$this->mssql->set('washer_testing5',$_POST['test5'][$key]);
				$this->mssql->set('washer_testing_average',$_POST['test_avg'][$key]);
				$this->mssql->set('washer_testing_status',$_POST['testing_status'][$key]);
				$this->mssql->set('washer_testing_remark',$_POST['remark'][$key]);
				$this->mssql->set('washer_testing_post_user_id',$data['washer_testing_post_user_id']);	
				$this->mssql->set('washer_testing_post_date',"NOW()",FALSE);
				$this->mssql->where("washer_testing_id",$_POST['washer_testing_id'][$key]);	
				$this->mssql->update('final_washer_testing');
			}
			
		foreach($_POST['washer_cer_size_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('washer_testing1',$_POST['ctest1'][$key]);
				$this->mssql->set('washer_testing2',$_POST['ctest2'][$key]);
				$this->mssql->set('washer_testing3',$_POST['ctest3'][$key]);
				$this->mssql->set('washer_testing4',$_POST['ctest4'][$key]);
				$this->mssql->set('washer_testing5',$_POST['ctest5'][$key]);
				$this->mssql->set('washer_testing_average',$_POST['ctest_avg'][$key]);
				$this->mssql->set('washer_testing_status',$_POST['ctesting_status'][$key]);
				$this->mssql->set('washer_testing_remark',$_POST['cremark'][$key]);
				$this->mssql->set('washer_testing_post_user_id',$data['washer_testing_post_user_id']);	
				$this->mssql->set('washer_testing_post_date',"NOW()",FALSE);
				$this->mssql->where("washer_testing_id",$_POST['washer_testing_final_id'][$key]);	
				$this->mssql->update('final_size_washer_testing');
			}
		
	}
	
	public function add_washer_testing_final($data)
	{
		foreach(@$_POST['washer_standard_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('income_id',@$_POST['income_id']);
				$this->mssql->set('washer_standard_id',@$_POST['washer_standard_id'][$key]);
				$this->mssql->set('washer_testing1',@$_POST['test1'][$key]);
				$this->mssql->set('washer_testing2',@$_POST['test2'][$key]);
				$this->mssql->set('washer_testing3',@$_POST['test3'][$key]);
				$this->mssql->set('washer_testing4',@$_POST['test4'][$key]);
				$this->mssql->set('washer_testing5',@$_POST['test5'][$key]);
				$this->mssql->set('washer_testing_average',@$_POST['test_avg'][$key]);
				$this->mssql->set('washer_testing_status',@$_POST['testing_status'][$key]);
				$this->mssql->set('washer_testing_remark',@$_POST['remark'][$key]);
				$this->mssql->set('washer_testing_post_user_id',$data['washer_testing_post_user_id']);	
				$this->mssql->set('washer_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('final_washer_testing');
			}
			
		if(@$_POST['washer_cer_size_id'] != NULL){
			foreach(@$_POST['washer_cer_size_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('income_id',@$_POST['income_id']);
				$this->mssql->set('washer_cer_size_id',@$_POST['washer_cer_size_id'][$key]);
				$this->mssql->set('washer_testing1',@$_POST['ctest1'][$key]);
				$this->mssql->set('washer_testing2',@$_POST['ctest2'][$key]);
				$this->mssql->set('washer_testing3',@$_POST['ctest3'][$key]);
				$this->mssql->set('washer_testing4',@$_POST['ctest4'][$key]);
				$this->mssql->set('washer_testing5',@$_POST['ctest5'][$key]);
				$this->mssql->set('washer_testing_average',@$_POST['ctest_avg'][$key]);
				$this->mssql->set('washer_testing_status',@$_POST['ctesting_status'][$key]);
				$this->mssql->set('washer_testing_remark',@$_POST['cremark'][$key]);
				$this->mssql->set('washer_testing_post_user_id',$data['washer_testing_post_user_id']);	
				$this->mssql->set('washer_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('final_size_washer_testing');
			}
		}
		
	}
		
	public function check_final_washer_tested($income_id,$final_id)
	{
				$this->mssql->where("final_id",$final_id);
			   $this->mssql->where("income_id",$income_id);
		return $this->mssql->count_all_results("final_washer_testing");
	}
	public function get_final_washer_tested($income_id,$std_id)
	{
		$this->mssql->where("washer_standard_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("final_washer_testing")->row_array();
	}
	
	public function get_final_washer_cer_size_tested($income_id,$std_id){
		$this->mssql->where("washer_cer_size_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("final_size_washer_testing")->row_array();

		
	}
	
	public function get_final_washer_tested_main($final_id,$income_id,$std_id)
	{
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("washer_standard_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("final_washer_testing")->row_array();
	}
	
	public function get_final_washer_cer_size_tested_main($final_id,$income_id,$std_id){
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("washer_cer_size_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("final_size_washer_testing")->row_array();

		
	}
	
	public function check_final_tested_washer($income_id,$final_id)
	{
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->count_all_results("final_washer_testing");
	}

	public function get_chemical_final_washer($income_id,$final_id)
	{
		$this->mssql->select("chemical_final_washer.*,product_chemical_washer.ps_min,product_chemical_washer.ps_max");
		$this->mssql->join("product_chemical_washer","product_chemical_washer.chemical_id=chemical_final_washer.chemical_id");
		$this->mssql->where("chemical_final_washer.income_id",$income_id);
		$this->mssql->where("chemical_final_washer.final_id",$final_id);
		$this->mssql->ORDER_by("chemical_id", "ASC");
		return $this->mssql->get("chemical_final_washer")->result_array();	
	}
	
	##################### end test washer ##############################
	
	
	##################### start test spring ############################
	public function get_final_spring_main($income_id,$final_id)
	{
		$this->mssql->where("final_spring_income_id",$income_id);
		$this->mssql->where("final_id",$final_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("final_spring")->row_array(); 
		return $row; 
	}
	public function get_spring_product_main($income_id,$final_id)
	{
		$final = $this->get_final_spring_main($income_id,$final_id);
		
		$this->mssql->where("product_id",$final['final_spring_id']);
		$this->mssql->limit(1);
		$row = $this->mssql->get("product_spring")->row_array(); 
		return $row; 
	}
		public function get_final_spring($income_id,$final_id)
	{
		$this->mssql->where("final_spring_income_id",$income_id);
		$this->mssql->where("final_id",$final_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("final_bolt")->row_array(); 
		return $row; 
	}
	public function get_spring_product($income_id,$final_id)
	{
		$final = $this->get_final_spring($income_id,$final_id);
		
		$this->mssql->where("product_id",$final['final_spring_id']);
		$this->mssql->limit(1);
		$row = $this->mssql->get("product_spring")->row_array(); 
		return $row; 
	}
	
	public function get_final_spring_test_info($final_id,$std_id)
	{
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("spring_standard_id",$std_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("final_spring_testing")->row_array(); 
		return $row;	
		
		
	}

	public function get_final_spring_size_test_info($final_id,$std_id)
	{
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("spring_cer_size_id",$std_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("final_size_spring_testing")->row_array(); 
		return $row;	
		
		
	}

	public function get_product_standard_spring($product_id)
	{
		/* $this->mssql->where('(ps_min <> 0 OR ps_max <> 0)'); */
		$this->mssql->where("product_id",$product_id);
		$row = $this->mssql->get("product_standard_spring"); 
		return $row;
	}
	
	public function get_product_standard_size_spring($product_id,$size_id)
	{
		
		/* $this->mssql->where('(ps_min <> NULL OR ps_max <> NULL)'); */
		$this->mssql->where("product_id",$product_id);
		$this->mssql->where("size_id",$size_id);
		$row = $this->mssql->get("product_cer_size_spring"); 
		return $row;
	}
	
	public function add_spring_testing_cer($data)
	{
		/*
var_dump($_POST);
		exit();
*/		
		if($_POST['spring_standard_id'] != NULL){
			foreach($_POST['spring_standard_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('spring_standard_id',$_POST['spring_standard_id'][$key]);
				$this->mssql->set('spring_testing1',$_POST['test1'][$key]);
				$this->mssql->set('spring_testing2',$_POST['test2'][$key]);
				$this->mssql->set('spring_testing3',$_POST['test3'][$key]);
				$this->mssql->set('spring_testing4',$_POST['test4'][$key]);
				$this->mssql->set('spring_testing5',$_POST['test5'][$key]);
				$this->mssql->set('spring_testing_status',$_POST['testing_status'][$key]);
				$this->mssql->set('spring_testing_remark',$_POST['remark'][$key]);
				$this->mssql->set('spring_testing_post_user_id',$data['spring_testing_post_user_id']);	
				$this->mssql->set('spring_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('cer_spring_testing');
			}
		}
		
		if($_POST['spring_cer_size_id'] != NULL){
			foreach($_POST['spring_cer_size_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('income_id',$_POST['income_id']);
				$this->mssql->set('spring_cer_size_id',$_POST['spring_cer_size_id'][$key]);
				$this->mssql->set('spring_testing1',$_POST['ctest1'][$key]);
				$this->mssql->set('spring_testing2',$_POST['ctest2'][$key]);
				$this->mssql->set('spring_testing3',$_POST['ctest3'][$key]);
				$this->mssql->set('spring_testing4',$_POST['ctest4'][$key]);
				$this->mssql->set('spring_testing5',$_POST['ctest5'][$key]);
				$this->mssql->set('spring_testing_status',$_POST['ctesting_status'][$key]);
				$this->mssql->set('spring_testing_remark',$_POST['cremark'][$key]);
				$this->mssql->set('spring_testing_post_user_id',$data['spring_testing_post_user_id']);	
				$this->mssql->set('spring_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('cer_size_spring_testing');
			}
		}
		
		
	}
	
	public function update_spring_testing_final($data)
	{
		foreach($_POST['spring_standard_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('final_status',"tested");
				$this->mssql->set('spring_testing1',$_POST['test1'][$key]);
				$this->mssql->set('spring_testing2',$_POST['test2'][$key]);
				$this->mssql->set('spring_testing3',$_POST['test3'][$key]);
				$this->mssql->set('spring_testing4',$_POST['test4'][$key]);
				$this->mssql->set('spring_testing5',$_POST['test5'][$key]);
				$this->mssql->set('spring_testing_status',$_POST['testing_status'][$key]);
				$this->mssql->set('spring_testing_remark',$_POST['remark'][$key]);
				$this->mssql->set('spring_testing_post_user_id',$data['spring_testing_post_user_id']);	
				$this->mssql->set('spring_testing_post_date',"NOW()",FALSE);
				$this->mssql->where("spring_testing_id",$_POST['spring_testing_id'][$key]);	
				$this->mssql->update('final_spring_testing');
			}
			
		foreach($_POST['spring_cer_size_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('spring_testing1',$_POST['ctest1'][$key]);
				$this->mssql->set('spring_testing2',$_POST['ctest2'][$key]);
				$this->mssql->set('spring_testing3',$_POST['ctest3'][$key]);
				$this->mssql->set('spring_testing4',$_POST['ctest4'][$key]);
				$this->mssql->set('spring_testing5',$_POST['ctest5'][$key]);
				$this->mssql->set('spring_testing_status',$_POST['ctesting_status'][$key]);
				$this->mssql->set('spring_testing_remark',$_POST['cremark'][$key]);
				$this->mssql->set('spring_testing_post_user_id',$data['spring_testing_post_user_id']);	
				$this->mssql->set('spring_testing_post_date',"NOW()",FALSE);
				$this->mssql->where("spring_testing_id",$_POST['spring_testing_final_id'][$key]);	
				$this->mssql->update('final_size_spring_testing');
			}
		
	}
	
	public function add_spring_testing_final($data)
	{
		foreach(@$_POST['spring_standard_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('income_id',@$_POST['income_id']);
				$this->mssql->set('spring_standard_id',@$_POST['spring_standard_id'][$key]);
				$this->mssql->set('spring_testing1',@$_POST['test1'][$key]);
				$this->mssql->set('spring_testing2',@$_POST['test2'][$key]);
				$this->mssql->set('spring_testing3',@$_POST['test3'][$key]);
				$this->mssql->set('spring_testing4',@$_POST['test4'][$key]);
				$this->mssql->set('spring_testing5',@$_POST['test5'][$key]);
				$this->mssql->set('spring_testing_status',@$_POST['testing_status'][$key]);
				$this->mssql->set('spring_testing_remark',@$_POST['remark'][$key]);
				$this->mssql->set('spring_testing_post_user_id',$data['spring_testing_post_user_id']);	
				$this->mssql->set('spring_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('final_spring_testing');
			}
			
		if(@$_POST['spring_cer_size_id'] != NULL){
			foreach(@$_POST['spring_cer_size_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('income_id',@$_POST['income_id']);
				$this->mssql->set('spring_cer_size_id',@$_POST['spring_cer_size_id'][$key]);
				$this->mssql->set('spring_testing1',@$_POST['ctest1'][$key]);
				$this->mssql->set('spring_testing2',@$_POST['ctest2'][$key]);
				$this->mssql->set('spring_testing3',@$_POST['ctest3'][$key]);
				$this->mssql->set('spring_testing4',@$_POST['ctest4'][$key]);
				$this->mssql->set('spring_testing5',@$_POST['ctest5'][$key]);
				$this->mssql->set('spring_testing_status',@$_POST['ctesting_status'][$key]);
				$this->mssql->set('spring_testing_remark',@$_POST['cremark'][$key]);
				$this->mssql->set('spring_testing_post_user_id',$data['spring_testing_post_user_id']);	
				$this->mssql->set('spring_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('final_size_spring_testing');
			}
		}
		
	}
		
	public function check_final_spring_tested($income_id,$final_id)
	{
				$this->mssql->where("final_id",$final_id);
			   $this->mssql->where("income_id",$income_id);
		return $this->mssql->count_all_results("final_spring_testing");
	}
	public function get_final_spring_tested($income_id,$std_id)
	{
		$this->mssql->where("spring_standard_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("final_spring_testing")->row_array();
	}
	
	public function get_final_spring_cer_size_tested($income_id,$std_id){
		$this->mssql->where("spring_cer_size_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("final_size_spring_testing")->row_array();

		
	}
	
	public function get_final_spring_tested_main($final_id,$income_id,$std_id)
	{
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("spring_standard_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("final_spring_testing")->row_array();
	}
	
	public function get_final_spring_cer_size_tested_main($final_id,$income_id,$std_id){
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("spring_cer_size_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("final_size_spring_testing")->row_array();

		
	}
	
	public function check_final_tested_spring($income_id,$final_id)
	{
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->count_all_results("final_spring_testing");
	}

	public function get_chemical_final_spring($income_id,$final_id)
	{
		$this->mssql->select("chemical_final_spring.*,product_chemical_spring.ps_min,product_chemical_spring.ps_max");
		$this->mssql->join("product_chemical_spring","product_chemical_spring.chemical_id=chemical_final_spring.chemical_id");
		$this->mssql->where("chemical_final_spring.income_id",$income_id);
		$this->mssql->where("chemical_final_spring.final_id",$final_id);
		$this->mssql->ORDER_by("chemical_id", "ASC");
		return $this->mssql->get("chemical_final_spring")->result_array();	
	}
	
	##################### end test spring ##############################
	
	##################### start test taper ############################
	public function get_final_taper_main($income_id,$final_id)
	{
		$this->mssql->where("final_taper_income_id",$income_id);
		$this->mssql->where("final_id",$final_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("final_taper")->row_array(); 
		return $row; 
	}
	public function get_taper_product_main($income_id,$final_id)
	{
		$final = $this->get_final_taper_main($income_id,$final_id);
		
		$this->mssql->where("product_id",$final['final_taper_id']);
		$this->mssql->limit(1);
		$row = $this->mssql->get("product_taper")->row_array(); 
		return $row; 
	}
		public function get_final_taper($income_id,$final_id)
	{
		$this->mssql->where("final_taper_income_id",$income_id);
		$this->mssql->where("final_id",$final_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("final_bolt")->row_array(); 
		return $row; 
	}
	public function get_taper_product($income_id,$final_id)
	{
		$final = $this->get_final_taper($income_id,$final_id);
		
		$this->mssql->where("product_id",$final['final_taper_id']);
		$this->mssql->limit(1);
		$row = $this->mssql->get("product_taper")->row_array(); 
		return $row; 
	}
	
	public function get_final_taper_test_info($final_id,$std_id)
	{
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("taper_standard_id",$std_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("final_taper_testing")->row_array(); 
		return $row;	
		
		
	}

	public function get_final_taper_size_test_info($final_id,$std_id)
	{
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("taper_cer_size_id",$std_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("final_size_taper_testing")->row_array(); 
		return $row;	
		
		
	}

	public function get_product_standard_taper($product_id)
	{
		/* $this->mssql->where('(ps_min <> 0 OR ps_max <> 0)'); */
		$this->mssql->where("product_id",$product_id);
		$row = $this->mssql->get("product_standard_taper"); 
		return $row;
	}
	
	public function get_product_standard_size_taper($product_id,$size_id)
	{
		
		/* $this->mssql->where('(ps_min <> NULL OR ps_max <> NULL)'); */
		$this->mssql->where("product_id",$product_id);
		$this->mssql->where("size_id",$size_id);
		$row = $this->mssql->get("product_cer_size_taper"); 
		return $row;
	}
	
	public function add_taper_testing_cer($data)
	{
		/*
var_dump($_POST);
		exit();
*/		
		if($_POST['taper_standard_id'] != NULL){
			foreach($_POST['taper_standard_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('taper_standard_id',$_POST['taper_standard_id'][$key]);
				$this->mssql->set('taper_testing1',$_POST['test1'][$key]);
				$this->mssql->set('taper_testing2',$_POST['test2'][$key]);
				$this->mssql->set('taper_testing3',$_POST['test3'][$key]);
				$this->mssql->set('taper_testing4',$_POST['test4'][$key]);
				$this->mssql->set('taper_testing5',$_POST['test5'][$key]);
				$this->mssql->set('taper_testing_status',$_POST['testing_status'][$key]);
				$this->mssql->set('taper_testing_remark',$_POST['remark'][$key]);
				$this->mssql->set('taper_testing_post_user_id',$data['taper_testing_post_user_id']);	
				$this->mssql->set('taper_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('cer_taper_testing');
			}
		}
		
		if($_POST['taper_cer_size_id'] != NULL){
			foreach($_POST['taper_cer_size_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('income_id',$_POST['income_id']);
				$this->mssql->set('taper_cer_size_id',$_POST['taper_cer_size_id'][$key]);
				$this->mssql->set('taper_testing1',$_POST['ctest1'][$key]);
				$this->mssql->set('taper_testing2',$_POST['ctest2'][$key]);
				$this->mssql->set('taper_testing3',$_POST['ctest3'][$key]);
				$this->mssql->set('taper_testing4',$_POST['ctest4'][$key]);
				$this->mssql->set('taper_testing5',$_POST['ctest5'][$key]);
				$this->mssql->set('taper_testing_status',$_POST['ctesting_status'][$key]);
				$this->mssql->set('taper_testing_remark',$_POST['cremark'][$key]);
				$this->mssql->set('taper_testing_post_user_id',$data['taper_testing_post_user_id']);	
				$this->mssql->set('taper_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('cer_size_taper_testing');
			}
		}
		
		
	}
	
	public function update_taper_testing_final($data)
	{
		foreach($_POST['taper_standard_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('final_status',"tested");
				$this->mssql->set('taper_testing1',$_POST['test1'][$key]);
				$this->mssql->set('taper_testing2',$_POST['test2'][$key]);
				$this->mssql->set('taper_testing3',$_POST['test3'][$key]);
				$this->mssql->set('taper_testing4',$_POST['test4'][$key]);
				$this->mssql->set('taper_testing5',$_POST['test5'][$key]);
				$this->mssql->set('taper_testing_status',$_POST['testing_status'][$key]);
				$this->mssql->set('taper_testing_remark',$_POST['remark'][$key]);
				$this->mssql->set('taper_testing_post_user_id',$data['taper_testing_post_user_id']);	
				$this->mssql->set('taper_testing_post_date',"NOW()",FALSE);
				$this->mssql->where("taper_testing_id",$_POST['taper_testing_id'][$key]);	
				$this->mssql->update('final_taper_testing');
			}
			
		foreach($_POST['taper_cer_size_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('taper_testing1',$_POST['ctest1'][$key]);
				$this->mssql->set('taper_testing2',$_POST['ctest2'][$key]);
				$this->mssql->set('taper_testing3',$_POST['ctest3'][$key]);
				$this->mssql->set('taper_testing4',$_POST['ctest4'][$key]);
				$this->mssql->set('taper_testing5',$_POST['ctest5'][$key]);
				$this->mssql->set('taper_testing_status',$_POST['ctesting_status'][$key]);
				$this->mssql->set('taper_testing_remark',$_POST['cremark'][$key]);
				$this->mssql->set('taper_testing_post_user_id',$data['taper_testing_post_user_id']);	
				$this->mssql->set('taper_testing_post_date',"NOW()",FALSE);
				$this->mssql->where("taper_testing_id",$_POST['taper_testing_final_id'][$key]);	
				$this->mssql->update('final_size_taper_testing');
			}
		
	}
	
	public function add_taper_testing_final($data)
	{
		foreach(@$_POST['taper_standard_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('income_id',@$_POST['income_id']);
				$this->mssql->set('taper_standard_id',@$_POST['taper_standard_id'][$key]);
				$this->mssql->set('taper_testing1',@$_POST['test1'][$key]);
				$this->mssql->set('taper_testing2',@$_POST['test2'][$key]);
				$this->mssql->set('taper_testing3',@$_POST['test3'][$key]);
				$this->mssql->set('taper_testing4',@$_POST['test4'][$key]);
				$this->mssql->set('taper_testing5',@$_POST['test5'][$key]);
				$this->mssql->set('taper_testing_status',@$_POST['testing_status'][$key]);
				$this->mssql->set('taper_testing_remark',@$_POST['remark'][$key]);
				$this->mssql->set('taper_testing_post_user_id',$data['taper_testing_post_user_id']);	
				$this->mssql->set('taper_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('final_taper_testing');
			}
			
		if(@$_POST['taper_cer_size_id'] != NULL){
			foreach(@$_POST['taper_cer_size_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('income_id',@$_POST['income_id']);
				$this->mssql->set('taper_cer_size_id',@$_POST['taper_cer_size_id'][$key]);
				$this->mssql->set('taper_testing1',@$_POST['ctest1'][$key]);
				$this->mssql->set('taper_testing2',@$_POST['ctest2'][$key]);
				$this->mssql->set('taper_testing3',@$_POST['ctest3'][$key]);
				$this->mssql->set('taper_testing4',@$_POST['ctest4'][$key]);
				$this->mssql->set('taper_testing5',@$_POST['ctest5'][$key]);
				$this->mssql->set('taper_testing_status',@$_POST['ctesting_status'][$key]);
				$this->mssql->set('taper_testing_remark',@$_POST['cremark'][$key]);
				$this->mssql->set('taper_testing_post_user_id',$data['taper_testing_post_user_id']);	
				$this->mssql->set('taper_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('final_size_taper_testing');
			}
		}
		
	}
		
	public function check_final_taper_tested($income_id,$final_id)
	{
				$this->mssql->where("final_id",$final_id);
			   $this->mssql->where("income_id",$income_id);
		return $this->mssql->count_all_results("final_taper_testing");
	}
	public function get_final_taper_tested($income_id,$std_id)
	{
		$this->mssql->where("taper_standard_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("final_taper_testing")->row_array();
	}
	
	public function get_final_taper_cer_size_tested($income_id,$std_id){
		$this->mssql->where("taper_cer_size_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("final_size_taper_testing")->row_array();

		
	}
	
	public function get_final_taper_tested_main($final_id,$income_id,$std_id)
	{
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("taper_standard_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("final_taper_testing")->row_array();
	}
	
	public function get_final_taper_cer_size_tested_main($final_id,$income_id,$std_id){
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("taper_cer_size_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("final_size_taper_testing")->row_array();

		
	}
	
	public function check_final_tested_taper($income_id,$final_id)
	{
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->count_all_results("final_taper_testing");
	}

	public function get_chemical_final_taper($income_id,$final_id)
	{
		$this->mssql->select("chemical_final_taper.*,product_chemical_taper.ps_min,product_chemical_taper.ps_max");
		$this->mssql->join("product_chemical_taper","product_chemical_taper.chemical_id=chemical_final_taper.chemical_id");
		$this->mssql->where("chemical_final_taper.income_id",$income_id);
		$this->mssql->where("chemical_final_taper.final_id",$final_id);
		$this->mssql->ORDER_by("chemical_id", "ASC");
		return $this->mssql->get("chemical_final_taper")->result_array();	
	}
	
	##################### end test taper ##############################
	
	##################### start test stud ############################
	public function get_final_stud_main($income_id,$final_id)
	{
		$this->mssql->where("final_stud_income_id",$income_id);
		$this->mssql->where("final_id",$final_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("final_stud")->row_array(); 
		return $row; 
	}
	public function get_stud_product_main($income_id,$final_id)
	{
		$final = $this->get_final_stud_main($income_id,$final_id);
		
		$this->mssql->where("product_id",$final['final_stud_id']);
		$this->mssql->limit(1);
		$row = $this->mssql->get("product_stud")->row_array(); 
		return $row; 
	}
		public function get_final_stud($income_id,$final_id)
	{
		$this->mssql->where("final_stud_income_id",$income_id);
		$this->mssql->where("final_id",$final_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("final_bolt")->row_array(); 
		return $row; 
	}
	public function get_stud_product($income_id,$final_id)
	{
		$final = $this->get_final_stud($income_id,$final_id);
		
		$this->mssql->where("product_id",$final['final_stud_id']);
		$this->mssql->limit(1);
		$row = $this->mssql->get("product_stud")->row_array(); 
		return $row; 
	}
	
	public function get_final_stud_test_info($final_id,$std_id)
	{
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("stud_standard_id",$std_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("final_stud_testing")->row_array(); 
		return $row;	
		
		
	}

	public function get_final_stud_size_test_info($final_id,$std_id)
	{
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("stud_cer_size_id",$std_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("final_size_stud_testing")->row_array(); 
		return $row;	
		
		
	}

	public function get_product_standard_stud($product_id)
	{
		/* $this->mssql->where('(ps_min <> 0 OR ps_max <> 0)'); */
		$this->mssql->where("product_id",$product_id);
		$row = $this->mssql->get("product_standard_stud"); 
		return $row;
	}
	
	public function get_product_standard_size_stud($product_id,$size_id)
	{
		
		/* $this->mssql->where('(ps_min <> NULL OR ps_max <> NULL)'); */
		$this->mssql->where("product_id",$product_id);
		$this->mssql->where("size_id",$size_id);
		$row = $this->mssql->get("product_cer_size_stud"); 
		return $row;
	}
	
	public function add_stud_testing_cer($data)
	{
		/*
var_dump($_POST);
		exit();
*/		
		if($_POST['stud_standard_id'] != NULL){
			foreach($_POST['stud_standard_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('stud_standard_id',$_POST['stud_standard_id'][$key]);
				$this->mssql->set('stud_testing1',$_POST['test1'][$key]);
				$this->mssql->set('stud_testing2',$_POST['test2'][$key]);
				$this->mssql->set('stud_testing3',$_POST['test3'][$key]);
				$this->mssql->set('stud_testing4',$_POST['test4'][$key]);
				$this->mssql->set('stud_testing5',$_POST['test5'][$key]);
				$this->mssql->set('stud_testing_status',$_POST['testing_status'][$key]);
				$this->mssql->set('stud_testing_remark',$_POST['remark'][$key]);
				$this->mssql->set('stud_testing_post_user_id',$data['stud_testing_post_user_id']);	
				$this->mssql->set('stud_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('cer_stud_testing');
			}
		}
		
		if($_POST['stud_cer_size_id'] != NULL){
			foreach($_POST['stud_cer_size_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('income_id',$_POST['income_id']);
				$this->mssql->set('stud_cer_size_id',$_POST['stud_cer_size_id'][$key]);
				$this->mssql->set('stud_testing1',$_POST['ctest1'][$key]);
				$this->mssql->set('stud_testing2',$_POST['ctest2'][$key]);
				$this->mssql->set('stud_testing3',$_POST['ctest3'][$key]);
				$this->mssql->set('stud_testing4',$_POST['ctest4'][$key]);
				$this->mssql->set('stud_testing5',$_POST['ctest5'][$key]);
				$this->mssql->set('stud_testing_status',$_POST['ctesting_status'][$key]);
				$this->mssql->set('stud_testing_remark',$_POST['cremark'][$key]);
				$this->mssql->set('stud_testing_post_user_id',$data['stud_testing_post_user_id']);	
				$this->mssql->set('stud_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('cer_size_stud_testing');
			}
		}
		
		
	}
	
	public function update_stud_testing_final($data)
	{
		foreach($_POST['stud_standard_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('final_status',"tested");
				$this->mssql->set('stud_testing1',$_POST['test1'][$key]);
				$this->mssql->set('stud_testing2',$_POST['test2'][$key]);
				$this->mssql->set('stud_testing3',$_POST['test3'][$key]);
				$this->mssql->set('stud_testing4',$_POST['test4'][$key]);
				$this->mssql->set('stud_testing5',$_POST['test5'][$key]);
				$this->mssql->set('stud_testing_status',$_POST['testing_status'][$key]);
				$this->mssql->set('stud_testing_remark',$_POST['remark'][$key]);
				$this->mssql->set('stud_testing_post_user_id',$data['stud_testing_post_user_id']);	
				$this->mssql->set('stud_testing_post_date',"NOW()",FALSE);
				$this->mssql->where("stud_testing_id",$_POST['stud_testing_id'][$key]);	
				$this->mssql->update('final_stud_testing');
			}
			
		foreach($_POST['stud_cer_size_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('stud_testing1',$_POST['ctest1'][$key]);
				$this->mssql->set('stud_testing2',$_POST['ctest2'][$key]);
				$this->mssql->set('stud_testing3',$_POST['ctest3'][$key]);
				$this->mssql->set('stud_testing4',$_POST['ctest4'][$key]);
				$this->mssql->set('stud_testing5',$_POST['ctest5'][$key]);
				$this->mssql->set('stud_testing_status',$_POST['ctesting_status'][$key]);
				$this->mssql->set('stud_testing_remark',$_POST['cremark'][$key]);
				$this->mssql->set('stud_testing_post_user_id',$data['stud_testing_post_user_id']);	
				$this->mssql->set('stud_testing_post_date',"NOW()",FALSE);
				$this->mssql->where("stud_testing_id",$_POST['stud_testing_final_id'][$key]);	
				$this->mssql->update('final_size_stud_testing');
			}
		
	}
	
	public function add_stud_testing_final($data)
	{
		foreach(@$_POST['stud_standard_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('income_id',@$_POST['income_id']);
				$this->mssql->set('stud_standard_id',@$_POST['stud_standard_id'][$key]);
				$this->mssql->set('stud_testing1',@$_POST['test1'][$key]);
				$this->mssql->set('stud_testing2',@$_POST['test2'][$key]);
				$this->mssql->set('stud_testing3',@$_POST['test3'][$key]);
				$this->mssql->set('stud_testing4',@$_POST['test4'][$key]);
				$this->mssql->set('stud_testing5',@$_POST['test5'][$key]);
				$this->mssql->set('stud_testing_status',@$_POST['testing_status'][$key]);
				$this->mssql->set('stud_testing_remark',@$_POST['remark'][$key]);
				$this->mssql->set('stud_testing_post_user_id',$data['stud_testing_post_user_id']);	
				$this->mssql->set('stud_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('final_stud_testing');
			}
			
		if(@$_POST['stud_cer_size_id'] != NULL){
			foreach(@$_POST['stud_cer_size_id'] as $key=>$value){
				$this->mssql->set('final_id',$data['final_id']);
				$this->mssql->set('income_id',@$_POST['income_id']);
				$this->mssql->set('stud_cer_size_id',@$_POST['stud_cer_size_id'][$key]);
				$this->mssql->set('stud_testing1',@$_POST['ctest1'][$key]);
				$this->mssql->set('stud_testing2',@$_POST['ctest2'][$key]);
				$this->mssql->set('stud_testing3',@$_POST['ctest3'][$key]);
				$this->mssql->set('stud_testing4',@$_POST['ctest4'][$key]);
				$this->mssql->set('stud_testing5',@$_POST['ctest5'][$key]);
				$this->mssql->set('stud_testing_status',@$_POST['ctesting_status'][$key]);
				$this->mssql->set('stud_testing_remark',@$_POST['cremark'][$key]);
				$this->mssql->set('stud_testing_post_user_id',$data['stud_testing_post_user_id']);	
				$this->mssql->set('stud_testing_post_date',"NOW()",FALSE);		
				$this->mssql->insert('final_size_stud_testing');
			}
		}
		
	}
		
	public function check_final_stud_tested($income_id,$final_id)
	{
				$this->mssql->where("final_id",$final_id);
			   $this->mssql->where("income_id",$income_id);
		return $this->mssql->count_all_results("final_stud_testing");
	}
	public function get_final_stud_tested($income_id,$std_id)
	{
		$this->mssql->where("stud_standard_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("final_stud_testing")->row_array();
	}
	
	public function get_final_stud_cer_size_tested($income_id,$std_id){
		$this->mssql->where("stud_cer_size_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("final_size_stud_testing")->row_array();

		
	}
	
	public function get_final_stud_tested_main($final_id,$income_id,$std_id)
	{
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("stud_standard_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("final_stud_testing")->row_array();
	}
	
	public function get_final_stud_cer_size_tested_main($final_id,$income_id,$std_id){
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("stud_cer_size_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("final_size_stud_testing")->row_array();

		
	}
	
	public function check_final_tested_stud($income_id,$final_id)
	{
		$this->mssql->where("final_id",$final_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->count_all_results("final_stud_testing");
	}

	public function get_chemical_final_stud($income_id,$final_id)
	{
		$this->mssql->select("chemical_final_stud.*,product_chemical_stud.ps_min,product_chemical_stud.ps_max");
		$this->mssql->join("product_chemical_stud","product_chemical_stud.chemical_id=chemical_final_stud.chemical_id");
		$this->mssql->where("chemical_final_stud.income_id",$income_id);
		$this->mssql->where("chemical_final_stud.final_id",$final_id);
		$this->mssql->ORDER_by("chemical_id", "ASC");
		return $this->mssql->get("chemical_final_stud")->result_array();	
	}
	
	##################### end test stud ##############################
	

}