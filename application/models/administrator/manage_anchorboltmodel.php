<?php
class Manage_anchorboltmodel extends CI_Model{
	function __construct(){
		parent::__construct();
		
		$this->mssql = $this->load->database("mssql", true);
	}
	
	public function dataTable($type='axel', $limit=0, $offset=0){
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("product_grade",$q);
		}
		$query = $this->mssql->where('product_status !=','discard')
								->where('product_type', $type)
								->order_by('product_createdtime','DESC')
								->limit($limit,$offset)
								->get('product_anchor')->result_array();
								
		return $query;
	}
	
	public function get_count($type)
	{
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("product_grade",$q);
		}
		$query = $this->mssql->where('product_status !=','discard')
					->where('product_type', $type)
					->order_by('product_createdtime','desc')
					->get('product_anchor')->num_rows();
		return $query;
	}
	
	public function getinfo($productid=0){
		$query = $this->mssql->where('product_id', $productid)
								->get('product_anchor')->row_array();
		return $query;
	}
	
	public function add(){
		$this->load->model("mobile_upload");
		$this->mobile_upload->set_upload_path("public/uploads/product/anchor_bolt");
		
		if($_FILES['product_thumbnail']['tmp_name']!=''){
			if($this->mobile_upload->upload("product_thumbnail")===true){
				$res_data = $this->mobile_upload->data();
				$filename = $res_data['file_name'];
			}else{
				$filename = '';
			}
		}else{
			$filename = '';
		}
		
		$aData = array(
			'product_grade' => $this->input->post('product_grade'),
			'product_thumbnail' => $filename,
			'product_type' => $this->input->post('product_type'),
			'product_low_carbon' => $this->input->post('product_low_carbon'),
			'product_status' => 'approved',
			'product_createdtime' => date("Y-m-d H:i:s"),
			'product_createdip' => $this->input->ip_address()
		);
		
		$this->mssql->insert('product_anchor', $aData);
	}
	
	public function edit($productid=0){
		$info = $this->getinfo($productid);
		
		$this->load->model("mobile_upload");
		$this->mobile_upload->set_upload_path("public/uploads/product/anchor_bolt");
		if($_FILES['product_thumbnail']['tmp_name']!=''){
			if($this->mobile_upload->upload("product_thumbnail")===true){
				unlink('public/uploads/product/anchor_bolt/'.$info['product_thumbnail']);
				$res_data = $this->mobile_upload->data();
				$filename = $res_data['file_name'];
			}else{
				return false;
				$filename = $info['product_thumbnail'];
			}
		}else{
			$filename = $info['product_thumbnail'];
		}
		
		$aData = array(
			'product_grade' => $this->input->post('product_grade'),
			'product_thumbnail' => $filename,
			'product_type' => $this->input->post('product_type'),
			'product_low_carbon' => $this->input->post('product_low_carbon'),
			'product_status' => 'approved',
			'product_updatedtime' => date("Y-m-d H:i:s"),
			'product_updatedip' => $this->input->ip_address()
		);
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->update('product_anchor', $aData);
		return true;
	}
	
	public function getdelete($productid=0){
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_anchor');
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_size_anchor');
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_standard_anchor');
	}
	
	public function size_dataTable($productid=0, $type='axel', $limit=0, $offset=0){
		$q = $this->input->get("q");
		if(trim($q)){
			if($type == "axel"){
			$this->mssql->where("(size_m like '%{$q}%' or size_length like '%{$q}%')",false,false);
			}else{
			$this->mssql->where("(size_t like '%{$q}%' or size_w like '%{$q}%' or size_length like '%{$q}%')",false,false);	
			}
			
		}
		$query = $this->mssql->where('product_id', $productid)
								->where('size_status !=','discard')
								->order_by('size_createdtime','desc')
								->limit($limit, $offset)
								->get('product_size_anchor')->result_array();
								echo $this->mssql->last_query();
		return $query;
	}
	
	public function get_size_count($productid=0, $type='axel'){
		$q = $this->input->get("q");
		if(trim($q)){
			if($type == "axel"){
			$this->mssql->where("(size_m like '%{$q}%' or size_length like '%{$q}%')",false,false);
			}else{
			$this->mssql->where("(size_t like '%{$q}%' or size_w like '%{$q}%' or size_length like '%{$q}%')",false,false);	
			}
			
		}
		$query = $this->mssql->where('product_id', $productid)
					->where('size_status !=','discard')
					->order_by('size_createdtime','desc')
					->get('product_size_anchor')->num_rows();
		return $query;
	}
	
	public function get_sizeinfo_byid($sizeid=0){
		$query = $this->mssql->where('size_id', $sizeid)
								->get('product_size_anchor')->row_array();
		return $query;
	}
	
	public function addsize(){
		$aData = array(
			'product_id' => $this->input->post('product_id'),
			'size_m' => $this->input->post('size_m'),
			'size_length' => $this->input->post('size_length'),
			'size_status' => 'approved',
			'size_createdtime' => date("Y-m-d H:i:s"),
			'size_createdip' => $this->input->ip_address()	
		);
		
		$this->mssql->insert('product_size_anchor', $aData);
		return $this->mssql->insert_id();
	}
	
	public function editsize($sizeid=0){
		$aData = array(
			'size_m' => $this->input->post('size_m'),
			'size_length' => $this->input->post('size_length'),
			'size_status' => 'approved',
			'size_updatedtime' => date("Y-m-d H:i:s"),
			'size_updatedip' => $this->input->ip_address()	
		);
		
		$this->mssql->where('size_id', $sizeid);
		$this->mssql->update('product_size_anchor', $aData);
	}
	
	public function addsize_sheet(){
		$aData = array(
			'product_id' => $this->input->post('product_id'),
			'size_t' => $this->input->post('size_t'),
			'size_w' => $this->input->post('size_w'),
			'size_length' => $this->input->post('size_length'),
			'size_status' => 'approved',
			'size_createdtime' => date("Y-m-d H:i:s"),
			'size_createdip' => $this->input->ip_address()	
		);
		
		$this->mssql->insert('product_size_anchor', $aData);
		return $this->mssql->insert_id();
	}
	
	public function editsize_sheet($sizeid=0){
		$aData = array(
			'size_t' => $this->input->post('size_t'),
			'size_w' => $this->input->post('size_w'),
			'size_length' => $this->input->post('size_length'),
			'size_status' => 'approved',
			'size_updatedtime' => date("Y-m-d H:i:s"),
			'size_updatedip' => $this->input->ip_address()	
		);
		
		$this->mssql->where('size_id', $sizeid);
		$this->mssql->update('product_size_anchor', $aData);
	}
	
	public function deletesize($sizeid=0){
		$this->mssql->where('size_id', $productid);
		$this->mssql->delete('product_size_anchor');
	}
	
	public function get_standardinfo_byid($productid=0){
		$query = $this->mssql->where('product_id', $productid)
								->limit(1)
								->get('product_standard_anchor')->row_array();
		return $query;
	}
	
	public function update_standard(){
		
		if($this->input->post('update_status')=='insert'){
			for($i=1; $i<=3; $i++){
				$aData = array(
					'product_id' => $this->input->post('product_id'),
					'ps_min' => $this->input->post('ps_min'),
					'ps_min_unit' => $this->input->post('ps_min_unit'),
					'ps_max' => $this->input->post('ps_max'),
					'ps_max_unit' => $this->input->post('ps_max_unit'),
					'ps_status' => 'approved',
					'standard_description' => $this->input->post('standard_description'),
					'ps_createdtime' => date("Y-m-d H:i:s"),
					'ps_createdip' => $this->input->ip_address
				);
				
				$this->mssql->insert('product_standard_anchor', $aData);
			}
		}else{
			$aData = array(
				'ps_min' => $this->input->post('ps_min'),
				'ps_min_unit' => $this->input->post('ps_min_unit'),
				'ps_max' => $this->input->post('ps_max'),
				'ps_max_unit' => $this->input->post('ps_max_unit'),
				'ps_status' => 'approved',
				'standard_description' => $this->input->post('standard_description'),
				'ps_updatedtime' => date("Y-m-d H:i:s"),
				'ps_updatedip' => $this->input->ip_address
			);
			
			$this->mssql->where('product_id', $this->input->post('product_id'));
			//$this->mssql->where('ps_id', $this->input->post('ps_id'));
			$this->mssql->update('product_standard_anchor', $aData);
			
		}
	}
	
	public function size_detail($sizeid=0){
		$this->mssql->where('size_id', $sizeid);
		return $this->mssql->get('product_size_anchor')->row_array();
	}
	
	public function stock_edit($sizeid=0){
		$old = $this->input->post('qty_old');
		$new = $this->input->post('qty');
		
		$total =  $old+$new ;
		
		$this->mssql->set("size_qty",$total);
		
		$this->mssql->where('size_id', $sizeid);
		$this->mssql->update('product_size_anchor');
		
		$bData = array(
			'qty' => $this->input->post('qty'),
			'qty_old' => $this->input->post('qty_old'),
			'remark' => $this->input->post('remark'),
			'size_id' => $sizeid,
			'createdate' => date("Y-m-d H:i:s")
		);
		
		$this->mssql->insert('log_stock_anchor', $bData);
	}
	
	public function stock_edit_left($sizeid=0){
		$old = $this->input->post('qty_old');
		$new = $this->input->post('qty');
		
		$total =  $old+$new ;
		
		$this->mssql->set("size_left_qty",$total);
		
		$this->mssql->where('size_id', $sizeid);
		$this->mssql->update('product_size_anchor');
		
		$bData = array(
			'qty' => $this->input->post('qty'),
			'qty_old' => $this->input->post('qty_old'),
			'remark' => $this->input->post('remark'),
			'size_id' => $sizeid,
			'createdate' => date("Y-m-d H:i:s")
		);
		
		$this->mssql->insert('log_stock_anchor_left', $bData);
	}
	
	public function get_anchor_history_dataTable($sizeid=0, $limit=100, $offset=0){
		$this->mssql->where("size_id",$sizeid);
		$this->mssql->limit($limit,$offset);
		$this->mssql->order_by("log_id","DESC");
		$res = $this->mssql->get("log_stock_anchor")->result_array();
		return $res;
	}
	
	public function get_anchor_left_history_dataTable($sizeid=0, $limit=100, $offset=0){
		$this->mssql->where("size_id",$sizeid);
		$this->mssql->limit($limit,$offset);
		$this->mssql->order_by("log_id","DESC");
		$res = $this->mssql->get("log_stock_anchor_left")->result_array();
		return $res;
	}
	
	public function get_anchor_incoming_dataTable($size_id=0, $pro_id=0){
		$this->mssql->where('test_status',2);
		$this->mssql->where('size_id', $size_id);
		$this->mssql->where('income_product_id', $pro_id);
		$this->mssql->where('test_status', 2);
		$this->mssql->order_by('income_id', 'ASC');
		$res = $this->mssql->get('income_anchor')->result_array();
		return $res;
	}
	
	public function get_supplierinfo_byid($supplier_id=0){
		$this->mssql->where('supplier_id', $supplier_id);
		$res = $this->mssql->get('supplier')->row_array();
		return $res;
	}
	
	public function get_doclist($size_id=0, $pro_id=0){
		$this->mssql->where('size_id', $size_id);
		return $this->mssql->get('doc_anchor_full')->result_array();
	}
	
	public function getdoc_info($doc_id=0){
		$this->mssql->where('doc_id', $doc_id);
		return $this->mssql->get('doc_anchor_full')->row_array();
	}
	
	public function doc_add_full($size_id=0, $pro_id=0){
		
		/* Decrease QTY - Start */
		$sizeinfo = $this->get_sizeinfo_byid($size_id);
		$cur_qty = $sizeinfo['size_qty'];
		$doc_qty = $this->input->post('doc_qty');
		
		$total = $cur_qty - $doc_qty;
		
		if($cur_qty>=$doc_qty){
		
			$this->mssql->set("size_qty",$total);
			
			$this->mssql->where('size_id', $size_id);
			$this->mssql->update('product_size_anchor');
			/* Decrease QTY - End */
			
			$aData = array(
				'product_id' => $pro_id,
				'size_id' => $size_id,
				'doc_no' => $this->input->post('doc_no'),
				'doc_qty' => $this->input->post('doc_qty'),
				'doc_outcome' => $this->input->post('doc_outcome'),
				'doc_compound' => $this->input->post('doc_compound'),
				'doc_amount' => $this->input->post('doc_amount'),
				'doc_customer' => $this->input->post('doc_customer'),
				'doc_due' => $this->input->post('doc_due'),
				'doc_remark' => $this->input->post('doc_remark'),
				'created_time' => date("Y-m-d H:i:s")	
			);
			
			$this->mssql->insert('doc_anchor_full', $aData);
			
			return 1;
		}else{
			return 0;
		}
		
	}
	
	public function doc_edit_full($size_id=0, $pro_id=0, $doc_id=0){
		
		$aData = array(
			'product_id' => $pro_id,
			'size_id' => $size_id,
			'doc_no' => $this->input->post('doc_no'),
			'doc_outcome' => $this->input->post('doc_outcome'),
			'doc_compound' => $this->input->post('doc_compound'),
			'doc_customer' => $this->input->post('doc_customer'),
			'doc_due' => $this->input->post('doc_due'),
			'doc_remark' => $this->input->post('doc_remark'),
			'updated_time' => date("Y-m-d H:i:s")	
		);
		
		$this->mssql->where('doc_id', $doc_id);
		$this->mssql->update('doc_anchor_full', $aData);
		
		return 1;
		
	}
	
	public function doc_delete_full($doc_id=0){
		$this->mssql->where('doc_id', $doc_id);
		$this->mssql->delete('doc_anchor_full');
	}
	
	public function get_doclist_left($size_id=0, $pro_id=0){
		$this->mssql->where('size_id', $size_id);
		return $this->mssql->get('doc_anchor_left')->result_array();
	}
	
	public function getdoc_info_left($doc_id=0){
		$this->mssql->where('doc_id', $doc_id);
		return $this->mssql->get('doc_anchor_left')->row_array();
	}
	
	public function doc_add_left($size_id=0, $pro_id=0){
		
		/* Decrease QTY - Start */
		$sizeinfo = $this->get_sizeinfo_byid($size_id);
		$cur_qty = $sizeinfo['size_left_qty'];
		$doc_qty = $this->input->post('doc_qty');
		
		$total = $cur_qty - $doc_qty;
		
		if($cur_qty>=$doc_qty){
		
			$this->mssql->set("size_left_qty",$total);
			
			$this->mssql->where('size_id', $size_id);
			$this->mssql->update('product_size_anchor');
			/* Decrease QTY - End */
			
			$aData = array(
				'product_id' => $pro_id,
				'size_id' => $size_id,
				'doc_no' => $this->input->post('doc_no'),
				'doc_qty' => $this->input->post('doc_qty'),
				'doc_outcome' => $this->input->post('doc_outcome'),
				'doc_compound' => $this->input->post('doc_compound'),
				'doc_amount' => $this->input->post('doc_amount'),
				'doc_customer' => $this->input->post('doc_customer'),
				'doc_due' => $this->input->post('doc_due'),
				'doc_remark' => $this->input->post('doc_remark'),
				'created_time' => date("Y-m-d H:i:s")	
			);
			
			$this->mssql->insert('doc_anchor_left', $aData);
			
			return 1;
		}else{
			return 0;
		}
		
	}
	
	public function doc_edit_left($size_id=0, $pro_id=0, $doc_id=0){
		
		$aData = array(
			'product_id' => $pro_id,
			'size_id' => $size_id,
			'doc_no' => $this->input->post('doc_no'),
			'doc_outcome' => $this->input->post('doc_outcome'),
			'doc_compound' => $this->input->post('doc_compound'),
			'doc_customer' => $this->input->post('doc_customer'),
			'doc_due' => $this->input->post('doc_due'),
			'doc_remark' => $this->input->post('doc_remark'),
			'updated_time' => date("Y-m-d H:i:s")	
		);
		
		$this->mssql->where('doc_id', $doc_id);
		$this->mssql->update('doc_anchor_left', $aData);
		
		return 1;
		
	}
	
	public function doc_delete_left($doc_id=0){
		$this->mssql->where('doc_id', $doc_id);
		$this->mssql->delete('doc_anchor_left');
	}
	
	
	public function anchor_chemicallist($incomeid=0){
		$query = $this->mssql->where('income_id', $incomeid)
							->where('chemical_status !=','discard')
							->order_by('chemical_id','asc')
							->get('product_chemical_anchor')->result_array();
		return $query;
	}
	
	public function get_cer_chem($cerid=0){
		$query = $this->mssql->where('certificate_id', $cerid)
							->where('chemical_status','1')
							->order_by('chemical_createdtime','desc')
							->get('certificate_chemical')->result_array();
		return $query;
	}
	
	public function get_cer_chem_foredit($incomeid=0){
		$query = $this->mssql->where('income_id', $incomeid)
							->where('chemical_status','approved')
							->order_by('chemical_id','asc')
							->get('product_chemical_anchor')->result_array();
		return $query;
	}
	
	public function anchor_chemical_add($incomeid=0){
		
		foreach($this->input->post('chemical_value') as $key=>$value){
				if($value!=''){
					$aData = array(
						'income_id' => $incomeid,
						'chemical_value' => $value,
						'ps_max' => $_POST['ps_max'][$key],
						
						'ps_min' => $_POST['ps_min'][$key],
						'chemical_result' => $_POST['chemical_result'][$key],
						'chemical_status' => 'approved',
						'chemical_createdtime' => date("Y-m-d H:i:s"),
						'chemical_createdip' => $this->input->ip_address()
					);
					
					$this->mssql->insert('product_chemical_anchor', $aData);
				}
			}
		
		
	}
	
	public function anchor_chemical_edit($incomeid=0){
		
		foreach($this->input->post('chemical_value') as $key=>$value){
				if($value!=''){
					$aData = array(
						'ps_max' => $_POST['ps_max'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'chemical_result' => $_POST['chemical_result'][$key],
						'chemical_createdtime' => date("Y-m-d H:i:s"),
						'chemical_createdip' => $this->input->ip_address()
					);
					
					$this->mssql->where('chemical_id', $_POST['chemical_id'][$key]);
					$this->mssql->update('product_chemical_anchor', $aData);
				}
			}
		
		
	}
	
	public function get_cer_std($cerid=0){
		$query = $this->mssql->where('certificate_id', $cerid)
							->where('standard_status','1')
							->order_by('standard_createdtime','desc')
							->get('certificate_mechanical')->result_array();
		return $query;
	}
	
	public function get_cer_std_foredit($incomeid=0){
		$query = $this->mssql->where('income_id', $incomeid)
								->where('ps_status','approved')
								->order_by('ps_id','asc')
								->get('product_mechanical_anchor')->result_array();
		return $query;
	}
	
	public function anchor_standard_add($incomeid=0){
		if($this->input->post('standard_description')){
			$this->mssql->where('income_id', $incomeid);
			$this->mssql->delete('product_mechanical_anchor');
			foreach($this->input->post('standard_description') as $key=>$value){
				if($value!=''){
					$aData = array(
						'income_id' => $incomeid,
						'standard_description' => $value,
						'ps_max' => $_POST['ps_max'][$key],
						'ps_max_unit' => $_POST['ps_max_unit'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'ps_min_unit' => $_POST['ps_min_unit'][$key],
						'ps_result' => $_POST['ps_result'][$key],
						'ps_status' => 'approved',
						'ps_createdtime' => date("Y-m-d H:i:s"),
						'ps_createdip' => $this->input->ip_address()
					);
					
					$this->mssql->insert('product_mechanical_anchor', $aData);
				}
			}
		}
	}
	
	public function anchor_standard_edit($incomeid=0){
		if($this->input->post('standard_description')){
			foreach($this->input->post('standard_description') as $key=>$value){
				if($value!=''){
					$aData = array(
						'ps_max' => $_POST['ps_max'][$key],
						'ps_max_unit' => $_POST['ps_max_unit'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'ps_min_unit' => $_POST['ps_min_unit'][$key],
						'ps_result' => $_POST['ps_result'][$key],
						'ps_updatedtime' => date("Y-m-d H:i:s"),
						'ps_updatedip' => $this->input->ip_address()
					);
					$this->mssql->where('ps_id', $_POST['ps_id'][$key]);
					$this->mssql->update('product_mechanical_anchor', $aData);
				}
			}
		}
	}
	
	
	
	
}
?>