<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
File Name 	: spring_final.php
Controller	: spring_final
Create By 	: Jarak Kritkiattisak
Create Date 	: 7/6/2557 BE
Project 	: iAon Project
Version 		: 1.0
*/
class Spring_certificate extends CI_Model {
	var $_data=array();
	public function __construct()
	{
		parent::__construct();
		$this->mssql = $this->load->database("mssql",true);
	}
	public function dataTable($limit=100,$offset=0)
	{
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("final_spring.final_invoice_no",$q);
			$this->mssql->or_like("product_spring.product_name",$q);
		}
		$this->mssql->join("product_spring","product_spring.product_id = final_spring.final_spring_id");
		$this->mssql->limit($limit,$offset);
		$this->mssql->where("test_status","2");
		$this->mssql->order_by("final_id","DESC");
		$res = $this->mssql->get("final_spring")->result_array();
		return $res;
	}
	function getcertificate($final_id)
	{
		$this->mssql->limit(1);
		$this->mssql->where("final_id",$final_id);
		$res = $this->mssql->get("final_spring")->row_array();
		return $res;
	}
	
	public function export_data()
	{
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("final_spring.final_invoice_no",$q);
			$this->mssql->or_like("product_spring.product_name",$q);
		}
		$this->mssql->join("product_spring","product_spring.product_id = final_spring.final_spring_id");
		$res = $this->mssql->get("final_spring");
		return $res;
	}
	
	public function add_certificate()
	{
		//$final_job_id=trim(strip_tags($this->input->post("final_job_id")));
		//$final_sale_order_no=trim(strip_tags($this->input->post("final_sale_order_no")));
		$customer_id=trim(strip_tags($this->input->post("customer_id")));
		$final_invoice_no=trim(strip_tags($this->input->post("final_invoice_no")));
		$final_po_no=trim(strip_tags($this->input->post("final_po_no")));
		$final_delivery_no=trim(strip_tags($this->input->post("final_delivery_no")));
		$final_purchase_no=trim(strip_tags($this->input->post("final_purchase_no")));
		$final_sale_order_no=trim(strip_tags($this->input->post("final_sale_order_no")));
		$final_spring_id=trim(strip_tags($this->input->post("final_spring_id")));
		$final_spring_size_id=trim(strip_tags($this->input->post("final_spring_size_id")));
		$final_spring_income_id=trim(strip_tags($this->input->post("final_spring_income_id")));
		$final_spring_qty=trim(strip_tags($this->input->post("final_spring_qty")));
		
		
		$final_createdtime=date("Y-m-d H:i:s");
		$final_createdip=$this->input->ip_address();
		$final_createdid=$this->admin_library->user_id();
		
		$pro_name = $this->getproduct_name($final_spring_id);
		$size_m = $this->get_size($final_spring_size_id);
		
		$final_job_id = $pro_name['product_initial']."M".$size_m['size_m']."-".date("Yhis");
				
		$this->mssql->set("final_job_id",$final_job_id);
		//$this->mssql->set("final_sale_order_no",$final_sale_order_no);
		$this->mssql->set("customer_id",$customer_id);
		$this->mssql->set("final_invoice_no",$final_invoice_no);
		$this->mssql->set("final_purchase_no",$final_purchase_no);
		$this->mssql->set("final_sale_order_no",$final_sale_order_no);
		$this->mssql->set("final_po_no",$final_po_no);
		$this->mssql->set("final_delivery_no",$final_delivery_no);
		
		$this->mssql->set("final_spring_id",$final_spring_id);
		$this->mssql->set("final_spring_size_id",$final_spring_size_id);
		$this->mssql->set("final_spring_income_id",$final_spring_income_id);
		$this->mssql->set("final_spring_qty",$final_spring_qty);

		/* spring - Start */
		$this->mssql->set('final_have_spring', $final_have_spring);
		$this->mssql->set('final_spring_id', $final_spring_id);
		$this->mssql->set('final_spring_size_id', $final_spring_size_id);
		$this->mssql->set('final_spring_income_id', $final_spring_income_id);
		$this->mssql->set('final_spring_qty', $final_spring_qty);
		/* spring - End */
		
		/* spring - Start */
		$this->mssql->set('final_have_spring', $final_have_spring);
		$this->mssql->set('final_spring_id', $final_spring_id);
		$this->mssql->set('final_spring_size_id', $final_spring_size_id);
		$this->mssql->set('final_spring_income_id', $final_spring_income_id);
		$this->mssql->set('final_spring_qty', $final_spring_qty);
		/* spring - End */
		
		$this->mssql->set("final_createdtime",$final_createdtime);
		$this->mssql->set("final_createdip",$final_createdip);
		$this->mssql->set("final_createdid",$final_createdid);
		$this->mssql->insert("final_spring");
					
	}
	public function edit_certificate()
	{
		$final_id=trim(strip_tags($this->input->post("final_id")));
		$final_text_set=trim(strip_tags($this->input->post("final_text_set")));
		$final_text_remark=trim(strip_tags($this->input->post("final_text_remark")));
		
		$final_createdtime=date("Y-m-d H:i:s");
		$final_createdip=$this->input->ip_address();
		$final_createdid=$this->admin_library->user_id();
		
		$this->mssql->set("final_text_set",$final_text_set);
		$this->mssql->set("final_text_remark",$final_text_remark);
		$this->mssql->set("final_updatedtime",$final_createdtime);
		$this->mssql->set("final_updatedip",$final_createdip);
		$this->mssql->set("final_updatedid",$final_createdid);
		$this->mssql->where("final_id",$final_id);
		$this->mssql->update("final_spring");
					
	}
	function delete_certificate($final_id)
	{
		
		
		$this->mssql->where("final_id",$final_id);
		$this->mssql->delete("final_spring");
		
			}
	
	public function getproduct_name($pro_id=0)
	{
		$this->mssql->limit(1);
		$this->mssql->where("product_id",$pro_id);
		$rs = $this->mssql->get("product_spring")->row_array();
		return $rs;
	}
	
	public function getcustomer_name($cus_id=0)
	{
		$this->mssql->limit(1);
		$this->mssql->where("customer_id",$cus_id);
		$rs = $this->mssql->get("customer")->row_array();
		return $rs;
	}
	
	public function getSupplier()
	{
		$rs = $this->mssql->get("supplier")->result_array();
		return $rs;
	}
	public function getCustomer()
	{
		$rs = $this->mssql->get("customer")->result_array();
		return $rs;
	}
	
	public function get_springsize_byid($springid=0){
		$query = $this->mssql->where('product_id', $springid)
							->get('product_size_spring')->result_array();
		return $query;
	}
	
	public function get_springincome_byid($springid=0,$springsizeid=0){
						$this->mssql->where('income_product_id', $springid);
						$this->mssql->where('size_id', $springsizeid);
						$this->mssql->where('test_status', "2");
			$query =	$this->mssql->get('income_spring')->result_array();
			
		return $query;
		
	}
	function get_size($size_id)
	{
		$query = $this->mssql->where('size_id', $size_id)
							->get('product_size_spring')->row_array();
		return $query;
	}
	function get_cer_size($product_id)
	{
		$query = $this->mssql->where('product_id', $product_id)
							->get('product_cer_size_spring')->result_array();
		return $query;
	}
}