<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class bolt_stock extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		$this->mssql = $this->load->database("mssql",true);
	}
	function getStockInfo($lot_id)
	{
		$this->mssql->where("lot_bolt.lot_id",$lot_id);
		$this->mssql->join('income_bolt','lot_bolt.income_id=income_bolt.income_id');
		$product = $this->mssql->get("lot_bolt")->row_array();
		return $product;
	}
	public function dataTable($limit=100,$offset=0)
	{
		/*$search_q = trim(strip_tags($this->input->get("q")));
		if($search_q != ""){
			$this->mssql->where("product_bolt.product_name",$search_q);
			$this->mssql->where("lot_bolt.lot_no",$search_q);
		}*/
		
		
		$this->mssql->order_by("stock_bolt.stock_createdtime","desc");
		//$this->mssql->join("product_size_bolt","product_size_bolt.size_id = lot_bolt.size_id","LEFT");
		$this->mssql->join("product_size_bolt","product_size_bolt.size_id = stock_bolt.size_id","LEFT");
		$this->mssql->limit($limit,$offset);
		$product = $this->mssql->get("stock_bolt")->result_array();
		//exit($this->mssql->last_query());
		return $product;
	}
	
	public function dataTable_size($pro_id,$limit=100,$offset=0)
	{
		/*$search_q = trim(strip_tags($this->input->get("q")));
		if($search_q != ""){
			$this->mssql->where("product_bolt.product_name",$search_q);
			$this->mssql->where("lot_bolt.lot_no",$search_q);
		}*/
		
		$this->mssql->where('product_id', $pro_id);
		$this->mssql->where('size_status !=','discard');

		$this->mssql->limit($limit,$offset);
		$product = $this->mssql->get("product_size_bolt")->result_array();
		//exit($this->mssql->last_query());
		return $product;
	}
	
	function export_data()
	{
		//$this->mssql->select("lot_bolt.lot_no,supplier.supplier_name,supplier.supplier_tel,product_size_bolt.size_m,product_size_bolt.size_p,product_size_bolt.size_length,product_bolt.product_name,product_bolt.product_grade,product_bolt.product_initial,product_bolt.product_iso,lot_bolt.lot_certificate_no,lot_bolt.lot_commodity,lot_bolt.lot_surface,lot_bolt.lot_material,lot_bolt.lot_amount,lot_bolt.lot_heat_no,lot_bolt.lot_color,lot_bolt.lot_color_value,lot_bolt.lot_remark,lot_bolt.lot_createdid,lot_bolt.lot_updatedid,lot_bolt.lot_import_date,lot_bolt.lot_remain,lot_bolt.lot_id");
		$this->mssql->select("lot_bolt.lot_no,supplier.supplier_name,supplier.supplier_tel,product_bolt.product_name,product_bolt.product_grade,product_bolt.product_initial,product_bolt.product_iso,lot_bolt.lot_amount,lot_bolt.lot_remark,lot_bolt.lot_createdid,lot_bolt.lot_updatedid,lot_bolt.lot_import_date,lot_bolt.lot_remain,lot_bolt.lot_id");
		$this->mssql->where("lot_bolt.lot_status","approved");
		
		$search_q = trim(strip_tags($this->input->get("q")));
		if($search_q != ""){
			$this->mssql->where("product_bolt.product_name",$search_q);
			$this->mssql->where("lot_bolt.lot_no",$search_q);
		}
		
		$this->mssql->order_by("lot_bolt.lot_id","desc");
		$this->mssql->join("product_bolt","product_bolt.product_id=lot_bolt.product_id","LEFT");
		//$this->mssql->join("product_size_bolt","product_size_bolt.size_id = lot_bolt.size_id","LEFT");
		$this->mssql->join("supplier","supplier.supplier_id = lot_bolt.supplier_id","LEFT");
		$product = $this->mssql->get("lot_bolt");
		return $product;

	}
	
	public function add_stock()
	{
		$lot_no=trim(strip_tags($this->input->post("lot_no")));
		$lot_amount=trim(strip_tags($this->input->post("lot_amount")));
		$income_id=trim(strip_tags($this->input->post("income_id")));
		$supplier_id=trim(strip_tags($this->input->post("supplier_id")));
		$product_id=trim(strip_tags($this->input->post("product_id")));
		$lot_remark=trim(strip_tags($this->input->post("lot_remark")));
		$lot_import_date=trim(strip_tags($this->input->post("lot_import_date")));
		$lot_import_date = str_replace('/', '-', $lot_import_date);
		$lot_import_date=date("Y-m-d",strtotime($lot_import_date));
		$this->mssql->set("lot_no",$lot_no);
		$this->mssql->set("income_id",$income_id);
		$this->mssql->set("supplier_id",$supplier_id);
		$this->mssql->set("product_id",$product_id);
		$this->mssql->set("lot_amount",$lot_amount);
		$this->mssql->set("lot_remain",$lot_amount);
		$this->mssql->set("lot_remark",$lot_remark);
		$this->mssql->set("lot_status","approved");
		$this->mssql->set("lot_createdtime",date("Y-m-d H:i:s"));
		$this->mssql->set("lot_createdip",$this->input->ip_address());
		$this->mssql->set("lot_createdid",$this->admin_library->user_id());
		$this->mssql->set("lot_import_date",$lot_import_date);
		$lot_id = $this->mssql->insert("lot_bolt");
	}
	public function edit_stock()
	{
		$lot_id=trim(strip_tags($this->input->post("lot_id")));
		$lot_no=trim(strip_tags($this->input->post("lot_no")));
		$lot_amount=trim(strip_tags($this->input->post("lot_amount")));
		$income_id=trim(strip_tags($this->input->post("income_id")));
		$supplier_id=trim(strip_tags($this->input->post("supplier_id")));
		$product_id=trim(strip_tags($this->input->post("product_id")));
		$lot_remark=trim(strip_tags($this->input->post("lot_remark")));
		$lot_import_date=trim(strip_tags($this->input->post("lot_import_date")));
		$lot_import_date = str_replace('/', '-', $lot_import_date);
		$lot_import_date=date("Y-m-d",strtotime($lot_import_date));


		$this->mssql->set("lot_no",$lot_no);
		$this->mssql->set("income_id",$income_id);
		$this->mssql->set("supplier_id",$supplier_id);
		$this->mssql->set("product_id",$product_id);
		$this->mssql->set("lot_amount",$lot_amount);
		$this->mssql->set("lot_remain",$lot_amount);
		$this->mssql->set("lot_remark",$lot_remark);
		$this->mssql->set("lot_status","approved");
		$this->mssql->set("lot_createdtime",date("Y-m-d H:i:s"));
		$this->mssql->set("lot_createdip",$this->input->ip_address());
		$this->mssql->set("lot_createdid",$this->admin_library->user_id());
		$this->mssql->set("lot_import_date",$lot_import_date);
		$this->mssql->where("lot_id",$lot_id);
		$lot_id = $this->mssql->update("lot_bolt");
	}

	public function delete_stock($lot_id)
	{
		$this->mssql->where("lot_id",$lot_id);
		return $this->mssql->delete("lot_bolt");	
	}
	
	public function getproduct_name($size_id)
	{
		$this->mssql->select("product_bolt.product_name");
		$this->mssql->where("product_size_bolt.size_id",$size_id);
		$this->mssql->where("product_size_bolt.size_status","approved");
		$this->mssql->where("product_bolt.product_status","approved");
		$this->mssql->join("product_size_bolt","product_bolt.product_id=product_size_bolt.product_id");
		$this->mssql->group_by("product_size_bolt.product_id,product_bolt.product_name");
		
		$rs = $this->mssql->get("product_bolt")->row_array();
		return $rs;
	}
	
	public function getSupplier()
	{
		$rs = $this->mssql->get("supplier")->result_array();
		return $rs;
	}
	public function getProduct()
	{
		$this->mssql->select("product_size_bolt.product_id,product_bolt.product_name, product_bolt.product_type");
		$this->mssql->where("product_size_bolt.size_status","approved");
		$this->mssql->where("product_bolt.product_status","approved");
		$this->mssql->join("product_size_bolt","product_bolt.product_id=product_size_bolt.product_id");
		$this->mssql->group_by("product_size_bolt.product_id, product_bolt.product_name, product_bolt.product_type");
		$this->mssql->order_by("product_size_bolt.product_id","DESC");
		$rs = $this->mssql->get("product_bolt")->result_array();
		return $rs;
	}
	public function getSize($product_id)
	{
		$this->mssql->where("product_id",$product_id);
		$this->mssql->where("size_status","approved");
		$rs = $this->mssql->get("product_size_bolt")->result_array();
		return $rs;		
	}
	
	public function getIncome(){
		$this->mssql->where("test_status","2");
		$rs = $this->mssql->get("income_bolt")->result_array();
		return $rs;
	}
	
	public function ajax_getstockinfo($incomeid=0){
		$query = $this->mssql->where('income_bolt.income_id', $incomeid)
								->where('income_bolt.test_status', '2')
								->join('supplier', 'income_bolt.income_supplier_id=supplier.supplier_id')
								->join('product_bolt','income_bolt.income_product_id=product_bolt.product_id')
								->join('product_size_bolt','income_bolt.size_id=product_size_bolt.size_id')
								->get('income_bolt')->row_array();
		return $query;
	}
	
	public function getSupplierName($supplier_id=0){
		$this->mssql->where("supplier_id",$supplier_id);
		$rs = $this->mssql->get("supplier")->result_array();
		return $rs;
		
	}
	
	public function getProductName($product_id=0){
		$this->mssql->where("product_id",$product_id);
		$rs = $this->mssql->get("product_bolt")->result_array();
		return $rs;
		
	}
	
}