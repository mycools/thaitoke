<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
File Name 	: bolt_final.php
Controller	: Bolt_final
Create By 	: Jarak Kritkiattisak
Create Date 	: 7/6/2557 BE
Project 	: iAon Project
Version 		: 1.0
*/
class Final_manager_anchor_model extends CI_Model {
	var $_data=array();
	public function __construct()
	{
		parent::__construct();
		$this->mssql = $this->load->database("mssql",true);
	}
	public function dataTable($productid, $type='axel',$limit=100,$offset=0)
	{
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("final_invoice_no",trim($q));
			//$this->mssql->or_like("product_bolt.product_name",$q);
		}
		$this->mssql->where("product_id",$productid);
		$this->mssql->where("product_type",$type);
		$this->mssql->limit($limit,$offset);
		$this->mssql->order_by("final_id","DESC");
		$res = $this->mssql->get("final_anchor")->result_array(); 
		return $res;
	}
	
	public function get_count($type,$productid)
	{
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("final_invoice_no",trim($q));
		}
		$query = $this->mssql->where('product_id',$productid)
					->where('product_type', $type)
					->get('final_anchor')->num_rows();
		return $query;
	}
	
	function getfinal($final_id)
	{
		$this->mssql->limit(1);
		$this->mssql->where("final_id",$final_id);
		$res = $this->mssql->get("final_anchor")->row_array();
		return $res;
	}
	
	public function getcustomer_name($cus_id=0)
	{
		$this->mssql->limit(1);
		$this->mssql->where("customer_id",$cus_id);
		$rs = $this->mssql->get("customer")->row_array();
		return $rs;
	}
	
	
	public function getCustomer()
	{
		$rs = $this->mssql->get("customer")->result_array();
		return $rs;
	}
	
	public function add_final(){
		
		
		$aData = array(
			'customer_id' => $this->input->post('customer_id'),
			'final_job_no' => $this->input->post('final_job_no'),
			'product_type' => $this->input->post('product_type'),
			'product_id' => $this->input->post('product_id'),
			'final_invoice_no' => $this->input->post('final_invoice_no'),
			'final_purchase_no' => $this->input->post('final_purchase_no'),
			'final_delivery_no' => $this->input->post('final_delivery_no'),
			'final_date_delivery' => todbdate($this->input->post("final_date_delivery")),
			'final_material_no' => $this->input->post('final_material_no'),
			'final_certificate_no' => $this->input->post('final_certificate_no'),
			'final_quantity' => $this->input->post('final_quantity'),
			'final_createdtime' => date("Y-m-d H:i:s"),
			'final_createdip' => $this->input->ip_address()
		);
		
		$this->mssql->insert('final_anchor', $aData);
	}
	
	public function edit_final(){
		
		$finalid = $this->input->post('final_id');
		
		$aData = array(
			'customer_id' => $this->input->post('customer_id'),
			'final_job_no' => $this->input->post('final_job_no'),
			'product_type' => $this->input->post('product_type'),
			'product_id' => $this->input->post('product_id'),
			'final_invoice_no' => $this->input->post('final_invoice_no'),
			'final_purchase_no' => $this->input->post('final_purchase_no'),
			'final_delivery_no' => $this->input->post('final_delivery_no'),
			'final_date_delivery' => todbdate($this->input->post("final_date_delivery")),
			'final_material_no' => $this->input->post('final_material_no'),
			'final_certificate_no' => $this->input->post('final_certificate_no'),
			'final_quantity' => $this->input->post('final_quantity'),
			'final_updatedtime' => date("Y-m-d H:i:s"),
			'final_updatedip' => $this->input->ip_address()
		);
		
		$this->mssql->where('final_id', $finalid);
		$this->mssql->update('final_anchor', $aData);
		
		return true;
	}
	
	public function delete_final($finalid=0){
		$this->mssql->where('final_id', $finalid);
		$this->mssql->delete('final_anchor');
		
	}
	
	public function get_size_id_for_back($productid){
		$query = $this->mssql->where('product_id', $productid)
								->get('anchor_product')->row_array();
		return $query['size_id'];
		
		
	}
	
	
}