<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class nut_stock extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		$this->mssql = $this->load->database("mssql",true);
	}
	function getStockInfo($lot_id)
	{
		$this->mssql->where("lot_nut.lot_id",$lot_id);
		$product = $this->mssql->get("lot_nut")->row_array();
		return $product;
	}
	public function dataTable($limit=100,$offset=0)
	{
		$this->mssql->select("lot_nut.lot_no,supplier.supplier_name,supplier.supplier_tel,product_size_nut.size_m,product_size_nut.size_p,product_size_nut.size_length,product_nut.product_name,product_nut.product_grade,product_nut.product_initial,product_nut.product_iso,lot_nut.lot_certificate_no,lot_nut.lot_commodity,lot_nut.lot_surface,lot_nut.lot_material,lot_nut.lot_amount,lot_nut.lot_heat_no,lot_nut.lot_color,lot_nut.lot_color_value,lot_nut.lot_remark,lot_nut.lot_createdid,lot_nut.lot_updatedid,lot_nut.lot_import_date,lot_nut.lot_remain,lot_nut.lot_id");
		$this->mssql->where("lot_nut.lot_status","approved");
		
		$search_q = trim(strip_tags($this->input->get("q")));
		if($search_q != ""){
			$this->mssql->where("product_nut.product_name",$search_q);
			$this->mssql->where("lot_nut.lot_no",$search_q);
		}
		
		
		$this->mssql->order_by("lot_nut.lot_id","desc");
		$this->mssql->join("product_nut","product_nut.product_id=lot_nut.product_id","LEFT");
		$this->mssql->join("product_size_nut","product_size_nut.size_id = lot_nut.size_id","LEFT");
		$this->mssql->join("supplier","supplier.supplier_id = lot_nut.supplier_id","LEFT");
		$this->mssql->limit($limit,$offset);
		$product = $this->mssql->get("lot_nut")->result_array();
		//exit($this->mssql->last_query());
		return $product;
	}
	function export_data()
	{
		$this->mssql->select("lot_nut.lot_no,supplier.supplier_name,supplier.supplier_tel,product_size_nut.size_m,product_size_nut.size_p,product_size_nut.size_length,product_nut.product_name,product_nut.product_grade,product_nut.product_initial,product_nut.product_iso,lot_nut.lot_certificate_no,lot_nut.lot_commodity,lot_nut.lot_surface,lot_nut.lot_material,lot_nut.lot_amount,lot_nut.lot_heat_no,lot_nut.lot_color,lot_nut.lot_color_value,lot_nut.lot_remark,lot_nut.lot_createdid,lot_nut.lot_updatedid,lot_nut.lot_import_date,lot_nut.lot_remain,lot_nut.lot_id");
		$this->mssql->where("lot_nut.lot_status","approved");
		
		$search_q = trim(strip_tags($this->input->get("q")));
		if($search_q != ""){
			$this->mssql->where("product_nut.product_name",$search_q);
			$this->mssql->where("lot_nut.lot_no",$search_q);
		}
		
		$this->mssql->order_by("lot_nut.lot_id","desc");
		$this->mssql->join("product_nut","product_nut.product_id=lot_nut.product_id","LEFT");
		$this->mssql->join("product_size_nut","product_size_nut.size_id = lot_nut.size_id","LEFT");
		$this->mssql->join("supplier","supplier.supplier_id = lot_nut.supplier_id","LEFT");
		$product = $this->mssql->get("lot_nut");
		return $product;

	}
	public function add_stock()
	{
		$lot_no=trim(strip_tags($this->input->post("lot_no")));
		$supplier_id=trim(strip_tags($this->input->post("supplier_id")));
		$product_id=trim(strip_tags($this->input->post("product_id")));
		$size_id=trim(strip_tags($this->input->post("size_id")));
		$lot_certificate_no=trim(strip_tags($this->input->post("lot_certificate_no")));
		$lot_surface=trim(strip_tags($this->input->post("lot_surface")));
		$lot_amount=trim(strip_tags($this->input->post("lot_amount")));
		$lot_commodity=trim(strip_tags($this->input->post("lot_commodity")));
		$lot_material=trim(strip_tags($this->input->post("lot_material")));
		$lot_heat_no=trim(strip_tags($this->input->post("lot_heat_no")));
		$lot_color=trim(strip_tags($this->input->post("lot_color")));
		$lot_color_value=trim(strip_tags($this->input->post("lot_color_value")));
		$lot_remark=trim(strip_tags($this->input->post("lot_remark")));
		$lot_import_date=trim(strip_tags($this->input->post("lot_import_date")));
		$lot_import_date = str_replace('/', '-', $lot_import_date);
		$lot_import_date=date("Y-m-d",strtotime($lot_import_date));
		$this->mssql->set("lot_no",$lot_no);
		$this->mssql->set("supplier_id",$supplier_id);
		$this->mssql->set("product_id",$product_id);
		$this->mssql->set("size_id",$size_id);
		$this->mssql->set("lot_certificate_no",$lot_certificate_no);
		$this->mssql->set("lot_surface",$lot_surface);
		$this->mssql->set("lot_amount",$lot_amount);
		$this->mssql->set("lot_remain",$lot_amount);
		$this->mssql->set("lot_commodity",$lot_commodity);
		$this->mssql->set("lot_material",$lot_material);
		$this->mssql->set("lot_heat_no",$lot_heat_no);
		$this->mssql->set("lot_color",$lot_color);
		$this->mssql->set("lot_color_value",$lot_color_value);
		$this->mssql->set("lot_remark",$lot_remark);
		$this->mssql->set("lot_status","approved");
		$this->mssql->set("lot_createdtime",date("Y-m-d H:i:s"));
		$this->mssql->set("lot_createdip",$this->input->ip_address());
		$this->mssql->set("lot_createdid",$this->admin_library->user_id());
		$this->mssql->set("lot_import_date",$lot_import_date);
		$lot_id = $this->mssql->insert("lot_nut");
	}
	public function edit_stock()
	{
		$lot_id=trim(strip_tags($this->input->post("lot_id")));
		$lot_no=trim(strip_tags($this->input->post("lot_no")));
		$supplier_id=trim(strip_tags($this->input->post("supplier_id")));
		$product_id=trim(strip_tags($this->input->post("product_id")));
		$size_id=trim(strip_tags($this->input->post("size_id")));
		$lot_certificate_no=trim(strip_tags($this->input->post("lot_certificate_no")));
		$lot_surface=trim(strip_tags($this->input->post("lot_surface")));
		$lot_amount=trim(strip_tags($this->input->post("lot_amount")));
		$lot_commodity=trim(strip_tags($this->input->post("lot_commodity")));
		$lot_material=trim(strip_tags($this->input->post("lot_material")));
		$lot_heat_no=trim(strip_tags($this->input->post("lot_heat_no")));
		$lot_color=trim(strip_tags($this->input->post("lot_color")));
		$lot_color_value=trim(strip_tags($this->input->post("lot_color_value")));
		$lot_remark=trim(strip_tags($this->input->post("lot_remark")));
		$lot_import_date=trim(strip_tags($this->input->post("lot_import_date")));
		$lot_import_date = str_replace('/', '-', $lot_import_date);
		$lot_import_date=date("Y-m-d",strtotime($lot_import_date));
		$this->mssql->set("lot_no",$lot_no);
		$this->mssql->set("supplier_id",$supplier_id);
		$this->mssql->set("product_id",$product_id);
		$this->mssql->set("size_id",$size_id);
		$this->mssql->set("lot_certificate_no",$lot_certificate_no);
		$this->mssql->set("lot_surface",$lot_surface);
		$this->mssql->set("lot_amount",$lot_amount);
		$this->mssql->set("lot_remain",$lot_amount);
		$this->mssql->set("lot_commodity",$lot_commodity);
		$this->mssql->set("lot_material",$lot_material);
		$this->mssql->set("lot_heat_no",$lot_heat_no);
		$this->mssql->set("lot_color",$lot_color);
		$this->mssql->set("lot_color_value",$lot_color_value);
		$this->mssql->set("lot_remark",$lot_remark);
		$this->mssql->set("lot_status","approved");
		$this->mssql->set("lot_updatedtime",date("Y-m-d H:i:s"));
		$this->mssql->set("lot_updatedip",$this->input->ip_address());
		$this->mssql->set("lot_updatedid",$this->admin_library->user_id());
		$this->mssql->set("lot_import_date",$lot_import_date);
		$this->mssql->where("lot_id",$lot_id);
		$lot_id = $this->mssql->update("lot_nut");
	}

	public function delete_stock($lot_id)
	{
		$this->mssql->where("lot_id",$lot_id);
		return $this->mssql->delete("lot_nut");	
	}
	public function getSupplier()
	{
		$rs = $this->mssql->get("supplier")->result_array();
		return $rs;
	}
	public function getProduct()
	{
		$this->mssql->select("product_size_nut.product_id,product_nut.product_name,product_nut.product_type");
		$this->mssql->where("product_size_nut.size_status","approved");
		$this->mssql->where("product_nut.product_status","approved");
		$this->mssql->join("product_size_nut","product_nut.product_id=product_size_nut.product_id");
		$this->mssql->group_by("product_size_nut.product_id,product_nut.product_name,product_nut.product_type");
		$this->mssql->order_by("product_size_nut.product_id","DESC");
		$rs = $this->mssql->get("product_nut")->result_array();
		return $rs;
	}
	public function getSize($product_id)
	{
		$this->mssql->where("product_id",$product_id);
		$this->mssql->where("size_status","approved");
		$rs = $this->mssql->get("product_size_nut")->result_array();
		return $rs;		
	}
	
}