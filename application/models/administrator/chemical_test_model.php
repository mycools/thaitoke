<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class chemical_test_model extends CI_Model {
	var $_data=array();
	public function __construct()
	{
		parent::__construct();
		$this->mssql = $this->load->database("mssql",true);
	}
	
	public function get_thickness()
	{
		
		$row = $this->mssql->get("thickness_galvanizes")->result_array(); 
		return $row; 
	}
	
	/* Bolt - Start */
	public function get_incomeinfo_bolt_byid($incomeid=0){
		$query = $this->mssql->where('income_bolt.income_id', $incomeid)
								
								->get('income_bolt')->row_array();
		return $query;
	}
	
	public function get_chemical_bolt($productid=0){
		$query = $this->mssql->where('product_chemical_bolt.product_id', $productid)
								->order_by('chemical_id','asc')
								->get('product_chemical_bolt')->result_array();
		return $query;
	}
	
	public function check_chemical_test_bolt_by_incomeid($incomeid=0){
		$query = $this->mssql->where('income_id', $incomeid)
							->count_all_results('chemical_test_bolt');
		return $query;
	}
	
	public function bolt_test_add($incomeid=0){
		$this->mssql->where('income_id', @$incomeid);
		$this->mssql->delete('chemical_test_bolt');
		
		$this->mssql->where('income_id', @$incomeid);
		$this->mssql->delete('chemical_final_bolt');
		
		foreach($_POST['chemical_id'] as $key=>$value){

			$aData = array(
				'income_id' => $incomeid,
				'chemical_id' => $value,
				'chemical_value' => $_POST['chemical_value'][$key],
				'test_value' => $_POST['test_value'][$key],
				'test_createdtime' => date("Y-m-d H:i:s"),
				'test_createdip' => $this->input->ip_address()
			);
			
			$this->mssql->insert('chemical_test_bolt', $aData);
			//$this->mssql->insert('chemical_final_bolt', $aData);
	
		}
		
	}
	
	public function get_boltthicknessinfo_byid($incomeid=0){
		$query = $this->mssql->where('income_id', $incomeid)
								->get('thickness_test_bolt')->row_array();
		return $query;
	}
	
	public function bolt_thickness_add($incomeid=0){
			
			$this->mssql->where('income_id', @$incomeid);
			$this->mssql->delete('thickness_test_bolt');
			
			$this->mssql->where('income_id', @$incomeid);
			$this->mssql->delete('thickness_final_bolt');
			
			$tData = array(
				'income_id' => $incomeid,
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'thickness_createdtime' => date("Y-m-d H:i:s"),
				'thickness_createdip' => $this->input->ip_address()
			);
			
			$this->mssql->insert('thickness_test_bolt', $tData);
			$this->mssql->insert('thickness_final_bolt', $tData);
		
		
	}
	
	public function update_income_status($incomeid=0)
	{
		$this->mssql->set("test_status",$_POST['test_status']);
		$this->mssql->where("income_id",$incomeid);
		$this->mssql->update("income_bolt");
	}
	
	public function update_product_qty($size_id=0,$qty=0, $incomeid=0, $test_status=0){
	
		$incomeinfo = $this->get_incomeinfo_bolt_byid($incomeid);

		if($incomeinfo['stock_add_status']==0 && $test_status==2){
	
			$row = $this->mssql->where('product_size_bolt.size_id', $size_id)
									->get('product_size_bolt')->row_array();

			$old_qty = $row['size_qty'];
			$total = $old_qty+$qty;
			
			$aUpdate = array(
				'size_qty' => $total
			);
			
			$this->mssql->where("size_id",$size_id);
			$this->mssql->update("product_size_bolt", $aUpdate);
			
			/* Update `stock_add_status` - Start */
			$this->mssql->set('stock_add_status',1);
			$this->mssql->where('income_id', $incomeid);
			$this->mssql->where('size_id', $size_id);
			$this->mssql->update('income_bolt');
			/* Update `stock_add_status` - End */
		
		}
		
		return $total;
		
	}
	
	public function get_bolt_test_result($incomeid=0, $chemicalid=0){
		$query = $this->mssql->where('income_id', $incomeid)
								->where('chemical_id', $chemicalid)
								->get('chemical_test_bolt')->row_array();
		return $query;
	}
	/* Bolt - End */
	
	/* Nut - Start */
	public function get_incomeinfo_nut_byid($incomeid=0){
		$query = $this->mssql->where('income_nut.income_id', $incomeid)
								
								->get('income_nut')->row_array();
		return $query;
	}
	
	public function get_chemical_nut($productid=0){
		$query = $this->mssql->where('product_chemical_nut.product_id', $productid)
								->get('product_chemical_nut')->result_array();
		return $query;
	}
	
	public function check_chemical_test_nut_by_incomeid($incomeid=0){
		$query = $this->mssql->where('income_id', $incomeid)
							->count_all_results('chemical_test_nut');
		return $query;
	}
	
	public function nut_test_add($incomeid=0){
		$this->mssql->where('income_id', @$incomeid);
		$this->mssql->delete('chemical_test_nut');
		
		$this->mssql->where('income_id', @$incomeid);
		$this->mssql->delete('chemical_final_nut');
		
		foreach($this->input->post('chemical_id') as $key=>$value){
			$aData = array(
				'income_id' => $incomeid,
				'chemical_id' => $value,
				'chemical_value' => $_POST['chemical_value'][$key],
				'test_value' => $_POST['test_value'][$key],
				'test_createdtime' => date("Y-m-d H:i:s"),
				'test_createdip' => $this->input->ip_address()
			);
			
			
			$this->mssql->insert('chemical_test_nut', $aData);
			//$this->mssql->insert('chemical_final_nut', $aData);
	
		}
			
	}
	
	public function get_nutthicknessinfo_byid($incomeid=0){
		$query = $this->mssql->where('income_id', $incomeid)
								->get('thickness_test_nut')->row_array();
		return $query;
	}
	
	public function nut_thickness_add($incomeid=0){
			
			$this->mssql->where('income_id', @$incomeid);
			$this->mssql->delete('thickness_test_nut');
			
			$this->mssql->where('income_id', @$incomeid);
			$this->mssql->delete('thickness_test_nut');
		
			$tData = array(
				'income_id' => $incomeid,
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'thickness_createdtime' => date("Y-m-d H:i:s"),
				'thickness_createdip' => $this->input->ip_address()
			);
			$this->mssql->insert('thickness_test_nut', $tData);
			$this->mssql->insert('thickness_final_nut', $tData);
		
		
	}
	
	public function update_income_status_nut($incomeid=0)
	{
		$this->mssql->set("test_status",$_POST['test_status']);
		$this->mssql->where("income_id",$incomeid);
		$this->mssql->update("income_nut");
	}
	
	public function update_product_nut_qty($size_id=0,$qty=0, $incomeid=0, $test_status=0){
		
		$incomeinfo = $this->get_incomeinfo_nut_byid($incomeid);
		
		if($incomeinfo['stock_add_status']==0 && $test_status==2){
		
			$row = $this->mssql->where('product_size_nut.size_id', $size_id)
									
									->get('product_size_nut')->row_array();
			$old_qty = $row['size_qty'];
			
			$total = $old_qty+$qty;
			
			$this->mssql->set("size_qty",$total);
			$this->mssql->where("size_id",$size_id);
			$this->mssql->update("product_size_nut");
			
			/* Update `stock_add_status` - Start */
			$this->mssql->set('stock_add_status',1);
			$this->mssql->where('income_id', $incomeid);
			$this->mssql->where('size_id', $size_id);
			$this->mssql->update('income_nut');
			/* Update `stock_add_status` - End */
		
		}
		
	}
	
	public function get_nut_test_result($incomeid=0, $chemicalid=0){
		$query = $this->mssql->where('income_id', $incomeid)
								->where('chemical_id', $chemicalid)
								->get('chemical_test_nut')->row_array();
		return $query;
	}
	/* Nut - End */
	
	/* Washer - Start */
	public function get_incomeinfo_washer_byid($incomeid=0){
		$query = $this->mssql->where('income_washer.income_id', $incomeid)
								
								->get('income_washer')->row_array();
		return $query;
	}
	
	public function get_chemical_washer($productid=0){
		$query = $this->mssql->where('product_chemical_washer.product_id', $productid)
								->get('product_chemical_washer')->result_array();
		return $query;
	}
	
	public function check_chemical_test_washer_by_incomeid($incomeid=0){
		$query = $this->mssql->where('income_id', $incomeid)
							->count_all_results('chemical_test_washer');
		return $query;
	}
	
	public function washer_test_add($incomeid=0){
		$this->mssql->where('income_id', @$incomeid);
		$this->mssql->delete('chemical_test_washer');
		
		$this->mssql->where('income_id', @$incomeid);
		$this->mssql->delete('chemical_final_washer');
		
		foreach($this->input->post('chemical_id') as $key=>$value){
			$aData = array(
				'income_id' => $incomeid,
				'chemical_id' => $value,
				'chemical_value' => $_POST['chemical_value'][$key],
				'test_value' => $_POST['test_value'][$key],
				'test_createdtime' => date("Y-m-d H:i:s"),
				'test_createdip' => $this->input->ip_address()
			);
			
			
			$this->mssql->insert('chemical_test_washer', $aData);
			//$this->mssql->insert('chemical_final_washer', $aData);
	
		}
			
	}
	
	public function get_washerthicknessinfo_byid($incomeid=0){
		$query = $this->mssql->where('income_id', $incomeid)
								->get('thickness_test_washer')->row_array();
		return $query;
	}
	
	public function washer_thickness_add($incomeid=0){
			
			$this->mssql->where('income_id', @$incomeid);
			$this->mssql->delete('thickness_test_washer');
			
			$this->mssql->where('income_id', @$incomeid);
			$this->mssql->delete('thickness_test_washer');
		
			$tData = array(
				'income_id' => $incomeid,
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'thickness_createdtime' => date("Y-m-d H:i:s"),
				'thickness_createdip' => $this->input->ip_address()
			);
			$this->mssql->insert('thickness_test_washer', $tData);
			$this->mssql->insert('thickness_final_washer', $tData);
		
		
	}
	
	public function update_income_status_washer($incomeid=0)
	{
		$this->mssql->set("test_status",$_POST['test_status']);
		$this->mssql->where("income_id",$incomeid);
		$this->mssql->update("income_washer");
	}
	
	public function update_product_washer_qty($size_id=0,$qty=0, $incomeid=0, $test_status=0){
		
		$incomeinfo = $this->get_incomeinfo_washer_byid($incomeid);
		
		if($incomeinfo['stock_add_status']==0 && $test_status==2){
		
			$row = $this->mssql->where('product_size_washer.size_id', $size_id)
									
									->get('product_size_washer')->row_array();
			$old_qty = $row['size_qty'];
			
			$total = $old_qty+$qty;
			
			$this->mssql->set("size_qty",$total);
			$this->mssql->where("size_id",$size_id);
			$this->mssql->update("product_size_washer");
			
			/* Update `stock_add_status` - Start */
			$this->mssql->set('stock_add_status',1);
			$this->mssql->where('income_id', $incomeid);
			$this->mssql->where('size_id', $size_id);
			$this->mssql->update('income_washer');
			/* Update `stock_add_status` - End */
		}
		
	}
	
	public function get_washer_test_result($incomeid=0, $chemicalid=0){
		$query = $this->mssql->where('income_id', $incomeid)
								->where('chemical_id', $chemicalid)
								->get('chemical_test_washer')->row_array();
		return $query;
	}
	/* Washer - End */
	
	/* Spring - Start */
	public function get_incomeinfo_spring_byid($incomeid=0){
		$query = $this->mssql->where('income_spring.income_id', $incomeid)
								
								->get('income_spring')->row_array();
		return $query;
	}
	
	public function get_chemical_spring($productid=0){
		$query = $this->mssql->where('product_chemical_spring.product_id', $productid)
								->get('product_chemical_spring')->result_array();
		return $query;
	}
	
	public function check_chemical_test_spring_by_incomeid($incomeid=0){
		$query = $this->mssql->where('income_id', $incomeid)
							->count_all_results('chemical_test_spring');
		return $query;
	}
	
	public function spring_test_add($incomeid=0){
		$this->mssql->where('income_id', @$incomeid);
		$this->mssql->delete('chemical_test_spring');
		
		$this->mssql->where('income_id', @$incomeid);
		$this->mssql->delete('chemical_final_spring');
		
		foreach($this->input->post('chemical_id') as $key=>$value){
			$aData = array(
				'income_id' => $incomeid,
				'chemical_id' => $value,
				'chemical_value' => $_POST['chemical_value'][$key],
				'test_value' => $_POST['test_value'][$key],
				'test_createdtime' => date("Y-m-d H:i:s"),
				'test_createdip' => $this->input->ip_address()
			);
			
			
			$this->mssql->insert('chemical_test_spring', $aData);
			//$this->mssql->insert('chemical_final_spring', $aData);
	
		}
			
	}
	
	public function get_springthicknessinfo_byid($incomeid=0){
		$query = $this->mssql->where('income_id', $incomeid)
								->get('thickness_test_spring')->row_array();
		return $query;
	}
	
	public function spring_thickness_add($incomeid=0){
			
			$this->mssql->where('income_id', @$incomeid);
			$this->mssql->delete('thickness_test_spring');
			
			$this->mssql->where('income_id', @$incomeid);
			$this->mssql->delete('thickness_test_spring');
		
			$tData = array(
				'income_id' => $incomeid,
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'thickness_createdtime' => date("Y-m-d H:i:s"),
				'thickness_createdip' => $this->input->ip_address()
			);
			$this->mssql->insert('thickness_test_spring', $tData);
			$this->mssql->insert('thickness_final_spring', $tData);
		
		
	}
	
	public function update_income_status_spring($incomeid=0)
	{
		$this->mssql->set("test_status",$_POST['test_status']);
		$this->mssql->where("income_id",$incomeid);
		$this->mssql->update("income_spring");
	}
	
	public function update_product_spring_qty($size_id=0,$qty=0){
		$row = $this->mssql->where('product_size_spring.size_id', $size_id)
								
								->get('product_size_spring')->row_array();
		$old_qty = $row['size_qty'];
		
		$total = $old_qty+$qty;
		
		$this->mssql->set("size_qty",$total);
		$this->mssql->where("size_id",$size_id);
		$this->mssql->update("product_size_spring");
		
	}
	
	public function get_spring_test_result($incomeid=0, $chemicalid=0){
		$query = $this->mssql->where('income_id', $incomeid)
								->where('chemical_id', $chemicalid)
								->get('chemical_test_spring')->row_array();
		return $query;
	}
	/* spring - End */
	
	/* Taper - Start */
	public function get_incomeinfo_taper_byid($incomeid=0){
		$query = $this->mssql->where('income_taper.income_id', $incomeid)
								
								->get('income_taper')->row_array();
		return $query;
	}
	
	public function get_chemical_taper($productid=0){
		$query = $this->mssql->where('product_chemical_taper.product_id', $productid)
								->get('product_chemical_taper')->result_array();
		return $query;
	}
	
	public function check_chemical_test_taper_by_incomeid($incomeid=0){
		$query = $this->mssql->where('income_id', $incomeid)
							->count_all_results('chemical_test_taper');
		return $query;
	}
	
	public function taper_test_add($incomeid=0){
		$this->mssql->where('income_id', @$incomeid);
		$this->mssql->delete('chemical_test_taper');
		
		$this->mssql->where('income_id', @$incomeid);
		$this->mssql->delete('chemical_final_taper');
		
		foreach($this->input->post('chemical_id') as $key=>$value){
			$aData = array(
				'income_id' => $incomeid,
				'chemical_id' => $value,
				'chemical_value' => $_POST['chemical_value'][$key],
				'test_value' => $_POST['test_value'][$key],
				'test_createdtime' => date("Y-m-d H:i:s"),
				'test_createdip' => $this->input->ip_address()
			);
			
			
			$this->mssql->insert('chemical_test_taper', $aData);
			//$this->mssql->insert('chemical_final_taper', $aData);
	
		}
			
	}
	
	public function get_taperthicknessinfo_byid($incomeid=0){
		$query = $this->mssql->where('income_id', $incomeid)
								->get('thickness_test_taper')->row_array();
		return $query;
	}
	
	public function taper_thickness_add($incomeid=0){
			
			$this->mssql->where('income_id', @$incomeid);
			$this->mssql->delete('thickness_test_taper');
			
			$this->mssql->where('income_id', @$incomeid);
			$this->mssql->delete('thickness_test_taper');
		
			$tData = array(
				'income_id' => $incomeid,
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'thickness_createdtime' => date("Y-m-d H:i:s"),
				'thickness_createdip' => $this->input->ip_address()
			);
			$this->mssql->insert('thickness_test_taper', $tData);
			$this->mssql->insert('thickness_final_taper', $tData);
		
		
	}
	
	public function update_income_status_taper($incomeid=0)
	{
		$this->mssql->set("test_status",$_POST['test_status']);
		$this->mssql->where("income_id",$incomeid);
		$this->mssql->update("income_taper");
	}
	
	public function update_product_taper_qty($size_id=0,$qty=0){
		$row = $this->mssql->where('product_size_taper.size_id', $size_id)
								
								->get('product_size_taper')->row_array();
		$old_qty = $row['size_qty'];
		
		$total = $old_qty+$qty;
		
		$this->mssql->set("size_qty",$total);
		$this->mssql->where("size_id",$size_id);
		$this->mssql->update("product_size_taper");
		
	}
	
	public function get_taper_test_result($incomeid=0, $chemicalid=0){
		$query = $this->mssql->where('income_id', $incomeid)
								->where('chemical_id', $chemicalid)
								->get('chemical_test_taper')->row_array();
		return $query;
	}
	/* Taper - End */
	
	/* Stud - Start */
	public function get_incomeinfo_stud_byid($incomeid=0){
		$query = $this->mssql->where('income_stud.income_id', $incomeid)
								
								->get('income_stud')->row_array();
		return $query;
	}
	
	public function get_chemical_stud($productid=0){
		$query = $this->mssql->where('product_chemical_stud.product_id', $productid)
								->get('product_chemical_stud')->result_array();
		return $query;
	}
	
	public function check_chemical_test_stud_by_incomeid($incomeid=0){
		$query = $this->mssql->where('income_id', $incomeid)
							->count_all_results('chemical_test_stud');
		return $query;
	}
	
	public function stud_test_add($incomeid=0){
		$this->mssql->where('income_id', @$incomeid);
		$this->mssql->delete('chemical_test_stud');
		
		$this->mssql->where('income_id', @$incomeid);
		$this->mssql->delete('chemical_final_stud');
		
		foreach($this->input->post('chemical_id') as $key=>$value){
			$aData = array(
				'income_id' => $incomeid,
				'chemical_id' => $value,
				'chemical_value' => $_POST['chemical_value'][$key],
				'test_value' => $_POST['test_value'][$key],
				'test_createdtime' => date("Y-m-d H:i:s"),
				'test_createdip' => $this->input->ip_address()
			);
			
			
			$this->mssql->insert('chemical_test_stud', $aData);
			//$this->mssql->insert('chemical_final_stud', $aData);
	
		}
			
	}
	
	public function get_studthicknessinfo_byid($incomeid=0){
		$query = $this->mssql->where('income_id', $incomeid)
								->get('thickness_test_stud')->row_array();
		return $query;
	}
	
	public function stud_thickness_add($incomeid=0){
			
			$this->mssql->where('income_id', @$incomeid);
			$this->mssql->delete('thickness_test_stud');
			
			$this->mssql->where('income_id', @$incomeid);
			$this->mssql->delete('thickness_test_stud');
		
			$tData = array(
				'income_id' => $incomeid,
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'thickness_createdtime' => date("Y-m-d H:i:s"),
				'thickness_createdip' => $this->input->ip_address()
			);
			$this->mssql->insert('thickness_test_stud', $tData);
			$this->mssql->insert('thickness_final_stud', $tData);
		
		
	}
	
	public function update_income_status_stud($incomeid=0)
	{
		$this->mssql->set("test_status",$_POST['test_status']);
		$this->mssql->where("income_id",$incomeid);
		$this->mssql->update("income_stud");
	}
	
	public function update_product_stud_qty($size_id=0,$qty=0){
		$row = $this->mssql->where('product_size_stud.size_id', $size_id)
								
								->get('product_size_stud')->row_array();
		$old_qty = $row['size_qty'];
		
		$total = $old_qty+$qty;
		
		$this->mssql->set("size_qty",$total);
		$this->mssql->where("size_id",$size_id);
		$this->mssql->update("product_size_stud");
		
	}
	
	public function get_stud_test_result($incomeid=0, $chemicalid=0){
		$query = $this->mssql->where('income_id', $incomeid)
								->where('chemical_id', $chemicalid)
								->get('chemical_test_stud')->row_array();
		return $query;
	}
	/* Stud - End */
	
	public function get_thickness_value_byid($thicknessid=0){
		$query = $this->mssql->where('id', $thicknessid)
								->get('thickness_galvanizes')->row_array();
		return $query;
	}
	
}