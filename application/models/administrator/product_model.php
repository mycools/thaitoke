<?php
class Product_model extends CI_Model{
	function __construct(){
		parent::__construct();
		
		$this->mssql = $this->load->database("mssql", true);
	}
	
	
	public function size_detail($size_id){
		$query = $this->mssql->where('size_id', $size_id)
							
							->limit(1)
							->get('product_size_bolt')->row_array();
		return $query;
		
	}
	
	public function size_detail_nut($size_id){
		$query = $this->mssql->where('size_id', $size_id)
							
							->limit(1)
							->get('product_size_nut')->row_array();
		return $query;
		
	}
	
	public function size_detail_washer($size_id){
		$query = $this->mssql->where('size_id', $size_id)
							
							->limit(1)
							->get('product_size_washer')->row_array();
		return $query;
		
	}
	
	public function size_detail_spring($size_id){
		$query = $this->mssql->where('size_id', $size_id)
							
							->limit(1)
							->get('product_size_spring')->row_array();
		return $query;
		
	}
	
	public function size_detail_taper($size_id){
		$query = $this->mssql->where('size_id', $size_id)
							
							->limit(1)
							->get('product_size_taper')->row_array();
		return $query;
		
	}
	
	public function size_detail_stud($size_id){
		$query = $this->mssql->where('size_id', $size_id)
							
							->limit(1)
							->get('product_size_stud')->row_array();
		return $query;
		
	}
	
	public function stock_edit($size_id){
		
		$old = $this->input->post('qty_old');
		$new = $this->input->post('qty');
		
		$total =  $old+$new ;
		
		
		$this->mssql->set("size_qty",$total);
		
		$this->mssql->where('size_id', $size_id);
		$this->mssql->update('product_size_bolt');
		
		$bData = array(
			'qty' => $this->input->post('qty'),
			'qty_old' => $this->input->post('qty_old'),
			'remark' => $this->input->post('remark'),
			'size_id' => $size_id,
			'createdate' => date("Y-m-d H:i:s")
		);
		
		$this->mssql->insert('log_stock_bolt', $bData);
		
		
	}
	
	public function stock_edit_nut($size_id){
		
		$old = $this->input->post('qty_old');
		$new = $this->input->post('qty');
		
		$total =  $old+$new ;
		
		
		$this->mssql->set("size_qty",$total);
		
		$this->mssql->where('size_id', $size_id);
		$this->mssql->update('product_size_nut');
		
		$bData = array(
			'qty' => $this->input->post('qty'),
			'qty_old' => $this->input->post('qty_old'),
			'remark' => $this->input->post('remark'),
			'size_id' => $size_id,
			'createdate' => date("Y-m-d H:i:s")
		);
		
		$this->mssql->insert('log_stock_nut', $bData);
		
		
	}
	
	public function stock_edit_washer($size_id){
		
		$old = $this->input->post('qty_old');
		$new = $this->input->post('qty');
		
		$total =  $old+$new ;
		
		
		$this->mssql->set("size_qty",$total);
		
		$this->mssql->where('size_id', $size_id);
		$this->mssql->update('product_size_washer');
		
		$bData = array(
			'qty' => $this->input->post('qty'),
			'qty_old' => $this->input->post('qty_old'),
			'remark' => $this->input->post('remark'),
			'size_id' => $size_id,
			'createdate' => date("Y-m-d H:i:s")
		);
		
		$this->mssql->insert('log_stock_washer', $bData);
		
		
	}
	
	public function stock_edit_spring($size_id){
		
		$old = $this->input->post('qty_old');
		$new = $this->input->post('qty');
		
		$total =  $old+$new ;
		
		
		$this->mssql->set("size_qty",$total);
		
		$this->mssql->where('size_id', $size_id);
		$this->mssql->update('product_size_spring');
		
		$bData = array(
			'qty' => $this->input->post('qty'),
			'qty_old' => $this->input->post('qty_old'),
			'remark' => $this->input->post('remark'),
			'size_id' => $size_id,
			'createdate' => date("Y-m-d H:i:s")
		);
		
		$this->mssql->insert('log_stock_spring', $bData);
		
		
	}
	
	public function stock_edit_taper($size_id){
		
		$old = $this->input->post('qty_old');
		$new = $this->input->post('qty');
		
		$total =  $old+$new ;
		
		
		$this->mssql->set("size_qty",$total);
		
		$this->mssql->where('size_id', $size_id);
		$this->mssql->update('product_size_taper');
		
		$bData = array(
			'qty' => $this->input->post('qty'),
			'qty_old' => $this->input->post('qty_old'),
			'remark' => $this->input->post('remark'),
			'size_id' => $size_id,
			'createdate' => date("Y-m-d H:i:s")
		);
		
		$this->mssql->insert('log_stock_taper', $bData);
		
		
	}
	
	public function stock_edit_stud($size_id){
		
		$old = $this->input->post('qty_old');
		$new = $this->input->post('qty');
		
		$total =  $old+$new ;
		
		
		$this->mssql->set("size_qty",$total);
		
		$this->mssql->where('size_id', $size_id);
		$this->mssql->update('product_size_stud');
		
		$bData = array(
			'qty' => $this->input->post('qty'),
			'qty_old' => $this->input->post('qty_old'),
			'remark' => $this->input->post('remark'),
			'size_id' => $size_id,
			'createdate' => date("Y-m-d H:i:s")
		);
		
		$this->mssql->insert('log_stock_stud', $bData);
		
		
	}
	
	/* Bolt - Start */
	public function bolt_dataTable($limit=25,$offset=0){
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("product_name",$q);
			$this->mssql->or_like("product_type",$q);
		}
		//exit(','.$offset);
		$query = $this->mssql->where('product_status !=','discard')
					->order_by('product_createdtime','desc')
					->limit($limit,$offset)
					->get('product_bolt')->result_array();
					
		return $query;
	}
	public function bolt_count()
	{
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("product_name",$q);
			$this->mssql->or_like("product_type",$q);
		}
		$query = $this->mssql->where('product_status !=','discard')
					->order_by('product_createdtime','desc')
					->get('product_bolt')->num_rows();
		return $query;
	}
	public function bolt_getinfo($productid=0){
		$query = $this->mssql->where('product_id', $productid)
								->get('product_bolt')->row_array();
		return $query;
	}
	
	public function bolt_add(){
		
		$this->load->model("mobile_upload");
		$this->mobile_upload->set_upload_path("public/uploads/product/bolt");
		if($this->mobile_upload->upload("product_image")===true){
			$res_data = $this->mobile_upload->data();
			$filename = $res_data['file_name'];
		}else{
			$filename = '';
		}
		
		$aData = array(
			'product_image' => $filename,
			'product_name' => $this->input->post('product_name'),
			'product_standard' => $this->input->post('product_standard'),
			'product_initial' => $this->input->post('product_initial'),
			'certificate_id' => $this->input->post('certificate_id'),
			'product_type' => $this->input->post('product_type'),
			'product_iso' => $this->input->post('product_iso'),
			'product_part_name' => $this->input->post('product_part_name'),
			'product_status' => 'approved',
			'product_createdtime' => date("Y-m-d H:i:s"),
			'product_createdip' => $this->input->ip_address()
		);
		
		$this->mssql->insert('product_bolt', $aData);
		
	}
	
	public function bolt_edit($productid=0){
		
		$productinfo = $this->bolt_getinfo($productid);
		
		$this->load->model("mobile_upload");
		$this->mobile_upload->set_upload_path("public/uploads/product/bolt");
		
		if($_FILES['product_image']['tmp_name']!=''){
			if($this->mobile_upload->upload("product_image")){
				unlink('public/uploads/product/bolt/'.$productinfo['product_image']);
				$res_data = $this->mobile_upload->data();
				$filename = $res_data['file_name'];
			}else{
				echo $this->mobile_upload->display_error();
				$filename = $productinfo['product_image'];
			}
		}else{
			$filename = $productinfo['product_image'];
		}
		
		$aData = array(
			'product_image' => $filename,
			'product_name' => $this->input->post('product_name'),
			'product_standard' => $this->input->post('product_standard'),
			'product_iso' => $this->input->post('product_iso'),
			'product_part_name' => $this->input->post('product_part_name'),
			'product_initial' => $this->input->post('product_initial'),
			'certificate_id' => $this->input->post('certificate_id'),
			'product_type' => $this->input->post('product_type'),
			'product_status' => 'approved',
			'product_updatedtime' => date("Y-m-d H:i:s"),
			'product_updatedip' => $this->input->ip_address()
		);
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->update('product_bolt', $aData);
	}
	
	public function bolt_delete($productid=0){
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_bolt');
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_size_bolt');
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_standard_bolt');
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_chemical_bolt');
	}
	
	public function cer_size($cerid=0){
		$query = $this->mssql->where('certificate_id', $cerid)
							->where('size_status','1')
							->order_by('size_id','asc')
							->get('certificate_size')->result_array();
		return $query;
	}
	
	public function cer_size_foredit($sizeid=0){
		$query = $this->mssql->where('size_id', $sizeid)
							->get('product_cer_size_bolt')->result_array();
		return $query;
	}
	
	public function get_sizebolt($sizeid=0){
		$query = $this->mssql->where('size_id', $sizeid)
							->get('product_size_bolt')->row_array();
		return $query;
	}
	
	public function bolt_size_dataTable($productid=0,$limit=25,$offset=0){
		$q = $this->input->get("q");
		if(trim($q)){ 
			$this->mssql->where("(size_m like '%{$q}%' or size_p like '%{$q}%' or size_length like '%{$q}%' or size_qty like '%{$q}%')",false,false);
			
		}
		$query = $this->mssql->where('product_id', $productid)
							->where('size_status !=','discard')
							->order_by('size_createdtime','desc')
							->limit($limit,$offset)
							->get('product_size_bolt')->result_array();
		return $query;
	}
	
	public function bolt_size_count($productid=0)
	{
		$q = $this->input->get("q");
		if(trim($q)){ 
			$this->mssql->where("(size_m like '%{$q}%' or size_p like '%{$q}%' or size_length like '%{$q}%' or size_qty like '%{$q}%')",false,false);
			
		}
		$query = $this->mssql->where('product_id', $productid)
							->where('size_status !=','discard')
							->order_by('size_createdtime','desc')
							->get('product_size_bolt')->num_rows();
		return $query;
	}
	
	
	public function bolt_size_add($productid=0){
		$aData = array(
			'product_id' => $productid,
			'size_m' => $this->input->post('size_m'),
			'size_p' => $this->input->post('size_p'),
			'size_length' => $this->input->post('size_length'),
			'size_item' => $this->input->post('size_item'),
			'size_status' => 'approved',
			'size_createdtime' => date("Y-m-d H:i:s"),
			'size_createdip' => $this->input->ip_address()
		);
		
		$this->mssql->insert('product_size_bolt', $aData);
		$size_id = $this->mssql->insert_id();
		
		foreach($this->input->post('size_description') as $key=>$value){
				if($value!=''){
					$bData = array(
						'size_id' => $size_id,
						'product_id' => $productid,
						'inspection_id' => $_POST['inspection_id'][$key],
						'ps_description' => $value,
						'ps_max' => $_POST['ps_max'][$key],
						'ps_max_unit' => $_POST['ps_max_unit'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'ps_min_unit' => $_POST['ps_min_unit'][$key],
						'ps_status' => 'approved',
						'ps_createdtime' => date("Y-m-d H:i:s"),
						'ps_createdip' => $this->input->ip_address()
					);
					
					$this->mssql->insert('product_cer_size_bolt', $bData);
				}
			}
			
			$cData = array(
			'size_id' => $size_id,
			'stock_createdtime' => date("Y-m-d H:i:s"),
			'stock_createdip' => $this->input->ip_address()
		);
		
		$this->mssql->insert('stock_bolt', $cData);

		
	}
	
	public function bolt_size_edit($productid=0, $sizeid=0){
		$aData = array(
			'size_m' => $this->input->post('size_m'),
			'size_p' => $this->input->post('size_p'),
			'size_length' => $this->input->post('size_length'),
			'size_item' => $this->input->post('size_item'),
			'size_updatedtime' => date("Y-m-d H:i:s"),
			'size_updatedip' => $this->input->ip_address()
		);
		
		$this->mssql->where('size_id', $sizeid);
		$this->mssql->update('product_size_bolt', $aData);
		
		foreach($this->input->post('size_description') as $key=>$value){
			if($value!=''){
				$bData = array(
					'inspection_id' => $_POST['inspection_id'][$key],
					'ps_max' => $_POST['ps_max'][$key],
					'ps_max_unit' => $_POST['ps_max_unit'][$key],
					'ps_min' => $_POST['ps_min'][$key],
					'ps_min_unit' => $_POST['ps_min_unit'][$key],
					'ps_updatedtime' => date("Y-m-d H:i:s"),
					'ps_updatedip' => $this->input->ip_address()
				);
				
				$this->mssql->where('ps_id', $_POST['ps_id'][$key]);
				$this->mssql->update('product_cer_size_bolt', $bData);
			}
		}
		
	}
	
	public function bolt_size_delete($productid=0){
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_size_bolt');
	}
	
	public function bolt_standardlist($productid=0){
		$query = $this->mssql->where('ps_status !=','discard')
								->where('product_id', $productid)
								->order_by('ps_id','asc')
								->get('product_standard_bolt')->result_array();
		return $query;
	}
	
	public function get_cer_std($cerid=0){
		$query = $this->mssql->where('certificate_id', $cerid)
							->where('standard_status','1')
							->order_by('standard_id','asc')
							->get('certificate_standard')->result_array();
		return $query;
	}
	
	public function get_cer_std_foredit($productid=0){
		$query = $this->mssql->where('product_id', $productid)
								->where('ps_status','approved')
								->order_by('ps_id','asc')
								->get('product_standard_bolt')->result_array();
		return $query;
	}
	
	public function bolt_standard_add($productid=0){
		if($this->input->post('standard_description')){
			$this->mssql->where('product_id', $productid);
			$this->mssql->delete('product_standard_bolt');
			foreach($this->input->post('standard_description') as $key=>$value){
				if($value!=''){
					$aData = array(
						'product_id' => $productid,
						'inspection_id' => $_POST['inspection_id'][$key],
						'standard_description' => $value,
						'ps_max' => $_POST['ps_max'][$key],
						'ps_max_unit' => $_POST['ps_max_unit'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'ps_min_unit' => $_POST['ps_min_unit'][$key],
						'ps_status' => 'approved',
						'ps_createdtime' => date("Y-m-d H:i:s"),
						'ps_createdip' => $this->input->ip_address()
					);
					
					$this->mssql->insert('product_standard_bolt', $aData);
				}
			}
		}
	}
	
	public function bolt_standard_edit($productid=0){
		if($this->input->post('standard_description')){
			foreach($this->input->post('standard_description') as $key=>$value){
				if($value!=''){
					$aData = array(
						'inspection_id' => $_POST['inspection_id'][$key],
						'ps_max' => $_POST['ps_max'][$key],
						'ps_max_unit' => $_POST['ps_max_unit'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'ps_min_unit' => $_POST['ps_min_unit'][$key],
						'ps_updatedtime' => date("Y-m-d H:i:s"),
						'ps_updatedip' => $this->input->ip_address()
					);
					$this->mssql->where('ps_id', $_POST['ps_id'][$key]);
					$this->mssql->update('product_standard_bolt', $aData);
				}
			}
		}
	}
	
	public function bolt_chemicallist($productid=0){
		$query = $this->mssql->where('product_id', $productid)
							->where('chemical_status !=','discard')
							->order_by('chemical_id','asc')
							->get('product_chemical_bolt')->result_array();
		return $query;
	}
	
	public function get_cer_chem($cerid=0){
		$query = $this->mssql->where('certificate_id', $cerid)
							->where('chemical_status','1')
							->order_by('chemical_id','asc')
							->get('certificate_chemical')->result_array();
		return $query;
	}
	
	public function get_cer_chem_foredit($productid=0){
		$query = $this->mssql->where('product_id', $productid)
							->where('chemical_status','approved')
							->order_by('chemical_id','asc')
							->get('product_chemical_bolt')->result_array();
		return $query;
	}
	
	public function bolt_chemical_add($productid=0){
		
		foreach($this->input->post('chemical_value') as $key=>$value){
				if($value!=''){
					$aData = array(
						'product_id' => $productid,
						'chemical_value' => $value,
						'ps_max' => $_POST['ps_max'][$key],
						
						'ps_min' => $_POST['ps_min'][$key],
						
						'chemical_status' => 'approved',
						'chemical_createdtime' => date("Y-m-d H:i:s"),
						'chemical_createdip' => $this->input->ip_address()
					);
					
					$this->mssql->insert('product_chemical_bolt', $aData);
				}
			}
		
		
	}
	
	public function bolt_chemical_edit($productid=0){
		
		foreach($this->input->post('chemical_value') as $key=>$value){
				if($value!=''){
					$aData = array(
						'ps_max' => $_POST['ps_max'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'chemical_updatedtime' => date("Y-m-d H:i:s"),
						'chemical_updatedip' => $this->input->ip_address()
					);
					
					$this->mssql->where('chemical_id', $_POST['chemical_id'][$key]);
					$this->mssql->update('product_chemical_bolt', $aData);
				}
			}
		
		
	}
	/* Bolt - End */
	
	/* Nut - Start */
	public function nut_dataTable($limit=25,$offset=0){
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("product_name",$q);
			$this->mssql->or_like("product_type",$q);
		}
		$query = $this->mssql->where('product_status !=','discard')
					->order_by('product_createdtime','desc')
					->limit($limit,$offset)
					->get('product_nut')->result_array();
		return $query;
	}
	public function nut_count($limit=25,$offset=0){
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("product_name",$q);
			$this->mssql->or_like("product_type",$q);
		}
		$query = $this->mssql->where('product_status !=','discard')
					->order_by('product_createdtime','desc')
					->limit($limit,$offset)
					->get('product_nut')->num_rows();
		return $query;
	}
	public function nut_getinfo($productid=0){
		$query = $this->mssql->where('product_id', $productid)
								->get('product_nut')->row_array();
		return $query;
	}
	
	public function nut_add(){
		
		$this->load->model("mobile_upload");
		$this->mobile_upload->set_upload_path("public/uploads/product/nut");
		if($this->mobile_upload->upload("product_image")===true){
			$res_data = $this->mobile_upload->data();
			$filename = $res_data['file_name'];
		}else{
			$filename = '';
		}
		
		$aData = array(
			'product_image' => $filename,
			'product_name' => $this->input->post('product_name'),
			'product_standard' => $this->input->post('product_standard'),
			'product_initial' => $this->input->post('product_initial'),
			'certificate_id' => $this->input->post('certificate_id'),
			'product_type' => $this->input->post('product_type'),
			'product_iso' => $this->input->post('product_iso'),
			'product_part_name' => $this->input->post('product_part_name'),
			'product_status' => 'approved',
			'product_createdtime' => date("Y-m-d H:i:s"),
			'product_createdip' => $this->input->ip_address()
		);
		
		$this->mssql->insert('product_nut', $aData);
		
	}
	
	public function nut_edit($productid=0){
		
		$productinfo = $this->nut_getinfo($productid);
		
		$this->load->model("mobile_upload");
		$this->mobile_upload->set_upload_path("public/uploads/product/nut");
		$filename = $productinfo['product_image'];
		
		if($_FILES['product_image']['tmp_name']!=''){
			if($this->mobile_upload->upload("product_image")){
				unlink('public/uploads/product/nut/'.$productinfo['product_image']);
				$res_data = $this->mobile_upload->data();
				$filename = $res_data['file_name'];
			}else{
				echo $this->mobile_upload->display_error();
				$filename = $productinfo['product_image'];
			}
		}else{
			$filename = $productinfo['product_image'];
		}
		
		$aData = array(
			'product_image' => $filename,
			'product_name' => $this->input->post('product_name'),
			'product_standard' => $this->input->post('product_standard'),
			'product_initial' => $this->input->post('product_initial'),
			'certificate_id' => $this->input->post('certificate_id'),
			'product_type' => $this->input->post('product_type'),
			'product_iso' => $this->input->post('product_iso'),
			'product_part_name' => $this->input->post('product_part_name'),
			'product_status' => 'approved',
			'product_updatedtime' => date("Y-m-d H:i:s"),
			'product_updatedip' => $this->input->ip_address()
		);
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->update('product_nut', $aData);
	}
	
	public function nut_delete($productid=0){
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_nut');
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_size_nut');
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_standard_nut');
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_chemical_nut');
	}
	
	public function get_nutsizeinfo_byid($sizeid=0){
		$query = $this->mssql->where('size_id', $sizeid)
								->get('product_size_nut')->row_array();
		return $query;
	}
	
	public function nut_cer_size($sizeid=0){
		$query = $this->mssql->where('size_id', $sizeid)
								->where('ps_status','approved')
								->order_by('ps_id','asc')
								->get('product_cer_size_nut')->result_array();
		return $query;
	}
	
	public function nut_size_dataTable($productid=0,$limit=25,$offset=0){
		$q = $this->input->get("q");
		if(trim($q)){ 
			$this->mssql->where("(size_m like '%{$q}%' or size_p like '%{$q}%' or size_length like '%{$q}%' or size_qty like '%{$q}%')",false,false);
			
		}
		$query = $this->mssql->where('product_id', $productid)
							->where('size_status !=','discard')
							->order_by('size_createdtime','desc')
							->limit($limit,$offset)
							->get('product_size_nut')->result_array();
		return $query;
	}
	
	public function nut_size_count($productid=0)
	{
		$q = $this->input->get("q");
		if(trim($q)){ 
			$this->mssql->where("(size_m like '%{$q}%' or size_p like '%{$q}%' or size_length like '%{$q}%' or size_qty like '%{$q}%')",false,false);
			
		}
		$query = $this->mssql->where('product_id', $productid)
							->where('size_status !=','discard')
							->order_by('size_createdtime','desc')
							->get('product_size_nut')->num_rows();
		return $query;
	}
	
	
	public function nut_size_add($productid=0){
		$aData = array(
			'product_id' => $productid,
			'size_m' => $this->input->post('size_m'),
			'size_p' => $this->input->post('size_p'),
			'size_length' => $this->input->post('size_length'),
			'size_status' => 'approved',
			'size_createdtime' => date("Y-m-d H:i:s"),
			'size_createdip' => $this->input->ip_address()
		);
		
		$this->mssql->insert('product_size_nut', $aData);
		$size_id = $this->mssql->insert_id();
		
		foreach($this->input->post('size_description') as $key=>$value){
				if($value!=''){
					$bData = array(
						'size_id' => $size_id,
						'product_id' => $productid,
						'inspection_id' => $_POST['inspection_id'][$key],
						'ps_description' => $value,
						'ps_max' => $_POST['ps_max'][$key],
						'ps_max_unit' => $_POST['ps_max_unit'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'ps_min_unit' => $_POST['ps_min_unit'][$key],
						'ps_status' => 'approved',
						'ps_createdtime' => date("Y-m-d H:i:s"),
						'ps_createdip' => $this->input->ip_address()
					);
					
					$this->mssql->insert('product_cer_size_nut', $bData);
				}
			}
			
			$cData = array(
			'size_id' => $size_id,
			'stock_createdtime' => date("Y-m-d H:i:s"),
			'stock_createdip' => $this->input->ip_address()
		);
		
		$this->mssql->insert('stock_nut', $cData);

		
	}
	
	public function nut_size_edit($sizeid=0){
		$aData = array(
			'size_m' => $this->input->post('size_m'),
			'size_p' => $this->input->post('size_p'),
			'size_updatedtime' => date("Y-m-d H:i:s"),
			'size_updatedip' => $this->input->ip_address()
		);
		
		$this->mssql->where('size_id', $sizeid);
		$this->mssql->update('product_size_nut', $aData);
		
		foreach($this->input->post('size_description') as $key=>$value){
			if($value!=''){
				$bData = array(
					'inspection_id' => $_POST['inspection_id'][$key],
					'ps_max' => $_POST['ps_max'][$key],
					'ps_max_unit' => $_POST['ps_max_unit'][$key],
					'ps_min' => $_POST['ps_min'][$key],
					'ps_min_unit' => $_POST['ps_min_unit'][$key],
					'ps_updatedtime' => date("Y-m-d H:i:s"),
					'ps_updatedip' => $this->input->ip_address()
				);
				
				$this->mssql->where('ps_id', $_POST['ps_id'][$key]);
				$this->mssql->update('product_cer_size_nut', $bData);
			}
		}
	}
	
	public function nut_size_delete($productid=0){
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_size_nut');
	}
	
	public function nut_standardlist($productid=0){
		$query = $this->mssql->where('ps_status !=','discard')
								->where('product_id', $productid)
								->order_by('ps_id','asc')
								->get('product_standard_nut')->result_array();
		return $query;
	}
	
	public function nut_get_cer_std_foredit($productid=0){
		$query = $this->mssql->where('product_id', $productid)
								->where('ps_status','approved')
								->order_by('ps_id','asc')
								->get('product_standard_nut')->result_array();
		return $query;
	}
	
	public function nut_standard_add($productid=0){
		if($this->input->post('standard_description')){
			$this->mssql->where('product_id', $productid);
			$this->mssql->delete('product_standard_nut');
			foreach($this->input->post('standard_description') as $key=>$value){
				if($value!=''){
					$aData = array(
						'product_id' => $productid,
						'inspection_id' => $_POST['inspection_id'][$key],
						'standard_description' => $value,
						'ps_max' => $_POST['ps_max'][$key],
						'ps_max_unit' => $_POST['ps_max_unit'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'ps_min_unit' => $_POST['ps_min_unit'][$key],
						'ps_status' => 'approved',
						'ps_createdtime' => date("Y-m-d H:i:s"),
						'ps_createdip' => $this->input->ip_address()
					);
					
					$this->mssql->insert('product_standard_nut', $aData);
				}
			}
		}
	}
	
	public function nut_standard_edit($productid=0){
		if($this->input->post('standard_description')){
			foreach($this->input->post('standard_description') as $key=>$value){
				if($value!=''){
					$aData = array(
						'inspection_id' => $_POST['inspection_id'][$key],
						'ps_max' => $_POST['ps_max'][$key],
						'ps_max_unit' => $_POST['ps_max_unit'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'ps_min_unit' => $_POST['ps_min_unit'][$key],
						'ps_updatedtime' => date("Y-m-d H:i:s"),
						'ps_updatedip' => $this->input->ip_address()
					);
					
					$this->mssql->where('ps_id', $_POST['ps_id'][$key]);
					$this->mssql->update('product_standard_nut', $aData);
				}
			}
		}
	}
	
	public function nut_chemicallist($productid=0){
		$query = $this->mssql->where('product_id', $productid)
							->where('chemical_status !=','discard')
							->order_by('chemical_id','asc')
							->get('product_chemical_nut')->result_array();
		return $query;
	}
	
	public function nut_get_cer_chem($productid=0){
		$query = $this->mssql->where('product_id', $productid)
								->where('chemical_status','approved')
								->get('product_chemical_nut')->result_array();
		return $query;
	}
	
	public function nut_chemical_add($productid=0){
		
		foreach($this->input->post('chemical_value') as $key=>$value){
				if($value!=''){
					$aData = array(
						'product_id' => $productid,
						'chemical_value' => $value,
						'ps_max' => $_POST['ps_max'][$key],
						
						'ps_min' => $_POST['ps_min'][$key],
						
						'chemical_status' => 'approved',
						'chemical_createdtime' => date("Y-m-d H:i:s"),
						'chemical_createdip' => $this->input->ip_address()
					);
					
					$this->mssql->insert('product_chemical_nut', $aData);
				}
			}
		
		
	}
	
	public function nut_chemical_edit($productid=0){
		
		foreach($this->input->post('chemical_value') as $key=>$value){
				if($value!=''){
					$aData = array(
						'ps_max' => $_POST['ps_max'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'chemical_updatedtime' => date("Y-m-d H:i:s"),
						'chemical_updatedip' => $this->input->ip_address()
					);
					
					$this->mssql->where('chemical_id', $_POST['chemical_id'][$key]);
					$this->mssql->update('product_chemical_nut', $aData);
				}
			}
		
		
	}
	/* Nut - End */
	
	/* Washer - Start */
	public function washer_dataTable($limit=25,$offset=0){
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("product_name",$q);
			$this->mssql->or_like("product_type",$q);
		}
		$query = $this->mssql->where('product_status !=','discard')
					->order_by('product_createdtime','desc')
					->limit($limit,$offset)
					->get('product_washer')->result_array();
		return $query;
	}
	public function washer_count($limit=25,$offset=0){
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("product_name",$q);
			$this->mssql->or_like("product_type",$q);
		}
		$query = $this->mssql->where('product_status !=','discard')
					->order_by('product_createdtime','desc')
					->limit($limit,$offset)
					->get('product_washer')->num_rows();
		return $query;
	}
	public function washer_getinfo($productid=0){
		$query = $this->mssql->where('product_id', $productid)
								->get('product_washer')->row_array();
		return $query;
	}
	
	public function washer_add(){
		
		$this->load->model("mobile_upload");
		$this->mobile_upload->set_upload_path("public/uploads/product/washer");
		if($this->mobile_upload->upload("product_image")===true){
			$res_data = $this->mobile_upload->data();
			$filename = $res_data['file_name'];
		}else{
			$filename = '';
		}
		
		$aData = array(
			'product_image' => $filename,
			'product_name' => $this->input->post('product_name'),
			'product_standard' => $this->input->post('product_standard'),
			'product_initial' => $this->input->post('product_initial'),
			'certificate_id' => $this->input->post('certificate_id'),
			'product_type' => $this->input->post('product_type'),
			'product_iso' => $this->input->post('product_iso'),
			'product_part_name' => $this->input->post('product_part_name'),
			'product_status' => 'approved',
			'product_createdtime' => date("Y-m-d H:i:s"),
			'product_createdip' => $this->input->ip_address()
		);
		
		$this->mssql->insert('product_washer', $aData);
		
	}
	
	public function washer_edit($productid=0){
		
		$productinfo = $this->washer_getinfo($productid);
		
		$this->load->model("mobile_upload");
		$this->mobile_upload->set_upload_path("public/uploads/product/washer");
		
		if($_FILES['product_image']['tmp_name']!=''){
			if($this->mobile_upload->upload("product_image")){
				unlink('public/uploads/product/washer/'.$productinfo['product_image']);
				$res_data = $this->mobile_upload->data();
				$filename = $res_data['file_name'];
			}else{
				echo $this->mobile_upload->display_error();
				$filename = $productinfo['product_image'];
			}
		}else{
			$filename = $productinfo['product_image'];
		}
		
		$aData = array(
			'product_image' => $filename,
			'product_name' => $this->input->post('product_name'),
			'product_standard' => $this->input->post('product_standard'),
			'product_initial' => $this->input->post('product_initial'),
			'certificate_id' => $this->input->post('certificate_id'),
			'product_type' => $this->input->post('product_type'),
			'product_iso' => $this->input->post('product_iso'),
			'product_part_name' => $this->input->post('product_part_name'),
			'product_status' => 'approved',
			'product_updatedtime' => date("Y-m-d H:i:s"),
			'product_updatedip' => $this->input->ip_address()
		);
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->update('product_washer', $aData);
	}
	
	public function washer_delete($productid=0){
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_washer');
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_size_washer');
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_standard_washer');
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_chemical_washer');
	}
	
	/*
public function cer_size($cerid=0){
		$query = $this->mssql->where('certificate_id', $cerid)
							->where('size_status','1')
							->order_by('size_createdtime','desc')
							->get('certificate_size')->result_array();
		return $query;
	}
*/
	
	public function washer_size_dataTable($productid=0,$limit=25,$offset=0){
		$q = $this->input->get("q");
		if(trim($q)){ 
			$this->mssql->where("(size_m like '%{$q}%' or size_p like '%{$q}%' or size_length like '%{$q}%' or size_qty like '%{$q}%')",false,false);
			
		}
		$query = $this->mssql->where('product_id', $productid)
							->where('size_status !=','discard')
							->order_by('size_createdtime','desc')
							->limit($limit,$offset)
							->get('product_size_washer')->result_array();
		return $query;
	}
	
	public function washer_size_count($productid=0)
	{
		$q = $this->input->get("q");
		if(trim($q)){ 
			$this->mssql->where("(size_m like '%{$q}%' or size_p like '%{$q}%' or size_length like '%{$q}%' or size_qty like '%{$q}%')",false,false);
			
		}
		$query = $this->mssql->where('product_id', $productid)
							->where('size_status !=','discard')
							->order_by('size_createdtime','desc')
							->get('product_size_washer')->num_rows();
		return $query;
	}
	
	
	public function washer_size_add($productid=0){
		$aData = array(
			'product_id' => $productid,
			'size_m' => $this->input->post('size_m'),
			'size_p' => $this->input->post('size_p'),
			'size_length' => $this->input->post('size_length'),
			'size_status' => 'approved',
			'size_createdtime' => date("Y-m-d H:i:s"),
			'size_createdip' => $this->input->ip_address()
		);
		
		$this->mssql->insert('product_size_washer', $aData);
		$size_id = $this->mssql->insert_id();
		
		foreach($this->input->post('size_description') as $key=>$value){
				if($value!=''){
					$bData = array(
						'size_id' => $size_id,
						'product_id' => $productid,
						'inspection_id' => $_POST['inspection_id'][$key],
						'ps_description' => $value,
						'ps_max' => $_POST['ps_max'][$key],
						'ps_max_unit' => $_POST['ps_max_unit'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'ps_min_unit' => $_POST['ps_min_unit'][$key],
						'ps_status' => 'approved',
						'ps_createdtime' => date("Y-m-d H:i:s"),
						'ps_createdip' => $this->input->ip_address()
					);
					
					$this->mssql->insert('product_cer_size_washer', $bData);
				}
			}
			
			$cData = array(
			'size_id' => $size_id,
			'stock_createdtime' => date("Y-m-d H:i:s"),
			'stock_createdip' => $this->input->ip_address()
		);
		
		$this->mssql->insert('stock_washer', $cData);

		
	}
	
	public function washer_size_edit($sizeid=0){
		$aData = array(
			'size_m' => $this->input->post('size_m'),
			'size_p' => $this->input->post('size_p'),
			'size_updatedtime' => date("Y-m-d H:i:s"),
			'size_updatedip' => $this->input->ip_address()
		);
		
		$this->mssql->where('size_id', $sizeid);
		$this->mssql->update('product_size_washer', $aData);
		
		foreach($this->input->post('size_description') as $key=>$value){
			if($value!=''){
				$bData = array(
					'inspection_id' => $_POST['inspection_id'][$key],
					'ps_max' => $_POST['ps_max'][$key],
					'ps_max_unit' => $_POST['ps_max_unit'][$key],
					'ps_min' => $_POST['ps_min'][$key],
					'ps_min_unit' => $_POST['ps_min_unit'][$key],
					'ps_updatedtime' => date("Y-m-d H:i:s"),
					'ps_updatedip' => $this->input->ip_address()
				);
				
				$this->mssql->where('ps_id', $_POST['ps_id'][$key]);
				$this->mssql->update('product_cer_size_washer', $bData);
			}
		}
	}
	
	public function washer_size_delete($productid=0){
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_size_washer');
	}
	
	public function washer_standardlist($productid=0){
		$query = $this->mssql->where('ps_status !=','discard')
								->where('product_id', $productid)
								->order_by('ps_id','asc')
								->get('product_standard_washer')->result_array();
		return $query;
	}
	
	public function get_washer_sizeinfo($sizeid=0){
		$query = $this->mssql->where('size_id', $sizeid)
								->get('product_size_washer')->row_array();
		return $query;
	}
	
	public function washer_cer_size($sizeid=0){
		$query = $this->mssql->where('size_id', $sizeid)
								->where('ps_status','approved')
								->order_by('ps_id', 'asc')
								->get('product_cer_size_washer')->result_array();
		return $query;
	}
	
	public function washer_standard_add($productid=0){
		if($this->input->post('standard_description')){
			$this->mssql->where('product_id', $productid);
			$this->mssql->delete('product_standard_washer');
			foreach($this->input->post('standard_description') as $key=>$value){
				if($value!=''){
					$aData = array(
						'product_id' => $productid,
						'inspection_id' => $_POST['inspection_id'][$key],
						'standard_description' => $value,
						'ps_max' => $_POST['ps_max'][$key],
						'ps_max_unit' => $_POST['ps_max_unit'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'ps_min_unit' => $_POST['ps_min_unit'][$key],
						'ps_status' => 'approved',
						'ps_createdtime' => date("Y-m-d H:i:s"),
						'ps_createdip' => $this->input->ip_address()
					);
					
					$this->mssql->insert('product_standard_washer', $aData);
				}
			}
		}
	}
	
	public function washer_standard_edit($productid=0){
		if($this->input->post('standard_description')){
			foreach($this->input->post('standard_description') as $key=>$value){
				if($value!=''){
					$aData = array(
						'inspection_id' => $_POST['inspection_id'][$key],
						'ps_max' => $_POST['ps_max'][$key],
						'ps_max_unit' => $_POST['ps_max_unit'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'ps_min_unit' => $_POST['ps_min_unit'][$key],
						'ps_updatedtime' => date("Y-m-d H:i:s"),
						'ps_updatedip' => $this->input->ip_address()
					);
					
					$this->mssql->where('ps_id', $_POST['ps_id'][$key]);
					$this->mssql->update('product_standard_washer', $aData);
				}
			}
		}
	}
	
	public function washer_chemicallist($productid=0){
		$query = $this->mssql->where('product_id', $productid)
							->where('chemical_status !=','discard')
							->order_by('chemical_id','asc')
							->get('product_chemical_washer')->result_array();
		return $query;
	}
	
	public function washer_get_cer_std($productid=0){
		$query = $this->mssql->where('product_id', $productid)
								->get('product_standard_washer')->result_array();
		return $query;
	}
	
	public function washer_get_cer_chem($productid=0){
		$query = $this->mssql->where('product_id', $productid)
								->get('product_chemical_washer')->result_array();
		return $query;
	}
	
	public function washer_chemical_add($productid=0){
		
		foreach($this->input->post('chemical_value') as $key=>$value){
				if($value!=''){
					$aData = array(
						'product_id' => $productid,
						'chemical_value' => $value,
						'ps_max' => $_POST['ps_max'][$key],
						
						'ps_min' => $_POST['ps_min'][$key],
						
						'chemical_status' => 'approved',
						'chemical_createdtime' => date("Y-m-d H:i:s"),
						'chemical_createdip' => $this->input->ip_address()
					);
					
					$this->mssql->insert('product_chemical_washer', $aData);
				}
			}
		
		
	}
	
	public function washer_chemical_edit($productid=0){
		
		foreach($this->input->post('chemical_value') as $key=>$value){
				if($value!=''){
					$aData = array(
						'ps_max' => $_POST['ps_max'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'chemical_createdtime' => date("Y-m-d H:i:s"),
						'chemical_createdip' => $this->input->ip_address()
					);
					
					$this->mssql->where('chemical_id', $_POST['chemical_id'][$key]);
					$this->mssql->update('product_chemical_washer', $aData);
				}
			}
		
		
	}
	/* Washer - End */
	
	/* Spring - Start */
	public function spring_dataTable($limit=25,$offset=0){
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("product_name",$q);
			$this->mssql->or_like("product_type",$q);
		}
		$query = $this->mssql->where('product_status !=','discard')
					->order_by('product_createdtime','desc')
					->limit($limit,$offset)
					->get('product_spring')->result_array();
		return $query;
	}
	public function spring_count($limit=25,$offset=0){
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("product_name",$q);
			$this->mssql->or_like("product_type",$q);
		}
		$query = $this->mssql->where('product_status !=','discard')
					->order_by('product_createdtime','desc')
					->limit($limit,$offset)
					->get('product_spring')->num_rows();
		return $query;
	}
	public function spring_getinfo($productid=0){
		$query = $this->mssql->where('product_id', $productid)
								->get('product_spring')->row_array();
		return $query;
	}
	
	public function spring_add(){
		
		$this->load->model("mobile_upload");
		$this->mobile_upload->set_upload_path("public/uploads/product/spring");
		if($this->mobile_upload->upload("product_image")===true){
			$res_data = $this->mobile_upload->data();
			$filename = $res_data['file_name'];
		}else{
			$filename = '';
		}
		
		$aData = array(
			'product_image' => $filename,
			'product_name' => $this->input->post('product_name'),
			'product_standard' => $this->input->post('product_standard'),
			'product_initial' => $this->input->post('product_initial'),
			'certificate_id' => $this->input->post('certificate_id'),
			'product_type' => $this->input->post('product_type'),
			'product_status' => 'approved',
			'product_createdtime' => date("Y-m-d H:i:s"),
			'product_createdip' => $this->input->ip_address()
		);
		
		$this->mssql->insert('product_spring', $aData);
		
	}
	
	public function spring_edit($productid=0){
		
		$productinfo = $this->spring_getinfo($productid);
		
		$this->load->model("mobile_upload");
		$this->mobile_upload->set_upload_path("public/uploads/product/spring");
		
		if($_FILES['product_image']['tmp_name']!=''){
			if($this->mobile_upload->upload("product_image")){
				unlink('public/uploads/product/spring/'.$productinfo['product_image']);
				$res_data = $this->mobile_upload->data();
				$filename = $res_data['file_name'];
			}else{
				echo $this->mobile_upload->display_error();
				$filename = $productinfo['product_image'];
			}
		}else{
			$filename = $productinfo['product_image'];
		}
		
		$aData = array(
			'product_image' => $filename,
			'product_name' => $this->input->post('product_name'),
			'product_standard' => $this->input->post('product_standard'),
			'product_initial' => $this->input->post('product_initial'),
			'certificate_id' => $this->input->post('certificate_id'),
			'product_type' => $this->input->post('product_type'),
			'product_status' => 'approved',
			'product_updatedtime' => date("Y-m-d H:i:s"),
			'product_updatedip' => $this->input->ip_address()
		);
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->update('product_spring', $aData);
	}
	
	public function spring_delete($productid=0){
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_spring');
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_size_spring');
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_standard_spring');
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_chemical_spring');
	}
	
	/*
public function cer_size($cerid=0){
		$query = $this->mssql->where('certificate_id', $cerid)
							->where('size_status','1')
							->order_by('size_createdtime','desc')
							->get('certificate_size')->result_array();
		return $query;
	}
*/
	
	public function spring_size_dataTable($productid=0,$limit=25,$offset=0){
		$q = $this->input->get("q");
		if(trim($q)){ 
			$this->mssql->where("(size_m like '%{$q}%' or size_p like '%{$q}%' or size_length like '%{$q}%' or size_qty like '%{$q}%')",false,false);
			
		}
		$query = $this->mssql->where('product_id', $productid)
							->where('size_status !=','discard')
							->order_by('size_createdtime','desc')
							->limit($limit,$offset)
							->get('product_size_spring')->result_array();
		return $query;
	}
	
	public function spring_size_count($productid=0)
	{
		$q = $this->input->get("q");
		if(trim($q)){ 
			$this->mssql->where("(size_m like '%{$q}%' or size_p like '%{$q}%' or size_length like '%{$q}%' or size_qty like '%{$q}%')",false,false);
			
		}
		$query = $this->mssql->where('product_id', $productid)
							->where('size_status !=','discard')
							->order_by('size_createdtime','desc')
							->get('product_size_spring')->num_rows();
		return $query;
	}
	
	
	public function spring_size_add($productid=0){
		$aData = array(
			'product_id' => $productid,
			'size_m' => $this->input->post('size_m'),
			'size_p' => $this->input->post('size_p'),
			'size_length' => $this->input->post('size_length'),
			'size_status' => 'approved',
			'size_createdtime' => date("Y-m-d H:i:s"),
			'size_createdip' => $this->input->ip_address()
		);
		
		$this->mssql->insert('product_size_spring', $aData);
		$size_id = $this->mssql->insert_id();
		
		foreach($this->input->post('size_description') as $key=>$value){
				if($value!=''){
					$bData = array(
						'size_id' => $size_id,
						'product_id' => $productid,
						'inspection_id' => $_POST['inspection_id'][$key],
						'ps_description' => $value,
						'ps_max' => $_POST['ps_max'][$key],
						'ps_max_unit' => $_POST['ps_max_unit'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'ps_min_unit' => $_POST['ps_min_unit'][$key],
						'ps_status' => 'approved',
						'ps_createdtime' => date("Y-m-d H:i:s"),
						'ps_createdip' => $this->input->ip_address()
					);
					
					$this->mssql->insert('product_cer_size_spring', $bData);
				}
			}
			
			$cData = array(
			'size_id' => $size_id,
			'stock_createdtime' => date("Y-m-d H:i:s"),
			'stock_createdip' => $this->input->ip_address()
		);
		
		$this->mssql->insert('stock_spring', $cData);

		
	}
	
	public function spring_size_edit($sizeid=0){
		$aData = array(
			'size_m' => $this->input->post('size_m'),
			'size_p' => $this->input->post('size_p'),
			'size_updatedtime' => date("Y-m-d H:i:s"),
			'size_updatedip' => $this->input->ip_address()
		);
		
		$this->mssql->where('size_id', $sizeid);
		$this->mssql->update('product_size_spring', $aData);
		
		foreach($this->input->post('size_description') as $key=>$value){
			if($value!=''){
				$bData = array(
					'inspection_id' => $_POST['inspection_id'][$key],
					'ps_max' => $_POST['ps_max'][$key],
					'ps_max_unit' => $_POST['ps_max_unit'][$key],
					'ps_min' => $_POST['ps_min'][$key],
					'ps_min_unit' => $_POST['ps_min_unit'][$key],
					'ps_updatedtime' => date("Y-m-d H:i:s"),
					'ps_updatedip' => $this->input->ip_address()
				);
				
				$this->mssql->where('ps_id', $_POST['ps_id'][$key]);
				$this->mssql->update('product_cer_size_spring', $bData);
			}
		}
	}
	
	public function spring_size_delete($productid=0){
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_size_spring');
	}
	
	public function spring_standardlist($productid=0){
		$query = $this->mssql->where('ps_status !=','discard')
								->where('product_id', $productid)
								->order_by('ps_id','asc')
								->get('product_standard_spring')->result_array();
		return $query;
	}
	
	public function get_spring_sizeinfo($sizeid=0){
		$query = $this->mssql->where('size_id', $sizeid)
								->get('product_size_spring')->row_array();
		return $query;
	}
	
	public function spring_cer_size($sizeid=0){
		$query = $this->mssql->where('size_id', $sizeid)
								->where('ps_status','approved')
								->order_by('ps_id', 'asc')
								->get('product_cer_size_spring')->result_array();
		return $query;
	}
	
	public function spring_standard_add($productid=0){
		if($this->input->post('standard_description')){
			$this->mssql->where('product_id', $productid);
			$this->mssql->delete('product_standard_spring');
			foreach($this->input->post('standard_description') as $key=>$value){
				if($value!=''){
					$aData = array(
						'product_id' => $productid,
						'inspection_id' => $_POST['inspection_id'][$key],
						'standard_description' => $value,
						'ps_max' => $_POST['ps_max'][$key],
						'ps_max_unit' => $_POST['ps_max_unit'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'ps_min_unit' => $_POST['ps_min_unit'][$key],
						'ps_status' => 'approved',
						'ps_createdtime' => date("Y-m-d H:i:s"),
						'ps_createdip' => $this->input->ip_address()
					);
					
					$this->mssql->insert('product_standard_spring', $aData);
				}
			}
		}
	}
	
	public function spring_standard_edit($productid=0){
		if($this->input->post('standard_description')){
			foreach($this->input->post('standard_description') as $key=>$value){
				if($value!=''){
					$aData = array(
						'inspection_id' => $_POST['inspection_id'][$key],
						'ps_max' => $_POST['ps_max'][$key],
						'ps_max_unit' => $_POST['ps_max_unit'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'ps_min_unit' => $_POST['ps_min_unit'][$key],
						'ps_updatedtime' => date("Y-m-d H:i:s"),
						'ps_updatedip' => $this->input->ip_address()
					);
					
					$this->mssql->where('ps_id', $_POST['ps_id'][$key]);
					$this->mssql->update('product_standard_spring', $aData);
				}
			}
		}
	}
	
	public function spring_chemicallist($productid=0){
		$query = $this->mssql->where('product_id', $productid)
							->where('chemical_status !=','discard')
							->order_by('chemical_id','asc')
							->get('product_chemical_spring')->result_array();
		return $query;
	}
	
	public function spring_get_cer_std($productid=0){
		$query = $this->mssql->where('product_id', $productid)
								->get('product_standard_spring')->result_array();
		return $query;
	}
	
	public function spring_get_cer_chem($productid=0){
		$query = $this->mssql->where('product_id', $productid)
								->get('product_chemical_spring')->result_array();
		return $query;
	}
	
	public function spring_chemical_add($productid=0){
		
		foreach($this->input->post('chemical_value') as $key=>$value){
				if($value!=''){
					$aData = array(
						'product_id' => $productid,
						'chemical_value' => $value,
						'ps_max' => $_POST['ps_max'][$key],
						
						'ps_min' => $_POST['ps_min'][$key],
						
						'chemical_status' => 'approved',
						'chemical_createdtime' => date("Y-m-d H:i:s"),
						'chemical_createdip' => $this->input->ip_address()
					);
					
					$this->mssql->insert('product_chemical_spring', $aData);
				}
			}
		
		
	}
	
	public function spring_chemical_edit($productid=0){
		
		foreach($this->input->post('chemical_value') as $key=>$value){
				if($value!=''){
					$aData = array(
						'ps_max' => $_POST['ps_max'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'chemical_createdtime' => date("Y-m-d H:i:s"),
						'chemical_createdip' => $this->input->ip_address()
					);
					
					$this->mssql->where('chemical_id', $_POST['chemical_id'][$key]);
					$this->mssql->update('product_chemical_spring', $aData);
				}
			}
		
		
	}
	/* Spring - End */
	
	/* Taper - Start */
	public function taper_dataTable($limit=25,$offset=0){
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("product_name",$q);
			$this->mssql->or_like("product_type",$q);
		}
		$query = $this->mssql->where('product_status !=','discard')
					->order_by('product_createdtime','desc')
					->limit($limit,$offset)
					->get('product_taper')->result_array();
		return $query;
	}
	public function taper_count($limit=25,$offset=0){
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("product_name",$q);
			$this->mssql->or_like("product_type",$q);
		}
		$query = $this->mssql->where('product_status !=','discard')
					->order_by('product_createdtime','desc')
					->limit($limit,$offset)
					->get('product_taper')->num_rows();
		return $query;
	}
	public function taper_getinfo($productid=0){
		$query = $this->mssql->where('product_id', $productid)
								->get('product_taper')->row_array();
		return $query;
	}
	
	public function taper_add(){
		
		$this->load->model("mobile_upload");
		$this->mobile_upload->set_upload_path("public/uploads/product/taper");
		if($this->mobile_upload->upload("product_image")===true){
			$res_data = $this->mobile_upload->data();
			$filename = $res_data['file_name'];
		}else{
			$filename = '';
		}
		
		$aData = array(
			'product_image' => $filename,
			'product_name' => $this->input->post('product_name'),
			'product_standard' => $this->input->post('product_standard'),
			'product_initial' => $this->input->post('product_initial'),
			'certificate_id' => $this->input->post('certificate_id'),
			'product_type' => $this->input->post('product_type'),
			'product_status' => 'approved',
			'product_createdtime' => date("Y-m-d H:i:s"),
			'product_createdip' => $this->input->ip_address()
		);
		
		$this->mssql->insert('product_taper', $aData);
		
	}
	
	public function taper_edit($productid=0){
		
		$productinfo = $this->taper_getinfo($productid);
		
		$this->load->model("mobile_upload");
		$this->mobile_upload->set_upload_path("public/uploads/product/taper");
		
		if($_FILES['product_image']['tmp_name']!=''){
			if($this->mobile_upload->upload("product_image")){
				unlink('public/uploads/product/taper/'.$productinfo['product_image']);
				$res_data = $this->mobile_upload->data();
				$filename = $res_data['file_name'];
			}else{
				echo $this->mobile_upload->display_error();
				$filename = $productinfo['product_image'];
			}
		}else{
			$filename = $productinfo['product_image'];
		}
		
		$aData = array(
			'product_image' => $filename,
			'product_name' => $this->input->post('product_name'),
			'product_standard' => $this->input->post('product_standard'),
			'product_initial' => $this->input->post('product_initial'),
			'certificate_id' => $this->input->post('certificate_id'),
			'product_type' => $this->input->post('product_type'),
			'product_status' => 'approved',
			'product_updatedtime' => date("Y-m-d H:i:s"),
			'product_updatedip' => $this->input->ip_address()
		);
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->update('product_taper', $aData);
	}
	
	public function taper_delete($productid=0){
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_taper');
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_size_taper');
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_standard_taper');
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_chemical_taper');
	}
	
	/*
public function cer_size($cerid=0){
		$query = $this->mssql->where('certificate_id', $cerid)
							->where('size_status','1')
							->order_by('size_createdtime','desc')
							->get('certificate_size')->result_array();
		return $query;
	}
*/
	
	public function taper_size_dataTable($productid=0,$limit=25,$offset=0){
		$q = $this->input->get("q");
		if(trim($q)){ 
			$this->mssql->where("(size_m like '%{$q}%' or size_p like '%{$q}%' or size_length like '%{$q}%' or size_qty like '%{$q}%')",false,false);
			
		}
		$query = $this->mssql->where('product_id', $productid)
							->where('size_status !=','discard')
							->order_by('size_createdtime','desc')
							->limit($limit,$offset)
							->get('product_size_taper')->result_array();
		return $query;
	}
	
	public function taper_size_count($productid=0)
	{
		$q = $this->input->get("q");
		if(trim($q)){ 
			$this->mssql->where("(size_m like '%{$q}%' or size_p like '%{$q}%' or size_length like '%{$q}%' or size_qty like '%{$q}%')",false,false);
			
		}
		$query = $this->mssql->where('product_id', $productid)
							->where('size_status !=','discard')
							->order_by('size_createdtime','desc')
							->get('product_size_taper')->num_rows();
		return $query;
	}
	
	
	public function taper_size_add($productid=0){
		$aData = array(
			'product_id' => $productid,
			'size_m' => $this->input->post('size_m'),
			'size_p' => $this->input->post('size_p'),
			'size_length' => $this->input->post('size_length'),
			'size_status' => 'approved',
			'size_createdtime' => date("Y-m-d H:i:s"),
			'size_createdip' => $this->input->ip_address()
		);
		
		$this->mssql->insert('product_size_taper', $aData);
		$size_id = $this->mssql->insert_id();
		
		foreach($this->input->post('size_description') as $key=>$value){
				if($value!=''){
					$bData = array(
						'size_id' => $size_id,
						'product_id' => $productid,
						'inspection_id' => $_POST['inspection_id'][$key],
						'ps_description' => $value,
						'ps_max' => $_POST['ps_max'][$key],
						'ps_max_unit' => $_POST['ps_max_unit'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'ps_min_unit' => $_POST['ps_min_unit'][$key],
						'ps_status' => 'approved',
						'ps_createdtime' => date("Y-m-d H:i:s"),
						'ps_createdip' => $this->input->ip_address()
					);
					
					$this->mssql->insert('product_cer_size_taper', $bData);
				}
			}
			
			$cData = array(
			'size_id' => $size_id,
			'stock_createdtime' => date("Y-m-d H:i:s"),
			'stock_createdip' => $this->input->ip_address()
		);
		
		$this->mssql->insert('stock_taper', $cData);

		
	}
	
	public function taper_size_edit($sizeid=0){
		$aData = array(
			'size_m' => $this->input->post('size_m'),
			'size_p' => $this->input->post('size_p'),
			'size_updatedtime' => date("Y-m-d H:i:s"),
			'size_updatedip' => $this->input->ip_address()
		);
		
		$this->mssql->where('size_id', $sizeid);
		$this->mssql->update('product_size_taper', $aData);
		
		foreach($this->input->post('size_description') as $key=>$value){
			if($value!=''){
				$bData = array(
					'inspection_id' => $_POST['inspection_id'][$key],
					'ps_max' => $_POST['ps_max'][$key],
					'ps_max_unit' => $_POST['ps_max_unit'][$key],
					'ps_min' => $_POST['ps_min'][$key],
					'ps_min_unit' => $_POST['ps_min_unit'][$key],
					'ps_updatedtime' => date("Y-m-d H:i:s"),
					'ps_updatedip' => $this->input->ip_address()
				);
				
				$this->mssql->where('ps_id', $_POST['ps_id'][$key]);
				$this->mssql->update('product_cer_size_taper', $bData);
			}
		}
	}
	
	public function taper_size_delete($productid=0){
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_size_taper');
	}
	
	public function taper_standardlist($productid=0){
		$query = $this->mssql->where('ps_status !=','discard')
								->where('product_id', $productid)
								->order_by('ps_id','asc')
								->get('product_standard_taper')->result_array();
		return $query;
	}
	
	public function get_taper_sizeinfo($sizeid=0){
		$query = $this->mssql->where('size_id', $sizeid)
								->get('product_size_taper')->row_array();
		return $query;
	}
	
	public function taper_cer_size($sizeid=0){
		$query = $this->mssql->where('size_id', $sizeid)
								->where('ps_status','approved')
								->order_by('ps_id', 'asc')
								->get('product_cer_size_taper')->result_array();
		return $query;
	}
	
	public function taper_standard_add($productid=0){
		if($this->input->post('standard_description')){
			$this->mssql->where('product_id', $productid);
			$this->mssql->delete('product_standard_taper');
			foreach($this->input->post('standard_description') as $key=>$value){
				if($value!=''){
					$aData = array(
						'product_id' => $productid,
						'inspection_id' => $_POST['inspection_id'][$key],
						'standard_description' => $value,
						'ps_max' => $_POST['ps_max'][$key],
						'ps_max_unit' => $_POST['ps_max_unit'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'ps_min_unit' => $_POST['ps_min_unit'][$key],
						'ps_status' => 'approved',
						'ps_createdtime' => date("Y-m-d H:i:s"),
						'ps_createdip' => $this->input->ip_address()
					);
					
					$this->mssql->insert('product_standard_taper', $aData);
				}
			}
		}
	}
	
	public function taper_standard_edit($productid=0){
		if($this->input->post('standard_description')){
			foreach($this->input->post('standard_description') as $key=>$value){
				if($value!=''){
					$aData = array(
						'inspection_id' => $_POST['inspection_id'][$key],
						'ps_max' => $_POST['ps_max'][$key],
						'ps_max_unit' => $_POST['ps_max_unit'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'ps_min_unit' => $_POST['ps_min_unit'][$key],
						'ps_updatedtime' => date("Y-m-d H:i:s"),
						'ps_updatedip' => $this->input->ip_address()
					);
					
					$this->mssql->where('ps_id', $_POST['ps_id'][$key]);
					$this->mssql->update('product_standard_taper', $aData);
				}
			}
		}
	}
	
	public function taper_chemicallist($productid=0){
		$query = $this->mssql->where('product_id', $productid)
							->where('chemical_status !=','discard')
							->order_by('chemical_id','asc')
							->get('product_chemical_taper')->result_array();
		return $query;
	}
	
	public function taper_get_cer_std($productid=0){
		$query = $this->mssql->where('product_id', $productid)
								->get('product_standard_taper')->result_array();
		return $query;
	}
	
	public function taper_get_cer_chem($productid=0){
		$query = $this->mssql->where('product_id', $productid)
								->get('product_chemical_taper')->result_array();
		return $query;
	}
	
	public function taper_chemical_add($productid=0){
		
		foreach($this->input->post('chemical_value') as $key=>$value){
				if($value!=''){
					$aData = array(
						'product_id' => $productid,
						'chemical_value' => $value,
						'ps_max' => $_POST['ps_max'][$key],
						
						'ps_min' => $_POST['ps_min'][$key],
						
						'chemical_status' => 'approved',
						'chemical_createdtime' => date("Y-m-d H:i:s"),
						'chemical_createdip' => $this->input->ip_address()
					);
					
					$this->mssql->insert('product_chemical_taper', $aData);
				}
			}
		
		
	}
	
	public function taper_chemical_edit($productid=0){
		
		foreach($this->input->post('chemical_value') as $key=>$value){
				if($value!=''){
					$aData = array(
						'ps_max' => $_POST['ps_max'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'chemical_createdtime' => date("Y-m-d H:i:s"),
						'chemical_createdip' => $this->input->ip_address()
					);
					
					$this->mssql->where('chemical_id', $_POST['chemical_id'][$key]);
					$this->mssql->update('product_chemical_taper', $aData);
				}
			}
		
		
	}
	/* Taper - End */
	
	/* Stud - Start */
	public function stud_dataTable($limit=25,$offset=0){
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("product_name",$q);
			$this->mssql->or_like("product_type",$q);
		}
		$query = $this->mssql->where('product_status !=','discard')
					->order_by('product_createdtime','desc')
					->limit($limit,$offset)
					->get('product_stud')->result_array();
		return $query;
	}
	public function stud_count($limit=25,$offset=0){
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("product_name",$q);
			$this->mssql->or_like("product_type",$q);
		}
		$query = $this->mssql->where('product_status !=','discard')
					->order_by('product_createdtime','desc')
					->limit($limit,$offset)
					->get('product_stud')->num_rows();
		return $query;
	}
	public function stud_getinfo($productid=0){
		$query = $this->mssql->where('product_id', $productid)
								->get('product_stud')->row_array();
		return $query;
	}
	
	public function stud_add(){
		
		$this->load->model("mobile_upload");
		$this->mobile_upload->set_upload_path("public/uploads/product/stud");
		if($this->mobile_upload->upload("product_image")===true){
			$res_data = $this->mobile_upload->data();
			$filename = $res_data['file_name'];
		}else{
			$filename = '';
		}
		
		$aData = array(
			'product_image' => $filename,
			'product_name' => $this->input->post('product_name'),
			'product_standard' => $this->input->post('product_standard'),
			'product_initial' => $this->input->post('product_initial'),
			'certificate_id' => $this->input->post('certificate_id'),
			'product_type' => $this->input->post('product_type'),
			'product_status' => 'approved',
			'product_createdtime' => date("Y-m-d H:i:s"),
			'product_createdip' => $this->input->ip_address()
		);
		
		$this->mssql->insert('product_stud', $aData);
		
	}
	
	public function stud_edit($productid=0){
		
		$productinfo = $this->stud_getinfo($productid);
		
		$this->load->model("mobile_upload");
		$this->mobile_upload->set_upload_path("public/uploads/product/stud");
		
		if($_FILES['product_image']['tmp_name']!=''){
			if($this->mobile_upload->upload("product_image")){
				unlink('public/uploads/product/stud/'.$productinfo['product_image']);
				$res_data = $this->mobile_upload->data();
				$filename = $res_data['file_name'];
			}else{
				echo $this->mobile_upload->display_error();
				$filename = $productinfo['product_image'];
			}
		}else{
			$filename = $productinfo['product_image'];
		}
		
		$aData = array(
			'product_image' => $filename,
			'product_name' => $this->input->post('product_name'),
			'product_standard' => $this->input->post('product_standard'),
			'product_initial' => $this->input->post('product_initial'),
			'certificate_id' => $this->input->post('certificate_id'),
			'product_type' => $this->input->post('product_type'),
			'product_status' => 'approved',
			'product_updatedtime' => date("Y-m-d H:i:s"),
			'product_updatedip' => $this->input->ip_address()
		);
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->update('product_stud', $aData);
	}
	
	public function stud_delete($productid=0){
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_stud');
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_size_stud');
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_standard_stud');
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_chemical_stud');
	}
	
	/*
public function cer_size($cerid=0){
		$query = $this->mssql->where('certificate_id', $cerid)
							->where('size_status','1')
							->order_by('size_createdtime','desc')
							->get('certificate_size')->result_array();
		return $query;
	}
*/
	
	public function stud_size_dataTable($productid=0,$limit=25,$offset=0){
		$q = $this->input->get("q");
		if(trim($q)){ 
			$this->mssql->where("(size_m like '%{$q}%' or size_p like '%{$q}%' or size_length like '%{$q}%' or size_qty like '%{$q}%')",false,false);
			
		}
		$query = $this->mssql->where('product_id', $productid)
							->where('size_status !=','discard')
							->order_by('size_createdtime','desc')
							->limit($limit,$offset)
							->get('product_size_stud')->result_array();
		return $query;
	}
	
	public function stud_size_count($productid=0)
	{
		$q = $this->input->get("q");
		if(trim($q)){ 
			$this->mssql->where("(size_m like '%{$q}%' or size_p like '%{$q}%' or size_length like '%{$q}%' or size_qty like '%{$q}%')",false,false);
			
		}
		$query = $this->mssql->where('product_id', $productid)
							->where('size_status !=','discard')
							->order_by('size_createdtime','desc')
							->get('product_size_stud')->num_rows();
		return $query;
	}
	
	
	public function stud_size_add($productid=0){
		$aData = array(
			'product_id' => $productid,
			'size_m' => $this->input->post('size_m'),
			'size_p' => $this->input->post('size_p'),
			'size_length' => $this->input->post('size_length'),
			'size_status' => 'approved',
			'size_createdtime' => date("Y-m-d H:i:s"),
			'size_createdip' => $this->input->ip_address()
		);
		
		$this->mssql->insert('product_size_stud', $aData);
		$size_id = $this->mssql->insert_id();
		
		foreach($this->input->post('size_description') as $key=>$value){
				if($value!=''){
					$bData = array(
						'size_id' => $size_id,
						'product_id' => $productid,
						'inspection_id' => $_POST['inspection_id'][$key],
						'ps_description' => $value,
						'ps_max' => $_POST['ps_max'][$key],
						'ps_max_unit' => $_POST['ps_max_unit'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'ps_min_unit' => $_POST['ps_min_unit'][$key],
						'ps_status' => 'approved',
						'ps_createdtime' => date("Y-m-d H:i:s"),
						'ps_createdip' => $this->input->ip_address()
					);
					
					$this->mssql->insert('product_cer_size_stud', $bData);
				}
			}
			
			$cData = array(
			'size_id' => $size_id,
			'stock_createdtime' => date("Y-m-d H:i:s"),
			'stock_createdip' => $this->input->ip_address()
		);
		
		$this->mssql->insert('stock_stud', $cData);

		
	}
	
	public function stud_size_edit($sizeid=0){
		$aData = array(
			'size_m' => $this->input->post('size_m'),
			'size_p' => $this->input->post('size_p'),
			'size_updatedtime' => date("Y-m-d H:i:s"),
			'size_updatedip' => $this->input->ip_address()
		);
		
		$this->mssql->where('size_id', $sizeid);
		$this->mssql->update('product_size_stud', $aData);
		
		foreach($this->input->post('size_description') as $key=>$value){
			if($value!=''){
				$bData = array(
					'inspection_id' => $_POST['inspection_id'][$key],
					'ps_max' => $_POST['ps_max'][$key],
					'ps_max_unit' => $_POST['ps_max_unit'][$key],
					'ps_min' => $_POST['ps_min'][$key],
					'ps_min_unit' => $_POST['ps_min_unit'][$key],
					'ps_updatedtime' => date("Y-m-d H:i:s"),
					'ps_updatedip' => $this->input->ip_address()
				);
				
				$this->mssql->where('ps_id', $_POST['ps_id'][$key]);
				$this->mssql->update('product_cer_size_stud', $bData);
			}
		}
	}
	
	public function stud_size_delete($productid=0){
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('product_size_stud');
	}
	
	public function stud_standardlist($productid=0){
		$query = $this->mssql->where('ps_status !=','discard')
								->where('product_id', $productid)
								->order_by('ps_id','asc')
								->get('product_standard_stud')->result_array();
		return $query;
	}
	
	public function get_stud_sizeinfo($sizeid=0){
		$query = $this->mssql->where('size_id', $sizeid)
								->get('product_size_stud')->row_array();
		return $query;
	}
	
	public function stud_cer_size($sizeid=0){
		$query = $this->mssql->where('size_id', $sizeid)
								->where('ps_status','approved')
								->order_by('ps_id', 'asc')
								->get('product_cer_size_stud')->result_array();
		return $query;
	}
	
	public function stud_standard_add($productid=0){
		if($this->input->post('standard_description')){
			$this->mssql->where('product_id', $productid);
			$this->mssql->delete('product_standard_stud');
			foreach($this->input->post('standard_description') as $key=>$value){
				if($value!=''){
					$aData = array(
						'product_id' => $productid,
						'inspection_id' => $_POST['inspection_id'][$key],
						'standard_description' => $value,
						'ps_max' => $_POST['ps_max'][$key],
						'ps_max_unit' => $_POST['ps_max_unit'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'ps_min_unit' => $_POST['ps_min_unit'][$key],
						'ps_status' => 'approved',
						'ps_createdtime' => date("Y-m-d H:i:s"),
						'ps_createdip' => $this->input->ip_address()
					);
					
					$this->mssql->insert('product_standard_stud', $aData);
				}
			}
		}
	}
	
	public function stud_standard_edit($productid=0){
		if($this->input->post('standard_description')){
			foreach($this->input->post('standard_description') as $key=>$value){
				if($value!=''){
					$aData = array(
						'inspection_id' => $_POST['inspection_id'][$key],
						'ps_max' => $_POST['ps_max'][$key],
						'ps_max_unit' => $_POST['ps_max_unit'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'ps_min_unit' => $_POST['ps_min_unit'][$key],
						'ps_updatedtime' => date("Y-m-d H:i:s"),
						'ps_updatedip' => $this->input->ip_address()
					);
					
					$this->mssql->where('ps_id', $_POST['ps_id'][$key]);
					$this->mssql->update('product_standard_stud', $aData);
				}
			}
		}
	}
	
	public function stud_chemicallist($productid=0){
		$query = $this->mssql->where('product_id', $productid)
							->where('chemical_status !=','discard')
							->order_by('chemical_id','asc')
							->get('product_chemical_stud')->result_array();
		return $query;
	}
	
	public function stud_get_cer_std($productid=0){
		$query = $this->mssql->where('product_id', $productid)
								->get('product_standard_stud')->result_array();
		return $query;
	}
	
	public function stud_get_cer_chem($productid=0){
		$query = $this->mssql->where('product_id', $productid)
								->get('product_chemical_stud')->result_array();
		return $query;
	}
	
	public function stud_chemical_add($productid=0){
		
		foreach($this->input->post('chemical_value') as $key=>$value){
				if($value!=''){
					$aData = array(
						'product_id' => $productid,
						'chemical_value' => $value,
						'ps_max' => $_POST['ps_max'][$key],
						
						'ps_min' => $_POST['ps_min'][$key],
						
						'chemical_status' => 'approved',
						'chemical_createdtime' => date("Y-m-d H:i:s"),
						'chemical_createdip' => $this->input->ip_address()
					);
					
					$this->mssql->insert('product_chemical_stud', $aData);
				}
			}
		
		
	}
	
	public function stud_chemical_edit($productid=0){
		
		foreach($this->input->post('chemical_value') as $key=>$value){
				if($value!=''){
					$aData = array(
						'ps_max' => $_POST['ps_max'][$key],
						'ps_min' => $_POST['ps_min'][$key],
						'chemical_createdtime' => date("Y-m-d H:i:s"),
						'chemical_createdip' => $this->input->ip_address()
					);
					
					$this->mssql->where('chemical_id', $_POST['chemical_id'][$key]);
					$this->mssql->update('product_chemical_stud', $aData);
				}
			}
		
		
	}
	/* Stud - End */
	
	public function get_bolt_history_dataTable($size_id=0,$limit=100,$offset=0)
	{
		$this->mssql->where("size_id",$size_id);
		$this->mssql->limit($limit,$offset);
		$this->mssql->order_by("log_id","DESC");
		$res = $this->mssql->get("log_stock_bolt")->result_array();
		return $res;
	}
	
	public function get_nut_history_dataTable($size_id=0,$limit=100,$offset=0)
	{
		$this->mssql->where("size_id",$size_id);
		$this->mssql->limit($limit,$offset);
		$this->mssql->order_by("log_id","DESC");
		$res = $this->mssql->get("log_stock_nut")->result_array();
		return $res;
	}
	
	public function get_washer_history_dataTable($size_id=0,$limit=100,$offset=0)
	{
		$this->mssql->where("size_id",$size_id);
		$this->mssql->limit($limit,$offset);
		$this->mssql->order_by("log_id","DESC");
		$res = $this->mssql->get("log_stock_washer")->result_array();
		return $res;
	}
	
	public function get_spring_history_dataTable($size_id=0,$limit=100,$offset=0)
	{
		$this->mssql->where("size_id",$size_id);
		$this->mssql->limit($limit,$offset);
		$this->mssql->order_by("log_id","DESC");
		$res = $this->mssql->get("log_stock_spring")->result_array();
		return $res;
	}
	
	public function get_taper_history_dataTable($size_id=0,$limit=100,$offset=0)
	{
		$this->mssql->where("size_id",$size_id);
		$this->mssql->limit($limit,$offset);
		$this->mssql->order_by("log_id","DESC");
		$res = $this->mssql->get("log_stock_taper")->result_array();
		return $res;
	}
	
	public function get_stud_history_dataTable($size_id=0,$limit=100,$offset=0)
	{
		$this->mssql->where("size_id",$size_id);
		$this->mssql->limit($limit,$offset);
		$this->mssql->order_by("log_id","DESC");
		$res = $this->mssql->get("log_stock_stud")->result_array();
		return $res;
	}
	
	
	public function get_bolt_incoming_dataTable($size_id=0, $pro_id=0, $show_status=''){
		$this->mssql->where('size_id', $size_id);
		$this->mssql->where('income_product_id', $pro_id);
		if($show_status=='logs'){
			$this->mssql->where('test_status',2);
		}
		$this->mssql->order_by('income_id', 'ASC');
		$res = $this->mssql->get('income_bolt')->result_array();
		return $res;
	}
	
	public function get_nut_incoming_dataTable($size_id=0, $pro_id=0, $show_status=''){
		$this->mssql->where('size_id', $size_id);
		$this->mssql->where('income_product_id', $pro_id);
		if($show_status=='logs'){
			$this->mssql->where('test_status',2);
		}
		$this->mssql->order_by('income_id', 'ASC');
		$res = $this->mssql->get('income_nut')->result_array();
		return $res;
	}
	
	public function get_washer_incoming_dataTable($size_id=0, $pro_id=0, $show_status=''){
		$this->mssql->where('size_id', $size_id);
		$this->mssql->where('income_product_id', $pro_id);
		if($show_status=='logs'){
			$this->mssql->where('test_status',2);
		}
		$this->mssql->order_by('income_id', 'ASC');
		$res = $this->mssql->get('income_washer')->result_array();
		return $res;
	}
	
	public function get_spring_incoming_dataTable($size_id=0, $pro_id=0){
		$this->mssql->where('size_id', $size_id);
		$this->mssql->where('income_product_id', $pro_id);
		$this->mssql->order_by('income_id', 'ASC');
		$res = $this->mssql->get('income_spring')->result_array();
		return $res;
	}
	
	public function get_taper_incoming_dataTable($size_id=0, $pro_id=0){
		$this->mssql->where('size_id', $size_id);
		$this->mssql->where('income_product_id', $pro_id);
		$this->mssql->order_by('income_id', 'ASC');
		$res = $this->mssql->get('income_taper')->result_array();
		return $res;
	}
	
	public function get_stud_incoming_dataTable($size_id=0, $pro_id=0){
		$this->mssql->where('size_id', $size_id);
		$this->mssql->where('income_product_id', $pro_id);
		$this->mssql->order_by('income_id', 'ASC');
		$res = $this->mssql->get('income_stud')->result_array();
		return $res;
	}
	
	public function get_supplierinfo_byid($supplier_id=0){
		$this->mssql->where('supplier_id', $supplier_id);
		$res = $this->mssql->get('supplier')->row_array();
		return $res;
	}
}
?>