<?php
class Finaltestanchormodel extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->mssql = $this->load->database("mssql",true);
	}
	
	function get_finaltestinfo($productid=0, $finalid=0){
		
		$productinfo = $this->get_productinfo_byid($productid);
		
		$query = $this->mssql->where('product_id', $productid)
								->where('size_id', $productinfo->size_id)
								->where('final_id', $finalid)
								->get('final_anchor_test')->row();
		return $query;
	}
	
	function get_productinfo_byid($productid=0){
		
		$product = $this->mssql->where('product_id', $productid)
							->where('product_status','approved')
							->limit(1)
							->order_by('product_createdtime','desc')
							->get('anchor_product')->row();
		return $product;
		
	}
	
	function get_incomeid_byproductid($productid=0){
		
		$product = $this->get_productinfo_byid($productid);
		
		$query = $this->mssql->where('size_id', $product->size_id)
							->where('test_status',2)
							->order_by('income_id','asc')
							->get('income_anchor')->result();
		
		return $query;
							
	}
	
	function get_incomeinfo_byincomeid($incomeid=0){
		$query = $this->mssql->where('income_anchor.income_id', $incomeid)
							->join('supplier','income_anchor.income_supplier_id=supplier.supplier_id','inner')
							->get('income_anchor')->row_array();
		return $query;
	}
	
	function add_income_anchor_test(){
		
		$aInsert = array(
			'product_id' => $this->input->post('product_id'),
			'final_id' => $this->input->post('final_id'),
			'size_id' => $this->input->post('size_id'),
			'income_id' => $this->input->post('income_id'),
			'supplier_id' => $this->input->post('supplier_id'),
			'income_test_date' => $this->input->post('income_test_date'),
			'product_type' => $this->input->post('product_type'),
			'final_test_createdtime' => date("Y-m-d H:i:s"),
			'final_test_createdip' => $this->input->ip_address()
		);
		
		$this->mssql->insert('final_anchor_test', $aInsert);
		return $this->mssql->insert_id();
		
	}
	
	function edit_income_anchor_test($testid=0){
		$aUpdate = array(
			'product_id' => $this->input->post('product_id'),
			'final_id' => $this->input->post('final_id'),
			'size_id' => $this->input->post('size_id'),
			'income_id' => $this->input->post('income_id'),
			'supplier_id' => $this->input->post('supplier_id'),
			'income_test_date' => $this->input->post('income_test_date'),
			'product_type' => $this->input->post('product_type'),
			'final_test_updatedtime' => date("Y-m-d H:i:s"),
			'final_test_updatedip' => $this->input->ip_address()
		);
		
		$this->mssql->where('final_test_id', $testid);
		$this->mssql->update('final_anchor_test', $aUpdate);
	}
	
	function get_testinfo_byid($testid=0){
		$query = $this->mssql->where('final_test_id', $testid)
								->get('final_anchor_test')->row();
		return $query;
	}
	
	function get_chemicallist_byincomeid($incomeid=0){
		$query = $this->mssql->where('income_id', $incomeid)
								->order_by('chemical_id','asc')
								->get('product_chemical_anchor')->result();
		return $query;
	}
	
	function get_chemical_final_status_bytestid($finalid=0, $testid=0){
		$query = $this->mssql->where('test_id', $testid)
							->where('final_id', $finalid)
							->count_all_results('chemical_final_anchor');
		return $query;
	}
	
	function get_chemicaltestlist_bytestid($testid=0){
		$query = $this->mssql->where('test_id', $testid)
							->order_by('chemical_id', 'asc')
							->get('chemical_final_anchor')->result();
		return $query;
	}
	
	function add_chemical_anchor_test(){
		
		/* Update chemical test using status - Start */
		$aUpdate = array(
			'chemical_test_use' => $this->input->post('chemical_test_using'),
			'final_test_updatedtime' => date("Y-m-d H:i:s"),
			'final_test_updatedip' => $this->input->ip_address()
		);
		
		$this->mssql->where('final_test_id', $this->input->post('test_id'));
		$this->mssql->update('final_anchor_test', $aUpdate);
		/* Update chemical test using status - End */
		
		if($this->input->post('process_status')=='update'){
			foreach($_POST['chemical_result'] as $i => $result){
				$aUpdate = array(
					'refer_chemical_id' => $_POST['chemical_id'][$i],
					'chemical_value' => $_POST['chemical_value'][$i],
					'ps_max' => $_POST['ps_max'][$i],
					'ps_min' => $_POST['ps_min'][$i],
					'chemical_updatedtime' => date("Y-m-d H:i:s"),
					'chemical_updatedip' => $this->input->ip_address(),
					'income_id' => $this->input->post('income_id'),
					'chemical_result' => $_POST['chemical_result'][$i],
					'product_id' => $this->input->post('product_id'),
					'final_id' => $this->input->post('final_id'),
					'test_id' => $this->input->post('test_id')
				);
				
				$this->mssql->where('test_id', $this->input->post('test_id'));
				$this->mssql->where('final_id', $this->input->post('final_id'));
				$this->mssql->where('chemical_id', $_POST['chemical_id'][$i]);
				$this->mssql->update('chemical_final_anchor', $aUpdate);
			}
		}else{
			foreach($_POST['chemical_result'] as $i => $result){
				$aInsert = array(
					'refer_chemical_id' => $_POST['chemical_id'][$i],
					'chemical_value' => $_POST['chemical_value'][$i],
					'ps_max' => $_POST['ps_max'][$i],
					'ps_min' => $_POST['ps_min'][$i],
					'chemical_createdtime' => date("Y-m-d H:i:s"),
					'chemical_createdip' => $this->input->ip_address(),
					'income_id' => $this->input->post('income_id'),
					'chemical_result' => $_POST['chemical_result'][$i],
					'product_id' => $this->input->post('product_id'),
					'final_id' => $this->input->post('final_id'),
					'test_id' => $this->input->post('test_id')
				);
				
				$this->mssql->insert('chemical_final_anchor', $aInsert);
			}
		}
		
	}
	
	function get_materiallist_byincomeid($incomeid=0){
		$query = $this->mssql->where('income_id', $incomeid)
							->where('ps_status','approved')
							->order_by('ps_id','asc')
							->get('product_mechanical_anchor')->result();
		return $query;
	}
	
	function get_materiallist_bytestid($testid=0){
		$query = $this->mssql->where('test_id', $testid)
								->order_by('mechanical_test_id','asc')
								->get('product_mechanical_anchor_testing')->result();
		return $query;
	}
	
	function get_material_final_status_bytestid($testid=0){
		$query = $this->mssql->where('test_id', $testid)
							->count_all_results('product_mechanical_anchor_testing');
		return $query;
	}
	
	function get_anchortestlist_byincomeid($incomeid=0){
		$query = $this->mssql->where('income_anchor_testing.income_id', $incomeid)
							->order_by('income_anchor_testing.anchor_testing_id','asc')
							->join('product_standard_anchor','income_anchor_testing.anchor_standard_id=product_standard_anchor.ps_id','inner')
							->limit(1)
							->get('income_anchor_testing')->row();
		return $query;
	}
	
	function add_material_anchor_test(){
		
		/* Update chemical test using status - Start */
		$aUpdate = array(
			'material_test_use' => $this->input->post('material_test_using'),
			'final_test_updatedtime' => date("Y-m-d H:i:s"),
			'final_test_updatedip' => $this->input->ip_address()
		);
		
		$this->mssql->where('final_test_id', $this->input->post('test_id'));
		$this->mssql->update('final_anchor_test', $aUpdate);
		/* Update chemical test using status - End */
		
		if($this->input->post('process_status')=='update'){
			foreach($_POST['test_result'] as $i => $result){
				$aUpdate = array(
					'income_id' => $this->input->post('income_id'),
					'product_id' => $this->input->post('product_id'),
					'final_id' => $this->input->post('final_id'),
					'test_id' => $this->input->post('test_id'),
					'description' => $_POST['description'][$i],
					'ps_min_unit' => $_POST['ps_min_unit'][$i],
					'ps_max_unit' => $_POST['ps_max_unit'][$i],
					'ps_min' => $_POST['ps_min'][$i],
					'ps_max' => $_POST['ps_max'][$i],
					'test_result' => $_POST['test_result'][$i],
					'mechanical_updatedtime' => date("Y-m-d H:i:s"),
					'mechanical_updatedip' => $this->input->ip_address()
				);
				
				$this->mssql->where('test_id', $this->input->post('test_id'));
				$this->mssql->where('final_id', $this->input->post('final_id'));
				$this->mssql->where('mechanical_test_id', $_POST['mechanical_test_id'][$i]);
				$this->mssql->update('product_mechanical_anchor_testing', $aUpdate);
			}
		}else{
			foreach($_POST['test_result'] as $i => $result){
				$aInsert = array(
					'income_id' => $this->input->post('income_id'),
					'product_id' => $this->input->post('product_id'),
					'final_id' => $this->input->post('final_id'),
					'test_id' => $this->input->post('test_id'),
					'description' => $_POST['description'][$i],
					'ps_min_unit' => $_POST['ps_min_unit'][$i],
					'ps_max_unit' => $_POST['ps_max_unit'][$i],
					'ps_min' => $_POST['ps_min'][$i],
					'ps_max' => $_POST['ps_max'][$i],
					'test_result' => $_POST['test_result'][$i],
					'mechanical_createdtime' => date("Y-m-d H:i:s"),
					'mechanical_createdip' => $this->input->ip_address()
				);
				
				$this->mssql->insert('product_mechanical_anchor_testing', $aInsert);
			}
		}
	}
	
	function get_standardlist(){
		$query = $this->mssql->order_by('id','asc')
							->get('thickness_galvanizes')->result();
		return $query;
	}
	
	function add_galvanized_anchor_test(){
		$aUpdate = array(
			'galvanized_test_using' => $this->input->post('galvanized_test_using'),
			'standard_id' => $this->input->post('standard_id'),
			'thickness_1' => $this->input->post('thickness_1'),	
			'thickness_2' => $this->input->post('thickness_2'),	
			'thickness_3' => $this->input->post('thickness_3'),	
			'thickness_4' => $this->input->post('thickness_4'),	
			'thickness_5' => $this->input->post('thickness_5'),	
			'average' => $this->input->post('average'),
			'heattreatment_test_using' => $this->input->post('heattreatment_test_using'),
			'quenching_temperature' => $this->input->post('quenching_temperature'),
			'quenching_min' => $this->input->post('quenching_min'),
			'quenching_max' => $this->input->post('quenching_max'),
			'quenching_test' => $this->input->post('quenching_test'),
			'tempering_temperature' => $this->input->post('tempering_temperature'),
			'tempering_min' => $this->input->post('tempering_min'),
			'tempering_max' => $this->input->post('tempering_max'),
			'tempering_test' => $this->input->post('tempering_test'),
			'final_test_updatedtime' => date("Y-m-d H:i:s"),
			'final_test_updatedip' => $this->input->ip_address()
		);
		
		$this->mssql->where('final_test_id', $this->input->post('test_id'));
		$this->mssql->update('final_anchor_test', $aUpdate);
		
		/* Update test result - Start */
		$aUpdate_result = array(
			'test_status' => $this->input->post('test_status'),
			'final_updatedtime' => date("Y-m-d H:i:s"),
			'final_updatedip' => $this->input->ip_address()
		);
		
		$this->mssql->where('final_id', $this->input->post('final_id'));
		$this->mssql->where('product_id', $this->input->post('product_id'));
		$this->mssql->update('final_anchor', $aUpdate_result);
		/* Update test result - End */
	}
	
	function get_supplierinfo_byid($supplierid=0){
		$query = $this->mssql->where('supplier_id', $supplierid)
							->get('supplier')->row();
		return $query;
	}
	
	function get_teststatus_byproductidandfinalid($productid=0, $finalid=0){
		$query = $this->mssql->where('final_id', $finalid)
								->where('product_id', $productid)
								->order_by('final_createdtime','desc')
								->limit(1)
								->get('final_anchor')->row();
		return $query;
	}
}
?>