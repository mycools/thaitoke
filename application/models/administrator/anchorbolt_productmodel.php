<?php
class Anchorbolt_productmodel extends CI_Model{
	function __construct(){
		parent::__construct();
		
		$this->mssql = $this->load->database("mssql", true);
	}
	
	public function dataTable($sizeid, $type='axel', $limit=0, $offset=0){
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("product_grade",trim($q));
		}
		$query = $this->mssql->where('size_id',$sizeid)
								->where('product_status !=','discard')
								->where('product_type', $type)
								->order_by('product_createdtime','DESC')
								->limit($limit,$offset)
								->get('anchor_product')->result_array();
								
		return $query;
	}
	
	public function get_count($sizeid,$type)
	{
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("product_grade",trim($q));
		}
		$query = $this->mssql->where('size_id',$sizeid)
					->where('product_status !=','discard')
					->where('product_type', $type)
					->order_by('product_createdtime','desc')
					->get('anchor_product')->num_rows();
		return $query;
	}
	
	public function getinfo($productid=0){
		$query = $this->mssql->where('product_id', $productid)
								->get('anchor_product')->row_array();
		return $query;
	}
	
	public function add(){
		
		
		$aData = array(
			'product_grade' => $this->input->post('product_grade'),
			'product_cate' => $this->input->post('product_cate'),
			'product_type' => $this->input->post('product_type'),
			'product_std' => $this->input->post('product_std'),
			'size_id' => $this->input->post('size_id'),
			'product_status' => 'approved',
			'product_createdtime' => date("Y-m-d H:i:s"),
			'product_createdip' => $this->input->ip_address()
		);
		
		$this->mssql->insert('anchor_product', $aData);
	}
	
	public function edit($productid=0){
		
		
		$aData = array(
			'product_grade' => $this->input->post('product_grade'),
			'product_cate' => $this->input->post('product_cate'),
			'product_type' => $this->input->post('product_type'),
			'product_std' => $this->input->post('product_std'),
			'size_id' => $this->input->post('size_id'),
			'product_status' => 'approved',
			'product_updatedtime' => date("Y-m-d H:i:s"),
			'product_updatedip' => $this->input->ip_address()
		);
		
		$this->mssql->where('product_id', $productid);
		$this->mssql->update('anchor_product', $aData);
		return true;
	}
	
	public function getdelete($productid=0){
		$this->mssql->where('product_id', $productid);
		$this->mssql->delete('anchor_product');
		
	}
	
	public function get_back_product_id($size_id){
		$query = $this->mssql->where('size_id', $size_id)
								->get('product_size_anchor')->row_array();
		return $query['product_id'];
		
	}
	
	
	
	
}
?>