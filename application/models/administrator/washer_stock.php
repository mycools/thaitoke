<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class washer_stock extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		$this->mssql = $this->load->database("mssql",true);
	}
	function getStockInfo($lot_id)
	{
		$this->mssql->where("lot_washer.lot_id",$lot_id);
		$product = $this->mssql->get("lot_washer")->row_array();
		return $product;
	}
	public function dataTable($limit=100,$offset=0)
	{
		$this->mssql->select("lot_washer.lot_no,supplier.supplier_name,supplier.supplier_tel,product_size_washer.size_m,product_size_washer.size_p,product_size_washer.size_length,product_washer.product_name,product_washer.product_grade,product_washer.product_initial,product_washer.product_iso,lot_washer.lot_certificate_no,lot_washer.lot_commodity,lot_washer.lot_surface,lot_washer.lot_material,lot_washer.lot_amount,lot_washer.lot_heat_no,lot_washer.lot_color,lot_washer.lot_color_value,lot_washer.lot_remark,lot_washer.lot_createdid,lot_washer.lot_updatedid,lot_washer.lot_import_date,lot_washer.lot_remain,lot_washer.lot_id");
		$this->mssql->where("lot_washer.lot_status","approved");
		
		$search_q = trim(strip_tags($this->input->get("q")));
		if($search_q != ""){
			$this->mssql->where("product_washer.product_name",$search_q);
			$this->mssql->where("lot_washer.lot_no",$search_q);
		}
		
		
		$this->mssql->order_by("lot_washer.lot_id","desc");
		$this->mssql->join("product_washer","product_washer.product_id=lot_washer.product_id","LEFT");
		$this->mssql->join("product_size_washer","product_size_washer.size_id = lot_washer.size_id","LEFT");
		$this->mssql->join("supplier","supplier.supplier_id = lot_washer.supplier_id","LEFT");
		$this->mssql->limit($limit,$offset);
		$product = $this->mssql->get("lot_washer")->result_array();
		//exit($this->mssql->last_query());
		return $product;
	}
	function export_data()
	{
		$this->mssql->select("lot_washer.lot_no,supplier.supplier_name,supplier.supplier_tel,product_size_washer.size_m,product_size_washer.size_p,product_size_washer.size_length,product_washer.product_name,product_washer.product_grade,product_washer.product_initial,product_washer.product_iso,lot_washer.lot_certificate_no,lot_washer.lot_commodity,lot_washer.lot_surface,lot_washer.lot_material,lot_washer.lot_amount,lot_washer.lot_heat_no,lot_washer.lot_color,lot_washer.lot_color_value,lot_washer.lot_remark,lot_washer.lot_createdid,lot_washer.lot_updatedid,lot_washer.lot_import_date,lot_washer.lot_remain,lot_washer.lot_id");
		$this->mssql->where("lot_washer.lot_status","approved");
		
		$search_q = trim(strip_tags($this->input->get("q")));
		if($search_q != ""){
			$this->mssql->where("product_washer.product_name",$search_q);
			$this->mssql->where("lot_washer.lot_no",$search_q);
		}
		
		$this->mssql->order_by("lot_washer.lot_id","desc");
		$this->mssql->join("product_washer","product_washer.product_id=lot_washer.product_id","LEFT");
		$this->mssql->join("product_size_washer","product_size_washer.size_id = lot_washer.size_id","LEFT");
		$this->mssql->join("supplier","supplier.supplier_id = lot_washer.supplier_id","LEFT");
		$product = $this->mssql->get("lot_washer");
		return $product;

	}
	public function add_stock()
	{
		$lot_no=trim(strip_tags($this->input->post("lot_no")));
		$supplier_id=trim(strip_tags($this->input->post("supplier_id")));
		$product_id=trim(strip_tags($this->input->post("product_id")));
		$size_id=trim(strip_tags($this->input->post("size_id")));
		$lot_certificate_no=trim(strip_tags($this->input->post("lot_certificate_no")));
		$lot_surface=trim(strip_tags($this->input->post("lot_surface")));
		$lot_amount=trim(strip_tags($this->input->post("lot_amount")));
		$lot_commodity=trim(strip_tags($this->input->post("lot_commodity")));
		$lot_material=trim(strip_tags($this->input->post("lot_material")));
		$lot_heat_no=trim(strip_tags($this->input->post("lot_heat_no")));
		$lot_color=trim(strip_tags($this->input->post("lot_color")));
		$lot_color_value=trim(strip_tags($this->input->post("lot_color_value")));
		$lot_remark=trim(strip_tags($this->input->post("lot_remark")));
		$lot_import_date=trim(strip_tags($this->input->post("lot_import_date")));
		$lot_import_date = str_replace('/', '-', $lot_import_date);
		$lot_import_date=date("Y-m-d",strtotime($lot_import_date));
		$this->mssql->set("lot_no",$lot_no);
		$this->mssql->set("supplier_id",$supplier_id);
		$this->mssql->set("product_id",$product_id);
		$this->mssql->set("size_id",$size_id);
		$this->mssql->set("lot_certificate_no",$lot_certificate_no);
		$this->mssql->set("lot_surface",$lot_surface);
		$this->mssql->set("lot_amount",$lot_amount);
		$this->mssql->set("lot_remain",$lot_amount);
		$this->mssql->set("lot_commodity",$lot_commodity);
		$this->mssql->set("lot_material",$lot_material);
		$this->mssql->set("lot_heat_no",$lot_heat_no);
		$this->mssql->set("lot_color",$lot_color);
		$this->mssql->set("lot_color_value",$lot_color_value);
		$this->mssql->set("lot_remark",$lot_remark);
		$this->mssql->set("lot_status","approved");
		$this->mssql->set("lot_createdtime",date("Y-m-d H:i:s"));
		$this->mssql->set("lot_createdip",$this->input->ip_address());
		$this->mssql->set("lot_createdid",$this->admin_library->user_id());
		$this->mssql->set("lot_import_date",$lot_import_date);
		$lot_id = $this->mssql->insert("lot_washer");
	}
	public function edit_stock()
	{
		$lot_id=trim(strip_tags($this->input->post("lot_id")));
		$lot_no=trim(strip_tags($this->input->post("lot_no")));
		$supplier_id=trim(strip_tags($this->input->post("supplier_id")));
		$product_id=trim(strip_tags($this->input->post("product_id")));
		$size_id=trim(strip_tags($this->input->post("size_id")));
		$lot_certificate_no=trim(strip_tags($this->input->post("lot_certificate_no")));
		$lot_surface=trim(strip_tags($this->input->post("lot_surface")));
		$lot_amount=trim(strip_tags($this->input->post("lot_amount")));
		$lot_commodity=trim(strip_tags($this->input->post("lot_commodity")));
		$lot_material=trim(strip_tags($this->input->post("lot_material")));
		$lot_heat_no=trim(strip_tags($this->input->post("lot_heat_no")));
		$lot_color=trim(strip_tags($this->input->post("lot_color")));
		$lot_color_value=trim(strip_tags($this->input->post("lot_color_value")));
		$lot_remark=trim(strip_tags($this->input->post("lot_remark")));
		$lot_import_date=trim(strip_tags($this->input->post("lot_import_date")));
		$lot_import_date = str_replace('/', '-', $lot_import_date);
		$lot_import_date=date("Y-m-d",strtotime($lot_import_date));
		$this->mssql->set("lot_no",$lot_no);
		$this->mssql->set("supplier_id",$supplier_id);
		$this->mssql->set("product_id",$product_id);
		$this->mssql->set("size_id",$size_id);
		$this->mssql->set("lot_certificate_no",$lot_certificate_no);
		$this->mssql->set("lot_surface",$lot_surface);
		$this->mssql->set("lot_amount",$lot_amount);
		$this->mssql->set("lot_remain",$lot_amount);
		$this->mssql->set("lot_commodity",$lot_commodity);
		$this->mssql->set("lot_material",$lot_material);
		$this->mssql->set("lot_heat_no",$lot_heat_no);
		$this->mssql->set("lot_color",$lot_color);
		$this->mssql->set("lot_color_value",$lot_color_value);
		$this->mssql->set("lot_remark",$lot_remark);
		$this->mssql->set("lot_status","approved");
		$this->mssql->set("lot_updatedtime",date("Y-m-d H:i:s"));
		$this->mssql->set("lot_updatedip",$this->input->ip_address());
		$this->mssql->set("lot_updatedid",$this->admin_library->user_id());
		$this->mssql->set("lot_import_date",$lot_import_date);
		$this->mssql->where("lot_id",$lot_id);
		$lot_id = $this->mssql->update("lot_washer");
	}

	public function delete_stock($lot_id)
	{
		$this->mssql->where("lot_id",$lot_id);
		return $this->mssql->delete("lot_washer");	
	}
	public function getSupplier()
	{
		$rs = $this->mssql->get("supplier")->result_array();
		return $rs;
	}
	public function getProduct()
	{
		$this->mssql->select("product_size_washer.product_id,product_washer.product_name,product_washer.product_type");
		$this->mssql->where("product_size_washer.size_status","approved");
		$this->mssql->where("product_washer.product_status","approved");
		$this->mssql->join("product_size_washer","product_washer.product_id=product_size_washer.product_id");
		$this->mssql->group_by("product_size_washer.product_id,product_washer.product_name,product_washer.product_type");
		$this->mssql->order_by("product_size_washer.product_id","DESC");
		$rs = $this->mssql->get("product_washer")->result_array();
		return $rs;
	}
	public function getSize($product_id)
	{
		$this->mssql->where("product_id",$product_id);
		$this->mssql->where("size_status","approved");
		$rs = $this->mssql->get("product_size_washer")->result_array();
		return $rs;		
	}
	
}