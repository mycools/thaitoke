<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
File Name 	: anchor_income.php
Controller	: anchor_income
Create By 	: Jarak Kritkiattisak
Create Date 	: 7/6/2557 BE
Project 	: iAon Project
Version 		: 1.0
*/
class Anchor_income extends CI_Model {
	var $_data=array();
	public function __construct()
	{
		parent::__construct();
		$this->mssql = $this->load->database("mssql",true);
	}
	public function dataTable($type,$limit=100,$offset=0)
	{
		$q = $this->input->get("q");
		$this->mssql->select("income_anchor.income_id, 
							income_anchor.income_quantity, 
							income_anchor.income_job_id, 
							income_anchor.income_invoice_no, 
							income_anchor.income_po_no, 
							income_anchor.income_test_no, 
							income_anchor.income_heat_no, 
							income_anchor.income_product_id,
							income_anchor.income_supplier_id,
							income_anchor.income_createdtime, 
							income_anchor.income_createdip, 
							income_anchor.income_createdid,
							income_anchor.test_status,
							product_anchor.product_grade, 
							product_anchor.product_thumbnail, 
							product_anchor.product_type, 
							product_anchor.product_low_carbon, 
							supplier.supplier_name, 
							supplier.supplier_address, 
							supplier.supplier_province, 
							supplier.supplier_email, 
							supplier.supplier_tel, 
							supplier.supplier_fax, 
							product_size_anchor.size_m,
							product_size_anchor.size_t,
							product_size_anchor.size_w,
							product_size_anchor.size_length, 
							product_size_anchor.size_status");
		$this->mssql->join("product_anchor","product_anchor.product_id = income_anchor.income_product_id");
		$this->mssql->join("supplier","supplier.supplier_id = income_anchor.income_supplier_id");
		$this->mssql->join("product_size_anchor","product_size_anchor.product_id = income_anchor.income_product_id AND product_size_anchor.size_id = income_anchor.size_id");
		
		if(trim($q)){
			$this->mssql->like("income_anchor.income_job_id",$q);
			$this->mssql->or_like("product_anchor.product_grade",$q);
			$this->mssql->or_like("supplier.supplier_name",$q);
		}
		//$this->mssql->join("product_anchor","product_anchor.product_id = income_anchor.income_product_id");
		$this->mssql->where("product_anchor.product_type",$type);
		$this->mssql->limit($limit,$offset);
		$this->mssql->order_by("income_anchor.income_id","DESC");
		$res = $this->mssql->get("income_anchor")->result_array();
		return $res;
	}
	function getIncome($income_id)
	{
		$this->mssql->limit(1);
		$this->mssql->where("income_id",$income_id);
		$res = $this->mssql->get("income_anchor")->row_array();
		return $res;
	}
	public function export_data($type)
	{
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("income_anchor.income_job_id",$q);
			$this->mssql->or_like("product_anchor.product_grade",$q);
		}
		$this->mssql->join("product_anchor","product_anchor.product_id = income_anchor.income_product_id");
		$this->mssql->where("product_anchor.product_type",$type);
		$res = $this->mssql->get("income_anchor");
		return $res;
	}
	
	function get_running_number_anchor(){
		
		$this->mssql->where("running_name","income_anchor");
		$res = $this->mssql->get("running")->row_array();
		return $res;
		
	}
	
	
	public function add_income()
	{
		//$income_job_id=trim(strip_tags($this->input->post("income_job_id")));
		//$income_sale_order_no=trim(strip_tags($this->input->post("income_sale_order_no")));
		$supplier_id=trim(strip_tags($this->input->post("supplier_id")));
		$product_id=trim(strip_tags($this->input->post("product_id")));
		$size_id=trim(strip_tags($this->input->post("size_id")));
		
		$income_invoice_no=trim(strip_tags($this->input->post("income_invoice_no")));
		//$income_purchase_no=trim(strip_tags($this->input->post("income_purchase_no")));
		//$income_material=trim(strip_tags($this->input->post("income_material")));
		//$income_commodity=trim(strip_tags($this->input->post("income_commodity")));
		//$income_certificate_no=trim(strip_tags($this->input->post("income_certificate_no")));
		$income_po_no=trim(strip_tags($this->input->post("income_po_no")));
		$income_heat_no=trim(strip_tags($this->input->post("income_heat_no")));
		$income_test_no=trim(strip_tags($this->input->post("income_test_no")));
		
		//$grade_mark=trim(strip_tags($this->input->post("grade_mark")));
		$quantity=trim(strip_tags($this->input->post("quantity")));
	
		$income_createdtime=date("Y-m-d H:i:s");
		$income_createdip=$this->input->ip_address();
		$income_createdid=$this->admin_library->user_id();
		
		$pro_name = $this->getproduct_name($product_id);
		$size_m = $this->get_size($size_id);
		
		$running = $this->get_running_number_anchor();
		if($pro_name['product_type'] == "axel"){
			$income_job_id = $pro_name['product_initial']."M".$size_m['size_m']."-".date("Y").$running['running_number'];

		}else{
			$income_job_id = $pro_name['product_initial']."T".$size_m['size_t']."-".date("Y").$running['running_number'];

		}
				
		$this->mssql->set("income_job_id",$income_job_id);
		//$this->mssql->set("income_sale_order_no",$income_sale_order_no);
		$this->mssql->set("income_supplier_id",$supplier_id);
		$this->mssql->set("income_invoice_no",$income_invoice_no);
		//$this->mssql->set("income_purchase_no",$income_purchase_no);
		//$this->mssql->set("income_material",$income_material);
		//$this->mssql->set("income_commodity",$income_commodity);
		//$this->mssql->set("income_certificate_no",$income_certificate_no);
		$this->mssql->set("income_po_no",$income_po_no);
		$this->mssql->set("income_test_no",$income_test_no);
		$this->mssql->set("income_heat_no",$income_heat_no);
		$this->mssql->set("income_product_id",$product_id);
		$this->mssql->set("size_id",$size_id);
		//$this->mssql->set("grade_mark",$grade_mark);
		$this->mssql->set("income_quantity",$quantity);


		
		$this->mssql->set("income_createdtime",$income_createdtime);
		$this->mssql->set("income_createdip",$income_createdip);
		$this->mssql->set("income_createdid",$income_createdid);
		$this->mssql->insert("income_anchor");
		
		$new_number = $running['running_number']+1;
		$new_number = sprintf("%06d", $new_number);
		$this->mssql->set("running_number",$new_number);
		$this->mssql->where("running_name","income_anchor");
		$this->mssql->update("running");
		
		return $this->mssql->insert_id();
					
	}
	public function edit_income()
	{
		$income_id=trim(strip_tags($this->input->post("income_id")));
		$supplier_id=trim(strip_tags($this->input->post("supplier_id")));
		$income_invoice_no=trim(strip_tags($this->input->post("income_invoice_no")));
		//$income_purchase_no=trim(strip_tags($this->input->post("income_purchase_no")));
		//$income_material=trim(strip_tags($this->input->post("income_material")));
		//$income_commodity=trim(strip_tags($this->input->post("income_commodity")));
		//$income_certificate_no=trim(strip_tags($this->input->post("income_certificate_no")));
		$income_po_no=trim(strip_tags($this->input->post("income_po_no")));
		$income_test_no=trim(strip_tags($this->input->post("income_test_no")));
		$income_heat_no=trim(strip_tags($this->input->post("income_heat_no")));
		//$grade_mark=trim(strip_tags($this->input->post("grade_mark")));
		
		$income_createdtime=date("Y-m-d H:i:s");
		$income_createdip=$this->input->ip_address();
		$income_createdid=$this->admin_library->user_id();
		
		$this->mssql->set("income_supplier_id",$supplier_id);
		$this->mssql->set("income_invoice_no",$income_invoice_no);
		//$this->mssql->set("income_purchase_no",$income_purchase_no);
		//$this->mssql->set("income_material",$income_material);
		//$this->mssql->set("income_commodity",$income_commodity);
		//$this->mssql->set("income_certificate_no",$income_certificate_no);
		$this->mssql->set("income_po_no",$income_po_no);
		$this->mssql->set("income_test_no",$income_test_no);
		$this->mssql->set("income_heat_no",$income_heat_no);
		//$this->mssql->set("grade_mark",$grade_mark);

		$this->mssql->set("income_updatedtime",$income_createdtime);
		$this->mssql->set("income_updatedip",$income_createdip);
		$this->mssql->set("income_updatedid",$income_createdid);
		$this->mssql->where("income_id",$income_id);
		$this->mssql->update("income_anchor");
					
	}
	function delete_income($income_id)
	{
		
		
		$this->mssql->where("income_id",$income_id);
		$this->mssql->delete("income_anchor");
		
			}
	
	public function getproduct_name($pro_id=0)
	{
		$this->mssql->limit(1);
		$this->mssql->where("product_id",$pro_id);
		$rs = $this->mssql->get("product_anchor")->row_array();
		return $rs;
	}
	
	public function getsupplier_name($sup_id=0)
	{
		$this->mssql->limit(1);
		$this->mssql->where("supplier_id",$sup_id);
		$rs = $this->mssql->get("supplier")->row_array();
		return $rs;
	}
	
	public function getSupplier()
	{
		$rs = $this->mssql->get("supplier")->result_array();
		return $rs;
	}
	public function getCustomer()
	{
		$rs = $this->mssql->get("customer")->result_array();
		return $rs;
	}
	
	public function get_anchorsize_byid($anchorid=0){
		$query = $this->mssql->where('product_id', $anchorid)
							->get('product_size_anchor')->result_array();
		return $query;
	}
	function get_size($size_id)
	{
		$query = $this->mssql->where('size_id', $size_id)
							->get('product_size_anchor')->row_array();
		return $query;
	}
	public function getProduct($type)
	{
		$this->mssql->select("product_size_anchor.product_id,product_anchor.product_grade, product_anchor.product_type");
		$this->mssql->where("product_anchor.product_type",$type);
		$this->mssql->where("product_size_anchor.size_status","approved");
		$this->mssql->where("product_anchor.product_status","approved");
		$this->mssql->join("product_size_anchor","product_anchor.product_id=product_size_anchor.product_id");
		$this->mssql->group_by("product_size_anchor.product_id, product_anchor.product_grade, product_anchor.product_type");
		$this->mssql->order_by("product_size_anchor.product_id","DESC");
		$rs = $this->mssql->get("product_anchor")->result_array();
		return $rs;
	}
}