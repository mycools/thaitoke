<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
File Name 	: stud_income.php
Controller	: Stud_income
Create By 	: Jarak Kritkiattisak
Create Date 	: 7/6/2557 BE
Project 	: iAon Project
Version 		: 1.0
*/
class Stud_income extends CI_Model {
	var $_data=array();
	public function __construct()
	{
		parent::__construct();
		$this->mssql = $this->load->database("mssql",true);
	}
	public function dataTable($limit=100,$offset=0)
	{
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("income_stud.income_job_id",$q);
			$this->mssql->or_like("product_stud.product_name",$q);
		}
		$this->mssql->join("product_stud","product_stud.product_id = income_stud.income_product_id");
		$this->mssql->limit($limit,$offset);
		$this->mssql->order_by("income_id","DESC");
		$res = $this->mssql->get("income_stud")->result_array();
		return $res;
	}
	function getIncome($income_id)
	{
		$this->mssql->limit(1);
		$this->mssql->where("income_id",$income_id);
		$res = $this->mssql->get("income_stud")->row_array();
		return $res;
	}
	public function export_data()
	{
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("income_stud.income_job_id",$q);
			$this->mssql->or_like("product_stud.product_name",$q);
		}
		$this->mssql->join("product_stud","product_stud.product_id = income_stud.income_product_id");
		$res = $this->mssql->get("income_stud");
		return $res;
	}
	
	function get_running_number_stud(){
		
		$this->mssql->where("running_name","income_stud");
		$res = $this->mssql->get("running")->row_array();
		return $res;
		
	}

	
	public function getBoltAvailableStock($edited=false)
	{
		$this->mssql->select("lot_stud.lot_no,supplier.supplier_name,supplier.supplier_tel,product_size_stud.size_m,product_size_stud.size_p,product_size_stud.size_length,product_stud.product_name,product_stud.product_grade,product_stud.product_initial,product_stud.product_iso,lot_stud.lot_certificate_no,lot_stud.lot_commodity,lot_stud.lot_surface,lot_stud.lot_material,lot_stud.lot_amount,lot_stud.lot_heat_no,lot_stud.lot_color,lot_stud.lot_color_value,lot_stud.lot_remark,lot_stud.lot_createdid,lot_stud.lot_updatedid,lot_stud.lot_import_date,lot_stud.lot_remain,lot_stud.lot_id");
		$this->mssql->where("lot_stud.lot_status","approved");
		if($edited==false)
		{
			$this->mssql->where("lot_stud.lot_remain >","0");
		}
		$this->mssql->order_by("lot_stud.lot_id","desc");
		$this->mssql->join("product_stud","product_stud.product_id=lot_stud.product_id","LEFT");
		$this->mssql->join("product_size_stud","product_size_stud.size_id = lot_stud.size_id","LEFT");
		$this->mssql->join("supplier","supplier.supplier_id = lot_stud.supplier_id","LEFT");

		$product = $this->mssql->get("lot_stud")->result_array();
		return $product;	
	}
	public function getNutAvailableStock($edited=false)
	{
		$this->mssql->select("lot_stud.lot_no,supplier.supplier_name,supplier.supplier_tel,product_size_stud.size_m,product_size_stud.size_p,product_size_stud.size_length,product_stud.product_name,product_stud.product_grade,product_stud.product_initial,product_stud.product_iso,lot_stud.lot_certificate_no,lot_stud.lot_commodity,lot_stud.lot_surface,lot_stud.lot_material,lot_stud.lot_amount,lot_stud.lot_heat_no,lot_stud.lot_color,lot_stud.lot_color_value,lot_stud.lot_remark,lot_stud.lot_createdid,lot_stud.lot_updatedid,lot_stud.lot_import_date,lot_stud.lot_remain,lot_stud.lot_id");
		$this->mssql->where("lot_stud.lot_status","approved");
		if($edited==false)
		{
			$this->mssql->where("lot_stud.lot_remain >","0");
		}
		$this->mssql->order_by("lot_stud.lot_id","desc");
		$this->mssql->join("product_stud","product_stud.product_id=lot_stud.product_id","LEFT");
		$this->mssql->join("product_size_stud","product_size_stud.size_id = lot_stud.size_id","LEFT");
		$this->mssql->join("supplier","supplier.supplier_id = lot_stud.supplier_id","LEFT");

		$product = $this->mssql->get("lot_stud")->result_array();
		return $product;	
	}
	public function getstudAvailableStock($edited=false)
	{
		$this->mssql->select("lot_stud.lot_no,supplier.supplier_name,supplier.supplier_tel,product_size_stud.size_m,product_size_stud.size_p,product_size_stud.size_length,product_stud.product_name,product_stud.product_grade,product_stud.product_initial,product_stud.product_iso,lot_stud.lot_certificate_no,lot_stud.lot_commodity,lot_stud.lot_surface,lot_stud.lot_material,lot_stud.lot_amount,lot_stud.lot_heat_no,lot_stud.lot_color,lot_stud.lot_color_value,lot_stud.lot_remark,lot_stud.lot_createdid,lot_stud.lot_updatedid,lot_stud.lot_import_date,lot_stud.lot_remain,lot_stud.lot_id");
		$this->mssql->where("lot_stud.lot_status","approved");
		if($edited==false)
		{
			$this->mssql->where("lot_stud.lot_remain >","0");
		}
		$this->mssql->order_by("lot_stud.lot_id","desc");
		$this->mssql->join("product_stud","product_stud.product_id=lot_stud.product_id","LEFT");
		$this->mssql->join("product_size_stud","product_size_stud.size_id = lot_stud.size_id","LEFT");
		$this->mssql->join("supplier","supplier.supplier_id = lot_stud.supplier_id","LEFT");

		$product = $this->mssql->get("lot_stud")->result_array();	
		return $product;	
	}
	public function add_income()
	{
		//$income_job_id=trim(strip_tags($this->input->post("income_job_id")));
		//$income_sale_order_no=trim(strip_tags($this->input->post("income_sale_order_no")));
		$supplier_id=trim(strip_tags($this->input->post("supplier_id")));
		$income_invoice_no=trim(strip_tags($this->input->post("income_invoice_no")));
		$income_purchase_no=trim(strip_tags($this->input->post("income_purchase_no")));
		$income_material=trim(strip_tags($this->input->post("income_material")));
		$income_commodity=trim(strip_tags($this->input->post("income_commodity")));
		$income_certificate_no=trim(strip_tags($this->input->post("income_certificate_no")));
		$income_po_no=trim(strip_tags($this->input->post("income_po_no")));
		$income_delivery_no=trim(strip_tags($this->input->post("income_delivery_no")));
		$income_heat_no=trim(strip_tags($this->input->post("income_heat_no")));
		$product_id=trim(strip_tags($this->input->post("product_id")));
		$size_id=trim(strip_tags($this->input->post("size_id")));
		$grade_mark=trim(strip_tags($this->input->post("grade_mark")));
		$quantity=trim(strip_tags($this->input->post("quantity")));
	
		$income_createdtime=date("Y-m-d H:i:s");
		$income_createdip=$this->input->ip_address();
		$income_createdid=$this->admin_library->user_id();
		
		$pro_name = $this->getproduct_name($product_id);
		$size_m = $this->get_size($size_id);
		
		$running = $this->get_running_number_stud();
		
		$income_job_id = $pro_name['product_initial']."M".$size_m['size_m']."-".date("Y").$running['running_number'];

		
		$this->mssql->set("income_job_id",$income_job_id);
		//$this->mssql->set("income_sale_order_no",$income_sale_order_no);
		$this->mssql->set("income_supplier_id",$supplier_id);
		$this->mssql->set("income_invoice_no",$income_invoice_no);
		$this->mssql->set("income_purchase_no",$income_purchase_no);
		$this->mssql->set("income_material",$income_material);
		$this->mssql->set("income_commodity",$income_commodity);
		$this->mssql->set("income_certificate_no",$income_certificate_no);
		$this->mssql->set("income_po_no",$income_po_no);
		$this->mssql->set("income_delivery_no",$income_delivery_no);
		$this->mssql->set("income_heat_no",$income_heat_no);
		$this->mssql->set("income_product_id",$product_id);
		$this->mssql->set("size_id",$size_id);
		$this->mssql->set("grade_mark",$grade_mark);
		$this->mssql->set("income_quantity",$quantity);


		
		$this->mssql->set("income_createdtime",$income_createdtime);
		$this->mssql->set("income_createdip",$income_createdip);
		$this->mssql->set("income_createdid",$income_createdid);
		$this->mssql->insert("income_stud");
		
		$new_number = $running['running_number']+1;
		$new_number = sprintf("%06d", $new_number);
		$this->mssql->set("running_number",$new_number);
		$this->mssql->where("running_name","income_stud");
		$this->mssql->update("running");
					
	}
	public function edit_income()
	{
		$income_id=trim(strip_tags($this->input->post("income_id")));
		$supplier_id=trim(strip_tags($this->input->post("supplier_id")));
		$income_invoice_no=trim(strip_tags($this->input->post("income_invoice_no")));
		$income_purchase_no=trim(strip_tags($this->input->post("income_purchase_no")));
		$income_material=trim(strip_tags($this->input->post("income_material")));
		$income_commodity=trim(strip_tags($this->input->post("income_commodity")));
		$income_certificate_no=trim(strip_tags($this->input->post("income_certificate_no")));
		$income_po_no=trim(strip_tags($this->input->post("income_po_no")));
		$income_delivery_no=trim(strip_tags($this->input->post("income_delivery_no")));
		$income_heat_no=trim(strip_tags($this->input->post("income_heat_no")));
		$income_createdtime=date("Y-m-d H:i:s");
		$income_createdip=$this->input->ip_address();
		$income_createdid=$this->admin_library->user_id();
		$grade_mark=trim(strip_tags($this->input->post("grade_mark")));
		

		$this->mssql->set("income_supplier_id",$supplier_id);
		$this->mssql->set("income_invoice_no",$income_invoice_no);
		$this->mssql->set("income_purchase_no",$income_purchase_no);
		$this->mssql->set("income_material",$income_material);
		$this->mssql->set("income_commodity",$income_commodity);
		$this->mssql->set("income_certificate_no",$income_certificate_no);
		$this->mssql->set("income_po_no",$income_po_no);
		$this->mssql->set("income_delivery_no",$income_delivery_no);
		$this->mssql->set("income_heat_no",$income_heat_no);
		$this->mssql->set("grade_mark",$grade_mark);
		$this->mssql->set("income_updatedtime",$income_createdtime);
		$this->mssql->set("income_updatedip",$income_createdip);
		$this->mssql->set("income_updatedid",$income_createdid);
		$this->mssql->where("income_id",$income_id);
		$this->mssql->update("income_stud");

	}
	function delete_income($income_id)
	{
		
		$this->mssql->where("income_id",$income_id);
		$this->mssql->delete("income_stud");
		
	}
	
	public function getproduct_name($pro_id=0)
	{
		$this->mssql->limit(1);
		$this->mssql->where("product_id",$pro_id);
		$rs = $this->mssql->get("product_stud")->row_array();
		return $rs;
	}
	
	public function getsupplier_name($sup_id=0)
	{
		$this->mssql->limit(1);
		$this->mssql->where("supplier_id",$sup_id);
		$rs = $this->mssql->get("supplier")->row_array();
		return $rs;
	}
	
	public function getSupplier()
	{
		$rs = $this->mssql->get("supplier")->result_array();
		return $rs;
	}
	public function getCustomer()
	{
		$rs = $this->mssql->get("customer")->result_array();
		return $rs;
	}
	
	public function get_studsize_byid($nutid=0){
		$query = $this->mssql->where('product_id', $nutid)
							->get('product_size_stud')->result_array();
		return $query;
	}
	function get_size($size_id)
	{
		$query = $this->mssql->where('size_id', $size_id)
							->get('product_size_stud')->row_array();
		return $query;
	}

	
	
}