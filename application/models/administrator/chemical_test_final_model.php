<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class chemical_test_final_model extends CI_Model {
	var $_data=array();
	public function __construct()
	{
		parent::__construct();
		$this->mssql = $this->load->database("mssql",true);
	}
	
	public function get_thickness()
	{
				
		$row = $this->mssql->get("thickness_galvanizes")->result_array(); 
		return $row; 
	}
	
	public function get_thickness_by_id($id=0)
	{
		
				$this->mssql->where('id', $id);
		$row = $this->mssql->get("thickness_galvanizes")->row_array(); 
		return $row; 
	}
	
	/* Bolt - Start */
	public function get_finalinfo_bolt_byid($finalid=0){
		$query = $this->mssql->where('final_bolt.final_id', $finalid)
								->get('final_bolt')->row_array();
		return $query;
	}
	
	public function bolt_test_add($finalid=0){
		$this->mssql->where('final_id', @$finalid);
		$this->mssql->delete('chemical_test_bolt');
		
		$this->mssql->where('final_id', @$finalid);
		$this->mssql->delete('chemical_final_bolt');
		
		foreach($this->input->post('chemical_id') as $key=>$value){
			$aData = array(
				'final_id' => $finalid,
				'chemical_id' => $value,
				'chemical_value' => $_POST['chemical_value'][$key],
				'test_value' => $_POST['test_value'][$key],
				'test_createdtime' => date("Y-m-d H:i:s"),
				'test_createdip' => $this->input->ip_address()
			);
			
			
			$this->mssql->insert('chemical_test_bolt', $aData);
			$this->mssql->insert('chemical_final_bolt', $aData);
	
		}
			
	}
	
	public function bolt_thickness_add($finalid=0){
		
			$tData = array(
				'final_id' => $finalid,
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'thickness_createdtime' => date("Y-m-d H:i:s"),
				'thickness_createdip' => $this->input->ip_address()
			);
			$this->mssql->insert('thickness_test_bolt', $tData);
			$this->mssql->insert('thickness_final_bolt', $tData);
		
		
	}
	
	public function update_final_status($finalid=0)
	{
		$this->mssql->set("test_status",$_POST['test_status']);
		$this->mssql->where("final_id",$finalid);
		$this->mssql->update("final_bolt");
	}
	
	public function update_product_qty($size_id=0,$qty=0){
		$row = $this->mssql->where('product_size_bolt.size_id', $size_id)
								
								->get('product_size_bolt')->row_array();
		$old_qty = $row['size_qty'];
		
		$total = $old_qty+$qty;
		
		$this->mssql->set("size_qty",$total);
		$this->mssql->where("size_id",$size_id);
		$this->mssql->update("product_size_bolt");
		
	}
	
	public function get_bolt_test_result($finalid=0, $chemicalid=0){
		$query = $this->mssql->where('final_id', $finalid)
								->where('chemical_id', $chemicalid)
								->get('chemical_test_bolt')->row_array();
		return $query;
	}
	
		/* Chemical test final - Start */
		public function check_test_bolt_status($incomeid=0, $finalid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->where('final_id', $finalid)
									->count_all_results('chemical_final_bolt');
			return $query;
		}
		
		public function get_chemical_bolt($incomeid=0){
			$query = $this->mssql->where('income_bolt.income_id', $incomeid)
									->join('income_bolt','product_chemical_bolt.product_id=income_bolt.income_product_id')
									->get('product_chemical_bolt')->result_array();
			return $query;
		}
		
		public function get_bolt_untest_value($incomeid=0, $chemicalid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->where('chemical_id', $chemicalid)
									->get('chemical_test_bolt')->row_array();
			return $query;
		}
		
		public function get_bolt_test_value($incomeid=0, $finalid=0, $chemicalid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->where('final_id', $finalid)
									->where('chemical_id', $chemicalid)
									->get('chemical_final_bolt')->row_array();
			return $query;
		}
		
		public function get_thicknesstestinfo_byincomeid_bolt($incomeid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->get('thickness_final_bolt')->row_array();
			return $query;
		}
		
		public function get_thicknesstestinfo_byincomeid_bolt_cer($incomeid=0,$final_id=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->where('final_id', $final_id)
									->get('thickness_final_bolt')->row_array();
			return $query;
		}
		
		public function save_final_chemical_bolt($incomeid=0, $finalid=0){
			
			foreach($this->input->post('test_id') as $key=>$testid){
				$aUpdate = array(
					'income_id' => $incomeid,
					'chemical_id' => $_POST['chemical_id'][$key],
					'chemical_value' => $_POST['chemical_value'][$key],
					'test_value' => $_POST['test_value'][$key],
					'final_id' => $finalid
				);
				
				$this->mssql->insert('chemical_final_bolt', $aUpdate);
			}
			
		}
		
		public function update_final_chemical_bolt($finalid=0){
			
			foreach($this->input->post('test_id') as $key=>$testid){
				$aUpdate = array(
					'test_value' => $_POST['test_value'][$key],
					'final_id' => $finalid
				);
				
				$this->mssql->where('test_id', $testid);
				$this->mssql->update('chemical_final_bolt', $aUpdate);
			}
			
		}
		
		public function save_cer_chemical_bolt($finalid=0){
			foreach($this->input->post('test_id') as $key=>$testid){
				$aInsert = array(
					'income_id' => $_POST['income_id'][$key],
					'chemical_id' => $_POST['chemical_id'][$key],
					'chemical_value' => $_POST['chemical_value'][$key],
					'test_value' => $_POST['test_value'][$key],
					'test_createdtime' => date("Y-m-d H:i:s"),
					'test_createdip' => $this->input->ip_address(),
					'test_status' => $_POST['test_status'],
					'final_id' => $finalid
				);
				
				$this->mssql->insert('chemical_cer_bolt', $aInsert);
			}
		}
		
		public function save_final_thickness_bolt($incomeid=0, $finalid=0){
			$aUpdate = array(
				'income_id' => $incomeid,
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'final_id' => $finalid
			);
			
			$this->mssql->insert('thickness_final_bolt', $aUpdate);
		}
		
		public function update_final_thickness_bolt($finalid=0){
			$aUpdate = array(
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'final_id' => $finalid
			);
			
			$this->mssql->where('thickness_test_id', $_POST['thickness_test_id']);
			$this->mssql->update('thickness_final_bolt', $aUpdate);
		}
		
		public function save_cer_thickness_bolt($finalid=0){
			$aInsert = array(
				'income_id' => $_POST['income_id'][0],
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'thickness_createdtime' => date("Y-m-d H:i:s"),
				'thickness_createdip' => $this->input->ip_address(),
				'test_status' => $_POST['test_status'],
				'final_id' => $finalid
			);
			
			$this->mssql->insert('thickness_cer_bolt', $aInsert);
		}
		
		public function update_test_status_bolt($finalid=0){
			$query = $this->mssql->set('test_bolt_status', $_POST['test_status'])
									->where('final_id', $finalid)
									->update('final_bolt');
		}
		
		public function update_test_status_bolt_nut($finalid=0){
			$query = $this->mssql->set('test_nut_status', $_POST['test_status'])
									->where('final_id', $finalid)
									->update('final_bolt');
		}
		
		public function update_test_status_bolt_washer($finalid=0){
			$query = $this->mssql->set('test_washer_status', $_POST['test_status'])
									->where('final_id', $finalid)
									->update('final_bolt');
		}
		
		public function update_stock_bolt($incomeid=0, $finalid=0, $test_status=0){
			$finalinfo = $this->mssql->where('final_bolt_income_id', $incomeid)
										->where('final_id', $finalid)
										->get('final_bolt')->row_array();
			
			if($finalinfo['stock_reduce_bolt_status']==0 && $test_status==2){
			
				$final_bolt_id = $finalinfo['final_bolt_id'];
				$final_bolt_sizeid = $finalinfo['final_bolt_size_id'];
				$final_qty = $finalinfo['final_bolt_qty'];
				
				$sizeinfo = $this->mssql->where('product_id', $final_bolt_id)
										->where('size_id', $final_bolt_sizeid)
										->get('product_size_bolt')->row_array();
				
				$current_qty = $sizeinfo['size_qty'];
				
				$upd_qty = intval($current_qty-$final_qty);
				
				$aUpdate = array(
					'size_qty' => $upd_qty,
					'size_updatedtime' => date("Y-m-d H:i:s"),
					'size_updatedip' => $this->input->ip_address()
				);
				
				$this->mssql->where('size_id', $sizeinfo['size_id']);
				$this->mssql->update('product_size_bolt', $aUpdate);
				
				/* Update `stock_reduce_bolt_status` - Start */
				$this->mssql->set('stock_reduce_bolt_status',1);
				$this->mssql->where('final_id', $finalid);
				$this->mssql->where('final_bolt_income_id', $incomeid);
				$this->mssql->update('final_bolt');
				/* Update `stock_reduce_bolt_status` - End */
			
			}
			
		}
		
		public function update_stock_bolt_nut($incomeid=0, $finalid=0, $test_status=0){
			$finalinfo = $this->mssql->where('final_nut_income_id', $incomeid)
										->where('final_id', $finalid)
										->get('final_bolt')->row_array();
			
			if($finalinfo['stock_reduce_nut_status']==0 && $test_status==2){
				
				$final_nut_id = $finalinfo['final_nut_id'];
				$final_nut_sizeid = $finalinfo['final_nut_size_id'];
				$final_qty = $finalinfo['final_nut_qty'];
				
				$sizeinfo = $this->mssql->where('product_id', $final_nut_id)
										->where('size_id', $final_nut_sizeid)
										->get('product_size_nut')->row_array();
				
				$current_qty = $sizeinfo['size_qty'];
				
				$upd_qty = intval($current_qty-$final_qty);
				
				$aUpdate = array(
					'size_qty' => $upd_qty,
					'size_updatedtime' => date("Y-m-d H:i:s"),
					'size_updatedip' => $this->input->ip_address()
				);
				
				$this->mssql->where('size_id', $sizeinfo['size_id']);
				$this->mssql->update('product_size_nut', $aUpdate);
				
				/* Update `stock_reduce_nut_status` - Start */
				$this->mssql->set('stock_reduce_nut_status',1);
				$this->mssql->where('final_id', $finalid);
				$this->mssql->where('final_nut_income_id', $incomeid);
				$this->mssql->update('final_bolt');
				/* Update `stock_reduce_nut_status` - End */
			}
			
		}
		
		public function update_stock_bolt_washer($incomeid=0, $finalid=0, $test_status=0){
			$finalinfo = $this->mssql->where('final_washer_income_id', $incomeid)
										->where('final_id', $finalid)
										->get('final_bolt')->row_array();
			
			if($finalinfo['stock_reduce_washer_status']==0 && $test_status==2){
			
				$final_washer_id = $finalinfo['final_washer_id'];
				$final_washer_sizeid = $finalinfo['final_washer_size_id'];
				$final_qty = $finalinfo['final_washer_qty'];
				
				$sizeinfo = $this->mssql->where('product_id', $final_washer_id)
										->where('size_id', $final_washer_sizeid)
										->get('product_size_washer')->row_array();
				
				$current_qty = $sizeinfo['size_qty'];
				
				$upd_qty = intval($current_qty-$final_qty);
				
				$aUpdate = array(
					'size_qty' => $upd_qty,
					'size_updatedtime' => date("Y-m-d H:i:s"),
					'size_updatedip' => $this->input->ip_address()
				);
				
				$this->mssql->where('size_id', $sizeinfo['size_id']);
				$this->mssql->update('product_size_washer', $aUpdate);
				
				/* Update `stock_reduce_washer_status' - Start */
				$this->mssql->set('stock_reduce_washer_status',1);
				$this->mssql->where('final_id', $finalid);
				$this->mssql->where('final_washer_income_id', $incomeid);
				$this->mssql->update('final_bolt');
				/* Update `stock_reduce_washer_status` - End */
				
			}
			
		}
		/* Chemical test final - End */
	
	/* Bolt - End */
	
	/* Nut - Start */
		/* Chemical test final - Start */
		public function check_test_nut_main_status($incomeid=0, $finalid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->where('final_id', $finalid)
									->count_all_results('chemical_final_nut');
			return $query;
		}
		
		public function update_status_nut($incomeid=0, $finalid=0, $teststatus=0){
			
			$this->mssql->set('test_status', $teststatus);
			$this->mssql->where('final_nut_income_id', $incomeid);
			$this->mssql->where('final_id', $finalid);
			$this->mssql->update('final_nut');
							
		}
		/* Chemical test final - End */
		
	public function get_finalinfo_nut_byid($finalid=0){
		$query = $this->mssql->where('final_nut.final_id', $finalid)
								->get('final_nut')->row_array();
		return $query;
	}
	
	/*
public function get_chemical_nut($productid=0){
		$query = $this->mssql->where('product_chemical_nut.product_id', $productid)
								->get('product_chemical_nut')->result_array();
		return $query;
	}
*/
	
	public function nut_test_add($finalid=0){
		$this->mssql->where('final_id', @$finalid);
		$this->mssql->delete('chemical_test_nut');
		
		$this->mssql->where('final_id', @$finalid);
		$this->mssql->delete('chemical_final_nut');
		
		foreach($this->input->post('chemical_id') as $key=>$value){
			$aData = array(
				'final_id' => $finalid,
				'chemical_id' => $value,
				'chemical_value' => $_POST['chemical_value'][$key],
				'test_value' => $_POST['test_value'][$key],
				'test_createdtime' => date("Y-m-d H:i:s"),
				'test_createdip' => $this->input->ip_address()
			);
			
			
			$this->mssql->insert('chemical_test_nut', $aData);
			$this->mssql->insert('chemical_final_nut', $aData);
	
		}
			
	}
	
	public function nut_thickness_add($finalid=0){
		
		
			$tData = array(
				'final_id' => $finalid,
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'thickness_createdtime' => date("Y-m-d H:i:s"),
				'thickness_createdip' => $this->input->ip_address()
			);
			$this->mssql->insert('thickness_test_nut', $tData);
			$this->mssql->insert('thickness_final_nut', $tData);
		
		
	}
	
	public function update_final_status_nut($finalid=0)
	{
		$this->mssql->set("test_status",$_POST['test_status']);
		$this->mssql->where("final_id",$finalid);
		$this->mssql->update("final_nut");
	}
	
	public function update_product_nut_qty($size_id=0,$qty=0){
		$row = $this->mssql->where('product_size_nut.size_id', $size_id)
								
								->get('product_size_nut')->row_array();
		$old_qty = $row['size_qty'];
		
		$total = $old_qty+$qty;
		
		$this->mssql->set("size_qty",$total);
		$this->mssql->where("size_id",$size_id);
		$this->mssql->update("product_size_nut");
		
	}
	
	public function get_nut_test_result($finalid=0, $chemicalid=0){
		$query = $this->mssql->where('final_id', $finalid)
								->where('chemical_id', $chemicalid)
								->get('chemical_test_nut')->row_array();
		return $query;
	}
	
		/* Chemical test final - Start */
		public function check_test_nut_status($incomeid=0, $finalid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->where('final_id', $finalid)
									->count_all_results('chemical_final_nut');
			return $query;
		}
		
		public function get_chemical_nut($incomeid=0){
			$query = $this->mssql->where('income_nut.income_id', $incomeid)
									->join('income_nut','product_chemical_nut.product_id=income_nut.income_product_id')
									->get('product_chemical_nut')->result_array();
			return $query;
		}
		
		public function get_nut_untest_value($incomeid=0, $chemicalid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->where('chemical_id', $chemicalid)
									->get('chemical_test_nut')->row_array();
			return $query;
		}
		
		public function get_nut_test_value($incomeid=0, $finalid=0, $chemicalid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->where('final_id', $finalid)
									->where('chemical_id', $chemicalid)
									->get('chemical_final_nut')->row_array();
			return $query;
		}
		
		public function get_thicknesstestinfo_byincomeid_nut($incomeid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->get('thickness_final_nut')->row_array();
			return $query;
		}
		
		public function get_thicknesstestinfo_byincomeid_nut_cer($incomeid=0,$final_id=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->where('final_id', $final_id)
									->get('thickness_final_nut')->row_array();
			return $query;
		}
		
		public function save_final_chemical_nut($incomeid=0, $finalid=0){
			foreach($this->input->post('test_id') as $key=>$testid){
				$aUpdate = array(
					'income_id' => $incomeid,
					'chemical_id' => $_POST['chemical_id'][$key],
					'chemical_value' => $_POST['chemical_value'][$key],
					'test_value' => $_POST['test_value'][$key],
					'final_id' => $finalid
				);
				
				$this->mssql->insert('chemical_final_nut', $aUpdate);
			}
			
		}
		
		public function update_final_chemical_nut($finalid=0){
			
			foreach($this->input->post('test_id') as $key=>$testid){
				$aUpdate = array(
					'test_value' => $_POST['test_value'][$key],
					'final_id' => $finalid
				);
				
				$this->mssql->where('test_id', $testid);
				$this->mssql->update('chemical_final_nut', $aUpdate);
			}
			
		}
		
		public function save_cer_chemical_nut($finalid=0){
			foreach($this->input->post('test_id') as $key=>$testid){
				$aInsert = array(
					'income_id' => $_POST['income_id'][$key],
					'chemical_id' => $_POST['chemical_id'][$key],
					'chemical_value' => $_POST['chemical_value'][$key],
					'test_value' => $_POST['test_value'][$key],
					'test_createdtime' => date("Y-m-d H:i:s"),
					'test_createdip' => $this->input->ip_address(),
					'test_status' => $_POST['test_status'],
					'final_id' => $finalid
				);
				
				$this->mssql->insert('chemical_cer_nut', $aInsert);
			}
		}
		
		public function save_final_thickness_nut($incomeid=0, $finalid=0){
			$aUpdate = array(
				'income_id' => $incomeid,
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'final_id' => $finalid
			);
			
			$this->mssql->insert('thickness_final_nut', $aUpdate);
		}
		
		public function update_final_thickness_nut($finalid=0){
			$aUpdate = array(
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'final_id' => $finalid
			);
			
			$this->mssql->where('thickness_test_id', $_POST['thickness_test_id']);
			$this->mssql->update('thickness_final_nut', $aUpdate);
		}
		
		public function save_cer_thickness_nut($finalid=0){
			$aInsert = array(
				'income_id' => $_POST['income_id'][0],
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'thickness_createdtime' => date("Y-m-d H:i:s"),
				'thickness_createdip' => $this->input->ip_address(),
				'test_status' => $_POST['test_status'],
				'final_id' => $finalid
			);
			
			$this->mssql->insert('thickness_cer_nut', $aInsert);
		}
		
		public function update_stock_nut($incomeid=0, $finalid=0){
			$finalinfo = $this->mssql->where('final_nut_income_id', $incomeid)
										->where('final_id', $finalid)
										->get('final_bolt')->row_array();
			
			$final_nut_id = $finalinfo['final_nut_id'];
			$final_nut_sizeid = $finalinfo['final_nut_size_id'];
			$final_qty = $finalinfo['final_nut_qty'];
			
			$sizeinfo = $this->mssql->where('product_id', $final_nut_id)
									->where('size_id', $final_nut_sizeid)
									->get('product_size_nut')->row_array();
			
			$current_qty = $sizeinfo['size_qty'];
			
			$upd_qty = intval($current_qty-$final_qty);
			
			$aUpdate = array(
				'size_qty' => $upd_qty,
				'size_updatedtime' => date("Y-m-d H:i:s"),
				'size_updatedip' => $this->input->ip_address()
			);
			
			$this->mssql->where('size_id', $sizeinfo['size_id']);
			$this->mssql->update('product_size_nut', $aUpdate);
			
		}
		
		public function update_stock_main_nut($incomeid=0, $finalid=0){
			$finalinfo = $this->mssql->where('final_nut_income_id', $incomeid)
										->where('final_id', $finalid)
										->get('final_nut')->row_array();
			
			$final_nut_id = $finalinfo['final_nut_id'];
			$final_nut_sizeid = $finalinfo['final_nut_size_id'];
			$final_qty = $finalinfo['final_nut_qty'];
			
			$sizeinfo = $this->mssql->where('product_id', $final_nut_id)
									->where('size_id', $final_nut_sizeid)
									->get('product_size_nut')->row_array();
			
			$current_qty = $sizeinfo['size_qty'];
			
			$upd_qty = intval($current_qty-$final_qty);
			
			$aUpdate = array(
				'size_qty' => $upd_qty,
				'size_updatedtime' => date("Y-m-d H:i:s"),
				'size_updatedip' => $this->input->ip_address()
			);
			
			$this->mssql->where('size_id', $sizeinfo['size_id']);
			$this->mssql->update('product_size_nut', $aUpdate);
			
		}
		/* Chemical test final - End */
	
	/* Nut - End */
	
	/* Washer - Start */
	public function get_finalinfo_washer_byid($finalid=0){
		$query = $this->mssql->where('final_washer.final_id', $finalid)
								
								->get('final_washer')->row_array();
		return $query;
	}
	
	/*
public function get_chemical_washer($productid=0){
		$query = $this->mssql->where('product_chemical_washer.product_id', $productid)
								->get('product_chemical_washer')->result_array();
		return $query;
	}
*/
	
	public function washer_test_add($finalid=0){
		$this->mssql->where('final_id', @$finalid);
		$this->mssql->delete('chemical_test_washer');
		
		$this->mssql->where('final_id', @$finalid);
		$this->mssql->delete('chemical_final_washer');
		
		foreach($this->input->post('chemical_id') as $key=>$value){
			$aData = array(
				'final_id' => $finalid,
				'chemical_id' => $value,
				'chemical_value' => $_POST['chemical_value'][$key],
				'test_value' => $_POST['test_value'][$key],
				'test_createdtime' => date("Y-m-d H:i:s"),
				'test_createdip' => $this->input->ip_address()
			);
			
			
			$this->mssql->insert('chemical_test_washer', $aData);
			$this->mssql->insert('chemical_final_washer', $aData);
	
		}
			
	}
	
	public function washer_thickness_add($finalid=0){
			
			
		
			$tData = array(
				'final_id' => $finalid,
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'thickness_createdtime' => date("Y-m-d H:i:s"),
				'thickness_createdip' => $this->input->ip_address()
			);
			$this->mssql->insert('thickness_test_washer', $tData);
			$this->mssql->insert('thickness_final_washer', $tData);
		
		
	}
	
	public function update_final_status_washer($finalid=0)
	{
		$this->mssql->set("test_status",$_POST['test_status']);
		$this->mssql->where("final_id",$finalid);
		$this->mssql->update("final_washer");
	}
	
	public function update_product_washer_qty($size_id=0,$qty=0){
		$row = $this->mssql->where('product_size_washer.size_id', $size_id)
								
								->get('product_size_washer')->row_array();
		$old_qty = $row['size_qty'];
		
		$total = $old_qty+$qty;
		
		$this->mssql->set("size_qty",$total);
		$this->mssql->where("size_id",$size_id);
		$this->mssql->update("product_size_washer");
		
	}
	
	public function get_washer_test_result($finalid=0, $chemicalid=0){
		$query = $this->mssql->where('final_id', $finalid)
								->where('chemical_id', $chemicalid)
								->get('chemical_test_washer')->row_array();
		return $query;
	}
		
		/* Chemical test final - Start */
		public function check_test_washer_main_status($incomeid=0, $finalid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->where('final_id', $finalid)
									->count_all_results('chemical_final_washer');
			return $query;
		}
		
		public function update_status_washer($incomeid=0, $finalid=0, $teststatus=0){
			
			$this->mssql->set('test_status', $teststatus);
			$this->mssql->where('final_washer_income_id', $incomeid);
			$this->mssql->where('final_id', $finalid);
			$this->mssql->update('final_washer');
							
		}
		
		public function check_test_washer_status($incomeid=0, $finalid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->where('final_id', $finalid)
									->count_all_results('chemical_final_washer');
			return $query;
		}
		
		public function get_chemical_washer($incomeid=0){
			$query = $this->mssql->where('income_washer.income_id', $incomeid)
									->join('income_washer','product_chemical_washer.product_id=income_washer.income_product_id')
									->get('product_chemical_washer')->result_array();
			return $query;
		}
		
		public function get_washer_untest_value($incomeid=0, $chemicalid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->where('chemical_id', $chemicalid)
									->get('chemical_test_washer')->row_array();
			return $query;
		}
		
		public function get_washer_test_value($incomeid=0, $finalid=0, $chemicalid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->where('final_id', $finalid)
									->where('chemical_id', $chemicalid)
									->get('chemical_final_washer')->row_array();
			return $query;
		}
		
		public function get_thicknesstestinfo_byincomeid_washer($incomeid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->get('thickness_final_washer')->row_array();
			return $query;
		}
		
		public function get_thicknesstestinfo_byincomeid_washer_cer($incomeid=0,$final_id=0){
			$query = $this->mssql->where('income_id', $incomeid)
								->where('final_id', $final_id)
									->get('thickness_final_washer')->row_array();
			return $query;
		}
		
		public function save_final_chemical_washer($incomeid=0, $finalid=0){
			
			foreach($this->input->post('test_id') as $key=>$testid){
				$aUpdate = array(
					'income_id' => $incomeid,
					'chemical_id' => $_POST['chemical_id'][$key],
					'chemical_value' => $_POST['chemical_value'][$key],
					'test_value' => $_POST['test_value'][$key],
					'final_id' => $finalid
				);
				
				$this->mssql->insert('chemical_final_washer', $aUpdate);
			}
			
		}
		
		public function update_final_chemical_washer($finalid=0){
			
			foreach($this->input->post('test_id') as $key=>$testid){
				$aUpdate = array(
					'test_value' => $_POST['test_value'][$key],
					'final_id' => $finalid
				);
				
				$this->mssql->where('test_id', $testid);
				$this->mssql->update('chemical_final_washer', $aUpdate);
			}
			
		}
		
		public function save_cer_chemical_washer($finalid=0){
			foreach($this->input->post('test_id') as $key=>$testid){
				$aInsert = array(
					'income_id' => $_POST['income_id'][$key],
					'chemical_id' => $_POST['chemical_id'][$key],
					'chemical_value' => $_POST['chemical_value'][$key],
					'test_value' => $_POST['test_value'][$key],
					'test_createdtime' => date("Y-m-d H:i:s"),
					'test_createdip' => $this->input->ip_address(),
					'test_status' => $_POST['test_status'],
					'final_id' => $finalid
				);
				
				$this->mssql->insert('chemical_cer_washer', $aInsert);
			}
		}
		
		public function save_final_thickness_washer($incomeid=0, $finalid=0){
			$aUpdate = array(
				'income_id' => $incomeid,
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'final_id' => $finalid
			);
			
			$this->mssql->insert('thickness_final_washer', $aUpdate);
		}
		
		public function update_final_thickness_washer($finalid=0){
			$aUpdate = array(
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'final_id' => $finalid
			);
			
			$this->mssql->where('thickness_test_id', $_POST['thickness_test_id']);
			$this->mssql->update('thickness_final_washer', $aUpdate);
		}
		
		public function save_cer_thickness_washer($finalid=0){
			$aInsert = array(
				'income_id' => $_POST['income_id'][0],
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'thickness_createdtime' => date("Y-m-d H:i:s"),
				'thickness_createdip' => $this->input->ip_address(),
				'test_status' => $_POST['test_status'],
				'final_id' => $finalid
			);
			
			$this->mssql->insert('thickness_cer_washer', $aInsert);
		}
		
		public function update_stock_washer($incomeid=0, $finalid=0){
			$finalinfo = $this->mssql->where('final_washer_income_id', $incomeid)
										->where('final_id', $finalid)
										->get('final_bolt')->row_array();
			
			$final_washer_id = $finalinfo['final_washer_id'];
			$final_washer_sizeid = $finalinfo['final_washer_size_id'];
			$final_qty = $finalinfo['final_washer_qty'];
			
			$sizeinfo = $this->mssql->where('product_id', $final_washer_id)
									->where('size_id', $final_washer_sizeid)
									->get('product_size_washer')->row_array();
			
			$current_qty = $sizeinfo['size_qty'];
			
			$upd_qty = intval($current_qty-$final_qty);
			
			$aUpdate = array(
				'size_qty' => $upd_qty,
				'size_updatedtime' => date("Y-m-d H:i:s"),
				'size_updatedip' => $this->input->ip_address()
			);
			
			$this->mssql->where('size_id', $sizeinfo['size_id']);
			$this->mssql->update('product_size_washer', $aUpdate);
			
		}
		
		public function update_stock_main_washer($incomeid=0, $finalid=0){
			$finalinfo = $this->mssql->where('final_washer_income_id', $incomeid)
										->where('final_id', $finalid)
										->get('final_washer')->row_array();
			
			$final_washer_id = $finalinfo['final_washer_id'];
			$final_washer_sizeid = $finalinfo['final_washer_size_id'];
			$final_qty = $finalinfo['final_washer_qty'];
			
			$sizeinfo = $this->mssql->where('product_id', $final_washer_id)
									->where('size_id', $final_washer_sizeid)
									->get('product_size_washer')->row_array();
			
			$current_qty = $sizeinfo['size_qty'];
			
			$upd_qty = intval($current_qty-$final_qty);
			
			$aUpdate = array(
				'size_qty' => $upd_qty,
				'size_updatedtime' => date("Y-m-d H:i:s"),
				'size_updatedip' => $this->input->ip_address()
			);
			
			$this->mssql->where('size_id', $sizeinfo['size_id']);
			$this->mssql->update('product_size_washer', $aUpdate);
			
		}
		/* Chemical test final - End */
	
	/* Washer - End */
	
	/* Spring - Start */
	public function get_finalinfo_spring_byid($finalid=0){
		$query = $this->mssql->where('final_spring.final_id', $finalid)
								
								->get('final_spring')->row_array();
		return $query;
	}
	
	/*
public function get_chemical_spring($productid=0){
		$query = $this->mssql->where('product_chemical_spring.product_id', $productid)
								->get('product_chemical_spring')->result_array();
		return $query;
	}
*/
	
	public function spring_test_add($finalid=0){
		$this->mssql->where('final_id', @$finalid);
		$this->mssql->delete('chemical_test_spring');
		
		$this->mssql->where('final_id', @$finalid);
		$this->mssql->delete('chemical_final_spring');
		
		foreach($this->input->post('chemical_id') as $key=>$value){
			$aData = array(
				'final_id' => $finalid,
				'chemical_id' => $value,
				'chemical_value' => $_POST['chemical_value'][$key],
				'test_value' => $_POST['test_value'][$key],
				'test_createdtime' => date("Y-m-d H:i:s"),
				'test_createdip' => $this->input->ip_address()
			);
			
			
			$this->mssql->insert('chemical_test_spring', $aData);
			$this->mssql->insert('chemical_final_spring', $aData);
	
		}
			
	}
	
	public function spring_thickness_add($finalid=0){
			
			
		
			$tData = array(
				'final_id' => $finalid,
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'thickness_createdtime' => date("Y-m-d H:i:s"),
				'thickness_createdip' => $this->input->ip_address()
			);
			$this->mssql->insert('thickness_test_spring', $tData);
			$this->mssql->insert('thickness_final_spring', $tData);
		
		
	}
	
	public function update_final_status_spring($finalid=0)
	{
		$this->mssql->set("test_status",$_POST['test_status']);
		$this->mssql->where("final_id",$finalid);
		$this->mssql->update("final_spring");
	}
	
	public function update_product_spring_qty($size_id=0,$qty=0){
		$row = $this->mssql->where('product_size_spring.size_id', $size_id)
								
								->get('product_size_spring')->row_array();
		$old_qty = $row['size_qty'];
		
		$total = $old_qty+$qty;
		
		$this->mssql->set("size_qty",$total);
		$this->mssql->where("size_id",$size_id);
		$this->mssql->update("product_size_spring");
		
	}
	
	public function get_spring_test_result($finalid=0, $chemicalid=0){
		$query = $this->mssql->where('final_id', $finalid)
								->where('chemical_id', $chemicalid)
								->get('chemical_test_spring')->row_array();
		return $query;
	}
		
		/* Chemical test final - Start */
		public function check_test_spring_main_status($incomeid=0, $finalid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->where('final_id', $finalid)
									->count_all_results('chemical_final_spring');
			return $query;
		}
		
		public function update_status_spring($incomeid=0, $finalid=0, $teststatus=0){
			
			$this->mssql->set('test_status', $teststatus);
			$this->mssql->where('final_spring_income_id', $incomeid);
			$this->mssql->where('final_id', $finalid);
			$this->mssql->update('final_spring');
							
		}
		
		public function check_test_spring_status($incomeid=0, $finalid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->where('final_id', $finalid)
									->count_all_results('chemical_final_spring');
			return $query;
		}
		
		public function get_chemical_spring($incomeid=0){
			$query = $this->mssql->where('income_spring.income_id', $incomeid)
									->join('income_spring','product_chemical_spring.product_id=income_spring.income_product_id')
									->get('product_chemical_spring')->result_array();
			return $query;
		}
		
		public function get_spring_untest_value($incomeid=0, $chemicalid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->where('chemical_id', $chemicalid)
									->get('chemical_test_spring')->row_array();
			return $query;
		}
		
		public function get_spring_test_value($incomeid=0, $finalid=0, $chemicalid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->where('final_id', $finalid)
									->where('chemical_id', $chemicalid)
									->get('chemical_final_spring')->row_array();
			return $query;
		}
		
		public function get_thicknesstestinfo_byincomeid_spring($incomeid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->get('thickness_final_spring')->row_array();
			return $query;
		}
		
		public function get_thicknesstestinfo_byincomeid_spring_cer($incomeid=0,$final_id=0){
			$query = $this->mssql->where('income_id', $incomeid)
								->where('final_id', $final_id)
									->get('thickness_final_spring')->row_array();
			return $query;
		}
		
		public function save_final_chemical_spring($incomeid=0, $finalid=0){
			
			foreach($this->input->post('test_id') as $key=>$testid){
				$aUpdate = array(
					'income_id' => $incomeid,
					'chemical_id' => $_POST['chemical_id'][$key],
					'chemical_value' => $_POST['chemical_value'][$key],
					'test_value' => $_POST['test_value'][$key],
					'final_id' => $finalid
				);
				
				$this->mssql->insert('chemical_final_spring', $aUpdate);
			}
			
		}
		
		public function update_final_chemical_spring($finalid=0){
			
			foreach($this->input->post('test_id') as $key=>$testid){
				$aUpdate = array(
					'test_value' => $_POST['test_value'][$key],
					'final_id' => $finalid
				);
				
				$this->mssql->where('test_id', $testid);
				$this->mssql->update('chemical_final_spring', $aUpdate);
			}
			
		}
		
		public function save_cer_chemical_spring($finalid=0){
			foreach($this->input->post('test_id') as $key=>$testid){
				$aInsert = array(
					'income_id' => $_POST['income_id'][$key],
					'chemical_id' => $_POST['chemical_id'][$key],
					'chemical_value' => $_POST['chemical_value'][$key],
					'test_value' => $_POST['test_value'][$key],
					'test_createdtime' => date("Y-m-d H:i:s"),
					'test_createdip' => $this->input->ip_address(),
					'test_status' => $_POST['test_status'],
					'final_id' => $finalid
				);
				
				$this->mssql->insert('chemical_cer_spring', $aInsert);
			}
		}
		
		public function save_final_thickness_spring($incomeid=0, $finalid=0){
			$aUpdate = array(
				'income_id' => $incomeid,
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'final_id' => $finalid
			);
			
			$this->mssql->insert('thickness_final_spring', $aUpdate);
		}
		
		public function update_final_thickness_spring($finalid=0){
			$aUpdate = array(
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'final_id' => $finalid
			);
			
			$this->mssql->where('thickness_test_id', $_POST['thickness_test_id']);
			$this->mssql->update('thickness_final_spring', $aUpdate);
		}
		
		public function save_cer_thickness_spring($finalid=0){
			$aInsert = array(
				'income_id' => $_POST['income_id'][0],
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'thickness_createdtime' => date("Y-m-d H:i:s"),
				'thickness_createdip' => $this->input->ip_address(),
				'test_status' => $_POST['test_status'],
				'final_id' => $finalid
			);
			
			$this->mssql->insert('thickness_cer_spring', $aInsert);
		}
		
		public function update_stock_spring($incomeid=0, $finalid=0){
			$finalinfo = $this->mssql->where('final_spring_income_id', $incomeid)
										->where('final_id', $finalid)
										->get('final_bolt')->row_array();
			
			$final_spring_id = $finalinfo['final_spring_id'];
			$final_spring_sizeid = $finalinfo['final_spring_size_id'];
			$final_qty = $finalinfo['final_spring_qty'];
			
			$sizeinfo = $this->mssql->where('product_id', $final_spring_id)
									->where('size_id', $final_spring_sizeid)
									->get('product_size_spring')->row_array();
			
			$current_qty = $sizeinfo['size_qty'];
			
			$upd_qty = intval($current_qty-$final_qty);
			
			$aUpdate = array(
				'size_qty' => $upd_qty,
				'size_updatedtime' => date("Y-m-d H:i:s"),
				'size_updatedip' => $this->input->ip_address()
			);
			
			$this->mssql->where('size_id', $sizeinfo['size_id']);
			$this->mssql->update('product_size_spring', $aUpdate);
			
		}
		
		public function update_stock_main_spring($incomeid=0, $finalid=0){
			$finalinfo = $this->mssql->where('final_spring_income_id', $incomeid)
										->where('final_id', $finalid)
										->get('final_spring')->row_array();
			
			$final_spring_id = $finalinfo['final_spring_id'];
			$final_spring_sizeid = $finalinfo['final_spring_size_id'];
			$final_qty = $finalinfo['final_spring_qty'];
			
			$sizeinfo = $this->mssql->where('product_id', $final_spring_id)
									->where('size_id', $final_spring_sizeid)
									->get('product_size_spring')->row_array();
			
			$current_qty = $sizeinfo['size_qty'];
			
			$upd_qty = intval($current_qty-$final_qty);
			
			$aUpdate = array(
				'size_qty' => $upd_qty,
				'size_updatedtime' => date("Y-m-d H:i:s"),
				'size_updatedip' => $this->input->ip_address()
			);
			
			$this->mssql->where('size_id', $sizeinfo['size_id']);
			$this->mssql->update('product_size_spring', $aUpdate);
			
		}
		/* Chemical test final - End */
	
	/* Spring - End */
	
	/* Taper - Start */
	public function get_finalinfo_taper_byid($finalid=0){
		$query = $this->mssql->where('final_taper.final_id', $finalid)
								
								->get('final_taper')->row_array();
		return $query;
	}
	
	/*
public function get_chemical_taper($productid=0){
		$query = $this->mssql->where('product_chemical_taper.product_id', $productid)
								->get('product_chemical_taper')->result_array();
		return $query;
	}
*/
	
	public function taper_test_add($finalid=0){
		$this->mssql->where('final_id', @$finalid);
		$this->mssql->delete('chemical_test_taper');
		
		$this->mssql->where('final_id', @$finalid);
		$this->mssql->delete('chemical_final_taper');
		
		foreach($this->input->post('chemical_id') as $key=>$value){
			$aData = array(
				'final_id' => $finalid,
				'chemical_id' => $value,
				'chemical_value' => $_POST['chemical_value'][$key],
				'test_value' => $_POST['test_value'][$key],
				'test_createdtime' => date("Y-m-d H:i:s"),
				'test_createdip' => $this->input->ip_address()
			);
			
			
			$this->mssql->insert('chemical_test_taper', $aData);
			$this->mssql->insert('chemical_final_taper', $aData);
	
		}
			
	}
	
	public function taper_thickness_add($finalid=0){
			
			
		
			$tData = array(
				'final_id' => $finalid,
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'thickness_createdtime' => date("Y-m-d H:i:s"),
				'thickness_createdip' => $this->input->ip_address()
			);
			$this->mssql->insert('thickness_test_taper', $tData);
			$this->mssql->insert('thickness_final_taper', $tData);
		
		
	}
	
	public function update_final_status_taper($finalid=0)
	{
		$this->mssql->set("test_status",$_POST['test_status']);
		$this->mssql->where("final_id",$finalid);
		$this->mssql->update("final_taper");
	}
	
	public function update_product_taper_qty($size_id=0,$qty=0){
		$row = $this->mssql->where('product_size_taper.size_id', $size_id)
								
								->get('product_size_taper')->row_array();
		$old_qty = $row['size_qty'];
		
		$total = $old_qty+$qty;
		
		$this->mssql->set("size_qty",$total);
		$this->mssql->where("size_id",$size_id);
		$this->mssql->update("product_size_taper");
		
	}
	
	public function get_taper_test_result($finalid=0, $chemicalid=0){
		$query = $this->mssql->where('final_id', $finalid)
								->where('chemical_id', $chemicalid)
								->get('chemical_test_taper')->row_array();
		return $query;
	}
		
		/* Chemical test final - Start */
		public function check_test_taper_main_status($incomeid=0, $finalid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->where('final_id', $finalid)
									->count_all_results('chemical_final_taper');
			return $query;
		}
		
		public function update_status_taper($incomeid=0, $finalid=0, $teststatus=0){
			
			$this->mssql->set('test_status', $teststatus);
			$this->mssql->where('final_taper_income_id', $incomeid);
			$this->mssql->where('final_id', $finalid);
			$this->mssql->update('final_taper');
							
		}
		
		public function check_test_taper_status($incomeid=0, $finalid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->where('final_id', $finalid)
									->count_all_results('chemical_final_taper');
			return $query;
		}
		
		public function get_chemical_taper($incomeid=0){
			$query = $this->mssql->where('income_taper.income_id', $incomeid)
									->join('income_taper','product_chemical_taper.product_id=income_taper.income_product_id')
									->get('product_chemical_taper')->result_array();
			return $query;
		}
		
		public function get_taper_untest_value($incomeid=0, $chemicalid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->where('chemical_id', $chemicalid)
									->get('chemical_test_taper')->row_array();
			return $query;
		}
		
		public function get_taper_test_value($incomeid=0, $finalid=0, $chemicalid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->where('final_id', $finalid)
									->where('chemical_id', $chemicalid)
									->get('chemical_final_taper')->row_array();
			return $query;
		}
		
		public function get_thicknesstestinfo_byincomeid_taper($incomeid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->get('thickness_final_taper')->row_array();
			return $query;
		}
		
		public function get_thicknesstestinfo_byincomeid_taper_cer($incomeid=0,$final_id=0){
			$query = $this->mssql->where('income_id', $incomeid)
								->where('final_id', $final_id)
									->get('thickness_final_taper')->row_array();
			return $query;
		}
		
		public function save_final_chemical_taper($incomeid=0, $finalid=0){
			
			foreach($this->input->post('test_id') as $key=>$testid){
				$aUpdate = array(
					'income_id' => $incomeid,
					'chemical_id' => $_POST['chemical_id'][$key],
					'chemical_value' => $_POST['chemical_value'][$key],
					'test_value' => $_POST['test_value'][$key],
					'final_id' => $finalid
				);
				
				$this->mssql->insert('chemical_final_taper', $aUpdate);
			}
			
		}
		
		public function update_final_chemical_taper($finalid=0){
			
			foreach($this->input->post('test_id') as $key=>$testid){
				$aUpdate = array(
					'test_value' => $_POST['test_value'][$key],
					'final_id' => $finalid
				);
				
				$this->mssql->where('test_id', $testid);
				$this->mssql->update('chemical_final_taper', $aUpdate);
			}
			
		}
		
		public function save_cer_chemical_taper($finalid=0){
			foreach($this->input->post('test_id') as $key=>$testid){
				$aInsert = array(
					'income_id' => $_POST['income_id'][$key],
					'chemical_id' => $_POST['chemical_id'][$key],
					'chemical_value' => $_POST['chemical_value'][$key],
					'test_value' => $_POST['test_value'][$key],
					'test_createdtime' => date("Y-m-d H:i:s"),
					'test_createdip' => $this->input->ip_address(),
					'test_status' => $_POST['test_status'],
					'final_id' => $finalid
				);
				
				$this->mssql->insert('chemical_cer_taper', $aInsert);
			}
		}
		
		public function save_final_thickness_taper($incomeid=0, $finalid=0){
			$aUpdate = array(
				'income_id' => $incomeid,
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'final_id' => $finalid
			);
			
			$this->mssql->insert('thickness_final_taper', $aUpdate);
		}
		
		public function update_final_thickness_taper($finalid=0){
			$aUpdate = array(
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'final_id' => $finalid
			);
			
			$this->mssql->where('thickness_test_id', $_POST['thickness_test_id']);
			$this->mssql->update('thickness_final_taper', $aUpdate);
		}
		
		public function save_cer_thickness_taper($finalid=0){
			$aInsert = array(
				'income_id' => $_POST['income_id'][0],
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'thickness_createdtime' => date("Y-m-d H:i:s"),
				'thickness_createdip' => $this->input->ip_address(),
				'test_status' => $_POST['test_status'],
				'final_id' => $finalid
			);
			
			$this->mssql->insert('thickness_cer_taper', $aInsert);
		}
		
		public function update_stock_taper($incomeid=0, $finalid=0){
			$finalinfo = $this->mssql->where('final_taper_income_id', $incomeid)
										->where('final_id', $finalid)
										->get('final_bolt')->row_array();
			
			$final_taper_id = $finalinfo['final_taper_id'];
			$final_taper_sizeid = $finalinfo['final_taper_size_id'];
			$final_qty = $finalinfo['final_taper_qty'];
			
			$sizeinfo = $this->mssql->where('product_id', $final_taper_id)
									->where('size_id', $final_taper_sizeid)
									->get('product_size_taper')->row_array();
			
			$current_qty = $sizeinfo['size_qty'];
			
			$upd_qty = intval($current_qty-$final_qty);
			
			$aUpdate = array(
				'size_qty' => $upd_qty,
				'size_updatedtime' => date("Y-m-d H:i:s"),
				'size_updatedip' => $this->input->ip_address()
			);
			
			$this->mssql->where('size_id', $sizeinfo['size_id']);
			$this->mssql->update('product_size_taper', $aUpdate);
			
		}
		
		public function update_stock_main_taper($incomeid=0, $finalid=0){
			$finalinfo = $this->mssql->where('final_taper_income_id', $incomeid)
										->where('final_id', $finalid)
										->get('final_taper')->row_array();
			
			$final_taper_id = $finalinfo['final_taper_id'];
			$final_taper_sizeid = $finalinfo['final_taper_size_id'];
			$final_qty = $finalinfo['final_taper_qty'];
			
			$sizeinfo = $this->mssql->where('product_id', $final_taper_id)
									->where('size_id', $final_taper_sizeid)
									->get('product_size_taper')->row_array();
			
			$current_qty = $sizeinfo['size_qty'];
			
			$upd_qty = intval($current_qty-$final_qty);
			
			$aUpdate = array(
				'size_qty' => $upd_qty,
				'size_updatedtime' => date("Y-m-d H:i:s"),
				'size_updatedip' => $this->input->ip_address()
			);
			
			$this->mssql->where('size_id', $sizeinfo['size_id']);
			$this->mssql->update('product_size_taper', $aUpdate);
			
		}
		/* Chemical test final - End */
	
	/* Taper - End */
	
	/* stud - Start */
	public function get_finalinfo_stud_byid($finalid=0){
		$query = $this->mssql->where('final_stud.final_id', $finalid)
								
								->get('final_stud')->row_array();
		return $query;
	}
	
	/*
public function get_chemical_stud($productid=0){
		$query = $this->mssql->where('product_chemical_stud.product_id', $productid)
								->get('product_chemical_stud')->result_array();
		return $query;
	}
*/
	
	public function stud_test_add($finalid=0){
		$this->mssql->where('final_id', @$finalid);
		$this->mssql->delete('chemical_test_stud');
		
		$this->mssql->where('final_id', @$finalid);
		$this->mssql->delete('chemical_final_stud');
		
		foreach($this->input->post('chemical_id') as $key=>$value){
			$aData = array(
				'final_id' => $finalid,
				'chemical_id' => $value,
				'chemical_value' => $_POST['chemical_value'][$key],
				'test_value' => $_POST['test_value'][$key],
				'test_createdtime' => date("Y-m-d H:i:s"),
				'test_createdip' => $this->input->ip_address()
			);
			
			
			$this->mssql->insert('chemical_test_stud', $aData);
			$this->mssql->insert('chemical_final_stud', $aData);
	
		}
			
	}
	
	public function stud_thickness_add($finalid=0){
			
			
		
			$tData = array(
				'final_id' => $finalid,
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'thickness_createdtime' => date("Y-m-d H:i:s"),
				'thickness_createdip' => $this->input->ip_address()
			);
			$this->mssql->insert('thickness_test_stud', $tData);
			$this->mssql->insert('thickness_final_stud', $tData);
		
		
	}
	
	public function update_final_status_stud($finalid=0)
	{
		$this->mssql->set("test_status",$_POST['test_status']);
		$this->mssql->where("final_id",$finalid);
		$this->mssql->update("final_stud");
	}
	
	public function update_product_stud_qty($size_id=0,$qty=0){
		$row = $this->mssql->where('product_size_stud.size_id', $size_id)
								
								->get('product_size_stud')->row_array();
		$old_qty = $row['size_qty'];
		
		$total = $old_qty+$qty;
		
		$this->mssql->set("size_qty",$total);
		$this->mssql->where("size_id",$size_id);
		$this->mssql->update("product_size_stud");
		
	}
	
	public function get_stud_test_result($finalid=0, $chemicalid=0){
		$query = $this->mssql->where('final_id', $finalid)
								->where('chemical_id', $chemicalid)
								->get('chemical_test_stud')->row_array();
		return $query;
	}
		
		/* Chemical test final - Start */
		public function check_test_stud_main_status($incomeid=0, $finalid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->where('final_id', $finalid)
									->count_all_results('chemical_final_stud');
			return $query;
		}
		
		public function update_status_stud($incomeid=0, $finalid=0, $teststatus=0){
			
			$this->mssql->set('test_status', $teststatus);
			$this->mssql->where('final_stud_income_id', $incomeid);
			$this->mssql->where('final_id', $finalid);
			$this->mssql->update('final_stud');
							
		}
		
		public function check_test_stud_status($incomeid=0, $finalid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->where('final_id', $finalid)
									->count_all_results('chemical_final_stud');
			return $query;
		}
		
		public function get_chemical_stud($incomeid=0){
			$query = $this->mssql->where('income_stud.income_id', $incomeid)
									->join('income_stud','product_chemical_stud.product_id=income_stud.income_product_id')
									->get('product_chemical_stud')->result_array();
			return $query;
		}
		
		public function get_stud_untest_value($incomeid=0, $chemicalid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->where('chemical_id', $chemicalid)
									->get('chemical_test_stud')->row_array();
			return $query;
		}
		
		public function get_stud_test_value($incomeid=0, $finalid=0, $chemicalid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->where('final_id', $finalid)
									->where('chemical_id', $chemicalid)
									->get('chemical_final_stud')->row_array();
			return $query;
		}
		
		public function get_thicknesstestinfo_byincomeid_stud($incomeid=0){
			$query = $this->mssql->where('income_id', $incomeid)
									->get('thickness_final_stud')->row_array();
			return $query;
		}
		
		public function get_thicknesstestinfo_byincomeid_stud_cer($incomeid=0,$final_id=0){
			$query = $this->mssql->where('income_id', $incomeid)
								->where('final_id', $final_id)
									->get('thickness_final_stud')->row_array();
			return $query;
		}
		
		public function save_final_chemical_stud($incomeid=0, $finalid=0){
			
			foreach($this->input->post('test_id') as $key=>$testid){
				$aUpdate = array(
					'income_id' => $incomeid,
					'chemical_id' => $_POST['chemical_id'][$key],
					'chemical_value' => $_POST['chemical_value'][$key],
					'test_value' => $_POST['test_value'][$key],
					'final_id' => $finalid
				);
				
				$this->mssql->insert('chemical_final_stud', $aUpdate);
			}
			
		}
		
		public function update_final_chemical_stud($finalid=0){
			
			foreach($this->input->post('test_id') as $key=>$testid){
				$aUpdate = array(
					'test_value' => $_POST['test_value'][$key],
					'final_id' => $finalid
				);
				
				$this->mssql->where('test_id', $testid);
				$this->mssql->update('chemical_final_stud', $aUpdate);
			}
			
		}
		
		public function save_cer_chemical_stud($finalid=0){
			foreach($this->input->post('test_id') as $key=>$testid){
				$aInsert = array(
					'income_id' => $_POST['income_id'][$key],
					'chemical_id' => $_POST['chemical_id'][$key],
					'chemical_value' => $_POST['chemical_value'][$key],
					'test_value' => $_POST['test_value'][$key],
					'test_createdtime' => date("Y-m-d H:i:s"),
					'test_createdip' => $this->input->ip_address(),
					'test_status' => $_POST['test_status'],
					'final_id' => $finalid
				);
				
				$this->mssql->insert('chemical_cer_stud', $aInsert);
			}
		}
		
		public function save_final_thickness_stud($incomeid=0, $finalid=0){
			$aUpdate = array(
				'income_id' => $incomeid,
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'final_id' => $finalid
			);
			
			$this->mssql->insert('thickness_final_stud', $aUpdate);
		}
		
		public function update_final_thickness_stud($finalid=0){
			$aUpdate = array(
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'final_id' => $finalid
			);
			
			$this->mssql->where('thickness_test_id', $_POST['thickness_test_id']);
			$this->mssql->update('thickness_final_stud', $aUpdate);
		}
		
		public function save_cer_thickness_stud($finalid=0){
			$aInsert = array(
				'income_id' => $_POST['income_id'][0],
				'thickness_id' => ($_POST['thickness_id'])?$_POST['thickness_id']:0,
				'thickness_value' => ($_POST['thickness_value'])?$_POST['thickness_value']:'-',
				'thickness_createdtime' => date("Y-m-d H:i:s"),
				'thickness_createdip' => $this->input->ip_address(),
				'test_status' => $_POST['test_status'],
				'final_id' => $finalid
			);
			
			$this->mssql->insert('thickness_cer_stud', $aInsert);
		}
		
		public function update_stock_stud($incomeid=0, $finalid=0){
			$finalinfo = $this->mssql->where('final_stud_income_id', $incomeid)
										->where('final_id', $finalid)
										->get('final_bolt')->row_array();
			
			$final_stud_id = $finalinfo['final_stud_id'];
			$final_stud_sizeid = $finalinfo['final_stud_size_id'];
			$final_qty = $finalinfo['final_stud_qty'];
			
			$sizeinfo = $this->mssql->where('product_id', $final_stud_id)
									->where('size_id', $final_stud_sizeid)
									->get('product_size_stud')->row_array();
			
			$current_qty = $sizeinfo['size_qty'];
			
			$upd_qty = intval($current_qty-$final_qty);
			
			$aUpdate = array(
				'size_qty' => $upd_qty,
				'size_updatedtime' => date("Y-m-d H:i:s"),
				'size_updatedip' => $this->input->ip_address()
			);
			
			$this->mssql->where('size_id', $sizeinfo['size_id']);
			$this->mssql->update('product_size_stud', $aUpdate);
			
		}
		
		public function update_stock_main_stud($incomeid=0, $finalid=0){
			$finalinfo = $this->mssql->where('final_stud_income_id', $incomeid)
										->where('final_id', $finalid)
										->get('final_stud')->row_array();
			
			$final_stud_id = $finalinfo['final_stud_id'];
			$final_stud_sizeid = $finalinfo['final_stud_size_id'];
			$final_qty = $finalinfo['final_stud_qty'];
			
			$sizeinfo = $this->mssql->where('product_id', $final_stud_id)
									->where('size_id', $final_stud_sizeid)
									->get('product_size_stud')->row_array();
			
			$current_qty = $sizeinfo['size_qty'];
			
			$upd_qty = intval($current_qty-$final_qty);
			
			$aUpdate = array(
				'size_qty' => $upd_qty,
				'size_updatedtime' => date("Y-m-d H:i:s"),
				'size_updatedip' => $this->input->ip_address()
			);
			
			$this->mssql->where('size_id', $sizeinfo['size_id']);
			$this->mssql->update('product_size_stud', $aUpdate);
			
		}
		/* Chemical test final - End */
	
	/* stud - End */
	
	public function get_thickness_value_byid($thicknessid=0){
		$query = $this->mssql->where('id', $thicknessid)
								->get('thickness_galvanizes')->row_array();
		return $query;
	}
	
}