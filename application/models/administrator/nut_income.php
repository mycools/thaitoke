<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
File Name 	: bolt_income.php
Controller	: Bolt_income
Create By 	: Jarak Kritkiattisak
Create Date 	: 7/6/2557 BE
Project 	: iAon Project
Version 		: 1.0
*/
class Nut_income extends CI_Model {
	var $_data=array();
	public function __construct()
	{
		parent::__construct();
		$this->mssql = $this->load->database("mssql",true);
	}
	public function dataTable($limit=100,$offset=0)
	{
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("income_nut.income_job_id",$q);
			$this->mssql->or_like("product_nut.product_name",$q);
			$this->mssql->or_like("income_nut.income_lot_no",$q);
		}
		$this->mssql->join("product_nut","product_nut.product_id = income_nut.income_product_id");
		$this->mssql->limit($limit,$offset);
		$this->mssql->order_by("income_id","DESC");
		$res = $this->mssql->get("income_nut")->result_array();
		return $res;
	}
	function getIncome($income_id)
	{
		$this->mssql->limit(1);
		$this->mssql->where("income_id",$income_id);

		$res = $this->mssql->get("income_nut")->row_array();
		return $res;
	}
	public function export_data()
	{
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("income_nut.income_job_id",$q);
			$this->mssql->or_like("product_nut.product_name",$q);
			$this->mssql->or_like("income_nut.income_lot_no",$q);
		}
		$this->mssql->join("product_nut","product_nut.product_id = income_nut.income_product_id");
		$res = $this->mssql->get("income_nut");
		return $res;
	}
	
	function get_running_number_nut(){
		
		$this->mssql->where("running_name","income_nut");
		$res = $this->mssql->get("running")->row_array();
		return $res;
		
	}

	public function getBoltAvailableStock($edited=false)
	{
		$this->mssql->select("lot_nut.lot_no,supplier.supplier_name,supplier.supplier_tel,product_size_nut.size_m,product_size_nut.size_p,product_size_nut.size_length,product_nut.product_name,product_nut.product_grade,product_nut.product_initial,product_nut.product_iso,lot_nut.lot_certificate_no,lot_nut.lot_commodity,lot_nut.lot_surface,lot_nut.lot_material,lot_nut.lot_amount,lot_nut.lot_heat_no,lot_nut.lot_color,lot_nut.lot_color_value,lot_nut.lot_remark,lot_nut.lot_createdid,lot_nut.lot_updatedid,lot_nut.lot_import_date,lot_nut.lot_remain,lot_nut.lot_id");
		$this->mssql->where("lot_nut.lot_status","approved");
		if($edited==false)
		{
			$this->mssql->where("lot_nut.lot_remain >","0");
		}
		$this->mssql->order_by("lot_nut.lot_id","desc");
		$this->mssql->join("product_nut","product_nut.product_id=lot_nut.product_id","LEFT");
		$this->mssql->join("product_size_nut","product_size_nut.size_id = lot_nut.size_id","LEFT");
		$this->mssql->join("supplier","supplier.supplier_id = lot_nut.supplier_id","LEFT");

		$product = $this->mssql->get("lot_nut")->result_array();
		return $product;	
	}
	public function getNutAvailableStock($edited=false)
	{
		$this->mssql->select("lot_nut.lot_no,supplier.supplier_name,supplier.supplier_tel,product_size_nut.size_m,product_size_nut.size_p,product_size_nut.size_length,product_nut.product_name,product_nut.product_grade,product_nut.product_initial,product_nut.product_iso,lot_nut.lot_certificate_no,lot_nut.lot_commodity,lot_nut.lot_surface,lot_nut.lot_material,lot_nut.lot_amount,lot_nut.lot_heat_no,lot_nut.lot_color,lot_nut.lot_color_value,lot_nut.lot_remark,lot_nut.lot_createdid,lot_nut.lot_updatedid,lot_nut.lot_import_date,lot_nut.lot_remain,lot_nut.lot_id");
		$this->mssql->where("lot_nut.lot_status","approved");
		if($edited==false)
		{
			$this->mssql->where("lot_nut.lot_remain >","0");
		}
		$this->mssql->order_by("lot_nut.lot_id","desc");
		$this->mssql->join("product_nut","product_nut.product_id=lot_nut.product_id","LEFT");
		$this->mssql->join("product_size_nut","product_size_nut.size_id = lot_nut.size_id","LEFT");
		$this->mssql->join("supplier","supplier.supplier_id = lot_nut.supplier_id","LEFT");

		$product = $this->mssql->get("lot_nut")->result_array();
		return $product;	
	}
	public function getWasherAvailableStock($edited=false)
	{
		$this->mssql->select("lot_washer.lot_no,supplier.supplier_name,supplier.supplier_tel,product_size_nut.size_m,product_size_nut.size_p,product_size_nut.size_length,product_nut.product_name,product_nut.product_grade,product_nut.product_initial,product_nut.product_iso,lot_washer.lot_certificate_no,lot_washer.lot_commodity,lot_washer.lot_surface,lot_washer.lot_material,lot_washer.lot_amount,lot_washer.lot_heat_no,lot_washer.lot_color,lot_washer.lot_color_value,lot_washer.lot_remark,lot_washer.lot_createdid,lot_washer.lot_updatedid,lot_washer.lot_import_date,lot_washer.lot_remain,lot_washer.lot_id");
		$this->mssql->where("lot_washer.lot_status","approved");
		if($edited==false)
		{
			$this->mssql->where("lot_washer.lot_remain >","0");
		}
		$this->mssql->order_by("lot_washer.lot_id","desc");
		$this->mssql->join("product_nut","product_nut.product_id=lot_washer.product_id","LEFT");
		$this->mssql->join("product_size_nut","product_size_nut.size_id = lot_washer.size_id","LEFT");
		$this->mssql->join("supplier","supplier.supplier_id = lot_washer.supplier_id","LEFT");

		$product = $this->mssql->get("lot_washer")->result_array();	
		return $product;	
	}
	public function add_income()
	{
		//$income_job_id=trim(strip_tags($this->input->post("income_job_id")));
		//$income_sale_order_no=trim(strip_tags($this->input->post("income_sale_order_no")));
		$supplier_id=trim(strip_tags($this->input->post("supplier_id")));
		$income_invoice_no=trim(strip_tags($this->input->post("income_invoice_no")));
		$income_purchase_no=trim(strip_tags($this->input->post("income_purchase_no")));
		$income_material=trim(strip_tags($this->input->post("income_material")));
		$income_commodity=trim(strip_tags($this->input->post("income_commodity")));
		$income_certificate_no=trim(strip_tags($this->input->post("income_certificate_no")));
		$income_po_no=trim(strip_tags($this->input->post("income_po_no")));
		$income_delivery_no=trim(strip_tags($this->input->post("income_delivery_no")));
		$income_heat_no=trim(strip_tags($this->input->post("income_heat_no")));
		$income_lot_no=trim(strip_tags($this->input->post("income_lot_no")));
		$product_id=trim(strip_tags($this->input->post("product_id")));
		$size_id=trim(strip_tags($this->input->post("size_id")));
		//$surface_condition=trim(strip_tags($this->input->post("surface_condition")));
		$quantity=trim(strip_tags($this->input->post("quantity")));
		$grade_mark=trim(strip_tags($this->input->post("grade_mark")));
	
		$income_createdtime=date("Y-m-d H:i:s");
		$income_createdip=$this->input->ip_address();
		$income_createdid=$this->admin_library->user_id();
		
		$pro_name = $this->getproduct_name($product_id);
		$size_m = $this->get_size($size_id);
		
		$running = $this->get_running_number_nut();

		
		$income_job_id = $pro_name['product_initial']."M".$size_m['size_m']."-".date("Y").$running['running_number'];

		
		$this->mssql->set("income_job_id",$income_job_id);
		//$this->mssql->set("income_sale_order_no",$income_sale_order_no);
		$this->mssql->set("income_supplier_id",$supplier_id);
		$this->mssql->set("income_invoice_no",$income_invoice_no);
		$this->mssql->set("income_purchase_no",$income_purchase_no);
		$this->mssql->set("income_material",$income_material);
		$this->mssql->set("income_commodity",$income_commodity);
		$this->mssql->set("income_certificate_no",$income_certificate_no);
		$this->mssql->set("income_po_no",$income_po_no);
		$this->mssql->set("income_delivery_no",$income_delivery_no);
		$this->mssql->set("income_heat_no",$income_heat_no);
		$this->mssql->set("income_lot_no",$income_lot_no);
		$this->mssql->set("income_product_id",$product_id);
		$this->mssql->set("size_id",$size_id);
		$this->mssql->set("grade_mark",$grade_mark);
		$this->mssql->set("income_quantity",$quantity);


		
		$this->mssql->set("income_createdtime",$income_createdtime);
		$this->mssql->set("income_createdip",$income_createdip);
		$this->mssql->set("income_createdid",$income_createdid);
		$this->mssql->insert("income_nut");
		
		$new_number = $running['running_number']+1;
		$new_number = sprintf("%06d", $new_number);
		$this->mssql->set("running_number",$new_number);
		$this->mssql->where("running_name","income_nut");
		$this->mssql->update("running");
					
	}
	public function edit_income()
	{
		$income_id=trim(strip_tags($this->input->post("income_id")));
		$supplier_id=trim(strip_tags($this->input->post("supplier_id")));
		$income_invoice_no=trim(strip_tags($this->input->post("income_invoice_no")));
		$income_purchase_no=trim(strip_tags($this->input->post("income_purchase_no")));
		$income_material=trim(strip_tags($this->input->post("income_material")));
		$income_commodity=trim(strip_tags($this->input->post("income_commodity")));
		$income_certificate_no=trim(strip_tags($this->input->post("income_certificate_no")));
		$income_po_no=trim(strip_tags($this->input->post("income_po_no")));
		$income_delivery_no=trim(strip_tags($this->input->post("income_delivery_no")));
		$income_heat_no=trim(strip_tags($this->input->post("income_heat_no")));
		$income_lot_no=trim(strip_tags($this->input->post("income_lot_no")));
		$income_createdtime=date("Y-m-d H:i:s");
		$income_createdip=$this->input->ip_address();
		$income_createdid=$this->admin_library->user_id();
		$grade_mark=trim(strip_tags($this->input->post("grade_mark")));
		
		
		$this->mssql->set("income_supplier_id",$supplier_id);
		$this->mssql->set("income_invoice_no",$income_invoice_no);
		$this->mssql->set("income_purchase_no",$income_purchase_no);
		$this->mssql->set("income_material",$income_material);
		$this->mssql->set("income_commodity",$income_commodity);
		$this->mssql->set("income_certificate_no",$income_certificate_no);
		$this->mssql->set("income_po_no",$income_po_no);
		$this->mssql->set("income_delivery_no",$income_delivery_no);
		$this->mssql->set("income_heat_no",$income_heat_no);
		$this->mssql->set("income_lot_no",$income_lot_no);
		$this->mssql->set("grade_mark",$grade_mark);
		$this->mssql->set("income_updatedtime",$income_createdtime);
		$this->mssql->set("income_updatedip",$income_createdip);
		$this->mssql->set("income_updatedid",$income_createdid);
		$this->mssql->where("income_id",$income_id);
		$this->mssql->update("income_nut");
		
	}
	function delete_income($income_id)
	{
		
		$this->mssql->where("income_id",$income_id);
		$this->mssql->delete("income_nut");
		
	}
	
	public function getproduct_name($pro_id=0)
	{
		$this->mssql->limit(1);
		$this->mssql->where("product_id",$pro_id);
		$rs = $this->mssql->get("product_nut")->row_array();
		return $rs;
	}
	
	public function getsupplier_name($sup_id=0)
	{
		$this->mssql->limit(1);
		$this->mssql->where("supplier_id",$sup_id);
		$rs = $this->mssql->get("supplier")->row_array();
		return $rs;
	}
	
	public function getSupplier()
	{
		$rs = $this->mssql->get("supplier")->result_array();
		return $rs;
	}
	public function getCustomer()
	{
		$rs = $this->mssql->get("customer")->result_array();
		return $rs;
	}
	
	public function get_nutsize_byid($nutid=0){
		$query = $this->mssql->where('product_id', $nutid)
							->get('product_size_nut')->result_array();
		return $query;
	}
	function get_size($size_id)
	{
		$query = $this->mssql->where('size_id', $size_id)
							->get('product_size_nut')->row_array();
		return $query;
	}
}