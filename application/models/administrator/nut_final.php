<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
File Name 	: nut_final.php
Controller	: nut_final
Create By 	: Jarak Kritkiattisak
Create Date 	: 7/6/2557 BE
Project 	: iAon Project
Version 		: 1.0
*/
class Nut_final extends CI_Model {
	var $_data=array();
	public function __construct()
	{
		parent::__construct();
		$this->mssql = $this->load->database("mssql",true);
	}
	public function dataTable($limit=100,$offset=0)
	{
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("final_nut.final_invoice_no",$q);
			$this->mssql->or_like("product_nut.product_name",$q);
		}
		$this->mssql->join("product_nut","product_nut.product_id = final_nut.final_nut_id");
		$this->mssql->limit($limit,$offset);
		$this->mssql->order_by("final_id","DESC");
		$res = $this->mssql->get("final_nut")->result_array();
		return $res;
	}
	function getfinal($final_id)
	{
		$this->mssql->limit(1);
		$this->mssql->where("final_id",$final_id);
		$res = $this->mssql->get("final_nut")->row_array();
		return $res;
	}
	
	public function export_data()
	{
		$q = $this->input->get("q");
		if(trim($q)){
			$this->mssql->like("final_nut.final_invoice_no",$q);
			$this->mssql->or_like("product_nut.product_name",$q);
		}
		$this->mssql->join("product_nut","product_nut.product_id = final_nut.final_nut_id");
		$res = $this->mssql->get("final_nut");
		return $res;
	}
	
	function get_nut_qty($pro_id,$size_id){
		
		$this->mssql->where("product_id",$pro_id);
		$this->mssql->where("size_id",$size_id);
		$res = $this->mssql->get("product_size_nut")->row_array();
		return $res;
		
	}
	
	function get_running_number(){
		
		$this->mssql->where("running_name","final_nut");
		$res = $this->mssql->get("running")->row_array();
		return $res;
		
	}
	
	public function add_final()
	{
		//$final_job_id=trim(strip_tags($this->input->post("final_job_id")));
		//$final_sale_order_no=trim(strip_tags($this->input->post("final_sale_order_no")));
		$customer_id=trim(strip_tags($this->input->post("customer_id")));
		$final_invoice_no=trim(strip_tags($this->input->post("final_invoice_no")));
		$final_po_no=trim(strip_tags($this->input->post("final_po_no")));
		$final_delivery_no=trim(strip_tags($this->input->post("final_delivery_no")));
		$final_purchase_no=trim(strip_tags($this->input->post("final_purchase_no")));
		$final_sale_order_no=trim(strip_tags($this->input->post("final_sale_order_no")));
		$final_nut_id=trim(strip_tags($this->input->post("final_nut_id")));
		$final_nut_size_id=trim(strip_tags($this->input->post("final_nut_size_id")));
		$final_nut_income_id=trim(strip_tags($this->input->post("final_nut_income_id")));
		$final_nut_qty=trim(strip_tags($this->input->post("final_nut_qty")));
		//$final_date_edit = date("Y-m-d",strtotime($this->input->post("final_date_edit")));
		$final_date_edit = todbdate($this->input->post("final_date_edit"));
	
		$nut_qty = $this->get_nut_qty($final_nut_id,$final_nut_size_id);
		// echo $bolt_qty['size_qty']."<".$final_bolt_qty;
		// exit();
		 
		 
		 if($nut_qty['size_qty'] < $final_nut_qty){
			 $this->session->set_flashdata("message-warning","สินค้ามีไม่พอให้ตัดสต็อก");
			admin_redirect("final_manager/final_nut_list");
			 
			 
		 }
		 
		$final_createdtime=date("Y-m-d H:i:s");
		$final_createdip=$this->input->ip_address();
		$final_createdid=$this->admin_library->user_id();
		
		$pro_name = $this->getproduct_name($final_nut_id);
		$size_m = $this->get_size($final_nut_size_id);
		
		$running = $this->get_running_number();
		
		
		
		
		$final_job_id = $pro_name['product_initial']."M".$size_m['size_m']."-".date("Y").$running['running_number'];
		
		$this->mssql->set("final_job_id",$final_job_id);
		//$this->mssql->set("final_sale_order_no",$final_sale_order_no);
		$this->mssql->set("customer_id",$customer_id);
		$this->mssql->set("final_invoice_no",$final_invoice_no);
		$this->mssql->set("final_purchase_no",$final_purchase_no);
		$this->mssql->set("final_sale_order_no",$final_sale_order_no);
		$this->mssql->set("final_po_no",$final_po_no);
		$this->mssql->set("final_delivery_no",$final_delivery_no);
		
		$this->mssql->set("final_nut_id",$final_nut_id);
		$this->mssql->set("final_nut_size_id",$final_nut_size_id);
		$this->mssql->set("final_nut_income_id",$final_nut_income_id);
		$this->mssql->set("final_nut_qty",$final_nut_qty);

		$this->mssql->set("final_date_edit",$final_date_edit);
		$this->mssql->set("final_createdtime",$final_createdtime);
		$this->mssql->set("final_createdip",$final_createdip);
		$this->mssql->set("final_createdid",$final_createdid);
		$this->mssql->insert("final_nut");
		
		
		$new_number = $running['running_number']+1;
		$new_number = sprintf("%06d", $new_number);
		$this->mssql->set("running_number",$new_number);
		$this->mssql->where("running_name","final_nut");
		$this->mssql->update("running");

		 
		 
					
	}
	public function edit_final()
	{
		$final_id=trim(strip_tags($this->input->post("final_id")));
		$customer_id=trim(strip_tags($this->input->post("customer_id")));
		$final_invoice_no=trim(strip_tags($this->input->post("final_invoice_no")));
		$final_po_no=trim(strip_tags($this->input->post("final_po_no")));
		$final_delivery_no=trim(strip_tags($this->input->post("final_delivery_no")));
		$final_purchase_no=trim(strip_tags($this->input->post("final_purchase_no")));
		$final_sale_order_no=trim(strip_tags($this->input->post("final_sale_order_no")));
		
		$final_date_edit = todbdate($this->input->post("final_date_edit"));
		
		$final_createdtime=date("Y-m-d H:i:s");
		$final_createdip=$this->input->ip_address();
		$final_createdid=$this->admin_library->user_id();
		
		$this->mssql->set("customer_id",$customer_id);
		$this->mssql->set("final_invoice_no",$final_invoice_no);
		$this->mssql->set("final_purchase_no",$final_purchase_no);
		$this->mssql->set("final_sale_order_no",$final_sale_order_no);
		$this->mssql->set("final_po_no",$final_po_no);
		$this->mssql->set("final_delivery_no",$final_delivery_no);
		$this->mssql->set("final_date_edit",$final_date_edit);

		$this->mssql->set("final_updatedtime",$final_createdtime);
		$this->mssql->set("final_updatedip",$final_createdip);
		$this->mssql->set("final_updatedid",$final_createdid);
		$this->mssql->where("final_id",$final_id);
		$this->mssql->update("final_nut");
					
	}
	function delete_final($final_id)
	{
		
		
		$this->mssql->where("final_id",$final_id);
		$this->mssql->delete("final_nut");
		
			}

	
	public function getproduct_name($pro_id=0)
	{
		$this->mssql->limit(1);
		$this->mssql->where("product_id",$pro_id);
		$rs = $this->mssql->get("product_nut")->row_array();
		return $rs;
	}
	
	public function getcustomer_name($cus_id=0)
	{
		$this->mssql->limit(1);
		$this->mssql->where("customer_id",$cus_id);
		$rs = $this->mssql->get("customer")->row_array();
		return $rs;
	}
	
	public function getSupplier()
	{
		$rs = $this->mssql->get("supplier")->result_array();
		return $rs;
	}
	public function getCustomer()
	{
		$rs = $this->mssql->get("customer")->result_array();
		return $rs;
	}
	
	public function get_nutsize_byid($nutid=0){
		$query = $this->mssql->where('product_id', $nutid)
							->get('product_size_nut')->result_array();
		return $query;
	}
	
	public function get_nutincome_byid($nutid=0,$nutsizeid=0){
						$this->mssql->where('income_product_id', $nutid);
						$this->mssql->where('size_id', $nutsizeid);
						$this->mssql->where('test_status', "2");
						$this->mssql->order_by('income_id','DESC');
			$query =	$this->mssql->get('income_nut')->result_array();
			
		return $query;
		
	}
	function get_size($size_id)
	{
		$query = $this->mssql->where('size_id', $size_id)
							->get('product_size_nut')->row_array();
		return $query;
	}
}