<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Income_test_anchor_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		$this->mssql = $this->load->database("mssql", true);
		
	}
	##################### income test anchor #############################
	
		
	public function get_income_anchor($income_id)
	{
		$this->mssql->where("income_id",$income_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("income_anchor")->row_array(); 
		return $row; 
	}
	public function get_anchor_product($income_id)
	{
		$income = $this->get_income_anchor($income_id);
		
		$this->mssql->where("product_id",$income['income_product_id']);
		$this->mssql->limit(1);
		$row = $this->mssql->get("product_anchor")->row_array(); 
		return $row; 
	}
	
	public function get_income_anchor_test_info($income_id,$std_id)
	{
		$this->mssql->where("income_id",$income_id);
		$this->mssql->where("anchor_standard_id",$std_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("income_anchor_testing")->row_array(); 
		return $row;	
		
		
	}
	
	public function get_income_anchor_size_test_info($income_id,$std_id)
	{
		$this->mssql->where("income_id",$income_id);
		$this->mssql->where("anchor_cer_size_id",$std_id);
		$this->mssql->limit(1);
		$row = $this->mssql->get("size_anchor_testing")->row_array(); 
		return $row;	
		
		
	}
	
	public function get_product_standard_anchor($product_id)
	{
		//$this->mssql->where('(ps_min <> NULL OR ps_max <> NULL)');
		$this->mssql->where("product_id",$product_id);
		$row = $this->mssql->get("product_standard_anchor");
		return $row;
	}
	
	public function get_product_standard_size_anchor($product_id,$size_id)
	{
		/* $this->mssql->where('(ps_min <> NULL OR ps_max <> NULL)'); */
		$this->mssql->where("product_id",$product_id);
		$this->mssql->where("size_id",$size_id);
		$row = $this->mssql->get("product_cer_size_anchor"); 
		return $row;
	}
	
	public function add_anchor_testing_std($data)
	{
		/*
var_dump(@$_POST);
		exit();
*/		
		if(@$_POST['anchor_standard_id'] != NULL){
			foreach(@$_POST['anchor_standard_id'] as $key=>$value){
				$this->mssql->set('income_id',@$_POST['income_id']);
				$this->mssql->set('anchor_standard_id',@$_POST['anchor_standard_id'][$key]);
				$this->mssql->set('anchor_testing1',@$_POST['test1'][$key]);
				$this->mssql->set('anchor_testing2',@$_POST['test2'][$key]);
				$this->mssql->set('anchor_testing3',@$_POST['test3'][$key]);
				$this->mssql->set('anchor_testing4',@$_POST['test4'][$key]);
				$this->mssql->set('anchor_testing5',@$_POST['test5'][$key]);
				$this->mssql->set('anchor_testing_status',@$_POST['testing_status'][$key]);
				$this->mssql->set('anchor_testing_remark',@$_POST['remark'][$key]);
				$this->mssql->set('anchor_testing_post_user_id',$data['anchor_testing_post_user_id']);	
				$this->mssql->set('anchor_testing_post_date',"SYSDATETIME()",FALSE);		
				$this->mssql->insert('income_anchor_testing');
			}
			
			if($_POST['test_status']== "2"){
				$this->mssql->set("test_status",$_POST['test_status']);
				$this->mssql->where("income_id",$_POST['income_id']);
				$this->mssql->update("income_anchor");
				
				return "pass";
			}else{
				$this->mssql->set("test_status",$_POST['test_status']);
				$this->mssql->where("income_id",$_POST['income_id']);
				$this->mssql->update("income_anchor");	
			}
			
			
		}
		
		
		
	}
	
	
	
	public function update_anchor_testing_std($data)
	{
		
		if(@$_POST['anchor_standard_id'] != NULL){
			foreach(@$_POST['anchor_standard_id'] as $key=>$value){
				
				$this->mssql->set('anchor_testing1',@$_POST['test1'][$key]);
				$this->mssql->set('anchor_testing2',@$_POST['test2'][$key]);
				$this->mssql->set('anchor_testing3',@$_POST['test3'][$key]);
				$this->mssql->set('anchor_testing4',@$_POST['test4'][$key]);
				$this->mssql->set('anchor_testing5',@$_POST['test5'][$key]);
				$this->mssql->set('anchor_testing_status',@$_POST['testing_status'][$key]);
				$this->mssql->set('anchor_testing_remark',@$_POST['remark'][$key]);
				$this->mssql->set('anchor_testing_post_user_id',$data['anchor_testing_post_user_id']);	
				$this->mssql->set('anchor_testing_post_date',"SYSDATETIME()",FALSE);
				$this->mssql->where("income_id",@$_POST['income_id']);
				$this->mssql->where("anchor_standard_id",@$_POST['anchor_standard_id'][$key]);
				$this->mssql->update('income_anchor_testing');
			}
			
			if($_POST['test_status'] == "2"){
				$this->mssql->set("test_status",$_POST['test_status']);
				$this->mssql->where("income_id",$_POST['income_id']);
				$this->mssql->update("income_anchor");
				
				return "pass";
			}else{
				$this->mssql->set("test_status",$_POST['test_status']);
				$this->mssql->where("income_id",$_POST['income_id']);
				$this->mssql->update("income_anchor");
			}
			
		}
			
		
		
	}

	
	public function delete_supplier($supplier_id)
	{
		

		$this->mssql->where("supplier_id",$supplier_id);
	    $this->mssql->delete("supplier");	
	}
	
	public function check_income_tested($income_id)
	{
			   $this->mssql->where("income_id",$income_id);
		return $this->mssql->count_all_results("income_anchor_testing");
	}
	public function get_income_tested($income_id,$std_id)
	{
		$this->mssql->where("anchor_standard_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("income_anchor_testing")->row_array();
	}
	
	public function get_income_cer_size_tested($income_id,$std_id){
		$this->mssql->where("anchor_cer_size_id",$std_id);
		$this->mssql->where("income_id",$income_id);
		return $this->mssql->get("size_anchor_testing")->row_array();	
	}
	
	
	
	
	public function update_product_qty($size_id=0,$qty=0, $incomeid=0){
	
			
			
			$row = $this->mssql->where('product_size_anchor.size_id', $size_id)
									->get('product_size_anchor')->row_array();

			$old_qty = $row['size_qty'];
			$total = $old_qty+$qty;
			
			$aUpdate = array(
				'size_qty' => $total
			);
			
			$this->mssql->where("size_id",$size_id);
			$this->mssql->update("product_size_anchor", $aUpdate);
			
			
		
		return $total;
		
	}
	
	public function update_product_qty_edit($size_id=0,$qty=0, $incomeid=0){
	
			$tested = $this->check_income_tested($incomeid);
			
			if($tested<=0){
			
			$row = $this->mssql->where('product_size_anchor.size_id', $size_id)
									->get('product_size_anchor')->row_array();

			$old_qty = $row['size_qty'];
			$total = $old_qty+$qty;
			
			$aUpdate = array(
				'size_qty' => $total
			);
			
			$this->mssql->where("size_id",$size_id);
			$this->mssql->update("product_size_anchor", $aUpdate);
			
			}
		
		return $total;
		
	}
	
	
	
	##################### end test anchor #############################
	
	
	


}