# README #

## THAI TOKE ##

### Software spec ###

* CSS 3.0
* HTML,HTML5
* Javascript 2.0
* Jquery 1.6+
* PHP 5.4+
* GD Library
* SMTP Enabled
* MySQL Server 5.5+ or MariaDB 10.1+
* Linux / Windows / OSX
* Apache 2.2+ or Nginx 1.6+ with PHP-FPM
* 1GB Disk space required.
* Best performance on SSD Storage
* 50MB Upload file allow
* Rewrite url enabled on Apache
* Safari, Google Chome, Firefox, Opera, Internet Explorer 10+,MS Edge

### How to install ###

* 1.) Download Source file as zip and extract this.
* 2.) Upload all file to webroot directory
* 3.) Create new database via phpMyAdmin or MySQL Management software and restore sqlfile ./database/you2play_db.sql
* 4.) Edit file ./application/configs/database.php to change database config on group "production"  
```php
$db['production']['hostname'] = 'localhost';
$db['production']['username'] = 'mysql_username';
$db['production']['password'] = 'mysql_password';
$db['production']['database'] = 'databse_name'; 
```
* 5.) Edit file ./index.php to change ENVIRONMENT to "production"
```php
<?php
@date_default_timezone_set("Asia/Bangkok");
$cms_dyn_route = array();
/*
 *---------------------------------------------------------------
 * APPLICATION ENVIRONMENT
 *---------------------------------------------------------------
 *
 * You can load different configurations depending on your
 * current environment. Setting the environment also influences
 * things like logging and error reporting.
 *
 * This can be set to anything, but default usage is:
 *
 *     development
 *     testing
 *     production
 *
 * NOTE: If you change these, also change the error_reporting() code below
 *
 */
 	//$ENVIRONMENT = (@$_SERVER['SERVER_ADDR']=="127.0.0.1" || !isset($_SERVER['SERVER_ADDR']))?'development':'production';
 	//Set for Testing Mode
 	$ENVIRONMENT="production";
	define('ENVIRONMENT', $ENVIRONMENT);
```
* 6.) Test request website http://yourdomainname.tld/ and go!
* 7.) Do shout “Hurray!” when you see the website after you install website success. 

### License ###

* Base sofware under license "EllisLab" [Terms of Service](https://www.codeigniter.com/help)

### Technical Support ###
Mycools Inc.

9/301 Pruksa-Prime 94 Saimai 56, Saimai Rd., Saimai, Bangkok 10220.

Email : jarak.krit@gmail.com

Call US : (+66) 9512 46999